/*
	dfahlivehistorydata.js
	Live data poller from JSON file prepared by DogecoinFah

	Requires:
		dfahgeneral.js (waitMainReady, loadFile, mfFileName)
		dfahtextmanager.js (getTextAsset)
		dfahlivedata.js (initLiveData, [branding configuration])
		dfahtxidlist.js (txidListFind)
		dfahworkerlist.js (getWorkerName)
		dfahlivefolderhistory.js (searchFolderData)
		

	Data files:
		mfAsset.pubdatRoundsIndex  (data/PublicRoundData.json)
		mfAsset.pubdatRounds       (data/PublicRoundData_%N.json)

	This file loads and displays the Round history data. It also
	provides interface and entry to folder history searching.
	 
	Folder History Searching:
	 
	This feature is provided by dfahlivefolderhistory.js including
	the generation of output text/HTML.
	 
	Round History Data:
	
	The index file lists which round ids are contained in
	each block file. Each block file contains data for multiple rounds
	grouped in to reasonable file sizes.
	
	Round data includes all round specifics, contribution information,
	market values, and data for every worker (folder).
	 
	Wallet addresses for workers (aka folders) are stored as indexes
	in this round data. They are looked up by utilizing features
	from dfahworkerlist.js , which loaded worker addresses using a
	similar file strategy of an index file and multiple block data files.
	
	Transaction IDs (aka Txid's) also are stored as indexes in this
	round data. They also are looked up by utilizing features of
	another script, dfahtxidlist.js , which stores the txid hashes
	packed tightly in their value's binary representation.
	

*/
var histUpdateTimeout = null;

var roundData = {
					initting: false,
					initStep: 0,
					ready: false,
					urlParams: false,
					index: {
						loading: false,
						loaded: false,
						version: 0,
						blockCount: 0
					},
					block: {
						loading: false,
						loaded: false,
						scrollTo: false,
						selBlock: 0,
						trySelBlock: 0,
						selRound: 0,
						trySelRound: -1, // -1 for max
						txids: [],
						rounds: [],
						dtUpdated: ''
					},
					timers: {
						loadWorkers: null,
						loadWorkersTimeout: 0,
						loadWorkersResponse: null,
					},
					display: {
						sortCol: -2,
						sorting: false,
						showIdle: false,
						showNoReward: false,
						dtDelaySortTill: 0,
						dtDelayRoundSelectTill: 0
						}
				};


function roundDataInit(response = null)
{
	// ------------------
	roundData.initting = true;
	roundData.initStep = 0;
	// ------------------
	var innerResponse = {
			onSuccess: function(resp)
						{
							//console.log('roundDataInit step '+roundData.initStep+' onSuccess');
							roundData.initStep++;
							var ok = true;
							switch (roundData.initStep)
							{
								case 1:
									if (!initLiveData(resp))
									{
										console.log('roundDataInit initLiveData failed');
										ok = false;
									}
									break;
								case 2:
									// initLiveData loaded the Text Manager.
									// Now we can  update display with
									// the 'loading...' message.
									buildRoundData();
									if (!initWorkers(true /*wantValid*/, true /*wantInvalid*/, resp))
									{
										console.log('roundDataInit initWorkers failed');
										ok = false;
									}
									break;								
								case 3:
									if (!txidListInit(resp))
										ok = false;
									break;
								case 4:
									if (!roundDataLoadIndex(resp))
										ok = false;
									roundData.urlParams = new URLSearchParams(window.location.search);
									break;
								case 5:
									var urlSelRndID = (roundData.urlParams.has('r') ? roundData.urlParams.get('r') : null);
									urlSelRndID = (urlSelRndID === null ? NaN : parseInt(urlSelRndID));
									if (!isNaN(urlSelRndID))
										if (!rdSelectRoundID(urlSelRndID))
											console.log('roundDataInit url sel rnd ID not found');
										else
											roundData.block.scrollTo = true; // found, scroll to it
									// trySelBlock was defaulted to last when index was loaded
									if (!roundDataLoadBlock(roundData.block.trySelBlock, resp))
										ok = false;
									break;
								case 6:
									roundData.ready = true;
									var folderSearch = (roundData.urlParams.has('s') ? roundData.urlParams.get('s') : null);
									if (folderSearch === null || folderSearch === '')
									{
										var ret = preInitFolderData(); // just does default draw, load/init's on demand
										if (ret !== true)
										{
											console.log('roundDataInit preInitFolderData failed: '+ret);
											ok = false;
										}
									}
									else if (!searchFolderData(folderSearch)) // existing query, Folder data load/init's on demand
									{
										console.log('roundDataInit searchFolderData failed'); // actual fail, succss if not found
										ok = false;
									}
									roundData.initting = false;
									buildRoundData();
									if (ok)
									{
										// done, send main response
										if (isObject(response) && response.hasOwnProperty('onSuccess'))
											response.onSuccess(response);
									}
									break;
							} // switch
							if (!ok)
							{
								console.log('roundDataInit step '+roundData.initStep+' failed');
								if (isObject(response) && response.hasOwnProperty('onFail'))
									response.onFail(response);
							}
						},
			onFail: function(resp)
						{
							roundData.initting = false;
							roundData.ready = false;
							console.log('roundDataInit step '+roundData.initStep+' onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
		};
	// ------------------
	waitMainReady(innerResponse);
	// ------------------
	return true;
}

function roundDataGetURLParams(reponse = null)
{
	if (!roundData.ready)
	{
		console.log('roundDataURLParams not ready');
		return false;
	}	
	roundData.urlParams = new URLSearchParams(window.location.search);
	return true;	
}

function roundDataLoadIndex(response = null)
{
	if (roundData.index.loaded)
	{
		console.log('roundDataLoadIndex already loaded');
		return false;
	}
	if (roundData.index.loading)
	{
		console.log('roundDataLoadIndex already loading');
		return false;
	}
	roundData.index.loading = true;

	var fileName = mfFileName(mfAsset.pubdatRoundsIndex);
	if (!loadFile(fileName, roundDataHandleIndex, response, false /*isBinary*/))
	{
		roundData.index.loading = false;
		console.log('roundDataLoadIndex loadFile failed');
		return false;
	}
	return true;
}

function roundDataLoadBlock(block = null, response = null)
{
	if (!roundData.index.loaded)
	{
		console.log('roundDataLoadBlock index not loaded');
		return false;
	}
	if (roundData.block.loading)
	{
		console.log('roundDataLoadBlock block already loading');
		return false;
	}
	roundData.block.loaded = false;
	roundData.block.loading = true;
	roundData.block.trySelBlock = (isNaN(block) ? 0 : block);
	//console.log("Requesting block: " + roundData.block.trySelBlock);
	var fileName = mfFileName(mfAsset.pubdatRounds, roundData.block.trySelBlock);
	if (!loadFile(fileName, roundDataHandle, response, false /*isBinary*/))
	{
		roundData.block.loading = false;
		console.log('roundDataLoadBlock loadFile failed');
		return false;
	}
	return true;
}

function roundDataHandleIndex(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = roundDataParseIndex(data);
	roundData.index.loading = false;
	if (ret !== true)
	{
		console.log('roundDataParseIndex failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		roundData.index.loaded = true;
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}	
}

function roundDataHandle(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = roundDataParse(data);
	if (ret !== true)
	{
		roundData.block.loading = false;
		roundData.block.loaded = false;
		console.log('roundDataHandle roundDataParse failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
		return;
	}
	roundData.block.selRound = (roundData.block.trySelRound === -1/*want max*/ ? (roundData.block.rounds.length == 0 ? 0 : (roundData.block.rounds.length - 1)) : roundData.block.trySelRound);
	var innerResponse = {
			onSuccess: function(resp)
						{
							roundData.block.loaded = true;
							roundData.block.loading = false;
							if (isObject(response) && response.hasOwnProperty('onSuccess'))
								response.onSuccess(response);
						},
			onFail: function(resp)
						{
							roundData.block.loading = false;
							roundData.block.loaded = false;
							console.log('roundDataHandle loadWorkers onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
					};
	roundData.timers.loadWorkersTimeout = Date.now() + 5000;
	roundDataLoadWorkers(innerResponse);
}

function roundDataParseIndex(data)
{
	if (!data)
		return 'data bad';
	var json = JSON.parse(data);
	if (!isObject(json))
		return 'data not an object';
	if (!json.hasOwnProperty('ver') || !json.hasOwnProperty('block_count'))
		return 'required fields missing';
	roundData.index.version    = parseInt(json.ver);
	if (roundData.index.version != 2)
		return 'version 2 required, unsupported: ' + roundData.index.version;
	roundData.index.blockCount = parseInt(json.block_count);
	roundData.index.roundCount = parseInt(json.round_count);
	roundData.index.block = [];
	for (b = 0;b < json.block_index.length;b++)
	{
		blockIdxData = json.block_index[b];
		roundData.index.block[b] = { fileSize: blockIdxData.bfs, rounds: [] };
		blockRounds = blockIdxData.rid;
		for (r = 0;r < blockRounds.length;r++)
			roundData.index.block[b].rounds[r] = parseInt(blockRounds[r]);			
	}	
	roundData.block.trySelBlock = roundData.index.blockCount - 1;
	if (roundData.block.trySelBlock < 0)
		roundData.block.trySelBlock = 0;
	return true;
}

function roundDataParse(data)
{
	if (!data)
		return 'data bad';
	var json = JSON.parse(data);
	var dtUpdated = new Date(parseInt(json.ts) * 1000);
	roundData.block.dtUpdated = dtUpdated.toLocaleString(undefined/*def local*/, {timeZoneName: 'short'});
	var txidNone = 0;
	var txidStored = 1;
	var txids = [getTextAsset('historydata/txid/error', '[error]'), getTextAsset('historydata/txid/notfound', '[not found]'), getTextAsset('historydata/txid/none', '[none]'), getTextAsset('historydata/txid/stored', '[stored]')];
	var round = [];
	var lastTxDataIdx;
	var lastTxIdx;
	// temps
	var id;
	var curTxDataIdx;
	var curTxIdx;
	var points;
	var statPay;
	var pay;
	for (var r = 0;r < json.rounds.length;r++)
	{
		var rdata = json.rounds[r];
		round[r] = {};
		round[r].id			= parseInt(rdata.id);
		round[r].comment	= rdata.cm;
		round[r].dtStarted	= new Date(parseInt(rdata.tsst) * 1000);
		round[r].dtPoll 	= new Date(parseInt(rdata.tssp) * 1000);
		round[r].dtPay 		= new Date(parseInt(rdata.tspd) * 1000);
		round[r].tPoints 	= parseInt(rdata.tpt);
		round[r].tPay 		= parseFloat(rdata.tpy);
		round[r].paidWorkers = parseInt(rdata.cpy);
		round[r].minPoints 	= parseInt(rdata.mnpt);
		round[r].maxPoints 	= parseInt(rdata.mxpt);
		round[r].avgPoints 	= parseInt(rdata.apt);
		round[r].minPay 	= parseFloat(rdata.mnpy);
		round[r].maxPay 	= parseFloat(rdata.mxpy);
		round[r].avgPay 	= parseFloat(rdata.apy);
		round[r].workers 	= [];
		round[r].contribs	= [];
		round[r].market		= (typeof rdata.mkt !== 'undefined' ? rdata.mkt : []);
		round[r].txid 		= 2; // none

		lastTxDataIdx = -999;
		lastTxIdx = 2; // none
		for (var w = 0;w < rdata.wd.length;w++)
		{
			workerData = rdata.wd[w];
			id = parseInt(workerData.w);
			curTxDataIdx = parseInt(workerData.tx);
			points = parseFloat(workerData.pt);
			if (isNaN(points))
				points = workerData.pt; // set back to unparsed text
			statPay = parseFloat(workerData.sp);
			pay = parseFloat(workerData.py);
			if (curTxDataIdx == txidNone)
				curTxIdx = 2;
			else if (curTxDataIdx == txidStored)
				curTxIdx = 3;
			else if (curTxDataIdx != lastTxDataIdx)
			{
				curTxIdx = false;
				for (var i = 3;i < txids.length;i++)
					if (txids[i] == curTxDataIdx)
					{
						curTxIdx = i;
						break;
					}
				if (curTxIdx === false)
				{
					curTxIdx = txids.length;
					txids[curTxIdx] = curTxDataIdx;
					if (round[r].txid !== false)
						round[r].txid = (round[r].txid === 2 /*none*/ ? curTxIdx : false /*became multi*/);
				}
				lastTxIdx = curTxIdx;
				lastTxDataIdx = curTxDataIdx;
			}
			else curTxIdx = lastTxIdx;
			round[r].workers[w] = [w, id, null, points, statPay, pay, curTxIdx];
		} // for w
		for (var c = 0;c < rdata.cd.length;c++)
		{
			contData = rdata.cd[c];
			name = contData.nm;
			mode = contData.md;
			total = parseFloat(contData.tp);
			if (isNaN(total))
				total = (typeof contData.tp === 'string' ? contData.tp : -1);
			count = parseInt(contData.cn);
			if (isNaN(count))
				count = (typeof contData.cn === 'string' ? contData.cn : -1);
			round[r].contribs[c] = [name, mode, total, count];
		}
	} // for r
	for (var i = 4;i < txids.length;i++)
	{
		var res = txidListFind(txids[i]);
		if (res === false)
		{
			console.log('roundDataParse txidListFind failed for: '+txids[i]);
			txids[i] = 0; // error
		}
		else if (res === 0)
		{
			console.log('roundDataParse txidListFind not found for: '+txids[i]);
			txids[i] = 1; // not found
		}
		else txids[i] = res; // text address
	} // for i
	roundData.block.txids = txids;
	roundData.block.rounds = round;
	roundData.block.selBlock = roundData.block.trySelBlock;
	return true;
}

function roundDataLoadWorkers(response = null)
{
	if (response !== null)
		roundData.timers.loadWorkersResponse = response;
	else if (roundData.timers.loadWorkersResponse !== null)
		response = roundData.timers.loadWorkersResponse;
	if (roundData.timers.loadWorkers !== null)
	{
		clearTimeout(roundData.timers.loadWorkers);
		roundData.timers.loadWorkers = null;
	}
	var rlen = roundData.block.rounds.length;
	if (roundData.block.selRound >= rlen)
	{
		console.log('roundDataLoadWorkers selRound bad '+roundData.block.selRound+'/'+rlen);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
		return;
	}
	var done = true;
	var selRound = roundData.block.rounds[roundData.block.selRound];
	var wlen = selRound.workers.length;
	for (var w = 0;w < wlen;w++)
	{
		if (selRound.workers[w][2] === null)
		{
			var repl = getWorkerName(selRound.workers[w][1], true /*loadNeeded*/, true /*knownValid*/);
			if (!repl.ok)
			{
				console.log('roundDataLoadWorkers getWorkerName failed for r '+roundData.block.selRound+' w '+w+' widx: '+selRound.workers[w][1]);
				console.log(repl);
				if (isObject(response) && response.hasOwnProperty('onFail'))
					response.onFail(response);
				return;
			}
			if (!repl.done)
				done = false;
			else if (!repl.found || repl.name === null)
			{
				console.log('roundDataLoadWorkers getWorkerName not found for r '+roundData.block.selRound+' w '+w+' widx: '+selRound.workers[w][1]);
				console.log(repl);
				selRound.workers[w][2] = getTextAsset('historydata/worker/notfound', '[not found]');
			}
			else
				selRound.workers[w][2] = repl.name;
		}
	}
	roundData.block.rounds[roundData.block.selRound] = selRound;
	if (!done)
	{
		if (roundData.timers.loadWorkersTimeout !== 0 && Date.now() > roundData.timers.loadWorkersTimeout)
		{
			roundData.timers.loadWorkersTimeout = 0;
			console.log('roundDataLoadWorkers timed out');
			if (isObject(response) && response.hasOwnProperty('onFail'))
				response.onFail(response);
			return;
		}
		else
			roundData.timers.loadWorkers = setTimeout('roundDataLoadWorkers()', 50);
	}
	else
	{
		roundData.timers.loadWorkersTimeout = 0;
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function rdSelectRoundID(rID)
{
	if (!roundData.index.loaded || roundData.index.loading)
	{
		console.log('rdSelectRoundID index not ready')
		return false;
	}
	rID = parseInt(rID);
	if (isNaN(rID))
	{
		console.log('rdSelectRoundID validate param failed')
		return false;
	}
	var found = false;
	const bLen = roundData.index.block.length;
	for (var b = 0;!found && b < bLen;b++)
	{
		const roundIds = roundData.index.block[b].rounds;
		const rLen = roundIds.length;
		for (var ridx = 0;ridx < rLen;ridx++)
			if (roundIds[ridx] == rID)
			{
				roundData.block.trySelBlock = b;
				roundData.block.trySelRound = ridx;
				console.log('rdSelectRoundID sel rnd id '+rID+' found at '+b+' - '+ridx);
				return true;
			}
	}
	return false;
}

function roundDataSortWorkers(workerData)
{
	var i;
	var col;
	var asc;
	var a, b;
	if (workerData.length <= 1)
		return;
	col = Math.abs(roundData.display.sortCol);
	asc = (roundData.display.sortCol < 0 ? false : true);
	var isString = (col == 1 || col == 4 ? true : false);
	if (col == 4)
		col = 6; // txid
	else
		col++;
	var done = false;
	var strCollator = new Intl.Collator();
	while (!done)
	{
		done = true;
		for (i = 0;i < workerData.length-1;i++)
		{
			a = workerData[  workerData[i][0]][col];
			b = workerData[workerData[i+1][0]][col];
			if (isString)
				cmp = (strCollator.compare(a, b) == (asc ? 1 : -1) ? true : false); 
				//cmp = (a.localeCompare(b) == (asc ? 1 : -1) ? true : false); 
			else
				cmp = (asc ? (a > b ? true : false) : (a < b ? true : false));
			if (cmp)
			{
				cmp = workerData[i][0];
				workerData[i][0] = workerData[i+1][0];
				workerData[i+1][0] = cmp;
				done = false;
			}
		} // for i
	} // while
}

function rdtRow(name, val)
{
	return "<tr><td>"+name+"</td><td><div style='margin:2px;'>"+val+"</div></td></tr>";
}

function buildRoundData()
{
	var rlen = roundData.block.rounds.length;
	if (!roundData.initting && roundData.ready && rlen != 0)
	{
		if (roundData.block.selRound >= rlen)
			roundData.block.selRound = 0;
		var selRound = roundData.block.rounds[roundData.block.selRound];
		var html = "<div id='histcont'><div id='histup'>" + getTextAsset('historydata/updated', "Updated: ") + roundData.block.dtUpdated + "</div><div style='overflow-x:auto;' id='histrtabs'>";
		html += "<div id='histtabnext' onclick='rdNextBlock();'>" + getTextAsset('historydata/page/next', "Next") + "</div>";
		for (var i = roundData.block.rounds.length-1;i >= 0;i--)
			html += "<div "+(i == roundData.block.selRound ? "id='histseltab' name='histseltab' " : "")+"onclick='rdRoundSelect("+i+");'>"+roundData.block.rounds[i].dtStarted.toLocaleDateString()+"</div>";
		html += "<div id='histtabprev' onclick='rdPrevBlock();'>" + getTextAsset('historydata/page/prev', "Prev") + "</div>";
		html += "</div><div style='width:100%;' id='histround'><h2><a style='float:left;font-size:initial;' href='./history.html?r="+selRound.id+"'><i class='fa fa-link' aria-hidden='true'>&nbsp;</i></a>Round #"+selRound.id+" for week of " + selRound.dtStarted.toLocaleDateString() + "</h2>";
		html += "<table id='histdetails'>";
		if (selRound.comment != '')
			html += rdtRow(getTextAsset('historydata/round/comment', "Comment"), 		selRound.comment);
		html += rdtRow(getTextAsset('historydata/round/date/polled', "Points polled"), 	selRound.dtPoll.toLocaleString(undefined/*def local*/, {timeZoneName: 'short'}));
		html += rdtRow(getTextAsset('historydata/round/date/reward', "Rewarded"), 		selRound.dtPay.toLocaleString(undefined/*def local*/, {timeZoneName: 'short'}));
		html += rdtRow(getTextAsset('historydata/round/points', "Total Points"), 		selRound.tPoints);
		html += rdtRow(getTextAsset('historydata/round/rewards', "Total Rewards"),		selRound.tPay.toFixed(Brand_Pay_Digits)+' '+Brand_Unit);
		html += rdtRow(getTextAsset('historydata/round/workers', "Rewarded Folders"),	selRound.paidWorkers);
		if (selRound.txid !== false) // all txid's match
		{
			strTxid =  roundData.block.txids[selRound.txid];
			html += rdtRow(getTextAsset('historydata/round/txid', "Payment Transaction"), (selRound.txid < 4 ? strTxid : "<a href='"+Brand_Tx_Link+strTxid+"'>"+strTxid.substring(0,32)+"...<br/>..."+strTxid.substring(32)+"</a>"));
		}
		if (selRound.market.length > 0)
		{
			var mktHtml = "<select style='display:inline-block;appearance:auto;height:auto;text-align:right;width:auto;padding:0.15em 0.25em;margin-left:2px;'>";
			for (var m = 0;m < selRound.market.length;m++)
				mktHtml += "<option style='text-align:right;'>"+selRound.market[m]+"</option>";
			mktHtml += "</select>";
			html += rdtRow(getTextAsset('historydata/round/market/title', "Market History"), "<div style='display:inline-block;text-align:right;'>"+getTextAsset('historydata/round/market/select', "$$BRAND_UNIT$$ value on ").replaceAll("$$BRAND_UNIT$$",  Brand_Unit) + selRound.dtStarted.toLocaleDateString(undefined/*def local*/, {timeZoneName: undefined})+mktHtml+"</div>");
		}
		html += "</table></div>";
		html += "<div><h3>"+getTextAsset('historydata/contrib/title', "Contributions")+"</h3><table id='histcontrib'>";
		html += "<tr><th>"+getTextAsset('historydata/contrib/th/title', "Title")+"</th><th>"+getTextAsset('historydata/contrib/th/type', "Type")+"</th><th>"+getTextAsset('historydata/contrib/th/total', "Total $$BRAND_UNIT$$").replaceAll("$$BRAND_UNIT$$",  Brand_Unit) + "</th><th>"+getTextAsset('historydata/contrib/th/count', "Count")+"</th></tr>"; 
		for (var c = 0;c < selRound.contribs.length;c++)
		{
			cont = selRound.contribs[c];
			html += "<tr><td>"+cont[0]+"</td><td>"+cont[1]+"</td><td>"+(cont[2] != -1.0 ? cont[2] : '&nbsp;')+"</td><td>"+(cont[3] != -1 ? (typeof cont[3] === 'string' ? "<span style='font-size:75%;'>" + cont[3] + "<span>" : cont[3]) : '&nbsp;')+"</td></tr>";
		}
		html += "</table></div>";
		html += "<div id='histworkers'></div>";
		document.getElementById("history").innerHTML = html;
		buildRoundDataWorkers();
		if (roundData.block.scrollTo)
		{
			roundData.block.scrollTo = false;
			document.getElementById("history").scrollIntoView(true);
		}
			
	}
	else if (!roundData.initting || roundData.ready && roundData.index.loaded && !roundData.index.loading && !roundData.block.loading)
		document.getElementById("history").innerHTML = "<div id='histcont'><div id='histup'>" + getTextAsset('historydata/status/norounds', "(No round history found)") + "</div></div>";
	else
		document.getElementById("history").innerHTML = "<div id='histcont'><div id='histup'>" + getTextAsset('historydata/status/loading', "Loading round history data...") + "</div></div>";
}

function rdtHeader(col, str)
{
	var selCol = Math.abs(roundData.display.sortCol);
	var html = "<th ";
	if (selCol == col)
		html += "id='" + (selCol == roundData.display.sortCol ? "histselhead" : "histselheadasc") + "' ";
	html += "onclick='rdSort("+col+");'>"+str+"</th>";
	return html;
}

function rdtWorkerRow(address, points, statPay, pay, txid)
{
	var isSpecial = false
	if (address.charAt(0) == "_") // special case to show Min/Max/Avg/Total labels
	{
		isSpecial = true;
		address = address.substr(1);
	}
	var html = "<tr><td>"+(isSpecial ? address : "<a href='"+Brand_Address_Link+address+"' target='_blank'>"+address+"</a>")+"</td><td>"+points+"</td><td>";
	if (isSpecial)
		html += statPay.toFixed(Brand_Pay_Digits);
	else if (pay == statPay)
		html += pay.toFixed(Brand_Pay_Digits);
	else if (pay == 0.0 && statPay != 0.0)
		html += "("+statPay.toFixed(Brand_Pay_Digits)+")";
	else
		html += "(+<span><i class='fa fa-bank' style='font-size:70%;'></i>"+(pay-statPay).toFixed(Brand_Pay_Digits)+"</span>) "+statPay.toFixed(Brand_Pay_Digits); // fa fa-university
	html += "</td>";
	if (txid === false)
		html += "</tr>";
	else
		html += "<td>"+(txid != getTextAsset('historydata/txid/none', "[none]") ? (txid.length < 10 ? txid : "<a href='"+Brand_Tx_Link+txid+"'>"+txid.substring(0, 7)+"...</a>") : "&nbsp;")+"</td></tr>";
	return html;
}

function buildRoundDataWorkers()
{
	// w[]: sortingIndex, workerID, workerName, points, statPay, pay, txID Index
	if (roundData.block.selRound >= roundData.block.rounds.length)
		return;
	var filler;
	var selRound = roundData.block.rounds[roundData.block.selRound];
	var workerData = selRound.workers;
	roundDataSortWorkers(workerData);
	
	var html = "<div><h3>"+getTextAsset('historydata/roundwork/title', "Folder Rewards")+"</h3>";

	var hasIdle = false;
	var hasNoPay = false;
	for (var i = 0;i < workerData.length;i++)
	{
		var w = workerData[workerData[i][0]];
		if (w[3] <= 0)
			hasIdle = true;
		else if (w[4] == 0.0)
			hasNoPay = true;
	}
	if (hasIdle == true)
		html += "<div style='text-align:left;'><input style='display:inline;z-index:0;-moz-appearance:checkbox;float:none;opacity:1.0;margin-right:5px;' id='show_idle' name='show_idle' type='checkbox' onchange='rdShowIdleToggle();' " + (roundData.display.showIdle ? "checked='checked' " : "") + " />" + getTextAsset('historydata/roundwork/showidle', "Show idle folders") + "</div>";
	if (hasNoPay == true)
		html += "<div style='text-align:left;'><input style='display:inline;z-index:0;-moz-appearance:checkbox;float:none;opacity:1.0;margin-right:5px;' id='show_nopay' name='show_nopay' type='checkbox' onchange='rdShowNoRewardToggle();' " + (roundData.display.showNoReward ? "checked='checked' " : "") + " />"+getTextAsset('historydata/roundwork/showzero', "Show un-rewarded folders") + "</div>";

	html += "<table id='histworkerstbl'><tr>";
	html += rdtHeader(1, getTextAsset('historydata/roundwork/th/address', "Address"));
	html += rdtHeader(2, getTextAsset('historydata/roundwork/th/points', "Points"));
	html += rdtHeader(3, getTextAsset('historydata/roundwork/th/reward', "Rewarded $$BRAND_UNIT$$").replaceAll("$$BRAND_UNIT$$", Brand_Unit));
	if (selRound.txid === false) // all txid's don't match?
	{
		filler = "[none]"; // txid column filler for stats
		html += rdtHeader(4,  getTextAsset('historydata/roundwork/th/txid', "Transaction ID"));
	}
	else filler = false;
	for (var i = 0;i < workerData.length;i++)
	{
		w = workerData[workerData[i][0]];
		if ( (roundData.display.showIdle || w[3] > 0) && (roundData.display.showNoReward || (w[4] > 0.0 || w[3] <= 0)) )
			html += rdtWorkerRow( w[2], w[3], w[4], w[5], (filler === false ? false : roundData.block.txids[w[6]]) );
	}
	html += rdtWorkerRow("_"+getTextAsset('historydata/rowtitle/min', "minimum"), parseFloat(selRound.minPoints).toFixed(0), selRound.minPay, 0, filler);
	html += rdtWorkerRow("_"+getTextAsset('historydata/rowtitle/max', "maximum"), parseFloat(selRound.maxPoints).toFixed(0), selRound.maxPay, 0, filler);
	html += rdtWorkerRow("_"+getTextAsset('historydata/rowtitle/avg', "average"), parseFloat(selRound.avgPoints).toFixed(0), selRound.avgPay, 0, filler);
	html += "</table></div><div id='csv' onclick='rdCSVClicked();'>&lt;" + getTextAsset('historydata/csv/build', "CSV") + "&gt;</div></div>"
	document.getElementById("histworkers").innerHTML = html;
}

function rdBuildCSV()
{
	var workerData = roundData.block.rounds[roundData.block.selRound].workers;
	var href = "data:application/octet-stream,";
	href += getTextAsset('historydata/csv/th/address', "address") + "%2C";
	href += getTextAsset('historydata/csv/th/points', "points") + "%2C";
	href += getTextAsset('historydata/csv/th/rewarded', "rewarded") + "%2C";
	href += getTextAsset('historydata/csv/th/paid', "paid") + "%2C";
	href += getTextAsset('historydata/csv/th/txid', "txid") + "%0A";
	roundDataSortWorkers(workerData);
	for (var i = 0;i < workerData.length;i++)
	{
		var w = workerData[workerData[i][0]];
		if ((roundData.display.showIdle || w[3] != 0) && (roundData.display.showNoReward ||  w[4] != 0.0) )
		{
			statPay = parseFloat(w[4]);
			if (isNaN(statPay))
				statPay = w[4]; // keep text message if not a number
			else
				statPay = statPay.toFixed(4) // number value, fix to 4 decimals
			pay = parseFloat(w[5]);
			if (isNaN(pay))
				pay = w[5]; // keep text message if not a number
			else
				pay = pay.toFixed(4) // number value, fix to 4 decimals
			href += w[2] + "%2C" + w[3] + "%2C" + statPay + "%2C" + pay + "%2C" + roundData.block.txids[w[6]] + "%0A";
		}
	}
	return href;
}

function rdRoundSelect(rnd)
{
	if (!roundData.ready || !roundData.block.loaded || roundData.block.loading)
		return;
	if (Date.now() < roundData.display.dtDelayRoundSelectTill)
		return;
	roundData.display.dtDelayRoundSelectTill = Date.now() + 300;
	roundData.block.selRound = rnd;
	roundData.block.loading = false;
	roundData.block.loaded = false;
	var response = {
			onSuccess: function(resp)
						{
							console.log('rdRoundSelect loadWorkers onSuccess');
							roundData.block.loaded = true;
							roundData.block.loading = false;
							buildRoundData();
						},
			onFail: function(resp)
						{
							console.log('rdRoundSelect loadWorkers onFail');
							roundData.block.loading = false;
							buildRoundData();
						}
					}
	roundData.timers.loadWorkersTimeout = Date.now() + 5000;
	roundDataLoadWorkers(response);
}

function rdPrevBlock()
{
	if (!roundData.ready || !roundData.index.loaded)
		return;
	if (roundData.block.selBlock <= 0)
		return; // no previous
	var block = roundData.block.selBlock - 1;
	roundData.block.trySelRound = -1; // max
	roundDataLoadBlock(block);
}

function rdNextBlock()
{
	if (!roundData.ready || !roundData.index.loaded)
		return;
	var block = roundData.block.selBlock + 1;
	if ((roundData.block.selBlock + 1) < roundData.index.blockCount)
	{
		roundData.block.trySelRound = 0;
		roundDataLoadBlock(block);
	}
}

function rdSort(col)
{
	if (!roundData.ready || !roundData.block.loaded || roundData.block.loading)
		return;
	if (roundData.display.sorting || Date.now() < roundData.display.dtDelaySortTill)
		return;
	roundData.display.dtDelaySortTill = Date.now() + 300;
	roundData.display.sorting = true;
	col = parseInt(col);
	if (roundData.display.sortCol == col) roundData.display.sortCol *= -1;
	else roundData.display.sortCol = col;
	buildRoundDataWorkers();
	roundData.display.sorting = false;
}

function rdShowIdleToggle()
{
	var obj = document.getElementById("show_idle");
	if (!obj)
		return;
	roundData.display.showIdle = !roundData.display.showIdle;
	if (roundData.display.showIdle)
		obj.setAttribute('checked', 'checked');
	else
		obj.removeAttribute('checked');
	buildRoundDataWorkers();	
}

function rdShowNoRewardToggle()
{
	var obj = document.getElementById("show_nopay");
	if (!obj)
		return;
	roundData.display.showNoReward = !roundData.display.showNoReward;
	if (roundData.display.showNoReward)
		obj.setAttribute('checked', 'checked');
	else
		obj.removeAttribute('checked');
	buildRoundDataWorkers();	
}

function rdCSVClicked()
{
	document.getElementById("csv").onclick = null;
	document.getElementById("csv").innerHTML = "<a href='" + rdBuildCSV() + "' download='" + getTextAsset('historydata/csv/filename', "round.csv") + "'>&lt;" + getTextAsset('historydata/csv/download', "Download CSV") + "&gt;</a>";
}
