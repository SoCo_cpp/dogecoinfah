/*
	dfahgeneral.js
	Live data poller from JSON file prepared by DogecoinFah

	This file provides the much used loadFile function,
	some small helper functions, and the manifest filename
	system.
*/

var debugToolDOMSaver = false;

if (!Date.now){ // IE8 compat
	Date.now = function() { return new Date().getTime(); } 
} 

function isObject(objValue)
{
  return objValue && typeof objValue === 'object' && objValue.constructor === Object;
}

function loadFile(url, onDone, onDoneArg = null, isBinary = false, noCache = false)
{
	var httpreq = null;
	var reqUrl = (noCache ? url + "?t=" + Date.now() : url);
	try{
	   // Opera 8.0+, Firefox, Chrome, Safari
	   httpreq = new XMLHttpRequest();
	}catch (e){
	   // IE
	   try{
		  httpreq = new ActiveXObject("Msxml2.XMLHTTP");
	   }catch (e) {
		
		  try{
			 httpreq = new ActiveXObject("Microsoft.XMLHTTP");
		  }catch (e){
			 // Something went wrong
			 alert("Your browser broke!");
			 return false;
		  }
			
	   }
	}
	if (httpreq === null)
		return false;
	httpreq.onreadystatechange = function() 
	{
		if (httpreq.readyState == 4)
		{
			if (onDoneArg !== null)
				onDone(httpreq.status, (isBinary ? httpreq.response : httpreq.responseText), onDoneArg);
			else
				onDone(httpreq.status, (isBinary ? httpreq.response : httpreq.responseText));
		}
    }
	httpreq.open("GET", reqUrl, true);
	if (isBinary)
		httpreq.responseType = "arraybuffer";
	else
		httpreq.responseType = "text";
	httpreq.send();
	return true;    
}

// manual manifest support, cache management of dynamic data

// manifest asset id constants
const mfAsset = {	none:					0,
					pubdatBalance:			1,
					pubdatChart:			2,
					pubdatTeam:				3,
					pubDatTxids:			4,
					pubdatWorkerListIndex:	5,
					pubdatInvWorkerList:	6,
					pubdatValWorkerList:	7,
					pubdatRoundsIndex:		8,
					pubdatRounds:			9,
					pubdatWorkerDataIndex:	10,
					pubdatWorkerData:		11,
					pubdatTextAssets:		12
			};


var mfData = {
				failed: false,
				loaded: false,
				mfver: -1,
				mfts: 0,
				versions: [],
				assets: []
			};

mfData.assets[mfAsset.pubdatBalance]			= {f:	'data/PublicData.json' };
mfData.assets[mfAsset.pubdatChart]				= {f:	'data/PublicRoundChartData.json'};
mfData.assets[mfAsset.pubdatTeam]				= {f:	'data/PublicTeamData.json'};
mfData.assets[mfAsset.pubDatTxids]				= {f:	'data/PublicTxidList.bin'};
mfData.assets[mfAsset.pubdatWorkerListIndex]	= {f:	'data/PublicWorkerList.bin'};
mfData.assets[mfAsset.pubdatInvWorkerList]		= {f:	'data/PublicWorkerListInval.json'};
mfData.assets[mfAsset.pubdatValWorkerList]		= {fb:	'data/PublicWorkerListVal_', fe: '.bin'};
mfData.assets[mfAsset.pubdatRoundsIndex]		= {f:	'data/PublicRoundData.json' };
mfData.assets[mfAsset.pubdatRounds]				= {fb:	'data/PublicRoundData_', fe: '.json'};
mfData.assets[mfAsset.pubdatWorkerDataIndex]	= {f:	'data/PublicWorkerData.json'};
mfData.assets[mfAsset.pubdatWorkerData]			= {fb:	'data/PublicWorkerData_', fe: '.json'};
mfData.assets[mfAsset.pubdatTextAssets]			= {fb:	'data/PublicTextAssets_', fe: '.json'};


var saveDOMReady = false;
function initSaveDOM()
{
	if (saveDOMReady)
		return;
	console.log("initting save DOM");
	var elDivSave = document.createElement("div");
	elDivSave.style = "width:220px;height: 70px;background-color:gray;position:fixed;";
	var elDlSave = document.createElement("a");
	elDlSave.id = "dlSaveDOM"
	var elDlSaveText = document.createTextNode("Save to prepare download");
	var elBr = document.createElement("br");
	var elBtnSave = document.createElement("button");
	elBtnSave.innerHTML = "Save DOM page";
	elBtnSave.onclick = function () {


		var css= [];

		for (var sheeti= 0; sheeti<document.styleSheets.length; sheeti++) {
			var sheet= document.styleSheets[sheeti];
			var rules= ('cssRules' in sheet)? sheet.cssRules : sheet.rules;
			for (var rulei= 0; rulei<rules.length; rulei++) {
				var rule= rules[rulei];
				if ('cssText' in rule)
					css.push(rule.cssText);
				else
					css.push(rule.selectorText+' {\n'+rule.style.cssText+'\n}\n');
			}
		}

		var txtOut = "<html><head><style>\n"+ css.join('\n') + "\n</style>\n</head>\n<body>\n" + document.body.innerHTML + "\n</body>\n</html>\n";
		var encodedOut = "";
		console.log("Save clicked, encoding " + txtOut.length + " bytes...");
		for (var i = 0;i < txtOut.length;i++)
			if (txtOut[i].match(/^[0-9a-z]+$/))
			{
				encodedOut += txtOut[i];
			}
			else
			{
				var cHex = txtOut.charCodeAt(i).toString(16);
				if (cHex.length == 1)
					cHex = '0'+cHex;
				encodedOut += '%' + cHex;
			}
		console.log("Save clicked, encoding done, result " + encodedOut.length + " bytes");
		var dlSave = document.getElementById("dlSaveDOM"); //parentElement.getElementsByTagName("a");
		if (dlSave === null)
		{
			console.log("dlSave not found!");
		}
		else
		{
			var fn = window.location.pathname;
			var fnParts = fn.split("/");
			fn = fnParts[fnParts.length -1];
			fnParts = fn.split("?");
			fn = fnParts[0];
			fnParts = fn.split("#");
			fn = fnParts[0];
			dlSave.href = "data:application/octet-stream," + encodedOut;
			if (fn.length < 3)
				fn = "bIndex.html";
			
			dlSave.download = fn;
			dlSave.innerHTML = "Download";
			console.log("dlSave ready to save as: " + fn);
		}
	};

	elDivSave.appendChild(elBtnSave);
	elDivSave.appendChild(elBr);
	elDlSave.appendChild(elDlSaveText);
	elDivSave.appendChild(elDlSave);
	document.body.appendChild(elDivSave);
	saveDOMReady = true;
}


function waitMainReady(response = null)
{
	if (mfData.loaded)
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
		if (debugToolDOMSaver)
			initSaveDOM();
	}
	else if (mfData.failed)
	{
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else setTimeout(function () { waitMainReady(response); }, 30);
}

function mfLoad(st, data)
{
	if (st != '200')
		return 'http response status: '+status;
	if (!data)
		return 'data bad';
	// data is as string <unix timestamp>,<hex string with every 2 chars being an asset version>
	data = data.split(',');
	if (data.length != 2)
		return 'data expected two fields';
	mfData.mfts = parseInt(data[0]) * 1000;
	if (mfData.mfts === NaN)
		return 'parse timestamp failed';
	data = data[1].match(/.{1,2}/g); // split hex string in to array of 2 char strings
	var ver;
	for (var a = 0;a < data.length;a++)
	{
		ver = parseInt(data[a], 16);
		if (ver === NaN)
			return 'asset invalid data';
		mfData.versions[a] = ver;
	}
	mfData.loaded = true;
	return true;
}

loadFile('data/PublicDataManifest.json', function(st, data)
				{
					var ret = mfLoad(st, data);
					if (ret !== true)
					{
						console.log('Data manifest load failed: '+ret);
						mfData.failed = true;
					}
				}, null /*doneArg*/, false /*isBinary*/, true /*noCache*/);


function mfFileName(assetId, assetIndex = null)
{
	if (!mfData.loaded)
		{console.log('mfFileName manifest not loaded');return false;}
	var asset = mfData.assets[assetId];
	if (typeof asset === 'undefined')
		{console.log('mfFileName asset id '+assetId+' not found');return false;}
	var ver = mfData.versions[assetId];
	if (typeof ver === 'undefined')
		{console.log('mfFileName asset id '+assetId+' version not set');return false;}
	var fileName;
	if (assetIndex !== null)
	{
		if (!asset.hasOwnProperty('fb') || !asset.hasOwnProperty('fe'))
			{console.log('mfFileName index given, but asset is not indexable');return false;}
		fileName = asset.fb+assetIndex+asset.fe;
	}
	else
	{
		if (!asset.hasOwnProperty('f'))
		{
			if (!asset.hasOwnProperty('fb') || !asset.hasOwnProperty('fe'))
				{console.log('mfFileName no index given, but asset requres one');return false;}
			else
				{console.log('mfFileName asset definition invalid');return false;}
		}
		fileName = asset.f;
	}
	return fileName+'?v='+ver;

}
