/*
	dfahlivehistorychart.js
	Live data poller from JSON file prepared by DogecoinFah

	Requires:
		dfahgeneral.js (loadFile, mfFileName)
		dfahtextmanager.js (getTextAsset)
		dfahlivedata.js (initLiveData, [branding configuration])

	Data files:
		mfAsset.pubdatChart (data/PublicRoundChartData.json)

	This file loads and displays the Round history charts.

*/
var C_Market_Decimal_Field = 'large_decimal'; // 'preferred_decimal', 'large_decimal', 'max_decimal', 'min_decimal'
var chartUpdateRate = 0; // 0 for off, value in milliseconds
var httpreqChart;
var history;
var mode = 0;
var roundUpdated = "";
var changeRoundDelayed = false;
var sortDelayed = false;
var showIdle = false;
var chartMaxDays = 182; // (days) default to 6 months
var chartSelMarket = -1;
var chartInitStep = 0;

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

if (!Date.now){ // IE8 compat
	Date.now = function() { return new Date().getTime(); } 
} 

function logmsg(msg)
{
	var logdom = document.getElementById("history")
	logdom.innerHTML = logdom.innerHTML + msg + "<br/>";
}

function roundFloat(value, decimals)
{
	return Number(Math.round(value + "e" + decimals) + "e-" + decimals)
}

/*
// Note: 12.00 trims
console.log('roundFloat test: (dec 4) 26.8518509999999996 -> '+roundFloat(26.8518509999999996, 4)); 
console.log('roundFloat test: (dec 2) 12 -> '+roundFloat(12, 2)+', 12.00555 -> '+roundFloat(12.00555, 2)+', 12.00444 -> '+roundFloat(12.00444, 2)+', 12.00666 -> '+roundFloat(12.00666, 2)); 
console.log('roundFloat test: (dec 1) 12 -> '+roundFloat(12, 1)+', 12.0555 -> '+roundFloat(12.0555, 1)+', 12.0444 -> '+roundFloat(12.0444, 1)+', 12.0666 -> '+roundFloat(12.0666, 1)); 
console.log('roundFloat test: (dec 0) 12 -> '+roundFloat(12, 0)+', 12.555 -> '+roundFloat(12.555, 0)+', 12.444 -> '+roundFloat(12.444, 0)+', 12.666 -> '+roundFloat(12.666, 0));
*/

function initLiveHistoryChart(response = null)
{
	// ------------------
	chartInitStep = 0;
	// ------------------
	var innerResponse = {
		onSuccess: function(resp)
					{
						// Note: resp should be another copy of innerReponse here
						chartInitStep++;
						var ok = true;
						switch (chartInitStep)
						{
							case 1:
								if (!requestLiveChartData(resp))
								{
									console.log('chart init requestLiveChartData failed');
									ok = false;
								}
								break;
							case 2:
								if (isObject(response) && response.hasOwnProperty('onSuccess'))
									response.onSuccess(response);
								break;
								
						}
						if (!ok)
						{
							console.log('initLiveHistoryChart init step '+chartInitStep+' failed');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
					},
		onFail: function(resp)
					{
						console.log('initLiveHistoryChart: init step '+chartInitStep+' failed');
						if (isObject(response) && response.hasOwnProperty('onFail'))
							response.onFail(response);
					}
				};
	// ------------------
	if (!initLiveData(innerResponse))
	{
		console.log('chart init initLiveData failed');
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
		return false;
	}
	// ------------------
	return true;
}

function handleLiveChartData(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseChartData(data);
	if (ret !== true)
	{
		console.log('parseChartData failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function parseChartData(data)
{
	if (!data)
		return 'bad data';
	var jsonChartData = JSON.parse(data);
	if (!isObject(jsonChartData))
		return 'data not an object';
	if (!jsonChartData.hasOwnProperty('ts') || !jsonChartData.hasOwnProperty('rounds'))
		return 'validated base data fields failed';
	var dtUpdated = new Date(parseInt(jsonChartData.ts) * 1000);
	roundUpdated = dtUpdated.toLocaleString();
	var dataRounds = jsonChartData.rounds;
	if (!Array.isArray(dataRounds))
		return 'data rounds is not an array';
	history.marketAssets = jsonChartData.mktast;
	history.id = [];
	history.date = [];
	history.points = [];
	history.pay = [];
	history.workers = [];
	history.maxpoints = [];
	history.avgpoints = [];
	history.maxpay = [];
	history.avgpay = [];
	history.market = [];
	history.marketsUsed = [];
	for (var r = 0;r < dataRounds.length;r++)
	{
		var rdata = dataRounds[r];
		history.id[r] 			= parseInt(rdata.id);
		history.date[r] 		= new Date(parseInt(rdata.tsst) * 1000);
		history.points[r] 		= parseInt(rdata.tpt);
		history.pay[r] 			= parseFloat(rdata.tpy);
		history.workers[r] 		= parseInt(rdata.cpy);
		history.maxpoints[r] 	= Math.round(parseInt(rdata.mxpt));
		history.avgpoints[r] 	= Math.round(parseInt(rdata.apt));
		history.maxpay[r] 		= parseFloat(rdata.mxpy);
		history.avgpay[r] 		= parseFloat(rdata.apy);
		history.market[r]		= (typeof rdata.mrkt !== 'undefined' ? rdata.mrkt : []);
		// rdata.minPoints and rdata.minPay not currently used
	}
	buildChart();
	if (chartUpdateRate != 0)
		setTimeout("requestLiveChartData()", chartUpdateRate);
	return true;
}

function requestLiveChartData(response = null)
{
	var fileName = mfFileName(mfAsset.pubdatChart);
	if (!loadFile(fileName, handleLiveChartData, response))
	{
		console.log('requestLiveChartData loadFile failed');
		return false;
	}
	return true;
}
function setMode(m)
{
	mode = m;
	buildChart();
}
function setMaxDays(newMax)
{
	chartMaxDays = newMax;
	buildChart();
}
function buildMarketSelector()
{
	var html = "<div style='font-size:80%;padding:0.35em;text-align:right;'>" + getTextAsset('histchart/reward_market/title', "Compare to historic market values") + "<br/><div>" + getTextAsset('histchart/reward_market/label', "Reward Display:");
	html += "<select id='chartMarketSel' style='display:inline-block;appearance:auto;padding:0.15em 0.5em;margin-left:0.4em;border:1px solid black;width:auto;height:auto;' onchange='chartSelMarket = this.value;buildChart();'>";
	html += "<option value='-1' style='font-weight:800;' "+(chartSelMarket == -1 ? "selected='selected'" : "")+">"+Brand_Name+"</option>";
	for (var id in history.marketAssets)
		if (id != Brand_Asset_ID)
			html += "<option value='"+id+"' "+(chartSelMarket == id ? "selected='selected'" : "")+">"+history.marketAssets[id].fullname+"</option>";
	html += "</select></div></div>";
	return html;
}
function buildChart()
{
	var chartDiv = document.getElementById("chart")
	
	var chartModeNames = [ getTextAsset('histchart/mode_summary/tab', "Summary"), getTextAsset('histchart/mode_points/tab', "Point Details"), getTextAsset('histchart/mode_rewards/tab', "Reward Details") ];
	var html = "<div id='histcont'><div style='overflow-x:auto;' id='histrtabs'>";
	for (var i = 0;i < chartModeNames.length;i++)
		html += "<div "+(i == mode ? "id='histseltab' " : "")+"onclick='setMode("+i+");'>"+chartModeNames[i]+"</div>";
	var mktSelHtml = buildMarketSelector();
	html += "</div><div style='width:100%;'>"+mktSelHtml+"</div><canvas id='histround'></canvas></div>";
	
	// Title, Max Days, Line Tension (0 - 1)
	var timeSpans = [ [ getTextAsset('histchart/range/0', "Maximum"), 0, 1 ],
					  [ getTextAsset('histchart/range/1', "Three Years"), 1095, 0.8 ],
					  [ getTextAsset('histchart/range/2', "One Year"), 365, 0.4 ],
					  [ getTextAsset('histchart/range/3', "Six Months"), 182, 0.25 ],
					  [ getTextAsset('histchart/range/4', "Three Months"), 91, 0.15 ],
					  [ getTextAsset('histchart/range/5', "One Month"), 30, 0 ] ];
	var chartLineTension = 1;
	html += "<div id='history' style='margin:15px;'>";
	for (var i = 0;i < timeSpans.length;i++)
	{
		html += "<span ";
		if (timeSpans[i][1] == chartMaxDays)
		{
			html += "style='border:2px solid rgb(75, 205, 90);margin:5px;padding:0px 2px;border-radius:10px;'";
			chartLineTension = timeSpans[i][2];
		}
		else
			html += "style='border:1px solid black;margin:5px;padding:0px 2px;border-radius:10px;'";
		html += " onclick='setMaxDays(" + timeSpans[i][1] + ");'>" + timeSpans[i][0] + "</span>";
	}

	html += "</div>";
	chartDiv.innerHTML = html;

	var dataDates = [];
	var dataPoints = [];
	var dataPay = [];
	var dataWorkers = [];
	var dataAvgPoints = [];
	var dataMaxPoints = [];
	var dataAvgPay = [];
	var dataMaxPay = [];
	var strUnit;
	var usingAltUnit = false;
	var mktRoundDecimals = 6; // rational fallback, set specific to market below
	if (chartSelMarket != -1 && (typeof history.marketAssets[chartSelMarket] === 'undefined' || !history.marketAssets[chartSelMarket].hasOwnProperty('unitname')))
	{
		console.log('Build sel market '+chartSelMarket+' marketAsset is undefined:');
		console.log(history.marketAssets[chartSelMarket]);
		chartSelMarket = -1;
	}
	if (chartSelMarket != -1)
	{
		usingAltUnit = true;
		strUnit = history.marketAssets[chartSelMarket].unitname;
		if (history.marketAssets[chartSelMarket].hasOwnProperty(C_Market_Decimal_Field))
			mktRoundDecimals = history.marketAssets[chartSelMarket][C_Market_Decimal_Field];
	}
	else
		strUnit = Brand_Unit;
	if (chartMaxDays == 0)
	{
		for (var r = 0;r < history.date.length;r++)
		{
			
			dataDates[r]		= history.date[r];
			dataPoints[r] 		= { t: history.date[r], y: history.points[r] };
			dataWorkers[r] 		= { t: history.date[r], y: history.workers[r] };
			dataAvgPoints[r] 	= { t: history.date[r], y: history.avgpoints[r] };
			dataMaxPoints[r] 	= { t: history.date[r], y: history.maxpoints[r] };
			if (chartSelMarket == -1)
			{
				dataPay[r] 			= { t: history.date[r], y: history.pay[r] };
				dataAvgPay[r] 		= { t: history.date[r], y: history.avgpay[r] };
				dataMaxPay[r] 		= { t: history.date[r], y: history.maxpay[r] };
			}
			else if (typeof history.market[r][chartSelMarket] !== 'undefined')
			{
				var val = history.market[r][chartSelMarket];
				dataPay[r] 			= { x: history.date[r], y: roundFloat((history.pay[r] * val), mktRoundDecimals) };
				dataAvgPay[r] 		= { t: history.date[r], y: roundFloat((history.avgpay[r] * val), mktRoundDecimals) };
				dataMaxPay[r] 		= { t: history.date[r], y: roundFloat((history.maxpay[r] * val), mktRoundDecimals) };
			}
			else
			{
				dataPay[r] 			= { x: history.date[r], y: NaN };
				dataAvgPay[r] 		= { t: history.date[r], y: NaN };
				dataMaxPay[r] 		= { t: history.date[r], y: NaN };
			}
		}
	}
	else
	{
		var dateMax = new Date();
		dateMax.setDate(dateMax.getDate() - chartMaxDays);
		dateMaxValue = dateMax.valueOf();
		for (var r = 0;r < history.date.length;r++)
			if (history.date[r].valueOf() > dateMaxValue)
			{
				dataDates.push(history.date[r]);
				dataPoints.push({ t: history.date[r], y: history.points[r] });
				
				dataWorkers.push({ t: history.date[r], y: history.workers[r] });
				dataAvgPoints.push({ t: history.date[r], y: history.avgpoints[r] });
				dataMaxPoints.push({ t: history.date[r], y: history.maxpoints[r] });
				if (chartSelMarket == -1)
				{
					dataPay.push({ t: history.date[r], y: history.pay[r] });
					dataAvgPay.push({ t: history.date[r], y: history.avgpay[r] });
					dataMaxPay.push({ t: history.date[r], y: history.maxpay[r] });
				}
				else if (typeof history.market[r][chartSelMarket] !== 'undefined')
				{
					var val = history.market[r][chartSelMarket];
					dataPay.push({ t: history.date[r],    y: roundFloat((history.pay[r] * val), mktRoundDecimals)});
					dataAvgPay.push({ t: history.date[r], y: roundFloat((history.avgpay[r] * val), mktRoundDecimals) });
					dataMaxPay.push({ t: history.date[r], y: roundFloat((history.maxpay[r] * val), mktRoundDecimals) });
				}
				else
				{
					dataPay.push({ t: history.date[r], y: NaN });
					dataAvgPay.push({ t: history.date[r], y: NaN });
					dataMaxPay.push({ t: history.date[r], y: NaN });
				}
			}
	} 

	var datasets = [];
	var scales = [];
	if (mode == 0 || mode == 1)
	{
		datasets.push(	{
				label: (mode == 0 ? getTextAsset('histchart/summary/points', "Points Folded") : getTextAsset('histchart/points/points', "Total Points")),
				borderColor: window.chartColors.red,
				backgroundColor: window.chartColors.red,
				fill: false,
				data: dataPoints,
				yAxisID: "y-L",
			} );
	}
	if (mode == 0 || mode == 2)
	{
		datasets.push(	{
				label: (mode == 0 ? getTextAsset('histchart/summary/reward', "$$ALT_UNIT$$ Value of Reward").replaceAll('$$ALT_UNIT$$', strUnit) : getTextAsset('histchart/reward/reward', "Total Reward")),
				borderColor: window.chartColors.blue,
				backgroundColor: window.chartColors.blue,
				fill: false,
				spanGaps: false,
				data: dataPay,
				yAxisID: (mode == 0 ? "y-R" : "y-L")
			} );
	}
	if (mode == 0)
	{
		datasets.push(	{
				label: getTextAsset('histchart/summary/folders', "Folders Rewarded"),
				borderColor: window.chartColors.purple,
				backgroundColor: window.chartColors.purple,
				fill: false,
				data: dataWorkers,
				yAxisID: "y-H"
			} );		
	}
	if (mode == 1)
	{
		datasets.push(	{
				label: getTextAsset('histchart/points/avg', "Avg Points"),
				borderColor: window.chartColors.purple,
				backgroundColor: window.chartColors.purple,
				fill: false,
				data: dataAvgPoints,
				yAxisID: "y-R",
			} );
		datasets.push(	{
				label: getTextAsset('histchart/points/max', "Max Points"),
				borderColor: window.chartColors.blue,
				backgroundColor: window.chartColors.blue,
				fill: false,
				data: dataMaxPoints,
				yAxisID: "y-R",
			} );
	}

	if (mode == 2)
	{
		datasets.push(	{
				label: getTextAsset('histchart/reward/avg', "Avg Reward"),
				borderColor: window.chartColors.purple,
				backgroundColor: window.chartColors.purple,
				fill: false,
				spanGaps: false,
				data: dataAvgPay,
				yAxisID: "y-R",
			} );
		datasets.push(	{
				label: getTextAsset('histchart/reward/max', "Max Reward"),
				borderColor: window.chartColors.red,
				backgroundColor: window.chartColors.red,
				fill: false,
				spanGaps: false,
				data: dataMaxPay,
				yAxisID: "y-R",
			} );
	}

    var lineChartData = {
		labels: dataDates,
        datasets: datasets,
    };

	var scales = [{
					type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					display: true,
					position: "left",
					id: "y-L",
					scaleLabel: { 
						display: true,
						labelString: (mode == 0 ? getTextAsset('histchart/summary/scale/left', "Points") : getTextAsset('histchart/details/scale/left', "Total")),
					},
				}, {
					type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					display: true,
					position: "right",
					id: "y-R",
					scaleLabel: { 
						display: true,
						labelString: (mode == 0 ? strUnit : getTextAsset('histchart/details/scale/right', "Avg / Max")),
					},
					// grid line settings
					gridLines: {
						drawOnChartArea: false, // only want the grid lines for one axis to show up
					}
				}];
				
	if (mode == 0)
	{
		scales.push( {
					type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					display: true,
					position: "right",
					id: "y-H",
					scaleLabel: { 
						display: true,
						labelString: getTextAsset('histchart/summary/scale/right', "Folders"),
					},
					// grid line settings
					gridLines: {
						drawOnChartArea: false, // only want the grid lines for one axis to show up
						}
					} );
	}
	
	if (mode == 0) titleText = getTextAsset('histchart/summary/title', "Summary");
	if (mode == 1) titleText = getTextAsset('histchart/points/title', "Points Folded");
	if (mode == 2) titleText = (usingAltUnit ? getTextAsset('histchart/reward/title/altunit', "$$ALT_UNIT$$ value of $$BRAND_UNIT$$ Rewarded") 
											 : getTextAsset('histchart/reward/title', "$$BRAND_UNIT$$ Rewarded")).replaceAll('$$ALT_UNIT$$', strUnit).replaceAll('$$BRAND_UNIT$$', Brand_Unit);
	var ctx = document.getElementById("histround").getContext("2d");
	window.myLine = Chart.Line(ctx, {
		data: lineChartData,
		options: {
			responsive: true,
			hoverMode: 'index',
			stacked: false,
			title:{
				display: true,
				text: titleText,
			},
			scales: {
				yAxes: scales,
				xAxes: [{
					type: 'time',
					distribution: 'series',
					time: {
							//unit: 'day', /*quarter month week*/
							tooltipFormat: 'll',
							displayFormats: {
								year: 'YYYY',
								quarter: 'MMM YYYY',
								month: 'MMM YYYY',
								week: 'll',
								day: 'MMM D'
							}
							
						}
				}],
            },
			elements: {line:{tension:chartLineTension}},
        }
	});	
    window.myLine.update();
}
