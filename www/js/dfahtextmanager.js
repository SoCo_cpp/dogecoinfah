/*
	dfahtextmanager.js

	Loads and distributes text assets of the proper translation.

	Requires:
		dfahgeneral.js (loadFile, mfFileName)

	Data files:
		mfAsset.pubdatTextAssets (PublicTextAssets_[LANGUAGE].json)
*/

const C_TXTMAN_DEF_LANG = 'en'; // default language to use on failure or unsupported language

var txtMan = {
			ready: false,
			loading: false,
			failed: false,
			errorMsgCount: 0,
			language: C_TXTMAN_DEF_LANG,
			// Optional:
			// Disable known unsupported languages. Prevents web requests
			// which would fail. They would gracefully fall back to
			// using the default language, but this way the request
			// attempts are skipped.
			disabledLanguages: ['de','fr','es','ru','pt'], // falls back to def lang
			data: {
				version: 0,
				assets: []
			}
		};

function initTxtMan(response = null)
{
	// ------------------
	if (txtMan.ready || txtMan.loading)
	{
		console.log('initTxtMan already ready/loading');
		return false;
	}
	// ------------------
	var innerResponse = {
		onSuccess: function(resp)
					{
						txtMan.language = document.documentElement.getAttribute("lang");
						if (txtMan.language === null || typeof txtMan.language !== 'string' || txtMan.language === '')
						{
							console.log("dfahtextmanager.js initTxtMan: couldn't detect language from document. Falling back to the default language '"+C_TXTMAN_DEF_LANG+"'. This only applies to this site's multi-language support for JavaScript features.");
							txtMan.language = C_TXTMAN_DEF_LANG;
						}
						else
						{
							var ok = true;
							if (txtMan.language.length < 2)
								ok = false;
							else
							{
								if (txtMan.language.length > 2) // chop off sub language/country
									txtMan.language = txtMan.language.substr(0, 2);
								txtMan.language = txtMan.language.toLowerCase();
								var cc1 = txtMan.language.charCodeAt(0);
								var cc2 = txtMan.language.charCodeAt(1);
								if (cc1 < 97 || cc2 < 97 || cc1 > 122 || cc2 > 122)
									ok = false;
							}
							if (ok === false)
							{
								console.log("dfahtextmanager.js initTxtMan: language specified seems invalid '"+txtMan.language+"'. Falling back to the default language '"+C_TXTMAN_DEF_LANG+"'. This only applies to this site's multi-language support for JavaScript features.");
								txtMan.language = C_TXTMAN_DEF_LANG;
							}
							else if (txtMan.disabledLanguages.indexOf(txtMan.language) > -1)
							{
								console.log("dfahtextmanager.js initTxtMan: detected expressly unsupported/disabled language '"+txtMan.language+"'. Falling back to the default language '"+C_TXTMAN_DEF_LANG+"'. This only applies to this site's multi-language support for JavaScript features.");
								txtMan.language = C_TXTMAN_DEF_LANG;
							}
						}
						if (!requestTxtManData(response))
						{
							console.log('initTxtMan requestTxtManData failed');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
					},
		onFail: function(resp)
					{
						console.log('initTxtMan waitMainReady failed cannot continue');
						if (isObject(response) && response.hasOwnProperty('onFail'))
							response.onFail(response);
					}
				};
	// ------------------
	waitMainReady(innerResponse);
	// ------------------
	return true;	
}

function handleTxtManData(status, data, response = null)
{
	if (status != '200')
	{
		txtMan.loading = false;
		console.log("handleTxtManData http request for data of language '"+txtMan.language+"' failed with status: "+status);
		if (txtMan.language == C_TXTMAN_DEF_LANG)
		{
			console.log('requestTxtManData load data failed for default language, giving up.');
			txtMan.failed = true;
			txtMan.ready = true; // return success and set to ready for permanent fail, so nothing waits on this as txtMan has a fallback mode
			if (isObject(response) && response.hasOwnProperty('onSuccess'))
				response.onSuccess(response);
			return;
		}
		txtMan.language = C_TXTMAN_DEF_LANG;
		txtMan.loading = false;
		txtMan.failed = false;
		console.log("requestTxtManData load data failed, falling back to default language '"+txtMan.language+"' and requesting data again...");
		if (!requestTxtManData(response))
		{
			console.log('requestTxtManData requestTxtManData failed, giving up.');
			txtMan.failed = true;
			txtMan.ready = true; // return success and set to ready for permanent fail, so nothing waits on this as txtMan has a fallback mode
			if (isObject(response) && response.hasOwnProperty('onSuccess'))
				response.onSuccess(response);
			return;
		}
		return;
	}
	txtMan.ready = false;
	ret = updateTxtManData(JSON.parse(data));
	txtMan.loading = false;
	if (ret !== true)
	{
		txtMan.failed = true;
		console.log('handleTxtManData updateTxtManData failed: '+ret);
		txtMan.ready = true; // return success and set to ready for permanent fail, so nothing waits on this as txtMan has a fallback mode
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
		return;
	}
	txtMan.failed = false;
	txtMan.ready = true;
	txtMan.errorMsgCount = 0;
	if (isObject(response) && response.hasOwnProperty('onSuccess')) // respond to caller of requestLiveData (initLiveData)
		response.onSuccess(response);
}

function requestTxtManData(response = null)
{
	if (txtMan.loading)
	{
		console.log('equestTxtManData already loading');
		return false;
	}
	txtMan.errorMsgCount = 0;
	txtMan.loading = true;
	var fileName = mfFileName(mfAsset.pubdatTextAssets, txtMan.language);
	if (fileName === false)
	{
		console.log('requestTxtManData mfFileName failed for lang: '+txtMan.language);
		txtMan.ready = true; // ready true for permanent fail, so nothing waits for init to finish
		txtMan.failed = true;
		return false;
	}
	if (!loadFile(fileName, handleTxtManData, response))
	{
		console.log('requestTxtManData loadFile failed');
		txtMan.ready = true; // ready true for permanent fail, so nothing waits for init to finish
		txtMan.failed = true;
		return false;
	}
	return true;	
}

function updateTxtManData(jsonObj)
{
	if (!jsonObj)
		return 'validate jsonObj failed';
	if (typeof jsonObj.lang === 'undefined' || typeof jsonObj.ver === 'undefined' || typeof jsonObj.assets === 'undefined')
		return 'required field is missing';
	if (txtMan.language != jsonObj.lang)
		return 'lang mismatch';
	txtMan.data.version = parseInt(jsonObj.ver);
	if (txtMan.data.version === NaN)
		return 'ver invalid';
	if (!Array.isArray(jsonObj.assets))
		return 'ver invalid';
	for (var i = 0;i < jsonObj.assets.length;i++)
	{
		var asset = jsonObj.assets[i];
		if (!Array.isArray(asset) || asset.length != 2)
			return 'asset ' + i + ' not valid';
		txtMan.data.assets[asset[0]] = asset[1];
		//console.log('loaded lang '+txtMan.language+", '"+asset[0]+"'='"+asset[1]+"'");
	}
	return true;
}

function getTextAsset(assetName, failValue = undefined)
{
	if (txtMan.failed)
	{
		if (txtMan.errorMsgCount++ < 5)
			console.log('getTextAsset Text Manager load failed, defaulting text...');
		return failValue;
	}
	if (!txtMan.ready)
	{
		if (txtMan.errorMsgCount++ < 5)
			console.log('getTextAsset Text Manager not ready, defaulting text...');
		return failValue;
	}
	if (typeof txtMan.data.assets[assetName] === 'undefined')
		return failValue;
	return txtMan.data.assets[assetName];
}
