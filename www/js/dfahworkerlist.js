/*
	dfahworkerlist.js
	Data poller from binary files prepared by DogecoinFah

*/
var strDelemiter = '--BREAK--';
var delemiter = null;
var delemiterLen = 0;

var workerList = {
				initting: false,
				ready: false,
				want: {
					loadValidIndex: true,
					loadInvalidIndex: false
				},
				valid: {
					indexLoaded: false,
					blockCount: 0,
					blocks: [], // {count:, idlist: , loaded:, loading:, namelist: }
				},
				invalid: {
					indexLoaded: false,
					loading: false,
					loaded: false,
					count: 0,
					idlist: [],
					namelist: []					
				}
			  };

function baFindDel(ba, start, len, debug = false)
{
	var match = 0;
	if (debug)
		console.log('start '+start+', len '+len+', delemiterLen '+delemiterLen);
	while (start < len)
	{
		if (debug)
			console.log('s '+start+', m '+match+', '+ba[start]+'/'+delemiter[match]+' ('+(ba[start]>=32 ? String.fromCharCode(ba[start]) : '_')+')');
		
		if (ba[start++] == delemiter[match])
		{
			match++;
			if (match >= delemiterLen)
				return start - delemiterLen;
		}
		else
		{
			 if (match > 1)
				start -= match;
			match = 0;
		}
	}
	return -1;
}

function handleIndex(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseIndex(data)
	if (ret !== true)
	{
		console.log('parseIndex failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function parseHeader(uint8Data, start, len)
{
	var str = '';
	var values = [];
	var errMsg = false;
	var pos = baFindDel(uint8Data, start, len);
	if (pos == -1)
		errMsg = 'del not found';
	else
	{
		str = String.fromCharCode.apply(null, uint8Data.slice(start, pos));
		var parts = str.split(',');
		for (var i = 0;i < parts.length;i++)
		{
			var keyval = parts[i].split('-');
			if (keyval.length == 2)
			{
				var ival = parseInt(keyval[1]);
				if (!isNaN(ival))
					values[i] = {key: keyval[0], val: ival};
				else
				{
					errMsg = 'value NaN for pair: ('+parts[i]+')';
					break;
				}
			}
			else
			{
				errMsg = 'part has no key/val pair: ('+parts[i]+')';
				break;
			}
		}
	}
	if (errMsg !== false)
		return {ok:false, error: errMsg, header: str};
	return {ok: true, nextpos: pos+delemiterLen, vals: values, header: str};
}

function parseIndexData(uint8Data, start, len, count, load = true)
{
	var values = [];
	var errMsg = false;
	var pos = baFindDel(uint8Data, start, len);
	if (pos == -1)
		errMsg = 'del not found';
	else if (pos != start + (4 * count))
		errMsg = 'data size not expected, pos '+pos+', start '+start+', count '+count;
	else if (load)
	{
		var dataView = new DataView(uint8Data.buffer, start, pos-start);
		var dlen = dataView.byteLength;
		if ((dlen % 4) != 0)
			errMsg = 'data not even size: '+dlen;
		else if (dlen < 4 && count != 0)
			errMsg = 'data len empty, but count not: '+dlen+'/'+count;
		else
		{
			var p = 0;
			for (var i = 0;i < count;i++)
			{
				if (p+4 > dlen)
				{
					errMsg = 'entry hit end of data: '+p+', len '+dlen;
					break;
				}
				values[i] = dataView.getUint32(p, true /*little-endian*/);
				p += 4;
			}
		}
	}
	if (errMsg !== false)
		return {ok:false, error: errMsg};
	return {ok: true, nextpos: pos+delemiterLen, vals: values};
}

function parseIndex(dataIn)
{
	if (!dataIn)
		return 'bad data';
	var valBCount = 0;
	var invCount = 0;
	var bCount;
	var blocks = [];
	var data = new Uint8Array(dataIn);
	var dataLen = data.length;
	var i = 0;
	var ret;
	
	ret = parseHeader(data, i, dataLen);
	if (!ret.ok)
		return 'parse first header failed: '+ret.error+', ('+ret.header+')';
	if (ret.vals.length != 2 || ret.vals[0].key != 'ValBlocks' || ret.vals[1].key != 'InvCount')
		return 'first header not expected keys, header: ('+ret.header+')';
	i = ret.nextpos;
	valBCount = ret.vals[0].val;
	invCount =  ret.vals[1].val;
	
	workerList.valid.blockCount = valBCount;
	workerList.invalid.count = invCount;

	for (var b = 0;b < valBCount;b++)
	{

		ret = parseHeader(data, i, dataLen);
		if (!ret.ok)
			return 'parse block '+b+' header failed: '+ret.error+', ('+ret.header+')';
		if (ret.vals.length != 2 || ret.vals[0].key != 'B'|| ret.vals[1].key != 'C')
			return 'block '+b+' header not expected keys, header: ('+ret.header+')';
		i = ret.nextpos;

		if (ret.vals[0].val != b)
			return 'bad block header out of sync '+ret.vals[0].val+'/'+b;
		bCount = ret.vals[1].val
		i = ret.nextpos;

		ret = parseIndexData(data, i, dataLen, bCount, workerList.want.loadValidIndex);
		if (!ret.ok)
			return 'parse block '+b+' data failed: '+ret.error;
		i = ret.nextpos;
		if (workerList.want.loadValidIndex)
			workerList.valid.blocks[b] = {count: bCount, idlist: ret.vals, loaded: false, loading: false, namelist: []};
	} // for b

	if (workerList.want.loadValidIndex)
		workerList.valid.indexLoaded = true;

	ret = parseHeader(data, i, dataLen);
	if (!ret.ok)
		return 'parse inv header failed: '+ret.error+', ('+ret.header+')';
	if (ret.vals.length != 1 || ret.vals[0].key != 'Inv')
		return 'first inv not expected keys, header: ('+ret.header+')';
	i = ret.nextpos;
	bCount = ret.vals[0].val;
	if (bCount != invCount)
		return 'inv block header count no match: '+bCount+'/'+invCount;

	ret = parseIndexData(data, i, dataLen, bCount, workerList.want.loadInvalidIndex);
	if (!ret.ok)
		return 'parse inv block data failed: '+ret.error;
	i = ret.nextpos;
	workerList.invalid.idlist = ret.vals;
	if (workerList.want.loadInvalidIndex)
		workerList.invalid.indexLoaded = true;

	var pos = baFindDel(data, i, dataLen);
	if (pos == -1)
		return 'end marker del not found: '+i+' dataLen '+dataLen;
	var str = String.fromCharCode.apply(null, data.slice(i, pos));
	if (str != '-END-')
		return 'end marker no match: ['+i+'] '+str;
	return true;
}

function encodeBase58(uint8Data)
{
	var alph = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
	var alphLen = alph.length;
	var dataLen = uint8Data.length;
	var digits = [0];
	var dlen = 1;
	var carry;
	for (var b = 0; b < dataLen;b++)
	{
		for (var d = 0;d < dlen;d++)
			digits[d] <<= 8;
			
		digits[0] += uint8Data[b];
		carry = 0;
		for (var d = 0;d < dlen;d++)
		{
			digits[d] += carry;
			carry = (digits[d] / 58) | 0;
			digits[d] %= 58;
		}
		
		while (carry)
		{
			digits[dlen++] = (carry % 58);
			carry = (carry / 58) | 0;
		}		
	}
	for (var b = 0;uint8Data[b] == 0 && b < dataLen - 1;b++)
		digits[dlen++] = 0;
	var result = '';
	for (var d = dlen-1;d >= 0;d--)
		result += alph.charAt(digits[d]);
	return result;
}

function parseBlockData(uint8Data, start, len, count)
{
	var values = [];
	var errMsg = false;
	var pos = baFindDel(uint8Data, start, len);
	if (pos == -1)
		errMsg = 'del not found';
	else if (pos != start + (25 * count))
		errMsg = 'data size not expected, pos '+pos+', start '+start+', count '+count;
	else
	{
		var dlen = pos-start;
		if ((dlen % 25) != 0)
			errMsg = 'data not even size: '+dlen+', s '+start+', p '+pos+', len '+len;
		else if (dlen < 25 && count != 0)
			errMsg = 'data len empty, but count not: '+dlen+'/'+count;
		else
		{
			var p = start;
			for (var i = 0;i < count;i++)
			{
				if (p+25 > len)
				{
					errMsg = 'entry hit end of data: '+p+', len '+len;
					break;
				}
				values[i] = encodeBase58(uint8Data.slice(p, p+25));
				p += 25;
			}
		}
	}
	if (errMsg !== false)
		return {ok:false, error: errMsg};
	return {ok: true, nextpos: pos+delemiterLen, vals: values};
}

function handleBlock(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseBlock(data)
	if (isObject(response) && response.hasOwnProperty('block') && response.block < workerList.valid.blockCount)
		workerList.valid.blocks[response.block].loading = false;
	if (ret !== true)
	{
		console.log('parseBlock failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}	
}

function parseBlock(dataIn)
{
	if (!workerList.valid.indexLoaded)
		return 'index not loaded';		
	if (!dataIn)
		return 'bad data';
	var bIdx;
	var bCount;
	var data = new Uint8Array(dataIn);
	var dataLen = data.length;
	var i = 0;
	var ret;
	
	ret = parseHeader(data, i, dataLen);
	if (!ret.ok)
		return 'parse header failed: '+ret.error+', ('+ret.header+')';
	if (ret.vals.length != 2 || ret.vals[0].key != 'B' || ret.vals[1].key != 'C')
		return 'header not expected keys, header: ('+ret.header+')';
	i = ret.nextpos;
	bIdx = ret.vals[0].val;
	bCount = ret.vals[1].val;
	if (bIdx >= workerList.valid.blockCount)
		return 'data block is greater than index blockCount: '+bIdx+'/'+workerList.valid.blockCount;
	if (bCount != workerList.valid.blocks[bIdx].count)
		return 'data count not match index: '+bCount+'/'+workerList.valid.blocks[bIdx].count;
	ret = parseBlockData(data, i, dataLen, bCount);
	if (!ret.ok)
		return 'parse data failed: '+ret.error;
	workerList.valid.blocks[bIdx].namelist = ret.vals;
	workerList.valid.blocks[bIdx].loaded = true;
	return true;
}

function handleInvalid(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseInvalid(data)
	workerList.invalid.loading = false;
	if (ret !== true)
	{
		console.log('parseInvalid failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function parseInvalid(data)
{
	if (!workerList.invalid.indexLoaded)
		return 'index not loaded';		
	 var json = JSON.parse(data);
	 if (!Array.isArray(json))
		return 'data is not an array';
	var len = json.length;
	if (len != workerList.invalid.count)
		return 'data count not match index: '+len+'/'+workerList.invalid.count;
	for (var i = 0;i < len;i++)
	{
		if ( !(typeof json[i] === 'string' || json[i] instanceof String) )
			return 'data entry '+i+' is not a string';
		workerList.invalid.namelist[i] = json[i];
	}
	workerList.invalid.loaded = true;
	return true;
}

function loadIndex(response = null)
{
	var fileName = mfFileName(mfAsset.pubdatWorkerListIndex);
	if (!loadFile(fileName, handleIndex, response /*onDoneArg*/, true /*isBinary*/))
	{
		console.log('load index failed');
		return false;
	}
	return true;
}

function loadValBlock(block, response = null)
{
	if (!workerList.ready)
	{
		console.log('workerList not ready');
		return false;
	}
	if (!workerList.valid.indexLoaded)
	{
		console.log('index not loaded');
		return false;
	}
	if (block >= workerList.valid.count)
	{
		console.log('block past end '+block+'/'+workerList.valid.count);
		return false;
	}
	if (workerList.valid.blocks[block].loaded)
	{
		console.log('block '+block+' already loaded (warn)');
		return true;
	}
	if (workerList.valid.blocks[block].loading)
	{
		//console.log('block '+block+' already loading (warn)');
		return true;
	}
	workerList.valid.blocks[block].loading = true;
	if (response === null)
		response = {};
	response.block = block;
	var fileName = mfFileName(mfAsset.pubdatValWorkerList, block);	
	if (!loadFile(fileName, handleBlock, response /*onDoneArg*/, true /*isBinary*/))
	{
		workerList.valid.blocks[block].loading = false;
		console.log('load val block '+block+' failed: '+fileName);
		return false;
	}
	return true;
}

function loadInvalid(response = null)
{
	if (!workerList.ready)
	{
		console.log('workerList not ready');
		return false;
	}
	if (!workerList.invalid.indexLoaded)
	{
		console.log('index not loaded');
		return false;
	}
	if (workerList.invalid.loaded)
	{
		console.log('inval already loaded (warn)');
		return true;
	}
	if (workerList.invalid.loading)
	{
		//console.log('inval already loading (warn)');
		return true;
	}
	workerList.invalid.loading = true;
	var fileName = mfFileName(mfAsset.pubdatInvWorkerList);
	if (!loadFile(fileName, handleInvalid, response /*onDoneArg*/, false /*isBinary*/))
	{
		workerList.invalid.loading = false;
		console.log('load invalid failed: '+fileNameInval);
		return false;
	}
	return true;
}

function initWorkers(wantValid = true, wantInvalid = false, response = null)
{
	if (workerList.ready || workerList.initting)
	{
		console.log('initWorkers already ready/initting');
		return false;
	}
	workerList.initting = true;
	workerList.want.loadValidIndex = wantValid;
	workerList.want.loadInvalidIndex = wantInvalid;

	if (delemiter === null)
	{
		delemiter = new Uint8Array(strDelemiter.length);
		for (var i = 0;i < strDelemiter.length;i++)
			delemiter[i] = strDelemiter.charCodeAt(i);

		delemiterLen = delemiter.length;
	}
	
	var outterResponse = {
			onSuccess: function(resp)
						{
							workerList.initting = false;
							workerList.ready = true;
							if (isObject(response) && response.hasOwnProperty('onSuccess'))
								response.onSuccess(response);
						},
			onFail: function(resp)
						{
							workerList.initting = false;
							workerList.ready = false;
							console.log('initWorkers loadIndex onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
		};
		
	if (!loadIndex(outterResponse))
	{
		console.log('initWorkers loadIndex failed');
		workerList.initting = false;
		return false;
	}
	return true;
}

function loadAllWorkers(response = null)
{
	if (!workerList.ready)
		return 'workerList not ready';
	if (!workerList.invalid.indexLoaded)
		return 'invalid index not loaded';
	
	var outterResponse = {
			onSuccess: function(resp)
						{
							var ret = loadAllValidWorkers(response);
							if (ret !== true)
							{
								console.log('loadAll loadAllVal failed: '+ret);
								if (isObject(response) && response.hasOwnProperty('onFail'))
									response.onFail(response);
							}
						},
			onFail: function(resp)
						{
							console.log('loadAll onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
		};
	if (workerList.invalid.loaded)
		outterResponse.onSuccess(outterResponse);
	else if (!loadInvalid(outterResponse))
		return 'loadInvalid failed';
	return true;
}

function loadAllValidWorkers(response = null)
{
	if (!workerList.ready)
		return 'workerList not ready';
	if (!workerList.valid.indexLoaded)
		return 'valid index not loaded';
	
	var outterResponse = {
			onSuccess: function(resp)
						{
							var ret = loadAllValidWorkers(response);
							if (ret !== true)
							{
								console.log('loadAllVal rec failed: '+ret);
								if (isObject(response) && response.hasOwnProperty('onFail'))
									response.onFail(response);
							}
						},
			onFail: function(resp)
						{
							console.log('loadAllVal onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
		};
	
	for (var b = 0;b < workerList.valid.blockCount;b++)
		if (!workerList.valid.blocks[b].loaded)
		{
			if (!loadValBlock(b, outterResponse))
				return 'loadValBlock '+b+' failed';
			return true;
		}
	if (isObject(response) && response.hasOwnProperty('onSuccess'))
		response.onSuccess(response);
	return true;
}

function getWorkerName(id, loadNeeded = true, knownValid = null)
{
	if (!Number.isInteger(id))
		return {ok: false, error: 'id invalid', done: false, found: false, name: null};
	if (!workerList.ready)
		return {ok: false, error: 'workerList not ready', done: false, found: false, name: null};
	
	if (knownValid !== false && workerList.valid.indexLoaded)
	{
		for (var b = 0;b < workerList.valid.blockCount;b++)
			for (var i = 0;i < workerList.valid.blocks[b].count;i++)
				if (workerList.valid.blocks[b].idlist[i] == id)
				{
					if (!workerList.valid.blocks[b].loaded)
					{
						if (!loadNeeded)
							return {ok: false, error: 'not loaded', done: false, found: true, name: null};
						if (!loadValBlock(b))
							return {ok: false, error: 'loadValBlock failed', done: false, found: true, name: null};
						return {ok: true, error: '', done: false, found: true, name: null};
					}
					return {ok: true, error: '', done: true, found: true, name: workerList.valid.blocks[b].namelist[i], valid: true};
				}
	}
	if (knownValid !== true && workerList.invalid.indexLoaded)
	{
		for (var i = 0;i < workerList.invalid.count;i++)
			if (workerList.invalid.idlist[i] == id)
			{
				if (!workerList.invalid.loaded)
				{
					if (!loadNeeded)
						return {ok: false, error: 'not loaded', done: false, found: true, name: null};
					if (!loadInvalid())
						return {ok: false, error: 'loadInvalid failed', done: false, found: true, name: null};
					return {ok: true, error: '', done: false, found: true, name: null};
				}
				return {ok: true, error: '', done: true, found: true, name: workerList.invalid.namelist[i], valid: false};
			}
	}
	return {ok: true, error: '', done: true, found: false, name: null};
}

function getWorkerNameSimple(id, defResult = false, loadNeeded = true, knownValid = null)
{
	var r = getWorkerName(id, loadNeeded, knownValid);
	if (!r.ok || !r.done || !r.found || !r.name === null)
		return defResult;
	return r.name;
}

function searchWorker(nameQuery)
{
	if (!workerList.ready)
		return false;
	if (workerList.invalid.indexLoaded && workerList.invalid.loaded)
		for (var i = 0;i < workerList.invalid.count;i++)
			if (workerList.invalid.namelist[i] == nameQuery)
				return workerList.invalid.idlist[i];
	if (workerList.valid.indexLoaded)
		for (var b = 0;b < workerList.valid.blockCount;b++)
			if (workerList.valid.blocks[b].loaded)
				for (var i = 0;i < workerList.valid.blocks[b].count;i++)
					if (workerList.valid.blocks[b].namelist[i] == nameQuery)
						return workerList.valid.blocks[b].idlist[i];
	return false;
}

function searchWorkers(nameQuery)
{
	var foundCount = 0;
	var foundList = [];
	if (!workerList.ready)
		return false;
	if (workerList.invalid.indexLoaded && workerList.invalid.loaded)
		for (var i = 0;i < workerList.invalid.count;i++)
			if (workerList.invalid.namelist[i].indexOf(nameQuery) != -1)
				foundList[foundCount++] = { id: workerList.invalid.idlist[i], name: workerList.invalid.namelist[i], valid: false};
	if (workerList.valid.indexLoaded)
		for (var b = 0;b < workerList.valid.blockCount;b++)
			if (workerList.valid.blocks[b].loaded)
				for (var i = 0;i < workerList.valid.blocks[b].count;i++)
					if (workerList.valid.blocks[b].namelist[i].indexOf(nameQuery) != -1)
						foundList[foundCount++] = { id: workerList.valid.blocks[b].idlist[i], name: workerList.valid.blocks[b].namelist[i], valid: true};
	return foundList;
}
