/*
	dfahfolderhistory.js
	Data poller from JSON files prepared by DogecoinFah

	Requires:
		dfahgeneral.js (waitMainReady, loadFile, mfFileName)
		dfahtextmanager.js (getTextAsset)
		dfahlivedata.js (initLiveData, [branding configuration])
		dfahtxidlist.js (txidListFind)
		dfahworkerlist.js (getWorkerName)

	Data files:
		mfAsset.pubdatWorkerDataIndex (data/PublicWorkerData.json)
		mfAsset.pubdatWorkerData      (data/PublicWorkerData_%N.json)

	This file searches for matches to full or partial folder usernames.
	It displays a list of results to select from and when a single result
	is selected, it displays a detailed round, balance, and payment history for
	that folder (aka worker). This includes "invalid" workers, who's
	folding username is not a valid wallet address.
	
	Lazy Initialization and load:
	
	A quick init can be done by calling preInitFolderData(). Later
	calls to searchFolderData() will automatically call initFolderData()
	to initialization and start loading data, if is hasn't been done yet.

	Initialization checks if its dependencies need initialized, ensuring
	it's tolerant of some or all already being initialized, by testing
	if each of their state objects exist and their ready member is true.
		
	
	Folder History Searching:
	
	searchFolderData(query) is the main entry, supporting full or partial
	worker names. 

	WorkerList's loadAllWorkers() is called, ensuring both valid
	and invalid worker names are fully loaded. Then WorkerList's
	searchWorkers() function is used to provide the list of	results.
	Results with multiple entries then display a result list.
	
	Search results with only one exact matching entry proceed to show
	the worker's history details. WorkerList's getWorkerName() is used 
	to again get the worker id's name and valid status, as it may have
	been selected from a results list. An invalid result is then displayed
	as so.

	The index file lists which round ids are contained in
	each block file. Each block file contains data for many workers
	grouped in to reasonable file sizes.

	A valid result, then uses the index to determine which workerData block
	that worker id is in, loads that data block file, then displays the
	full history.
	
*/

var folderData = {
			initting: false,
			initStep: 0,
			ready: false,
			search: {
					query: '',
					active: false,
					searching: false,
					found: false,
					workerID: 0
				},
			index: {
					loading: false,
					loaded: false,
					blockCount: 0,
					blockSize: 0,
					workerCount: 0,
					payoutLimit: 0,
					blockIndexes: []					
				},
			results: {
				loading: false,
				loaded: false,
				workerID: 0,
				block: 0,
				name: '',
				valid: false,
				balance: 0,
				payouts: [],
				checkNameTimeout: null,
				onFail: null,
				onSuccess: null							
				}
		};

function clearFolderDataResults()
{
	folderData.search.query = '';
	folderData.search.searching = false;
	folderData.search.found = false;
	folderData.search.workerID = 0;
	folderData.results.loaded = false;
	folderData.results.loading = false;
	folderData.results.workerID = 0;
	folderData.results.block = 0;
	folderData.results.name = '';
	folderData.results.valid = false;
	folderData.results.balance = 0;
	folderData.results.payouts = [];
}		
function preInitFolderData()
{
	var ret = buildFolderDataResult();
	if (ret !== true)
		return 'preInit fd build result failed: '+ret;
	return true;
}

function initFolderData(response = null)
{
	// ------------------
	if (folderData.ready || folderData.initting)
	{
		console.log('');
		return false;
	}
	// ------------------
	folderData.initting = true;
	folderData.initStep = 0;
	// ------------------
	var innerResponse = {
			onSuccess: function(resp)
					{
						//console.log('initFolderData step '+folderData.initStep+' done successfully');
						folderData.initStep++;
						var ok = true;
						switch (folderData.initStep)
						{
							case 1:
								if (typeof dfLiveData !== 'undefined' && dfLiveData.ready)
								{
									resp.onSuccess(resp);
								}
								else if (!initLiveData(resp))
								{
									console.log('initFolderData initLiveData failed');
									ok = false;
								}
								break;
							case 2:
								if (typeof workerList !== 'undefined' && workerList.ready)
								{
									resp.onSuccess(resp);
								}
								else if (!initWorkers(true /*wantValid*/, true /*wantInvalid*/, resp))
								{
									console.log('initFolderData initWorkers failed');
									ok = false;
								}
								break;
							case 3:
								if (typeof txidList !== 'undefined' && txidList.ready)
								{
									resp.onSuccess(resp);
								}
								else if (!txidListInit(resp))
								{
									console.log('initFolderData txidListInit failed');
									ok = false;
								}
								break;
							case 4:
								if (!loadFolderDataIndex(resp))
								{
									console.log('initFolderData load index failed');
									ok = false;
								}
								break;
							case 5:
								folderData.initting = false;
								folderData.ready = true;
								var ret = buildFolderDataResult();
								if (ret !== true)
								{
									console.log('initFolderData build result 1 failed: '+ret);
									ok = false;
								}
								else if (isObject(response) && response.hasOwnProperty('onSuccess'))
									response.onSuccess(response);
								break;
						} // switch
						if (!ok)
						{
							console.log('initFolderData step '+folderData.initStep+' failed');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
					},
			onFail: function(resp)
					{
						console.log('initFolderData load index onFail');
						var ret = buildFolderDataResult();
						if (ret !== true)
							console.log('initFolderData build result 2 failed: '+ret);
						if (isObject(response) && response.hasOwnProperty('onFail'))
							response.onFail(response);
					}
				};
	// ------------------
	waitMainReady(innerResponse);
	// ------------------
	return true;
}

function loadFolderDataIndex(response = null)
{
	if (folderData.index.loaded)
	{
		console.log('load fd index already loaded');
		return false;
	}
	if (folderData.index.loading)
	{
		console.log('load fd index already loading');
		return false;
	}
	folderData.index.loading = true;
	var fileName = mfFileName(mfAsset.pubdatWorkerDataIndex);
	if (!loadFile(fileName, handleFolderDataIndex, response /*onDoneArg*/, false /*isBinary*/))
	{
		folderData.index.loading = false;
		console.log('load fd index failed');
		return false;
	}
	return true;
}

function handleFolderDataIndex(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseFolderDataIndex(data)
	folderData.index.loading = false;
	if (ret !== true)
	{
		console.log('parse fd Index failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function parseFolderDataIndex(data)
{
	var json = JSON.parse(data);
	if (!isObject(json))
		return 'data is not an object';
	if (!json.hasOwnProperty('ver') || !Number.isInteger(json.ver))
		return 'data validate ver field failed';
	if (json.ver > 1)
		return 'data has unsupported version: '+json.ver;
	if (!json.hasOwnProperty('block_count') || !json.hasOwnProperty('worker_count') || !json.hasOwnProperty('workers_per_block') || !json.hasOwnProperty('payout_limit') || !json.hasOwnProperty('blocks'))
		return 'data is missing fields';
	if (!Number.isInteger(json.block_count) || !Number.isInteger(json.workers_per_block) || !Number.isInteger(json.payout_limit) || !Number.isInteger(json.payout_limit))
	{
		console.log(json);
		return 'data validate int fields failed';
	}
	if (!Array.isArray(json.blocks))
		return 'data validate array block field failed';
		
	folderData.index.blockCount = json.block_count;	
	folderData.index.blockSize = json.workers_per_block;	
	folderData.index.workerCount = json.worker_count;	
	folderData.index.payoutLimit = json.payout_limit;	
	folderData.index.blockIndexes = json.blocks;	
	folderData.index.loaded = true;
	return true;
}

function loadFolderData(workerID)
{
	if (folderData.results.loading)
	{
		console.log('load fd results already loading');
		return false;
	}
	folderData.results.loading = true;
	folderData.results.workerID = workerID;
	checkFolderDataName();	
	return true;
}

function failFolderDataResult()
{
	folderData.results.loading = false;
	var ret = buildFolderDataResult();
	if (ret !== true)
		console.log('fail fd build fd result failed: '+ret);
	if (folderData.results.onFail !== null)
		folderData.results.onFail();
}

function checkFolderDataName()
{
	if (folderData.results.nameTimeout !== null)
	{
		clearTimeout(folderData.results.nameTimeout);
		folderData.results.nameTimeout = null;
	}
	if (!folderData.results.loading)
	{
		console.log('ch fd name no longer loading?');
		return; // canceled?
	}
	var ret = getWorkerName(folderData.results.workerID);
	if (!ret.ok)
	{
		failFolderDataResult();
		console.log('ch fd name failed: '+ret.error);
		return;
	}
	if (!ret.done)
	{
		folderData.results.nameTimeout = setTimeout('checkFolderDataName()', 100);
		return;
	}
	if (!ret.found || ret.name === null)
	{
		folderData.results.name = '[not found]';
		folderData.results.valid = false;
	}
	else
	{
		folderData.results.name = ret.name;
		folderData.results.valid = ret.valid;
	}
	if (!folderData.results.valid) // invalid
	{
		folderData.results.block   = -1;
		folderData.results.balance = 0;
		folderData.results.payouts = [];
		folderData.results.loading = false;
		folderData.results.loaded  = true;
		var ret = buildFolderDataResult();
		if (ret !== true)
			console.log('ch fd name, invalid, build fd result failed: '+ret);
		if (folderData.results.onSuccess !== null)
			folderData.results.onSuccess();
		return;
	}
	var block = findFolderDataBlock(folderData.results.workerID);
	if (!Number.isInteger(block))
	{
		failFolderDataResult();
		console.log('ch fd name find block failed: '+block)
		return;
	}
	if (block == -1)
	{
		failFolderDataResult();
		console.log('ch fd name find block not found wid: '+folderData.results.workerID);
		//console.log(block);
		return;
	}
	folderData.results.block = block;
	var response = {
			onFail: function(resp)
					{
						console.log('ch fd name load fd block '+folderData.results.block+' onFail');
						failFolderDataResult();
					},
			onSuccess: function(resp)
					{
						folderData.results.loading = false;
						var ret = buildFolderDataResult();
						if (ret !== true)
							console.log('ch fd name load fd block success, but build fd result failed: '+ret);
						if (folderData.results.onSuccess !== null)
							folderData.results.onSuccess();
					}		
				};
	if (!loadFolderDataBlock(folderData.results.block, response))
	{
		console.log('ch fd name load fd block '+folderData.results.block+' failed');
		failFolderDataResult();
	}
}

function findFolderDataBlock(workerID)
{
	if (!Number.isInteger(workerID))
		return 'find fd b validate wid failed';
	if (!folderData.index.loaded)
		return 'find fd b index not loaded';
	var bLen = folderData.index.blockIndexes.length;
	if (bLen != folderData.index.blockCount)
		return 'find fd b block index length doesnt match count: '+bLen+'/'+folderData.index.blockCount;

	for (var b = 0;b < bLen;b++)
	{
		if (!Array.isArray(folderData.index.blockIndexes[b]) || folderData.index.blockIndexes[b].length != 2)
			return 'find fd b block index entry bad: '+b;
		if (workerID >= folderData.index.blockIndexes[b][0] && workerID <= folderData.index.blockIndexes[b][1])
			return b;
	}
	return -1;
}

function loadFolderDataBlock(block, response = null)
{
	var fileName = mfFileName(mfAsset.pubdatWorkerData, block);
	if (!loadFile(fileName, handleFolderDataBlock, response /*onDoneArg*/, false /*isBinary*/))
	{
		console.log('load fd index failed');
		return false;
	}
	return true;
}

function handleFolderDataBlock(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = parseFolderDataBlock(data)
	if (ret !== true && ret !== -1 /*success, was invalid*/)
	{
		console.log('parse fd block failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}
}

function parseFolderDataBlock(data)
{
	if (!folderData.results.loading)
		return 'not in loading state';
	var json = JSON.parse(data);
	if (!Array.isArray(json))
		return 'data is not an array';
	var wlen = json.length;
	for (var w = 0;w < wlen;w++)
	{
		if (!Array.isArray(json[w]) || json[w].length < 1 || !Number.isInteger(json[w][0]))
			return 'worker entry validate idx failed';
		var widx = json[w][0];
		if (widx == -1) // special section of empty workers
		{
			if (json[w].length != 2 || !Array.isArray(json[w][1]))
				return 'worker empty entry validate failed';
			var ewList = json[w][1];
			var ewlen = ewList.length;
			for (var ew = 0;ew < ewlen;ew++)
				if (Number.isInteger(ewList[ew]) && ewList[ew] == folderData.results.workerID)
				{
					folderData.results.balance = 0;
					folderData.results.payouts = [];
					folderData.results.loaded = true;
					console.log('parse fd block found in empty worker list, done.');
					return true;
				}
		}
		else if (json[w][0] == folderData.results.workerID)
		{
			if (json[w].length != 3)
				return 'worker entry validate len failed';
			var bal = parseFloat(json[w][1]);
			if (isNaN(bal))
				return 'worker entry validate bal failed w '+w+'/'+wlen;
			folderData.results.balance = bal;
			var payouts = json[w][2];
			if (!isObject(payouts))
			{
				if (Number.isInteger(payouts) && payouts == 0)
					folderData.results.payouts = []; // none
				else
					return 'worker entry validate payouts failed';					
			}
			else
			{
				var pcount = 0;
				if (payouts.hasOwnProperty('pd'))
				{
					if (!Array.isArray(payouts.pd))
						return 'worker entry validate pd failed';
					var plen = payouts.pd.length;
					for (var p = 0;p < plen;p++)
					{
						payout = payouts.pd[p];
						if (!Array.isArray(payout) || payout.length != 8 || !Number.isInteger(payout[0]) || !Number.isInteger(payout[6]) || !Number.isInteger(payout[7]))
							return 'payout pd validate failed';
						var id = payout[0];
						var pay = parseFloat(payout[1]);
						var statPay = parseFloat(payout[2]);
						var storePay = parseFloat(payout[3]);
						 // unix timestamps are secs, JS Date wants timestamps in ms
						var utsStored = (payout[4] === 0 ? 0 : parseInt(payout[4]));
						var utsPaid = (payout[5] === 0 ? 0 : parseInt(payout[5]));
						var dtStored = (utsStored === 0 || isNaN(utsStored) ? false : new Date(utsStored * 1000));
						var dtPaid = (utsPaid === 0 || isNaN(utsPaid) ? false : new Date(utsPaid * 1000));
						var storePayoutID = payout[6];
						var txid = payout[7]; // index into list
						if (isNaN(pay) || isNaN(statPay) || isNaN(storePay) || (dtStored !== false && isNaN(dtStored)) || (dtPaid !== false && isNaN(dtPaid)) )
							return 'payout pd validate fields failed';
						folderData.results.payouts[pcount++] = {id: id, type: 'P', pay: pay, statPay: statPay, storePay: storePay, dtStored: dtStored, dtPaid: dtPaid, storePayoutID: storePayoutID, txid: txid};
					}
				}
				if (payouts.hasOwnProperty('upd'))
				{
					if (!Array.isArray(payouts.upd))
						return 'worker entry validate upd failed';
					var plen = payouts.upd.length;
					for (var p = 0;p < plen;p++)
					{
						payout = payouts.upd[p];
						if (!Array.isArray(payout) || payout.length != 3 || !Number.isInteger(payout[0]))
							return 'payout upd validate failed';
						var id = payout[0];
						var statPay = parseFloat(payout[1]);
						 // unix timestamps are secs, JS Date wants timestamps in ms
						var utsStored = (payout[2] === 0 ? 0 : parseInt(payout[2]));
						var dtStored = (utsStored === 0 || isNaN(utsStored) ? false : new Date(utsStored * 1000));
						if (isNaN(statPay) || (dtStored !== false && isNaN(dtStored)))
							return 'payout upd validate fields failed';
						folderData.results.payouts[pcount++] = {id: id, type: 'U', statPay: statPay, dtStored: dtStored};
					}
				}
				if (payouts.hasOwnProperty('ff'))
				{
					if (!Array.isArray(payouts.ff))
						return 'worker entry validate ff failed';
					var plen = payouts.ff.length;
					for (var p = 0;p < plen;p++)
					{
						payout = payouts.ff[p];
						if (!Array.isArray(payout) || payout.length != 4 || !Number.isInteger(payout[0]))
							return 'payout ff validate failed';
						var id = payout[0];
						var statPay = parseFloat(payout[1]);
						 // unix timestamps are secs, JS Date wants timestamps in ms
						var utsStored = (payout[2] === 0 ? 0 : parseInt(payout[2]));
						var utsPaid = (payout[3] === 0 ? 0 : parseInt(payout[3]));
						var dtStored = (utsStored === 0 || isNaN(utsStored) ? false : new Date(utsStored * 1000));
						var dtPaid = (utsPaid === 0 || isNaN(utsPaid) ? false : new Date(utsPaid * 1000));
						if (isNaN(statPay) || (dtStored !== false && isNaN(dtStored)) || (dtPaid !== false && isNaN(dtPaid)) )
							return 'payout ff validate fields failed';
						folderData.results.payouts[pcount++] = {id: id, type: 'F', statPay: statPay, dtStored: dtStored, dtPaid: dtPaid};
					}
				}				
			}
			folderData.results.loaded = true;
			return true;			
		}		
	} // for w
	folderData.results.balance = 0;
	folderData.results.payouts = [];
	folderData.results.valid = false;
	folderData.results.loaded = true;
	//console.log('parse fd block worker not found in block (not valid), done.');
	return -1;
}

function searchFolderData(query)
{
	if (!folderData.ready)
	{
		if (!initFolderDataBeforeSearch(query))
		{
			console.log('search initBefore failed');
			return false;
		}
		return true; // searchFolderData will be called again when the init is done
	}
	//console.log("searching: '"+query+"'");
	clearFolderDataResults();
	folderData.search.query = query;
	folderData.search.active = true;
	folderData.search.searching = true;
	var response = {
			onSuccess: function(resp)
						{
							var ret = searchWorkers(folderData.search.query.trim());
							if (!Array.isArray(ret) || ret.length == 0)
							{
								if (!Array.isArray(ret))
									console.log('search searchWorkers failed: '+ret);
								else
									console.log('search searchWorker not found: '+folderData.search.query);
								folderData.search.searching = false;
								buildFolderDataResult();
								console.log(workerList);
								return;
							}
							if (ret.length > 1 || (ret.length == 1 && ret[0].name != folderData.search.query.trim()))
							{
								folderData.search.searching = false;
								buildFolderSearchResult(ret);
								return;
							}
							// fall through if only one exact result
							folderData.search.workerID = ret[0].id;
							folderData.search.found = true;
							folderData.search.searching = false;
							buildFolderDataResult();
							if (!loadFolderData(folderData.search.workerID))
							{
								console.log('search loadFolderData failed');
								folderData.search.searching = false;
								buildFolderDataResult();
								return;
							}
							
						},
			onFail: function(resp)
						{
							console.log('search loadAllWorkers onFail');
							olderData.search.searching = false;
							buildFolderDataResult();
						}
		};
	
	var ret = loadAllWorkers(response);
	if (ret !== true)
	{
		console.log('search loadAllWorkers failed: '+ret);
		folderData.search.searching = false;
		return false;
	}
	return true;
}

function initFolderDataBeforeSearch(query)
{
	var response = {
			onSuccess: function(resp)
						{
							if (!folderData.ready)
							{
								console.log('initBeforeSearch onSuccess verify ready failed'); // prevent loop
								return;
							}
							if (!searchFolderData(query))
							{
								console.log('initBeforeSearch searchFolderData failed');
								return;
							}
						},
			onFail: function(resp)
						{
							console.log('initBeforeSearch onFail');
						}
		};	
	
	if (!initFolderData(response))
	{
		console.log('initBeforeSearch initFolderData failed');
		return false;
	}
	return true;
}

function getFolderDataResultText()
{
	var htmlResult = "<p><a style='float:left;font-size:initial;' href='./history.html?s="+encodeURIComponent(folderData.results.name)+"'><i class='fa fa-link' aria-hidden='true'>&nbsp;</i></a>";
	if (folderData.results.valid)
		htmlResult += getTextAsset('folderhistory/result/title/valid', "Folder: ($$ID$$) $$NAME$$ (valid address)").replaceAll('$$ID$$', folderData.results.workerID).replaceAll('$$NAME$$', folderData.results.name);
	else
		htmlResult += getTextAsset('folderhistory/result/title/invalid', "Folder: ($$ID$$) $$NAME$$ (not a valid address)").replaceAll('$$ID$$', folderData.results.workerID).replaceAll('$$NAME$$', folderData.results.name);
	htmlResult += "</p>";

	htmlResult += "<p>" + getTextAsset('folderhistory/result/balance', "Reward Balance: $$VALUE$$ $$BRAND_UNIT$$").replaceAll('$$VALUE$$', folderData.results.balance).replaceAll('$$BRAND_UNIT$$', Brand_Unit) + "</p>";
	var plen = folderData.results.payouts.length;
	if (plen == 0)
		htmlResult += "<p>" + getTextAsset('folderhistory/result/nohistory', "No reward history.")+ "</p>";
	else
	{
		var p;
		var sort = 1;
		for (p = 0;p < plen;p++)
		{
			var payout = folderData.results.payouts[p];
			payout.order = p;
			if (payout.type == 'P')
			{
				if (payout.dtStored !== false)
					payout.dtSortFirst = payout.dtStored;
				else
					payout.dtSortFirst = payout.dtPaid;
				payout.dtSortLast = payout.dtPaid;
			}
			else if (payout.type == 'U')
			{
				if (payout.dtStored !== false)
					payout.dtSortFirst = payout.dtSortLast = payout.dtStored;
				else
					payout.dtSortFirst = payout.dtSortLast = false;
			}
			else if (payout.type == 'F')
			{
				if (payout.dtStored !== false)
					payout.dtSortFirst = payout.dtStored;
				else
					payout.dtSortFirst = payout.dtPaid;
				payout.dtSortLast = payout.dtPaid;
			}						
		}
		if (Math.abs(sort) == 1) // payout id
		{
			var passCount = 0;
			var done = false;
			while (!done)
			{
				done = true;
				for (p = 1;p < plen;p++)
				{
					tp = folderData.results.payouts[folderData.results.payouts[p-1].order].id;
					np = folderData.results.payouts[folderData.results.payouts[p].order].id;
					if ( (sort > 0 && tp > np) || (sort < 0 && tp < np) )
					{
						var tmp =  folderData.results.payouts[p-1].order;
						folderData.results.payouts[p-1].order = folderData.results.payouts[p].order;
						folderData.results.payouts[p].order = tmp;
						done = false;
					}
				}
				passCount++;
				if (passCount > 100)
				{
					console.log('passCount exceeded max, stopping sort');
					done = true;
				}
				
			}
		}
		htmlResult += "<table>";
		htmlResult += "<tr>";
		//htmlResult += "<th>" + getTextAsset('folderhistory/result/th/round', "Round") + "</th>";
		htmlResult += "<th>" + getTextAsset('folderhistory/result/th/date', "Date") + "</th>";
		htmlResult += "<th>" + getTextAsset('folderhistory/result/th/payid', "ID") + "</th>";
		htmlResult += "<th>" + getTextAsset('folderhistory/result/th/reward', "Reward $$BRAND_UNIT$$").replaceAll('$$BRAND_UNIT$$', Brand_Unit) + "</th>";
		htmlResult += "<th>" + getTextAsset('folderhistory/result/th/status', "Status") + "</th>";
		htmlResult += "<tr>";
		for (p = 0;p < plen;p++)
		{
			payout = folderData.results.payouts[folderData.results.payouts[p].order];
			//var strRound = "";
			var strDate = (payout.dtStored !== false ? payout.dtStored.toLocaleDateString() : (typeof payout.dtPaid !== 'undefined' && payout.dtPaid !== false ? payout.dtPaid.toLocaleDateString() : getTextAsset('folderhistory/result/baddate', "????") ));
			var strPayid = payout.id;
			var strReward = payout.statPay.toFixed(Brand_Pay_Digits);
			var strStatus = "";
			if (payout.dtStored !== false || payout.type == 'U' || payout.type == 'F')
			{
				if (payout.type == 'P' && payout.storePayoutID != -1)
					strStatus += getTextAsset('folderhistory/result/status/storedpaid', "stored, later paid by ID $$PAYID$$ on $$DATE$$").replaceAll('$$PAYID$$', payout.storePayoutID).replaceAll('$$DATE$$', (payout.dtPaid !== false ? payout.dtPaid.toLocaleDateString() : getTextAsset('folderhistory/result/baddate', "????")) );
				else if (payout.type == 'F')
					strStatus += getTextAsset('folderhistory/result/status/forfeit', "stored (not paid), later forfeited on $$DATE$$").replaceAll('$$DATE$$', (payout.dtPaid !== false ? payout.dtPaid.toLocaleDateString() : getTextAsset('folderhistory/result/baddate', "????")) );
				else
					strStatus += getTextAsset('folderhistory/result/status/stored', "stored (not paid)");
				if (payout.dtStored === false && payout.type == 'U')
				{
					strStatus += getTextAsset('folderhistory/result/error', "(error detected)");
					console.log("Error: result entry type is unpaid, but dtStored is not set. Payout: ");
					console.log(payout);
				}
			}
			else
			{
				var txid = txidListFind(payout.txid);
				var strTxid;
				if (txid === false)
					strTxid = getTextAsset('folderhistory/txid/error', "[error]");
				else if (txid === 0)
					strTxid = getTextAsset('folderhistory/txid/notfound', "[not found]");
				else if (txid.length < 10)
					strTxid = txid;
				else
					strTxid = "<a href='"+Brand_Tx_Link+txid+"'>"+txid.substring(0, 7)+"...</a>";

				if (payout.pay == payout.statPay)
					strStatus += getTextAsset('folderhistory/result/status/paid',"paid, txid: $$TXID$$").replaceAll('$$TXID$$', strTxid);
				else
				{
					strReward += " (+" + payout.storePay +")";
					strStatus += getTextAsset('folderhistory/result/status/paidwithstored',"paid total $$TOTALPAY$$ $$BRAND_UNIT$$, txid: $$TXID$$").replaceAll('$$TOTALPAY$$', payout.pay).replaceAll('$$BRAND_UNIT$$', Brand_Unit).replaceAll('$$TXID$$', strTxid);
				}
			}
			htmlResult += "<tr>";
			//htmlResult += "<td>"+ strRound + "</td>";
			htmlResult += "<td>"+ strDate + "</td>";
			htmlResult += "<td>"+ strPayid + "</td>";
			htmlResult += "<td>"+ strReward + "</td>";
			htmlResult += "<td>"+ strStatus + "</td>";
			htmlResult += "</tr>";
		} // for p (display)					
		htmlResult += "</table>";
	} // if plen
	return htmlResult;
}

function selectSearchResult(selId, selName)
{
	folderData.search.query = selName;
	folderData.search.workerID = selId;
	folderData.search.found = true;
	folderData.search.searching = false;
	buildFolderDataResult();
	if (!loadFolderData(folderData.search.workerID))
	{
		console.log('selSearchRes loadFolderData failed');
		folderData.search.searching = false;
		buildFolderDataResult();
		return;
	}	
}

function buildFolderSearchResult(resultList)
{
	var main = document.getElementById('searchFolder');
	if (!main)
		return 'validate main element failed';
	if (resultList.length == 0)
		return 'result list is empty';
	var html = "<div style='width:55%;background-color:#474747;padding: 5px 2px;margin-bottom:1em;'>";
	html += "<h3 style='color:#ffffff;margin-bottom:0.4em;'>" +  getTextAsset('folderhistory/searchbox/title', "Lookup folding username") + "</h3>";
	html += "<form style='margin:0 0 0.2em 0; text-align:left;' method='post' action='javascript' enctype='text/plain' onsubmit='searchFolderData(this.qry.value);return false;'>";
	html += "<input style='display:inline;padding: 0 0.25em;width:70%;background-color:#878787;color:#ffffff;' id='qry' name='qry' type='text' maxlength='50' "+(folderData.search.active ? "value='"+folderData.search.query+"'" : "")+" />";
	html += "<input style='width:30%;' type='submit' value='"+ getTextAsset('folderhistory/searchbox/button', "Search") + "' />";
	html += "</form>";
	html += "<div style='width:100%;max-height:500px;padding:0.25em;overflow:auto;text-align:left;line-height:18px;background-color:#cdced0;'><ul>";
	for (var i = 0;i < resultList.length;i++)
		html += "<li><a href=\"javascript:selectSearchResult("+resultList[i].id+",'"+resultList[i].name+"');\">"+resultList[i].name+"</a></li>";
	html += "</ul></div></div>";
	main.innerHTML = html;
	return true;
}

function buildFolderDataResult()
{
	var main = document.getElementById('searchFolder');
	if (!main)
		return 'validate main element failed';
	var htmlSearch = "<h3 style='color:#ffffff;margin-bottom:0.4em;'>" +  getTextAsset('folderhistory/searchbox/title', "Lookup folding username") + "</h3>";
	htmlSearch += "<form style='margin:0 0 0.2em 0; text-align:left;' method='post' action='javascript' enctype='text/plain' onsubmit='searchFolderData(this.qry.value);return false;'>";
	htmlSearch += "<input style='display:inline;padding: 0 0.25em;width:70%;background-color:#878787;color:#ffffff;' id='qry' name='qry' type='text' maxlength='50' "+(folderData.search.active ? "value='"+folderData.search.query+"'" : "")+" />";
	htmlSearch += "<input style='width:30%;' type='submit' value='"+ getTextAsset('folderhistory/searchbox/button', "Search") + "' />";
	htmlSearch += "</form>";
	var haveResult = false;
	var htmlStatus = "";
	var htmlTextResult = "";
	if (!folderData.search.active)
		htmlStatus += getTextAsset('folderhistory/state/initial', "Search for your folding username ($$BRAND_NAME$$ address) to lookup your reward balance and payout history.").replaceAll('$$BRAND_NAME$$', Brand_Name);
	else
	{
		if (folderData.search.searching)
			htmlStatus += "<h3>" + getTextAsset('folderhistory/state/searching/title', "Searching") + "</h3><p>" + getTextAsset('folderhistory/state/searchingfor/details', "Searching for &quot;<sup>$$QUERY$$</sup>&quot;...").replaceAll('$$QUERY$$', folderData.search.query) + "</p>";
		else if (!folderData.search.found)
			htmlStatus += "<h3>" + getTextAsset('folderhistory/state/notfound/title', "Not Found") + "</h3><p>" + getTextAsset('folderhistory/state/notfound/details', "Searching for &quot;<sup>$$QUERY$$</sup>&quot;...").replaceAll('$$QUERY$$', folderData.search.query) + "</p>";
		else
		{
			if (folderData.results.loading)
				htmlStatus += "<h3>" + getTextAsset('folderhistory/state/loading/title', "Folder History") + "</h3><p>" + getTextAsset('folderhistory/state/loading/query', "Found &quot;<sup>$$QUERY$$</sup>&quot;").replaceAll('$$QUERY$$', folderData.search.query) + "</p><p>" + getTextAsset('folderhistory/state/loading/details', "Loading...") + "</p>";
			else if (!folderData.results.loaded)
				htmlStatus += "<h3>" + getTextAsset('folderhistory/state/loadfail/title', "Folder History") + "</h3><p>" + getTextAsset('folderhistory/state/loadfail/query', "Found &quot;<sup>$$QUERY$$</sup>&quot;").replaceAll('$$QUERY$$', folderData.search.query) + "</p><p>" + getTextAsset('folderhistory/state/loadfail/details', "Failed to load.") + "</p>";
			else
			{
				haveResult = true;
				htmlTextResult = getFolderDataResultText(); // clear previous content
			}
		}
	}
	
	if (htmlStatus != '')
	{
		htmlStatus = "<div style='width:100%;padding:0.25em;text-align:left;line-height:18px;background-color:#cdced0;'>" + htmlStatus + "</div>";
		htmlSearch += htmlStatus;
	}
	if (htmlTextResult != '')
	{
		htmlTextResult = "<div style='width:100%;max-height:500px;overflow:auto;padding:0.25em;text-align:left;line-height:18px;background-color:#cdced0;'>" + htmlTextResult + "</div>";
		htmlSearch += htmlTextResult;
	}
	htmlSearch = "<div style='width:"+(haveResult ? "80%" : "55%")+";background-color:#474747;padding: 5px 2px;margin-bottom:1em;'>" + htmlSearch + "</div>";
	main.innerHTML = htmlSearch;
	if (folderData.search.active)
		main.scrollIntoView(false);
	return true;
}
