/*
	dfahlivedata.js
	Live data poller from JSON file prepared by DogecoinFah

	Requires:
		dfahgeneral.js (loadFile, mfFileName)
		dfahtextmanager.js (initTxtMan, getTextAsset)

	Data files:
		mfAsset.pubdatBalance (data/PublicData.json)
		mfAsset.pubdatTeam (data/PublicTeamData.json)
		
	This file loads data and displays the donate widget (top right) with a
	QR code, the contribution ads, the balance widget (middle) listing
	contribution amounts and totals, and the team widget (top left).
	It also loads the branding configuration.

*/
//------Customize--------------
// These brand values are later processed from PublicData.json by updateData() and shared with the other JS files.
var Brand_Address_Link = "/";
var Brand_Tx_Link = "/";
var Brand_Unit = "???";
var Brand_Name = "???";
var Brand_Pay_Digits = "???";
var Brand_Asset_ID = "???";
//-----------------------------
var dfLiveData = {
					initting: false,
					ready: false,
					updateRate: 1200000, // 0 for off, 180,000 ms / 3 min, 300,000 ms / 5 min, 1,200,000 ms / 20 min
					lastAddress: "",
					updatePending: false,
					teamDataRequested: false
				 };
//-----------------------------
const C_CONM_NONE		= 0;
const C_CONM_FLAT		= 1;
const C_CONM_PER		= 2;
const C_CONM_ALL		= 3;
const C_CONM_EACH		= 4;
const C_CONM_PROPORTION	= 5;
const C_CONM_RANDDROP	= 6;
const C_CONM_PAY_INACT_STORED = 7; // special, not displayed

const C_CONF_DISABLED	= 1;
const C_CONF_WAITFUNDS	= 2;
const C_CONF_FULLBAL	= 4;
const C_CONF_MAXTOTAL	= 8;
const C_CONF_MAXCOUNT	= 16;

const CONT_AD_FLAG_NONE =  			0;
const CONT_AD_FLAG_SHOW_ALWAYS = 	1;
const CONT_AD_FLAG_SHOW_TITLE = 	2;
const CONT_AD_FLAG_SHOW_VALUE = 	4;
const CONT_AD_FLAG_SHOW_TEXT =	 	8;
const CONT_AD_FLAG_SHOW_IMAGE = 	16;
const CONT_AD_FLAG_LINK_TITLE =  	32;
const CONT_AD_FLAG_LINK_TEXT =  	64;
const CONT_AD_FLAG_LINK_IMAGE = 	128;


if (!Date.now){ // IE8 compat
	Date.now = function() { return new Date().getTime(); } 
} 
function XHasFlag(flag, value)
{
	return ((flag & value) != 0 ? true : false);
}

function setCharAt(str,index,chr) {
    if (index >= str.length) return str;
    //console.log("full '" +str+ "' first (" + index +") '" + str.substr(0,index) + "' end '" +  str.substr(index+1) + "' char '" + chr + "'");
    return str.substr(0,index) + chr + str.substr(index+1);
}
function initLiveData(response = null)
{
	// ------------------
	if (dfLiveData.ready || dfLiveData.initting)
	{
		console.log('initLiveData already ready/initting');
		return false;
	}
	// ------------------
	dfLiveData.initting = true;
	dfLiveData.ready = false;
	// ------------------Extra init for email protection
	var m1 = document.getElementById("m1");
	var m2 = document.getElementById("m2");
	var m3 = document.getElementById("m3");
	var m4 = document.getElementById("m4");
	if (m1 && m1.href.length > 27)
		m1.href = m1.href.substr(0,10) + '@' + m1.href.substr(13, 11) + '.' + m1.href.substr(27);
	if (m2 && m2.href.length > 27)
		m2.href = m2.href.substr(0,10) + '@' + m2.href.substr(13, 11) + '.' + m2.href.substr(27);
	if (m3 && m3.href.length > 28)
		m3.href = m3.href.substr(0,12) + '@' + m3.href.substr(15, 11) + '.' + m3.href.substr(29);
	if (m4 && m4.href.length > 28)
		m4.href = m4.href.substr(0,12) + '@' + m4.href.substr(15, 11) + '.' + m4.href.substr(29);
	// ------------------
	var innerResponse = {
		onSuccess: function(resp)
					{
						dfLiveData.initting = false;
						dfLiveData.ready = true;
						requestLiveData(response); // Make the first request, respond to initLiveData caller
					},
		onFail: function(resp)
					{
						console.log('initLiveData: initTxtMan failed cannot continue');
						if (isObject(response) && response.hasOwnProperty('onFail'))
							response.onFail(response);
					}
				};
	// ------------------
	initTxtMan(innerResponse);
	// ------------------
	return true;
} // initLiveData

function handleLiveData(status, data, response = null)
{
	if (dfLiveData.updateRate != 0 && !dfLiveData.updatePending)
	{
		dfLiveData.updatePending = true;
		setTimeout("requestLiveData()", dfLiveData.updateRate);
	}
	if (status != '200')
	{
		console.log('requestLiveData: http request failed with status: '+status);
		return;// don't request team yet, but start timer so this retries
	}
	updateData(JSON.parse(data));
	if (!dfLiveData.teamDataRequested)
	{
		dfLiveData.teamDataRequested = true;
		requestLiveTeamData(); // request Live Team Data only once, after first LiveData request
	}
	if (isObject(response) && response.hasOwnProperty('onSuccess')) // respond to caller of requestLiveData (initLiveData)
		response.onSuccess(response);
}

function requestLiveData(response = null)
{
	dfLiveData.updatePending = false;
	var fileName = mfFileName(mfAsset.pubdatBalance);
	if (!loadFile(fileName, handleLiveData, response))
	{
		console.log('requestLiveData loadFile failed');
		return false;
	}
	return true;	
}

function handleLiveTeamData(status, data)
{
	if (status != '200')
	{
		console.log('requestLiveTeamData: http request failed with status: '+status);
		return;
	}
	try
	{
		updateTeamData(JSON.parse(data));		  
	}
	catch(e)
	{
		// request probably failed (parsing exception)
	}
}

function requestLiveTeamData()
{
	var fileName = mfFileName(mfAsset.pubdatTeam);
	if (!loadFile(fileName, handleLiveTeamData))
	{
		console.log('requestLiveTeamData loadFile failed');
		return false;
	}
}

function getBalRow(name, val, unit = false){
	return "<tr><td>"+name+"</td><td>"+val+"</td><td>" + (unit == false ? "&nbsp;" : unit ) + "</td></tr>";
}

function getTeamRow(name, val){
	return "<tr><td>"+name+"</td><td>"+val+"</td></tr>";
}

function updateData(jsonObj)
{
	// Set global brand info from JSON. These are also used by dfahlivehistorydata.js and dfahlivehistorychart.js too.
	if (typeof jsonObj.brand_addr_link !== "undefined")
		Brand_Address_Link = jsonObj.brand_addr_link;
	if (typeof jsonObj.brand_tx_link !== "undefined")
		Brand_Tx_Link = jsonObj.brand_tx_link;
	if (typeof jsonObj.brand_name !== "undefined")
		Brand_Name = jsonObj.brand_name;
	if (typeof jsonObj.brand_unit !== "undefined")
		Brand_Unit = jsonObj.brand_unit;
	if (typeof jsonObj.pay_digits !== "undefined")
		Brand_Pay_Digits = jsonObj.pay_digits;
	if (typeof jsonObj.brand_asset !== "undefined")
		Brand_Asset_ID = jsonObj.brand_asset;

	var dcLink = Brand_Address_Link + jsonObj.activeAddress;
	var htmlBal = "<table id='livebalextra'>";
	var htmlAltBal = "";
	var fee =  parseFloat(jsonObj.fee);
	var bal = parseFloat(jsonObj.activeBalance);
	var payoutActive = (parseInt(jsonObj.payoutActive) == 0 ? false : true);
	var dtUpdated = new Date(parseInt(jsonObj.updated) * 1000); // unix timestamps are secs, JS Date wants timestamps in ms
	var age = ((new Date()) - dtUpdated) / 1000.0 / 60.0; // Date values are in ms, convert to minutes.
	var htmlUpdated = "<div style='line-height:90%;";
	if (age >= 120.0) // Update expected every 5 minutes. Red >= one hour, Yellow >= 10 minutes
		htmlUpdated += "color:red;";
	else if (age >= 30.0)
		htmlUpdated += "color:#cc6600"; //#FF9205;"; // orange
	else 
		htmlUpdated += "font-size:90%;";
	htmlUpdated += "'>" + getTextAsset('livebal/updated', "Updated:") + " "+dtUpdated.toLocaleString(undefined/*def local*/, {timeZoneName: 'short'})+"</div>";
	if (payoutActive)
	{
		htmlBal = "<div style='line-height:97%;margin:4px 0;color:#0066ff;'><sup>&Dagger;</sup> "+getTextAsset('livebal/inprogress', "Payout in progress")+" <i class='fa fa-refresh fa-spin'></i></div>" + htmlBal;
		bal = 0;
	}
	htmlBal += "<span style='line-height:90%;font-size:90%;font-weight:600;'>" + getTextAsset('livebal/prop', "Proportional:") + "</span><br/>";
	htmlBal += getBalRow(getTextAsset('livebal/cont/donations', "Donations"), bal + (payoutActive ? " <sup>&Dagger;</sup>" : ""));
	bal -=  fee; // take fee off top, so % contrs calc correctly
    var DebugBalance = false;
	for (var i = 0;i < jsonObj.contributions.length;i++)
	{
	  var c = jsonObj.contributions[i];
	  var name = atob(c.name);
	  var value = parseFloat(c.value);
	  var subvalue = parseFloat(c.subvalue);
	  var countvalue = parseInt(c.countvalue);
	  var mode = parseInt(c.mode);
	  var flags = parseInt(c.flags);
	  var fDisabled = XHasFlag(flags, C_CONF_DISABLED);
	  var fMaxTotal = XHasFlag(flags, C_CONF_MAXTOTAL);
	  var fMaxCount = XHasFlag(flags, C_CONF_MAXCOUNT);
	  if (mode != C_CONM_NONE && mode != C_CONM_PAY_INACT_STORED && !fDisabled)
	  {
		  if (name == "") name = getTextAsset('livebal/cont/defname', "Contribution")+" "+parseInt(c.order);
		  if (mode == C_CONM_EACH)
		  {
			  var fmtValue = (fMaxTotal ? getTextAsset('livebal/cont/each/value/max', "$$VALUE$$ of $$MAX$$ max") : getTextAsset('livebal/cont/each/value/normal', "$$VALUE$$"));
			  var fmtCount = (fMaxCount ? getTextAsset('livebal/cont/each/count/max', "each of $$MAX$$ max") : getTextAsset('livebal/cont/each/count/normal', "each"));
			  value = fmtValue.replaceAll('$$VALUE$$', value).replaceAll('$$MAX$$', subvalue);
			  count = fmtCount.replaceAll('$$MAX$$', countvalue);
			  htmlAltBal += getBalRow(name, value, count);
		  }
		  else if (mode == C_CONM_RANDDROP)
		  {
			  var fmtValue = (fMaxTotal ? getTextAsset('livebal/cont/randdrop/value/max', "$$VALUE$$ of $$MAX$$ max") : getTextAsset('livebal/cont/randdrop/value/normal', "$$VALUE$$"));
			  var fmtCount = (fMaxCount ? getTextAsset('livebal/cont/randdrop/count/max', "random folders of $$MAX$$ max") : getTextAsset('livebal/cont/randdrop/count/normal', "random folders"));
			  value = fmtValue.replaceAll('$$VALUE$$', value).replaceAll('$$MAX$$', subvalue);
			  count = fmtCount.replaceAll('$$MAX$$', countvalue);
			  htmlAltBal += getBalRow(name, value, count);
		  }
		  else
		  {
			  if (mode == C_CONM_PER)
			  {
				  bal += (bal * (value / 100.0));
				  value += "%";
			  }
			  else 
			  {
				  bal += value;
				  if (payoutActive)
					value += "&nbsp;&nbsp;"; // space for dagger (not needed with %)
			  }
			  htmlBal += getBalRow(name, value, (DebugBalance ? bal : false));
		  }	 
	   }
	}
	htmlBal += getBalRow(getTextAsset('livebal/cont/fee', "Est. Fee"), "-" + fee + (payoutActive ? "&nbsp;&nbsp;" : ""));
	if (payoutActive)
		bal += " <sup>&Dagger;</sup>";
	htmlBal += getBalRow(getTextAsset('livebal/cont/total', "Total"), bal, Brand_Unit);
	htmlBal += "</table>";
	if (htmlAltBal != "")
	{
		htmlBal += "<br/><span style='line-height:90%;font-size:90%;font-weight:600;'>"+getTextAsset('livebal/cont/additionally', "Additionally:")+"</span><br/><table id='livebalextra'>";
		htmlBal += htmlAltBal;
		htmlBal += getBalRow("&nbsp;", "&nbsp;", "&nbsp;"); // place holder for last row balance
		htmlBal += "</table>";
	}
	document.getElementById("liveaddr").innerHTML = "<div style='overflow: hidden;text-overflow: ellipsis;'>" + jsonObj.activeAddress + "</div>";
	document.getElementById("liveaddr").href = dcLink;
	document.getElementById("livebal").innerHTML = htmlBal;
	document.getElementById("liveqr").href = dcLink;
	document.getElementById("livedatadate").innerHTML = htmlUpdated;

	if (dfLiveData.lastAddress != jsonObj.activeAddress)
	{
	  node = document.getElementById("liveqr");
	  while (node.hasChildNodes()) 
		node.removeChild(node.lastChild);
	  
	  $(document.getElementById("liveqr")).qrcode({	"render": "div",
													"left": 0,
													"top": 0,
													"size": 100,
													"color": "#3a3",
													"text": jsonObj.activeAddress
													});
	 dfLiveData.lastAddress = jsonObj.activeAddress;
	}
	updateContributionData(jsonObj);
}

function htmlLPad(str, len)
{
	var strLen = str.length;
	if (strLen >= len)
		return str;
	len -= strLen;
	while (len--)
		str = "&nbsp;" + str;
	return str;
}

function commaNum(num)
{
	return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function updateTeamData(jsonObj)
{
	var htmlTeam = "<div id='liveteamex'>";
	var teamLink = "<a href='http://folding.extremeoverclocking.com/team_summary.php?s=&amp;t="+parseInt(jsonObj.id)+"'>";
	var name = ""+jsonObj.name;
	var rank24 = parseInt(jsonObj.rank24);
	var rank7 = parseInt(jsonObj.rank7);
	var points24 = parseInt(jsonObj.points24);
	var rank24c;
	var rank7c;
	var actUsers = parseInt(jsonObj.actusers);
	name = name.charAt(0).toUpperCase() + name.substr(1);
	if (rank24 > 0)
	{
		rank24 = "+" + rank24;
		rank24c = "green";
	}
	else if (rank24 == 0) rank24c = "yellow";
	else rank24c = "red";
	if (rank7 > 0)
	{
		rank7 = "+" + rank7;
		rank7c = "green";
	}
	else if (rank7 == 0) rank7c = "yellow";
	else rank7c = "red";
	rank24 = (rank24 == -9999 ? " ?" : htmlLPad(""+rank24, 4));
	rank7 = (rank7 == -9999 ? " ?" : htmlLPad(""+rank7, 4));
	points24 = (points24 == -9999 ? " ?" : commaNum(points24));
	actUsers = (actUsers == -9999 ? " ?" : commaNum(actUsers));
	htmlTeam += teamLink+name+" #"+parseInt(jsonObj.id)+"</a><br/>";
	htmlTeam += "<div style='color:#a0a0a0;font-size:60%;'>"; // Main Containter
	htmlTeam += "<div style='padding:0;'>"+getTextAsset('liveteam/active', "Active Members:")+"<span style='font-size:140%;color:white;'>&nbsp;"+actUsers+"</span>/"+commaNum(parseInt(jsonObj.users))+"</div>";
	htmlTeam += "<div style='padding:0;width:100%;display:inline-block;'>"; // Rank
	// Rank Left  float:left;
	htmlTeam += "<div style='display:inline-block;padding:0 0 5px 25px;vertical-align:middle;'>"+getTextAsset('liveteam/teamrank', "Team Rank:")+"<span style='font-size:140%;color:white;'>&nbsp;#"+commaNum(parseInt(jsonObj.rank))+"</span></div>";
	// Rank Right ;display:block;
	htmlTeam += "<div style='display:inline-block;padding:0px 8px;vertical-align:middle;font-size:90%;font-family:monospace;line-height:90%;'>"; // Rank Right container
	htmlTeam += "<span style='color:"+rank24c+";'>"+rank24+"</span>&nbsp;"+getTextAsset('liveteam/teamrank/day', "day")+"<br/><span style='color:"+rank7c+";'>"+rank7+"</span>&nbsp;"+getTextAsset('liveteam/teamrank/week', "week");
	htmlTeam += "</div>"; // // Rank Right container
	htmlTeam += "</div>"; // Rank
	htmlTeam += "<div style='padding:0 0 5px 25px;'>"+getTextAsset('liveteam/wus', "Work Units:")+"<span style='font-size:140%;color:white;'>&nbsp;"+commaNum(parseInt(jsonObj.wu))+"</span></div>";
	htmlTeam += "<div style='padding:0;'>"+getTextAsset('liveteam/points', "Points:")+"<span style='font-size:140%;color:white;'>&nbsp;" + commaNum(parseInt(jsonObj.points)) + "</span>";
	htmlTeam += "&nbsp;<span><span style='color:green;'>"+points24+"</span>&nbsp;"+getTextAsset('liveteam/points/day', "day")+"</div>";
	htmlTeam += "</div>"; // Main Containter
	htmlTeam += "</div>"; // liveteamex 
	document.getElementById("extratitle").innerHTML = htmlTeam;
}

function getContributionBlock(ctitle, cmode, cvalue, cstyle, clink, ctext, cimage)
{
	var html;
	if (cstyle != "")
		html = "<div class='" + cstyle + "'>";
	else
		html = "<div>";

	if (XHasFlag(cmode, CONT_AD_FLAG_SHOW_TITLE) && ctitle != "")
	{
		html += "<div id='cont_title'>";
		if (XHasFlag(cmode, CONT_AD_FLAG_LINK_TITLE))
			html += "<a href='" + clink + "'>";
		html += ctitle;
		if (XHasFlag(cmode, CONT_AD_FLAG_LINK_TITLE))
			html += "</a>";
		html += "</div>";
	}

	if (XHasFlag(cmode, CONT_AD_FLAG_SHOW_VALUE))
		html += "<div id='cont_value'>" + cvalue + "</div>";

	if (XHasFlag(cmode, CONT_AD_FLAG_SHOW_IMAGE))
	{
		html += "<div id='cont_img'>";
		if (XHasFlag(cmode, CONT_AD_FLAG_LINK_IMAGE))
			html += "<a href='" + clink + "'>";
		html += "<img src='" + cimage + "' alt='" + ctitle + "' />";
		if (XHasFlag(cmode, CONT_AD_FLAG_LINK_IMAGE))
			html += "</a>";
		html += "</div>";
	}

	if (XHasFlag(cmode, CONT_AD_FLAG_SHOW_TEXT) && ctext != "")
	{
		html += "<div id='cont_txt'>";

		if (XHasFlag(cmode, CONT_AD_FLAG_LINK_TEXT))
			html += "<a href='" + clink + "'>" + ctext + "</a>";
		else 
			html += "<span>" + ctext + "</span>";
		html += "</div>";
	}
	
	return html + "</div>";
}

function updateContributionData(jsonObj)
{
	var html = "<ul class=\"contad\" >";
	for (var i = 0;i < jsonObj.contributions.length;i++)
	{
	  var c = jsonObj.contributions[i];
	  var mode = parseInt(c.mode);
	  var flags = parseInt(c.flags);
	  var adFlags = parseInt(c.adFlags);
	  if (XHasFlag(adFlags, CONT_AD_FLAG_SHOW_ALWAYS) || (mode != C_CONM_NONE && !XHasFlag(flags, C_CONF_DISABLED) && adFlags != CONT_AD_FLAG_NONE))
	  {
		  var name = atob(c.name);
		  var adTitle = atob(c.adTitle);
		  var adStyle = atob(c.adStyle);
		  var adLink = atob(c.adLink);
		  var adText = atob(c.adText);
		  var adImage = atob(c.adImage);
		  var value = parseFloat(c.value);
		  var subvalue = parseFloat(c.subvalue);
		  var countvalue = parseInt(c.countvalue);
		  var flags = parseInt(c.flags);
		  
		  if (name == "") name = getTextAsset('contad/defname', "Contribution")+" "+parseInt(c.number);

		  var txtAsset = 'contad/';
		  switch (mode)
		  {
			case C_CONM_NONE: txtAsset += 'none'; break;
			case C_CONM_PER: txtAsset += 'perc'; break;
			case C_CONM_EACH: txtAsset += 'each'; break;
			case C_CONM_PROPORTION: txtAsset += 'prop'; break;
			case C_CONM_RANDDROP: txtAsset += 'randdrop'; break;
			case C_CONM_FLAT: txtAsset += 'flat'; break;
			case C_CONM_ALL: txtAsset += 'all'; break;
			default: txtAsset += 'default'; break;
		  }
		  var txtAssetMax = '';
		  if (XHasFlag(flags, C_CONF_MAXTOTAL))
		  {
			if (XHasFlag(flags, C_CONF_MAXCOUNT))
				txtAsset += '/tot+cnt'
			else
				txtAsset += '/tot'
		  }
		  else if (XHasFlag(flags, C_CONF_MAXCOUNT))
			txtAsset += '/cnt'

			value = getTextAsset(txtAsset, "$$VALUE$$").replaceAll('$$VALUE$$', value).replaceAll('$$VALUE_MAX$$', subvalue).replaceAll('$$COUNT_MAX$$', countvalue).replaceAll('$$UNIT$$', Brand_Unit);
			html += "<li>"+getContributionBlock(adTitle, adFlags, value, adStyle, adLink, adText, adImage)+"</li>";
		}
	}
	html += "</ul>";
	document.getElementById("contributions").innerHTML = html;
}

