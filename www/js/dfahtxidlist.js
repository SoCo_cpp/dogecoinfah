/*
	dfahtxidlist.js
	Data poller from binary files prepared by DogecoinFah

*/
var txidList = {
					initting: false,
					ready: false,
					count: 0,
					list: []	
				};

var txlDelemiter = [0x0D, 0x0A];
var txlDelemiterLen = 2;
				
function txidBaFindDel(ba, start, len, debug = false)
{
	var match = 0;
	if (debug)
		console.log('start '+start+', len '+len+', delemiterLen '+delemiterLen);
	while (start < len)
	{
		if (debug)
			console.log('s '+start+', m '+match+', '+ba[start]+'/'+txlDelemiter[match]+' ('+(ba[start]>=32 ? String.fromCharCode(ba[start]) : '_')+')');
		
		if (ba[start++] == txlDelemiter[match])
		{
			match++;
			if (match >= txlDelemiterLen)
				return start - txlDelemiterLen;
		}
		else
		{
			 if (match > 1)
				start -= match;
			match = 0;
		}
	}
	return -1;
}
							
function txidListInit(response = null)
{
	if (txidList.ready || txidList.initting)
	{
		console.log('txidListInit already ready/initting');
		return false;
	}
	txidList.initting = true;
	var outterResponse = {
			onSuccess: function(resp)
						{
							txidList.initting = false;
							txidList.ready = true;
							if (isObject(response) && response.hasOwnProperty('onSuccess'))
								response.onSuccess(response);
						},
			onFail: function(resp)
						{
							txidList.initting = false;
							txidList.ready = false;
							console.log('txidListInit load onFail');
							if (isObject(response) && response.hasOwnProperty('onFail'))
								response.onFail(response);
						}
		};
	var fileName = mfFileName(mfAsset.pubDatTxids);
	if (!loadFile(fileName, txidListHandle, outterResponse /*onDoneArg*/, true /*isBinary*/))
	{
		console.log('txidListInit loadFile failed');
		txidList.initting = false;
		return false;
	}
	return true;	
}

function txidListHandle(status, data, response = null)
{
	var ret;
	if (status != '200')
		ret = 'http request failed with status: '+status;
	else
		ret = txidListParse(data)
	if (ret !== true)
	{
		console.log('txidListParse failed: '+ret);
		if (isObject(response) && response.hasOwnProperty('onFail'))
			response.onFail(response);
	}
	else
	{
		if (isObject(response) && response.hasOwnProperty('onSuccess'))
			response.onSuccess(response);
	}	
}

function txidListParse(dataIn)
{
	if (!dataIn)
		return 'bad data';
	var data = new Uint8Array(dataIn);
	var dataLen = data.length;
	
	var p = txidBaFindDel(data, 0, dataLen);
	if (p == -1)
		return 'header del not found';
	var str = String.fromCharCode.apply(null, data.slice(0, p));
	if (!str.startsWith('C-'))
		return 'header start not valid';
	str = str.substr(2);
	var count = parseInt(str);
	if (str.length != count.toString().length)
		return 'header too long: '+str.length+'/'+count.toString().length+', p '+p;
	p += txlDelemiterLen;
	txidList.list = [];
	var binTxid;
	var strTxid;
	var isTxt;
	for (var t = 0;t < count;t++)
	{
		if ((p+32) >= dataLen)
			return 'data len empty but count not. '+t+'/'+count+', '+p+'/'+dataLen;
		binTxid = data.slice(p, (p+32));
		if (binTxid.length != 32)
			return 'validate binTx len failed. '+binTxid.length+'/32, t '+t;
		strTxid = false;
		if (binTxid[0] == 0x5B /*[*/)
		{
			for (var b = 21;b >= 0;b--)
				if (binTxid[b] != 0)
				{
					if (binTxid[b] == 0x5D /*]*/)
						strTxid = String.fromCharCode.apply(null, binTxid.slice(0,(b+1)));
					break; // done either way on first non-zero from end
				}
		}
		if (strTxid === false)
		{
			strTxid = '';
			for (var b = 0;b < 32;b++)
			{
				if (binTxid[b] <= 0xF)
					strTxid += '0';
				strTxid += binTxid[b].toString(16);
			}
		}
		txidList.list[t] = strTxid;
		p += 32;		
	}
	txidList.count = txidList.list.length;
	return true;
}

function txidListFind(idx)
{
	if (!txidList.ready)
	{
		console.log('txidListFind not ready');
		return false;
	}
	if (idx < 0 || isNaN(idx))
	{
		console.log('txidListFind idx invalid: '+idx);
		return false;
	}
	if (idx >= txidList.count)
		return 0;
	return txidList.list[idx];		
}
