<?php
//-------------------------------------------------------------
/*
*	AutomationRound.php
*
* This page shouldn't be accessible to the web.
* This script should be executed every couple of minutes
* by a cron job.
* 
* This script will check the state machine for active rounds.
* 
*/
//-------------------------------------------------------------
define('CFG_AUTOMATION_ROUND_MUTEX', 'auto_round_mutex');
define('AUTOMATION_STALE_MUTEX_AGE', 30.0); // minutes
//-------------------------------------------------------------
require('./include/Init.php');
//---------------------------
//XLogDebug("AutomationRound.php activated");
//---------------------------
class AutomationRound
{
	var $Automation = false;
	var $Config = false;
	var $Monitor = false;
	var $activeRounds = false;
	var $noactionRounds = true;
	var $automationWasActive = false;
	//---------------------------
	function Init()
	{
		//------------------
		$this->Config = new Config() or die("Create object failed");
		$this->Automation = new Automation() or die("Create object failed");
		$this->Monitor = new Monitor() or die("Create object failed");
		//------------------
		return true;
	}
	//---------------------------
	function TryLock()
	{
		//------------------
		if ($this->Config === false)
		{
			XLogError("AutomationRound::TryLock validate Config object failed");
			return false;
		}
		//------------------
		$MutexTime = $this->Config->Get(CFG_AUTOMATION_ROUND_MUTEX);
		//------------------
		if ($MutexTime !== false && $MutexTime !== "")
		{
			//------------------
			$minDiff = XDateTimeDiff($MutexTime, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
			if ($minDiff === false)
			{
				XLogError("AutomationRound::TryLock XDateTimeDiff failed. Cannot verify MutexTime: '$MutexTime'");
				return false;
			}
			//------------------
			$mutexStale = ($minDiff < AUTOMATION_STALE_MUTEX_AGE ? false : true);
			//------------------
			$minDiff = floor($minDiff);
			if (!$this->Monitor->RoundLocked('AutomationRound TryLock' /*source*/, CFG_AUTOMATION_ROUND_MUTEX /*cfgLockName*/, ($minDiff * 60) /*elapsedSec*/, (AUTOMATION_STALE_MUTEX_AGE * 60) /*elapsedMaxSec*/, $mutexStale /*isIgnored*/))
			{
				XLogError("AutomationRound::TryLock Monitor RoundLocked failed");
				return false;
			}
			//------------------
			if ($mutexStale)
				XLogNotify("AutomationRound::TryLock ignoring stale mutex '".CFG_AUTOMATION_ROUND_MUTEX."' that is $minDiff minutes old.");
			else
			{
				XLogNotify("AutomationRound::TryLock Mutex is still locked. Locked '".CFG_AUTOMATION_ROUND_MUTEX."' at '$MutexTime', $minDiff minutes ago.");
				return false;
			}
			//------------------
		}
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		$strNowUtc = $nowUtc->format(MYSQL_DATETIME_FORMAT);
		//------------------
		if (!$this->Config->Set(CFG_AUTOMATION_ROUND_MUTEX, $strNowUtc))
		{
			XLogError("AutomationRound::TryLock Config set mutex failed");
			return false;
		}
		//------------------
		$MutexTime = $this->Config->Get(CFG_AUTOMATION_ROUND_MUTEX);
		//------------------
		if ($MutexTime !== $strNowUtc)
		{
			XLogWarn("AutomationRound::TryLock verify set mutex failed. Lost the race? Current mutex '$MutexTime', but just set it to '$strNowUtc'");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function Unlock()
	{
		//------------------
		if ($this->Config === false)
		{
			XLogError("AutomationRound::Unlock validate Config object failed");
			return false;
		}
		//------------------
		if (!$this->Config->Set(CFG_AUTOMATION_ROUND_MUTEX, ""))
		{
			XLogError("AutomationRound::Unlock Config set mutex to blank failed");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function FindActive()
	{
		//------------------
		$rounds = new Rounds() or die("Create object failed");
		$this->activeRounds = $rounds->getActiveRounds();
		//------------------
		if ($this->activeRounds === false)
		{
			XLogError("AutomationRound::FindActive rounds failed to getActiveRounds");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function UpdateAutomation()
	{
		//------------------
		if (!$this->Automation->isRoundCheckActive() && $this->automationWasActive)
		{
			XLogWarn("AutomationRound::UpdateAutomation automation was active at start, but no longer is. Assuming user manually stopped it and not updating automation state.");
			return true;
		}
		//------------------
		$roundActive = (sizeof($this->activeRounds) == 0 ? false : true); 
		//------------------
		if (!$this->Automation->setAutomationStates($roundActive))
		{
			XLogError("AutomationRound::UpdateAutomation automation failed to setAutomationStates");
			return false;
		}
		//------------------
		if ($roundActive === true)
		{
			//------------------
			if ($this->noactionRounds == true && !$this->Automation->IncRoundCheckNoAction())
			{
				XLogError("AutomationRound::UpdateAutomation automation failed to IncRoundCheckNoAction");
				return false;
			}
			//------------------
			if ($this->noactionRounds == false && !$this->Automation->ClearRoundCheckNoAction())
			{
				XLogError("AutomationRound::UpdateAutomation automation failed to ClearRoundCheckNoAction");
				return false;
			}
			//------------------
		}
		//------------------
		if (!$this->Automation->UpdateLastRoundCheck())
		{
			XLogError("AutomationRound::UpdateAutomation automation failed to UpdateLastRoundCheck");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function ExecuteActive($round)
	{
		//------------------
		$lastAction = $round->currentAction(); 
		if ($lastAction === false)
		{
			XLogError("AutomationRound::ExecuteActive round $round->id currentAction (last) failed");
			return false;
		}
		//------------------
		if (!$this->Monitor->RoundStepStart($round))
		{
			XLogError("AutomationRound::ExecuteActive Monitor RoundStepStart failed");
			return false;
		}
		//------------------
		if (!$round->step())
		{
			XLogError("AutomationRound::ExecuteActive round $round->id failed to step");
			if (!$this->Monitor->RoundStepFail($round))
				XLogError("AutomationRound::ExecuteActive Monitor RoundStepFail failed");
			return false;
		}
		//------------------
		if (!$this->Monitor->RoundStepStop($round))
		{
			XLogError("AutomationRound::ExecuteActive Monitor RoundStepStop failed");
			return false;
		}
		//------------------
		$newAction = $round->currentAction();
		if ($newAction === false)
		{
			XLogError("AutomationRound::ExecuteActive round $round->id currentAction (new) failed");
			return false;
		}
		//------------------
		if ($lastAction != $newAction)
			$this->noactionRounds = false;
		else
		{
			$Rounds = new Rounds() or die("Create object failed");
			if ($Rounds->isMultiPassAction($lastAction))
				$this->noactionRounds = false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function StepActive()
	{
		//------------------
		if ($this->Config === false)
		{
			XLogError("AutomationRound::StepActive validate Config object failed");
			return false;
		}
		//------------------
		if (!$this->FindActive())
		{
			XLogError("AutomationRound::StepActive FindActive failed");
			return false;
		}
		//------------------
		if (sizeof($this->activeRounds) == 0) // none alive
		{
			//------------------
			XLogNotify("AutomationRound::StepActive no active rounds");
			//------------------
			if (!$this->Config->Set(CFG_ROUND_OLDEST_ACTIVE, 0))
			{
				XLogError("AutomationRound::StepActive Config Set oldest active (none) failed");
				return false;
			}
			//------------------
		}
		else
		{
			//------------------
			if (!$this->Config->Set(CFG_ROUND_OLDEST_ACTIVE, $this->activeRounds[0]->id))
			{
				XLogError("AutomationRound::StepActive Config Set oldest active failed");
				return false;
			}
			//------------------
			foreach ($this->activeRounds as $round)
				if (!$this->ExecuteActive($round))
					XLogError("AutomationRound::StepActive ExecuteActive round $round->id failed");
			//------------------
		}
		//------------------
		if (!$this->UpdateAutomation())
		{
			XLogError("AutomationRound::StepActive UpdateAutomation failed");
			return false;
		}
		//------------------
		return true;
	}		
	//---------------------------
	function Main()
	{
		//------------------
		if (!$this->Init())
		{
			XLogError("AutomationRound::Main init failed");
			return false;
		}
		//------------------
		$Monitor = new Monitor() or die('Create object failed');
		//---------------------------
		if (!$Monitor->checkErrorLog('Automation Round'))
		{
			XLogError("AutomationRound::Main Monitor checkErrorLog failed");
			return false;
		}
		//---------------------------
		$eventList = $Monitor->RoundEventsBlocking(true /*ridx auto*/, true /*alwaysIncludeRoundZero*/);
		if ($eventList === false)
		{
			XLogError("AutomationRound::Main Monitor RoundEventsBlocking failed");
			return false;
		}
		//---------------------------
		if (sizeof($eventList) != 0)
		{
			XLogWarn("AutomationRound::Main Monitor events blocking");
			$event = $eventList[0];
			$resolved = ($event->isResolved() ? "Yes" : "No");
			$flags = $event->flags;
			$type = $Monitor->typeName($event->type);
			$issue = $Monitor->issueName($event->issue);
			XLogNotify("AutomationRound::Main First Monitor event blocking: id $event->id, round $event->ridx, type $type, issue $issue, resolved $resolved, flags $flags");
			return true; // don't fail
		}
		//---------------------------
		if (!$this->TryLock())
		{
			XLogWarn("AutomationRound::Main TryLock failed");
			return false;
		}
		//------------------
		$this->automationWasActive = $this->Automation->isRoundCheckActive();
		//------------------
		$retval = $this->StepActive();
		//------------------
		if (!$this->Unlock())
		{
			XLogError("AutomationRound::Main Unlock failed");
			return false;
		}
		//------------------
		return $retval;
	}
	//---------------------------
} // class AutomationRound
//---------------------------
$au = new AutomationRound() or die("Create object failed");
if (!$au->Main())
	echo "failed";
else
	echo "ok";
//---------------------------
//XLogDebug("AutomationRound.php finished");
//---------------------------
?>
