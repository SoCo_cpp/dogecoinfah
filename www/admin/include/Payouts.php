<?php
/*
 *	www/include/Payouts.php
 * 
 * 
* 
*/
//---------------
class Payout
{
	var $id = -1;
	var $dtcreated = false;
	var $dtPaid = false;
	var $dtStored = false;
	var $storePayout = false;
	var $roundIdx = "";
	var $workerIdx = 0;
	var $address = 0;
	var $statPay = 0.0;
	var $storePay = 0.0;
	var $pay = 0.0;
	var $txid = false;
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id   = $row[DB_PAYOUT_ID];
		$this->dtcreated = (isset($row[DB_PAYOUT_DATE_CREATED]) ? $row[DB_PAYOUT_DATE_CREATED] : false);
		$this->dtPaid = (isset($row[DB_PAYOUT_DATE_PAID]) ? $row[DB_PAYOUT_DATE_PAID] : false);
		$this->dtStored = (isset($row[DB_PAYOUT_DATE_STORED]) ? $row[DB_PAYOUT_DATE_STORED] : false);
		$this->storePayout = (isset($row[DB_PAYOUT_STORE_PAYOUT]) &&  $row[DB_PAYOUT_STORE_PAYOUT] !== null ? $row[DB_PAYOUT_STORE_PAYOUT] : false);
		$this->roundIdx = $row[DB_PAYOUT_ROUND];
		$this->workerIdx = $row[DB_PAYOUT_WORKER];
		$this->address = $row[DB_PAYOUT_ADDRESS];
		$this->statPay = $row[DB_PAYOUT_STAT_PAY]; 
		$this->storePay = (isset($row[DB_PAYOUT_STORE_PAY]) ? $row[DB_PAYOUT_STORE_PAY] : 0.0); 
		$this->pay = (isset($row[DB_PAYOUT_PAY]) ? $row[DB_PAYOUT_PAY] : 0.0);
		$this->txid = (isset($row[DB_PAYOUT_TXID]) ? $row[DB_PAYOUT_TXID] : "");  
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbPayoutFields;
		//------------------
		$this->id 	 = -1;
		$this->dtcreated	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_DATE_CREATED);
		$this->dtPaid	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_DATE_PAID);
		$this->dtStored	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_DATE_STORED);
		$this->storePayout	 		 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_STORE_PAYOUT);
		$this->roundIdx	 		 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_ROUND);
		$this->workerIdx	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_WORKER);
		$this->address	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_ADDRESS);
		$this->statPay	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_STAT_PAY);
		$this->storePay	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_STORE_PAY);
		$this->pay	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_PAY);
		$this->txid	 = $dbPayoutFields->GetMaxSize(DB_PAYOUT_TXID);
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbPayoutFields;
		//---------------------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_DATE_CREATED, $this->dtcreated);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_DATE_PAID, XFalse2Null($this->dtPaid));
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_DATE_STORED, XFalse2Null($this->dtStored));
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_STORE_PAYOUT, XFalse2Null($this->storePayout));
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_ROUND, $this->roundIdx);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_WORKER, $this->workerIdx);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_ADDRESS, $this->address);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_STAT_PAY, $this->statPay);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_STORE_PAY, $this->storePay);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_PAY, $this->pay);
		$dbPayoutFields->SetValuePrepared(DB_PAYOUT_TXID, $this->txid);
		//---------------------------------
		$sql = $dbPayoutFields->scriptUpdate(DB_PAYOUT_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Payout::Update db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbPayoutFields->BindValues($dbs))
		{
			XLogError("Payout::Update BindValues failed. sql: $sql");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Payout::Update db prepared statement execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function addUpdateStatPay($addValue)
	{
		global $db, $dbPayoutFields;
		//---------------------------------
		$this->statPay = bcadd($this->statPay, $addValue, 8);
		//---------------------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_STAT_PAY, $this->statPay);
		$sql = $dbPayoutFields->scriptUpdate(DB_PAYOUT_ID."=".$this->id);
		if (!$db->Execute($sql))
		{
			XLogError("Payout::addUpdateStatPay - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
} // class Payout
//---------------
class Payouts
{
	//------------------
	var $payouts = array();
	var $isLoaded = false;
	//------------------
	function Install()
	{
		global $db, $dbPayoutFields;
		//------------------------------------
		$sql = $dbPayoutFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbPayoutFields;
		//------------------------------------
		$sql = $dbPayoutFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbPayoutFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // Added DB_PAYOUT_DATE_STORED, DB_PAYOUT_STORE_PAYOUT, DB_PAYOUT_STAT_PAY, DB_PAYOUT_STORE_PAY
				//---------------
				$oldFieldsArray = array();
				$fieldsArray = $dbPayoutFields->GetNameArray();
				for ($i=0;$i < sizeof($fieldsArray);$i++)
					if ($fieldsArray[$i] != DB_PAYOUT_DATE_STORED && $fieldsArray[$i] != DB_PAYOUT_STORE_PAYOUT && $fieldsArray[$i] != DB_PAYOUT_STAT_PAY && $fieldsArray[$i] != DB_PAYOUT_STORE_PAY)
						$oldFieldsArray[] = $fieldsArray[$i];
				//---------------
				$oldFields = implode(",", $oldFieldsArray);
				$newFields = $oldFields.",".DB_PAYOUT_DATE_STORED.",".DB_PAYOUT_STORE_PAYOUT.",".DB_PAYOUT_STAT_PAY.",".DB_PAYOUT_STORE_PAY;
				$sql = "INSERT INTO $dbPayoutFields->tableName ($newFields) SELECT $oldFields, '' AS ".DB_PAYOUT_DATE_STORED.", NULL AS ".DB_PAYOUT_STORE_PAYOUT.", ".DB_PAYOUT_PAY." AS ".DB_PAYOUT_STAT_PAY.", 0 AS ".DB_PAYOUT_STORE_PAY." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Payouts::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 2: // just alter DB_PAYOUT_WORKER to be signed (instead of unsigned), fall through to simple copy
			case 3: // 3->4 changed  DB_PAYOUT_DATE_CREATED/DB_PAYOUT_DATE_PAID/DB_PAYOUT_DATE_STORED 
				//---------------
				$dbPayoutFields->SetValues();
				$dbPayoutFields->ClearValue(DB_PAYOUT_DATE_CREATED);
				$dbPayoutFields->ClearValue(DB_PAYOUT_DATE_PAID);
				$dbPayoutFields->ClearValue(DB_PAYOUT_DATE_STORED);
				//---------------
				$fieldNames = $dbPayoutFields->GetNameListString(true/*SkipNotSet*/);
				//---------------
				$sql = "INSERT INTO $dbPayoutFields->tableName ($fieldNames,".DB_PAYOUT_DATE_CREATED.",".DB_PAYOUT_DATE_PAID.",".DB_PAYOUT_DATE_STORED.") ";
				$sql .= "SELECT $fieldNames,UNIX_TIMESTAMP(".DB_PAYOUT_DATE_CREATED.") AS ".DB_PAYOUT_DATE_CREATED;
				$sql .= ",CASE WHEN ".DB_PAYOUT_DATE_PAID." IS NULL THEN NULL WHEN ".DB_PAYOUT_DATE_PAID."='' THEN NULL ELSE UNIX_TIMESTAMP(".DB_PAYOUT_DATE_PAID.") END AS ".DB_PAYOUT_DATE_PAID;
				$sql .= ",CASE WHEN ".DB_PAYOUT_DATE_STORED." IS NULL THEN NULL WHEN ".DB_PAYOUT_DATE_STORED."='' THEN NULL ELSE UNIX_TIMESTAMP(".DB_PAYOUT_DATE_STORED.") END AS ".DB_PAYOUT_DATE_STORED;
				$sql .= " FROM  $oldTableName";
				//---------------
				XLogDebug("Payouts::Import sql: $sql");
				if (!$db->Execute($sql))
				{
					XLogError("Payouts::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbPayoutFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbPayoutFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Payouts::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Payouts::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msizePayout = new Payout();
		$msizePayout->setMaxSizes();
		//------------------------------------
		return $msizePayout;		
	}
	//------------------
	function deletePayout($idx)
	{
		global $db, $dbPayoutFields;
		//---------------------------------
		$sql = $dbPayoutFields->scriptDelete(DB_PAYOUT_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::deletePayout - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		if ($this->loadPayouts() === false)
		{
			XLogError("Payouts::deletePayout - loadPayouts failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbPayoutFields;
		//---------------------------------
		$sql = $dbPayoutFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->payouts = array();
		//---------------------------------
		$this->isLoaded = true;
		//------------------
		return true;
	}
	//---------------------------------	
	function loadPayoutRaw($idx)
	{
		global $db, $dbPayoutFields;
		//------------------
		$dbPayoutFields->SetValues();
		//------------------
		$sql = $dbPayoutFields->scriptSelect(DB_PAYOUT_ID."=".$idx, false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::loadPayoutRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function loadPayout($idx)
	{
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Payouts::loadPayout - validate index failed");
			return false;
		}
		//------------------
		$qr = $this->loadPayoutRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Payouts::loadPayout - loadPayoutRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Payouts::loadPayout - index $idx not found.");
			return false;
		}
		//------------------
		return new Payout($s);
	}
	//------------------
	function getPayoutRoundWorker($ridx, $widx)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($ridx) || !is_numeric($widx))
		{
			XLogError("Payouts::getPayoutRoundWorker - validate parameters failed");
			return false;
		}
		//------------------
		$dbPayoutFields->SetValues();
		$where = DB_STATS_ROUND."=$ridx AND ".DB_STATS_WORKER."=$widx";
		//------------------
		$sql = $dbPayoutFields->scriptSelect($where, false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::getPayoutRoundWorker - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Payouts::getPayoutRoundWorker - payout ridx $ridx, widx $widx not found.");
			return false;
		}
		//------------------
		return new Payout($s);
	}
	//------------------
	function getPayout($idx)
	{
		//---------------------------------
		if (!is_numeric($idx))
		{
			XLogError("Payouts::getPayout - validate index failed");
			return false;
		}
		//------------------
		if ($this->isLoaded)
			foreach ($this->payouts as $p)
				if ($p->id == $idx)
					return $p;
		//---------------------------------
		return $this->loadPayout($idx);
	}
	//------------------
	function loadPayoutsRaw($limit = false, $sort = false, $where = false)
	{
		global $db, $dbPayoutFields;
		//------------------
		if ($sort === false)
			$orderby = DB_PAYOUT_ID;
		else
			$orderby = $sort;
		//------------------
		$dbPayoutFields->SetValues();
		$sql = $dbPayoutFields->scriptSelect($where /*where*/, $orderby /*orderby*/, $limit /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::loadPayoutsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function loadPayouts($limit = false, $sort = false, $where = false)
	{
		$this->payouts = array();
		//------------------
		$qr = $this->loadPayoutsRaw($limit, $sort, $where);
		//------------------
		if ($qr === false)
		{
			XLogError("Payouts::loadPayouts - loadPayoutsRaw failed");
			return false;
		}
		//------------------
		while ($p = $qr->GetRowArray())
			$this->payouts[] = new Payout($p);
		//------------------
		$this->isLoaded = true;
		//------------------
		return $this->payouts;
	}
	//------------------
	function loadStoredPayouts($onlyPaid = false, $onlyUnpaid = false, $limit = false, $sort = false, $where = false)
	{
		//------------------
		if ($onlyPaid !== false && $onlyUnpaid !== false)
		{
			XLogError("Payouts::loadStoredPayouts - cannot choose both onlyPaid and onlyUnpaid");
			return false;
		}
		//------------------
		if ($limit !== false && !is_numeric($limit))
		{
			XLogError("Payouts::loadStoredPayouts - validate limit parameter failed");
			return false;
		}
		//------------------
		if ($sort === false)
			$sort = DB_PAYOUT_WORKER.",".DB_PAYOUT_DATE_CREATED;
		//------------------
		$where = ($where === false ? "" : "$where AND ");
		$where .= "(".DB_PAYOUT_DATE_STORED." IS NOT NULL AND ".DB_PAYOUT_DATE_STORED."!='')";
		//------------------
		if ($onlyPaid !== false)
			$where .= " AND (".DB_PAYOUT_DATE_PAID." IS NOT NULL AND ".DB_PAYOUT_DATE_PAID."!='')";
		//------------------
		if ($onlyUnpaid !== false)
			$where .= " AND (".DB_PAYOUT_DATE_PAID." IS NULL OR ".DB_PAYOUT_DATE_PAID."='')";
		//------------------
		$rPayouts = $this->loadPayouts($limit, $sort, $where);
		if ($rPayouts === false)
		{
			XLogError("Payouts::loadStoredPayouts - loadPayouts failed");
			return false;
		}
		//------------------
		return $rPayouts;
	}
	//------------------
	function findRoundPayouts($roundIdx, $orderBy = false, $includePaid = true, $limit = false, $workerIdx = false, $includeSpecialWorkers = false, $payoutIDIndexed = false, $workerIDIndexed = false)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Payouts::findRoundPayouts - validate roundIdx failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		if ($limit !== false && !is_numeric($limit))
		{
			XLogError("Payouts::findRoundPayouts - validate limit failed: ".XVarDump($limit));
			return false;
		}
		//------------------
		if ($workerIdx !== false && !is_numeric($workerIdx))
		{
			XLogError("Payouts::findRoundPayouts - validate workerIdx failed: ".XVarDump($workerIdx));
			return false;
		}
		//------------------
		if ($orderBy === false)
			$orderBy = DB_PAYOUT_ID;
		//------------------
		$where = DB_PAYOUT_ROUND."=$roundIdx";
		if (!$includePaid)
			$where .= " AND (".DB_PAYOUT_DATE_PAID." IS NULL OR ".DB_PAYOUT_DATE_PAID."='')";
		if ($workerIdx !== false)
			$where .= " AND ".DB_PAYOUT_WORKER."=$workerIdx"; 
		else if (!$includeSpecialWorkers)
			$where .= " AND ".DB_PAYOUT_WORKER.">=0"; 
		//------------------
		$dbPayoutFields->SetValues();
		$sql = $dbPayoutFields->scriptSelect($where, $orderBy, $limit);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findRoundPayouts db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$payouts = array();
		//------------------
		if ($payoutIDIndexed)
		{
			while ($p = $qr->GetRowArray())
				$payouts[$p[DB_PAYOUT_ID]] = new Payout($p);
		}
		else if ($workerIDIndexed)
		{
			while ($p = $qr->GetRowArray())
				$payouts[$p[DB_PAYOUT_WORKER]] = new Payout($p);
		}
		else
		{
			while ($p = $qr->GetRowArray())
				$payouts[] = new Payout($p);
		}
		//------------------
		return $payouts;
	}
	//------------------
	function findRoundWorkersDuplicates($roundIdx)
	{
		global $db;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Payouts::findRoundWorkersDuplicates - validate roundIdx failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		$idField = DB_PAYOUT_ID;
		$workerField = DB_PAYOUT_WORKER;
		$roundField = DB_PAYOUT_ROUND;
		$tableName = DB_PAYOUT;
		//------------------
		$sql =  "SELECT a.$workerField, a.$idField FROM $tableName a ";
		$sql .= "JOIN (SELECT $workerField, COUNT(*) AS cnt FROM $tableName WHERE $roundField=$roundIdx AND $workerField>0 GROUP BY $workerField) b ";
		$sql .= "ON a.$workerField=b.$workerField WHERE a.$roundField=$roundIdx AND b.cnt > 1 ORDER BY a.$workerField, a.$idField";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findRoundWorkersDuplicates - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ( ($r = $qr->GetRow()) )
			$rData[(int)$r[0]/*workerID*/] = (int)$r[1]/*payoutID*/;
		//------------------
		return $rData;
	}
	//------------------
	function findRoundPayStoredSummary($roundIdx)
	{
		global $db;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Payouts::findRoundPayStoredSummary validate roundIdx is_numeric failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		$sql = "SELECT ".DB_PAYOUT_ID.",".DB_PAYOUT_PAY.",".DB_PAYOUT_STAT_PAY." FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ROUND."=$roundIdx AND ".DB_PAYOUT_WORKER." >= 0 AND ".DB_PAYOUT_DATE_STORED." IS NOT NULL AND ".DB_PAYOUT_DATE_STORED." != '' ORDER BY ".DB_PAYOUT_ID;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findRoundPayStoredSummary - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$totalStoredToPay = "0";
		$totalStored = "0";
		$countNoPay = 0;
		$countStoredToPay = 0;
		$anomalyCount = 0;
		//------------------
		while ( ($r = $qr->GetRow()) )
		{
			$pay = (isset($r[1]) && is_numeric($r[1]) ? ''.$r[1] : '0');
			$reward = (isset($r[2]) && is_numeric($r[2]) ? ''.$r[2] : '0');
			if ($pay != '0')
			{
				if (bccomp($reward, $pay, 8) > 0)
				{
					$anomalyCount++;
					XLogWarn("Payouts::findRoundPayStoredSummary anomaly detected. Round $roundIdx payout ".$r[0]." has a partially paid stored reward. paid/reward $pay / $reward . (Counted as not paid, but included in both totals)");
					$totalStoredToPay = bcadd($totalStoredToPay, $pay, 8);
					$totalStored = bcadd($totalStored, bcsub($reward, $pay, 8), 8);
					$countNoPay++;					
				}
				else
				{
					$countStoredToPay++;
					$totalStoredToPay = bcadd($totalStoredToPay, $pay, 8);
				}
			}
			else
			{
				$countNoPay++;					
				$totalStored = bcadd($totalStored,$reward, 8);

			}
		} // while
		//------------------
		return array('storedToPay' => $totalStoredToPay, 'storedNoPay' => $totalStored, 'countToPay' => $countStoredToPay, 'countNoPay' => $countNoPay, 'anomalyCount' => $anomalyCount);
	}
	//------------------
	function findRoundWorkerPayoutSummaryList($roundIdx)
	{
		global $db;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Payouts::findRoundWorkerPayoutSummaryList validate roundIdx is_numeric failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		// need fields in order, so don't use dbPayoutFields
		$sql = "SELECT ".DB_PAYOUT_ID.",".DB_PAYOUT_WORKER." FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ROUND."=$roundIdx AND ".DB_PAYOUT_WORKER." >= 0 ORDER BY ".DB_PAYOUT_ID;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findRoundWorkerPayoutSummaryList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ( ($r = $qr->GetRow()) )
			$rData[(int)$r[1]/*workerID*/] = (int)$r[0]/*payoutID*/;
		//------------------
		return $rData;
	}
	//------------------
	function findWorkerStoredPayoutSummaryList($workerIdx, $includePaid = false, $onlyPaid = false)
	{
		global $db;
		//------------------
		if (!is_numeric($workerIdx))
		{
			XLogError("Payouts::findWorkerStoredPayoutSummaryList validate workerIdx is_numeric failed: ".XVarDump($workerIdx));
			return false;
		}
		//------------------
		// need fields in order, so don't use dbPayoutFields
		$sql = "SELECT ".DB_PAYOUT_ID.",".DB_PAYOUT_STAT_PAY.",".DB_PAYOUT_DATE_STORED;
		if ($includePaid !== false)
			$sql .= ",".DB_PAYOUT_STORE_PAYOUT.",".DB_PAYOUT_DATE_PAID;
		//------------------
		$sql .= " FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_WORKER."=$workerIdx AND ".DB_PAYOUT_DATE_STORED." IS NOT NULL AND ".DB_PAYOUT_DATE_STORED." != ''";
		if ($includePaid === false)
			$sql .= " AND ".DB_PAYOUT_STORE_PAYOUT." IS NULL AND (".DB_PAYOUT_DATE_PAID." IS NULL OR ".DB_PAYOUT_DATE_PAID." = '')";
		else if ($onlyPaid !== false)
			$sql .= " AND ".DB_PAYOUT_STORE_PAYOUT." IS NOT NULL AND ".DB_PAYOUT_DATE_PAID." IS NOT NULL AND ".DB_PAYOUT_DATE_PAID." != ''";
		//------------------
		$sql .= " ORDER BY ".DB_PAYOUT_ID;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findWorkerStoredPayoutSummaryList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		if ($includePaid !== false)
		{
			while ( ($r = $qr->GetRow()) )
				$rData[] = array((int)$r[0]/*payoutID*/, "".$r[1]/*stat pay, ensure string*/, (is_null($r[2]) ? "" : $r[2]), ((is_null($r[3]) || !is_numeric($r[3])) ? false : (int)$r[3]), (is_null($r[4]) ? "" : $r[4]));
		}
		else
		{
			while ( ($r = $qr->GetRow()) )
				$rData[] = array((int)$r[0]/*payoutID*/, "".$r[1]/*stat pay, ensure string*/, (is_null($r[2]) ? "" : $r[2]));
		}
		//------------------
		return $rData;
	}
	//------------------
	function addPayout($roundIdx, $workerIdx, $address, $statPay, $pay = false, $storePay = false, $dtPaid = false, $txid = false, $returnID = false)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Payouts::addPayout validate roundIdx is_numeric failed for ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		if (!is_numeric($workerIdx))
		{
			XLogError("Payouts::addPayout validate workerIdx is_numeric failed for ".XVarDump($workerIdx));
			return false;
		}
		//------------------
		if (!is_numeric($statPay))
		{
			XLogError("Payouts::addPayout validate statPay is_numeric failed for ".XVarDump($statPay));
			return false;
		}
		//------------------
		if ($pay !== false && !is_numeric($pay))
		{
			XLogError("Payouts::addPayout validate pay is_numeric failed for ".XVarDump($pay));
			return false;
		}
		//------------------
		if ($storePay !== false && !is_numeric($storePay))
		{
			XLogError("Payouts::addPayout validate storePay is_numeric failed for ".XVarDump($storePay));
			return false;
		}
		//------------------
		if ($dtPaid !== false && !is_integer($dtPaid))
		{
			XLogError("Payouts::addPayout validate dtPaid is string failed for ".XVarDump($dtPaid));
			return false;
		}
		//------------------
		$tsNow = time();
		//------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_DATE_CREATED, $tsNow);
		$dbPayoutFields->SetValue(DB_PAYOUT_ROUND, $roundIdx);
		$dbPayoutFields->SetValue(DB_PAYOUT_WORKER, $workerIdx);
		$dbPayoutFields->SetValue(DB_PAYOUT_ADDRESS, $address);
		$dbPayoutFields->SetValue(DB_PAYOUT_STAT_PAY, $statPay);
		//------------------
		if ($pay !== false)
			$dbPayoutFields->SetValue(DB_PAYOUT_PAY, $pay);
		if ($storePay !== false)
			$dbPayoutFields->SetValue(DB_PAYOUT_STORE_PAY, $storePay);
		if ($dtPaid !== false)
			$dbPayoutFields->SetValue(DB_PAYOUT_DATE_PAID, $dtPaid);
		if ($txid !== false)
			$dbPayoutFields->SetValue(DB_PAYOUT_TXID, $txid);
		//------------------
		$sql = $dbPayoutFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::addPayout - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->payouts = array();
		$this->isLoaded = false; // list modified, set to reload
		//------------------
		if ($returnID !== false)
		{
			//------------------
			$where = DB_PAYOUT_DATE_CREATED."=$tsNow AND ".DB_PAYOUT_ROUND."=$roundIdx AND ".DB_PAYOUT_WORKER."=$workerIdx";
			//------------------
			$dbPayoutFields->ClearValues();
			$dbPayoutFields->SetValue(DB_PAYOUT_ID);
			//------------------
			$sql = $dbPayoutFields->scriptSelect($where, DB_PAYOUT_ID.' DESC' /*orderby*/, 1 /*limit*/);
			//------------------
			if (!($qr = $db->Query($sql)))
			{
				XLogError("Payouts::addPayout db Query failed.\nsql: $sql");
				return false;
			}
			//------------------
			$value = $qr->GetOneValue();
			//------------------
			if ($value === false)
			{
				XLogWarn("Payouts::addPayout query result GetOneValue failed, new payout not found sql: $sql");
				return false;
			}
			//------------------
			return (int)$value;
		}
		//------------------
		return true;
	}
	//------------------
	function getPayouts($limit = false, $sort = false, $where = false)
	{
		//---------------------------------
		if ($this->isLoaded)
			return $this->payouts;
		//---------------------------------
		return $this->loadPayouts($limit, $sort, $where);
	}
	//------------------
	function deleteAllRound($ridx)
	{
		global $db, $dbPayoutFields;
		//------------------
		$sql = $dbPayoutFields->scriptDelete(DB_PAYOUT_ROUND."=$ridx");
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Payouts::deleteAllRound - db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		return true;
	}
	//------------------
	function updateStorePayoutID($payoutID, $storePayoutID)
	{
		global $db, $dbPayoutFields;
		//------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_STORE_PAYOUT, $storePayoutID);
		//---------------------------------
		$sql = $dbPayoutFields->scriptUpdate(DB_PAYOUT_ID."=$payoutID");
		if (!$db->Execute($sql))
		{
			XLogError("Payout::updateStorePayoutID - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function updateStorePaidPayouts($storePayoutID, $dtPaid)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($storePayoutID) || !is_integer($dtPaid))
		{
			XLogError("Payouts::updateStorePaidPayouts validate parameters failed: storePayoutID ".XVarDump($storePayoutID).", int dtPaid ".XVarDump($dtPaid));
			return false;
		}
		//---------------------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_DATE_PAID, $dtPaid);
		//---------------------------------
		$sql = $dbPayoutFields->scriptUpdate(DB_PAYOUT_STORE_PAYOUT."=$storePayoutID");
		if (!$db->Execute($sql))
		{
			XLogError("Payout::updateStorePaidPayouts - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function unpayStorePaidPayouts($storePayoutID)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($storePayoutID))
		{
			XLogError("Payouts::unpayStorePaidPayouts validate storePayoutID is_numeric failed for: ".XVarDump($storePayoutID));
			return false;
		}
		//---------------------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_DATE_PAID, null);
		$dbPayoutFields->SetValue(DB_PAYOUT_STORE_PAYOUT, null);
		//---------------------------------
		$sql = $dbPayoutFields->scriptUpdate(DB_PAYOUT_STORE_PAYOUT."=$storePayoutID");
		if (!$db->Execute($sql))
		{
			XLogError("Payout::unpayStorePaidPayouts - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function getWorkerPayouts($workerIdx, $onlyPaid = false, $onlyUnpaid = false, $sort = false, $limit = false)
	{
		//---------------------------------
		if (!is_numeric($workerIdx))
		{
			XLogError("Payouts::getWorkerPayouts validate workerIdx is_numeric failed for: ".XVarDump($workerIdx));
			return false;
		}
		//---------------------------------
		if ($onlyPaid !== false && $onlyUnpaid !== false)
		{
			XLogError("Payouts::getWorkerPayouts can't have both onlyPaid and onlyUnpaid selected");
			return false;
		}
		//---------------------------------
		if ($sort === false)
			$sort = DB_PAYOUT_DATE_CREATED;
		//---------------------------------
		$where = DB_PAYOUT_WORKER."=$workerIdx";
		if ($onlyPaid !== false)
			$where .= " AND ".DB_PAYOUT_DATE_PAID." IS NOT NULL";
		if ($onlyUnpaid !== false)
			$where .= " AND ".DB_PAYOUT_DATE_PAID." IS NULL";
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		if ($this->loadPayouts($limit, $sort, $where) === false)
		{
			XLogError("Payouts::getWorkerPayouts loadPayouts failed");
			return false;
		}
		//---------------------------------
		return $this->payouts;
	}
	//------------------
	function findOldStoredPayouts($dtMaxStoreDate, $orderBy = false, $limit = false)
	{
		//------------------
		if ($dtMaxStoreDate === false || !is_integer($dtMaxStoreDate))
		{
			XLogError("Payouts::findOldStoredPayouts validate dtMaxStoreDate failed: ".XVarDump($dtMaxStoreDate));
			return false;
		}		
		//------------------
		if ($orderBy === false)
			$orderBy = DB_PAYOUT_ID;
		//------------------
		// ensure unpaid
		$where = DB_PAYOUT_DATE_PAID." IS NULL";
		// ensure storedPayout
		$where .= " AND ".DB_PAYOUT_DATE_STORED." IS NOT NULL";
		// ensure older than max store date
		$where .= " AND ".DB_PAYOUT_DATE_STORED."<=$dtMaxStoreDate";
		//------------------
		$payouts = $this->loadPayouts($limit, $orderBy, $where);
		if ($payouts === false)
		{
			XLogError("Payouts::findOldStoredPayouts - loadPayouts failed.");
			return false;
		}
		//------------------
		return $payouts;
	}
	//------------------
	function findPaidByMissingPayouts()
	{
		global $db, $dbPayoutFields;
		//------------------
		$sql = "SELECT a.* FROM ".DB_PAYOUT." a LEFT OUTER JOIN ".DB_PAYOUT." b ON a.".DB_PAYOUT_STORE_PAYOUT."=b.".DB_PAYOUT_ID." WHERE a.".DB_PAYOUT_STORE_PAYOUT." IS NOT NULL AND b.".DB_PAYOUT_ID." IS NULL";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findPaidByMissingPayouts - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$payoutList = array();
		while ($p = $qr->GetRowArray())
			$payoutList[] = new Payout($p);
		//------------------
		return $payoutList;
	}
	//------------------
	function getIdleWorkerStoredPayoutSummary($dtMinIdleDate)
	{
		global $db;
		//------------------
		if ($dtMinIdleDate === false || !is_integer($dtMinIdleDate))
		{
			XLogError("Payouts::getIdleWorkerStoredPayoutSummary validate dtMinIdleDate failed: ".XVarDump($dtMinIdleDate));
			return false;
		}		
		//------------------
		// make constants variables for SQL readability
		$payoutTable = DB_PAYOUT;
		$fpId = DB_PAYOUT_ID;
		$fpWidx = DB_PAYOUT_WORKER;
		$fpPaid = DB_PAYOUT_DATE_PAID;
		$fpStored = DB_PAYOUT_DATE_STORED;
		//------------------
		$statTable = DB_STATS;
		$fsId = DB_STATS_ID;
		$fsWidx = DB_STATS_WORKER;
		$fsRidx = DB_STATS_ROUND;
		$fsCreated = DB_STATS_DATE_CREATED;
		$fsWPts = DB_STATS_WEEK_POINTS;
		//------------------
		$sql = "SELECT *, js.mx AS lsDt FROM $payoutTable sp
				JOIN ( 
					SELECT $fsWidx, MAX($fsCreated) AS mx FROM $statTable ss
					WHERE $fsWPts IS NOT NULL AND $fsWPts!=0 
					GROUP BY $fsWidx  
				) js 
				ON js.$fsWidx = sp.$fpWidx 
				WHERE (sp.$fpPaid IS NULL OR sp.$fpPaid='') AND sp.$fpStored IS NOT NULL AND sp.$fpStored!=''
				AND js.mx<$dtMinIdleDate";
		//------------------
		//XLogDebug("Payouts::getIdleWorkerStoredPayoutSummary sql:\n$sql");
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::getIdleWorkerStoredPayoutSummary - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$summary = array();
		//------------------
		while ($p = $qr->GetRowArray())
			$summary[] = array('payout' => new Payout($p), 'dtLastWork' => $p['lsDt']);
		//------------------
		return $summary;
	}
	//------------------
	function getWorkerStoredBalance($workerIdx)
	{
		global $db;
		//------------------
		if (!is_numeric($workerIdx))
		{
			XLogError("Payouts::getWorkerStoredBalance validate workerIdx is_numeric failed for: ".XVarDump($workerIdx));
			return false;
		}
		//---------------------------------
		$sql = "SELECT SUM(".DB_PAYOUT_STAT_PAY.") FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_WORKER."=$workerIdx AND ".DB_PAYOUT_DATE_PAID." IS NULL";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::getWorkerStoredBalance - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(0 /*emptyValue*/);
		if ($value === false)
		{
			XLogError("Payouts::getWorkerStoredBalance - db GetOneValue failed. sql: $sql");
			return false;
		}
		//------------------
		return (float)$value;
	}
	//------------------
	function getPayoutWorkerIdx($payoutIdx)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($payoutIdx))
		{
			XLogError("Payouts::getPayoutWorkerIdx validate payoutIdx is_numeric failed for: ".XVarDump($payoutIdx));
			return false;
		}
		//---------------------------------
		$sql = "SELECT ".DB_PAYOUT_WORKER." FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ID."=$payoutIdx";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::getPayoutWorkerIdx - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(true /*emptyValue*/);
		if ($value === false)
		{
			XLogError("Payouts::getPayoutWorkerIdx - db GetOneValue.\nsql: $sql");
			return false;
		}
		//------------------
		if ($value === null)
			$value = true; // not found
		//------------------
		return ($value === true ? $value : (int)$value);
	}
	//------------------
	function getPayoutTxidList()
	{
		global $db;
		//------------------
		$sql = "SELECT DISTINCT(".DB_PAYOUT_TXID.") FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_TXID." IS NOT NULL AND ".DB_PAYOUT_TXID."!=''";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::getPayoutTxidList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$txidList = array();
		while ($r = $qr->GetRow())
			if (isset($r[0]))
				$txidList[] = $r[0];
		//------------------
		return $txidList;
	}
	//------------------
	function getRoundPayoutsWithTXID($ridx, $txid, $limit = false)
	{
		//------------------
		if (!is_numeric($ridx) || !is_string($txid) || strlen($txid) == 0 || ($limit !== false && !is_numeric($limit)))
		{
			XLogError("Payouts::getRoundPayoutsWithTXID validate parameters failed. ridx ".XVarDump($ridx).", txid ".XVarDump($txid).", limit ".XVarDump($limit));
			return false;
		}
		//------------------
		$where = DB_PAYOUT_ROUND."=$ridx AND ".DB_PAYOUT_TXID."='$txid'";
		//------------------
		$payoutList =  $this->loadPayouts($limit, DB_PAYOUT_ID, $where);
		if ($payoutList === false)
		{
			XLogError("Payouts::getRoundPayoutsWithTXID loadPayouts failed");
			return false;
		}
		//------------------
		return $payoutList;
	}
	//------------------
	function deleteRoundPayoutsWithTXID($ridx, $txid)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($ridx) || !is_string($txid) || strlen($txid) == 0)
		{
			XLogError("Payouts::deleteRoundPayoutsWithTXID validate parameters failed. ridx ".XVarDump($ridx).", txid ".XVarDump($txid));
			return false;
		}
		//------------------
		$where = DB_PAYOUT_ROUND."=$ridx AND ".DB_PAYOUT_TXID."='$txid'";
		//------------------
		$sql = $dbPayoutFields->scriptDelete($where);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Payout::deleteRoundPayoutsWithTXID db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function findWorkerOfStorePayout($storePayerPayoutID)
	{
		global $db, $dbPayoutFields;
		//------------------
		if (!is_numeric($storePayerPayoutID))
		{
			XLogError("Payouts::findWorkerOfStorePayout validate parameters failed");
			return false;
		}
		//------------------
		$dbPayoutFields->ClearValues();
		$dbPayoutFields->SetValue(DB_PAYOUT_WORKER, null);
		//---------------------------------
		$sql = $dbPayoutFields->scriptSelect(DB_PAYOUT_STORE_PAYOUT."=$storePayerPayoutID", DB_PAYOUT_ID.' DESC' /*orderBy*/, 1 /*limit*/);
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::findWorkerOfStorePayout - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$widx = $qr->GetOneValue(0 /*emptyValue*/);
		if ($widx === false || !is_numeric($widx))
		{
			XLogError("Payouts::findWorkerOfStorePayout GetOneValue failed");
			return false;
		}
		//------------------
		return $widx;
	}
	//------------------
	function backupTable($tableNameAppend = 'backup', $failExists = true, $clobber = true)
	{
		global $db;
		//------------------
		$bakTableName = 'bak_'.DB_PAYOUT.'_'.$tableNameAppend;
		//------------------
		if (!$db->duplicateTable(DB_PAYOUT, $bakTableName, $failExists, $clobber))
		{
			XLogError("Payouts::backupTable db duplicateTable failed: ".$db->errorString());
			return false;
		}
		//------------------
		return $bakTableName;
	}
	//------------------
} // class Payouts
//---------------
?>
