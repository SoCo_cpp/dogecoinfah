<?php
/*
 *	www/include/Stats.php
 * 
 * 
* 
*/
//---------------
class Stat
{
	var $id = -1;
	var $dtCreated = false;
	var $roundIdx = false;
	var $workerIdx = false;
	var $weekPoints = false;
	var $totPoints = false;
	var $wus = false; // work units
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id			= $row[DB_STATS_ID];
		$this->dtCreated 	= (isset($row[DB_STATS_DATE_CREATED]) ? $row[DB_STATS_DATE_CREATED] : false);
		$this->roundIdx 	= (isset($row[DB_STATS_ROUND]) ? $row[DB_STATS_ROUND] : false);
		$this->workerIdx 	= $row[DB_STATS_WORKER];
		$this->weekPoints	= (isset($row[DB_STATS_WEEK_POINTS]) ? $row[DB_STATS_WEEK_POINTS] : false);  
		$this->totPoints	= (isset($row[DB_STATS_TOTAL_POINTS]) ? $row[DB_STATS_TOTAL_POINTS] : false);  
		$this->wus			= (isset($row[DB_STATS_WUS]) ? $row[DB_STATS_WUS] : false);  
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbStatsFields;
		//------------------
		$this->id 	 = -1;
		$this->dtCreated	= $dbStatsFields->GetMaxSize(DB_STATS_DATE_CREATED);
		$this->roundIdx	 	= $dbStatsFields->GetMaxSize(DB_STATS_ROUND);
		$this->workerIdx	= $dbStatsFields->GetMaxSize(DB_STATS_WORKER);
		$this->weekPoints 	= $dbStatsFields->GetMaxSize(DB_STATS_WEEK_POINTS);
		$this->totPoints 	= $dbStatsFields->GetMaxSize(DB_STATS_TOTAL_POINTS);
		$this->wus 			= $dbStatsFields->GetMaxSize(DB_STATS_WUS);
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbStatsFields;
		//---------------------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_DATE_CREATED,   $this->dtCreated);
		$dbStatsFields->SetValue(DB_STATS_ROUND, $this->roundIdx);
		$dbStatsFields->SetValue(DB_STATS_WORKER, $this->workerIdx);
		//---------------------------------
		if ($this->weekPoints !== false)
			$dbStatsFields->SetValue(DB_STATS_WEEK_POINTS, $this->weekPoints);
		if ($this->totPoints !== false)
			$dbStatsFields->SetValue(DB_STATS_TOTAL_POINTS, $this->totPoints);
		if ($this->wus !== false)
			$dbStatsFields->SetValue(DB_STATS_WUS, $this->wus);
		//---------------------------------
		$sql = $dbStatsFields->scriptUpdate(DB_STATS_ID."=".$this->id);
		if (!$db->Execute($sql))
		{
			XLogError("Stat::Update - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function UpdateWeekpointsPolled()
	{
		global $db, $dbStatsFields;
		//---------------------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_WEEK_POINTS, XFalse2Null($this->weekPoints));
		//---------------------------------
		$sql = $dbStatsFields->scriptUpdate(DB_STATS_ID."=".$this->id);
		if (!$db->Execute($sql))
		{
			XLogError("Stat::UpdateWeekpointsPolled - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function work()
	{
		//---------------------------------
		return $this->weekPoints;
	}
	//------------------
} // class Stat
//---------------
class Stats
{
	//------------------
	function Install()
	{
		global $db, $dbStatsFields;
		//------------------------------------
		$sql = $dbStatsFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Stats::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbStatsFields;
		//------------------------------------
		$sql = $dbStatsFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Stats::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbStatsFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // old version changed work field
				//---------------
				$dbStatsFields->SetValues();
				//---------------
				$newFields = $dbStatsFields->GetNameListString();
				$fieldsArray = $dbStatsFields->GetNameArray();
				//---------------
				for ($i=0;$i < sizeof($fieldsArray);$i++)
					if ($fieldsArray[$i] == DB_STATS_WEEK_POINTS)
						$fieldsArray[$i] = 'work';
					//else if ($fieldsArray[$i] == 'smode')
					//	$fieldsArray[$i] = (ROUND_STATS_MODE_FAH_WORKER | ROUND_STATS_MODE_EO_WEEK_SCORE);
				//---------------
				$oldFields = implode(",", $fieldsArray);
				//---------------
				$sql = "INSERT INTO $dbStatsFields->tableName ($newFields) SELECT $oldFields FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Stats::Import db Execute table import failed (convert ver < 2) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 2: // 2->3 Updated week/avg/tot points fields to BIGINT, just do a copy
			case 3: // 3->4 renamed rank and mode field that became a mysql keyword
				/*
				//---------------
				$dbStatsFields->SetValues();
				//---------------
				$newFields = $dbStatsFields->GetNameListString();
				$fieldsArray = $dbStatsFields->GetNameArray();
				//---------------
				for ($i=0;$i < sizeof($fieldsArray);$i++)
					if ($fieldsArray[$i] == 'wrank')
						$fieldsArray[$i] = "`rank`";
					else if ($fieldsArray[$i] == 'smode')
						$fieldsArray[$i] = "`mode`";
				//---------------
				$oldFields = implode(",", $fieldsArray);
				//---------------
				$sql = "INSERT INTO $dbStatsFields->tableName ($newFields) SELECT $oldFields FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Stats::Import db Execute table import failed (convert ver < 4) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
				*/
			case 4: // 4->5 gutted unused fields: DB_STATS_WORKER_USER_ID, DB_STATS_TEAM, DB_STATS_TEAM_RANK, DB_STATS_RANK, DB_STATS_AVG_POINTS, DB_STATS_WORK (wuid, team, teamrank, wrank, avgpoints, work)
				/*
				//---------------
				$dbStatsFields->SetValues();
				//---------------
				$newFields = $dbStatsFields->GetNameListString();
				//---------------
				$sql = "INSERT INTO $dbStatsFields->tableName ($newFields) SELECT $newFields FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Stats::Import db Execute table import failed (convert ver < 5) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
				*/
			case 5: // 5->6 changed DB_STATS_DATE_CREATED from string to timestamp and renamed it from dtcreated to dt, removed fields: DB_STATS_PAYOUT, DB_STATS_DATE_POLLED and DB_STATS_MODE (pidx, dtpoll, smode)
				//---------------
				$dbStatsFields->SetValues();
				$dbStatsFields->ClearValue(DB_STATS_DATE_CREATED);
				//---------------
				$newFields = $dbStatsFields->GetNameListString(true /*SkipNotSet*/);
				//---------------
				$sql = "INSERT INTO $dbStatsFields->tableName ($newFields,".DB_STATS_DATE_CREATED.") SELECT $newFields, UNIX_TIMESTAMP(dtcreated) AS dt FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Stats::Import db Execute table import failed (convert ver < 6) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbStatsFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbStatsFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Stats::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Stats::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msizeStat = new Stats();
		$msizeStat->setMaxSizes();
		//------------------------------------
		return $msizeStat;		
	}
	//------------------
	function deleteStat($idx)
	{
		global $db, $dbStatsFields;
		//---------------------------------
		$sql = $dbStatsFields->scriptDelete(DB_STATS_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::deleteStat - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbStatsFields;
		//---------------------------------
		$sql = $dbStatsFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->stats = array();
		//---------------------------------
		$this->isLoaded = true;
		//------------------
		return true;
	}
	//---------------------------------	
	function loadStatRaw($idx)
	{
		global $db, $dbStatsFields;
		//------------------
		$dbStatsFields->SetValues();
		//------------------
		$sql = $dbStatsFields->scriptSelect(DB_STATS_ID."=$idx", false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::loadStatRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getStat($idx)
	{
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Stats::getStat validate parameter failed");
			return false;
		}
		//------------------
		$qr = $this->loadStatRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Stats::getStat loadStatRaw $idx failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Stats::getStat index $idx not found.");
			return false;
		}
		//------------------
		return new Stat($s);
	}
	//------------------
	function findStat($roundIdx, $workerIdx)
	{
		//---------------------------------
		global $db, $dbStatsFields;
		//------------------
		$dbStatsFields->SetValues();
		//------------------
		$sql = $dbStatsFields->scriptSelect(DB_STATS_ROUND."=$roundIdx AND ".DB_STATS_WORKER."=$workerIdx", false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::findStat - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogNotify("Stats::findStat - not found.");
			return false;
		}
		//------------------
		return new Stat($s);
	}
	//------------------
	function findRoundStats($roundIdx, $orderBy = false, $limit = false, $onlyWithWeekPoints = false)
	{
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Stats::findRoundStats validate parameters failed");
			return false;
		}
		//------------------
		if ($orderBy === false)
			$orderBy = DB_STATS_ID;
		//------------------
		$where = DB_STATS_ROUND."=$roundIdx";
		//------------------
		if ($onlyWithWeekPoints)
			$where .= ' AND '.DB_STATS_WEEK_POINTS.' IS NOT NULL AND '.DB_STATS_WEEK_POINTS.'>0';
		//------------------
		$stats =  $this->getStats($limit, $orderBy, $where);
		if ($stats === false)
		{
			XLogError("Stats::findRoundStats getStats failed");
			return false;
		}
		//------------------
		return $stats;
	}
	//------------------
	function loadStatsRaw($limit = false, $sort = false, $where = false, $pointsSummary = false)
	{
		global $db, $dbStatsFields;
		//------------------
		if ($sort === false)
			$orderBy = DB_STATS_ID." DESC";
		else
			$orderBy = $sort;
		//------------------
		if ($pointsSummary === false)
			$dbStatsFields->SetValues();
		else
		{
			$dbStatsFields->ClearValues();
			$dbStatsFields->SetValue(DB_STATS_ID);
			$dbStatsFields->SetValue(DB_STATS_WORKER);
			$dbStatsFields->SetValue(DB_STATS_TOTAL_POINTS);
			$dbStatsFields->SetValue(DB_STATS_WEEK_POINTS);
		}
		//------------------
		$sql = $dbStatsFields->scriptSelect($where /*where*/, $orderBy /*orderby*/, $limit /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::loadStatsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getStats($limit = false, $sort = false, $where = false, $pointsSummary = false)
	{
		//------------------
		$qr = $this->loadStatsRaw($limit, $sort, $where, $pointsSummary);
		//------------------
		if ($qr === false)
		{
			XLogError("Stats::loadStats - loadStatsRaw failed");
			return false;
		}
		//------------------
		$statList = array();
		while ($s = $qr->GetRowArray())
			$statList[] = new Stat($s);
		//------------------
		return $statList;
	}
	//------------------
	function addStat($roundIdx, $workerIdx)
	{
		global $db, $dbStatsFields;
		//------------------
		if (!is_numeric($roundIdx) || !is_numeric($workerIdx))
		{
			XLogError("Stats::addStat - validate parameters are numeric failed round idx: ".XVarDump($roundIdx).", worker idx:".XVarDump($workerIdx));
			return false;
		}
		//------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_DATE_CREATED, time());
		$dbStatsFields->SetValue(DB_STATS_ROUND, $roundIdx);
		$dbStatsFields->SetValue(DB_STATS_WORKER, $workerIdx);
		//------------------
		$sql = $dbStatsFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::addStat - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function addFahClientStat($roundIdx, $workerIdx, $points, $wus)
	{
		global $db, $dbStatsFields;
		//------------------
		if (!is_numeric($roundIdx) || !is_numeric($workerIdx) || !is_numeric($points) || !is_numeric($wus))
		{
			XLogError("Stats::addFahClientStat - validate parameters are numeric failed round idx: ".XVarDump($roundIdx).", worker idx:".XVarDump($workerIdx).", points:".XVarDump($points).", wus:".XVarDump($wus));
			return false;
		}
		//------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_DATE_CREATED, time());
		$dbStatsFields->SetValue(DB_STATS_ROUND, $roundIdx);
		$dbStatsFields->SetValue(DB_STATS_WORKER, $workerIdx);
		$dbStatsFields->SetValue(DB_STATS_TOTAL_POINTS, $points);
		$dbStatsFields->SetValue(DB_STATS_WUS, $wus);
		//------------------
		$sql = $dbStatsFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::addFahClientStat - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------ 
	function addCompleteStat($roundIdx, $workerIdx, $weekPoints, $totPoints, $wus, $dtCreated = false)
	{
		global $db, $dbStatsFields;
		//------------------
		if (!is_numeric($roundIdx) || !is_numeric($workerIdx) || !is_numeric($weekPoints) || !is_numeric($totPoints) || !is_numeric($wus) || ($dtCreated !== false && !is_numeric($dtCreated)) )
		{
			XLogError("Stats::addCompleteStat - validate parameters are numeric failed round idx: ".XVarDump($roundIdx).", worker idx:".XVarDump($workerIdx).", weekPoints:".XVarDump($weekPoints).", totPoints:".XVarDump($totPoints).", wus:".XVarDump($wus).", dtCreated: ".XVarDump($dtCreated));
			return false;
		}
		//------------------
		if ($dtCreated === false)
			$dtCreated = time();
		//---------------------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_DATE_CREATED, $dtCreated);
		$dbStatsFields->SetValue(DB_STATS_ROUND, $roundIdx);
		$dbStatsFields->SetValue(DB_STATS_WORKER, $workerIdx);
		$dbStatsFields->SetValue(DB_STATS_WEEK_POINTS, $weekPoints);
		$dbStatsFields->SetValue(DB_STATS_TOTAL_POINTS, $totPoints);
		$dbStatsFields->SetValue(DB_STATS_WUS, $wus);
		//------------------
		$sql = $dbStatsFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::addCompleteStat - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function deleteAllRound($ridx)
	{
		global $db, $dbStatsFields;
		//------------------
		$sql = $dbStatsFields->scriptDelete(DB_STATS_ROUND."=$ridx");
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Stats::deleteAllRound - db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		return true;
	}
	//------------------
	function findRoundStatsPointSummary($roundIdx, $workerIdxAsIndex = false, $weekPointsNotTotal = false, $onlyWithWeekPoints = false, $orderBy = false, $limit = false)
	{
		global $db, $dbStatsFields;
		//------------------
		if ($orderBy === false)
			$orderBy = DB_STATS_ID;
		//------------------
		$where = DB_STATS_ROUND."=$roundIdx";
		//------------------
		if ($onlyWithWeekPoints)
			$where .= ' AND '.DB_STATS_WEEK_POINTS.' IS NOT NULL AND '.DB_STATS_WEEK_POINTS.'>0';
		//------------------
		$pointsField = ($weekPointsNotTotal ? DB_STATS_WEEK_POINTS : DB_STATS_TOTAL_POINTS);
		$pointsKey = ($weekPointsNotTotal ? 'wp' : 'tp');
		//------------------
		$dbStatsFields->ClearValues();
		$dbStatsFields->SetValue(DB_STATS_ID);
		$dbStatsFields->SetValue(DB_STATS_WORKER);
		$dbStatsFields->SetValue($pointsField);
		//------------------
		$sql = $dbStatsFields->scriptSelect($where /*where*/, $orderBy /*orderby*/, $limit /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::findRoundStatsPointSummary - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ($row = $qr->GetRowArray())
		{
			//------------------
			if (!isset($row[DB_STATS_ID]) || !isset($row[DB_STATS_WORKER]) || !isset($row[$pointsField]) || 
				!is_numeric($row[DB_STATS_ID]) || !is_numeric($row[DB_STATS_WORKER]) || !is_numeric($row[$pointsField]))
			{
				XLogError("Stats::findRoundStatsPointSummary - validate row data failed: ".XVarDump($row));
				return false;
			}
			//------------------
			if ($workerIdxAsIndex)
				$rData[$row[DB_STATS_WORKER]] = array(
								"id"  => $row[DB_STATS_ID],
								$pointsKey => $row[$pointsField]
								);
			else
				$rData[] = array(
								"id"  => $row[DB_STATS_ID],
								"wid" => $row[DB_STATS_WORKER],
								$pointsKey => $row[$pointsField]
								);
			//------------------
		}
		//------------------
		return $rData;
	}
	//------------------
	function findIncompleteStats($roundIdx = false, $limit = false, $order = false, $pointsSummary = false)
	{
		//------------------
		if ($order === false)
			$orderBy = DB_STATS_ID;
		else
			$orderBy = $order;
		//------------------
		if ($roundIdx !== false)
			$where = DB_STATS_ROUND."=$roundIdx AND ";
		else
			$where = "";
		//------------------
		$where .= DB_STATS_WEEK_POINTS." is NULL";
		//------------------
		$stats =  $this->getStats($limit /*limit*/, $orderBy  /*orderBy*/, $where, $pointsSummary);
		if ($stats === false)
		{
			XLogError("Stats::findIncompleteStats getStats failed");
			return false;
		}
		//------------------
		return $stats;
	}
	//------------------
	function findRoundWorkersDuplicates($roundIdx)
	{
		global $db;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Stats::findRoundWorkersDuplicates - validate roundIdx failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		$idField = DB_STATS_ID;
		$workerField = DB_STATS_WORKER;
		$roundField = DB_STATS_ROUND;
		$tableName = DB_STATS;
		//------------------
		$sql =  "SELECT a.$workerField, a.$idField FROM $tableName a ";
		$sql .= "JOIN (SELECT $workerField, COUNT(*) AS cnt FROM $tableName WHERE $roundField=$roundIdx AND $workerField>0 GROUP BY $workerField) b ";
		$sql .= "ON a.$workerField=b.$workerField WHERE a.$roundField=$roundIdx AND b.cnt > 1 ORDER BY a.$workerField, a.$idField";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::findRoundWorkersDuplicates - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ( ($r = $qr->GetRow()) )
			$rData[(int)$r[0]/*workerID*/] = (int)$r[1]/*statID*/;
		//------------------
		return $rData;
	}
	//------------------
	function workerHasStats($widx)
	{
		//---------------------------------
		global $db;
		//------------------
		$sql = "SELECT ".DB_STATS_ID." FROM ".DB_STATS." WHERE ".DB_STATS_WORKER."=$widx LIMIT 1";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::workerHasStats - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(-1 /*emptyValue*/);
		if ($value === false)
		{
			XLogError("Stats::workerHasStats db Query GetOneValue failed");
			return false;
		}
		//------------------
		return ($value === null ? -1 /*empty*/ : $value);
	}
	//------------------
	function deepSearchLastTotalPoints($ridx, $widx)
	{
		global $db;
		//------------------
		$sql = "SELECT ".DB_STATS_TOTAL_POINTS." FROM ".DB_STATS." WHERE ".DB_STATS_WORKER."=$widx AND ".DB_STATS_ROUND."=(SELECT MAX(".DB_STATS_ROUND.") FROM ".DB_STATS." WHERE ".DB_STATS_WORKER."=$widx AND ".DB_STATS_ROUND."<$ridx)";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Stats::deepSearchLastTotalPoints - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(-1 /*empty*/);
		//------------------
		if ($value === -1 || $value === null)
		{
			//XLogNotify("Stats::getLastTotalPoints - not found, using zero.");
			return 0;
		}
		//------------------
		if ($value === false || !is_numeric($value))
		{
			XLogError("Stats::deepSearchLastTotalPoints db query GetOneValue failed");
			return false;
		}
		//------------------
		return (int)$value;
	}
	//------------------
} // class Stats
//---------------
?>
