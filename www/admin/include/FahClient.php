<?php
/*
 *	www/include/FahClient.php
 * 
 * The FAH RPC Client, FahClient works with
 * Folding@home's JSON API.
*/
//---------------
define('FAH_API_TEAM_URL', "https://api2.foldingathome.org/team/");
define('FAH_ARCHIVE_FOLDER', './log/archive/');
define('FAH_TEAM_MEMBERS_POLL_RATE', 360.0); // minutes (360min = 6 hours)
//---------------
define('FAH_RPC_DEBUG_FAIL', true);
define('FAH_RPC_DEBUG_SUCCESS', false);
//------------------------
define('CFG_LAST_POLL_TEAM_MEMBERS', 'last-poll-team-members');
//------------------------
require_once('jsonRPCClient.php');
//------------------
class FahClient
{
	var $clientSession = false;
	//------------------
	function pollTeam($rIdx, $teamId)
	{
		//------------------
		XLogDebug("FahClient::pollTeam beginning poll for team $teamId, round $rIdx");
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$reply = $this->request(FAH_API_TEAM_URL.$teamId."/members"); 
		//------------------
		if ($reply === false)
		{
			XLogError("FahClient::pollTeam request members failed");
			return false;
		}
		//------------------
		XLogDebug(XVardump($reply));
		list($rawData, $jsonData) = $reply;
		//------------------
		$fileName = FAH_ARCHIVE_FOLDER."FahClient_$rIdx.json";
		if (@file_put_contents($fileName, $rawData) === false)
		{
			XLogError("FahClient::pollTeam file_put_contents failed: $fileName");
			return false;
		}
		//------------------
		if (!file_exists($fileName))
		{
			XLogError("FahClient::pollTeam verifiy file_exists failed: $fileName");
			return false;
		}
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::pollTeam request time: $secDiff");
		//------------------
		if (!$this->processData($rIdx, $jsonData))
		{
			XLogError("FahClient::pollTeam processData failed");
			return false;
		}
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::pollTeam processData time: $secDiff");
		//------------------
		return true;
	}
	//------------------
	function request($url)
	{
		//------------------
		$client = new jsonRPCClient($url);
		//------------------
		try
		{
			//------------------
			if (FAH_RPC_DEBUG_SUCCESS || FAH_RPC_DEBUG_FAIL)
				$client->debug = true;
			//------------------
			$result = $client->__request(true/*wantRaw*/);
			//------------------
		}
		catch (exception $e)
		{
			//------------------
			XLogNotify("FahClient::request  debug: '$client->debug', failed for url '$url', with exception: $e");
			//------------------
			return false;
		}
		//------------------
		
		//------------------
		if (FAH_RPC_DEBUG_SUCCESS)
			XLogNotify("FahClient::pollTeam success debug: $client->debug");
		//------------------
		return $result;		
	}
	//------------------
	function reparseProcessData($rIdx)
	{
		//------------------
		$fileName = FAH_ARCHIVE_FOLDER."FahClient_$rIdx.json";
		$data = @file_get_contents($fileName);
		if ($data === false)
		{
			XLogError("FahClient::reparseProcessData file_get_contents failed: $fileName");
			return false;
		}
		//------------------
		$data = json_decode($data, true);
		if (json_last_error() !== JSON_ERROR_NONE)
		{
			XLogError("FahClient::reparseData json_decode data failed");
			return false;
		}
		if (!$this->processData($rIdx, $data, true/*reparse*/))
		{
			XLogError("FahClient::reparseData processData failed");
			return false;
		}
		//------------------
		return true;	
	}
	//------------------
	function processData($rIdx, $data, $reparse = false)
	{
		//------------------
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$Rounds = new Rounds() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		$Workers = new Workers() or die ("Create object failed");
		$Wallet = new Wallet() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		//------------------
		$validationMode = $Config->Get(CFG_WORKER_VALIDATION_MODE);
		if ($validationMode === false)
		{
			$validationMode = DEF_CFG_WORKER_VALIDATION_MODE;
			if (!$Config->Set(CFG_WORKER_VALIDATION_MODE, $validationMode))
			{
				XLogError("FahClient::processData Config Set default worker validation mode failed");
				return false;
			}
		}
		//------------------
		if (!is_array($data) || !is_array($data[0]) || $data[0][0] !== "name")
		{
			XLogError("FahClient::processData - validate data format failed");
			return false;
		}
		//------------------
		$round = $Rounds->getRound($rIdx);
		if ($round === false)
		{
			XLogError("FahClient::processData - roundIdx not found: ".$rIdx);
			return false;
		}
		//---------------------------------
		if ($round->teamId === false || !is_numeric($round->teamId))
		{
			XLogError("FahClient::processData - validate round teamId failed: ".XVarDump($round->teamId));
			return false;
		}
		//---------------------------------
		$workerList = $Workers->loadWorkers();
		if ($workerList === false)
		{
			XLogError("FahClient::processData Workers loadWorkers failed");
			return false;
		}
		//------------------
		$newWorkers = array();
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::processData : prep/init $secDiff");
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$workersByName = array();
		// first index 0 was column headers, skip
		// name, id, rank, score, wus
		for ($idx = 1;$idx < sizeof($data);$idx++)  // first pass, add new workers
		{
			//------------------
			if (!sizeof($data[$idx]) == 5 || !is_numeric($data[$idx][1])|| !is_numeric($data[$idx][2])|| !is_numeric($data[$idx][3])|| !is_numeric($data[$idx][4]))
			{
				XLogWarn("FahClient::processData validate donor data entry failed");
				if (FAH_RPC_DEBUG_FAIL)
					XLogNotify("FahClient::processData donor: ".XVarDump($data[$idx]));
				return false;
			}
			//------------------
			$wname 	= $data[$idx][0];
			//------------------
			$worker = false;
			foreach ($workerList as $w)
				if ($w->uname == $wname)
				{
					$worker = $w;
					break;
				}
			//------------------
			if ($worker === false)
				$newWorkers[] = $wname;
			else
				$workersByName[$wname] = $worker; // save looked up object
			//------------------
		} // for $data
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::processData : first pass $secDiff, doner count ".(sizeof($data) - 1));
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		XLogWarn("FahClient::processData found ".sizeof($newWorkers)." new workers.");
		if (sizeof($newWorkers) > 0) // add new workers
		{
			//------------------
			foreach ($newWorkers as $wname)
			{
				if (strlen($wname) > C_MAX_WALLET_ADDRESS_LENGTH)
					$waddress = false;
				else
					$waddrress = $wname;
				if (!$Workers->addWorker($wname, false/*address*/, "<FahClient Automation>"))
				{
					XLogError("FahClient::processData Workers addWorkerfailed for: $wname");
					return false;					
				}
			}
			//------------------
			$workerList = $Workers->loadWorkers();
			if ($workerList === false)
			{
				XLogError("FahClient::processData Workers loadWorkers after new workers failed");
				return false;
			}
			//------------------
			
		} // if (sizeof($newWorkers) > 0)
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::processData add new workers time: $secDiff");
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$updatedWorkers = array();
		// first index 0 was column headers, skip
		// name, id, rank, score, wus
		for ($idx = 1;$idx < sizeof($data);$idx++)  // second pass, add stat for each donor
		{
			//------------------
			$wteam 		= $round->teamId;
			$wname 		= $data[$idx][0];
			$wid		= $data[$idx][1];
			$wrank		= $data[$idx][2];
			$wpoints 	= $data[$idx][3];
			$wwu 		= $data[$idx][4];
			//------------------
			if (isset($workersByName[$wname]))
				$worker = $workersByName[$wname];
			else
			{
				//------------------
				XLogDebug("FahClient::processData second pass looking up worker $wname");
				$worker = false;
				foreach ($workerList as $w)
					if ($w->uname == $wname)
					{
						$worker = $w;
						break;
					}
				//------------------
			}
			//------------------
			if ($worker === false)
			{
				XLogError("FahClient::processData failed to find worker for: $wname after already added new workers");
				return false;					
			}
			//------------------
			$skip = false;
			//------------------
			if ($worker->disabled)
				$skip = 10; 
			//------------------
			if (!$worker->validateAddress($validationMode))
			{
				XLogError("FahClient::processData worker failed to validateAddress for: $wname");
				return false;					
			}
			//------------------
			if (!$worker->validAddress || $worker->address == "")
				$skip = 1;
			else if ($reparse && $Stats->findStat($rIdx, $worker->id) === false)
				$skip = 4; 
			//------------------
			if ($skip === false)
			{
				//------------------
				//XLogDebug("FahClient::processData adding: ridx: $rIdx, workeridx: $worker->id, team: $wteam, wid: $wid, points: $wpoints, wu: $wwu, rank: ".XVardump($wrank));
				if (!$Stats->addFahClientStat($rIdx, $worker->id, $wpoints, $wwu))
				{
					XLogError("FahClient::processData - addFahClientStat failed");
					return false;
				}
				//------------------
			}
			//---------------------------------
			$updatedWorkers[] = $worker;
			//---------------------------------
		} // for $data
		//------------------
		$secDiff = XDateTimeDiff($startUtc); // default against now in UTC returning seconds
		XLogDebug("FahClient::processData second pass, add stats time: $secDiff");
		//------------------
		return true;
	}
	//------------------
	function checkStats($rIdx, $countLimit = false, $reparse = false)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		$timer2 = new XTimer();
		$Rounds = new Rounds() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		//------------------
		$round = $Rounds->getRound($rIdx);
		if ($round === false)
		{
			XLogError("FahClient::checkStats - roundIdx not found: ".$rIdx);
			return false;
		}
		//------------------
		XLogDebug("FahClient::checkStats : looked up rounds took ".$timer->restartMs(true));
		//------------------
		if ($reparse !== false)
			$newStats = $Stats->findRoundStats($rIdx, false /*orderBy default*/, false /*limit*/); // ignore countLimit for reparse
		else
			$newStats  = $Stats->findIncompleteStats($rIdx, $countLimit, false /*orderBy default*/, true /*pointsSummary*/);
		//------------------
		if ($newStats === false)
		{
			XLogError("FahClient::checkStats - findRoundStats/findIncompleteStats failed for new rIdx: ".$rIdx);
			return false;
		}
		//------------------
		$newStatsCount = sizeof($newStats);
		XLogDebug("FahClient::checkStats : looked up $newStatsCount / $countLimit new status for new round $rIdx took ".$timer->restartMs(true));
		//------------------
		$rIdxLast = $Rounds->getLastRoundIndex(true /*done*/, $rIdx /*beforeIdx*/); // returns false on fail, -1 for none, or round index
		if ($rIdxLast === false)
		{
			XLogError("FahClient::checkStats - getLastRoundIndex failed for last rIdx: ".$rIdx);
			return false;
		}
		//------------------
		XLogDebug("FahClient::checkStats : looked up round index $rIdxLast before new round $rIdx took ".$timer->restartMs(true));
		//------------------
		if ($rIdxLast === -1) // if there was no previous round
		{
			$roundLast = false;
			$lastStats = false;
		}
		else // if ($rIdxLast === -1)
		{
			//------------------
			$roundLast = $Rounds->getRound($rIdxLast);
			if ($roundLast === false)
			{
				XLogError("FahClient::checkStats - roundIdx not found: ".$rIdxLast);
				return false;
			}
			//------------------
			XLogDebug("FahClient::checkStats : looked up last round $rIdxLast took ".$timer->restartMs(true));
			//------------------
			$lastStats = $Stats->findRoundStatsPointSummary($rIdxLast, true /*workerIdxAsIndex*/);
			if ($lastStats === false)
			{
				XLogError("FahClient::checkStats - findRoundStatsPointSummary failed for last rIdx: ".$rIdxLast);
				return false;
			}
			//------------------
			XLogDebug("FahClient::checkStats : looked up ".sizeof($lastStats)." old stat point summaries for last round $rIdxLast took ".$timer->restartMs(true));
			//------------------
		} // else // if ($rIdxLast === -1)
		//------------------
		$msDeepSearching = "0";
		$msUpdating = "0";
		$deepCount = 0;
		//------------------
		XLogDebug("FahClient::checkStats - comparing rounds $rIdx and $rIdxLast");
		//------------------
		$weekPoints = array();
		//------------------
		//$debugVerbose  = "FahClient::checkStats:\n";
		$notFoundLastStat = array();
		foreach ($newStats as $nStat)
		{
			//------------------
			$lastStat = ($lastStats !== false && isset($lastStats[$nStat->workerIdx]) ? $lastStats[$nStat->workerIdx] : false);
			//------------------
			if ($lastStat === false)
			{
				//------------------
				$timer2->start();
				//XLogWarn("FahClient::checkStats - (first pass) last stat not found for worker idx: ".$nStat->workerIdx.", deep searching.");
				//------------------
				$lastTotalPoints = $this->getLastTotalPoints($rIdx, $nStat->workerIdx);
				if ($lastTotalPoints === false)
				{
					//------------------
					XLogError("FahClient::checkStats - validate total points for stat and last stat failed. Last stat new stat (".$nStat->id.", ".XVarDump($nStat->totPoints).")");
					return false;
					//------------------
				}
				//------------------
				$deepCount++;
				$msDeepSearching = bcadd($msDeepSearching, $timer2->elapsedMs());
				//------------------
			}
			else // if ($lastStat === false)
			{
				//------------------
				if (!is_numeric($nStat->totPoints) || !is_numeric($lastStat["tp"]))
				{
					//------------------
					XLogError("FahClient::checkStats - validate total points for stat and last stat failed. Last stat (".XVarDump($lastStat)."), new stat (".$nStat->id.", ".XVarDump($nStat->totPoints).")");
					return false;
					//------------------
				}
				//------------------
				$lastTotalPoints = (int)$lastStat["tp"];
				//------------------
			} // else // if ($lastStat === false)
			//------------------
			//if ($lastStat === false)
			//	$strLast = "(deep $lastTotalPoints)";
			//else
			//	$strLast = "($lastStat->id) $lastStat->totPoints";
			//------------------
			//$strCur = "($nStat->id) $nStat->totPoints";
			//------------------
			//$debugVerbose .= "cur: $strCur, last: $strLast\n";
			//------------------
			$weekDiff = $nStat->totPoints - $lastTotalPoints;
			//------------------
			if ($weekPoints < 0)
			{
				XLogError("FahClient::checkStats points anomaly for stat id $nStat->id, $weekDiff = $nStat->totPoints - $lastTotalPoints");
				return false;
			}
			//------------------
			$weekPoints[$nStat->id] = $weekDiff;
			//------------------
		} // foreach newStats
		//------------------
		XLogDebug("FahClient::checkStats - checked $newStatsCount workers, with $deepCount deep searches, took ".$timer->elapsedMs(true).", with $msDeepSearching ms deep searching.");
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahClient::checkStats - db beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($newStats as $nStat)
		{
			$nStat->weekPoints = $weekPoints[$nStat->id];
			if (!$nStat->UpdateWeekpointsPolled())
			{
				XLogError("FahClient::checkStats - UpdateWeekpointsPolled stat failed, stat idx: ".$nStat->id);
				return false;
			}
		}
		//------------------
		if (!$db->commit())
		{
			XLogError("FahClient::checkStats - db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::checkStats - updated weekpoints/polled for $newStatsCount stats, took ".$timer->elapsedMs(true));
		//------------------
		return $newStatsCount;
	}
	//------------------
	function getLastTotalPoints($ridx, $widx)
	{
		global $db;
		//------------------
		$sql = "SELECT ".DB_STATS_TOTAL_POINTS." FROM ".DB_STATS." WHERE ".DB_STATS_WORKER."=$widx AND ".DB_STATS_ROUND."=(SELECT MAX(".DB_STATS_ROUND.") FROM ".DB_STATS." WHERE ".DB_STATS_WORKER."=$widx AND ".DB_STATS_ROUND."<$ridx)";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("FahClient::getLastTotalPoints - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			//XLogNotify("FahClient::getLastTotalPoints - not found, using zero.");
			return 0;
		}
		//------------------
		if (!isset($s[DB_STATS_TOTAL_POINTS]) || !is_numeric($s[DB_STATS_TOTAL_POINTS]))
		{
			XLogNotify("FahClient::getLastTotalPoints - validate total points is set failed. Result: ".XVarDump($s));
			return false;
		}
		//------------------
		return (int)$s[DB_STATS_TOTAL_POINTS];
	}
	//------------------
	function pollTeamOverview()
	{
		global $db, $dbTeamFields;
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		$teamId = $Config ->Get(CFG_ROUND_TEAM_ID);
		if ($teamId === false || !is_numeric($teamId))
		{
			XLogError("FahClient::pollTeamOverview get team ID config failed");
			return false;
		}
		//------------------
		$Team = new Team() or die("Create object failed");
		//------------------
		XLogDebug("FahClient::pollTeamOverview beginning poll for team $teamId");
		$startUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$timer = new XTimer();
		//------------------
		$reply = $this->request(FAH_API_TEAM_URL.$teamId);
		if ($reply === false)
		{
			XLogError("FahClient::pollTeamOverview request failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamOverview request team overview took ".$timer->restartMs(true));
		//------------------
		list($rawData, $jsonData) = $reply;
		//------------------
		$replyCount = $this->request(FAH_API_TEAM_URL."count"); // total team count
		if ($replyCount === false)
		{
			XLogError("FahClient::pollTeamOverview request team count failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamOverview request team count took ".$timer->restartMs(true));
		list($rawDataCount, $jsonDataCount) = $replyCount;
		if (!is_numeric($jsonDataCount))
		{
			XLogError("FahClient::pollTeamOverview validate team count failed: ".XVarDump($replyCount));
			return false;
		}
		//------------------
		$jsonData["total_teams"] = $jsonDataCount;
		//------------------
		$overview = $this->processTeamOverviewData($jsonData);
		//------------------
		if ($overview === false)
		{
			XLogError("FahClient::pollTeamOverview processTeamOverviewData failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamOverview processTeamOverviewData took ".$timer->restartMs(true));
		if (!isset($overview["id"]) || $overview["id"] != $teamId)
		{
			XLogWarn("FahClient::pollTeamOverview verify team id of reply failed. Requested ".XVarDump($teamId).", replied ".XVarDump($overview["id"]));
			XLogDebug(XVardump($reply));
			if (FAH_RPC_DEBUG_FAIL)
				XLogNotify("FahClient::pollTeamOverview reply: ".XVarDump($reply));
			return false;
		}
		//------------------
		if (!$Team->AddFromOverview($overview))
		{
			XLogError("FahClient::processTeamOverviewData Team AddFromOverview failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamOverview team AddFromOverview took ".$timer->restartMs(true));
		//------------------
		return true;
	}
	//------------------
	function pollTeamMembers()
	{
		//---------------------------------
		if (!$this->checkMemberRateLimit())
			return true; // done, don't need team member data yet
		//---------------------------------
		$reply = $this->request(FAH_API_TEAM_URL.$teamId."/members");
		if ($reply === false)
		{
			XLogError("FahClient::pollTeamMembers request team members failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamMembers request team members data took ".$timer->restartMs(true));
		list($rawData, $jsonData) = $reply;
		//------------------
		if (!$this->processTeamMemberData($jsonData))
		{
			XLogError("FahClient::pollTeamMembers processTeamMemberData failed");
			return false;
		}
		//------------------
		XLogDebug("FahClient::pollTeamMembers processTeamMemberData took ".$timer->restartMs(true));
		//------------------
		if (!$this->updateMemberRateLimit())
		{
			XLogError("FahClient::pollTeamMembers updateMemberRateLimit failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function processTeamOverviewData($data)
	{
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		if ($data === false)
		{
			XLogError("FahClient::processTeamOverviewData data not set");
			return false;
		}
		//------------------
		$fieldNames = array();
		$fieldNames[] = "id";
		$fieldNames[] = "name";
		$fieldNames[] = "rank";
		$fieldNames[] = "total_teams";
		$fieldNames[] = "score";
		$fieldNames[] = "wus";
		//------------------
		$overview = array();
		foreach ($fieldNames as $fn)
			if (!isset($data[$fn]) || ($fn != "name" && !is_numeric($data[$fn])))
			{
				XLogWarn("FahClient::processTeamOverviewData required field '$fn' not found or not valid");
				XLogDebug(XVardump($data));
				if (FAH_RPC_DEBUG_FAIL)
					XLogNotify("FahClient::processTeamOverviewData reply: ".XVarDump($data));
				return false;
			}
			else $overview[$fn] = $data[$fn];
		//------------------
		if (!$Config->Set(CFG_TEAM_NAME, $data["name"])) // team's name
		{
			XLogWarn("FahClient::processTeamOverviewData config set team name failed");
			return false;
		}
		//------------------
		return $overview;	
	}
	//------------------
	function processTeamMemberData($data)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$Config = new Config() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		$Team = new Team() or die("Create object failed");
		//------------------
		if ($data === false)
		{
			XLogError("FahClient::processTeamMemberData data not set");
			return false;
		}
		//------------------
		if (!is_array($data) || !is_array($data[0]) || $data[0][0] !== "name")
		{
			XLogError("FahClient::processTeamMemberData - validate data format failed");
			return false;
		}
		//------------------
		$timer = new XTimer();
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahClient::processTeamMemberData db beginTransaction failed");
			return false;
		}
		//------------------
		$strDate = new DateTime('now',  new DateTimeZone('UTC'));
		$strDate = $strDate->format(MYSQL_DATETIME_FORMAT);
		$activeDonors = 0;
		// first index 0 was column headers, skip
		// name, id, rank, score, wus
		foreach ($data as $dataEntry)
		{
			//------------------
			if (sizeof($dataEntry) == 5 && $dataEntry[0] == "name" && $dataEntry[1] == "id" && $dataEntry[3] == "score" && $dataEntry[4] == "wus")
				continue; // this is header column entry, skip
			//------------------
			if (!sizeof($dataEntry) == 5 || !is_numeric($dataEntry[1]) || !is_numeric($dataEntry[3]) || !is_numeric($dataEntry[4]))
			{
				XLogWarn("FahClient::processTeamMemberData validate donor data entry failed");
				if (FAH_RPC_DEBUG_FAIL)
					XLogNotify("FahClient::processTeamMemberData donor: ".XVarDump($dataEntry));
				return false;
			}
			//------------------
			if ($dataEntry[4]/*wus*/ > 0 && $dataEntry[3]/*points/score*/ > 0)
			{
				if (!$Team->addUserDonor($dataEntry[1]/*userID*/, $dataEntry[3]/*points/score*/, $dataEntry[4]/*wus*/, $strDate))
				{
					XLogError("FahClient::processTeamOverviewData Team addUserDonor failed");
					return false;
				}
				$activeDonors++;
			}
			//------------------
		} // for $data
		//------------------
		if (!$db->commit())
		{
			XLogError("FahClient::processTeamOverviewData db commit failed");
			return false;
		}
		//------------------
		XLogError("FahClient::processTeamOverviewData addUserDonor of $activeDonors donors took ".$timer->restartMs(true));
		return true;
	}
	//------------------
	function checkMemberRateLimit()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$lastPoll = $Config->Get(CFG_LAST_POLL_TEAM_MEMBERS);
		if ($lastPoll === false || $lastPoll === "")
		{
			 XLogWarn("FahClient::checkMemberRateLimit No Rate limit info");
			 return true;
		}
		//------------------
		$d = XDateTimeDiff($lastPoll, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
		//------------------
		if ($d === false)
		{
			 XLogError("FahClient::checkMemberRateLimit XDateTimeDiff failed. lastPoll: '$lastPoll'");
			 return false;
		}
		//------------------
		if ($d < FAH_TEAM_MEMBERS_POLL_RATE)
		{
			XLogWarn("FahClient::checkMemberRateLimit rate limitted. minutes since last fah poll: $d");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function updateMemberRateLimit()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		XLogNotify("FahClient::updateMemberRateLimit");
		$dtNow = new DateTime('now', new DateTimeZone('UTC'));
		if (!$Config->Set(CFG_LAST_POLL_TEAM_MEMBERS, $dtNow->format(MYSQL_DATETIME_FORMAT)))
		{
			 XLogError("FahClient::updateMemberRateLimit Config Set last FahClient team members poll failed");
			 return true;
		}
		//------------------
		return true;
	}
	//---------------------------
}// class FahClient
?>
