<?php
/*
 *	www/include/Workers.php
 * 
 * 
* 
*/
//---------------
define('CFG_WORKER_VALIDATION_MODE', 'worker_valid_mode');
define('DEF_CFG_WORKER_VALIDATION_MODE', 0x3B); // current all (63) 0x3F, all but fail on not set (59) 0x3B, all without fail 58 (0x3A)
//---------------
define('WORKER_VALMODE_NONE', 					0x0);
define('WORKER_VALMODE_FAILON_ADDRMISMATCH', 	0x1);
define('WORKER_VALMODE_WARNON_ADDRMISMATCH', 	0x2);
define('WORKER_VALMODE_FAILON_VALIDNOTSET', 	0x4);
define('WORKER_VALMODE_WARNON_VALIDNOTSET', 	0x8);
define('WORKER_VALMODE_WARNON_VALIDATION', 		0x10);
define('WORKER_VALMODE_WARNON_NOVALIDATION', 	0x20);
//---------------
define('WORKER_SPECIAL_MAIN', -1); // used only for sending forfeited funds from stored rewards to main
define('WORKER_SPECIAL_STORE', -2); // used for Payout sending Round's cumulative rewards to be stored
//---------------
class Worker
{
	var $id = -1;
	var $uname = "";
	var $address = "";
	var $dtCreated = "";
	var $dtUpdated = "";
	var $updatedBy = "";
	var $validAddressKnown = true; 
	var $validAddress = false;
	var $disabled = false;
	var $activity = 0;
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id   = $row[DB_WORKER_ID];
		$this->uname = (isset($row[DB_WORKER_USER_NAME]) ? $row[DB_WORKER_USER_NAME] : "");
		$this->address = (isset($row[DB_WORKER_PAYOUT_ADDRESS]) ? $row[DB_WORKER_PAYOUT_ADDRESS] : "");
		$this->dtCreated = (isset($row[DB_WORKER_DATE_CREATED]) ? $row[DB_WORKER_DATE_CREATED] : "");
		if ($this->dtCreated != "")
			if (strtotime($this->dtCreated) === false)
				$this->dtCreated = "";
		$this->dtUpdated = (isset($row[DB_WORKER_DATE_UPDATED]) ? $row[DB_WORKER_DATE_UPDATED] : "");
		if ($this->dtUpdated != "")
			if (strtotime($this->dtUpdated) === false)
				$this->dtUpdated = "";
		$this->updatedBy 	= (isset($row[DB_WORKER_UPDATED_BY]) ? $row[DB_WORKER_UPDATED_BY] : "");
		$this->disabled 	= (isset($row[DB_WORKER_DISABLED]) 		&& $row[DB_WORKER_DISABLED] != 0 ? true : false);
		$this->activity 	= (isset($row[DB_WORKER_ACTIVITY]) ? $row[DB_WORKER_ACTIVITY] : 0);		
		//------------------
		$this->validAddressKnown = (isset($row[DB_WORKER_VALID_ADDRESS]) ? true : false);
		$this->validAddress = ($this->validAddressKnown && $row[DB_WORKER_VALID_ADDRESS] != 0 ? true : false);
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbWorkerFields;
		//------------------
		$this->id 	 = -1;
		$this->uname = $dbWorkerFields->GetMaxSize(DB_WORKER_USER_NAME);
		$this->address = $dbWorkerFields->GetMaxSize(DB_WORKER_PAYOUT_ADDRESS);
		$this->dtCreated = $dbWorkerFields->GetMaxSize(DB_WORKER_DATE_CREATED);
		$this->dtUpdated = $dbWorkerFields->GetMaxSize(DB_WORKER_DATE_UPDATED);
		$this->updatedBy = $dbWorkerFields->GetMaxSize(DB_WORKER_UPDATED_BY);
		$this->validAddress = $dbWorkerFields->GetMaxSize(DB_WORKER_VALID_ADDRESS);
		$this->disabled = $dbWorkerFields->GetMaxSize(DB_WORKER_DISABLED);
		$this->activity = $dbWorkerFields->GetMaxSize(DB_WORKER_ACTIVITY);
		//------------------
	}
	//------------------
	function Update($updatedBy = "")
	{
		global $db, $dbWorkerFields;
		//---------------------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//---------------------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValuePrepared(DB_WORKER_USER_NAME, $this->uname);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_DATE_UPDATED, $nowUtc->format(MYSQL_DATETIME_FORMAT));
		$dbWorkerFields->SetValuePrepared(DB_WORKER_UPDATED_BY, $updatedBy);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_VALID_ADDRESS, $this->validAddress);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_PAYOUT_ADDRESS, $this->address);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_DISABLED, $this->disabled);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_ACTIVITY, $this->activity);
		//---------------------------------
		$sql = $dbWorkerFields->scriptUpdate(DB_WORKER_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Worker::Update - db Prepare failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbWorkerFields->BindValues($dbs))
		{
			XLogError("Worker::Update - BindValues failed.");
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("Worker::Update - dbs Execute scriptUpdate (prepared) failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function UpdateValidAddress()
	{
		global $db, $dbWorkerFields;
		//---------------------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValuePrepared(DB_WORKER_VALID_ADDRESS, $this->validAddress);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_PAYOUT_ADDRESS, $this->address);
		//---------------------------------
		$sql = $dbWorkerFields->scriptUpdate(DB_WORKER_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Worker::UpdateValidAddress - db Prepare failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbWorkerFields->BindValues($dbs))
		{
			XLogError("Worker::UpdateValidAddress - BindValues failed.");
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("Worker::UpdateValidAddress - dbs Execute scriptUpdate (prepared) failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function validateAddress($validationMode = false)
	{
		$Wallet = new Wallet() or die ("Create object failed");
		//---------------------------------
		if ($validationMode === false)
		{
			$Config = new Config() or die("Create object failed");
			$validationMode = $Config->Get(CFG_WORKER_VALIDATION_MODE);
			if ($validationMode === false)
			{
				$validationMode = DEF_CFG_WORKER_VALIDATION_MODE;
				if (!$Config->Set(CFG_WORKER_VALIDATION_MODE, $validationMode))
				{
					XLogError("Worker::validateAddress Config Set default validation mode failed");
					return false;
				}
			}
		}
		//---------------------------------
		if ($this->address == "" || $this->validAddressKnown === false)
		{
			//------------------
			if ($this->validAddressKnown === false)
			{
				if (XMaskContains($validationMode, WORKER_VALMODE_FAILON_VALIDNOTSET))
				{
					XLogError("Fail on #address valid unknown# ($this->id) wuname '$this->uname', wAddress '$this->address'");
					return false;
				}
				if (XMaskContains($validationMode, WORKER_VALMODE_WARNON_VALIDNOTSET))
					XLogWarn("#address valid unknown# ($this->id) wuname '$this->uname', wAddress '$this->address'");
			}
			//------------------
			$wasAddress = $this->address;
			$wasValid = $this->validAddress;
			//------------------
			$address = ($this->address != "" && $this->address != "x" ? $this->address : $this->uname);
			$isValid = $Wallet->isValidAddress($address);
			if ($isValid === false)
			{
				XLogError("Worker::validateAddress wallet isValidAddress failed");
				return false;
			}
			//------------------
			$this->validAddress = ($isValid == $address ? true : false);
			if (XMaskContains($validationMode, WORKER_VALMODE_WARNON_VALIDATION))
				XLogWarn("#Validate# ($this->id) valid ".BoolYN($this->validAddress)." isValid '$isValid' address '$address' wuname '$this->uname', wAddress '$this->address', was valid ".BoolYN($wasValid).($this->validAddressKnown ? "" : " (not set)").", was address '$wasAddress'");
			//------------------
			if ($this->validAddress)
				$this->address = $address;
			else
				$this->address = "x";
			//------------------
			if (!$this->UpdateValidAddress())
			{
				XLogError("Worker::validateAddress worker failed to UpdateValidAddress");
				return false;
			}
			//------------------
		} 
		else // if ($this->address == "" || $this->validAddressKnown === false)
		{
			if (is_string($this->address) && $this->address != "x" && $this->uname != $this->address)
			{
				if (XMaskContains($validationMode, WORKER_VALMODE_FAILON_ADDRMISMATCH))
				{
					XLogError("Worker::validateAddress Fail on #address missmatch# ($this->id) (addr valid) wuname '$this->uname', wAddress '$this->address'");
					return false;
				}
				if (XMaskContains($validationMode, WORKER_VALMODE_WARNON_ADDRMISMATCH))
					XLogWarn("#address missmatch# ($this->id) (addr valid) wuname '$this->uname', wAddress '$this->address'");
				//------------------
				$wasValid = $this->validAddress;
				$address = $this->address;
				$isValid = $Wallet->isValidAddress($address);
				//------------------
				if ($isValid === false)
				{
					XLogError("Worker::validateAddress wallet isValidAddress failed");
					return false;
				}
				//------------------
				$this->validAddress = ($isValid == $address ? true : false);
				//------------------
				if (XMaskContains($validationMode, WORKER_VALMODE_WARNON_VALIDATION))
					XLogWarn("#Validate - missmatch# ($this->id) valid ".BoolYN($this->validAddress)." isValid '$isValid' wuname '$this->uname', wAddress '$this->address', was valid ".BoolYN($wasValid));
				//------------------
				if ($wasValid !== $this->validAddress)
				{
					//------------------
					if (!$this->validAddress)
						$this->address = "x";
					//------------------
					if (!$this->UpdateValidAddress())
					{
						XLogError("Worker::validateAddress worker failed to UpdateValidAddress");
						return false;
					}
					//------------------
				}
				//------------------
			}
			else if (XMaskContains($validationMode, WORKER_VALMODE_WARNON_NOVALIDATION))
			{
				$msg = " - [not tested] ($this->id) valid ".BoolYN($this->validAddress).($this->validAddressKnown ? "" : " (not set)")." - $this->uname";
				if ($this->uname != $this->address)
					$msg .= "($this->address)";
				 XLogWarn($msg);
			}
		}		
		//---------------------------------
		return true;
	}
	//------------------
} // class Worker
//---------------
class Workers
{
	//------------------
	var $workers = array();
	var $isLoaded = false;
	var $workersSortedBy = false;
	var $workersFilter = false;
	var $workersLimit = false;
	//------------------
	function Install()
	{
		global $db, $dbWorkerFields;
		//------------------------------------
		$sql = $dbWorkerFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Workers::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbWorkerFields;
		//------------------------------------
		$sql = $dbWorkerFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Workers::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbWorkerFields;
		//------------------------------------
		XLogError("Workers::Import newVersion: ".$dbWorkerFields->tableVersion.", oldVersion: ".$oldTableVer.", oldTableName: ".$oldTableName);
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1:
				//--------------- Add new Activity field defaulted to zero
				$sql = "INSERT INTO $dbWorkerFields->tableName SELECT $oldTableName.*,0 AS ".DB_WORKER_ACTIVITY." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Workers::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
				//---------------
			case $dbWorkerFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbWorkerFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Workers::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Workers::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msizeWorker = new Worker();
		$msizeWorker->setMaxSizes();
		//------------------------------------
		return $msizeWorker;		
	}
	//------------------
	function deleteWorker($idx)
	{
		global $db, $dbWorkerFields;
		//---------------------------------
		$sql = $dbWorkerFields->scriptDelete(DB_WORKER_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Workers::deleteWorker - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		if ($this->loadWorkers() === false)
		{
			XLogError("Workers::deleteWorker - loadWorkers failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbWorkerFields;
		//---------------------------------
		$sql = $dbWorkerFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Workers::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->workers = array();
		//---------------------------------
		$this->isLoaded = true;
		//------------------
		return true;
	}
	//---------------------------------	
	function getWorkerCount($onlyValid = false)
	{
		global $db, $dbWorkerFields;
		//---------------------------------
		$sql = "SELECT COUNT(*) FROM ".DB_WORKERS;
		//---------------------------------
		if ($onlyValid !== false)
			$sql .= " WHERE ".DB_WORKER_VALID_ADDRESS." IS NOT NULL AND ".DB_WORKER_VALID_ADDRESS." <> 0 AND ".DB_WORKER_PAYOUT_ADDRESS." IS NOT NULL AND ".DB_WORKER_PAYOUT_ADDRESS." <> '' AND ".DB_WORKER_PAYOUT_ADDRESS." <> 'x'";
		//---------------------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::getWorkerCount db Query failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$value = $qr->GetOneValue();
		if ($value === false)
		{
			XLogError("Workers::getWorkerCount db query result GetOneValue failed");
			return false;
		}
		//---------------------------------
		return (int)$value;
	}
	function loadWorkerRaw($idx = false, $uname = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		$dbWorkerFields->SetValues();
		//------------------
		if (($idx === false && $uname === false) || ($idx !== false && $uname !== false))
		{
			XLogError("Workers::loadWorkerRaw - you must search by idx or uname, but not both.");
			return false;
		}
		//------------------
		$where = "";
		if ($idx !== false)
			$where = DB_WORKER_ID."=".$idx;
		if ($uname !== false)
			$where .= DB_WORKER_USER_NAME."=".$uname;
		//------------------
		$sql = $dbWorkerFields->scriptSelect($where, false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::loadWorkerRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function loadWorker($idx, $uname = false)
	{
		//------------------
		$qr = $this->loadWorkerRaw($idx, $uname);
		//------------------
		if ($qr === false)
		{
			XLogError("Workers::loadWorker - loadWorkerRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Workers::loadWorker - worker ($idx, $uname) not found.");
			return false;
		}
		//------------------
		return new Worker($s);
	}
	//------------------
	function getWorker($idx, $uname = false)
	{
		//---------------------------------
		if ($this->isLoaded)
			foreach ($this->workers as $w)
			{
				if ($idx !== false && $w->id == $idx)
					return $w;
				if ($uname !== false && $w->uname == $uname)
					return $w;
				else
				{
					XLogWarn("Workers::getWorker - worker ($idx, $uname) not found.");
					return false;
				}
			}
		//---------------------------------
		return $this->loadWorker($idx, $uname);
	}
	//------------------
	function loadWorkersRaw($sort = false, $filter = false, $limit = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		if ($sort === false)
			$sort = $this->workersSortedBy;
		if ($filter === false)
			$filter = $this->workersFilter;
		//------------------
		if ($sort === false)
		{
			$this->workersSortedBy = false;
			$sort = DB_WORKER_ID;
		}
		//------------------
		if ($filter !== false && is_array($filter))
		{
			//------------------
			$strWhere = "";
			//------------------
			if (isset($filter['disabled']))
				$strWhere .= ($strWhere == "" ? "" : " AND ").($filter['disabled'] === true ? DB_WORKER_DISABLED." <> 0" : "(".DB_WORKER_DISABLED." = 0 OR ".DB_WORKER_DISABLED." IS NULL )");
			//------------------
			if (isset($filter['valid']))
			{
				$strWhere .= ($strWhere == "" ? "" : " AND ");
				if ($filter['valid'] === true)
					$strWhere .= DB_WORKER_VALID_ADDRESS." IS NOT NULL AND ".DB_WORKER_VALID_ADDRESS." <> 0 AND ".DB_WORKER_PAYOUT_ADDRESS." IS NOT NULL AND ".DB_WORKER_PAYOUT_ADDRESS." <> '' AND ".DB_WORKER_PAYOUT_ADDRESS." <> 'x'";
				else
					$strWhere .= DB_WORKER_VALID_ADDRESS." IS NULL OR ".DB_WORKER_VALID_ADDRESS."=0 OR ".DB_WORKER_PAYOUT_ADDRESS." IS NULL OR ".DB_WORKER_PAYOUT_ADDRESS."='' OR ".DB_WORKER_PAYOUT_ADDRESS."='x'";
			}
			//------------------
			if (isset($filter['search']) && $filter['search'] === true)
			{
				//------------------
				if (!isset($filter['search_type']) || !isset($filter['search_text']) || $filter['search_text'] == "" || ($filter['search_type'] == 1 /*ID*/ && !is_numeric($filter['search_text'])))
				{
					XLogError("Workers::loadWorkersRaw - validate parameters failed: ".XVarDump($filter));
					return false;
				}
				//------------------
				$strWhere .= ($strWhere == "" ? "" : " AND ");
				if ($filter['search_type'] == 0 /*Name/Address*/)
				{
					if (isset($filter['search_exact']) && $filter['search_exact'] === true)
						$strWhere .= "(".DB_WORKER_USER_NAME."='".$filter['search_text']."' OR ".DB_WORKER_PAYOUT_ADDRESS."='".$filter['search_text']."')";
					else
						$strWhere .= "(".DB_WORKER_USER_NAME." LIKE '%".$filter['search_text']."%' OR ".DB_WORKER_PAYOUT_ADDRESS." LIKE '%".$filter['search_text']."%')";
				}
				else if ($filter['search_type'] == 1 /*ID*/)
					$strWhere .= DB_WORKER_ID."=".$filter['search_text'];
				else
				{
					XLogError("Workers::loadWorkersRaw - unknown filter search type: ".XVarDump($filter));
					return false;
				}
				//------------------
			}
			//------------------
			$this->workersFilter = $filter;
		}
		else if ($filter !== false && is_string($filter))
		{
			$strWhere = $filter;
			$this->workersFilter = $filter;
		}
		else
		{
			$strWhere = false;
			$this->workersFilter = false;
		}
		//------------------
		$dbWorkerFields->SetValues();
		$sql = $dbWorkerFields->scriptSelect( $strWhere /*where*/, $sort /*orderby*/, $limit /*limit*/);
		//XLogDebug("Workers::loadWorkersRaw sql: $sql");
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::loadWorkersRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function loadWorkers($sort = false, $filter = false, $limit = false)
	{
		$this->workers = array();
		//------------------
		$qr = $this->loadWorkersRaw($sort, $filter, $limit);
		//------------------
		if ($qr === false)
		{
			XLogError("Workers::loadWorkers - loadWorkersRaw failed");
			return false;
		}
		//------------------
		while ($s = $qr->GetRowArray())
			$this->workers[] = new Worker($s);
		//------------------
		$this->isLoaded = true;
		//------------------
		return $this->workers;
	}
	//------------------
	function addWorker($uname, $address = false, $updatedBy = "", $reloadWorkers = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		if ($address === false)
			$address = "";
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//---------------------------------
		if (strlen($uname) > C_MAX_NAME_TEXT_LENGTH)
		{
			XLogError("Worker::addWorker - uname length exceeds max: ".XVarDump($uname));
			return false;
		}
		//---------------------------------
		if ($address !== false && strlen($address) > C_MAX_WALLET_ADDRESS_LENGTH)
		{
			XLogError("Worker::addWorker - address length exceeds max: ".XVarDump($address));
			return false;
		}
		//---------------------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValuePrepared(DB_WORKER_USER_NAME, $uname);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_PAYOUT_ADDRESS, $address);
		$dbWorkerFields->SetValuePrepared(DB_WORKER_DATE_CREATED, $nowUtc->format(MYSQL_DATETIME_FORMAT));
		$dbWorkerFields->SetValuePrepared(DB_WORKER_DATE_UPDATED, $nowUtc->format(MYSQL_DATETIME_FORMAT));
		$dbWorkerFields->SetValuePrepared(DB_WORKER_UPDATED_BY, $updatedBy);
		//------------------
		$sql = $dbWorkerFields->scriptInsert();
		//------------------
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Worker::addWorker - db Prepare failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbWorkerFields->BindValues($dbs))
		{
			XLogError("Worker::addWorker - BindValues failed");
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("Workers::addWorker - dbs Execute scriptInsert (prepared) failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		if ($reloadWorkers === true)
			if ($this->loadWorkers() === false)
			{
				XLogError("Workers::addWorker - loadWorkers failed.");
				return false;
			}
		//---------------------------------
		return true;
	}
	//------------------
	function getWorkerNameIDList($indexIsName = true, $validOnly = false, $invalidOnly = false, $where = false, $order = false, $limit = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		if ($validOnly !== false && $invalidOnly !== false)
		{
			XLogError("Workers::getWorkerNameIDList - can't specify both valid and invalid only");
			return false;
		}
		//------------------
		if ($validOnly !== false || $invalidOnly !== false)
		{
			if ($where === false)
				$where = "";
			else
				$where .= " AND ";
			if ($validOnly !== false)
				$where .= DB_WORKER_VALID_ADDRESS." IS NOT NULL AND ".DB_WORKER_VALID_ADDRESS."<>0 AND ".DB_WORKER_PAYOUT_ADDRESS." IS NOT NULL AND ".DB_WORKER_PAYOUT_ADDRESS."<>'' AND ".DB_WORKER_PAYOUT_ADDRESS."<>'x'";
			if ($invalidOnly !== false)
				$where .= "(".DB_WORKER_VALID_ADDRESS." IS NULL OR ".DB_WORKER_VALID_ADDRESS."=0) AND (".DB_WORKER_PAYOUT_ADDRESS." IS NULL OR ".DB_WORKER_PAYOUT_ADDRESS."='' OR ".DB_WORKER_PAYOUT_ADDRESS."='x')";
		}		
		//------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValue(DB_WORKER_ID);
		$dbWorkerFields->SetValue(DB_WORKER_USER_NAME);
		$sql = $dbWorkerFields->scriptSelect($where/*where*/, ($order == false ? DB_WORKER_ID : $order) /*orderby*/, $limit /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::getWorkerNameIDList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rList = array();
		if ($indexIsName)
		{
			while ($row = $qr->GetRowArray())
				if (isset($row[DB_WORKER_ID]) && isset($row[DB_WORKER_USER_NAME]) && $row[DB_WORKER_USER_NAME] != "")
					$rList[$row[DB_WORKER_USER_NAME]] = $row[DB_WORKER_ID];
		}
		else
		{
			while ($row = $qr->GetRowArray())
				if (isset($row[DB_WORKER_ID]) && isset($row[DB_WORKER_USER_NAME]))
					$rList[$row[DB_WORKER_ID]] = $row[DB_WORKER_USER_NAME];
		}
		//------------------
		return $rList;
	}
	//------------------
	function getWorkers($sort = false, $filter = false, $limit = false)
	{
		//---------------------------------
		if ($this->isLoaded && $sort === $this->workersSortedBy && $filter === $this->workersFilter && $limit === $this->workersLimit)
			return $this->workers;
		//---------------------------------
		return $this->loadWorkers($sort, $filter, $limit);
	}
	//------------------
	function getWorkerName($idx)
	{
		global $db, $dbWorkerFields;
		//------------------
		if ($this->isLoaded)
			foreach ($this->workers as $w)
				if ($w->id == $idx)
					return $w->uname;
		//---------------------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValue(DB_WORKER_USER_NAME);
		$sql = $dbWorkerFields->scriptSelect(DB_WORKER_ID."=$idx", false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::getWorkerName - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$r = $qr->GetRow(); // returns one row, numerically indexed field array
		if (!$r || !isset($r[0]))
			return false;
		//------------------
		return $r[0];
	}
	//------------------
	function resetAllAddressValidations()
	{
		global $db, $dbWorkerFields;
		//------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValue(DB_WORKER_VALID_ADDRESS, false);
		$sql = $dbWorkerFields->scriptUpdate();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Workers::resetAllAddressValidations - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//------------------
		return true;
	}
	//------------------
	function retestInvalidAddresses($updatedBy = "", $clearInvalidAddresses = true)
	{
		//------------------
		$Wallet = new Wallet() or die ("Create object failed");
		//------------------
		$wlist = $this->loadWorkers(false/*$sort*/, false/*$filter*/);
		if ($wlist === false)
		{
			XLogError("Workers::retestInvalidAddresses - loadWorkers failed.");
			return false;
		}
		if (sizeof($wlist) == 0)
		{
			XLogNotify("Workers::retestInvalidAddresses - getWorkers returned an empty list.");
			return true;
		}
		//------------------
		foreach ($wlist as $w)
		{
			//------------------
			if (!$w->disabled && (!$w->validAddressKnown || !$w->validAddress || $w->address == ""))
			{
				//------------------
				if ($w->address != "" && $w->address != "x")
				{
					//------------------
					$address = $w->address;
					//------------------
					if ($w->uname === "0" && strlen($w->address) >= 4)
						$w->uname = $w->address;
					//------------------
				}
				else if ($w->uname != "")
					$address = $w->uname;
				else
					$address = false;
				//------------------
				if ($address !== false)
				{
					//------------------
					$isValid = $Wallet->isValidAddress($address);
					if ($isValid === false)
					{
						XLogError("Workers::retestInvalidAddresses wallet isValidAddress failed");
						return false;
					}
					//------------------
					$w->validAddress = ($isValid == $address ? true : false);
					XLogDebug("Workers::retestInvalidAddresses w-id: $w->id, w-usr: ".XVarDump($w->uname).", w-address: ".XVarDump($w->address).", address: ".XVarDump($address).", isValid: ".XVarDump($isValid).", validAddress: ".XVarDump($w->validAddress));
					if ($w->validAddress)
					{
						//------------------
						$w->address = $address; // could be changing to uname
						if (!$w->Update($updatedBy))
						{
							XLogError("Workers::retestInvalidAddresses worker failed to Update");
							return false;
						}
						//------------------
					} 
					else if (!$w->validAddressKnown || ($clearInvalidAddresses && $w->address != "x")) // not valid, clear address
					{
						//------------------
						if ($clearInvalidAddresses)
							$w->address = "x";
						if (!$w->Update($updatedBy))
						{
							XLogError("Workers::retestInvalidAddresses worker failed to Update");
							return false;
						}
						//------------------
					}
					//------------------
				}
				//------------------
			}
			//------------------
		}
		//------------------
		return true;
	}
	//------------------
	function invalidateAllUsernameAddressMismatches($updatedBy = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValue(DB_WORKER_VALID_ADDRESS, false);
		$dbWorkerFields->SetValue(DB_WORKER_PAYOUT_ADDRESS, "");
		if ($updatedBy !== false)
		{
			$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
			$dbWorkerFields->SetValue(DB_WORKER_UPDATED_BY, $updatedBy);
			$dbWorkerFields->SetValue(DB_WORKER_DATE_UPDATED, $nowUtc->format(MYSQL_DATETIME_FORMAT));
		}
		//---------------------------------
		$where = DB_WORKER_VALID_ADDRESS." IS NOT NULL AND ".DB_WORKER_VALID_ADDRESS."<>0 AND ".DB_WORKER_PAYOUT_ADDRESS."<>".DB_WORKER_USER_NAME;
		//---------------------------------
		$sql = $dbWorkerFields->scriptUpdate($where);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Workers::invalidateAllUsernameAddressMismatches - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//------------------
		return true;
	}
	//------------------
	function findDuplicates()
	{
		//------------------
		$nameList = $this->getWorkerNameIDList(false/*indexIsName*/);
		//------------------
		if ($nameList === false)
		{
			XLogError("Workers::findDuplicates - getWorkerNameIDList failed.");
			return false;
		}
		//------------------
		$duplicates = array();
		//------------------
		foreach ($nameList as $id => $name)
		{
			$dupeIds = array();
			foreach ($nameList as $tid => $tname)
				if ($id != $tid && $name == $tname)
				{
					if (!sizeof($dupeIds))
						$dupeIds[] = $id; // add current, before first match
					$dupeIds[] = $tid;
				}
			if (sizeof($dupeIds))
				$duplicates[$name] = $dupeIds;			
		}
		//------------------
		return $duplicates;
	}
	//------------------
	function getWorkerIDs($onlyValid = false)
	{
		global $db, $dbWorkerFields;
		//------------------
		$dbWorkerFields->ClearValues();
		$dbWorkerFields->SetValue(DB_WORKER_ID);
		//---------------------------------
		if ($onlyValid === false)
			$where = false;
		else
			$where = DB_WORKER_VALID_ADDRESS." IS NOT NULL AND ".DB_WORKER_VALID_ADDRESS."<>0 AND ".DB_WORKER_PAYOUT_ADDRESS." IS NOT NULL AND ".DB_WORKER_PAYOUT_ADDRESS."<>'' AND ".DB_WORKER_PAYOUT_ADDRESS."<>'x'";
		//---------------------------------
		$sql = $dbWorkerFields->scriptSelect($where, DB_WORKER_ID /*orderBy*/);
		//---------------------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::getWorkerIDs - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$list = array();
		while ( ($r = $qr->GetRow()) ) // returns one row, numerically indexed field array
		if (isset($r[0]))
			$list[] = $r[0];
		//------------------
		return $list;
		//---------------------------------
	}
	//------------------
	// only includes valid
	function getWorkerSummaryByActiveAge($includeDetails = false, $indexedList = false) 
	{
		global $db, $dbWorkerFields;
		//------------------
		$sql = "SELECT s1.".DB_WORKER_ID;
		if ($includeDetails !== false)
		{
			$sql .= ", s1.".DB_WORKER_USER_NAME;
			$sql .= ",  CASE WHEN s1.".DB_WORKER_DISABLED." IS NULL THEN 0 ELSE s1.".DB_WORKER_DISABLED." END AS dsb";		
		}
		$sql .= ", j1.mrid FROM ".DB_WORKERS." s1 JOIN (SELECT ".DB_STATS_WORKER.", MAX(".DB_STATS_ROUND.") AS mrid FROM ".DB_STATS." s2 GROUP BY ".DB_STATS_WORKER.") j1 ON s1.".DB_WORKER_ID."=j1.".DB_STATS_WORKER;
		$sql .= " WHERE s1.". DB_WORKER_VALID_ADDRESS." IS NOT NULL AND s1.".DB_WORKER_VALID_ADDRESS." <> 0 AND s1.".DB_WORKER_PAYOUT_ADDRESS." IS NOT NULL AND s1.".DB_WORKER_PAYOUT_ADDRESS." <> '' AND s1.".DB_WORKER_PAYOUT_ADDRESS." <> 'x'";
		$sql .= " ORDER BY j1.mrid DESC";
		//---------------------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Workers::getWorkerSummaryByActiveAge - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$list = array();
		//------------------
		if ($includeDetails === false)
		{
			while ( ($r = $qr->GetRow()) ) // returns one row, numerically indexed field array
				if (isset($r[0]) && is_numeric($r[0]))
					$list[] = $r[0];
		}
		else
		{
			// id,uname,disabled
			if ($indexedList !== false)
			{
				while ( ($r = $qr->GetRow()) ) // returns one row, numerically indexed field array
					if (isset($r[0]) && isset($r[1]) && isset($r[2]) && is_numeric($r[0]) && is_numeric($r[2]))
						$list[$r[0]] = array($r[1], $r[2]); // [id](uname,disabled)
			}
			else
			{
				while ( ($r = $qr->GetRow()) ) // returns one row, numerically indexed field array
					if (isset($r[0]) && isset($r[1]) && isset($r[2]) && is_numeric($r[0]) && is_numeric($r[2]))
						$list[] = array($r[0], $r[1], $r[2]); // [](id,uname,disabled)
			}
		}
		//---------------------------------
		return $list;
	}
	//------------------
} // class Workers
//---------------
?>
