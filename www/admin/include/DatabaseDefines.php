<?php
//---------------
/*
 *	include/DatabaseDefines.php
 * 
 *  
 * 
*/
//---------------
define('DB_TABLE_PREFIX', BRAND_DB_PREFIX);
define('C_MAX_NAME_TEXT_LENGTH', 	 80);
define('C_MAX_TITLE_TEXT_LENGTH', 	120);
define('C_MAX_COMMENT_TEXT_LENGTH', 200);
define('C_MAX_DATE_TEXT_LENGTH', 	120);
define('C_MAX_CONFIG_VALUE_LENGTH', 120);
define('C_MAX_WALLET_ADDRESS_LENGTH', 40); //  26-35, but extra big
define('C_MAX_TRANSACTION_ID_LENGTH', 80); // 64, but extra big
define('MYSQL_DATETIME_FORMAT', "Y-m-d H:i:s"); // format for PHP Datetime::format to store in MySQL
define('MYSQL_STR_TO_DATE_FORMAT', "%Y-%m-%d %H:%i:%s"); // matching format for MySQL STR_TO_DATE to read these
define('C_MAX_AD_LINK_TEXT_LENGTH', 	120);
define('C_MAX_AD_TEXT_LENGTH', 	 		400);
define('C_MAX_AD_IMAGE_TEXT_LENGTH', 	120);
define('C_MAX_BLOG_TEXT_LENGTH', 	8000);
define('C_MAX_MONITOR_EVENT_DETAILS_LENGTH', 400);
//--------------- tables
define('DB_CONFIG',   				DB_TABLE_PREFIX.'config');
define('DB_TEAM',   				DB_TABLE_PREFIX.'team');
define('DB_TEAM_USERS',   			DB_TABLE_PREFIX.'tusers');
define('DB_WORKERS',   				DB_TABLE_PREFIX.'workers');
define('DB_ROUND',   				DB_TABLE_PREFIX.'round');
define('DB_PAYOUT',   				DB_TABLE_PREFIX.'payout');
define('DB_STATS',   				DB_TABLE_PREFIX.'stats');
define('DB_CONTRIBUTION',			DB_TABLE_PREFIX.'contribs');
define('DB_ADS',					DB_TABLE_PREFIX.'ads');
define('DB_BLOG',					DB_TABLE_PREFIX.'blog');
define('DB_NEWDATA',				DB_TABLE_PREFIX.'newdata');
define('DB_MONITOR_EVENT',			DB_TABLE_PREFIX.'monevent');
define('DB_MARKET_SNAPSHOT',		DB_TABLE_PREFIX.'mktsnap');
define('DB_MARKET_DATA',			DB_TABLE_PREFIX.'mktdata');
//--------------- DB_CONFIG
define('DB_CFG_VER', 				3);
define('DB_CFG_NAME', 				'cfgname');
define('DB_CFG_VALUE', 				'cfgval');
define('DB_CFG_COMMENT', 			'comnt');
define('DB_CFG_DATE_MODIFIED', 		'dtmod');
define('DB_CFG_MODIFIED_USER_ID', 	'modby');
//--------------- DB_TEAM
define('DB_TEAM_VER', 				4);
define('DB_TEAM_ID', 				'id');
define('DB_TEAM_DATE', 				'date');
define('DB_TEAM_RANK', 				'teamrank');
define('DB_TEAM_TEAMS', 			'teams');
define('DB_TEAM_CREDIT', 			'credit');
define('DB_TEAM_WUS', 				'wus');
define('DB_TEAM_ACTIVE50', 			'active_50');
define('DB_TEAM_ACTIVEDONORS', 		'active_donors');
//--------------- DB_TEAM_USERS
define('DB_TEAM_USERS_VER', 				3);
define('DB_TEAM_USERS_ID', 				'id');
define('DB_TEAM_USERS_DATE',			'dt');
define('DB_TEAM_USERS_USER_ID',			'uid');
define('DB_TEAM_USERS_POINTS',			'pt');
define('DB_TEAM_USERS_WUS', 			'wu');
//--------------- DB_WORKERS
define('DB_WORKER_VER', 					2);
define('DB_WORKER_ID', 					 'id');
define('DB_WORKER_USER_NAME', 		   'name');
define('DB_WORKER_DATE_CREATED', 	'created');
define('DB_WORKER_DATE_UPDATED',	'updated');
define('DB_WORKER_UPDATED_BY',		'updatedby');
define('DB_WORKER_PAYOUT_ADDRESS',	'payaddr');
define('DB_WORKER_VALID_ADDRESS',	'valaddr');
define('DB_WORKER_DISABLED',		'disabled');
define('DB_WORKER_ACTIVITY',		'activity');
//--------------- DB_ROUND
define('DB_ROUND_VER', 				6);
define('DB_ROUND_ID', 				'id');
define('DB_ROUND_TEAM_ID',			'teamid');
define('DB_ROUND_COMMENT', 			'comment');
define('DB_ROUND_DATE_STARTED', 	'dtstart');
define('DB_ROUND_DATE_STATS', 		'dtstats');
define('DB_ROUND_DATE_PAID', 		'dtpaid');
define('DB_ROUND_FLAGS', 			'flags');
define('DB_ROUND_TOTAL_WORK', 		'totwork');
define('DB_ROUND_TOTAL_PAY', 		'totpay');
define('DB_ROUND_STORE_PAY', 		'storepay');
define('DB_ROUND_PAY_STORED', 		'paystored');
define('DB_ROUND_STEP',				'step');
//--------------- DB_PAYOUT
define('DB_PAYOUT_VER', 				4);
define('DB_PAYOUT_ID', 				'id');
define('DB_PAYOUT_DATE_CREATED',	'dtcreated');
define('DB_PAYOUT_DATE_PAID',		'dtpaid');
define('DB_PAYOUT_DATE_STORED',		'dtstored');
define('DB_PAYOUT_STORE_PAYOUT',	'spid');
define('DB_PAYOUT_ROUND', 			'ridx');
define('DB_PAYOUT_WORKER', 			'widx');
define('DB_PAYOUT_ADDRESS', 		'addr');
define('DB_PAYOUT_STAT_PAY', 		'statpay');
define('DB_PAYOUT_STORE_PAY', 		'storepay');
define('DB_PAYOUT_PAY', 		    'pay');
define('DB_PAYOUT_TXID', 		    'tx');
//--------------- DB_CONTRIBUTION 
define('DB_CONTRIBUTION_VER', 		5);
define('DB_CONTRIBUTION_ID', 		'id');
define('DB_CONTRIBUTION_DATE_CREATED',	'dtcreated');
define('DB_CONTRIBUTION_DATE_DONE',	'dtdone');
define('DB_CONTRIBUTION_OUTCOME',	'outcome');
define('DB_CONTRIBUTION_ROUND', 	'ridx');
define('DB_CONTRIBUTION_ORDER', 	'ord');
define('DB_CONTRIBUTION_NAME', 		'name');
define('DB_CONTRIBUTION_TRUST', 	'trust');
define('DB_CONTRIBUTION_MODE', 		'cmode');
define('DB_CONTRIBUTION_ADDRESS',	'acc');
define('DB_CONTRIBUTION_VALUE',	    'value');
define('DB_CONTRIBUTION_SUBVALUE',	'subval');
define('DB_CONTRIBUTION_COUNTVALUE','cntval');
define('DB_CONTRIBUTION_FLAGS',	    'flags');
define('DB_CONTRIBUTION_TXID',	    'txid');
define('DB_CONTRIBUTION_AD',		'ad');
define('DB_CONTRIBUTION_TOTAL',	    'tot');
define('DB_CONTRIBUTION_COUNT',	    'cnt');
//--------------- DB_ADS
define('DB_ADS_VER',		2);
define('DB_ADS_ID', 		'id');
define('DB_ADS_TITLE',	   'ttl');
define('DB_ADS_FLAGS',	   'flg');
define('DB_ADS_STYLE',	   'sty');
define('DB_ADS_LINK',	   'lnk');
define('DB_ADS_IMAGE',	   'img');
define('DB_ADS_TEXT',	   'txt');
//--------------- DB_STATS
define('DB_STATS_VER', 				6);
define('DB_STATS_ID', 				'id');
define('DB_STATS_DATE_CREATED',		'dt');
define('DB_STATS_ROUND', 			'ridx');
define('DB_STATS_WORKER', 			'widx');
define('DB_STATS_WEEK_POINTS', 		'weekpoints');
define('DB_STATS_TOTAL_POINTS', 	'totpoints');
define('DB_STATS_WUS', 				'wus'); // work units 
//--------------- DB_NEWDATA
define('DB_NEWDATA_VER', 				1);
define('DB_NEWDATA_ID', 				'id');
define('DB_NEWDATA_WORKER_ID',		   	'wid');
define('DB_NEWDATA_WORKER_NAME',		'wn');
define('DB_NEWDATA_WORKER_STATUS',		'ws');
define('DB_NEWDATA_WUS',		   		'wus');
define('DB_NEWDATA_POINTS',		   		'pt');
define('DB_NEWDATA_WEEK_POINTS',	 	'wpt');
define('DB_NEWDATA_POINTS_STATUS',		'ps');
//--------------- DB_BLOG
define('DB_BLOG_VER',					1);
define('DB_BLOG_ID', 					'id');
define('DB_BLOG_FLAGS',			'flags');
define('DB_BLOG_DATE_CREATED',	'dtcreated');
define('DB_BLOG_DATE_PUBLISHED','dtpub');
define('DB_BLOG_SLUG',			'slug');
define('DB_BLOG_USER_ID',		'uid');
define('DB_BLOG_AUTHOR',		'auth');
define('DB_BLOG_TITLE',			'title');
define('DB_BLOG_KEYWORDS',		'keywords');
define('DB_BLOG_TAGS',			'tags');
define('DB_BLOG_CONTENT',	  'content');
//--------------- DB_MONITOR_EVENT
define('DB_MONEVENT_VER',	  			1);
define('DB_MONEVENT_ID',	  			'id');
define('DB_MONEVENT_ROUND',	  			'rid');
define('DB_MONEVENT_DATE',	  			'dt');
define('DB_MONEVENT_END_DATE',			'edt');
define('DB_MONEVENT_TYPE',	  			'tp');
define('DB_MONEVENT_FLAGS',	  			'fl');
define('DB_MONEVENT_ISSUE',	  			'iss');
define('DB_MONEVENT_DETAILS',			'det');
//--------------- DB_MARKET_SNAPSHOT
define('DB_MARKETSNAPSHOT_VER',	  				1);
define('DB_MARKETSNAPSHOT_ID',					'id');
define('DB_MARKETSNAPSHOT_DATE_START',			'dts');
define('DB_MARKETSNAPSHOT_DATE_END',			'dte');
define('DB_MARKETSNAPSHOT_POLL_DATE',			'pd');
define('DB_MARKETSNAPSHOT_PROVIDER',			'pv');
define('DB_MARKETSNAPSHOT_ASSET',				'ast');
//--------------- DB_MARKET_DATA
define('DB_MARKETDATA_VER',	  				1);
define('DB_MARKETDATA_ID',					'id');
define('DB_MARKETDATA_SNAPSHOT',			'sh');
define('DB_MARKETDATA_OTHER_ASSET',			'oa');
define('DB_MARKETDATA_VALUE',				'val');
//--------------- DB_CONFIG fields class
$dbConfigFields = new TXDBFields(DB_CONFIG, DB_CFG_VER);
$dbConfigFields->Add(DB_CFG_NAME,				'varchar('.C_MAX_NAME_TEXT_LENGTH.') NOT NULL PRIMARY KEY', C_MAX_NAME_TEXT_LENGTH);
$dbConfigFields->Add(DB_CFG_VALUE,				'varchar('.C_MAX_CONFIG_VALUE_LENGTH.')', C_MAX_CONFIG_VALUE_LENGTH);
$dbConfigFields->Add(DB_CFG_COMMENT,			'varchar('.C_MAX_COMMENT_TEXT_LENGTH.')', C_MAX_COMMENT_TEXT_LENGTH);
$dbConfigFields->Add(DB_CFG_DATE_MODIFIED,		'INT UNSIGNED');
$dbConfigFields->Add(DB_CFG_MODIFIED_USER_ID,	'INT UNSIGNED');
//--------------- DB_TEAM fields class
$dbTeamFields = new TXDBFields(DB_TEAM, DB_TEAM_VER);
$dbTeamFields->Add(DB_TEAM_ID, 				'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbTeamFields->Add(DB_TEAM_DATE,			'varchar('.C_MAX_DATE_TEXT_LENGTH.')', C_MAX_DATE_TEXT_LENGTH);
$dbTeamFields->Add(DB_TEAM_RANK,			'INT UNSIGNED');
$dbTeamFields->Add(DB_TEAM_TEAMS,			'INT UNSIGNED');
$dbTeamFields->Add(DB_TEAM_CREDIT,			'BIGINT UNSIGNED');
$dbTeamFields->Add(DB_TEAM_WUS,				'INT UNSIGNED');
$dbTeamFields->Add(DB_TEAM_ACTIVE50,		'INT UNSIGNED');
$dbTeamFields->Add(DB_TEAM_ACTIVEDONORS,	'INT UNSIGNED');
//--------------- DB_TEAM_USERS fields class
$dbTeamUsersFields = new TXDBFields(DB_TEAM_USERS, DB_TEAM_USERS_VER);
$dbTeamUsersFields->Add(DB_TEAM_USERS_ID, 				'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbTeamUsersFields->Add(DB_TEAM_USERS_DATE,				'varchar('.C_MAX_DATE_TEXT_LENGTH.')', C_MAX_DATE_TEXT_LENGTH);
$dbTeamUsersFields->Add(DB_TEAM_USERS_USER_ID, 			'INT UNSIGNED');
$dbTeamUsersFields->Add(DB_TEAM_USERS_POINTS,			'BIGINT UNSIGNED');
$dbTeamUsersFields->Add(DB_TEAM_USERS_WUS,				'INT UNSIGNED');
//--------------- DB_WORKERS fields class
$dbWorkerFields = new TXDBFields(DB_WORKERS, DB_WORKER_VER);
$dbWorkerFields->Add(DB_WORKER_ID, 	'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbWorkerFields->Add(DB_WORKER_USER_NAME,	'varchar('.C_MAX_NAME_TEXT_LENGTH.')');
$dbWorkerFields->Add(DB_WORKER_DATE_CREATED,	'varchar('.C_MAX_DATE_TEXT_LENGTH.') NOT NULL', C_MAX_DATE_TEXT_LENGTH);
$dbWorkerFields->Add(DB_WORKER_DATE_UPDATED,	'varchar('.C_MAX_DATE_TEXT_LENGTH.')', C_MAX_DATE_TEXT_LENGTH);
$dbWorkerFields->Add(DB_WORKER_UPDATED_BY,		'varchar('.(C_MAX_NAME_TEXT_LENGTH + 5).')', C_MAX_NAME_TEXT_LENGTH + 5); // extra room to prepend user's ID
$dbWorkerFields->Add(DB_WORKER_PAYOUT_ADDRESS,	'varchar('.C_MAX_WALLET_ADDRESS_LENGTH.')', C_MAX_WALLET_ADDRESS_LENGTH);
$dbWorkerFields->Add(DB_WORKER_VALID_ADDRESS,	'TINYINT(1)');
$dbWorkerFields->Add(DB_WORKER_DISABLED,		'TINYINT(1)');
$dbWorkerFields->Add(DB_WORKER_ACTIVITY,		'INT');
//--------------- DB_ROUND fields class
$dbRoundFields = new TXDBFields(DB_ROUND, DB_ROUND_VER);
$dbRoundFields->Add(DB_ROUND_ID, 			'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbRoundFields->Add(DB_ROUND_TEAM_ID,		'INT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_COMMENT,		'varchar('.C_MAX_COMMENT_TEXT_LENGTH.')', C_MAX_COMMENT_TEXT_LENGTH);
$dbRoundFields->Add(DB_ROUND_DATE_STARTED,	'INT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_DATE_STATS,	'INT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_DATE_PAID,		'INT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_FLAGS,			'INT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_TOTAL_WORK,	'BIGINT UNSIGNED');
$dbRoundFields->Add(DB_ROUND_TOTAL_PAY,		'DOUBLE');
$dbRoundFields->Add(DB_ROUND_STORE_PAY,		'DOUBLE');
$dbRoundFields->Add(DB_ROUND_PAY_STORED,	'DOUBLE');
$dbRoundFields->Add(DB_ROUND_STEP,			'INT UNSIGNED');
//--------------- DB_PAYOUT fields class
$dbPayoutFields = new TXDBFields(DB_PAYOUT, DB_PAYOUT_VER);
$dbPayoutFields->Add(DB_PAYOUT_ID, 	'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbPayoutFields->Add(DB_PAYOUT_DATE_CREATED,	'INT UNSIGNED NOT NULL');
$dbPayoutFields->Add(DB_PAYOUT_DATE_PAID,	'INT UNSIGNED');
$dbPayoutFields->Add(DB_PAYOUT_DATE_STORED,	'INT UNSIGNED');
$dbPayoutFields->Add(DB_PAYOUT_STORE_PAYOUT,	'INT UNSIGNED');
$dbPayoutFields->Add(DB_PAYOUT_ROUND,	'INT UNSIGNED');
$dbPayoutFields->Add(DB_PAYOUT_WORKER,	'INT');
$dbPayoutFields->Add(DB_PAYOUT_ADDRESS,	'varchar('.C_MAX_WALLET_ADDRESS_LENGTH.')', C_MAX_WALLET_ADDRESS_LENGTH);
$dbPayoutFields->Add(DB_PAYOUT_STAT_PAY,	'DOUBLE');
$dbPayoutFields->Add(DB_PAYOUT_STORE_PAY,	'DOUBLE');
$dbPayoutFields->Add(DB_PAYOUT_PAY,		'DOUBLE');
$dbPayoutFields->Add(DB_PAYOUT_TXID,	'varchar('.C_MAX_TRANSACTION_ID_LENGTH.')', C_MAX_TRANSACTION_ID_LENGTH);
//--------------- DB_CONTRIBUTION fields class
$dbContributionFields = new TXDBFields(DB_CONTRIBUTION, DB_CONTRIBUTION_VER);
$dbContributionFields->Add(DB_CONTRIBUTION_ID, 		'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbContributionFields->Add(DB_CONTRIBUTION_DATE_CREATED,	'INT UNSIGNED NOT NULL');
$dbContributionFields->Add(DB_CONTRIBUTION_DATE_DONE,	'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_OUTCOME,		'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_ORDER,		'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_NAME,		'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbContributionFields->Add(DB_CONTRIBUTION_TRUST,		'INT');
$dbContributionFields->Add(DB_CONTRIBUTION_ROUND,		'INT'); // -1 denotes default
$dbContributionFields->Add(DB_CONTRIBUTION_MODE,		'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_ADDRESS,		'varchar('.C_MAX_WALLET_ADDRESS_LENGTH.')', C_MAX_WALLET_ADDRESS_LENGTH);
$dbContributionFields->Add(DB_CONTRIBUTION_VALUE,		'DOUBLE');
$dbContributionFields->Add(DB_CONTRIBUTION_SUBVALUE,	'DOUBLE');
$dbContributionFields->Add(DB_CONTRIBUTION_COUNTVALUE,	'INT');
$dbContributionFields->Add(DB_CONTRIBUTION_FLAGS,		'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_TXID,		'varchar('.C_MAX_TRANSACTION_ID_LENGTH.')', C_MAX_TRANSACTION_ID_LENGTH);
$dbContributionFields->Add(DB_CONTRIBUTION_AD,			'INT UNSIGNED');
$dbContributionFields->Add(DB_CONTRIBUTION_TOTAL,		'DOUBLE');
$dbContributionFields->Add(DB_CONTRIBUTION_COUNT,		'INT');
//--------------- DB_ADS fields class
$dbAds = new TXDBFields(DB_ADS, DB_ADS_VER);
$dbAds->Add(DB_ADS_ID, 		'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbAds->Add(DB_ADS_TITLE,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbAds->Add(DB_ADS_FLAGS,	'INT UNSIGNED');
$dbAds->Add(DB_ADS_STYLE,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbAds->Add(DB_ADS_LINK,	'varchar('.C_MAX_AD_LINK_TEXT_LENGTH.')', C_MAX_AD_LINK_TEXT_LENGTH);
$dbAds->Add(DB_ADS_IMAGE,	'varchar('.C_MAX_AD_IMAGE_TEXT_LENGTH.')', C_MAX_AD_IMAGE_TEXT_LENGTH);
$dbAds->Add(DB_ADS_TEXT,	'varchar('.C_MAX_AD_TEXT_LENGTH.')', C_MAX_AD_TEXT_LENGTH);
//--------------- DB_STATS fields class
$dbStatsFields = new TXDBFields(DB_STATS, DB_STATS_VER);
$dbStatsFields->Add(DB_STATS_ID, 	'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbStatsFields->Add(DB_STATS_DATE_CREATED,	'INT UNSIGNED NOT NULL');
$dbStatsFields->Add(DB_STATS_ROUND,	'INT UNSIGNED');
$dbStatsFields->Add(DB_STATS_WORKER,	'INT UNSIGNED');
$dbStatsFields->Add(DB_STATS_WEEK_POINTS,	'BIGINT UNSIGNED');
$dbStatsFields->Add(DB_STATS_TOTAL_POINTS,	'BIGINT UNSIGNED');
$dbStatsFields->Add(DB_STATS_WUS,	'INT UNSIGNED');
//--------------- DB_NEWDATA fields class
$dbNewDataFields = new TXDBFields(DB_NEWDATA, DB_NEWDATA_VER);
$dbNewDataFields->Add(DB_NEWDATA_ID, 				'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbNewDataFields->Add(DB_NEWDATA_WORKER_ID,			'INT UNSIGNED');
$dbNewDataFields->Add(DB_NEWDATA_WORKER_NAME,		'varchar('.C_MAX_NAME_TEXT_LENGTH.')', C_MAX_NAME_TEXT_LENGTH);
$dbNewDataFields->Add(DB_NEWDATA_WORKER_STATUS,		'INT UNSIGNED');
$dbNewDataFields->Add(DB_NEWDATA_WUS,				'BIGINT UNSIGNED');
$dbNewDataFields->Add(DB_NEWDATA_POINTS,			'BIGINT UNSIGNED');
$dbNewDataFields->Add(DB_NEWDATA_WEEK_POINTS,		'BIGINT UNSIGNED');
$dbNewDataFields->Add(DB_NEWDATA_POINTS_STATUS,		'INT UNSIGNED');
//--------------- DB_BLOG fields class
$dbBlog = new TXDBFields(DB_BLOG, DB_BLOG_VER);
$dbBlog->Add(DB_BLOG_ID, 		'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbBlog->Add(DB_BLOG_FLAGS,	'INT UNSIGNED');
$dbBlog->Add(DB_BLOG_DATE_CREATED,	'varchar('.C_MAX_DATE_TEXT_LENGTH.') NOT NULL', C_MAX_DATE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_DATE_PUBLISHED,	'varchar('.C_MAX_DATE_TEXT_LENGTH.')', C_MAX_DATE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_SLUG,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_USER_ID,	'INT UNSIGNED');
$dbBlog->Add(DB_BLOG_AUTHOR,	'varchar('.C_MAX_NAME_TEXT_LENGTH.')', C_MAX_NAME_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_TITLE,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_KEYWORDS,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_TAGS,	'varchar('.C_MAX_TITLE_TEXT_LENGTH.')', C_MAX_TITLE_TEXT_LENGTH);
$dbBlog->Add(DB_BLOG_CONTENT,	'varchar('.C_MAX_BLOG_TEXT_LENGTH.')', C_MAX_BLOG_TEXT_LENGTH);
//--------------- DB_MONITOR_EVENT fields class
$dbMonEventFields = new TXDBFields(DB_MONITOR_EVENT, DB_MONEVENT_VER);
$dbMonEventFields->Add(DB_MONEVENT_ID, 			'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbMonEventFields->Add(DB_MONEVENT_ROUND,		'INT');
$dbMonEventFields->Add(DB_MONEVENT_DATE,		'INT UNSIGNED');
$dbMonEventFields->Add(DB_MONEVENT_END_DATE,	'INT UNSIGNED');
$dbMonEventFields->Add(DB_MONEVENT_TYPE,		'INT UNSIGNED');
$dbMonEventFields->Add(DB_MONEVENT_FLAGS,		'INT UNSIGNED');
$dbMonEventFields->Add(DB_MONEVENT_ISSUE,		'INT UNSIGNED');
$dbMonEventFields->Add(DB_MONEVENT_DETAILS,		'varchar('.C_MAX_MONITOR_EVENT_DETAILS_LENGTH.')', C_MAX_MONITOR_EVENT_DETAILS_LENGTH);
//--------------- DB_MARKET_SNAPSHOT fields class
$dbMarketSnapshotFields = new TXDBFields(DB_MARKET_SNAPSHOT, DB_MARKETSNAPSHOT_VER);
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_ID, 			'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_DATE_START,	'INT UNSIGNED');
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_DATE_END,	'INT UNSIGNED');
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_POLL_DATE,	'INT UNSIGNED');
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_PROVIDER,	'INT UNSIGNED');
$dbMarketSnapshotFields->Add(DB_MARKETSNAPSHOT_ASSET,		'INT UNSIGNED');
//--------------- DB_MARKET_DATA fields class
$dbMarketDataFields = new TXDBFields(DB_MARKET_DATA, DB_MARKETDATA_VER);
$dbMarketDataFields->Add(DB_MARKETDATA_ID, 			'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
$dbMarketDataFields->Add(DB_MARKETDATA_SNAPSHOT,	'INT UNSIGNED');
$dbMarketDataFields->Add(DB_MARKETDATA_OTHER_ASSET,	'INT UNSIGNED');
$dbMarketDataFields->Add(DB_MARKETDATA_VALUE,		'DOUBLE');
//--------------- 
?>
