<?php
/*
 *	www/include/Rounds.php
 * 
 * 
* 
*/
//---------------
define('ROUND_FLAG_NONE', 					0x0);
define('ROUND_FLAG_ACTIVE',					0x1);
define('ROUND_FLAG_DONE',					0x2);
define('ROUND_FLAG_STORE_VALUES', 			0x4);
define('ROUND_FLAG_FORFEIT_STORE',			0x8);
define('ROUND_FLAG_FUNDED',					0x10); // 16
define('ROUND_FLAG_APPROVED',				0x20); // 32
//-----
define('ROUND_FLAG_DRYRUN', 				0x100); //(256) 
define('ROUND_FLAG_HIDDEN', 				0x200); //(512)
define('ROUND_FLAG_TEST_BALANCE', 			0x400); //(1024)
//-----
define('ROUND_FLAG_LEGACY_STATMODE_EOW',	0x20000000); //(536870912)  EO Week (all but 4 had FAH worker also)
define('ROUND_FLAG_LEGACY_STATMODE_FAH',	0x40000000); //(1073741824) FAH score
define('ROUND_FLAG_LEGACY_STATMODE_OTHER',	0x80000000); //(2147483648) EO Total less week score. Round 37 only, a correction payout.
//---------------
define('DEFAULT_ROUND_TEAM_ID', BRAND_FOLDING_TEAM_ID);
define('DEFAULT_ROUND_FLAGS', (ROUND_FLAG_ACTIVE | ROUND_FLAG_STORE_VALUES | ROUND_FLAG_FORFEIT_STORE));
define('DEFAULT_ROUND_PAY_DIGITS', 4);
define('DEFAULT_ROUND_TRANS_WAIT', 8); // ms
define('DEFAULT_ROUND_ACTION_STACK', '1,A,B,1,C,D,E,F,10,11,1,1E,32,34,47,34,36,1,29,2A,35,1F,32,33,46,33,36,1,28,29,2A,50,FF');
define('DEFAULT_ROUND_PAY_MINIMUM', 1); 
define('DEFAULT_ROUND_STORE_MINIMUM', 0.5); 
define('DEFAULT_ROUND_STORE_TX_MINIMUM', 3); 
define('DEFAULT_ROUND_STORE_FORFEIT_DAYS', 0);
define('DEFAULT_ROUND_STORE_FORFEIT_IDLE_DAYS', 0);
//---------------
define('CFG_ROUND_TEAM_ID', 'def_round_team_id');
define('CFG_ROUND_FLAGS', 'def_round_flags');
define('CFG_ROUND_PAY_DIGITS', 'def_round_pay_digits');
define('CFG_ROUND_TRANS_WAIT', 'round_trans_wait'); // ms
define('CFG_ROUND_ACTION_STACK', 'round_action_stack');
define('CFG_ROUND_PAY_MINIMUM', 'round_pay_minimum'); 
define('CFG_ROUND_STORE_MINIMUM', 'round_store_minimum'); 
define('CFG_ROUND_STORE_TX_MINIMUM', 'round_store_tx_minimum'); 
define('CFG_ROUND_STORE_FORFEIT_DAYS', 'round_store_forfeit_days'); // days
define('CFG_ROUND_STORE_FORFEIT_IDLE_DAYS', 'round_store_forfeit_idle_days'); // days
//-----------
define('CFG_ROUND_LAST_TRANSACTION', 'round_last_trans');
define('CFG_ROUND_OLDEST_ACTIVE', 'round_oldest_active');
define('CFG_ROUND_LAST_DONE', 'round_last_done'); 
//---------------
define('CFG_ROUND_PASSCOUNT_FAHAPP_STEP_BASE', 'round_passcount_fahapp_step_');
define('DEF_ROUND_PASSCOUNT_FAHAPP', 50);
define('CFG_ROUND_TIMEOUT_FAHAPP_STEP', 'round_timeout_fahapp_step');
define('DEF_ROUND_TIMEOUT_FAHAPP_STEP', 40); // seconds
define('CFG_ROUND_PROGRESS', 'round_progress'); // ridx,action,prog,progMax
//---------------
define('ROUND_ACTION_UNKNOWN', 						  -2); // -2
define('ROUND_ACTION_NOT_SET', 						  -1); // -1
define('ROUND_ACTION_NONE', 						   0); // 0
define('ROUND_ACTION_WAIT_MONITOR_EVENTS', 			   1); // 1
define('ROUND_ACTION_FAH_APP_STATS', 				 0xA); // 10
define('ROUND_ACTION_VERIFY_NEW_DATA', 				 0xB); // 11
define('ROUND_ACTION_FAH_APP_NEW_WORKERS',			 0xC); // 12
define('ROUND_ACTION_FAH_APP_VALIDATE_WORKERS',		 0xD); // 13
define('ROUND_ACTION_FAH_APP_WEEK_STATS',			 0xE); // 14
define('ROUND_ACTION_FAH_APP_DEEP_STATS',			 0xF); // 15
define('ROUND_ACTION_FAH_APP_ADD_STATS',			0x10); // 16
define('ROUND_ACTION_VERIFY_NEW_STATS',				0x11); // 17
define('ROUND_ACTION_TEST_FORFEIT',					0x1E); // 30
define('ROUND_ACTION_CHECK_FORFEIT',			   	0x1F); // 31
define('ROUND_ACTION_WAIT_FUNDED',			   		0x28); // 40
define('ROUND_ACTION_CLEAR_APPROVED',			   	0x29); // 41
define('ROUND_ACTION_WAIT_APPROVED',			   	0x2A); // 42
define('ROUND_ACTION_CONTRIBUTIONS_REQ',			0x32); // 50
define('ROUND_ACTION_CONTRIBUTIONS_STEP',			0x33); // 51
define('ROUND_ACTION_CONTRIBUTIONS_TEST',			0x34); // 52
define('ROUND_ACTION_CONTRIBUTIONS_UNTEST',			0x35); // 53
define('ROUND_ACTION_VERIFY_CONTRIBUTIONS',			0x36); // 54
define('ROUND_ACTION_ACCUMULATE_STOREPAYS',			0x46); // 70
define('ROUND_ACTION_ACCUMULATE_STOREPAYS_TEST',	0x47); // 71
define('ROUND_ACTION_SEND_PAYOUT',					0x50); // 80
define('ROUND_ACTION_DONE',							0xFF); // 255
//---------------
define('ROUND_STEP_NONE',	0);
define('ROUND_STEP_START', 	1);
define('ROUND_STEP_DONE', 	999);
//---------------
class Round
{
	var $id = -1;
	var $teamId = false;
	var $comment = false;
	var $dtStarted = false;
	var $dtStats = false;
	var $dtPaid = false;
	var $flags = ROUND_FLAG_NONE;
	var $totalWork = 0;
	var $totalPay = 0.0;
	var $storePay = 0.0;
	var $payStored = 0.0;
	var $step = ROUND_STEP_NONE;
	//------------------
	// set dynamically
	var $actionStack = false;
	var $action = ROUND_ACTION_NOT_SET; 
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->comment = false; // load on demand
		//------------------
		$this->id			= XArray($row, DB_ROUND_ID, -1);
		$this->teamId		= XArray($row, DB_ROUND_TEAM_ID, false);
		$this->comment	 	= XArray($row, DB_ROUND_COMMENT, false);
		$this->dtStarted	= XArray($row, DB_ROUND_DATE_STARTED, false);
		$this->dtStats		= XArray($row, DB_ROUND_DATE_STATS, false);
		$this->dtPaid		= XArray($row, DB_ROUND_DATE_PAID, false);
		$this->flags		= XArray($row, DB_ROUND_FLAGS, ROUND_FLAG_NONE);
		$this->totalWork	= XArray($row, DB_ROUND_TOTAL_WORK, 0);
		$this->totalPay		= XArray($row, DB_ROUND_TOTAL_PAY, 0.0);
		$this->storePay		= XArray($row, DB_ROUND_STORE_PAY, 0.0);
		$this->payStored	= XArray($row, DB_ROUND_PAY_STORED, 0.0);
		$this->step			= XArray($row, DB_ROUND_STEP, ROUND_STEP_NONE);
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbRoundFields;
		//------------------		
		$this->id 	 		= -1;
		$this->teamId	 	= $dbRoundFields->GetMaxSize(DB_ROUND_TEAM_ID);
		$this->comment	 	= $dbRoundFields->GetMaxSize(DB_ROUND_COMMENT);
		$this->dtStarted	= $dbRoundFields->GetMaxSize(DB_ROUND_DATE_STARTED);
		$this->dtStats		= $dbRoundFields->GetMaxSize(DB_ROUND_DATE_STATS);
		$this->dtPaid	 	= $dbRoundFields->GetMaxSize(DB_ROUND_DATE_PAID);
		$this->flags		= $dbRoundFields->GetMaxSize(DB_ROUND_FLAGS);
		$this->totalWork	= $dbRoundFields->GetMaxSize(DB_ROUND_TOTAL_WORK);
		$this->totalPay	 	= $dbRoundFields->GetMaxSize(DB_ROUND_TOTAL_PAY);
		$this->storePay	 	= $dbRoundFields->GetMaxSize(DB_ROUND_STORE_PAY);
		$this->payStored 	= $dbRoundFields->GetMaxSize(DB_ROUND_PAY_STORED);
		$this->step	 		= $dbRoundFields->GetMaxSize(DB_ROUND_STEP);
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbRoundFields;
		//---------------------------------
		$dbRoundFields->ClearValues();
		$dbRoundFields->SetValue(DB_ROUND_TEAM_ID,		$this->teamId);
		$dbRoundFields->SetValue(DB_ROUND_DATE_STATS,	XFalse2Null($this->dtStats));
		$dbRoundFields->SetValue(DB_ROUND_DATE_PAID,	XFalse2Null($this->dtPaid));
		$dbRoundFields->SetValue(DB_ROUND_FLAGS,		$this->flags);
		$dbRoundFields->SetValue(DB_ROUND_TOTAL_WORK,	$this->totalWork);
		$dbRoundFields->SetValue(DB_ROUND_TOTAL_PAY,	$this->totalPay);
		$dbRoundFields->SetValue(DB_ROUND_STORE_PAY,	$this->storePay);
		$dbRoundFields->SetValue(DB_ROUND_PAY_STORED,	$this->payStored);
		$dbRoundFields->SetValue(DB_ROUND_STEP,			$this->step);
		//---------------------------------
		$sql = $dbRoundFields->scriptUpdate(DB_ROUND_ID."=".$this->id);
		if (!$db->Execute($sql))
		{
			XLogError("Round::Update - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function updateValue($field, $value, $useFalse2Null = true)
	{
		return $this->updateValues( array( $field => $value ), $useFalse2Null);
	}
	//------------------
	function updateValues($fieldValueArray, $useFalse2Null = true)
	{
		global $db, $dbRoundFields;
		//------------------
		if (!is_array($fieldValueArray) || sizeof($fieldValueArray) == 0)
		{
			XLogError("Round::updateValues validate parameter failed: ".XVarDump($fieldValueArray));
			return false;
		}
		//------------------
		$dbRoundFields->ClearValues();
		
		foreach ($fieldValueArray as $field => $value)
			$dbRoundFields->SetValuePrepared($field, ($useFalse2Null === true ? XFalse2Null($value) : $value));
		//------------------
		$sql = $dbRoundFields->scriptUpdate(DB_ROUND_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Round::updateValues db Prepare failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbRoundFields->BindValues($dbs))
		{
			XLogError("Round::updateValues BindValues failed. fieldValueArray: ".XVarDump($fieldValueArray));
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("Round::updateValues db prepared statement Execute failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function initActionStack($forceReload = false)
	{
		$Rounds = new Rounds() or die("Create object failed");
		//------------------
		$this->action = ROUND_ACTION_NOT_SET; // set in case we fail
		//------------------
		if ($forceReload !== false || $this->actionStack === false)
		{
			//------------------
			$this->actionStack = $Rounds->getRoundActionStack();
			if ($this->actionStack === false)
			{
				XLogError("Round::initActionStack Rounds getRoundActionStack failed");
				return false;
			}
			//------------------
		} // if reloading action stack
		//------------------
		$this->action = $Rounds->getActionFromStep($this->step, $this->actionStack);
		if ($this->action === false)
		{
			$this->action = ROUND_ACTION_NOT_SET;
			XLogError("Round::initActionStack Rounds getActionFromStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function hasFlag($flagValue)
	{
		//------------------
		return XMaskContains($this->flags, $flagValue);
	}
	//------------------
	function setFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("Round::setFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		$this->flags |= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function clearFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("Round::clearFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		if ( ($this->flags & $flagValue) != 0)
			$this->flags ^= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function updateFlags($newValue = false)
	{
		//------------------
		if ( ($newValue !== false && !is_numeric($newValue)) || ($newValue === false && !is_numeric($this->flags)) )
		{
			XLogError("Round::updateFlags validate values failed");
			return false;
		}
		//------------------
		if ($newValue !== false)
			$this->flags = $newValue;
		//------------------
		if (!$this->updateValue(DB_ROUND_FLAGS, $this->flags))
		{
			XLogError("Round::updateFlags updateValue failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function clearProgress()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		if (!$Config->Set(CFG_ROUND_PROGRESS, '-1,-1,0,0'))
		{
			XLogError("Round::clearProgress Config Set failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function updateProgress($value, $maxValue, $noChangeCount = false)
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		if (!is_numeric($this->action) || !is_numeric($value) || !is_numeric($maxValue) || ($noChangeCount !== false && !is_numeric($noChangeCount)))
		{
			XLogError("Round::updateProgress validate values failed");
			return false;
		}
		//------------------
		if ($noChangeCount === false)
		{
			//------------------
			$dataLast = $this->getProgress();
			if ($dataLast === false)
			{
				XLogError("Round::updateProgress getProgress (last) failed");
				return false;
			}
			//------------------
			if ($dataLast[0] == 0 && $dataLast[1] == 0 && $dataLast[2] == 0)
				$noChangeCount = 0;
			else
			{
				$noChangeCount = $dataLast[2];
				if ($dataLast[0] != $value)
					$noChangeCount = 0;
				else
					$noChangeCount++;
			}				
			//------------------
		}		
		//------------------
		$data = "$this->id,$this->action,$value,$maxValue,$noChangeCount";
		//------------------
		if (!$Config->Set(CFG_ROUND_PROGRESS, $data))
		{
			XLogError("Round::updateProgress Config Set failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function getProgress()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$data = $Config->GetSetDefault(CFG_ROUND_PROGRESS, '-1,-1,0,0,0');
		if ($data === false)
		{
			XLogError("Round::getProgress Config Get failed");
			return false;
		}
		//------------------
		$dataList = explode(',', $data);
		if (sizeof($dataList) != 5 || !is_numeric($dataList[0]) || !is_numeric($dataList[1]) || !is_numeric($dataList[2]) || !is_numeric($dataList[3]) || !is_numeric($dataList[4]))
		{
			XLogWarn("Round::getProgress validate progress data failed, clearing...");
			return array(0, 0, 0);
		}
		//------------------
		if ($dataList[0] != $this->id || $dataList[1] != $this->action)
			return array(0, 0, 0); // progress not set or for a previous round/action, return zeros
		//------------------
		return  array($dataList[2] /*value*/, $dataList[3] /*maxValue*/, $dataList[4] /*noChangeCount*/);
	}
	//------------------
	function isActive()
	{
		//------------------
		return $this->hasFlag(ROUND_FLAG_ACTIVE);
	}
	//------------------
	function isDone()
	{
		//------------------
		return $this->hasFlag(ROUND_FLAG_DONE);
	}
	//------------------
	function isHidden()
	{
		//------------------
		return $this->hasFlag(ROUND_FLAG_HIDDEN);
	}
	//------------------
	function currentAction($initActinStack = true)
	{
		//------------------
		if ($initActinStack === true && $this->actionStack === false)
			if (!$this->initActionStack())
			{
				XLogError("Round::currentAction initActionStack failed");
				return false;
			}
		//------------------
		return $this->action;
	}
	//------------------
	function actionName($initActinStack = true)
	{
		$Rounds = new Rounds() or die("Create object failed");
		//------------------
		if ($initActinStack === true && $this->actionStack === false)
			if (!$this->initActionStack())
			{
				XLogError("Round::actionName initActionStack failed");
				return false;
			}
		//------------------
		if ($this->action == ROUND_ACTION_NOT_SET)
			return "[not set]";
		//------------------
		if ($this->action == ROUND_ACTION_UNKNOWN)
			return "[unknown]";
		//------------------
		$names = $Rounds->getActionTextNames();
		//------------------
		if (!isset($names[$this->action]))
			return "[unkown ".$this->action."]";
		return $names[$this->action];		
	}
	//------------------
	function incStep($updateStep = true)
	{
		//------------------
		$this->step++;
		//------------------
		if ($updateStep === true)
		{
			if (!$this->updateStep())
			{
				XLogError("Rounds::incStep updateStep failed");
				return false;
			}
		}
		else if (!$this->initActionStack()) // Update current action. updateStep does this, but was skipped.
		{
			XLogError("Rounds::incStep initActionStack failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function updateStep($newStep = false)
	{
		//------------------
		$oldStep = $this->step;
		//------------------
		if ($newStep !== false)
			$this->step = $newStep;
		//------------------
		if (!is_numeric($this->step))
		{
			XLogError("Rounds::updateStep step in not a valid number: ".XVarDump($this->step).", step passed as param: ".XVarDump($newStep).", old step: ".XVarDump($oldStep));
			return false;
		}
		//------------------
		if (!$this->updateValue(DB_ROUND_STEP, $this->step))
		{
			XLogError("Rounds::updateStep updateValue failed");
			return false;
		}
		//------------------
		if (!$this->initActionStack()) // Update current action
		{
			XLogError("Rounds::updateStep initActionStack failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function getComment($default = "")
	{
		global $db, $dbRoundFields;
		//------------------
		if ($this->comment !== false)
			return $this->comment;
		//------------------
		$dbRoundFields->ClearValues();
		$dbRoundFields->SetValue(DB_ROUND_COMMENT);
		//------------------
		$sql = $dbRoundFields->scriptSelect(DB_ROUND_ID."=".$this->id, false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::getComment db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(0 /*emptyValue*/);
		if ($value === false || ($value !== 0 && !is_string($value)))
		{
			XLogError("Rounds::getComment GetOneValue failed, idx: $this->id, reply: ".XVarDump($value));
			return false;
		}
		//------------------
		if ($value === 0 /*emptyValue*/)
			return $default;
		//------------------
		$this->comment = $value;
		//------------------
		return $this->comment;
	}
	//------------------
	function setComment($comment = false)
	{
		//------------------
		if (!$this->updateValue(DB_ROUND_COMMENT, $comment))
		{
			XLogError("Round::setComment updateValue failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function step()
	{
		//------------------
		if (!$this->initActionStack())
		{
			XLogError("Round::step initActionStack failed");
			return false;
		}
		//------------------
		XLogDebug("Round::step step: $this->step, action: ".XVarDump($this->action)." (".$this->actionName().")");
		//------------------
		if ($this->action == ROUND_ACTION_NONE)
		{
			//------------------
			if (!$this->hasFlag(ROUND_FLAG_ACTIVE))
				return true; // do nothing
			//------------------
			if (!$this->step == ROUND_STEP_NONE)
			{
				XLogError("Round::step (action none) validate step failed. Expected to also be none, but step is: ".XVarDump($this->step));
				return false;
			}
			//------------------
			$Monitor = new Monitor or die ('Create object failed');
			if (!$Monitor->PreValidate($this))
			{
				XLogError("Rounds::step (action none) Monitor PreValidate failed");
				return false;
			}
			//------------------
			$Contributions = new Contributions or die ('Create object failed');
			if (!$Contributions->cleanupContributionsRoundStart())
			{
				XLogError("Round::step Contributions cleanupContributionsRoundStart (action none) failed");
				return false;
			}
			//------------------
			if (!$this->updateStep(ROUND_STEP_START))
			{
				XLogError("Round::step (action none) updateStep to start failed");
				return false;
			}
			//------------------
			return $this->step(); // call step recursively, so action stack is initted and immediately start
		}
		else if ($this->action == ROUND_ACTION_WAIT_MONITOR_EVENTS)
		{
			$Monitor = new Monitor or die ('Create object failed');
			//------------------
			$eventList = $Monitor->RoundEventsAlarming($this->id);
			if ($eventList === false)
			{
				XLogError("Rounds::step (wait mon events) Monitor RoundEventsAlarming failed");
				return false;
			}
			//---------------------------------
			if (sizeof($eventList) != 0)
			{
				XLogDebug("Rounds::step (wait mon events) Monitor PreValidate alarm events detected. Waiting for them to be resolved...");
				return true; // waiting, not a failure
			}
			//------------------
			if (!$this->incStep())
			{
				XLogError("Round::step (wait mon events) incStep failed");
				return false;
			}
			//------------------
			return true;
		}
		else if ($this->action == ROUND_ACTION_DONE)
		{
			//------------------
			$this->clearFlag(ROUND_FLAG_ACTIVE, false /*updateChanged*/);
			$this->setFlag(ROUND_FLAG_DONE, false /*updateChanged*/);
			if (!$this->updateFlags())
			{
				XLogError("Round::step updateFlags (action done) failed");
				return false;
			}
			//------------------
			if (!$this->updateStep(ROUND_STEP_DONE))
			{
				XLogError("Round::step updateFlags (action done) failed");
				return false;
			}
			//------------------
			$Config = new Config() or die("Create object failed");
			$last = $Config->Get(CFG_ROUND_LAST_DONE, false);
			if (!is_numeric($last) || $last < $this->id)
				if (!$Config->Set(CFG_ROUND_LAST_DONE, $this->id))
				{
					XLogError("Round::step Config Set last done failed");
					return false;
				}
			//------------------
			return true; // do nothing
		}
		else if ($this->action == ROUND_ACTION_UNKNOWN)
		{
			XLogWarn("Round::step step $this->step has an unknown action, continuing with next step...");
			XLogDebug("Round::step actionStack: ".XVarDump($this->actionStack));
			if (!$this->incStep())
			{
				XLogError("Round::step (action unknown) incStep failed");
				return false;
			}
			return true; // do nothing but warn, soft error
		}
		else if ($this->action == ROUND_ACTION_NOT_SET)
		{
			XLogError("Round::step action not set");
			return false;
		}
		//------------------
		if (!$this->hasFlag(ROUND_FLAG_ACTIVE))
		{
			//------------------
			if (!$this->setFlag(ROUND_FLAG_ACTIVE))
			{
				XLogError("Round::step setFlag active failed");
				return false;
			}
			//------------------
			$Config = new Config() or die("Create object failed");
			$oldest = $Config->Get(CFG_ROUND_OLDEST_ACTIVE, false);
			if (!is_numeric($oldest) || $oldest < $this->id)
				if (!$Config->Set(CFG_ROUND_OLDEST_ACTIVE, $this->id))
				{
					XLogError("Round::step Config Set oldest active failed");
					return false;
				}
			//------------------
		}
		//------------------
		if ($this->action == ROUND_ACTION_FAH_APP_STATS)
			$rval = $this->requestFahAppStats();
		else if ($this->action == ROUND_ACTION_VERIFY_NEW_DATA)
			$rval = $this->verifyNewData();
		else if ($this->action == ROUND_ACTION_VERIFY_NEW_STATS)
			$rval = $this->verifyNewStats();
		else if ($this->action == ROUND_ACTION_VERIFY_CONTRIBUTIONS)
			$rval = $this->verifyContributions();
		else if ($this->action == ROUND_ACTION_FAH_APP_NEW_WORKERS || $this->action == ROUND_ACTION_FAH_APP_VALIDATE_WORKERS || $this->action == ROUND_ACTION_FAH_APP_WEEK_STATS || $this->action == ROUND_ACTION_FAH_APP_DEEP_STATS || $this->action == ROUND_ACTION_FAH_APP_ADD_STATS)
			$rval = $this->fahAppStatSteps();
		else if ($this->action == ROUND_ACTION_TEST_FORFEIT)
			$rval = $this->checkForfeit(true /*testRun*/);
		else if ($this->action == ROUND_ACTION_CHECK_FORFEIT)
			$rval = $this->checkForfeit();
		else if ($this->action == ROUND_ACTION_WAIT_FUNDED)
			$rval = $this->waitFunded();
		else if ($this->action == ROUND_ACTION_CLEAR_APPROVED)
			$rval = $this->clearApproved(true /*incStep*/);
		else if ($this->action == ROUND_ACTION_WAIT_APPROVED)
			$rval = $this->waitApproved();
		else if ($this->action == ROUND_ACTION_CONTRIBUTIONS_REQ)
			$rval = $this->requestContributions();
		else if ($this->action == ROUND_ACTION_CONTRIBUTIONS_STEP)
			$rval = $this->stepContributions();
		else if ($this->action == ROUND_ACTION_CONTRIBUTIONS_TEST)
			$rval = $this->testContributions();
		else if ($this->action == ROUND_ACTION_CONTRIBUTIONS_UNTEST)
			$rval = $this->untestContributions();
		else if ($this->action == ROUND_ACTION_ACCUMULATE_STOREPAYS)
			$rval = $this->accumulateStoredPay();
		else if ($this->action == ROUND_ACTION_ACCUMULATE_STOREPAYS_TEST)
			$rval = $this->accumulateStoredPay(true /*testRun*/);
		else if ($this->action == ROUND_ACTION_SEND_PAYOUT)
			$rval = $this->sendPayout();
		else
		{
			XLogWarn("Round::step step $this->step has an unhandled action: ".XVarDump($this->action).", continuing with next step...");
			XLogDebug("Round::step actionStack: ".XVarDump($this->actionStack));
			if (!$this->incStep())
			{
				XLogError("Round::step (action unhandled) incStep failed");
				return false;
			}
			return true; // do nothing but warn, soft error
		}
		//------------------
		if ($rval === false)
		{
			$actionName = $this->actionName();
			XLogError("Round::step step $this->step, action $this->action ($actionName), failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function requestFahAppStats($reparse = false)
	{
		$FahAppClient = new FahAppClient() or die ("Create object failed");
		//------------------
		$retVal = $FahAppClient->pollTeam($this->teamId, $this->id, $reparse, true /*noRateLimitFail*/);
		if ($retVal === false)
		{
			XLogError("Round::requestFahAppStats FahAppClient pollTeam failed");
			return false;
		}
		//------------------
		if ($retVal === 0)
		{
			XLogWarn("Round::requestFahAppStats FahAppClient pollTeam was rate limitted, continuing...");
			return true; // return success, skip update Stats date and indcrement step
		}
		//------------------
		$this->dtStats = time();
		$this->incStep(false /*updateStep*/);
		//------------------
		if (!$this->Update())
		{
			XLogError("Round::requestFahAppStats update failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function verifyNewData()
	{
		//------------------
		$Monitor = new Monitor() or die('Create object failed');
		//------------------
		if (!$Monitor->PostNewDataAddValidate($this))
		{
			XLogError("Rounds::verifyNewData Monitor PostNewDataAddValidate failed");
			return false;
		}
		//---------------------------------
		if (!$this->incStep())
		{
			XLogError("Round::verifyNewData incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function verifyNewStats()
	{
		//------------------
		$Monitor = new Monitor() or die('Create object failed');
		//------------------
		if (!$Monitor->PostNewDataAddStatsValidate($this))
		{
			XLogError("Rounds::verifyNewStats Monitor PostNewDataAddStatsValidate failed");
			return false;
		}
		//---------------------------------
		if (!$this->incStep())
		{
			XLogError("Round::verifyNewStats incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function verifyContributions()
	{
		//------------------
		$Monitor = new Monitor() or die('Create object failed');
		//------------------
		if (!$Monitor->PostContributionsValidate($this))
		{
			XLogError("Rounds::verifyContributions Monitor PostContributionsValidate failed");
			return false;
		}
		//---------------------------------
		if (!$this->incStep())
		{
			XLogError("Round::verifyContributions incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function fahAppStatSteps()
	{
		//------------------
		$FahAppClient = new FahAppClient() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		$Contributions = new Contributions() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$passCountID = ($this->action - ROUND_ACTION_FAH_APP_NEW_WORKERS);
		$passCount = $Config->GetSetDefault(CFG_ROUND_PASSCOUNT_FAHAPP_STEP_BASE.$passCountID, DEF_ROUND_PASSCOUNT_FAHAPP);
		if ($passCount === false || !is_numeric($passCount))
		{
			XLogError("Round::fahAppStatSteps Config GetSetDefault passCount");
			return false;
		}
		$passCount = (int)$passCount;
		//------------------
		$passTimeout = $Config->GetSetDefault(CFG_ROUND_TIMEOUT_FAHAPP_STEP, DEF_ROUND_TIMEOUT_FAHAPP_STEP);
		if ($passTimeout === false || !is_numeric($passTimeout))
		{
			XLogError("Round::fahAppStatSteps Config GetSetDefault passTimeout failed");
			return false;
		}
		$passTimeoutMs = ((int)$passTimeout /*seconds*/) * 1000;
		//------------------
		$progData = $this->getProgress();
		if ($progData === false)
		{
			XLogError("Round::fahAppStatSteps getProgress failed");
			return false;
		}
		//------------------
		$progress    = (int)$progData[0];
		$progressMax = (int)$progData[1];
		//------------------
		$timeoutTimer = new XTimer();
		$numberOfPasses = 0;
		$handledCount = $passCount;
		while ($handledCount >= $passCount)
		{
			//------------------
			if ($timeoutTimer->elapsedMs() >= $passTimeoutMs)
			{
				//------------------
				XLogDebug("Round::fahAppStatSteps action ".$this->action." (".$this->actionName().") timed out after $numberOfPasses passes. took ".$timeoutTimer->elapsedMs(true));
				//------------------
				if (!$this->updateProgress($progress, $progressMax))
				{
					XLogError("Round::fahAppStatSteps updateProgress (timeout) failed");
					return false;
				}
				//------------------
				return true; // timed out, try next pass
			}
			//------------------
			if ($progressMax == 0)
			{
				//------------------
				$timer->start();
				//XLogDebug("Round::fahAppStatSteps new action, getting progress count...");
				switch ($this->action)
				{
					case ROUND_ACTION_FAH_APP_NEW_WORKERS:		$progressMax = $FahAppClient->getProcessNewWorkersCount(); break;
					case ROUND_ACTION_FAH_APP_VALIDATE_WORKERS:	$progressMax = $FahAppClient->getProcessValidateWorkersCount(); break;
					case ROUND_ACTION_FAH_APP_WEEK_STATS:		$progressMax = $FahAppClient->getProcessSimpleWeekStatsCount(); break;
					case ROUND_ACTION_FAH_APP_DEEP_STATS:		$progressMax = $FahAppClient->getProcessDeepSearchStatsCount(); break;
					case ROUND_ACTION_FAH_APP_ADD_STATS:		$progressMax = $FahAppClient->getProcessAddStatsCount(); break;
					default:
						XLogError("Round::fahAppStatSteps unexpected action: ".$this->action);
						return false;				
				} // switch
				//------------------
				if ($progressMax === false)
				{
					XLogError("Round::fahAppStatSteps action ".$this->action." (".$this->actionName().") FahAppClient getProcess Count failed");
					return false;
				}
				//------------------
				$progress = 0;
				if (!$this->updateProgress($progress, $progressMax))
				{
					XLogError("Round::fahAppStatSteps updateProgress (new action) failed");
					return false;
				}
				//------------------
				XLogDebug("Round::fahAppStatSteps got progressMax $progressMax for new action ".$this->action." (".$this->actionName()."). FahAppStatStep $passCountID, pass count $passCount. took ".$timer->elapsedMs(true).", starting work...");
				//------------------
			}
			//------------------
			$timer->start();
			switch ($this->action)
			{
				case ROUND_ACTION_FAH_APP_NEW_WORKERS:		$handledCount = $FahAppClient->processNewWorkers($passCount); break;
				case ROUND_ACTION_FAH_APP_VALIDATE_WORKERS:	$handledCount = $FahAppClient->processValidateWorkers($this->id, $passCount); break;
				case ROUND_ACTION_FAH_APP_WEEK_STATS:		$handledCount = $FahAppClient->processSimpleWeekStats($this->id, $passCount); break;
				case ROUND_ACTION_FAH_APP_DEEP_STATS:		$handledCount = $FahAppClient->processDeepSearchStats($this->id, $passCount); break;
				case ROUND_ACTION_FAH_APP_ADD_STATS:		$handledCount = $FahAppClient->processAddStats($this->id, $passCount); break;
				default:
					XLogError("Round::fahAppStatSteps unexpected state: ".$this->state);
					return false;				
			} // switch
			//------------------
			if ($handledCount === false)
			{
				XLogError("Round::fahAppStatSteps FahAppClient function failed. action ".$this->action." (".$this->actionName().")");
				return false;				
			}
			//------------------
			$progress += $handledCount;
			$numberOfPasses++;
			XLogDebug("Round::fahAppStatSteps pass handledCount ".XVarDump($handledCount)."/ $passCount in pass $numberOfPasses, progress $progress / $progressMax. took ".$timer->elapsedMs(true));
			//------------------
			if ($progress > $progressMax)
			{
				XLogError("Round::fahAppStatSteps action ".$this->action." (".$this->actionName().")'s progress has exceeded its progressMax");
				return false;				
			}
			//------------------
		} // while
		//------------------
		XLogDebug("Round::fahAppStatSteps while loop ".$this->action." (".$this->actionName().") ended after $numberOfPasses passes after ".$timeoutTimer->elapsedMs(true).", completed step");
		//------------------
		if ($this->action == ROUND_ACTION_FAH_APP_ADD_STATS)
		{
			//------------------
			$NewRoundData = new NewRoundData() or die ("Create object failed");
			$totalWork = $NewRoundData->getWeekPointsTotal();
			if ($totalWork === false)
			{
				XLogWarn("Round::fahAppStatSteps NewRoundData getWeekPointsTotal failed when all done. Setting Round totalWork to zero and continuing...");
				$this->totalWork = 0;
			}
			else
			{
				XLogNotify("Round::fahAppStatSteps New Round Data finished: active valid workers $totalWork[0], total week points $totalWork[1]");
				$this->totalWork = (int)$totalWork[1];
			}
			//------------------
		}
		//------------------
		$wasStep = $this->step;
		$this->incStep(false /*updateStep*/);
		//------------------
		if (!$this->Update()) // updating step and totalWork
		{
			XLogError("Round::requestFahAppStats update failed");
			return false;
		}
		//------------------
		XLogDebug("Round::fahAppStatSteps step $wasStep finished. took ".$timeoutTimer->elapsedMs(true));
		//------------------
		return true;
	}
	//------------------
	function checkFunded()
	{
		//------------------
		$funded = false;
		//------------------
		if ($this->hasFlag(ROUND_FLAG_DRYRUN))
			$funded = true; // fake payout, dry run, always funded
		else
		{
			//------------------
			$Wallet = new Wallet() or die ("Create object failed");
			//------------------
			$fullBalance = $Wallet->getBalance();
			if ($fullBalance === false)
			{
				XLogError("Round::checkFunded wallet failed to getBalance");
				return false;
			}
			//------------------
			$totalFees = $Wallet->getTotalFees();
			if ($totalFees === false)
			{
				XLogError("Round::checkFunded wallet failed to getTotalFees");
				return false;
			}
			//------------------
			$balance = bcsub($fullBalance, $totalFees, 8);
			//------------------
			if ((bccomp($this->totalPay, "0.0", 8) == 0 && bccomp($this->totalPay, "-0.0", 8) == 0) || bccomp($this->totalPay, $balance, 8) <= 0) // zero pay or balance is larger/equal
				$funded = true;
			//------------------
		}
		//------------------
		if ($funded)
		{
			if (!$this->setFlag(ROUND_FLAG_FUNDED))
			{
				XLogError("Round::checkFunded setFlag failed");
				return false;
			}
		}
		else
		{
			if (!$this->clearFlag(ROUND_FLAG_FUNDED))
			{
				XLogError("Round::checkFunded clearFlag failed");
				return false;
			}
		}
		//------------------
		return true;
	}
	//------------------
	function waitFunded()
	{
		//------------------
		if (!$this->checkFunded())
		{
			XLogError("Round::waitFunded checkFunded failed");
			return false;
		}
		//------------------
		if ($this->hasFlag(ROUND_FLAG_FUNDED))
			if (!$this->incStep())
			{
				XLogError("Round::waitFunded incStep failed");
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function clearApproved($incStep = false)
	{
		//------------------
		if (!$this->clearFlag(ROUND_FLAG_APPROVED))
		{
			XLogError("Round::clearApproved clearFlag failed");
			return false;
		}
		//------------------
		if ($incStep !== false && !$this->incStep())
		{
			XLogError("Round::clearApproved incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function setApproved($pushRoundAutomation = false)
	{
		//------------------
		if (!$this->setFlag(ROUND_FLAG_APPROVED))
		{
			XLogError("Round::setApproved setFlag failed");
			return false;
		}
		//------------------
		if ($pushRoundAutomation !== false)
		{
			$Automation = new Automation() or die("Create object failed");
			if (!$Automation->ClearRoundCheckNoAction())
			{
				XLogError("Round::setApproved Automation ClearRoundCheckNoAction failed");
				return false;
			}
		}
		//------------------
		return true;
	}
	//------------------
	function waitApproved()
	{
		//------------------
		if ($this->hasFlag(ROUND_FLAG_APPROVED))
			if (!$this->incStep())
			{
				XLogError("Round::waitApproved incStep failed");
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function requestContributions()
	{
		$Contributions = new Contributions() or die("Create object failed");
		//------------------
		if (!$Contributions->requestContributions($this->id))
		{
			XLogError("Round::requestContributions Contributions requestContributions failed");
			return false;
		}	
		//------------------
		if (!$this->incStep())
		{
			XLogError("Round::requestContributions incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function stepContributions()
	{
		$Contributions = new Contributions() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$progData = $this->getProgress();
		if ($progData === false)
		{
			XLogError("Round::stepContributions getProgress failed");
			return false;
		}
		//------------------
		$timer->start();
		$rval = $Contributions->stepContributions($this->id, false /*isTest*/, 40000 /*maxStepTimeMs*/);
		//------------------
		if ($rval === false)
		{
			XLogError("Round::stepContributions Contributions stepContributions failed");
			return false;
		}			
		//------------------
		XLogDebug("Round::stepContributions Contributions stepContributions took ".$timer->restartMs(true));
		//------------------
		if ($rval !== true)
		{
			//------------------
			if (!is_array($rval) || sizeof($rval) != 2)
			{
				XLogError("Round::stepContributions validate Contributions stepContributions reply failed: ".XVarDump($rval));
				return false;
			}
			//------------------
			if (!$this->updateProgress( $progData[0] + $rval[0], $rval[1]))
			{
				XLogError("Round::stepContributions updateProgress failed");
				return false;
			}
			//------------------
			return true; // success, but not done stepping
		}
		//------------------
		if (!$this->incStep())
		{
			XLogError("Round::stepContributions incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function testContributions()
	{
		$Contributions = new Contributions() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$progData = $this->getProgress();
		if ($progData === false)
		{
			XLogError("Round::testContributions getProgress failed");
			return false;
		}
		//------------------
		$timer->start();
		$rval = $Contributions->stepContributions($this->id, true /*isTest*/, 40000 /*maxStepTimeMs*/);
		if ($rval === false)
		{
			XLogError("Round::testContributions Contributions stepContributions failed");
			return false;
		}
		//------------------
		XLogDebug("Round::testContributions Contributions stepContributions took ".$timer->restartMs(true));
		//------------------
		if ($rval !== true) // not done
		{
			//------------------ 
			if (!is_array($rval) || sizeof($rval) != 2)
			{
				XLogError("Round::testContributions validate Contributions stepContributions reply failed: ".XVarDump($rval));
				return false;
			}
			//------------------
			if ($rval[0] == 0 && $progData[2 /*noChangecount*/] >= 2)
			{
				XLogError("Round::testContributions Contributions stepContributions hung on step ".$progData[0]."/".$progData[1]." for ".$progData[2]." tries, failing");
				return false;
			}
			//------------------
			if (!$this->updateProgress( $progData[0] + $rval[0], $rval[1]))
			{
				XLogError("Round::testContributions updateProgress failed");
				return false;
			}
			//------------------
		}
		else if (!$this->incStep()) // done
		{
			XLogError("Round::testContributions incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function untestContributions()
	{
		$Contributions = new Contributions() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		if (!$Contributions->cleanupRoundTestContributions($this->id))
		{
			XLogError("Round::untestContributions Contributions cleanupRoundTestContributions failed");
			return false;
		}
		//------------------
		XLogDebug("Round::untestContributions Contributions roundTestContributions took ".$timer->restartMs(true));
		//------------------
		if (!$this->incStep())
		{
			XLogError("Round::untestContributions incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function accumulateStoredPay($testRun = false)
	{
		global $db;
		//------------------
		$Payouts = new Payouts() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		$Wallet = new Wallet() or die("Create object failed");
		//------------------
		$storeAddress = $Wallet->getStoreAddress();
		if ($storeAddress === false || $storeAddress == "")
		{
			XLogError("Round::accumulateStoredPay Wallet getStoreAddress failed: ".XVarDump($storeAddress));
			return false;
		}
		//------------------
		$payMinimum = $Config->GetSetDefault(CFG_ROUND_PAY_MINIMUM, DEFAULT_ROUND_PAY_MINIMUM);
		if ($payMinimum === false)
		{
			XLogError("Round::accumulateStoredPay Config GetSetDefault pay minimum failed");
			return false;
		}
		//------------------
		$storeTxMinimum = $Config->GetSetDefault(CFG_ROUND_STORE_TX_MINIMUM, DEFAULT_ROUND_STORE_TX_MINIMUM);
		if ($storeTxMinimum === false)
		{
			XLogError("Round::accumulateStoredPay Config GetSetDefault storeTxMinimum failed");
			return false;
		}
		//------------------
		$storePayEnabled = $this->hasFlag(ROUND_FLAG_STORE_VALUES);
		//------------------
		$timer = new XTimer();
		//------------------
		$payoutList = $Payouts->findRoundPayouts($this->id, false /*orderBy default*/, false /*includePaid*/);
		if ($payoutList === false)
		{
			XLogError("Round::accumulateStoredPay Payouts failed to findRoundPayouts");
			return false;
		}
		//------------------
		XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."Payouts findRoundPayouts, found ".sizeof($payoutList)." and took ".$timer->restartMs(true)); 
		//------------------
		$workerStoreList = array();
		$doStorePay = false;
		$payFromStoreTotal = "0";
		$totalStorePayCount = 0;
		$workerStorePayCount = 0;
		//------------------
		if (!$storePayEnabled)
			XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."store pay not enabled, skipping first pass");
		else
		{
			//------------------
			foreach ($payoutList as $payout)
			{
				//------------------
				if (isset($workerStoreList[$payout->workerIdx]))
				{
					XLogError("Round::accumulateStoredPay multiple payouts found for worker ".$payout->workerIdx.", current payout ".$payout->id.", previous payout ".(is_array($workerStoreList[$payout->workerIdx]) && is_object($workerStoreList[$payout->workerIdx]['p']) ? $workerStoreList[$payout->workerIdx]['p']->id : "Bad entry: ".XVarDump($workerStoreList[$payout->workerIdx])).", validation failure");
					return false;
				}
				//------------------
				// payoutID, statPay, dtStored
				$storeList = $Payouts->findWorkerStoredPayoutSummaryList($payout->workerIdx);
				if ($storeList === false)
				{
					XLogError("Round::accumulateStoredPay Payouts findWorkerStoredPayoutSummaryList failed for payoutIdx ".$payout->id.", workerIdx ".$payout->workerIdx);
					return false;
				}
				//------------------
				$payout->pay = $payout->statPay;
				//------------------
				$storeListSize = sizeof($storeList);
				if ($storeListSize != 0)
				{
					//------------------
					$totalStorePay = "0.0";
					$payTotal = "0.0";
					$storePayCount = 0;
					//------------------
					foreach ($storeList as $store)
					{
						$totalStorePay = bcadd($totalStorePay, $store[1 /*storePay*/], 8);
						$storePayCount++;
					}
					//------------------
					$payTotal = bcadd($totalStorePay, $payout->statPay, 8);
					//------------------
					if (bccomp($payTotal, $payMinimum, 8) >= 0) // now pay above payMinimum
					{
						//------------------
						$workerStorePayCount++;
						$totalStorePayCount += $storePayCount;
						$payFromStoreTotal = bcadd($payFromStoreTotal, $totalStorePay, 8);
						$workerStoreList[$payout->workerIdx] = array('p' => $payout, 'sList' => $storeList, 'sListSize' => $storeListSize, 'payTotal' => $payTotal, 'totalStorePay' => $totalStorePay);
						//------------------
					}
					else $workerStoreList[$payout->workerIdx] = array('p' => $payout, 'sListSize' => 0);
					//------------------
				} // if (sizeof($storeList) != 0)
				else $workerStoreList[$payout->workerIdx] = array('p' => $payout, 'sListSize' => 0);
				//------------------
				//XLogDebug("Round::accumulateStoredPay payout $payout->id, worker $payout->workerIdx, stored count ".sizeof($storeList).", totalStorePay $totalStorePay, $payout->statPay, new total $payTotal");
				//------------------
			} // foreach ($payoutList as $payout)		
			//------------------
			XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."first pass of ".sizeof($payoutList)." payouts, took ".$timer->restartMs(true)); 
			//------------------
			if (bccomp($payFromStoreTotal, $storeTxMinimum, 8) >= 0) // store pay above storeTxMinimum to do a store pay
			{
				$doStorePay = true;
				XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."payFromStoreTotal: $payFromStoreTotal / storeTxMinimum: $storeTxMinimum, of storePayCount: $totalStorePayCount, for workers $workerStorePayCount / ".sizeof($workerStoreList).", payMinimum: $payMinimum, do store pay: ".XVarDump($doStorePay));
			}
			else
			{
				$doStorePay = false;
				XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."pay from store store too low. payFromStoreTotal: $payFromStoreTotal / storeTxMinimum: $storeTxMinimum, of storePayCount: $totalStorePayCount, for workers $workerStorePayCount / ".sizeof($workerStoreList).", payMinimum: $payMinimum, do store pay: ".XVarDump($doStorePay));
				$totalStorePayCount = 0;
				$payFromStoreTotal = "0";
			}
			//------------------
		} // if storePayEnabled
		//------------------
		if (sizeof($workerStoreList) != 0 && $doStorePay === true)
			XLogRaw(XLOG_LEVEL_DEBUG, "Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."storePay (from store) count tested ".sizeof($workerStoreList).":\npayout/worker paying (total_from_store) stored_value->stored_payout, [...]\n");
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("Round::accumulateStoredPay db beginTransaction failed");
			return false;
		}
		//------------------
		$roundTotalStatPay = "0";
		$payToStoreTotal = "0";
		foreach ($workerStoreList as $wid => $workerStore)
		{
			//------------------
			$payout = $workerStore['p'];
			//------------------
			if ($doStorePay && $workerStore['sListSize'] != 0)
			{
				//------------------
				$payout->pay = $workerStore['payTotal'];
				$payout->storePay = $workerStore['totalStorePay'];
				//------------------
				$dbgPays = '';
				foreach ($workerStore['sList'] as $store)
				{
					$dbgPays .= $store[1 /*storePay*/].'->'.$store[0].', ';
					if (!$testRun)
						if (!$Payouts->updateStorePayoutID($store[0], $payout->id))
						{
							XLogError("Round::accumulateStoredPay Payouts updateStorePayoutID failed for payoutIdx ".XVarDump($store[0]).", storePayoutID $payout->id");
							return false;
						}
				}
				//------------------
				if ($dbgPays != '')
					XLogRaw(XLOG_LEVEL_DEBUG, ($testRun ? "[TestRun] " : "")."$payout->id/$wid paying ($payout->storePay) $dbgPays\r\n", true /*leaveopen*/);
				//------------------
			} // if ($doStorePay && $workerStore['sListSize'] != 0)
			else $payout->storePay = "0";
			//------------------
			if (bccomp($payout->pay, $payMinimum, 8) < 0) // pay below payMinimum
			{
				//------------------
				//if ($storeListSize !== false && $storeListSize != 0)
					//XLogRaw(XLOG_LEVEL_DEBUG, $payout->id.'/'.$payout->workerIdx.' '.$storeListSize.'x'.((float)$totalStorePay)."+$payout->statPay=".((float)$payTotal).",too low\r\n", true /*leaveopen*/);
				//------------------
				$payToStoreTotal = bcadd($payToStoreTotal, $payout->pay, 8);
				$payout->pay = "0";
				$payout->dtStored = time();
				//------------------
			}
			else $payout->dtStored = false;
			//------------------
			$roundTotalStatPay = bcadd($roundTotalStatPay, $payout->statPay, 8);
			//------------------
			if (!$payout->Update())
			{
				XLogError("Round::accumulateStoredPay payout Update failed for payoutIdx ".$payout->id);
				return false;
			}
			//------------------
		} // foreach ($payoutList as $payout)		
		//------------------
		if (!$db->commit())
		{
			XLogError("Round::accumulateStoredPay db commit failed");
			return false;
		}
		//------------------
		XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."second pass of ".sizeof($workerStoreList)." worker payouts, payFromStoreTotal $payFromStoreTotal, payToStoreTotal $payToStoreTotal, roundTotalStatPay $roundTotalStatPay, took ".$timer->restartMs(true)); 
		//------------------
		$this->payStored = $this->storePay = "0.0";
		//------------------
		if (bccomp($payToStoreTotal, "0.0", 8) != 0 && bccomp($payToStoreTotal, "-0.0", 8) != 0)
		{
			//------------------
			$storePayPadding = 0.0;
			$paddedPayToStoreTotal = $payToStoreTotal;
			if (bccomp($payToStoreTotal, $payMinimum, 8) < 0) // pay to store needed but below payMinimum, storePay it so we can payStore
			{
				//------------------
				$storePayPadding = bcsub($payMinimum, $payToStoreTotal, 8);
				$paddedPayToStoreTotal = bcadd($payToStoreTotal, $storePayPadding, 8);
				$paddedPayFromStoreTotal = bcadd($payFromStoreTotal, $storePayPadding, 8);
				//------------------
				if (bccomp($paddedPayFromStoreTotal, $storeTxMinimum, 8) < 0) // store pay above storeTxMinimum to do a store pay
				{
					XLogDebug("Round::accumulateStoredPay paddedPayFromStoreTotal $paddedPayFromStoreTotal is below storeTxMinimum $storeTxMinimum. Adding further padding...");
					$storePayPadding = bcadd($storePayPadding, bcsub($storeTxMinimum, $paddedPayFromStoreTotal, 8), 8);
					$paddedPayToStoreTotal = bcadd($payToStoreTotal, $storePayPadding, 8);
					$paddedPayFromStoreTotal = bcadd($payFromStoreTotal, $storePayPadding, 8);
				}
				//------------------
				$doStorePay = true;
				$payFromStoreTotal = $paddedPayFromStoreTotal;
				XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."payToStoreTotal $payToStoreTotal below payMinimum $payMinimum. Adding $storePayPadding padding from store.  payToStoreTotal $payToStoreTotal, paddedPayToStoreTotal $paddedPayToStoreTotal, storePayPadding $storePayPadding");
				//------------------
			}
			//------------------
			if (!$Payouts->addPayout($this->id, WORKER_SPECIAL_STORE /*workerIdx*/, $storeAddress, $payToStoreTotal /*statPay*/,  $paddedPayToStoreTotal /*pay*/, $storePayPadding /*storePay*/))
			{
				XLogError("Round::accumulateStoredPay Payouts addPayout for pay to store failed");
				return false;
			}
			//------------------
			$this->payStored = $payToStoreTotal;
			//------------------
		}
		//------------------
		if ($doStorePay && bccomp($payFromStoreTotal, "0.0", 8) != 0 && bccomp($payFromStoreTotal, "-0.0", 8) != 0)
		{
			$Contributions = new Contributions() or die("Create object failed");
			//------------------
			if (!$Contributions->addContribution($this->id, 9999 /*order*/, "Store Pay", $storeAddress, CONT_MODE_SPECIAL_STORE_PAY, CONT_FLAG_WAIT_FOR_FUNDS, $payFromStoreTotal, 0.0 /*subvalue*/, 0 /*countvalue*/, false /*total*/, $workerStorePayCount /*count*/))
			{
				XLogError("Round::accumulateStoredPay Contributions addContribution for store pay failed");
				return false; 
			}
			//------------------
			$this->storePay = $payFromStoreTotal;
			//------------------
		}
		//------------------
		XLogDebug("Round::accumulateStoredPay ".($testRun ? "[TestRun] " : "")."storePay (third part) took ".$timer->restartMs(true)); 
		//------------------
		$this->totalPay = $roundTotalStatPay;
		$this->incStep(false /*updateStep*/);
		//------------------
		if (!$this->Update())
		{
			XLogError("Round::accumulateStoredPay update failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function checkForfeit($testRun = false)
	{
		//------------------
		if (!$this->hasFlag(ROUND_FLAG_FORFEIT_STORE))
		{
			XLogDebug("Round::checkForfeit ".($testRun ? '[Test Run] ' : '')."forfeit store Round flag is not enabled, skipping...");
			if (!$this->incStep())
			{
				XLogError("Round::checkForfeit incStep (skip) failed");
				return false;
			}
			return true;
		}
		//------------------
		$Config = new Config() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		//------------------
		$timer = new XTimer();
		//------------------
		$summary = $Rounds->getForfeitPayoutSummary($this->dtStarted /*dtNow*/);
		if ($summary === false)
		{
			XLogError("Round::checkForfeit Rounds getForfeitPayoutSummary failed");
			return false;
		}
		//------------------
		XLogDebug("Round::checkForfeit ".($testRun ? "TEST " : "")."Rounds getForfeitPayoutSummary took ".$timer->restartMs(true)); 
		$txtSummary = $Rounds->printForfeitPayoutSummary($summary);
		if ($txtSummary === false)
		{
			XLogError("Round::forfeitStorePays Rounds printForfeitPayoutSummary failed");
			return false;
		}
		XLogNotify($txtSummary);
		//------------------
		if ($summary['totals']['payouts'] == 0)
			XLogNotify("Rounds::checkForfeit ".($testRun ? "TEST " : "")."no forfeit payouts found.");
		else
		{
			//------------------
			$storeTxMinimum = $Config->GetSetDefault(CFG_ROUND_STORE_TX_MINIMUM, DEFAULT_ROUND_STORE_TX_MINIMUM);
			if ($storeTxMinimum === false)
			{
				XLogError("Round::checkForfeit Config GetSetDefault storeTxMinimum failed");
				return false;
			}
			//------------------
			$payMinimum = $Config->GetSetDefault(CFG_ROUND_PAY_MINIMUM, DEFAULT_ROUND_PAY_MINIMUM);
			if ($payMinimum === false)
			{
				XLogError("Round::checkForfeit Config GetSetDefault pay minimum failed");
				return false;
			}
			//------------------
			if (bccomp($summary['totals']['value'], $storeTxMinimum, 8) < 0) // forfeitTotal above storeTxMinimum to do a store forfeit transaction
				XLogNotify("Rounds::checkForfeit forfeit total value ".$summary['totals']['value']." doesn't meet storeTxMimimum $storeTxMinimum, skipping forfeit.");
			else
			{
				//------------------
				$Contributions = new Contributions() or die ("Create object failed");
				$Payouts = new Payouts() or die ("Create object failed");
				$Wallet = new Wallet() or die ("Create object failed");
				//------------------
				$mainAddress = $Wallet->getMainAddress();
				if ($mainAddress === false || $mainAddress == "")
				{
					XLogError("Round::checkForfeit Wallet getMainAddress failed: ".XVarDump($mainAddress));
					return false;
				}
				//------------------
				$storeAddress = $Wallet->getStoreAddress();
				if ($storeAddress === false || $storeAddress == "")
				{
					XLogError("Round::checkForfeit Wallet getStoreAddress failed: ".XVarDump($storeAddress));
					return false;
				}
				//------------------
				if (!$Contributions->addContribution($this->id, 0 /*order*/, "Forfeit From Store", $storeAddress, CONT_MODE_SPECIAL_STORE_FORFEIT /*mode*/, CONT_FLAG_WAIT_FOR_FUNDS /*flags*/, $summary['totals']['value'] /*value*/, 0.0 /*subvalue*/, 0 /*countvalue*/, false /*total*/, $summary['totals']['workers'] /*count*/))
				{
					XLogError("Round::checkForfeit Contributions addContribution failed");
					return false;
				}
				//------------------
				if (!$testRun)
				{
					//------------------
					XLogNotify("Round::checkForfeit Round ".$this->id." ".($testRun ? "TEST " : "")."marking payouts forfeited for ".$summary['totals']['payouts']." payouts, from ".$summary['totals']['workers']." workers, totaling ".$summary['totals']['value']);
					//------------------
					foreach ($summary as $widx => $wdata)
						if (is_numeric($widx))
						{
							//------------------
							$payoutID = $Payouts->addPayout($this->id, WORKER_SPECIAL_MAIN /*workerIdx for forfeit to main*/, $mainAddress, $widx /*statPay*/, $wdata['total'] /*pay*/, $wdata['total'] /*storePay*/, false /*dtPaid*/, false /*txid*/, true /*returnID*/);
							if ($payoutID === false || !is_numeric($payoutID))
							{
								XLogError("Round::checkForfeit Payouts addPayout failed for workerIdx: $widx, result: ".XVarDump($payoutID));
								return false;
							}
							//------------------
							XLogRaw(XLOG_LEVEL_NOTIFY, 'worker '.$widx.' total '.$wdata['total'].' from '.sizeof($wdata['payouts']).' payouts forfeited to payout '.$payoutID."\r\n", true /*leaveopen*/);
							//------------------
							foreach ($wdata['payouts'] as $pdata)
							{
								//------------------
								$payout = $pdata['payout'];
								//XLogDebug("Round::checkForfeit updating storePayoutID to $payoutID for payout ".$payout->id.", for worker $widx");
								if (!$Payouts->updateStorePayoutID($payout->id, $payoutID))
								{
									XLogError("Round::checkForfeit Payouts updateStorePayoutID failed for workerIdx: $widx, payout id: ".XVarDump($payout->id).", to be paid by payout ID: ".XVarDump($payoutID));
									return false;
								}
								//------------------
							} // foreach $wdata['payouts']
							//------------------
						}
					//------------------
					XLogDebug("Round::checkForfeit added forfeit storePayout and ".($testRun ? "TEST " : "")."updated paid stored value payouts took ".$timer->restartMs(true));
					//------------------
				} // if (!$testRun)
				//------------------
			}
			//------------------
		} // else // if ($dtStoreMaxDate === true) // true means disabled
		//------------------
		if (!$this->incStep())
		{
			XLogError("Round::checkForfeit incStep failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function sendPayout()
	{
		global $db; // for beginTransaction/commit
		//------------------
		$Payouts = new Payouts() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		//------------------
		$payMinimum = $Config->Get(CFG_ROUND_PAY_MINIMUM);
		if ($payMinimum === false)
		{
			XLogError("Rounds::sendPayout Config get pay minimum failed");
			return false;
		}
		//------------------
		$timer = new XTimer();
		$payoutList = $Payouts->findRoundPayouts($this->id, DB_PAYOUT_WORKER." DESC" /*orderBy (special last)*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, true /*includeSpecialWorkers*/);
		if ($payoutList === false)
		{
			XLogError("Round::sendPayout Payouts failed to findRoundPayouts");
			return false;
		}
		//------------------
		XLogDebug("Round::sendPayout Payouts findRoundPayouts took ".$timer->restartMs(true));
		//------------------
		$payoutToStore = false;
		$storeAddress = ""; // back scope off for payout creation, when finished
		//------------------
		$dupePayCount = 0;
		$dupeStoreCount = 0;
		$dupeCrossCount = 0;
		$toStoreCount = 0;
		$toStoreTotal = "0";
		$fromStoreCount = 0;
		$fromStoreTotal = "0";
		$total = "0";
		$addressStoredPayouts = array();
		$addressValues = array();
		XLogDebug("Round::sendPayout processing address values...");
		foreach ($payoutList as $payout)
		{
			if ($payout->dtStored !== false)
			{
				if (!isset($addressStoredPayouts[$payout->address]))
					$addressStoredPayouts[$payout->address] = $payout->id;
				else 
				{
					XLogWarn("Round::sendPayout stored payout ".$payout->id.", address '".$payout->address."', already has stored value with payout id ".$addressStoredPayouts[$payout->address]);
					$dupeStoreCount++;
				}
				if (isset($addressValues[$payout->address]))
				{
					XLogWarn("Round::sendPayout stored payout ".$payout->id.", address '".$payout->address."', already has at least one paying payout");
					$dupeCrossCount++;
				}
				$toStoreTotal = bcadd($toStoreTotal, $payout->statPay, 8);
				XLogRaw(XLOG_LEVEL_DEBUG, $payout->id.','.$payout->workerIdx.',0,'.$payout->statPay."\r\n", true /*leaveopen*/);
				$toStoreCount++;
			}
			else
			{
				if (bccomp($payout->pay, "0", 8) > 0 || bccomp($payout->pay, "-0", 8) > 0)
				{
					if (isset($addressValues[$payout->address])) // duplicate addresses may cause send to fail
					{
						XLogWarn("Round::sendPayout merging payout to address '".$payout->address."' already has pay of '".$addressValues[$payout->address]."', but adding '$payout->pay' more for payout $payout->id");
						$addressValues[$payout->address] = round(bcadd($addressValues[$payout->address], $payout->pay, 8), 8);
					}
					else 
					{
						XLogRaw(XLOG_LEVEL_DEBUG, $payout->id.','.$payout->workerIdx.','.$payout->pay.','.$payout->statPay."\r\n", true /*leaveopen*/);
						$addressValues[$payout->address] = round(bcadd($payout->pay, "0.0", 8), 8); // trim to 8 decimal points by adding zero
					}
					if ($payout->workerIdx != WORKER_SPECIAL_STORE) // don't count store pay towards total, but it might be padded with storePay
						$total = bcadd($total, $payout->pay, 8);
				}
				if (bccomp($payout->storePay, "0", 8) > 0 || bccomp($payout->storePay, "-0", 8) > 0)
				{
					if (isset($addressStoredPayouts[$payout->address]))
					{
						XLogWarn("Round::sendPayout paying payout ".$payout->id.", address '".$payout->address."', already has stored value with payout id ".$addressStoredPayouts[$payout->address]);
						$dupeCrossCount++;
					}
					$fromStoreCount++;
					$fromStoreTotal = bcadd($fromStoreTotal, $payout->storePay, 8);
				}
			}
		}
		//------------------
		XLogDebug("Round::sendPayout process address values and totals took ".$timer->restartMs(true));
		//------------------
		if ($dupePayCount != 0 || $dupeStoreCount != 0 || $dupeCrossCount != 0)
		{
			$Monitor = new Monitor() or die('Create object failed');
			$msg = 'Round payout found duplicates addresses for: ';
			if ($dupePayCount != 0)
				$msg .= $dupePayCount.' payouts ';
			if ($dupeStoreCount != 0)
				$msg .= $dupeStoreCount.' stored ';
			if ($dupePayCount != 0)
				$msg .= $dupeCrossCount.' pay/store crossed';
			$details = $Monitor->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'Round sendPayout', MONEVENT_DETAIL_FIELD => 'Address pay/store lists', MONEVENT_DETAIL_COUNT => sizeof($addressValues), MONEVENT_DETAIL_NOTE => $msg));
			$retVal = $Monitor->addUpdateEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_DATA_INVALID, $details, $details /*requireDetails*/);
			if ($retVal === false)
			{
				XLogError("Round::sendPayout Monitor addUpdateEvent duplicate pay/store values failed");
				return false;
			}
			if ($retVal === true)
			{
				XLogWarn("Round::sendPayout Monitor event is blocking for duplicate pay/store values");
				return true;
			}
			XLogWarn("Round::sendPayout Monitor event blocking for duplicate pay/store values has been resolved, despite the issue persisting, continuing...");
		}
		//------------------
		$outputTotal = bcadd($total, $toStoreTotal, 8);
		$adjustedTotal = bcsub($outputTotal, $fromStoreTotal, 8);
		if (bccomp($this->totalPay, $adjustedTotal, 8) != 0 || bccomp($toStoreTotal, $this->payStored, 8) != 0)
		{
			XLogError("Round::sendPayout payout totals don't match. Payouts from list total (adjusted) ".XVarDump($adjustedTotal).", but request total was ".XVarDump($this->totalPay).". Total out counted $total, total to store total $toStoreTotal. Previously recorded payStored: ".XVarDump($this->payStored).", from store total $fromStoreTotal. Previously recorded storePay: ".XVarDump($this->storePay));
			$Monitor = new Monitor() or die('Create object failed');
			$msg = "Round payout adjusted total $adjustedTotal / expected $this->totalPay ($total + $toStoreTotal - $fromStoreTotal / $this->storePay - $this->payStored)";
			$details = $Monitor->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'Round sendPayout', MONEVENT_DETAIL_FIELD => 'Pay/store Totals', MONEVENT_DETAIL_VALUE => $adjustedTotal, MONEVENT_DETAIL_NEW_VALUE => $this->totalPay, MONEVENT_DETAIL_NOTE => $msg));
			$retVal = $Monitor->addUpdateEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_DATA_INVALID, $details, $details /*requireDetails*/);
			if ($retVal === false)
			{
				XLogError("Round::sendPayout Monitor addUpdateEvent total pay/store values failed");
				return false;
			}
			if ($retVal === true)
			{
				XLogWarn("Round::sendPayout Monitor event is blocking for total pay/payStored values not matching expected values");
				return true;
			}
			XLogWarn("Round::sendPayout Monitor event blocking for total pay/payStored values not matching expected values has been resolved, despite the issue persisting, continuing...");
		}
		//------------------
		XLogNotify("Round::sendPayout Payouts from list total (adjusted) ".XVarDump($adjustedTotal).", request total was ".XVarDump($this->totalPay).". Total out counted $total, total to store total $toStoreTotal. Previously recorded payStored: ".XVarDump($this->payStored).", from store total $fromStoreTotal. Previously recorded storePay: ".XVarDump($this->storePay));
		//------------------
		if (bccomp($outputTotal, "0.0", 8) <= 0) // zero total payout output 
		{
			//------------------
			XLogWarn("Round::sendPayout Round $this->id total pay out determined to be none/zero. outputTotal (list total + to store): $outputTotal, (Payouts from list total: ".XVarDump($total)." (toStoreTotal: $toStoreTotal), payout from request total:".XVarDump($this->totalPay).", adjustedTotal $adjustedTotal) Payout List: ".XVarDump($payoutList));
			//------------------
			$Monitor = new Monitor() or die('Create object failed');
			$msg = 'Round payout determined to be zero (no transaction outputs)';
			$details = $Monitor->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'Round sendPayout', MONEVENT_DETAIL_FIELD => 'No outputs', MONEVENT_DETAIL_NOTE => $msg));
			$retVal = $Monitor->addUpdateEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_DATA_INVALID, $details, $details /*requireDetails*/);
			if ($retVal === false)
			{
				XLogError("Round::sendPayout Monitor addUpdateEvent zero outputs failed");
				return false;
			}
			if ($retVal === true)
			{
				XLogWarn("Round::sendPayout Monitor event is blocking for zero outputs");
				return true;
			}
			//------------------
			XLogWarn("Round::sendPayout Monitor event blocking for zero payout outputs has been resolved, despite the issue persisting. Marking round complete, skipping transaction, and continuing to mark payouts as paid...");
			$txid = "[NONE]";
			//------------------
		}
		else // Payout has output to pay
		{
			//------------------
			if (sizeof($payoutList) == 0 && $toStoreTotal == 0)
			{
				XLogError("Round::sendPayout Round $this->id total pay present, but no payouts listed. (Payouts from list total: ".XVarDump($total).", payout from request total:".XVarDump($this->totalPay).", toStore count: $toStoreTotal, toStoreTotal $toStoreTotal, adjustedTotal $adjustedTotal) Marking round complete and returning success. Payout List: ".XVarDump($payoutList));
				return false;
			}
			//------------------
			$Wallet = new Wallet() or die ("Create object failed");
			//------------------
			$mainAddress = $Wallet->getMainAddress();
			if ($mainAddress === false || $mainAddress == "")
			{
				XLogError("Round::sendPayout Wallet getMainAddress failed: ".XVarDump($mainAddress));
				return false;
			}
			//------------------
			$storeAddress = $Wallet->getStoreAddress();
			if ($storeAddress === false || $storeAddress == "")
			{
				XLogError("Round::sendPayout Wallet getStoreAddress failed: ".XVarDump($storeAddress));
				return false;
			}
			//------------------
			$extraFee = $Wallet->getExtraFee();
			if ($extraFee === false)
			{
				XLogError("Round::sendPayout Wallet getExtraFee failed: ".XVarDump($extraFee));
				return false;
			}
			//------------------
			if (!is_numeric($extraFee))
				$extraFee = "0";
			//------------------
			if ($toStoreCount != 0 && bccomp($toStoreTotal, "0.0", 8) != 0 && bccomp($toStoreTotal, "-0.0", 8) != 0)
			{
				XLogNotify("Rounds::sendPayout included $toStoreCount stored payouts, totaling '$toStoreTotal', to store Address '$storeAddress'");
				$payoutToStore = true;
			}
			//------------------
			if (is_numeric($this->id) && is_string($this->dtStarted))
				$txComment = BRAND_TX_COMMENT."$this->id $this->dtStarted";
			else
				$txComment = "";
			//------------------
			if ($this->hasFlag(ROUND_FLAG_DRYRUN))
			{
				XLogWarn("Round::sendPayout Round $this->id set to dry run, FAKING PAYOUT!");
				XLogDebug("Round::sendPayout Round $this->id, dry run, transaction dump:");
				XLogDebug("   mainAddress: $mainAddress");
				XLogDebug("   storeAddress: $storeAddress");
				XLogDebug("   store count: $toStoreCount, store total: $toStoreTotal / from store count: $fromStoreCount, from store total: $fromStoreTotal");
				XLogDebug(XVarDump($addressValues));
				$txid = $Wallet->sendTransaction($mainAddress, $mainAddress, $addressValues, true /*testOnly*/, "0" /*fee*/, "0" /*change*/, $extraFee);
				if ($txid === false)
				{
					XLogWarn("Round::sendPayout wallet failed to sendTransaction (Dry Run)");
					XLogWarn("Round::sendPayout wallet sendTxResult: ".XVarDump($Wallet->sendTxResult));
					$Monitor = new Monitor() or die('Create object failed');
					if (!$Monitor->TransactionFailed($Wallet->sendTxResult, $Wallet->sendTxLastStep, 'Round Send Payout (Dry Run)', false/*isAlarm*/))
					{
						XLogError("Round::sendPayout Monitor TransactionFailed failed after Wallet sendTransaction (Dry Run) failed");
						return false;
					}
				}
				$txid = "[RND-$this->id]";
			}
			else $txid = $Wallet->sendTransaction($mainAddress, $mainAddress, $addressValues, false /*testOnly*/, "0" /*fee*/, "0" /*change*/, $extraFee);
			//------------------
			if ($txid === false)
			{
				XLogError("Round::sendPayout wallet failed to sendTransaction");
				XLogWarn("Round::sendPayout wallet sendTxResult: ".XVarDump($Wallet->sendTxResult));
				$Monitor = new Monitor() or die('Create object failed');
				if (!$Monitor->TransactionFailed($Wallet->sendTxResult, $Wallet->sendTxLastStep, 'Round Send Payout', true /*isAlarm*/))
					XLogError("Round::sendPayout wallet Monitor TransactionFailed failed");
				return false;
			}
			//------------------
			$Config = new Config() or die("Create object failed");
			$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
			if (!$Config->Set(CFG_ROUND_LAST_TRANSACTION, $nowUtc->format(MYSQL_DATETIME_FORMAT)))
				XLogError("Rounds::sendPayout Config failed to set last transaction time (continuing).");
			//------------------
		}
		//------------------ Payout completed successfully, update Round and payouts (if any)
		XLogDebug("Round::sendPayout send transaction took ".$timer->restartMs(true));
		//------------------
		$retValue = true;
		$this->dtPaid = time();
		//------------------
		if (!$this->Update())
		{
			XLogError("Round::sendPayout update failed");
			$retValue = false;
		}
		//------------------
		XLogDebug("Round::sendPayout marking payouts (and their storepays) as paid...");
		$timer->start();
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("Round::sendPayout db beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($payoutList as $payout)
		{
			//------------------
			if ($payout->dtStored === false) // Only mark paid if not a stored value, but always set txid (store txid when stored value)
				$payout->dtPaid = $this->dtPaid;
			//------------------
			$payout->txid = $txid;
			//------------------
			if (!$payout->Update())
			{
				XLogError("Round::sendPayout payout update failed");
				$retValue = false;
			}
			//------------------
			if ($payout->dtStored === false && bccomp($payout->storePay, "0.0", 8) != 0 && bccomp($payout->storePay, "-0.0", 8) != 0)
			{
				XLogRaw(XLOG_LEVEL_DEBUG, $payout->id.'/'.$payout->workerIdx.' storepay '.$payout->storePay."\r\n", true /*leaveopen*/);				
				if (!$Payouts->updateStorePaidPayouts($payout->id, $this->dtPaid))
				{
					XLogError("Round::sendPayout Payouts updateStorePaidPayouts failed");
					$retValue = false;
				}
			}
			//------------------
		} // foreach $payoutList
		//------------------
		if (!$db->commit())
		{
			XLogError("Round::sendPayout db commit failed");
			return false;
		}
		//------------------
		XLogDebug("Round::sendPayout update ".sizeof($payoutList)." payouts and their storePays took ".$timer->restartMs(true));
		//------------------
		if (!$this->incStep())
		{
			XLogError("Round::sendPayout incStep failed");
			return false;
		}
		//------------------
		return $retValue;
	} // function sendPayout
	//------------------
	function unpayPayouts($includeSpecial = false, $backupPayoutsTable = true)
	{
		$payouts = new Payouts() or die ("Create object failed");
		//------------------
		if ($backupPayoutsTable)
		{
			$backupTableName = $payouts->backupTable('rnd_'.$this->id.'_unpay', false /*failExists*/, true /*clobber*/);
			if ($backupTableName !== false)
				XLogNotify("Round::unpayPayouts backed up Payouts table to: $backupTableName");
			else
			{
				XLogError("Round::unpayPayouts payouts backupTable");
				return false;
			}
		}
		//------------------
		$payoutList = $payouts->findRoundPayouts($this->id, false /*orderBy default*/, true /*includePaid*/, false /*limit*/, false /*workerIdx*/, $includeSpecial/*includeSpecialWorkers*/);
		if ($payoutList === false)
		{
			XLogError("Round::unpayPayouts payouts failed to findRoundPayouts");
			return false;
		}
		//------------------
		foreach ($payoutList as $payout)
		{
			//------------------
			if (bccomp($payout->storePay, "0.0", 8) != 0 && bccomp($payout->storePay, "-0.0", 8) != 0)
				if (!$payouts->unpayStorePaidPayouts($payout->id))
				{
					XLogError("Round::unpayPayouts payouts unpayStorePaidPayouts failed");
					$retValue = false;
				}
			//------------------
			$payout->dtPaid = false;
			$payout->txid = false;
			//------------------
			if (!$payout->Update())
			{
				XLogError("Round::unpayPayouts payout update failed");
				$retValue = false;
			}
			//------------------
		}
		//------------------
		return true;		
	}
	//------------------
	function getCountSummary($includeTxId = false)
	{
		global $db;
		//------------------
		$rid = $this->id;
		$sql = "(SELECT COUNT(".DB_STATS_ID.") FROM ".DB_STATS." WHERE ".DB_STATS_ROUND."=$rid) ";
		$sql .= "UNION ALL (SELECT COUNT(".DB_STATS_ID.") FROM ".DB_STATS." WHERE ".DB_STATS_ROUND."=$rid AND ".DB_STATS_WEEK_POINTS." IS NOT NULL AND ".DB_STATS_WEEK_POINTS.">0) ";
		$sql .= "UNION ALL (SELECT COUNT(".DB_PAYOUT_ID.") FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ROUND."=$rid AND ".DB_PAYOUT_WORKER.">=0 AND ".DB_PAYOUT_STAT_PAY." IS NOT NULL AND ".DB_PAYOUT_STAT_PAY.">0) ";
		$sql .= "UNION ALL (SELECT COUNT(".DB_PAYOUT_ID.") FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ROUND."=$rid AND ".DB_PAYOUT_WORKER.">=0 AND ".DB_PAYOUT_PAY." IS NOT NULL AND ".DB_PAYOUT_PAY.">0) ";
		if ($includeTxId)
			$sql .= "UNION ALL (SELECT ".DB_PAYOUT_TXID." FROM ".DB_PAYOUT." WHERE ".DB_PAYOUT_ROUND."=$rid AND ".DB_PAYOUT_TXID." IS NOT NULL AND ".DB_PAYOUT_TXID."!='' ORDER BY ".DB_PAYOUT_ID." DESC LIMIT 1)";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Round::getCountSummary  db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rows = array();
		while (($r = $qr->GetRow()))
			$rows[] = $r[0];
		//------------------
		if (sizeof($rows) < 4)
		{
			XLogError("Round::getCountSummary expected field missing: ".XVarDump($rows));
			return false;
		}
		//------------------
		$rData = array( 'stats' => $rows[0], 'nonzero' => $rows[1], 'rewards' => $rows[2], 'payouts' => $rows[3]);
		//------------------
		if ($includeTxId)
			$rData['txid'] = (isset($rows[4]) && $rows[4] !== null ? $rows[4] : false);
		//------------------
		return $rData;
	}
	//------------------
} // class Round
//---------------
class Rounds
{
	//------------------
	function Install()
	{
		global $db, $dbRoundFields;
		//------------------------------------
		$sql = $dbRoundFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Rounds::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbRoundFields;
		//------------------------------------
		$sql = $dbRoundFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Rounds::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbRoundFields;
		//------------------------------------
		$newFields = array(); // newField["newFieldName"] = strDefaultValue; (value, NULL, or other tableName)
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // added field comment in ver 2
				//---------------
				$sql = "INSERT INTO $dbRoundFields->tableName SELECT $oldTableName.*,'' AS ".DB_ROUND_COMMENT." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Rounds::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 2: // 2->3 Changed totwork type to BIGINT, just do a copy
			case 3: // 3->4 added DB_ROUND_STEP and DB_ROUND_PROGRESS
				//---------------
				$newFields[DB_ROUND_STEP] = 0;
				$newFields[DB_ROUND_PROGRESS] = 0;
				//---------------
				// fall through
			case 4: // 4->5 added DB_ROUND_STORE_PAY, DB_ROUND_PAY_STORED, DB_ROUND_DATE_POST_CONT_REQUESTED, and DB_ROUND_DATE_POST_CONT_DONE
				//---------------
				$newFields[DB_ROUND_STORE_PAY] = "NULL";
				$newFields[DB_ROUND_PAY_STORED] = "NULL";
				$newFields[DB_ROUND_DATE_POST_CONT_REQUESTED] = "NULL";
				$newFields[DB_ROUND_DATE_POST_CONT_DONE] = "NULL";
				//---------------
				$oldFieldsArray = array();
				$fieldsArray = $dbRoundFields->GetNameArray();
				foreach ($fieldsArray as $fn)
					if (!array_key_exists($fn, $newFields))
						$oldFieldsArray[] = $fn;
				//---------------
				$newAssignArray = array();
				$newFieldsArray = $oldFieldsArray;
				foreach ($newFields as $fn=>$val)
				{
					$newFieldsArray[] = $fn;
					$newAssignArray[] = "$val AS $fn";
				}
				//---------------
				$sql = "INSERT INTO $dbRoundFields->tableName (".implode(",", $newFieldsArray).") SELECT ".implode(",", $oldFieldsArray).(sizeof($newAssignArray) == 0 ? "" : ",".implode(",", $newAssignArray))." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Rounds::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 5: // 5->6 big changes, need to re-interpret the data
				//---------------
				$utcTimeZone = new DateTimeZone('UTC');
				//---------------
				$sql = "ALTER TABLE ".DB_ROUND." MODIFY ".DB_ROUND_ID." INT UNSIGNED NOT NULL";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Rounds::Import db Execute table alter disabling primary key failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				$sql = "SELECT * FROM $oldTableName ORDER BY ".DB_ROUND_ID." DESC";
				if (!($qr = $db->Query($sql)))
				{
					XLogError("Rounds::Import db Query failed.\nsql: $sql");
					return $default;
				}
				//------------------
				$timer = new XTimer();
				while ( ($oldRow = $qr->GetRowArray()) !== false )
				{
					//------------------
					$dbRoundFields->ClearValues();
					$dbRoundFields->SetValuePrepared(DB_ROUND_ID, $oldRow['id']);
					$dbRoundFields->SetValuePrepared(DB_ROUND_TEAM_ID, $oldRow['teamid']);
					if (isset($oldRow['comment']))
						$dbRoundFields->SetValuePrepared(DB_ROUND_COMMENT, $oldRow['comment']);
					//------------------
					$strStart = $oldRow['started'];
					if ( (!isset($oldRow['fahstatsdone']) || $oldRow['fahstatsdone'] = '') && (!isset($oldRow['statsdone']) || $oldRow['statsdone'] == '') )
					{
						XLogError("Rounds::Import old Round ".$oldRow['id']." has neither fahstatsdone nor statsdone set");
						return false;
					}
					$strStats = (isset($oldRow['fahstatsdone']) && $oldRow['fahstatsdone'] != '' ? $oldRow['fahstatsdone'] : $oldRow['statsdone']);
					$strPaid = $oldRow['paid'];
					if (!is_string($strStart) || $strStart == '' || !is_string($strStats) || $strStats == '' || !is_string($strPaid) || $strPaid == '')
					{
						XLogError("Rounds::Import old Round ".$oldRow['id']." validate date values failed: start ".XVarDump($strStart).", stats ".XVarDump($strStats).", paid ".XVarDump($strPaid));
						return false;
					}
					$dtStart = XStringToDate($strStart, $utcTimeZone);
					$dtStats = XStringToDate($strStats, $utcTimeZone);
					$dtPaid = XStringToDate($strPaid, $utcTimeZone);
					if ($dtStart === false || $dtStats === false || $dtPaid === false)
					{
						XLogError("Rounds::Import old Round ".$oldRow['id']." XStringToDate date values failed: start ".XVarDump($strStart).", stats ".XVarDump($strStats).", paid ".XVarDump($strPaid).", dtStart ".XVarDump($dtStart).", dtStats ".XVarDump($dtStats).", dtPaid ".XVarDump($dtPaid));
						return false;
					}
					$tsStart = $dtStart->getTimestamp();
					$tsStats = $dtStats->getTimestamp();
					$tsPaid = $dtPaid->getTimestamp();
					if ($dtStart === false || $dtStats === false || $dtPaid === false)
					{
						XLogError("Rounds::Import old Round ".$oldRow['id']." verify timestamp values failed: str start ".XVarDump($strStart).", stats ".XVarDump($strStats).", paid ".XVarDump($strPaid).", dtStart ".XVarDump($dtStart).", dtStats ".XVarDump($dtStats).", dtPaid ".XVarDump($dtPaid).", tsStart ".XVarDump($tsStart).", tsStats ".XVarDump($tsStats).", tsPaid ".XVarDump($tsPaid));
						return false;
					}
					$dbRoundFields->SetValuePrepared(DB_ROUND_DATE_STARTED, $tsStart);
					$dbRoundFields->SetValuePrepared(DB_ROUND_DATE_STATS, $tsStats);
					$dbRoundFields->SetValuePrepared(DB_ROUND_DATE_PAID, $tsPaid);
					//------------------
					$payMode = $oldRow['paymode'];
					$statsMode = $oldRow['statsmode'];
					$flags = ROUND_FLAG_DONE | ROUND_FLAG_FUNDED | ROUND_FLAG_APPROVED;
					if (XMaskContains($payMode, 0x80))
						$flags |= ROUND_FLAG_DRYRUN;
					if (XMaskContains($payMode, 0x100))
						$flags |= ROUND_FLAG_HIDDEN;
					if (XMaskContains($payMode, 0x200))
						$flags |= ROUND_FLAG_TEST_BALANCE;
					if (XMaskContains($payMode, 0x400))
						$flags |= ROUND_FLAG_STORE_VALUES;
					if (XMaskContains($payMode, 0x800))
						$flags |= ROUND_FLAG_FORFEIT_STORE;
					if ($statsMode == 16)
						$flags |= ROUND_FLAG_LEGACY_STATMODE_OTHER;
					else if ($statsMode == 3 || $statsMode == 1)
						$flags |= ROUND_FLAG_LEGACY_STATMODE_EOW;
					else if ($statsMode == 4)
						$flags |= ROUND_FLAG_LEGACY_STATMODE_FAH;
					else if ($statsMode != 32) // FAH App Score
					{
						XLogError("Rounds::Import old Round ".$oldRow['id']." has unexpected statsMode: ".XVarDump($statsMode));
						return false;
					}
					$dbRoundFields->SetValuePrepared(DB_ROUND_FLAGS, $flags);
					//------------------
					$dbRoundFields->SetValuePrepared(DB_ROUND_TOTAL_WORK, (isset($oldRow['totwork']) ? $oldRow['totwork'] : 0));
					$dbRoundFields->SetValuePrepared(DB_ROUND_TOTAL_PAY, (isset($oldRow['totpay']) ? $oldRow['totpay'] : 0.0));
					$dbRoundFields->SetValuePrepared(DB_ROUND_STORE_PAY, (isset($oldRow['storepay']) ? $oldRow['storepay'] : 0.0));
					$dbRoundFields->SetValuePrepared(DB_ROUND_PAY_STORED, (isset($oldRow['paystored']) ? $oldRow['paystored'] : 0.0));
					//------------------
					$dbRoundFields->SetValuePrepared(DB_ROUND_STEP, ROUND_STEP_DONE);
					//------------------
					$sql = $dbRoundFields->scriptInsert();
					$dbs = $db->Prepare($sql);
					if ($dbs === false)
					{
						XLogError("Rounds::Import db Prepare failed.\nsql: $sql");
						return false;
					}
					//------------------
					if (!$dbRoundFields->BindValues($dbs))
					{
						XLogError("Rounds::Import BindValues failed.");
						return false;
					}
					//------------------
					if (!$dbs->execute())
					{
						XLogError("Rounds::Import db prepared statement Execute failed.\nsql: $sql");
						return false;
					}
					//------------------
				} // while ( ($oldRow = $qr->GetRowArray() )			
				//---------------
				$sql = "ALTER TABLE ".DB_ROUND." MODIFY ".DB_ROUND_ID." INT UNSIGNED NOT NULL AUTO_INCREMENT";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Rounds::Import db Execute table alter re-enabling primary key failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				XLogDebug("Rounds::Import 5->6 took ".$timer->restartMs(true));
				//---------------
				break;
			case $dbRoundFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbRoundFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Rounds::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Rounds::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msizeRound = new Round();
		$msizeRound->setMaxSizes();
		//------------------------------------
		return $msizeRound;		
	}
	//------------------
	function Init()
	{
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		$cfgDefList = array();
		//------------------
		$cfgDefList[CFG_ROUND_TEAM_ID] = DEFAULT_ROUND_TEAM_ID;
		$cfgDefList[CFG_ROUND_FLAGS] = DEFAULT_ROUND_FLAGS;
		$cfgDefList[CFG_ROUND_PAY_DIGITS] = DEFAULT_ROUND_PAY_DIGITS;
		$cfgDefList[CFG_ROUND_TRANS_WAIT] = DEFAULT_ROUND_TRANS_WAIT;
		$cfgDefList[CFG_ROUND_ACTION_STACK] = DEFAULT_ROUND_ACTION_STACK;
		$cfgDefList[CFG_ROUND_PAY_MINIMUM] = DEFAULT_ROUND_PAY_MINIMUM;
		$cfgDefList[CFG_ROUND_STORE_MINIMUM]= DEFAULT_ROUND_STORE_MINIMUM;
		$cfgDefList[CFG_ROUND_STORE_TX_MINIMUM] = DEFAULT_ROUND_STORE_TX_MINIMUM;
		$cfgDefList[CFG_ROUND_STORE_FORFEIT_DAYS] = DEFAULT_ROUND_STORE_FORFEIT_DAYS;
		$cfgDefList[CFG_ROUND_STORE_FORFEIT_IDLE_DAYS] = DEFAULT_ROUND_STORE_FORFEIT_IDLE_DAYS;
		$cfgDefList[CFG_ROUND_TIMEOUT_FAHAPP_STEP] = DEF_ROUND_TIMEOUT_FAHAPP_STEP;
		//------------------
		foreach ($cfgDefList as $name => $defValue)
			if ($Config->GetSetDefault($name, $defValue) === false)
			{
				XLogError("Rounds::Init Config GetSetDefault '$name' failed");
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function getActionTextNames()
	{
		//------------------
		$names = array(	
						ROUND_ACTION_UNKNOWN => 'Unknown',
						ROUND_ACTION_NOT_SET => 'Not Set',
						ROUND_ACTION_NONE => 'None',
						ROUND_ACTION_WAIT_MONITOR_EVENTS => 'Wait For Monitor Events',
						ROUND_ACTION_FAH_APP_STATS => 'FahAppClient Request',
						ROUND_ACTION_VERIFY_NEW_DATA => 'Verify New Data',
						ROUND_ACTION_FAH_APP_NEW_WORKERS => 'FahAppClient New Workers',
						ROUND_ACTION_FAH_APP_VALIDATE_WORKERS => 'FahAppClient Validate Workers',
						ROUND_ACTION_FAH_APP_WEEK_STATS => 'FahAppClient Week Stats',
						ROUND_ACTION_FAH_APP_DEEP_STATS => 'FahAppClient Deep Stats',
						ROUND_ACTION_FAH_APP_ADD_STATS => 'FahAppClient Add Stats',
						ROUND_ACTION_VERIFY_NEW_STATS => 'Verify New Stats',
						ROUND_ACTION_TEST_FORFEIT => 'Test Store Forfeit',
						ROUND_ACTION_CHECK_FORFEIT => 'Check Store Forfeit',
						ROUND_ACTION_WAIT_FUNDED => 'Wait Funded',
						ROUND_ACTION_CLEAR_APPROVED => 'Clear Approved',
						ROUND_ACTION_WAIT_APPROVED => 'Wait Approved',
						ROUND_ACTION_CONTRIBUTIONS_REQ => 'Request Contributions',
						ROUND_ACTION_CONTRIBUTIONS_STEP => 'Step Contributions',
						ROUND_ACTION_CONTRIBUTIONS_TEST => 'Test Contributions',
						ROUND_ACTION_CONTRIBUTIONS_UNTEST => 'Cleanup Contributions Test',
						ROUND_ACTION_VERIFY_CONTRIBUTIONS => 'Verify Contributions',
						ROUND_ACTION_ACCUMULATE_STOREPAYS => 'Accumulate Store Pays',
						ROUND_ACTION_ACCUMULATE_STOREPAYS_TEST => 'Accumulate Store Pays Test',						
						ROUND_ACTION_SEND_PAYOUT => 'Send Payout',
						ROUND_ACTION_DONE => 'Done');
		//------------------
		return $names;		
	}
	//------------------
	function isMultiPassAction($action)
	{
		//------------------
		$multipass = array(	ROUND_ACTION_WAIT_MONITOR_EVENTS,
							ROUND_ACTION_FAH_APP_NEW_WORKERS,
							ROUND_ACTION_FAH_APP_VALIDATE_WORKERS,
							ROUND_ACTION_FAH_APP_WEEK_STATS,
							ROUND_ACTION_FAH_APP_DEEP_STATS,
							ROUND_ACTION_FAH_APP_ADD_STATS,
							ROUND_ACTION_CONTRIBUTIONS_STEP,
							ROUND_ACTION_CONTRIBUTIONS_TEST,
							ROUND_ACTION_WAIT_APPROVED
						  );
		//------------------
		return in_array($action, $multipass);
	}
	//------------------
	function getRoundActionStack()
	{
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		$csvActionStack = $Config->GetSetDefault(CFG_ROUND_ACTION_STACK, DEFAULT_ROUND_ACTION_STACK);
		if ($csvActionStack === false)
		{
			XLogError("Rounds::getRoundActionStack Config GetSetDefault failed");
			return false;
		}
		//------------------
		$actionStack = array();
		$hexActionStack = explode(',', $csvActionStack);
		foreach ($hexActionStack as $hexAction)
			$actionStack[] = hexdec($hexAction);
		//------------------
		return $actionStack;
	}
	//------------------
	function getActionFromStep($step, $actionStack = false)
	{
		//------------------
		if (!is_numeric($step))
		{
			XLogError("Rounds::getActionFromStep validate parameters failed");
			return false;
		}
		//------------------
		if ($actionStack === false)
		{
			$actionStack = $this->getRoundActionStack();
			if ($actionStack === false)
			{
				XLogError("Rounds::getActionFromStep getRoundActionStack failed");
				return false;
			}
		}
		//------------------
		if ($step == ROUND_STEP_NONE)
			return ROUND_ACTION_NONE;
		if ($step == ROUND_STEP_DONE)
			return ROUND_ACTION_DONE;
		//------------------
		$step--;
		//------------------
		if ($step >= sizeof($actionStack) || !isset($actionStack[$step]))
			return ROUND_ACTION_UNKNOWN;
		//------------------
		return $actionStack[$step];
	}
	//------------------
	function deleteRound($idx)
	{
		global $db, $dbRoundFields;
		//---------------------------------
		$sql = $dbRoundFields->scriptDelete(DB_ROUND_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Rounds::deleteRound - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbRoundFields;
		//---------------------------------
		$sql = $dbRoundFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Rounds::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function loadRoundRaw($idx)
	{
		global $db, $dbRoundFields;
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Rounds::loadRoundRaw validate parameters failed. idx: ".XVarDump($idx));
			return false;
		}
		//------------------
		$dbRoundFields->SetValues();
		//------------------
		$sql = $dbRoundFields->scriptSelect(DB_ROUND_ID."=".$idx, false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::loadRoundRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getRound($idx)
	{
		//------------------
		$qr = $this->loadRoundRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Rounds::getRound loadRoundRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Rounds::getRound index $idx not found");
			return false;
		}
		//------------------
		return new Round($s);
	}
	//------------------
	function getLastRound()
	{
		global $db, $dbRoundFields;
		//------------------
		$dbRoundFields->SetValues();
		//------------------
		$sql = $dbRoundFields->scriptSelect(false /*where*/, DB_ROUND_ID.' DESC' /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::getLastRound - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray(0 /*emptyValue*/);
		//------------------
		if ($s === false)
		{
			XLogWarn("Rounds::getLastRound query result GetRowArray failed");
			return false;
		}
		//------------------
		if ($s === 0)
			return 0; // empty
		//------------------
		return new Round($s);
	}
	//------------------
	function getRoundIDs($onlyPaid = false, $maxCount = false, $orderby = false, $where = false)
	{
		global $db, $dbRoundFields;
		//------------------
		$dbRoundFields->ClearValues();
		$dbRoundFields->SetValue(DB_ROUND_ID);
		//------------------
		if ($onlyPaid === true)
			$where = DB_ROUND_DATE_PAID.' IS NOT NULL'.($where !== false ? " AND ($where)": '');
		//------------------
		if ($orderby === false)
			$orderby = DB_ROUND_ID;
		//------------------
		$sql = $dbRoundFields->scriptSelect($where /*where*/, $orderby /*orderby*/, $maxCount /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::getRoundIDs - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$roundIDs = array();
		while ( ($value = $qr->GetOneValue()) !== false )
			$roundIDs[] = $value;
		//------------------
		return $roundIDs;
	}
	//------------------
	function loadRoundsRaw($maxCount = false, $sort = false, $where = false)
	{
		global $db, $dbRoundFields;
		//------------------
		$dbRoundFields->SetValues();
		//------------------
		if ($sort === false)
			$orderby = DB_ROUND_ID;
		else
			$orderby = $sort;
		//------------------
		$sql = $dbRoundFields->scriptSelect($where /*where*/, $orderby /*orderby*/, $maxCount /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::loadRoundsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getRounds($onlyPaid = false, $onlyNotPaid = false, $maxCount = false, $sort = false, $where = false)
	{
		//------------------
		if ($onlyPaid && $onlyNotPaid)
		{
			XLogError("Rounds::getRounds validate params onlyPaid/onlyNotPaid failed");
			return false;
		}
		//------------------
		if ($onlyPaid || $onlyNotPaid)
		{
			$where = ($where === false ? '' : $where.' AND ');
			$where .= DB_ROUND_DATE_PAID.' IS '.($onlyPaid ? 'NOT NULL' : 'NULL');
		}
		//------------------
		$qr = $this->loadRoundsRaw($maxCount, $sort, $where);
		//------------------
		if ($qr === false)
		{
			XLogError("Rounds::getRounds - loadRoundsRaw failed");
			return false;
		}
		//------------------
		$rounds = array();
		while ($s = $qr->GetRowArray())
			$rounds[] = new Round($s);
		//------------------
		return $rounds;
	}
	//------------------
	function addRound($teamId, $flags = ROUND_FLAG_NONE, $startAutomation = true)
	{
		global $db, $dbRoundFields;
		//------------------
		if (!is_numeric($flags) || !is_numeric($teamId))
		{
			XLogError("Rounds::addRound validate params failed");
			return false;
		}
		//------------------
		$dbRoundFields->ClearValues();
		$dbRoundFields->SetValue(DB_ROUND_DATE_STARTED, time());
		$dbRoundFields->SetValue(DB_ROUND_FLAGS, $flags);
		$dbRoundFields->SetValue(DB_ROUND_TEAM_ID, $teamId);
		$dbRoundFields->SetValue(DB_ROUND_STEP, ROUND_STEP_NONE);
		//------------------
		$sql = $dbRoundFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Rounds::addRound - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if ($startAutomation !== false)
		{
			//---------------------------------
			$Automation = new Automation() or die("Create object failed");
			//---------------------------------
			if (!$Automation->StartRoundCheckPage())
			{
				XLogError("Rounds::addRound - Automation StartRoundCheckPage failed");
				return false;
			}
			//---------------------------------
		}
		//---------------------------------
		return true;
	}
	//------------------
	function getRoundDate($idx)
	{
		global $db, $dbRoundFields;
		//------------------
		$dbRoundFields->ClearValues();
		$dbRoundFields->SetValue(DB_ROUND_DATE_STARTED);
		$sql = $dbRoundFields->scriptSelect(DB_ROUND_ID."=$idx", false /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::getRoundDate db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(-1 /*emptyValue*/);
		if ($value === false)
		{
			XLogError("Rounds::getRoundDate GetOneValue failed");
			return false;
		}
		//------------------
		if ($value === -1)
		{
			XLogError("Rounds::getRoundDate round id $idx not found");
			return false;
		}
		//------------------
		return (int)$value;
	}
	//------------------
	function getLastRoundIndex($onlyPaid = false, $beforeIdx = false, $notHidden = true)
	{
		global $db, $dbRoundFields;
		//------------------
		$where = "";
		if ($beforeIdx !== false)
			$where .= DB_ROUND_ID."<$beforeIdx";
		//------------------
		if ($onlyPaid !== false)
		{
			if ($where != "")
				$where .= " AND ";
			$where .= DB_ROUND_DATE_PAID." IS NOT NULL";
		}
		//------------------
		$sql = "SELECT ".DB_ROUND_ID.", ".DB_ROUND_FLAGS." FROM ".DB_ROUND." WHERE ".DB_ROUND_ID."=(SELECT MAX(".DB_ROUND_ID.") FROM ".DB_ROUND.($where != "" ? " WHERE $where)" : ")");
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Rounds::getLastRoundIndex db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$r = $qr->GetRow(-1 /*emptyValue*/);
		if ($r === false)
		{
			XLogWarn("Rounds::getLastRoundIndex GetRow failed.");
			return false;
		}
		//------------------
		if ($r === -1)
			return -1; // no round found
		//------------------
		if ($notHidden === true && (!isset($r[1]) || !is_numeric($r[1]) || XMaskContains($r[1] /*flags*/, ROUND_FLAG_HIDDEN)) )
			return $this->getLastRoundIndex($onlyPaid, (int)$r[0] /*beforeIdx*/, $notHidden); // This one's hidden. recursively try again with the next one back
		//------------------
		return (int)$r[0];
	}
	//------------------
	function getActiveRounds()
	{
		//------------------
		$roundList = $this->getRounds(false /*onlyPaid*/, false /*onlyNotPaid*/);
		if ($roundList === false)
		{
			XLogError("Rounds::getActiveRounds getRounds failed");
			return false;
		}
		//------------------
		$activeRounds = array();
		foreach ($roundList as $round)
			if ($round->isActive())
				$activeRounds[] = $round;
		//------------------
		return $activeRounds;
	}
	//------------------
	function hasActiveRounds()
	{
		//------------------
		$activeRounds = $this->getActiveRounds();
		//------------------
		return ($activeRounds !== false && sizeof($activeRounds) > 0);
	}
	//------------------
	function getStoreForfeitMaxDate($dtNow /*MixedDate*/, $forfeitDays)
	{
		//------------------
		$tryDtNow = XMixedToDate($dtNow);
		//------------------
		if ($tryDtNow === false)
		{
			XLogError("Rounds::getStoreForfeitMaxDate XMixedToDate dtNow parameter failed: ".XVarDump($dtNow));
			return false;
		}
		//------------------
		$dtMaxDate = clone $tryDtNow; // ensure deep copy
		//------------------
		if (!is_numeric($forfeitDays) || $forfeitDays < 0)
		{
			XLogError("Rounds::getStoreForfeitMaxDate validate configured forfeitDays value failed: ".XVarDump($forfeitDays));
			return false;
		}
		//------------------
		if ($forfeitDays == 0)
			return true; // Forfeit is disabled, return TRUE
		//------------------
		try
		{
			$intervalForfeit = new DateInterval("P".$forfeitDays."D");
		}
		catch (Exception $e)
		{
			XLogNotify("Rounds::getStoreForfeitMaxDate exception creating DateInterval  from configured forfeitDays value: ".XVarDump($forfeitDays));
			return false;
		}
		//------------------
		$dtMaxDate = $dtMaxDate->sub($intervalForfeit);
		//------------------
		if ($dtMaxDate === false)
		{
			XLogNotify("Rounds::getStoreForfeitMaxDate subtract intervalForfeit from now DateTime failed. forfeitDays: ".XVarDump($forfeitDays).", intervalForfeit: ".XVarDump($intervalForfeit).", dtNow: ".XVarDump($dtNow).", dtMaxDate: ".XVarDump($dtMaxDate));
			return false;
		}
		//------------------
		return $dtMaxDate;
	}
	//------------------
	function getForfeitPayoutSummary($dtNow = false, $forfeitDays = false, $forfeitIdleDays = false)
	{
		//------------------
		$Payouts = new Payouts() or die ("Create object failed");
		$Stats = new Stats() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$tryDtNow = XMixedToDate($dtNow);
		//------------------
		if ($tryDtNow === false)
		{
			XLogError("Rounds::getForfeitPayoutSummary XMixedToDate dtNow parameter failed: ".XVarDump($dtNow));
			return false;
		}
		//------------------
		$dtNow = $tryDtNow;
		//------------------
		$workerSummary = array();
		$workerSummary['tests'] = array();
		//------------------
		$storeMinimum = $Config->GetSetDefault(CFG_ROUND_STORE_MINIMUM, DEFAULT_ROUND_STORE_MINIMUM);
		if ($storeMinimum === false)
		{
			XLogError("Rounds::getForfeitPayoutSummary Config GetSetDefault storeMinimum failed");
			return false;
		}
		//------------------
		$storeTxMinimum = $Config->GetSetDefault(CFG_ROUND_STORE_TX_MINIMUM, DEFAULT_ROUND_STORE_TX_MINIMUM);
		if ($storeTxMinimum === false)
		{
			XLogError("Rounds::getForfeitPayoutSummary Config GetSetDefault storeTxMinimum failed");
			return false;
		}
		//------------------
		if ($forfeitDays === false)
		{
			$forfeitDays = $Config->GetSetDefault(CFG_ROUND_STORE_FORFEIT_DAYS, DEFAULT_ROUND_STORE_FORFEIT_DAYS);
			if ($forfeitDays === false)
			{
				XLogError("Rounds::getForfeitPayoutSummary Config GetSetDefault forfeitDays failed");
				return false;
			}
		}
		//------------------
		$dtStoreMaxDate = $this->getStoreForfeitMaxDate($dtNow, $forfeitDays);
		if ($dtStoreMaxDate === false)
		{
			XLogError("Rounds::getForfeitPayoutSummary getStoreForfeitMaxDate failed");
			return false;
		}
		//------------------
		$timer->start();
		if ($dtStoreMaxDate === true) 
		{
			//------------------
			XLogNotify("Rounds::getForfeitPayoutSummary forfeitDays disabled per configuration (".CFG_ROUND_STORE_FORFEIT_DAYS."=0), skipping.");
			//------------------
			$workerSummary['tests']['forfeitDays'] = array('enabled' => false, 'limit' => $forfeitDays, 'dtNow' => $dtNow->getTimestamp(), 'dtLimit' => false, 'workerCount' => 0, 'payoutCount' => 0);
			//------------------
		}
		else
		{
			//------------------
			$payoutList = $Payouts->findOldStoredPayouts($dtStoreMaxDate->getTimestamp());
			if ($payoutList === false)
			{
				XLogError("Rounds::getForfeitPayoutSummary Payouts findOldStoredPayouts failed");
				return false;
			}
			//------------------
			XLogDebug("Rounds::getForfeitPayoutSummary forfeitDays: '$forfeitDays', Payouts findOldStoredPayouts, found ".sizeof($payoutList)." older than '".$dtStoreMaxDate->format(MYSQL_DATETIME_FORMAT)."' and took ".$timer->restartMs(true)); 
			//------------------
			$forfeitWorkerList = array();
			$forfeitPayoutList = array();
			//------------------
			foreach ($payoutList as $payout)
			{
				//------------------
				if (!is_numeric($payout->workerIdx) || $payout->workerIdx < 0)
				{
					XLogError("Rounds::getForfeitPayoutSummary validate payout workerIdx failed. Payout in list id: ".$payout->id.", workerIdx: ".XVarDump($payout->workerIdx));
					return false;
				}
				//------------------
				if (!in_array($payout->workerIdx, $forfeitWorkerList))
					$forfeitWorkerList[] = $payout->workerIdx;
				//------------------
				if (!in_array($payout->id, $forfeitPayoutList))
					$forfeitPayoutList[] = $payout->id;
				//------------------
				$reasonDetails = "$payout->id,".($payout->dtStored === false ? '0' : $payout->dtStored);
				//------------------
				if (!isset($workerSummary[$payout->workerIdx]))
					$workerSummary[$payout->workerIdx] = array('total' => $payout->statPay, 'payouts' => array(array('reasons' => array(array('test' => 'forfeitDays', 'details' => $reasonDetails)),  'payout' => $payout)));
				else
				{
					//------------------
					$pidx = false;
					for ($idx = 0;$idx < sizeof($workerSummary[$payout->workerIdx]['payouts']);$idx++)
						if ($workerSummary[$payout->workerIdx]['payouts'][$idx]['payout']->id == $payout->id)
						{
							$pidx = $idx;
							break;
						}
					//------------------
					if ($pidx !== false)
						$workerSummary[$payout->workerIdx]['payouts'][$pidx]['reasons'][] = array('test' => 'forfeitDays', 'details' => $reasonDetails);
					else
					{
						$workerSummary[$payout->workerIdx]['total'] = bcadd($workerSummary[$payout->workerIdx]['total'] , $payout->statPay, 8);
						$workerSummary[$payout->workerIdx]['payouts'][] = array('reasons' => array(array('test' => 'forfeitDays', 'details' => $reasonDetails)), 'payout' => $payout);
					}
					//------------------
				}
				//------------------
			} // foreach ($payoutList as $payout)
			//------------------
			$workerSummary['tests']['forfeitDays'] = array('enabled' => true, 'limit' => $forfeitDays, 'dtNow' => $dtNow->getTimestamp(), 'dtLimit' => $dtStoreMaxDate->getTimestamp(), 
															'workerCount' => sizeof($forfeitWorkerList), 'payoutCount' => sizeof($forfeitPayoutList));
			//------------------
		} // else // if ($dtStoreMaxDate === true) 
		//------------------
		XLogDebug("Round::getForfeitPayoutSummary forfeitDays processing ".sizeof($payoutList)." payouts found ".(sizeof($workerSummary) - 1 /*tests*/)." workers and took ".$timer->restartMs(true)); 
		//------------------
		if ($forfeitIdleDays === false)
		{
			$forfeitIdleDays = $Config->GetSetDefault(CFG_ROUND_STORE_FORFEIT_IDLE_DAYS, DEFAULT_ROUND_STORE_FORFEIT_IDLE_DAYS);
			if ($forfeitIdleDays === false)
			{
				XLogError("Rounds::getForfeitPayoutSummary Config set default forfeitIdleDays failed");
				return false;
			}
		}
		//------------------
		$dtStoreMaxDate = $this->getStoreForfeitMaxDate($dtNow, $forfeitIdleDays);
		if ($dtStoreMaxDate === false)
		{
			XLogError("Rounds::getForfeitPayoutSummary getStoreForfeitMaxDate (idle) failed");
			return false;
		}
		//------------------
		$timer->start();
		if ($dtStoreMaxDate === true) 
		{
			//------------------
			XLogNotify("Rounds::getForfeitPayoutSummary forfeitIdleDays idle disabled per configuration (".CFG_ROUND_STORE_FORFEIT_IDLE_DAYS."=0), skipping.");
			//------------------
			$workerSummary['tests']['forfeitIdleDays'] = array('enabled' => false, 'limit' => $forfeitIdleDays, 'dtNow' => $dtNow->getTimestamp(), 'dtLimit' => false, 'workerCount' => 0, 'payoutCount' => 0);
			//------------------
		}
		else
		{
			//------------------
			$idleSummary =  $Payouts->getIdleWorkerStoredPayoutSummary($dtStoreMaxDate->getTimestamp());
			if ($idleSummary === false)
			{
				XLogError("Rounds::getForfeitPayoutSummary forfeitIdleDays Payouts getIdleWorkerStoredPayoutSummary failed");
				return false;
			}
			//------------------
			XLogDebug("Rounds::getForfeitPayoutSummary forfeitIdleDays Payouts getIdleWorkerStoredPayoutSummary, found ".sizeof($idleSummary)." and took ".$timer->restartMs(true)); 
			//------------------
			$forfeitWorkerList = array();
			$forfeitPayoutList = array();
			//------------------
			foreach ($idleSummary as $idleData)
			{
				//------------------
				$payout = $idleData['payout'];
				$workerIdx = $payout->workerIdx;
				$reasonDetails =  $idleData['dtLastWork'];
				//------------------
				if (!in_array($payout->workerIdx, $forfeitWorkerList))
					$forfeitWorkerList[] = $payout->workerIdx;
				//------------------
				if (!in_array($payout->id, $forfeitPayoutList))
					$forfeitPayoutList[] = $payout->id;
				//------------------
				if (!isset($workerSummary[$workerIdx]))
					$workerSummary[$workerIdx] = array('total' => $payout->statPay, 'payouts' => array(array('reasons' => array(array('test' => 'forfeitIdleDays', 'details' => $reasonDetails)), 'payout' => $payout)));
				else
				{
					//------------------
					$pidx = false;
					for ($idx = 0;$idx < sizeof($workerSummary[$payout->workerIdx]['payouts']);$idx++)
						if ($workerSummary[$payout->workerIdx]['payouts'][$idx]['payout']->id == $payout->id)
						{
							$pidx = $idx;
							break;
						}
					//------------------
					if ($pidx !== false)
						$workerSummary[$payout->workerIdx]['payouts'][$pidx]['reasons'][] = array('test' => 'forfeitIdleDays', 'details' => $reasonDetails);
					else
					{
						$workerSummary[$payout->workerIdx]['total'] = bcadd($workerSummary[$payout->workerIdx]['total'] , $payout->statPay, 8);
						$workerSummary[$payout->workerIdx]['payouts'][] = array('reasons' => array(array('test' => 'forfeitIdleDays', 'details' => $reasonDetails)), 'payout' => $payout);
					}
					//------------------
				}
				//------------------
			} // foreach ($idleSummary as $idleData)
			//------------------
			XLogDebug("Round::getForfeitPayoutSummary forfeitIdleDays processed ".sizeof($idleSummary)." idleSummary entries, found ".sizeof($forfeitWorkerList)." idle workers, with ".sizeof($forfeitPayoutList)." forfeit payouts, took ".$timer->restartMs(true)); 
			//------------------
			$workerSummary['tests']['forfeitIdleDays'] = array('enabled' => true, 'limit' => $forfeitIdleDays, 'dtNow' => $dtNow->getTimestamp(), 'dtLimit' => $dtStoreMaxDate->getTimestamp(), 
																'workerCount' => sizeof($forfeitWorkerList), 'payoutCount' => sizeof($forfeitPayoutList));
			//------------------
		} // else // if ($dtStoreMaxDate === true)
		//------------------
		$valueTotal = "0";
		$workerTotal = 0;
		$payoutTotal = 0;
		//------------------
		foreach ($workerSummary as $widx => $wdata)
			if (is_numeric($widx))
			{
				$valueTotal = bcadd($valueTotal, $wdata['total'], 8);
				$workerTotal++;
				$payoutTotal += sizeof($wdata['payouts']);
			}
		//------------------
		$workerSummary['totals'] = array('workers' => $workerTotal, 'payouts' => $payoutTotal, 'value' => $valueTotal);
		//------------------
		return $workerSummary;
	}
	//------------------
	function printForfeitPayoutSummary($summary)
	{
		$txt = "";
		//------------------
		if (!is_array($summary))
		{
			XLogError("Rounds::printForfeitPayoutSummary validate summary failed: ".XVarDump($summary));
			return false;
		}
		//------------------
		$txt .= "\n-----------Forfeit Summary--------\n";
		$txt .= "Totals:\n";
		$txt .= "\tValue: ".$summary['totals']['value']."\n";
		$txt .= "\tWorkers: ".$summary['totals']['workers']."\n";
		$txt .= "\tPayouts: ".$summary['totals']['payouts']."\n";
		$txt .= "\n";
		//------------------
		$txt .= "Tests:\n";
		foreach ($summary['tests'] as $testName => $testData)
		{
			$txt .= "\t$testName: ";
			if ($testData['enabled'] !== true)
				$txt .= '(disabled)';
			else
			{
				$strNow = ($testData['dtNow'] === false ? '(none)' : gmdate(MYSQL_DATETIME_FORMAT, $testData['dtNow']));
				$strLimit = ($testData['dtLimit'] === false ? '(none)' : gmdate(MYSQL_DATETIME_FORMAT, $testData['dtLimit']));
				$txt .= 'limit '.$testData['limit'].' from '.$strNow.', cutoff '.$strLimit.' found '.$testData['workerCount'].' workers with '.$testData['payoutCount'].' payouts';
			}
			$txt .= "\n";
		}
		$txt .= "\n";
		//------------------
		$txt .= "Forfeits:\n";
		foreach ($summary as $workerIdx => $data)
			if (is_numeric($workerIdx))
			{
				if (sizeof($data['payouts']) == 1)
				{
					$payout = $data['payouts'][0]['payout'];
					$value = (bccomp($payout->statPay, $data['total'], 8) == 0 ? $payout->statPay : $payout->statPay.' (total '.$data['total'].')');
					$txtReasons = "";
					foreach ($data['payouts'][0]['reasons'] as $reasonData)
						$txtReasons .= "(".$reasonData['test'].": ".$reasonData['details'].")";
					$txt .= "\tWorker $workerIdx, forfeit Payout $payout->id value $value, reasons: $txtReasons\n";
				}
				else
				{
					$txt .= "\tWorker $workerIdx, forfeit (multi) total ".$data['total'].":\n";
					foreach ($data['payouts'] as $forfeitPayout)
					{
						$payout = $forfeitPayout['payout'];
						$txtReasons = "";
						foreach ($forfeitPayout['reasons'] as $reasonData)
							$txtReasons .= "(".$reasonData['test'].": ".$reasonData['details'].")";
						$txt .= "\t\tPayout $payout->id (Rnd $payout->roundIdx) value $payout->statPay, reasons: $txtReasons\n";
					}
				}
			}
		//------------------
		$txt .= "-----------------------------------\n\n";
		//------------------
		return $txt;
	}
	//------------------
	function setAutoIncrement($value)
	{
		global $db;
		//------------------
		$sql = "ALTER TABLE ".DB_ROUND." AUTO_INCREMENT = $value";
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Round::setAutoIncrement db Execute failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function getAutoIncrement()
	{
		global $db;
		//------------------
		$sql = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".XDATABASE_NAME."' AND TABLE_NAME   = '".DB_ROUND."'";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Round::getAutoIncrement db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue();
		if ($value === false || !is_numeric($value))
		{
			XLogError("Round::getAutoIncrement query result GetOneValue failed or was invalid: ".XVarDump($value));
			return false;
		}
		//------------------
		return $value;
	}
	//------------------
} // class Rounds
//---------------
?>
