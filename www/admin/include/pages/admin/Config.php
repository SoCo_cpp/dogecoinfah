<?php
//---------------------------------
/*
 * admin/include/pages/admin/Config.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Config = new Config() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action');
$selCfg = XGetPost('cfg');
$showdbvers = (XGetPost('showdbvers') != "" ? true : false);
$selValue = "";
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1"><!-- Start Main Left Column -->
		<div>
			<h1>Config:</h1>
			<p>
			Folding @ Home General Configuration.
			</p>
			<br/>
		</div>
		<?php
		//---------------------------------
		if ($action == 'Set' || $action == 'Clear')
		{
			//---------------------------------
			if ($selCfg != "" && $Login->HasPrivilege(PRIV_CONFIG_MANAGE))
			{
				//---------------------------------
				$oldValue = $Config->Get($selCfg);
				if ($oldValue === false)
				{
					XLogError("Admin Config set Config Get oldValue failed. User $Login->User attempted to change '$selCfg' to '$newValue'");
					echo "<div>Set Config failed.</div>\n";
				}
				else
				{
					//---------------------------------
					if ($action == 'Clear')
					{
						$newValue = "";
						$newComment = false; // don't change
					}
					else
					{
						$newValue = XPost('newValue');
						$newComment = XPost('newComment');
					}
					//---------------------------------
					if (!$Config->Set($selCfg, $newValue, $newComment))
					{
						XLogError("Admin Config set failed. User $Login->User attempted to change '$selCfg' from '$oldValue' to '$newValue'");
						echo "<div>Set Config failed.</div>\n";
					}
					else
					{
						XLogNotify("Admin Config set successfully. User $Login->User changed Config '$selCfg' from '$oldValue' to '$newValue'");
						echo "<div>Config updated.</div>\n";
					}
				}
				//---------------------------------
			}
			else
				echo "<div>Config $action: No selection or missing Config Manage privilege.</div>\n";
			//---------------------------------
		}
		else if ($action == 'New')
		{
			//---------------------------------
			$newCfg = XPost('newCfg');
			//---------------------------------
			if (trim($newCfg) != "" && $Login->HasPrivilege(PRIV_CONFIG_MANAGE))
			{
				//---------------------------------
				if (!$Config->Set($newCfg, ""))
				{
					XLogError("Admin Config New failed.");
					echo "<div>Set Config failed. User $Login->User attempted to create Config '$newCfg'</div>\n";
				}
				else
				{
					XLogNotify("Admin Config created successfully. User $Login->User created Config '$newCfg'");
					echo "<div>Config created.</div>\n";
				}
				//---------------------------------
			}
			else
				echo "<div>Config $action: Empty new config name or Missing Config Manage privilege.</div>\n";
			//---------------------------------
		}
		else if ($action == 'Delete')
		{
			//---------------------------------
			if (trim($selCfg) != "" && $Login->HasPrivilege(PRIV_CONFIG_MANAGE))
			{
				//---------------------------------
				$oldValue = $Config->Get($selCfg);
				if ($oldValue === false)
				{
					XLogError("Admin Config Delete Config Get oldValue failed. User $Login->User attempted to delete '$selCfg'");
					echo "<div>Delete Config failed.</div>\n";
				}
				else
				{
					//---------------------------------
					if (!$Config->Delete($selCfg))
					{
						XLogError("Admin Config Delete failed. User $Login->User attempted to delete '$selCfg'");
						echo "<div>Delte Config failed.</div>\n";
					}
					else
					{
						XLogNotify("Admin Config deleted successfully. User $Login->User deleted Config '$selCfg' was value '$oldValue'");
						echo "<div>Config deleted.</div>\n";
					}
					//---------------------------------
				}
				//---------------------------------
			}
			else
				echo "<div>Config $action: No selection or Missing Config Manage privilege.</div>\n";
			//---------------------------------
		}
		//---------------------------------
		?>
		<?php 
		if ($Login->HasPrivilege(PRIV_CONFIG_MANAGE))
		{
			if ($selCfg !== false && $selCfg != "" )
			{
				$selCfgData = $Config->getFull($selCfg, false /*default*/);
				if ($selCfgData === false)
					$selCfg = "";
				else 
				{
					//XLogDebug("Config Admin Page selCfgData: ".XVarDump($selCfgData));
					$selCfgName = $selCfgData[0];
					$selCfgValue = $selCfgData[1];
					$selCfgComment = $selCfgData[2];
					$selCfgModDate = $selCfgData[3];
					$selCfgModByID = $selCfgData[4];
					$selCfgModDateStr = ($selCfgModDate == 0 ? "[not set]" : $Display->localDateString($selCfgModDate));
					$selCfgModByStr = ($selCfgModByID == -1 ? "[not set]" : $selCfgModByID);
					
				}
			}
		?>
		<div class="col1 pad_left1"><!-- Start Edit Column -->
		<div class="admin-edit" style="width:75%;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;">Configuration:</legend>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>" /><!-- Page -->
				<?php if ($selCfg != "") { ?>
					<input type="hidden" name="cfg" value="<?php echo $selCfg ?>" />
					<label class="loginLabel" for="newValue"><?php echo $selCfg; ?></label><br/>
					<input style="margin-top: 8px;width: 100%;" id="newValue" type="text" name="newValue" value="<?php echo XEncodeHTML($selCfgValue);?>" maxlength="<?php echo C_MAX_CONFIG_VALUE_LENGTH;?>" />
					<label class="loginLabel" for="newValue">Comment</label><br/>
					<input style="margin-top: 8px;width: 100%;" id="newComment" type="text" name="newComment" value="<?php echo XEncodeHTML($selCfgComment);?>" maxlength="<?php echo C_MAX_COMMENT_TEXT_LENGTH;?>" />
					<br/>
					<div>
						Modified Date:&nbsp;<?php echo $selCfgModDateStr; ?><br/>
						Modified By:&nbsp;<?php echo $selCfgModByStr; ?><br/>
					</div>
					<br/>
					<input type="submit" name="action" value="Set" /> <input type="submit" name="action" value="Clear" />
					<br/>
					<br/>
					<hr style='margin-top:8px;height:8px;border-top: 1px solid black;text-align: center;'/>
					<br/>
					<input type="submit" name="action" value="Delete" />
					<br/>
					<br/>
				<?php 
				} // if ($selCfg != "") 
				?>
				<input style="margin-top: 8px; width:220px;" id="newCfg" type="text" name="newCfg" value="<?php echo XEncodeHTML($selCfg);?>" maxlength="<?php echo C_MAX_NAME_TEXT_LENGTH;?>" />
				<br/>
				<br/>
				<input type="submit" name="action" value="New" />
			</fieldset>
		</form>
		</div><!-- admin-edit -->
		<br/>
		</div><!-- End Edit Column -->
		<?php 
		} // if ($Login->HasPrivilege(PRIV_CONFIG_MANAGE))
		?>
		<div class="col1 pad_left1"><!-- Start Left Column -->
		<div class="admin-edit" style="width:250px;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>" /><!-- Page -->
				<input id='showdbvers' type='checkbox' name='showdbvers' value='1' <?php echo ($showdbvers ?  " checked='checked'" : ""); ?> />Show Database Versions
				&nbsp;<input type='submit' name='faction' value='Set' />
			</fieldset>
		</form>
		</div><!-- admin-edit -->
		<?php
			//------------------------	
		if ($Login->HasPrivilege(PRIV_CONFIG_MANAGE))
			echo "<div>(click row to select for editting or deleting)</div>\n";
		else
			echo "<div>(read only privileges)</div>\n";
		//---------------------------------
		?>		
		<table class='admin-table'>
		<tr><th style='width:320px;'>Name</th><th style='width:180px;'>Value</th><th style='width:140px;'>Comment</th></tr>
		<?php
			//------------------------	
			$clist = $Config->getList( ($showdbvers !== false ? false : DB_CFG_NAME." NOT LIKE 'dbtable%'"), false /*justNameValues*/ );
			if ($clist === false)
			{
				XLogError("Config admin page - getList failed");
				echo "<tr class='admin-row'><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
			}
			else
			{
				//------------------------
				foreach ($clist as $name => $config)
				{
					//------------------------
					$value = $config[0];
					$comment = $config[1];
					//------------------------
					$rowClass = ($name == $selCfg ? 'admin-row-sel' : 'admin-row');
					//------------------------
					echo "<tr class='$rowClass' onclick=\"document.location.href='./Admin.php?p=$pagenum&amp;cfg=".XEncodeHTML($name)."&amp;showdbvers=".($showdbvers ? "1" : "")."&amp;".SecToken()."';\">";
					echo "<td style='width:120px;'>".XEncodeHTML($name)."</td><td style='width:80px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'>".XEncodeHTML($value)."</td><td style='width:80px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'>".XEncodeHTML($comment)."</td></tr>\n";
					//------------------------
				}
				//------------------------
			}
			//------------------------
			echo "</table>\n";		
			//------------------------	
		?>
		</div> <!-- End Left Column -->
		</div> <!-- End Main Left Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
