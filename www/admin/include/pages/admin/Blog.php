<?php
//---------------------------------
/*
 * include/pages/admin/Blog.php
 * 
 * 
*/
//---------------------------------
global $Login;
$hideManage = false;
$Blogs = new Blogs() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action');
$selIdx = XGetPost('bid');
if (!is_numeric($selIdx))
	$selIdx = false;
//---------------------------------
function getListRow($entry, $Columns, $pagenum, $selIdx, $canManage)
{
	$vals = array();
	//------------------------
	if ($entry === false)
	{
		$out = "<tr class='admin-row'>";
		foreach ($Columns as $col)
			$out .= "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
		return $out."</tr>"; // empty row
	}
	//------------------------
	$vals[] = XEncodeHTML($entry->dtCreated);
	$vals[] = XEncodeHTML($entry->userID);
	$vals[] = XEncodeHTML($entry->author);
	$vals[] = XEncodeHTML(substr($entry->title, 0, 20));
	$out = "<tr class='admin-row".($entry->id == $selIdx ? "-sel" : "")."' onclick=\"document.location.href='./Admin.php?p=$pagenum&amp;bid=$entry->id&amp;".SecToken()."';\">\n";
	//------------------------
	$cidx = 0;
	foreach ($Columns as $col)
		$out .= "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">".$vals[$cidx++]."</td>\n";
	//------------------------
	$out .= "</tr>\n";
	//------------------------
	return $out;
}
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="float:none; width:740px;"><!-- Start Left Column -->
		<div>
			<h1>Blog Entries:</h1>
			<p>
			Folding @ Home Blog Configuration.
			</p>
			<br/>
		</div>
		<?php
		//---------------------------------
		$blogEntries = $Blogs->getBlogs(true /*noContent*/);
		if ($blogEntries === false)
			XLogError("Admin Blogs failed to getBlogs");
		$canManage = $Login->HasPrivilege(PRIV_BLOG_MANAGE);
		$canAuthor = $Login->HasPrivilege(PRIV_BLOG_AUTHOR);
		//---------------------------------
		//XLogDebug("action : ".XVarDump($action).", idx: ".XVarDump($selIdx));
		//XLogDebug("canManage: ".XVarDump($canManage).", canAuthor: ".XVarDump($canAuthor));
		//---------------------------------
		if ($action == "New")
		{
			//---------------------------------
			$selIdx = false;
			$hideManage = true;
			//---------------------------------
		}
		else if ($action == "Create")
		{
			//---------------------------------
			if ($canAuthor)
			{
				//---------------------------------
				$bTitle = XGetPost('bTitle');
				$bContent = XGetPost('bContent');
				//---------------------------------
				if ($bTitle === false || $bContent === false)
				{
					XLogError("Admin Wallet Blogs parameters missing (for create)");
					echo "<div>Save blog failed and content was lost.</div>\n";
				}
				else if (!$Blogs->addBlog($Login->UserID, true/*dtPublished*/, $Login->UserName, $bTitle, ""/*keywords*/, ""/*tags*/, $bContent))
				{
					XLogError("Admin Wallet Blogs add failed");
					echo "<div>Add new blog failed.</div>\n";
				}
				else
					echo "<div>New blog added successfully.</div>\n";
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
			$blogEntries = $Blogs->getBlogs(true /*noContent*/);
			if ($blogEntries === false)
				XLogError("Admin Blogs failed to getBlogs (after action)");
			//---------------------------------
		} // if ($action == "Create")
		else if (($action == "R" || $action == "Show" || $action == "Hide" || $action == "Save" || $action == "Publish" || $action == "Unpublish") && $selIdx !== false)
		{
			//---------------------------------
			$entry = $Blogs->getBlog($selIdx);
			if ($entry === false)
			{
				XLogError("Admin Wallet Blogs get selected failed (for $action)");
				echo "<div>Save blog failed and content was lost.</div>\n";
			}
			else if ($canManage || ($canAuthor && $Login->UserID == $entry->userID))
			{
				//---------------------------------
				$bTitle = XGetPost('bTitle');
				$bKeywords = XGetPost('bKeywords');
				$bTags = XGetPost('bTags');
				$bContent = XGetPost('bContent');
				$bSlug = XGetPost('bdtSlug');
				$bRepubIndex = XGetPost('repubIndex');
				//---------------------------------
				if ($bTitle === false || $bKeywords === false || $bContent === false || $bSlug === false)
				{
					XLogError("Admin Wallet Blogs parameters missing (for save)");
					echo "<div>Save blog failed and content was lost.</div>\n";
				}
				else
				{
					//---------------------------------
					$success = true;
					$entry->title = $bTitle;
					$entry->content = $bContent;
					$entry->slug = $bSlug;
					$entry->keywords = $bKeywords;
					$entry->tags = $bTags;
					//---------------------------------
					if ($action == "R")
					{
						//---------------------------------
						$entry->slug = false; // needed before generateSlug on "R" action
						if (!$entry->generateSlug())
						{
							XLogError("Admin Wallet Blog entry generateSlug failed (for save)");
							echo "<div>Regenerate blog slug failed and changes may have been lost.</div>\n";
							$success = false;
						}
						//---------------------------------
					}
					else if ($action == "Publish")
					{
						//---------------------------------
						if (!$entry->loadContent())
						{
							XLogError("Admin Blogs failed to loadContent (publish)");
							echo "<div>Publish blog failed.</div>\n";
							$success = false;
						}
						else if (!$entry->Publish())
						{
							XLogError("Admin Wallet Blog entry Publish failed (for publish)");
							echo "<div>Publish blog failed.</div>\n";
							$success = false;
						}
						//---------------------------------
					}
					else if ($action == "Unpublish")
					{
						//---------------------------------
						if (!$entry->loadContent())
						{
							XLogError("Admin Blogs failed to loadContent (unpublish)");
							echo "<div>Unpublish blog failed.</div>\n";
							$success = false;
						}
						else if (!$entry->Unpublish())
						{
							XLogError("Admin Wallet Blog entry Unpublish failed (unpublish)");
							echo "<div>Unpublish blog failed .</div>\n";
							$success = false;
						}
						//---------------------------------
					}
					else if ($action == "Show")
					{
						$entry->clearFlag(BLOG_FLAG_HIDDEN);
					}
					else if ($action == "Hide")
					{
						$entry->setFlag(BLOG_FLAG_HIDDEN);
					}
					//---------------------------------
					if ($success)
						if (!$entry->Update())
						{
							XLogError("Admin Wallet Blog entry Update failed (for save)");
							echo "<div>Save blog failed and changes were lost.</div>\n";
							$success = false;
						}
					//---------------------------------
					if ($success && $bRepubIndex !== false && $bRepubIndex !== "")
					{
						echo "<div>Republishing blog index...</div>\n";
						if (!$Blogs->createIndexFile())
						{
							XLogError("Admin Wallet Blog entry Update failed to republish the blog index (for save)");
							echo "<div>Save blog succeded, but failed to update the blog index.</div>\n";
							$success = false;
						}
					}
					//---------------------------------
					if ($success)
						echo "<div>Updated blog successfully.</div>\n";
					//---------------------------------
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
			$blogEntries = $Blogs->getBlogs(true /*noContent*/);
			if ($blogEntries === false)
				XLogError("Admin Blogs failed to getBlogs (after action)");
			//---------------------------------
		} // else if ($action == "Save" && $selIdx !== false)
		else if ($action == "Delete" && $selIdx !== false)
		{
			//---------------------------------
			$entry = $Blogs->getBlog($selIdx);
			if ($entry === false)
			{
				XLogError("Admin Wallet Blogs get selected failed (for delete)");
				echo "<div>Delete blog failed to find entry.</div>\n";
			}
			else if ($canManage || $Login->UserID == $entry->userID)
			{
				//---------------------------------
				$bRepubIndex = XGetPost('repubIndex');
				//---------------------------------
				if (!$Blogs->deleteBlog($selIdx))
				{
					//---------------------------------
					XLogError("Admin Blogs deleteBlog failed (for Delete)");
					echo "<div>Delete blog failed.</div>\n";
					//---------------------------------
				}
				else 
				{
					//---------------------------------
					echo "<div>Delete blog successfully.</div>\n";
					$selIdx = false;
					//---------------------------------
				}
				//---------------------------------
				if ($bRepubIndex !== false && $bRepubIndex !== "")
				{
					echo "<div>Republishing blog index...</div>\n";
					if (!$Blogs->createIndexFile())
					{
						XLogError("Admin Wallet Blog entry Update failed to republish the blog index (for delete)");
						echo "<div>Failed to update the blog index.</div>\n";
					}
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
			$blogEntries = $Blogs->getBlogs(true /*noContent*/);
			if ($blogEntries === false)
				XLogError("Admin Blogs failed to getBlogs (after action)");
			//---------------------------------
		} // else if ($action == "Delete" && $selIdx !== false)
		else if ($action == "Publish Index")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				if (!$Blogs->createIndexFile())
				{
					XLogError("Admin Wallet Blog createIndexFile failed (for Publish Index)");
					echo "<div>Publish blog index failed.</div>\n";
				}
				else echo "<div>Published blog index successfully.</div>\n";
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		} // else if ($action == "Publish Index")		
		//---------------------------------
		?>
		</div> <!-- End Left Column -->
		<div class="col1 pad_left1" style="float:none;width:80%;"><!-- Start Right Column -->
		<div class="admin-edit" style="width:810px;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
				<div>
					<?php
							//------------------------	
							$Columns = array(	array(	"200px",	"Created"),
												array(	"65px",		"UID"),
												array(	"150px",	"Author"),
												array(	"340px",	"Title")
												);
							//------------------------	
							if ($selIdx !== false && is_numeric($selIdx))
								echo "<input type='hidden' name='bid' value='$selIdx'/>\n";
							//------------------------	
							$selEntry = false;
							if ($blogEntries !== false)
								foreach ($blogEntries as $entry)
									if ($entry->id == $selIdx)
										$selEntry = $entry;
							//------------------------	
							if ($selEntry === false && $hideManage === false)
							{
								//------------------------	
								$htmlTable = "<legend style='font-weight:bold;font-size:120%;'>Manage Blog Entries:</legend><br/>";
								$htmlTable .= "<table class='admin-table' style='width: 800px;'>\n";
								$htmlTable .= "<thead class='admin-scrolltable-header'>\n<tr>\n";
								//------------------------	
								foreach ($Columns as $col)
									$htmlTable .= "\t<th".($col[0] !== false ? " style='width:$col[0];'" : "").">$col[1]</th>\n";
								//------------------------	
								$htmlTable .= "</tr></thead>\n";
								//------------------------	
								$htmlTable .= "<tbody class='admin-scrolltable-body' style='height: 300px;'>\n";
								//------------------------	
								if ($blogEntries === false)
									$htmlTable .= getListRow(false, $Columns, $pagenum, $selIdx, $canManage); // empty row
								else
								{
									foreach ($blogEntries as $entry)
										if ($canManage || $Login->UserID == $entry->userID)
											$htmlTable .= getListRow($entry, $Columns, $pagenum, $selIdx, $canManage);
								}
								//---------------------------------
								$htmlTable .= "</tbody></table>\n";
								//---------------------------------
								echo $htmlTable;
								//---------------------------------
							} // if ($selEntry === false)
							//---------------------------------
							if ($selEntry !== false || $hideManage === false)
								echo "<input style='margin-top:10px;' type='submit' name='action' value='New' />\n";
							//---------------------------------
							if ($selEntry !== false || $hideManage === true)
								echo "<input style='margin-top:10px;' type='submit' name='bid' value='Manage Entries' /> \n";
							//---------------------------------
							if ($selEntry !== false)
							{
								//---------------------------------
								if (!$selEntry->loadContent())
									XLogError("Admin Blogs failed to loadContent (selected entry)");
								else
								{
									//---------------------------------
									$readonly = false;
									if (!$canAuthor)
										$readonly = true;
									//---------------
									$isPublished = $selEntry->isPublished();
									$isVisible = $selEntry->isVisible();
									$isHidden = $selEntry->hasFlag(BLOG_FLAG_HIDDEN);									
									//---------------------------------
									echo "<input style='margin-top:10px;' type='submit' name='action' value='Delete' /> \n";
									if ($canManage !== false)
										echo "<input style='margin-top:10px;' type='submit' name='action' value='Publish Index' /> \n";
									echo "<br/><div style='width:600px;padding:2px;'>\n";
									echo "<p style='width:100%;'>Blog $selEntry->id</p><br/>\n";
									echo "<span>Author (".$selEntry->userID."):".$selEntry->author."</span><br/>\n";
									echo "<span>Created:</span><input style='width:200px;' id='bdtCre' type='text' name='bdtCre' value='$selEntry->dtCreated' maxlength='".C_MAX_TITLE_TEXT_LENGTH."' readonly='readonly' /><br/>\n";
									echo "<br/>\n";
									echo "<span>Status: ".($isVisible ? "Visible" : "Not-Visible")." / ".($isPublished ? "Published" : "Unpublished").($isHidden ? " (Hidden)" : "")."</span><br/>\n";
									echo "<input style='margin-top:10px;' type='checkbox' name='repubIndex' value='repubIndex' checked='checked' />&nbsp;Auto republish blog index<br/>\n";
									echo "<input style='margin-top:10px;' type='submit' name='action' value='".($isPublished ? "Unpublish" : "Publish")."' /> ";
									echo "<input style='margin-top:10px;' type='submit' name='action' value='".($isHidden ? "Show" : "Hide")."' /><br/>\n";
									if (!$isPublished)
										echo "<span>Slug <input style='margin-top:10px;' type='submit' name='action' value='R' />:</span><input style='width:60%;' id='bdtSlug' type='text' name='bdtSlug' value='".($selEntry->slug === false ? "" : $selEntry->slug)."' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".($readonly !== false ? " readonly='readonly'" : "")." /><br/>\n";
									else
									{
										echo "<br/>\n";
										echo "<a href='".$selEntry->fullSlugFileName()."' target='_blank'>Preview: ".$selEntry->fullSlugFileName()."</a><br/>\n";
										echo "<input type='hidden' id='bdtSlug' name='bdtSlug' value='".($selEntry->slug === false ? "" : $selEntry->slug)."' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".($readonly !== false ? " readonly='readonly'" : "")." />\n";
									}
									echo "<br/>\n";
									echo "<span>Keywords:</span><br/>\n";
									echo "<input style='width:75%;' id='bKeywords' type='text' name='bKeywords' value='$selEntry->keywords' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".($readonly !== false ? " readonly='readonly'" : "")." /><br/>\n";
									echo "<br/>\n";
									echo "<span>Tags:</span><br/>\n";
									echo "<input style='width:75%;' id='bTags' type='text' name='bTags' value='$selEntry->tags' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".($readonly !== false ? " readonly='readonly'" : "")." /><br/>\n";
									echo "<br/>\n";
									echo "<span>Title:</span><br/>\n";
									echo "<input style='width:100%;' id='bTitle' type='text' name='bTitle' value='$selEntry->title' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".($readonly !== false ? " readonly='readonly'" : "")." /><br/>\n";
									echo "<br/>\n";
									echo "<textarea rows='15' cols='80' name='bContent' id='bContent' placeholder='Blog Content Here'".($readonly !== false ? " readonly='readonly'" : "").">".($selEntry->content === false ? "" : $selEntry->content)."</textarea><br/>\n";
									echo "<input style='margin-top:10px;' type='submit' name='action' value='Save' />\n";
									echo "</div>";
									//---------------------------------
								} // if (!$selEntry->loadContent())
							} // if ($selEntry !== false)
							else if ($canAuthor && $hideManage === true)
							{
									//---------------------------------
									if ($canManage !== false)
										echo "<input style='margin-top:10px;' type='submit' name='action' value='Publish Index' /><br/>\n";
									echo "<br/><br/><div style='width:600px;padding:2px;'>\n";
									echo "<p style='width:100%;'>New Blog Entry</p><br/>\n";
									echo "<br/>\n";
									echo "<input style='width:100%;padding:2px;' id='bTitle' type='text' name='bTitle' value='' maxlength='".C_MAX_TITLE_TEXT_LENGTH."' /><br/>\n";
									echo "<br/>\n";
									echo "<textarea rows='25' cols='80' name='bContent' id='bContent' placeholder='Blog Content Here'></textarea><br/>\n";
									echo "<input style='margin-top:10px;' type='submit' name='action' value='Create' /><br/>\n";
									echo "</div>";
									//---------------------------------
							}
							else if ($canManage !== false)
								echo "<input style='margin-top:10px;' type='submit' name='action' value='Publish Index' /> \n";
							//------------------------	
					?>
				</div>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
