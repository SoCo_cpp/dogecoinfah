<?php
//---------------------------------
/*
 * include/pages/admin/Contributions.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Contributions = new Contributions() or die("Create object failed");
$Ads = new Ads() or die("Create object failed");
$Wallet = new Wallet() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action');

$cSort = XGetPost("csrt");
if (!is_numeric($cSort))
	$cSort = 1;
$aSort = XGetPost("asrt");
if (!is_numeric($aSort))
	$aSort = 1;
$limit = XGetPost("lmt");
$defFilters = ($limit == "" ? true : false);
if ($limit === false || $limit == "" || !is_numeric($limit))
	$limit = 100;
else if ($limit == 0)
	$limit = false;
$cidx = XGetPost("cidx");
if (!is_numeric($cidx))
	$cidx = false;
$aidx = XGetPost("aidx");
if (!is_numeric($aidx))
	$aidx = false;
$selCont = false;
$selAd = false;
//---------------------------------
$contModeNamesLongNoSpecial = $Contributions->getModeNames(false /*short*/, false /*includeSpecial*/);
$contModeNamesShort = $Contributions->getModeNames(true /*short*/);
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="float:none; width:740px;"><!-- Start Left Column -->
		<div>
			<h1>Contributions:</h1>
			<p>
			Folding @ Home Contribution Configuration.
			</p>
			<br/>
		</div>
		<?php
		//---------------------------------
		$contList = $Contributions->findRoundContributions(-1 /*roundIdx*/);
		$canManage = $Login->HasPrivilege(PRIV_WALLET_MANAGE);
		//---------------------------------
		if ($action == "Add Contribution")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				if (!$Contributions->addContribution(-1 /*not round related*/, 50 /*order*/, "new contribution", "", CONT_MODE_NONE, CONT_FLAG_DISABLED, 0.0, 0.0, 0))
				{
					XLogError("Admin Contributions addContribution failed");
					echo "<div>Add new contribution failed.</div>\n";
				}
				else
					echo "<div>New contribution added successfully.</div>\n";
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contributions action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contributions $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
			$contList = $Contributions->findRoundContributions(-1 /*roundIdx*/);
			foreach ($contList as $cont)
				if ($cont->name == "new contribution" && $cont->order == 50 && $cont->mode == CONT_MODE_NONE && $cont->value == 0.0)
				{
					$cidx = $cont->id;
					break;
				}
			//---------------------------------
		}
		else if ($cidx !== false && $action == "Update Contribution")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$name = XPost('cName');
				$order = XPost('cOrd');
				$mode = XPost('cMode');
				$flags = XPost('cFlg');
				$value = XPost('cVal');
				$subValue = XPost('cSubVal');
				$countValue = XPost('cCntVal');
				$address = XPost('cAddr');
				$adID = XPost('cAd');
				//---------------------------------
				if (!is_numeric($flags) || !is_numeric($mode) || !is_numeric($order) || !is_numeric($value) || trim($name) == '' || ($value != '' && (!is_numeric($value) || $value < 0)) || ($subValue != '' && (!is_numeric($subValue) || $subValue < 0)) || ($countValue != '' && (!is_numeric($countValue) || $countValue < 0)) || ($adID != '' && (!is_numeric($adID) || $adID < 0)))
					echo "<div>Validate value to '$action' failed.</div>\n";
				else
				{
					//---------------------------------
					$cont = $Contributions->getContribution($cidx);
					if ($cont === false)
					{
						XLogError("Admin Contributions '$action' id $cidx not found");
						echo "<div>Auto account '$action' not found.</div>\n";
					}
					else
					{
						$cont->name = $name;
						$cont->order = $order;
						$cont->mode = $mode;
						$cont->address = (trim($address) == '' ? false :  $address);
						$cont->value = (!is_numeric($value) ? false : $value);
						$cont->subvalue = (!is_numeric($subValue) ? false : $subValue);
						$cont->countvalue = (!is_numeric($countValue) ? false : (!is_integer($countValue) ? (int)floor($countValue) : (int)$countValue));
						$cont->flags = $flags;
						$cont->ad = (!is_numeric($adID) ? false : $adID);
						if (!$cont->Update())
						{
							XLogError("Admin Contributions '$action' contribution Update id $cidx failed");
							echo "<div>contribution '$action' failed.</div>\n";
						}
						else 
						{
							XLogNotify("Admin Contributions Contribution Update id $cidx success");
							echo "<div>Contribution '$action' successfully.</div>\n";
						}
					}
					//---------------------------------
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contributions action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contributions $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($cidx !== false && $action == "Delete Contribution")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$cont = $Contributions->getContribution($cidx);
				if ($cont === false)
				{
					XLogError("Admin Contributions '$action' id $cidx not found");
					echo "<div>Auto account '$action' not found.</div>\n";
				}
				else if (!$Contributions->deleteContribution($cidx))
				{
					XLogError("Admin Contributions '$action' Contributions deleteContribution id $cidx failed");
					echo "<div>contribution '$action' failed.</div>\n";
				}
				else 
				{
					XLogNotify("Admin Contributions '$action' id $cidx success");
					echo "<div>Contribution '$action' successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contributions action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contributions $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Add Ad")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				if (!$Ads->addAd())
				{
					XLogError("Admin Contributions Ads addAd failed");
					echo "<div>Add new ad failed.</div>\n";
				}
				else
					echo "<div>New ad added successfully.</div>\n";
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contributions action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contributions $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($aidx !== false && $action == "Update Ad")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$title = XPost('aTitle');
				$style = XPost('aStyle');
				$flags = XPost('aFlags');
				$link = XPost('aLink');
				$image = XPost('aImage');
				$text = XPost('aText');
				//---------------------------------
				if (!is_numeric($flags))
					echo "<div>Validate value to '$action' failed.</div>\n";
				else
				{
					//---------------------------------
					$ad = $Ads->getAd($aidx);
					if ($ad === false)
					{
						XLogError("Admin Contributions '$action' id $aidx not found");
						echo "<div>Auto account '$action' not found.</div>\n";
					}
					else
					{
						$ad->title = $title;
						$ad->style = $style;
						$ad->flags = $flags;
						$ad->link = $link;
						$ad->image = $image;
						$ad->text = $text;
						if (!$ad->Update())
						{
							XLogError("Admin Contributions '$action' add Update id $aidx failed");
							echo "<div>contribution '$action' failed.</div>\n";
						}
						else 
						{
							XLogNotify("Admin Contributions $action id $aidx success");
							echo "<div>Contribution '$action' successfully.</div>\n";
						}
					}
					//---------------------------------
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contribution action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contribution $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($aidx !== false && $action == "Delete Ad")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$delAd = $Ads->getAd($aidx);
				if ($delAd === false)
				{
					XLogError("Admin Contributions '$action' id $aidx not found");
					echo "<div>Auto account '$action' not found.</div>\n";
				}
				else
				{
					if (!$Ads->deleteAd($aidx))
					{
						XLogError("Admin Contributions '$action' add deleteAd id $aidx failed");
						echo "<div>contribution '$action' failed.</div>\n";
					}
					else 
					{
						XLogNotify("Admin Contributions $action id $aidx success");
						echo "<div>Contribution '$action' successfully.</div>\n";
					}
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Contribution action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Contribution $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		//---------------------------------
		?>
		</div> <!-- End Left Column -->
		<div class="col1 pad_left1" style="float:none;width:100%;">
		<div class="admin-edit" style="width:720px;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;">Manage Contributions:</legend>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>" /><!-- Page -->
				<input type="hidden" name="cidx" value="<?php echo ($cidx === false ? "" : $cidx); ?>" />
				<input type="hidden" name="csort" value="<?php echo $cSort; ?>" />
				<input type="hidden" name="aidx" value="<?php echo ($aidx === false ? "" : $aidx); ?>" />
				<input type="hidden" name="asort" value="<?php echo $aSort; ?>" />
				<input type="hidden" name="lmt" value="<?php echo ($limit === false ? "0" : $limit); ?>" />
					<?php
							//------------------------	
							$Columns = array(	array(	"75px",		"ID",		1),
												array(	"180px",	"Name",		2),
												array(	"70px",		"Order",	3),
												array(	"200px",	"Mode",		4),
												array(	"100%",		"Value",	5)
												);
							//------------------------	
							echo "<script>\nfunction selCont(id, sort=$cSort){\n document.location.href='./Admin.php?p=$pagenum&cidx=' + id + '&csrt=' + sort + '&lmt=".($limit === false ? 0 : $limit)."&".SecToken()."'; }\n</script>\n";
							//------------------------	
							echo "<div><table class='admin-table' style='width:700px;'>\n<thead class='admin-scrolltable-header'>\n<tr>\n";// style='width:100%;'>\n";
							//------------------------	
							foreach ($Columns as $col)
							{
								$style = "";
								if ($col[0] !== false)
									$style .= ($col[0] == '100%' ? "width:$col[0];" : "min-width:$col[0];");
								if (abs($cSort) == $col[2])
									$style .= "background:#EEEEEE;";
								echo "\t<th".($style != "" ? " style='$style'" : "");
								echo " onclick=\"selCont(".($cidx === false ? "''" : $cidx).", ".(abs($cSort) == $col[2] ? (-1 * $cSort) : $col[2]).");\">$col[1]</th>\n";
							}
							//------------------------	
							echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
							//------------------------	
							$absCSort = abs($cSort);
							if ($absCSort == 1)
								$sort = DB_CONTRIBUTION_ID;
							else if ($absCSort == 2)
								$sort = DB_CONTRIBUTION_NAME;
							else if ($absCSort == 2)
								$sort = DB_CONTRIBUTION_ORDER;
							else if ($absCSort == 2)
								$sort = DB_CONTRIBUTION_MODE;
							else if ($absCSort == 2)
								$sort = DB_CONTRIBUTION_VALUE.','.DB_CONTRIBUTION_SUBVALUE;
							else 
								$sort = false;
							if ($sort !== false && $cSort < 0)
								$sort .= " DESC";
							//------------------------	
							if ($defFilters && $cidx !== false)
							{
								$singleCont = $Contributions->getContribution($cidx);
								if ($singleCont !== false)
									$clist = array($singleCont);
								else
								{
									XLogError("Contributions admin page - Contributions getContribution failed: ".XVarDump($cidx));
									$clist = array();
									$cidx = false;
								}
							}
							else
								$clist = $Contributions->getContributions(-1 /*roundIdx*/, true /*includeSpecial*/, false /*where*/, $sort, $limit);
							if ($clist === false)
								XLogError("Contributions admin page - Contributions getContribution failed");
							if ($clist === false || sizeof($clist) == 0)
							{
								//------------------------
								echo "<tr class='admin-row'>";
								foreach ($Columns as $col)
									echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
								echo "</tr>\n";
								//------------------------
							}
							else
							{
								//------------------------
								$rowIdx = 0;
								foreach ($clist as $cont)
								{
									//------------------------
									if ($cidx !== false && $cidx == $cont->id)
									{
										$selCont = $cont;
										$rowClass = 'admin-row-sel';
									}
									else $rowClass = 'admin-row';
									//------------------------
									//------------------------
									echo "<tr class='$rowClass' onclick=\"selCont($cont->id);\">\n";
									//------------------------
									$strName = XEncodeHTML($cont->name);
									$strMode = (isset($contModeNamesShort[$cont->mode]) ? $contModeNamesShort[$cont->mode] : 'Unknown ('.$cont->mode.')');
									if ($cont->isDisabled())
									{
										if ($cont->mode == CONT_OUTCOME_NONE)
											$strMode = '(disabled)'; // clear action name and just put disabled
										else
											$strMode .= ' (disabled)';
									}
									$strMode = XEncodeHTML($strMode);
									$strValue = $cont->value;
									//------------------------
									$colData = array(	$cont->id,
														$strName,
														$cont->order,
														$strMode,
														$strValue
													);
									//------------------------
									for ($i = 0;$i < sizeof($colData);$i++)
									{
										$style = "";
										if ($rowIdx == 0 && $Columns[$i][0] !== false)
											$style .= ($Columns[$i][0] == '100%' ? "width:".$Columns[$i][0].";" : "min-width:".$Columns[$i][0].";");
										if ($style != "")
											$style = " style='$style'";
										echo "<td$style>".$colData[$i]."</td>";
									}
									//------------------------
									echo "</tr>\n";
									//------------------------
									$rowIdx++;
									//------------------------
								} // foreach clist
								//------------------------
							} // $clist isn't empty
							//------------------------
							echo "</tbody></table>\n"; // end of contribution list table
							//------------------------
							if ($selCont !== false)
							{
								//------------------------
								$selBalance = $selCont->getBalance(0 /*ignore fee*/);
								if ($selBalance === false)
									$selBalance = '<error>';
								else
									$selBalance = XEncodeHTML(sprintf("%0.8f", $selBalance));
								//------------------------
								$selName = XEncodeHTML($selCont->name);
								$selOrder = ($selCont->order === false ? 50 : $selCont->order);
								$selAdID = ($selCont->ad === false ? "" : $selCont->ad);
								$selMode = $selCont->mode;
								$selFlags = $selCont->flags;
								$selValue = $selCont->value;
								$selSubValue = $selCont->subvalue;
								$selCountValue = $selCont->countvalue;
								$selAddress = ($selCont->address === false ? "" : XEncodeHTML($selCont->address));
								//------------------------
								$selFlagDisabled = $selCont->hasFlag(CONT_FLAG_DISABLED);
								$selFlagWaitForFunds = $selCont->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS);
								$selFlagFullBalance = $selCont->hasFlag(CONT_FLAG_FULL_BALANCE);
								$selFlagMaxTotal = $selCont->hasFlag(CONT_FLAG_MAX_TOTAL);
								$selFlagMaxCount = $selCont->hasFlag(CONT_FLAG_MAX_COUNT);
								//------------------------
								$valueDescription = '';
								$subValueDescription = '';
								$countValueDescription = '';
								//------------------------
								$valueReadOnly = !$canManage;
								$subValueReadOnly = !$canManage;
								$countValueReadOnly = !$canManage;
								//------------------------
								$flagFullBalReadOnly = !$canManage;
								$flagMaxTotReadOnly = !$canManage;
								$flagMaxCountReadOnly = !$canManage;
								//------------------------
								echo "<script>\n";
								echo "var flagUpdating = false;\n";
								echo "function flagChanged(){\n";
								echo "if (flagUpdating) return;\n";
								echo "flagUpdating = true;\n";
								echo "var val = ".CONT_FLAG_NONE.";\n";
								echo "if (document.getElementById('cfDisable').checked) val |= ".CONT_FLAG_DISABLED.";\n";
								echo "if (document.getElementById('cfWaitFund').checked) val |= ".CONT_FLAG_WAIT_FOR_FUNDS.";\n";
								echo "if (document.getElementById('cfFullBal').checked) val |= ".CONT_FLAG_FULL_BALANCE.";\n";
								echo "if (document.getElementById('cfMaxTot').checked) val |= ".CONT_FLAG_MAX_TOTAL.";\n";
								echo "if (document.getElementById('cfMaxCnt').checked) val |= ".CONT_FLAG_MAX_COUNT.";\n";
								//echo "if (document.getElementById('').checked) val |= "..";\n";
								echo "document.getElementById('cFlg').value = val;\n";
								echo "flagUpdating = false;\n";
								echo "}\n";
								echo "function flagCodeChanged(){\n";
								echo "if (flagUpdating) return;\n";
								echo "flagUpdating = true;\n";
								echo "var val = document.getElementById('cFlg').value;\n";
								echo "document.getElementById('cfDisable').checked = ((val & ".CONT_FLAG_DISABLED.") != 0);\n";
								echo "document.getElementById('cfWaitFund').checked = ((val & ".CONT_FLAG_WAIT_FOR_FUNDS.") != 0);\n";
								echo "document.getElementById('cfFullBal').checked = ((val & ".CONT_FLAG_FULL_BALANCE.") != 0);\n";
								echo "document.getElementById('cfMaxTot').checked = ((val & ".CONT_FLAG_MAX_TOTAL.") != 0);\n";
								echo "document.getElementById('cfMaxCnt').checked = ((val & ".CONT_FLAG_MAX_COUNT.") != 0);\n";
								echo "flagUpdating = false;\n";
								echo "}\n";
								echo "</script>\n";
								//------------------------
								echo "<div style='width:520px;'>\n";
								echo "<h4>Contribution $cidx: $selName</h4>\n";
								echo "<table class='admin-table' style='width:420px;'>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='cName'>Name</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:98%;' id='cName' type='text' name='cName' value='$selName' maxlength='45'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cOrd'>Order</label>\n</td><td>";
								echo "<input style='width:60px;' id='cOrd' type='text' name='cOrd' value='$selOrder' maxlength='45'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cAd'>Ad ID</label>\n</td><td>";
								echo "<input style='width:60px;' id='cAd' type='text' name='cAd' value='$selAdID' maxlength='8'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:80px;'>Mode</td><td><select name='cMode' id='cMode' ".(!$canManage ? " disabled" : "").">\n";
								//---------
								foreach ($contModeNamesLongNoSpecial as $val => $name)
									echo "<option value='$val'".($selMode == $val ? " selected" : "").">$name</option>\n";
								//---------
								echo "</select></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cFlg'>Flags</label>\n</td><td>";
								echo "<input style='width:60px;' id='cFlg' name='cFlg' type='text' onchange='flagCodeChanged()' value='$selFlags' maxlength='8'".(!$canManage ? " readonly='readonly'" : "")." />&nbsp;";
								echo "<input id='cfDisable' name='cfDisable' type='checkbox' onchange='flagChanged()'".($selFlagDisabled !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Disabled&nbsp;";
								echo "<input id='cfWaitFund' name='cfWaitFund' type='checkbox' onchange='flagChanged()'".($selFlagWaitForFunds !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Wait For Funds</td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cVal'>Value</label>\n</td><td>";
								echo "<input style='width:150px;' id='cVal' type='text' name='cVal' value='$selValue' maxlength='16'".($valueReadOnly ? " readonly='readonly'" : "")." />&nbsp;";
								echo "<input id='cfFullBal' name='cfFullBal' type='checkbox' onchange='flagChanged()'".($selFlagFullBalance !== false ? " checked='checked'" : "").($flagFullBalReadOnly !== false ? " readonly='readonly'" : "")." />Full Balance&nbsp;$valueDescription</td></tr>\n";
								//---------
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cSubVal'>Sub Value</label>\n</td><td>";
								echo "<input style='width:150px;' id='cSubVal' type='text' name='cSubVal' value='$selSubValue' maxlength='16'".($subValueReadOnly ? " readonly='readonly'" : "")." />&nbsp;";
								echo "<input id='cfMaxTot' name='cfMaxTot' type='checkbox' onchange='flagChanged()'".($selFlagMaxTotal !== false ? " checked='checked'" : "").($flagMaxTotReadOnly !== false ? " readonly='readonly'" : "")." />Max Total&nbsp;$subValueDescription</td></tr>\n";
								//---------
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cCntVal'>Count Val</label>\n</td><td>";
								echo "<input style='width:150px;' id='cCntVal' type='text' name='cCntVal' value='$selCountValue' maxlength='8'".($countValueReadOnly ? " readonly='readonly'" : "")." />&nbsp;";
								echo "<input id='cfMaxCnt' name='cfMaxCnt' type='checkbox' onchange='flagChanged()'".($selFlagMaxCount !== false ? " checked='checked'" : "").($flagMaxCountReadOnly !== false ? " readonly='readonly'" : "")." />Max Count&nbsp;$countValueDescription</td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cAddr'>Address</label>\n</td><td>";
								echo "<input style='width:98%;' id='cAddr' type='text' name='cAddr' value='$selAddress' maxlength='45'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td><label class='loginLabel' for='cBal'>Balance</label>\n</td><td>$selBalance</td></tr>\n";
								//------------------------	
								echo "</table>\n";
								//------------------------	
								if ($canManage)
									echo "<input style='margin: 2px;' type='submit' name='action' value='Update Contribution' />&nbsp;&nbsp;<input style='margin: 2px;' type='submit' name='action' value='Delete Contribution' /><br/>\n";
								//------------------------	
								echo "<br/></div>\n";
								//------------------------	
							} // if ($selCont !== false)
							//------------------------	
							if ($canManage)
								echo "<input style='margin: 2px;' type='submit' name='action' value='Add Contribution' /><br/>\n";
							//------------------------
							echo "</div>\n"; // Contributions container
							//------------------------	
							echo "<hr style='margin-top:8px;height:8px;border-top:1px solid black;text-align:center;' />\n";
							//------------------------	

					// ----- Ads---------------------------------------------------------------------------------
							//------------------------	
							$Columns = array(	array(	"75px",		"ID",		1),
												array( "100%",	 "Title",		2)
												);
							//------------------------	
							echo "<script>\nfunction selAd(id, sort=$aSort){\n document.location.href='./Admin.php?p=$pagenum&aidx=' + id + '&asrt=' + sort + '&cidx=$cidx&csrt=$cSort&lmt=".($limit === false ? 0 : $limit)."&".SecToken()."'; }\n</script>\n";
							//------------------------	
							echo "<div><h3>Manage Ads:</h3><table class='admin-table' style='width:700px;'>\n<thead class='admin-scrolltable-header'>\n<tr>\n";// style='width:100%;'>\n";
							//------------------------	
							foreach ($Columns as $col)
							{
								$style = "";
								if ($col[0] !== false)
									$style .= ($col[0] == '100%' ? "width:$col[0];" : "min-width:$col[0];");
								if (abs($aSort) == $col[2])
									$style .= "background:#EEEEEE;";
								echo "\t<th".($style != "" ? " style='$style'" : "");
								echo " onclick=\"selAd(".($aidx === false ? "''" : $aidx).", ".(abs($aSort) == $col[2] ? (-1 * $aSort) : $col[2]).");\">$col[1]</th>\n";
							}
							//------------------------	
							echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
							//------------------------	
							$absaSort = abs($aSort);
							if ($absaSort == 1)
								$sort = DB_ADS_ID;
							else 
								$sort = false;
							if ($sort !== false && $aSort < 0)
								$sort .= " DESC";
							//------------------------	
							if ($defFilters && $aidx !== false)
							{
								$singleAd = $Ads->getAd($aidx);
								if ($singleAd !== false)
									$adList = array($singleAd);
								else
								{
									XLogError("Contributions admin page - Ads getAd failed: ".XVarDump($aidx));
									$adList = array();
									$aidx = false;
								}
							}
							else
								$adList = $Ads->getAds(); // sort, limit
							if ($adList === false)
								XLogError("Contributions admin page - Ads getAds/getAd failed");
							if ($adList === false || sizeof($adList) == 0)
							{
								//------------------------
								echo "<tr class='admin-row'>";
								foreach ($Columns as $col)
									echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
								echo "</tr>\n";
								//------------------------
							}
							else
							{
								//------------------------
								$rowIdx = 0;
								foreach ($adList as $ad)
								{
									//------------------------
									if ($aidx !== false && $aidx == $ad->id)
									{
										$selAd = $ad;
										$rowClass = 'admin-row-sel';
									}
									else $rowClass = 'admin-row';
									//------------------------
									//------------------------
									echo "<tr class='$rowClass' onclick=\"selAd($ad->id);\">\n";
									//------------------------
									$strTitle = ($ad->title === false || $ad->title == '' ? '&nbsp;' : (strlen($ad->title) > 15 ? XEncodeHTML(substr($ad->title, 0, 15)).'...' : XEncodeHTML($ad->title)));
									//------------------------
									$colData = array(	$ad->id,
														$strTitle
													);
									//------------------------
									for ($i = 0;$i < sizeof($colData);$i++)
									{
										$style = "";
										if ($rowIdx == 0 && $Columns[$i][0] !== false)
											$style .= ($Columns[$i][0] == '100%' ? "width:".$Columns[$i][0].";" : "min-width:".$Columns[$i][0].";");
										if ($style != "")
											$style = " style='$style'";
										echo "<td$style>".$colData[$i]."</td>";
									}
									//------------------------
									echo "</tr>\n";
									//------------------------
									$rowIdx++;
									//------------------------
								} // foreach adList
								//------------------------
							} // else // if ($adList === false || sizeof($adList) == 0)
							//------------------------	
							echo "</tbody></table>\n"; // end of ad list table
							//------------------------
							if ($selAd !== false)
							{
								//------------------------
								$selAdTitle = XEncodeHTML($selAd->title);
								$selAdFlags = $selAd->flags;
								$selAdText = XEncodeHTML($selAd->text);
								$selAdLink = XEncodeHTML($selAd->link);
								$selAdImage = XEncodeHTML($selAd->image);
								$selAdStyle = XEncodeHTML($selAd->style);
								//------------------------
								$selAdFlagShowAlways = XMaskContains($selAdFlags, AD_FLAG_SHOW_ALWAYS);
								$selAdFlagShowTitle = XMaskContains($selAdFlags, AD_FLAG_SHOW_TITLE);
								$selAdFlagShowValue = XMaskContains($selAdFlags, AD_FLAG_SHOW_VALUE);
								$selAdFlagShowText = XMaskContains($selAdFlags, AD_FLAG_SHOW_TEXT);
								$selAdFlagShowImage = XMaskContains($selAdFlags, AD_FLAG_SHOW_IMAGE);
								$selAdFlagLinkTitle = XMaskContains($selAdFlags, AD_FLAG_LINK_TITLE);
								$selAdFlagLinkText = XMaskContains($selAdFlags, AD_FLAG_LINK_TEXT);
								$selAdFlagLinkImage = XMaskContains($selAdFlags, AD_FLAG_LINK_IMAGE);
								//------------------------
								echo "<script>\n";
								echo "var adFlagUpdating = false;\n";
								echo "function adFlagChanged(){\n";
								echo "if (adFlagUpdating) return;\n";
								echo "adFlagUpdating = true;\n";
								echo "var val = ".CONT_FLAG_NONE.";\n";
								echo "if (document.getElementById('afSAlways').checked) val |= ".AD_FLAG_SHOW_ALWAYS.";\n";
								echo "if (document.getElementById('afSTitle').checked) val |= ".AD_FLAG_SHOW_TITLE.";\n";
								echo "if (document.getElementById('afSValue').checked) val |= ".AD_FLAG_SHOW_VALUE.";\n";
								echo "if (document.getElementById('afSText').checked) val |= ".AD_FLAG_SHOW_TEXT.";\n";
								echo "if (document.getElementById('afSImage').checked) val |= ".AD_FLAG_SHOW_IMAGE.";\n";
								echo "if (document.getElementById('afLTitle').checked) val |= ".AD_FLAG_LINK_TITLE.";\n";
								echo "if (document.getElementById('afLText').checked) val |= ".AD_FLAG_LINK_TEXT.";\n";
								echo "if (document.getElementById('afLImage').checked) val |= ".AD_FLAG_LINK_IMAGE.";\n";
								echo "document.getElementById('aFlags').value = val;\n";
								echo "adFlagUpdating = false;\n";
								echo "}\n";
								echo "function adFlagCodeChanged(){\n";
								echo "if (adFlagUpdating) return;\n";
								echo "adFlagUpdating = true;\n";
								echo "var val = document.getElementById('aFlags').value;\n";
								echo "document.getElementById('afSAlways').checked = ((val & ".AD_FLAG_SHOW_ALWAYS.") != 0);\n";
								echo "document.getElementById('afSTitle').checked = ((val & ".AD_FLAG_SHOW_TITLE.") != 0);\n";
								echo "document.getElementById('afSValue').checked = ((val & ".AD_FLAG_SHOW_VALUE.") != 0);\n";
								echo "document.getElementById('afSText').checked = ((val & ".AD_FLAG_SHOW_TEXT.") != 0);\n";
								echo "document.getElementById('afSImage').checked = ((val & ".AD_FLAG_SHOW_IMAGE.") != 0);\n";
								echo "document.getElementById('afLTitle').checked = ((val & ".AD_FLAG_LINK_TITLE.") != 0);\n";
								echo "document.getElementById('afLText').checked = ((val & ".AD_FLAG_LINK_TEXT.") != 0);\n";
								echo "document.getElementById('afLImage').checked = ((val & ".AD_FLAG_LINK_IMAGE.") != 0);\n";
								echo "adFlagUpdating = false;\n";
								echo "}\n";
								echo "</script>\n";
								//------------------------
								echo "<div style='width:520px;'>\n";
								echo "<h4>Ad $aidx</h4>\n";
								echo "<table class='admin-table' style='width:420px;'>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aTitle'>Title</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:98%;' id='aTitle' name='aTitle' type='text' value='$selAdTitle' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aStyle'>Style</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:98%;' id='aStyle' name='aStyle' type='text' value='$selAdStyle' maxlength='".C_MAX_TITLE_TEXT_LENGTH."'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aFlags'>Flags</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:80px;' id='aFlags' name='aFlags' onchange='adFlagCodeChanged()' type='text' value='$selAdFlags' maxlength='5'".(!$canManage ? " readonly='readonly'" : "")." /><br/>";
								echo "<div>\n";
								echo "<div style='float:left;'>\n";
								echo "<input id='afSAlways' name='afSAlways' type='checkbox' onchange='adFlagChanged()'".($selAdFlagShowAlways !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Show Always<br/>";
								echo "<input id='afSTitle' name='afSTitle' type='checkbox' onchange='adFlagChanged()'".($selAdFlagShowTitle !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Show Title<br/>";
								echo "<input id='afSValue' name='afSValue' type='checkbox' onchange='adFlagChanged()'".($selAdFlagShowValue !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Show Value<br/>";
								echo "<input id='afSText' name='afSText' type='checkbox' onchange='adFlagChanged()'".($selAdFlagShowText !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Show Text<br/>";
								echo "<input id='afSImage' name='afSImage' type='checkbox' onchange='adFlagChanged()'".($selAdFlagShowImage !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Show Image<br/>";
								echo "</div>\n";
								echo "<div style='float:right;min-width:40%''>\n";
								echo "<input id='afLTitle' name='afLTitle' type='checkbox' onchange='adFlagChanged()'".($selAdFlagLinkTitle !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Link Title<br/>";
								echo "<input id='afLText' name='afLText' type='checkbox' onchange='adFlagChanged()'".($selAdFlagLinkText !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Link Text<br/>";
								echo "<input id='afLImage' name='afLImage' type='checkbox' onchange='adFlagChanged()'".($selAdFlagLinkImage !== false ? " checked='checked'" : "").($canManage !== false ? " readonly='readonly'" : "")." />Link Image";
								echo "</div>\n";
								echo "</td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aLink'>Link</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:98%;' id='aLink' name='aLink' type='text' value='$selAdLink' maxlength='".C_MAX_AD_LINK_TEXT_LENGTH."'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aImage'>Image</label>\n</td><td style='width:100%;'>";
								echo "<input style='width:98%;' id='aImage' name='aImage' type='text' value='$selAdImage' maxlength='".C_MAX_AD_IMAGE_TEXT_LENGTH."'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "<tr class='admin-row'><td style='width:90px;'><label class='loginLabel' for='aText'>Text</label>\n</td><td style='width:100%;'>";
								echo "<textarea rows='8' cols='40' name='aText' id='aText' placeholder='Ad text block content here'".(!$canManage ? " readonly='readonly'" : "").">$selAdText</textarea></td></tr>\n";
								//echo "<input style='width:98%;' id='aText' name='aText' type='text' value='$selAdText' maxlength='".C_MAX_AD_TEXT_LENGTH."'".(!$canManage ? " readonly='readonly'" : "")." /></td></tr>\n";
								//------------------------	
								echo "</table>\n";
								//------------------------	
								if ($canManage)
									echo "<input style='margin-top:10px;' type='submit' name='action' value='Update Ad' />&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Delete Ad' /><br/>\n";
								//------------------------
							} // if ($selAd !== false)
							//------------------------	
							echo "<input style='margin-top:10px;' type='submit' name='action' value='Add Ad' />\n";
							echo "</div>\n"; // end Ads container
							//------------------------	
					?>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
