<?php
//---------------------------------
/*
 * include/pages/admin/ContPayouts.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Contributions = new Contributions() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$Wallet = new Wallet() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action', false);
$showConts = (XGetPost('sc') != "" ? true : false);
$cIdx = XGetPost('cidx');
if (!is_numeric($cIdx))
	$cIdx = false;
$dridx = XGetPost("dridx"); // delete idx
if ($dridx == "")
	$dridx = false;
$limit = XGetPost('lmt');
$defFilters = ($limit == "" ? true : false);
if (!is_numeric($limit))
	$limit = 20;
else if ($limit == 0)
	$limit = false;
$fltRnd = XGetPost('fltRnd');
if (!is_numeric($fltRnd))
	$fltRnd = false;
$listSort = XGetPost('sort');
if (!is_numeric($listSort))
	$listSort = -1;
$selCont = false;
if ($defFilters && $cIdx !== false)
	$limit = "";
//---------------------------------
// we are inside a nested encapsulation.
// for getContributionRow's global import to work
// we must make this global 
global $contModeNames;
global $contOutcomeNames;
$contModeNames = $Contributions->getModeNames();
$contOutcomeNames = $Contributions->getOutcomeNames();
//---------------------------------
function getContributionRow($Display, $idx, $roundIdx, $number, $name, $mode, $value, $address, $flags, $adID, $outcome, $dtDone, $txid, $total)
{
	global $contModeNames;
	global $contOutcomeNames;
	//------------------------
	$number = XEncodeHTML($number);
	$name = XEncodeHTML($name);
	$address = XEncodeHTML($address);
	$adID = XEncodeHTML($adID);
	//------------------------
	if ($total !== false && $total != $value)
		$value = XEncodeHTML($value)."(".XEncodeHTML($total).")";
	else
		$value = XEncodeHTML($value);
	//------------------------
	if (!isset($contModeNames[$mode]))
		XLogDebug("ContPayout getContributionRow mode: ".XVarDump($mode).", contModeNames: ".XVarDump($contModeNames).", contOutcomeNames: ".XVarDump($contOutcomeNames));
	$modeText = (is_numeric($mode) && isset($contModeNames[$mode]) ? $contModeNames[$mode] : "(Unknown ".XEncodeHTML($mode).")");
	$strOutcome = (is_numeric($outcome) && isset($contOutcomeNames[$outcome]) ? $contOutcomeNames[$outcome] : "(Unknown ".XEncodeHTML($outcome).")");
	//------------------------
	$flagsText = "";
	if (XMaskContains($flags, WALLET_AUTO_FLAG_REQUIRED))
		$flagsText .= " (Required)";
	//------------------------
	if ($txid !== "0" && $txid !== "" && $txid !== false)
		$txidText = (strpos($txid, "[") !== false ? XEncodeHTML($txid) : "<a href='".BRAND_TX_LINK.XEncodeHTML($txid)."'>".XEncodeHTML($txid)."...</a>");
	else
		$txidText = "&nbsp;";
	//------------------------
	if (XMaskContains($flags, CONT_FLAG_MANUALLY_BYPASSED))
		$strTableOutcome = "Manually Bypassed";
	else
		$strTableOutcome = $strOutcome;
	if ($dtDone !== false)
		$strTableOutcome .= " (Done)";
	//------------------------
	return array($idx, $roundIdx, $name, $modeText.$flagsText, $value, $strTableOutcome, $address, $txidText, $number, $adID, $strOutcome);
}
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="float:none; width:780px;"><!-- Start Left Column -->
		<div>
			<h1>Contributions:</h1>
			<p>
			Folding @ Home Contribution Configuration.
			</p>
			<br/>
		</div>
				<div>
					<div>(Click row to select for editing or deleting. Click header to sort.)</div>
					<?php
							//------------------------	
							$canDelete = $Login->HasPrivilege(PRIV_DELETE_HISTORY);
							//---------------------------------
							if ($action == "Clear Payment")
							{
								//---------------------------------
								if ($canDelete)
								{
									//---------------------------------
									$selCont = $Contributions->getContribution($cIdx);
									if ($selCont === false)
									{
										XLogError("Admin Contribution $action Contributions getContribution failed: ".XVarDump($cIdx));
										echo "<div>$action failed.</div>\n";
									}
									else
									{
										//---------------------------------
										$selCont->dtDone = false;
										$selCont->outcome = CONT_OUTCOME_NONE;
										$selCont->txid = false;
										if (!$selCont->Update())
										{
											XLogError("Admin Contribution $action selected contribution Update failed");
											echo "<div>$action failed.</div>\n";
										}
										else
											echo "<div>Cleared Contribution Payout successfully.</div>\n";
										//---------------------------------
									}
									//---------------------------------
								}
								else
								{
									XLogNotify("Admin Contribution Payout action '$action' doesn't have proper privileges: $Login->UserName");
									echo "Contribution Payout $action failed. Action requires proper privileges.<br/>";
								}
								//---------------------------------
							}
							if ($action == "Manually Bypass")
							{
								//---------------------------------
								if ($canDelete)
								{
									//---------------------------------
									$selCont = $Contributions->getContribution($cIdx);
									if ($selCont === false)
									{
										XLogError("Admin Contribution $action Contributions getContribution failed: ".XVarDump($cIdx));
										echo "<div>$action failed.</div>\n";
									}
									else
									{
										//---------------------------------
										$bypassNote = XGetPost('bypnt', '');
										//---------------------------------
										if (!$selCont->manuallyBypass($bypassNote))
										{
											XLogError("Admin Contribution $action selected contribution manuallyBypass failed");
											echo "<div>$action failed.</div>\n";
										}
										else
											echo "<div>Contribution Payout manually bypass successfully.</div>\n";
										//---------------------------------
									}
									//---------------------------------
								}
								else
								{
									XLogNotify("Admin Contribution Payout action '$action' doesn't have proper privileges: $Login->UserName");
									echo "Contribution Payout $action failed. Action requires proper privileges.<br/>";
								}
								//---------------------------------
							}
							if ($action == "Set Outcome")
							{
								//---------------------------------
								if ($canDelete)
								{
									//---------------------------------
									$selCont = $Contributions->getContribution($cIdx);
									if ($selCont === false)
									{
										XLogError("Admin Contribution $action Contributions getContribution failed: ".XVarDump($cIdx));
										echo "<div>$action failed.</div>\n";
									}
									else
									{
										//---------------------------------
										$clrDone = (XPost('clrdone', false) !== false ? true : false);;
										$setDone = (XPost('setdone', false) !== false ? true : false);;
										$newOutcome = XPost('noc', false);
										//---------------------------------
										if (!is_numeric($newOutcome))
										{
											XLogError("Admin Contribution $action validate newOutcome failed: ".XVarDump($newOutcome));
											echo "<div>$action failed.</div>\n";
										}
										else
										{
											//---------------------------------
											$wasOutcome = $selCont->outcome;
											$wasDone = $selCont->dtDone;
											//---------------------------------
											$selCont->outcome = $newOutcome;
											if ($clrDone)
												$selCont->dtDone = false;
											else if ($setDone)
												$selCont->dtDone = time();
											//---------------------------------
											XLogNotify("Admin Contribution Payout manually setting cont $selCont->id, outcome $wasOutcome -> $newOutcome,  dtDone ".XVarDump($wasDone).($clrDone ? " -> Cleared" : ($setDone ? " -> Set to ".XVarDump($selCont->dtDone) : " (no change)")).", by user: $Login->UserName");
											//---------------------------------
											if (!$selCont->Update())
											{
												XLogError("Admin Contribution $action selected contribution Update failed");
												echo "<div>$action failed.</div>\n";
											}
											else
												echo "<div>Contribution Payout outcome set successfully.</div>\n";
											//---------------------------------
										}
									}
									//---------------------------------
								}
								else
								{
									XLogNotify("Admin Contribution Payout action '$action' doesn't have proper privileges: $Login->UserName");
									echo "Contribution Payout $action failed. Action requires proper privileges.<br/>";
								}
								//---------------------------------
							}
							else if ($action == "Delete")
							{
								//---------------------------------
								if ($canDelete)
								{
									//---------------------------------
									if (!$Contributions->deleteContribution($cIdx))
									{
										XLogError("Admin Contribution Payout deleteContribution failed");
										echo "<div>Delete Contribution Payout failed.</div>\n";
									}
									else
										echo "<div>Contribution Payout deleted successfully.</div>\n";
									//---------------------------------
								}
								else
								{
									XLogNotify("Admin Contribution Payout action '$action' doesn't have proper privileges: $Login->UserName");
									echo "Contribution Payout $action failed. Action requires proper privileges.<br/>";
								}
								//---------------------------------
								$cIdx = false;
								//---------------------------------
							}
							else if ($action == "Delete All From Round")
							{
								//---------------------------------
								if ($canDelete)
								{
									//---------------------------------
									if ($dridx !== false && is_numeric($dridx))
									{
										//---------------------------------
										if (!$Contributions->deleteAllRound($dridx))
										{
											XLogError("Admin Contribution Payout deleteAllRound failed");
											echo "<div>Delete all contributions from round failed.</div>\n";
										}
										else
										{
											echo "<div>Deleted all contributions from round successfully.</div>\n";
										}
										//---------------------------------
									}
									else "Delete all contributions from round, validate round index failed.<br/><br/>\n";
									//---------------------------------
								}
								else
								{
									XLogNotify("Admin Contribution Payout action '$action' doesn't have proper privileges: $Login->UserName");
									echo "Contribution Payout $action failed. Action requires proper privileges.<br/>";
								}
								//---------------------------------
								$cIdx = false;
								//---------------------------------
							}
							//---------------------------------
							$cols = array(	array("ID", 55, "text-align:right;", 1),
											array("Rnd", 55, "text-align:right;", 2),
											array("Name", 140, false, 3),
											array("Mode", 200, false, 4),
											array("Value", 150, "text-align:right;", 5),
											array("Outcome", 220, "text-align:right;", 6),
											);
							//------------------------	
							if ($fltRnd === false)
								$where = false;
							else
								$where = DB_CONTRIBUTION_ROUND."=$fltRnd";
							//------------------------	
							if ($listSort !== false)
							{
								if (abs($listSort) == 1)
									$sort = DB_CONTRIBUTION_ID;
								else if (abs($listSort) == 2)
									$sort = DB_CONTRIBUTION_ROUND;
								else if (abs($listSort) == 3)
									$sort = DB_CONTRIBUTION_NAME;
								else if (abs($listSort) == 4)
									$sort = DB_CONTRIBUTION_MODE;
								else if (abs($listSort) == 5)
									$sort = DB_CONTRIBUTION_VALUE;
								else if (abs($listSort) == 6)
									$sort = DB_CONTRIBUTION_OUTCOME;
								else
									$sort = false;
									
								if ($sort !== false && $listSort < 0)
									$sort .= " DESC";
							}
							else $sort = false;
							//------------------------	
							if ($defFilters && $cIdx !== false)
							{
								$singleCont = $Contributions->getContribution($cIdx);
								if ($singleCont === false)
								{
									XLogError("Admin Contributions Contributions getContribution failed: ".XVarDump($cIdx));
									$contList = array();
									$cIdx = false;
								}
								else $contList = array($singleCont);
							}
							else $contList = $Contributions->getContributions(false /*roundIdx*/, ($showConts ? false : true) /*includeSpecial*/, $where, $sort, ($limit == "" ? false : $limit));
							if ($contList === false)
								XLogError("Admin Contributions Contributions failed to getContributions (a) (default)");
							else
							{
								//---------------------------------
								echo "<script>\nfunction selCont(id, sort=".($listSort === false ? "''" : $listSort)."){\n document.location.href='./Admin.php?p=$pagenum&cidx=' + id + '&lmt=".($limit === false ? 0 : $limit)."&";
								if ($fltRnd !== false) echo "fltRnd=$fltRnd&";
								if ($showConts !== false) echo "sc=1&";
								echo "sort=' + sort + '&";
								echo SecToken()."';\n}\n</script>\n";
								//------------------------
								echo "<table class='admin-table' style='width: 890px;'>\n";
								echo "<thead class='admin-scrolltable-header'><tr style='width:100%;'>\n";
								$colIdx = 0;
								foreach ($cols as $col)
								{
									echo "<th style='min-width:".$col[1]."px;";
									if ($listSort !== false && abs($listSort) == $col[3])
										echo "background:#EEEEEE;";
									echo "'";
									if ($col[3] !== false)
										echo " onclick=\"selCont(".($cIdx === false ? "''" : $cIdx).", ".($listSort !== false && $listSort == $col[3] ? (-1 * $col[3]) : $col[3]).");\"";
									echo ">".$col[0]."</th>\n";
								}
								echo "</tr></thead>\n";
								//------------------------	
								echo "<tbody class='admin-scrolltable-body' style='height: 400px;'>\n";
								//------------------------	
								$selCont = false;
								$rows = array();
								foreach ($contList as $cont)
								{
									$rowData = array($cont->id, getContributionRow($Display, $cont->id, $cont->roundIdx, $cont->order, $cont->name, $cont->mode, $cont->value, $cont->address, 
																			$cont->flags, $cont->ad, $cont->outcome, $cont->dtDone, $cont->txid, $cont->total));
									$rows[] = $rowData;
									if ($cIdx !== false && $cIdx == $cont->id)
									{
										$selCont = $cont;
										$selContData = $rowData[1];
									}
								}
								//---------------------------------
								foreach ($rows as $row)
								{
									$colIdx = 0;
									if ($cIdx !== false && $cIdx == $row[0])
										$rowClass = 'admin-row-sel';
									else 
										$rowClass = 'admin-row';
									echo "<tr class='$rowClass' onclick=\"selCont($row[0]);\">";
									foreach ($row[1] as $col)
									{
										if ($colIdx != 6 && $colIdx != 7 && $colIdx != 8 && $colIdx != 9&& $colIdx != 10)
										{
											$style = "min-width:".$cols[$colIdx][1]."px;max-width:".$cols[$colIdx][1]."px;width:".$cols[$colIdx][1]."px;";
											if ($cols[$colIdx][2] !== false)
												$style .= " ".$cols[$colIdx][2];
											echo "<td style='$style'>$col</td>";
										}
										$colIdx++;
									}
									echo "</tr>";
								}
								//------------------------
							}
							//---------------------------------
							echo "</tbody></table></div>\n";
							//---------------------------------
						?>
						<div class="col1 pad_left1" style="float:none;width:80%;"><!-- Start Right Column -->
						<div class="admin-edit" style="width:550px;">
						<form action="./Admin.php" method="post" enctype="multipart/form-data">
							<fieldset class="loginBox">
								<legend style="font-weight:bold;">List filters:</legend>
								<br/>
								<?php 
									PrintSecTokenInput(); 
									if ($listSort !== false) echo  "<input type='hidden' name='sort' value='$listSort' />";
								?>
								<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
								<input type="hidden" name="cidx" value="<?php echo ($cIdx === false ? "" : $cIdx); ?>"/>
								<div>
									<label class="loginLabel" for="lmt">Limit rows:</label>
									<input style="width:90px;margin-bottom:4px;" id="lmt" type="text" name="lmt" value="<?php echo ($limit === false ? 0 : $limit);?>" maxlength="6" /><br/>
									<label class="loginLabel" for="fltRnd">Filter Round:</label>
									<input style='width:80px;margin-bottom:4px;' id='fltRnd' type='text' name='fltRnd' value='<?php echo ($fltRnd === false ? "" : $fltRnd); ?>' maxlength='6'/><br/>
									<input id='sc' type='checkbox' name='sc'<?php echo ($showConts !== false ? " checked='checked'" : ""); ?> />Show Scheduled Contributions<br/>
									<input type='submit' name='noaction' value='Update Filters' />
								</div>
						</fieldset>
					</form>
					</div>
					</div>


		</div> <!-- End Left Column -->
		<div class="col1 pad_left1" style="float:none;width:80%;"><!-- Start Right Column -->
		<div class="admin-edit" style="width:750px;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;">Manage Contribution Payouts:</legend>
				<br/>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
				<input type="hidden" name="cidx" value="<?php echo ($cIdx === false ? "" : $cIdx); ?>"/>
				<input type="hidden" name="lmt" value="<?php echo ($limit === false ? 0 : $limit); ?>"/>
				<?php 
					PrintSecTokenInput(); 
					if ($showConts !== false) echo "<input type='hidden' name='sc' value='1' />";
					if ($fltRnd !== false) echo "<input type='hidden' name='fltRnd' value='1' />";
					if ($listSort !== false) echo  "<input type='hidden' name='sort' value='$listSort' />";
				?>
				<?php
							//---------------------------------
							if ($selCont !== false && $selContData !== false)
							{
								//---------------------------------
								$isManuallyBypassed = false;
								//---------------------------------
								echo "<div class='admin-edit'>\n";
								echo "<table style='width:100%;'>\n<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'><th style='min-width:140px;'>Name</th><th style='width:100%;'>Value</th></tr>\n</thead>\n";
								echo "<tbody class='admin-scrolltable-body' style='max-height:none;'>\n";
								//---------------------------------
								for ($i = 0;$i < sizeof($cols);$i++)
								{
									if ($i == 0 /*ID*/) // enforce first row widths
										echo "<tr class='admin-row' style='width:100%;'><td style='min-width:140px;'>".$cols[$i][0]."</td><td style='width:100%;'>".$selContData[$i]."</td></tr>\n";
									else if ($i == 1 /*Round*/)
									{
										$Rounds = new Rounds() or die('Create object failed');
										$tsRoundStart = $Rounds->getRoundDate($selContData[1]);
										$strRoundStart = XEncodeHTML(($tsRoundStart !== false ? $Display->htmlLocalDate($tsRoundStart) : '[date not found]'));
										echo "<tr class='admin-row'><td>Round</td><td>(<a href='./Admin.php?p=6&amp;rid=$selContData[1]&amp;".SecToken()."'>$selContData[1]</a>) Round started $strRoundStart</td></tr>\n";
									}
									else if ($i == 4 /*Value*/)
									{
										echo "<tr class='admin-row'><td>Value</td><td>$selCont->value / ".($selCont->subvalue !== false ? $selCont->subvalue : '_')." / ".($selCont->countvalue !== false ? $selCont->countvalue : '_')."</td></tr>\n";
									}
									else if ($i == 5 /*Outcome*/)
									{
										echo "<tr class='admin-row'><td>Outcome</td><td>".($isManuallyBypassed ? '(Manually bypassed) ' : '').$selContData[10].($selCont->total !== false ? " (".$selCont->total." / ".($selCont->count !== false ? $selCont->count : '_').")" : "")."</td></tr>\n";
										echo "<tr class='admin-row'><td>Done</td><td>".XEncodeHTML(($selCont->dtDone !== false ? $Display->htmlLocalDate($selCont->dtDone) : '(not done)'))."</td></tr>\n";
									}
									else
									{
										echo "<tr class='admin-row'><td>".$cols[$i][0]."</td><td>".$selContData[$i]."</td></tr>\n";
										if ($i == 2) // add-on created/orde/ad id after name
										{
											echo "<tr class='admin-row'><td>Created</td><td>".XEncodeHTML(($selCont->dtCreated !== false ? $Display->htmlLocalDate($selCont->dtCreated) : '(created date not set)'))."</td></tr>\n";
											echo "<tr class='admin-row'><td>Order</td><td>".$selContData[8]."</td></tr>\n";
											echo "<tr class='admin-row'><td>Ad ID</td><td>".$selContData[9]."</td></tr>\n";
										}
										else if ($i == 3) // add-on flags after mode
										{
											$aFlags = array();
											if ($selCont->hasFlag(CONT_FLAG_DISABLED))
												$aFlags[] = 'Disabled';
											if ($selCont->hasFlag(CONT_FLAG_MANUALLY_BYPASSED))
											{
												$aFlags[] = 'Manually Bypassed';
												$isManuallyBypassed = true;
											}
											if ($selCont->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
												$aFlags[] = 'Wait For Funds';
											if ($selCont->hasFlag(CONT_FLAG_FULL_BALANCE))
												$aFlags[] = 'Full Balance';
											if ($selCont->hasFlag(CONT_FLAG_MAX_TOTAL))
												$aFlags[] = 'Use Max Value';
											if ($selCont->hasFlag(CONT_FLAG_MAX_COUNT))
												$aFlags[] = 'Use Max Count';
											if ($selCont->hasFlag(CONT_FLAG_MANUALLY_BYPASSED))
												$aFlags[] = 'Manually Bypassed';
											if ($selCont->hasFlag(CONT_FLAG_MAIN_CONTRIBUTION))
												$aFlags[] = 'Main Contrib.';
											
											if (sizeof($aFlags) == 0)
												$strFlags = '(none)';
											else
												$strFlags = implode(', ', $aFlags);
											$strFlags = XEncodeHTML($strFlags);
											echo "<tr class='admin-row'><td>Flags</td><td>$strFlags</td></tr>\n";											
										}
									}
								}
								//---------------------------------
								echo "<tr class='admin-row'><td>Address</td><td><a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selContData[6])."'>".XEncodeHTML($selContData[6])."</a></td></tr>\n";
								echo "<tr class='admin-row'><td>Txid</td><td>".($isManuallyBypassed ? XEncodeHTML( ($selCont->txid !== false ? $selCont->txid : '[Not set]') ) : $selContData[7])."</td></tr>\n";
								echo "</tbody></table>\n";
								echo "</div>\n";
								//---------------------------------
								if ($canDelete)
								{
									echo "<br/><input type='submit' name='action' value='Manually Bypass' /> Bypass&nbsp;Note: <input style='width:63%;' id='bypnt' name='bypnt' type='text' value='' maxlength='".(C_MAX_TRANSACTION_ID_LENGTH - 2)."'/><br/>\n";
									echo "<br/><input type='submit' name='action' value='Clear Payment' /><br/>\n";
									echo "<br/><input type='submit' name='action' value='Delete' /><br/>\n";

									
									echo "<br/><input type='submit' name='action' value='Set Outcome' /> \n";
									echo "<select name='noc' id='noc'>\n";
									foreach ($contOutcomeNames as $oid => $oname)
										echo "<option value='$oid'".($selCont->outcome !== false && $selCont->outcome == $oid ? " selected='selected'" : "").">$oname</option>\n";
									echo "</select> \n";
									
									if ($selCont->dtDone !== false)
										echo "<input id='clrdone' name='clrdone' type='checkbox' />Clear Done<br/><br/>\n";
									else
										echo "<input id='setdone' name='setdone' type='checkbox' />Set Done<br/><br/>\n";
								}
								//---------------------------------
							} // if ($selCont !== false)
							//------------------------	
							if ($canDelete)
							{
								echo "<br/><input type='submit' name='action' value='Delete All From Round' />\n";
								echo "Round index: <input style='max-width:100px;' id='dridx' type='text' name='dridx' value='' maxlength='5'/><br/>\n";
							}
							//------------------------	
					?>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
