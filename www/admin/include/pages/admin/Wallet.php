<?php
//---------------------------------
/*
 * include/pages/admin/Wallet.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Wallet = new Wallet() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action');
$actionAA1 = XGetPost('actionAA_1');
$actionAA2 = XGetPost('actionAA_2');
$accIdx = XGetPost('acidx');
if ($accIdx == "")
	$accIdx = 0;
$selAddress = false;
$actMainAddress = false;
$activeAccAddress = false;

$actAddress = false;
//---------------------------------
function showInfo($Wallet)
{
	//---------------------------------
	$detailList = $Wallet->getInfo();
	if ($detailList === false)
	{
		echo "<div>Request info from wallet client failed.</div>\n";
		return false;
	}
	else
	{
		//---------------------------------
		echo "<table class='admin-table'>\n";
		echo "<tr><th style='width:120px;'>Name</th><th style='width:120px;'>Value</th></tr>\n";
		//---------------------------------
		if (sizeof($detailList) == 0)
			echo "<tr><td style='width:120px;'>&nbsp;</td><td style='width:80px;'>&nbsp;</td></tr>\n";
		else
		{
			//---------------------------------
			foreach ($detailList as $name => $val)
			{
				echo "<tr class='admin-row'>\n";
				echo "<td style='width:120px;'>$name</td><td style='width:80px; text-align: right;'>$val</td></tr>\n";
			}
			//---------------------------------
		}
		//---------------------------------
		echo "</table>\n";		
		//---------------------------------
	}
	//---------------------------------
	return true;
}
function showBalances($Wallet)
{
	$Contributions = new Contributions() or die("Create object failed");
	//---------------------------------
	$selAddress = false;
	$contList = $Contributions->findRoundContributions(-1 /*roundIdx*/);
	//---------------------------------
	$balances = $Wallet->listUnspentTotals();
	$pagenum = XGetPost('p');
	$accIdx = XGetPost('acidx');
	if ($accIdx == "")
		$accIdx = 0;
	//---------------------------------
	if ($contList === false)
	{
		echo "<div>Get contributions list failed.</div>\n";
		return false;
	}
	else if ($balances === false)
	{
		echo "<div>Request balances from wallet failed.</div>\n";
		return false;
	}
	else
	{
		//---------------------------------
		$Columns = array(	array(	"200px",	"Label"),
							array(	"390px",	"Address"),
							array(	"120px",		"Balance"));
		//---------------------------------
		echo "<table class='admin-table'>\n<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'>\n";
		//---------------------------------
		foreach ($Columns as $col)
			echo "\t<th".($col[0] !== false ? " style='width:$col[0];'" : "").">$col[1]</th>\n";
		echo "</thead>\n<tbody class='admin-scrolltable-body'>\n";
		//---------------------------------
		foreach ($contList as $cont)
			if ($cont->address !== false && trim($cont->address) != "")
				if (!isset($balances[$cont->address]))
					$balances[$cont->address] = 0.0;
		//---------------------------------
		$mainAddress = $Wallet->getMainAddress();
		if ($mainAddress !== false && $mainAddress != "" && !isset($balances[$mainAddress]))
				$balances[$mainAddress] = 0.0;
		//---------------------------------
		$storeAddress = $Wallet->getStoreAddress();
		if ($storeAddress !== false && $storeAddress != "" && !isset($balances[$storeAddress]))
				$balances[$storeAddress] = 0.0;
		//---------------------------------
		if (sizeof($balances) == 0)
			echo "<tr class='admin-row' style='width:100%;'><td".($Columns[0][0] !== false ? " style='width:".$Columns[0][0].";'" : "").">&nbsp;</td><td".($Columns[1][0] !== false ? " style='width:".$Columns[1][0].";'" : "").">(none)</td><td".($Columns[2][0] !== false ? " style='text-align: right;width:".$Columns[2][0].";'" : "").">0</td></tr>\n";
		else
		{
			//---------------------------------
			$idx = 0;
			//---------------------------------
			foreach ($balances as $address => $balance)
			{
				//---------------------------------
				//XLogDebug("Admin Wallet [$idx] '$address' => '$balance'");
				if ($idx == $accIdx)
				{
					$selAddress = $address;
					$rowClass = 'admin-row-sel';
				}
				else
					$rowClass = 'admin-row';
				//---------------------------------
				$label = "";
				//---------------------------------
				// Disabling address account/label lookup for being very slow and not too useful
				/*
				$addrLabel = $Wallet->getLabelOfAddress($address);
				if ($addrLabel !== false && $addrLabel != "")
					$label = "($addrLabel) ";
				*/
				//---------------------------------
				foreach ($contList as $cont)
					if ($address == $cont->address)
						$label .= $cont->name;
				//---------------------------------
				if ($address == $mainAddress)
					$label .= "[main]";
				if ($address == $storeAddress)
					$label .= "[store]";
				$labelText = ($label == "" ? "&nbsp;" : $label);
				$addressText = "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($address)."'>".XEncodeHTML($address)."</a>";
				//---------------------------------
				echo "<tr class='$rowClass' onclick=\"document.location.href='./Admin.php?p=$pagenum&amp;action=List%20Accounts&amp;acidx=$idx&amp;".SecToken()."';\">\n";
				echo "<td".($Columns[0][0] !== false ? " style='width:".$Columns[0][0].";'" : "")."><div style='overflow: auto;'>$labelText</div></td>";
				echo "<td".($Columns[1][0] !== false ? " style='width:".$Columns[1][0].";'" : "")."><div style='overflow: auto;'>$addressText</div></td>";
				echo "<td style='text-align: right;".($Columns[2][0] !== false ? "width:".$Columns[2][0].";" : "")."'><div style='overflow: auto;'>$balance</div></td>";
				echo "</tr>\n";
				//---------------------------------
				$idx++;
				//---------------------------------
			}
			//---------------------------------
		}
		//---------------------------------
		echo "</tbody></table>\n";		
		//---------------------------------
	}
	//---------------------------------
	return $selAddress;
}
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="float:none; width:740px;"><!-- Start Left Column -->
		<div>
			<h1>Wallet:</h1>
			<p>
			Folding @ Home Wallet Configuration.
			</p>
			<br/>
		</div>
		<?php
		//---------------------------------
		$canManage = $Login->HasPrivilege(PRIV_WALLET_MANAGE);
		//---------------------------------
		if ($action == 'Set Main Address')
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$newMainAddress = XPost('addrMain');
				if (!$Wallet->setMainAddress($newMainAddress))
				{
					XLogError("Admin Wallet wallet set main address failed.");
					echo "<div>Set main address failed.</div>\n";
				}
				else
				{
					echo "<div>Payout main address set.</div>\n";
					$selAddress = $newMainAddress;
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == 'Set Store Address')
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$newStoreAddress = XPost('addrStore');
				if (!$Wallet->setStoreAddress($newStoreAddress))
				{
					XLogError("Admin Wallet wallet set store address failed.");
					echo "<div>Set store address failed.</div>\n";
				}
				else
				{
					echo "<div>Store address set.</div>\n";
					$selAddress = $newStoreAddress;
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == 'Create Address')
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$newLabel = XPost('newalabel');
				$newAddress = $Wallet->generateAddresss($newLabel);
				if ($newAddress === false)
				{
					XLogError("Admin Wallet Create Address wallet generateAddresss failed.");
					echo "<div>Generate address failed.</div>\n";
				}
				else
				{
					echo "<div>Generate arress successful:<br/><pre>$newAddress</pre></div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Fee")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$txfee = XPost('txfee');
				//---------------------------------
				if (!is_numeric($txfee))
					echo "<div>Validate value to set fee failed.</div>\n";
				else
				{
					if (!$Wallet->setFee($txfee))
						echo "<div>Set fee failed.</div>\n";
					else 
						echo "<div>Fee set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Est Fee")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$estfee = XPost('estfee');
				//---------------------------------
				if (!is_numeric($estfee))
					echo "<div>Validate value to set est. fee failed.</div>\n";
				else
				{
					if (!$Wallet->setEstFee($estfee))
						echo "<div>Set est. fee failed.</div>\n";
					else 
						echo "<div>Est fee set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Est Cont Fee")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$estcontfee = XPost('estcontfee');
				//---------------------------------
				if (!is_numeric($estcontfee))
					echo "<div>Validate value to set est. cont fee failed.</div>\n";
				else
				{
					if (!$Wallet->setEstContFee($estcontfee))
						echo "<div>Set est. cont fee failed.</div>\n";
					else 
						echo "<div>Est cont fee set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Min Conf")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$minConf = XPost('minconf');
				//---------------------------------
				if (!is_numeric($minConf))
					echo "<div>Validate value to set minimum confirmations failed.</div>\n";
				else
				{
					if (!$Wallet->setMinConf($minConf))
						echo "<div>Set minimum confirmations failed.</div>\n";
					else 
						echo "<div>Minimum confirmations set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Min Output")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$minOutput = XPost('minoutput');
				//---------------------------------
				if (!is_numeric($minOutput))
					echo "<div>Validate value to set minimum output failed.</div>\n";
				else
				{
					if (!$Wallet->setMinOutput($minOutput))
						echo "<div>Set minimum output failed.</div>\n";
					else 
						echo "<div>Minimum output set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Min Fee")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$minFee = XPost('minfee');
				//---------------------------------
				if (!is_numeric($minFee))
					echo "<div>Validate value to set min fee failed.</div>\n";
				else
				{
					if (!$Wallet->setMinFee($minFee))
						echo "<div>Set min fee failed.</div>\n";
					else 
						echo "<div>Fee min set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Fee Per Kb")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$feePerKb = XPost('feeperkb');
				//---------------------------------
				if (!is_numeric($feePerKb))
					echo "<div>Validate value to set fee per kb failed.</div>\n";
				else
				{
					if (!$Wallet->setFeePerKb($feePerKb))
						echo "<div>Set fee per kb failed.</div>\n";
					else 
						echo "<div>Fee fee per kb successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		else if ($action == "Set Extra Fee")
		{
			//---------------------------------
			if ($canManage)
			{
				//---------------------------------
				$extraFee = XPost('extrafee');
				//---------------------------------
				if (!is_numeric($extraFee))
					echo "<div>Validate value to set extra fee failed.</div>\n";
				else
				{
					if (!$Wallet->setExtraFee($extraFee))
						echo "<div>Set extra fee failed.</div>\n";
					else 
						echo "<div>Fee extra set successfully.</div>\n";
				}
				//---------------------------------
			}
			else
			{
				XLogNotify("Admin Wallet action '$action' doesn't have proper privileges: $Login->UserName");
				echo "Wallet $action failed. Action requires proper privileges.<br/>";
			}
			//---------------------------------
		}
		//---------------------------------
		$estFee = $Wallet->getEstFee();
		$estContFee = $Wallet->getEstContFee();
		$minConf = $Wallet->getMinConf();
		$minOutput = $Wallet->getMinOutput();
		$minFee = $Wallet->getMinFee();
		$feePerKb = $Wallet->getFeePerKb();
		$extraFee = $Wallet->getExtraFee();
		//---------------------------------
		$actMainAddress = $Wallet->getMainAddress();
		if ($actMainAddress === false)
			$actMainAddress = "";
		//---------------------------------
		$storeAddress = $Wallet->getStoreAddress();
		if ($storeAddress === false)
			$storeAddress = "";
		//---------------------------------
		$payoutAccBalance = $Wallet->getBalance();
		//---------------------------------
		$address = $Wallet->getAccountAddress();
		if ($address !== false)
			$activeAccAddress = $address;
		//---------------------------------
		if ($payoutAccBalance === false)
			$payoutAccBalance = "(error)";
		//---------------------------------
		/*
		$txfee = $Wallet->getFee();
		//---------------------------------
		if ($txfee === false)
		{
			$txfee = "(error)";
			echo "<div style='color:red;'>Wallet RPC connectivity failed!</div>\n";
		}
		*/
		//---------------------------------
		$showEmpty = true;
		//---------------------------------
		if (showInfo($Wallet))
			$selAddress = showBalances($Wallet, $showEmpty);
		//---------------------------------
		?>
		</div> <!-- End Left Column -->
		<div class="col1 pad_left1" style="float:none;width:80%;"><!-- Start Right Column -->
		<div class="admin-edit" style="width:80%;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;">Configure Wallet:</legend>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
				<input type="hidden" name="acidx" value="<?php echo $accIdx ?>" />
				<div>
					<label class="loginLabel" for="addrMain">Main Payout Address:</label><br/>
					<div style='min-width:220px;background:#AAAAAA; padding: 1px 1px; margin: 1px 1px;'><?php echo ($actMainAddress == "" ? "(none)" : $actMainAddress); ?></div>
					<?php 
						if ($canManage)
						{
							echo "<br/><label class='loginLabel' for='addrMain'>Selected Address:</label><br/>";
							echo "<input style='width:280px;' id='addrMain' type='text' name='addrMain' value='".XEncodeHTML( ($selAddress !== false ? $selAddress : $actMainAddress) )."' maxlength='".C_MAX_NAME_TEXT_LENGTH."' />\n";
							echo "<br/><input style='margin-top:10px;' type='submit' name='action' value='Set Main Address' />\n";
						}
					?>
				</div>
				<div>
					<label class="loginLabel" for="addrStore">Store Address:</label><br/>
					<div style='min-width:220px;background:#AAAAAA; padding: 1px 1px; margin: 1px 1px;'><?php echo ($storeAddress == "" ? "(none)" : $storeAddress); ?></div>
					<?php 
						if ($canManage)
						{
							echo "<br/><label class='loginLabel' for='addrStore'>Selected Address:</label><br/>";
							echo "<input style='width:280px;' id='addrStore' type='text' name='addrStore' value='".XEncodeHTML( ($selAddress !== false ? $selAddress : $storeAddress) )."' maxlength='".C_MAX_NAME_TEXT_LENGTH."' />\n";
							echo "<br/><input style='margin-top:10px;' type='submit' name='action' value='Set Store Address' />\n";
						}
					?>
				</div>
				<br/>
				<div>
					<label class="loginLabel" for="accBal">Payout Account Balance:</label><br/>
					<input style="width:220px;" id="accBal" type="text" name="accBal" value="<?php echo XEncodeHTML(sprintf("%0.8f", $payoutAccBalance));?>" readonly="readonly" />
				</div>
				<br/>
				<div>
					<?php
						echo "<label class='loginLabel' for='estfee'>Estimated Round Fee Buffer:</label><br/>\n";
						echo "<input style='width:220px;' id='estfee' type='text' name='estfee' value='$estFee' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Est Fee' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<label class='loginLabel' for='estcontfee'>Estimated Contribution Fee Buffer:</label><br/>\n";
						echo "<input style='width:220px;' id='estcontfee' type='text' name='estcontfee' value='$estContFee' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Est Cont Fee' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<br/>\n";
						echo "<label class='loginLabel' for='minconf'>Minimum Confirmations:</label><br/>\n";
						echo "<input style='width:120px;' id='minconf' type='text' name='minconf' value='$minConf' maxlength='8' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Min Conf' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<label class='loginLabel' for='minoutput'>Minimum Output Value (dust limit):</label><br/>\n";
						echo "<input style='width:220px;' id='minoutput' type='text' name='minoutput' value='$minOutput' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Min Output' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<label class='loginLabel' for='minfee'>Minimum Fee:</label><br/>\n";
						echo "<input style='width:220px;' id='minfee' type='text' name='minfee' value='$minFee' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Min Fee' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<label class='loginLabel' for='feeperkb'>Fee Per Kb:</label><br/>\n";
						echo "<input style='width:220px;' id='feeperkb' type='text' name='feeperkb' value='$feePerKb' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Fee Per Kb' /><br/>\n" : "readonly='readonly' /><br/>\n");
						echo "<label class='loginLabel' for='extrafee'>Extra Fee:</label><br/>\n";
						echo "<input style='width:220px;' id='extrafee' type='text' name='extrafee' value='$extraFee' maxlength='10' ".($canManage ? "/>&nbsp;<input style='margin-top:10px;' type='submit' name='action' value='Set Extra Fee' /><br/>\n" : "readonly='readonly' /><br/>\n");
					?>
				</div>
				<br/>
				<?php
						if ($canManage)
						{
							echo "<div></div><br/><legend style='font-weight:bold;font-size:120%;'>Create wallet address:</legend><br/>";
							echo "<label class='loginLabel' for='txfee'>Label:</label><br/>";
							echo "<input style='width:220px;' id='newalabel' type='text' name='newalabel' value='' maxlength='48' /><br/>";
							echo "<input style='margin-top:10px;' type='submit' name='action' value='Create Address' />\n";
							echo "</div>";
						}
				?>
				<br/>
				<br/>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
