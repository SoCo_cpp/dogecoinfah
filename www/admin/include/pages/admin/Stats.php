<?php
//---------------------------------
/*
 * include/pages/admin/Stats.php
 * 
 * 
*/
//---------------------------------
global $Login; 
//---------------------------------
$Stats = new Stats() or die("Create object failed");
$Workers = new Workers() or die("Create object failed");
$Rounds  = new Rounds() or die("Create object failed");
$Payouts = new Payouts() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$Config = new Config() or die("Create object failed");
//---------------------------------
$pagenum = XGetPost('p');
$sidx = XGetPost("idx");
if ($sidx == "")
	$sidx = false;
$action = XPost('action');
$sSort = XGetPost("srt");
if (!is_numeric($sSort) || $sSort == 0 || abs($sSort) > 4)
	$sSort = 1;
$widx = XGetPost("widx");
if ($widx == "")
	$widx = false;
$ridx = XGetPost("ridx");
if ($ridx == "")
	$ridx = false;
$limit = XGetPost("lmt");
$defFilters = ($limit == "" ? true : false);
if ($limit === false || $limit == "" || !is_numeric($limit))
	$limit = 100;
else if ($limit == 0)
	$limit = false;
$fltRnd = XGetPost("fltRnd");
if ($fltRnd == "" || !is_numeric($fltRnd))
	$fltRnd = false;
$fltWrk = XGetPost("fltWrk");
if ($fltWrk == "" || !is_numeric($fltWrk))
	$fltWrk = false;
$selRoundStr = "";
$selWorkerStr = "";
$selStat = false;
$dridx = XPost('dridx', false);
if ($defFilters && $sidx !== false)
	$limit = "";
//---------------------------------
$defTeamID = $Config->Get(CFG_ROUND_TEAM_ID, DEFAULT_ROUND_TEAM_ID);
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="width: 98%;"><!-- Start Left Column -->
		<div>
			<h1>Worker stats:</h1>
			<p>
			Folding @ Home stats list.
			</p>
			<br/>
		</div>
		<?php
			//------------------------	
			if ($action == "Add")
			{
				//------------------------	
				$wpnt = XGetPost("wpnt");
				if ($wpnt == "")
					$wpnt = false;
				$tpnt = XGetPost("tpnt");
				if ($tpnt == "")
					$tpnt = false;
				$wus = XGetPost("wus");
				if ($wus == "")
					$wus = false;
				$dupcnt = XGetPost("dcnt");
				if ($dupcnt == "")
					$dupcnt = false;
				//------------------------	
				XLogNotify("User $Login->User is adding stat for round: $ridx, worker idx: $widx, dup count: $dupcnt, work/wk points: $wpnt, tot points: $tpnt, wus: $wus");
				//------------------------	
				if ($ridx === false || !is_numeric($ridx) && $widx === false || !is_numeric($widx) || $tpnt === false || !is_numeric($tpnt) || $wus === false || !is_numeric($wus) || $dupcnt === false || !is_numeric($dupcnt) || $dupcnt > 100)
				{
					XLogNotify("Stats admin page - stats add failed to validate parameters");
					echo "Failed to add stat, invalid parmeters provided.<br/><br/>\n";
				}
				else if ($Login->HasPrivilege('D')) // delete history
				{
					
					$worker = $Workers->loadWorker($widx);
					if ($worker === false)
					{
						XLogNotify("Stats admin page - stats add failed to find worker $widx");
						echo "Failed to add stat, worker index not found.<br/><br/>\n";
					}
					else
					{
						$dleft = $dupcnt;
						$ok = true;
						while ($dleft > 0 && $ok === true)
						{
							if (!$Stats->addFahClientStat((int)$ridx, (int)$widx, (int)$tpnt, (int)$wus))
							{
								$ok = false;
								XLogNotify("Stats admin page - stats failed to addFahClientStat, round idx: $ridx, worker idx: $widx, dup count: $dupcnt, dup left: $dleft");
								echo "Failed to add stat.<br/><br/>\n";
							}
							$dleft--;
						}
						if ($ok === true)
						{
							XLogNotify("Stats admin page - stat added successfully. round idx: $ridx, worker idx: $widx, dup count: $dupcnt");
							echo "Successfully added $dupcnt duplicate stat(s).<br/><br/>\n";
						}
					}
				}
				else 
					echo "Deleting historical data required 'D' delete privileges.<br/><br/>\n";
			}
			else if ($action == "Delete" && $sidx !== false && is_numeric($sidx))
			{
				if ($Login->HasPrivilege('D')) // delete history
				{
					XLogNotify("User $Login->User is deleting stat: $sidx");
					if (!$Stats->deleteStat($sidx))
					{
						XLogNotify("Stats admin page - stats failed to delete Stat $sidx");
						echo "Failed to delete stat.<br/><br/>\n";
					}
					else
					{
						echo "Stat successfully deleted.<br/><br/>\n";
						$sidx = false;
					}
				}
				else 
					echo "Deleting historical data required 'D' delete privileges.<br/><br/>\n";
			}
			else if ($action == "Update Points" && $sidx !== false && is_numeric($sidx))
			{
				if ($Login->HasPrivilege('D')) // delete history
				{
					$stat = $Stats->getStat($sidx);
					if ($stat === false)
					{
						XLogNotify("Stats admin page - stats getStat failed find Stat $sidx to update points");
						echo "Failed to update stat points.<br/><br/>\n";
					}
					else
					{
						$wp = XPost('selwp');
						$tp = XPost('seltp');
						if ($wp === false || $tp === false || !is_numeric($wp) || !is_numeric($tp))
						{
							XLogWarn("Stats admin page - stats failed validate points for Stat $sidx to update points");
							echo "Failed to update stat points, points invalid.<br/><br/>\n";
						}
						else
						{
							XLogNotify("User $Login->User is updating stat: $sidx's points from (".$stat->weekPoints."/".$stat->totPoints.") to ($wp/$tp)");
							$stat->weekPoints = $wp;
							$stat->totPoints = $tp;
							if (!$stat->Update())
							{
								XLogError("Stats admin page - stat failed to update for Stat $sidx to update points");
								echo "Failed to update stat points update failed.<br/><br/>\n";
							}
							else
							{
								echo "Stat successfully updated stat points.<br/><br/>\n";
							}
						}
					}
				}
				else 
					echo "Update historical data required 'D' delete privileges.<br/><br/>\n";
			}
			else if ($action == "Delete All From Round")
			{
				if ($Login->HasPrivilege('D')) // delete history
				{
					if ($dridx !== false && is_numeric($dridx))
					{
						XLogNotify("User $Login->User is deleting all stats from round: $dridx");
						if (!$Stats->deleteAllRound($dridx))
						{
							XLogNotify("Stats admin page - stats failed to delete all from round $dridx");
							echo "Failed to delete all from round $dridx.<br/><br/>\n";
						}
						else
						{
							echo "Stats successfully deleted.<br/><br/>\n";
							$sidx = false;
						}
						
					}
					else "Deleting all from round, validate round index failed.<br/><br/>\n";
				}
				else 
					echo "Deleting historical data required 'D' delete privileges.<br/><br/>\n";
			}
			//------------------------	
			$Columns = array(	array(	"100px",	"ID",		1),
								array(	"250px",	"Round",	2),
								array(	"270px",	"Worker",	3),
								array(	"120px",	"Work",		4)
								);
			//------------------------	
			echo "<script>\nfunction selStat(id, sort=$sSort){\n document.location.href='./Admin.php?p=$pagenum&idx=' + id + '&srt=' + sort + '&lmt=".($limit === false ? 0 : $limit).($fltRnd === false ? "" : "&fltRnd=$fltRnd").($fltWrk === false ? "" : "&fltWrk=$fltWrk")."&".SecToken()."'; }\n</script>\n";
			//------------------------	
			echo "<table class='admin-table' style='width:802px;'>\n<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'>\n";
			//------------------------	
			foreach ($Columns as $col)
			{
				$style = "";
				if ($col[0] !== false)
					$style .= "width:$col[0];";
				if (abs($sSort) == $col[2])
					$style .= "background:#EEEEEE;";
				echo "\t<th".($style != "" ? " style='$style'" : "");
				echo " onclick=\"selStat(".($sidx === false ? "''" : $sidx).", ".(abs($sSort) == $col[2] ? (-1 * $sSort) : $col[2]).");\">$col[1]</th>\n";
			}
			//------------------------	
			echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
			//------------------------	
			if (abs($sSort) == 1)
				$sort = DB_STATS_ID;
			else if (abs($sSort) == 2)
				$sort = DB_STATS_ROUND;
			else if (abs($sSort) == 3)
				$sort = DB_STATS_WORKER;
			else if (abs($sSort) == 4)
				$sort = DB_STATS_WEEK_POINTS;
			else 
				$sort = false;
			if ($sort !== false && $sSort < 0)
				$sort .= " DESC";
			//------------------------	
			if ($defFilters && $sidx !== false)
			{
				$singleStat = $Stats->getStat($sidx);
				if ($singleStat === false)
				{
					XLogError("Stats admin page - Stats getStat failed: ".XVarDump($sidx));
					$slist = array();
					$sidx = false;
				}
				else $slist = array($singleStat);
			}
			else
			{
				$where = false;
				if ($fltRnd !== false && is_numeric($fltRnd))
					$where = DB_STATS_ROUND."=$fltRnd";
				if ($fltWrk !== false && is_numeric($fltWrk))
					$where = ($where !== false ? "$where AND " : "").DB_STATS_WORKER."=$fltWrk";
				$slist = $Stats->getStats($limit, $sort, $where);
			}
			if ($slist === false)
				XLogError("Stats admin page - Stats getStats/findRoundStats failed");
			if ($slist === false || sizeof($slist) == 0)
			{
				//------------------------
				echo "<tr class='admin-row'>";
				foreach ($Columns as $col)
					echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
				echo "</tr>\n";
				//------------------------
			}
			else
			{
				//------------------------
				$rowIdx = 0;
				foreach ($slist as $s)
				{
					//------------------------
					$dtRound = $Rounds->getRoundDate($s->roundIdx);
					if ($dtRound === false || $dtRound == -1)
						$strRound = $s->roundIdx;
					else
						$strRound = "($s->roundIdx) ".$Display->htmlLocalDate($dtRound);
					//------------------------
					$worker = $Workers->getWorker($s->workerIdx);
					if ($worker === false)
						$strWorker = "($s->workerIdx) [not found]";
					else
					{
						$strWorkerName = ($worker->address == "" ? $worker->uname : $worker->address);
						if (strlen($strWorkerName) > 15)
							$strWorkerName = substr($strWorkerName, 0, 15).'...';
						$strWorker = "($s->workerIdx) ".XEncodeHTML($strWorkerName);
					}
					//------------------------
					$strWork = $s->work();
					if ($strWork === false) 
						$strWork = "";
					//------------------------
					if ($sidx !== false && $sidx == $s->id)
					{
						$rowClass = 'admin-row-sel';
						$selRoundStr = $strRound;
						$selStat = $s;
						$selWorker = $worker;
						$selWork = $strWork;
					}
					else $rowClass = 'admin-row';
					//------------------------
					echo "<tr class='$rowClass' onclick=\"selStat($s->id);\">\n";
					//------------------------
					$colData = array(	$s->id,
										$strRound,
										$strWorker,
										$strWork
									);
					//------------------------
					for ($i = 0;$i < sizeof($colData);$i++)
					{
						$style = "";
						if ($rowIdx == 0 && $Columns[$i][0] !== false)
							$style .= "width:".$Columns[$i][0].";";
						if ($Columns[$i][1] == "Work")
							$style .= "text-align:right;";
						if ($style != "")
							$style = " style='$style'";
						echo "<td$style>".$colData[$i]."</td>";
					}
					//------------------------
					echo "</tr>\n";
					//------------------------
					$rowIdx++;
					//------------------------
				} // foreach
				//------------------------
			}
			//------------------------
			echo "</tbody></table>\n";		
			//---------------------------
		?>
		<div class="admin-edit pad_left1" style="width:600px;">
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<?php
						//---------------------------
						echo "Limit rows: <input style='width:80px;' id='lmt' type='text' name='lmt' value='".($limit === false ? 0 : $limit)."' maxlength='6'/><br/>\n";
						echo "Filter Round: <input style='width:80px;' id='fltRnd' type='text' name='fltRnd' value='".($fltRnd === false ? "" : $fltRnd)."' maxlength='6'/>\n";
						echo "Filter Worker: <input style='width:80px;' id='fltWrk' type='text' name='fltWrk' value='".($fltWrk === false ? "" : $fltWrk)."' maxlength='6'/>\n";
						echo "<input style='margin-bottom:4px;' type='submit' name='action' value='Update Limit/Filter' /><br/>\n";
						//---------------------------
					?>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $sidx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
				</fieldset>
			</form>
		</div>
		</div><!-- End Left Column -->
		<?php
		//---------------------------
		if ($sidx !== false && $selStat !== false)
		{
		?>
		<div class="col1 pad_left1" style="width:800px;"><!-- Start Right Column -->
		<div class="admin-edit" style="margin: auto;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<br/>
				<div style="font-weight:bold;font-size:120%;">Stat Details:</div>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="idx" value="<?php echo $sidx; ?>" />
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
				<input type="hidden" name="lmt" value="<?php echo ($limit === false ? 0 : $limit); ?>"/><!-- Page -->
				<?php 
					//---------------------------
					if ($fltRnd !== false)
						echo "<input type='hidden' name='fltRnd' value='$fltRnd' />";
					if ($fltWrk !== false)
						echo "<input type='hidden' name='fltWrk' value='$fltWrk' />";
					//---------------------------
					echo "<table class='admin-table'>\n<tr><th style='width:120px;'>Attribute</th><th >Value</th></tr>\n";
					//---------------------------
					$strRound = $Rounds->getRoundDate($selStat->roundIdx);
					$selRoundStr = "(<a href='./Admin.php?p=6&amp;rid=$selStat->roundIdx&amp;".SecToken()."'>$selStat->roundIdx</a>) ".$Display->htmlLocalDate($strRound);
					if ($selWorker === false)
						$selWorkerStr = "($selStat->workerIdx) [not found]";
					else
					{
						$selWorkerStr = "(<a href='./Admin.php?p=5&amp;idx=$selStat->workerIdx&amp;".SecToken()."'>$selStat->workerIdx</a>) ";
						if ($selWorker->address != $selWorker->uname)
							$selWorkerStr .= $selWorker->uname." (".($selWorker->address == "" ? "not set" : "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selWorker->address)."'>".XEncodeHTML($selWorker->address)."</a>").")";
						else
							$selWorkerStr .= "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selWorker->address)."'>".XEncodeHTML($selWorker->address)."</a>";
					}
					//------------------------
					$attributes = array(	'ID' => $sidx,
											'Created' => $Display->htmlLocalDate($selStat->dtCreated),
											'Round' => $selRoundStr,
											'Worker' => $selWorkerStr,
											'Work' => $selWork
									);
					//---------------------------
					$payout = $Payouts->getPayoutRoundWorker($selStat->roundIdx, $selStat->workerIdx);
					if ($payout === false)
						$attributes['Payout'] = "(not paid or queued)";
					else
					{
						$strPayoutResult = ($payout->dtStored === false ? "" : "Stored at ".$Display->htmlLocalDate($payout->dtStored).", ");
						$strPayoutResult .= ($payout->dtPaid === false ? "Not yet paid" : "Paid at ".$Display->htmlLocalDate($payout->dtPaid));
						if ($payout->storePayout !== false)
							$strPayoutResult .= " (stored value paid by payout <a href='./Admin.php?p=$pagenum&amp;idx=$payout->storePayout&amp;".SecToken()."'>".$payout->storePayout."</a>)";
						$attributes['Payout'] = "(<a href='./Admin.php?p=7&amp;idx=$payout->id&amp;".SecToken()."'>$payout->id</a>) $strPayoutResult";
						$attributes['Payment'] = "Pay $payout->pay (Stat Pay $payout->statPay) ".($payout->storePay != 0 ? "(".$payout->storePay." from stored)" : "")."&nbsp;".BRAND_UNIT.($payout->dtStored !== false ? ", Stored pay of ".$payout->statPay."&nbsp;".BRAND_UNIT : "");
						if ($selWorker !== false && $selWorker->address !== $payout->address)
							$attributes['Payment Address'] = "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($payout->address)."'>".XEncodeHTML($payout->address)."</a>";
						if ($payout->txid !== false && $payout->txid != "")
							$attributes['Payment TxID'] = ($payout->txid != "" ? (strpos($payout->txid, "[") !== false ? XEncodeHTML($payout->txid) : "<a href='".BRAND_TX_LINK.XEncodeHTML($payout->txid)."'>".XEncodeHTML($payout->txid)."</a>") : "&nbsp;");
					}
					//---------------------------
					$attributes['Week Points'] = $selStat->weekPoints;
					$attributes['Total Points'] = $selStat->totPoints;
					$attributes['Work Units'] = $selStat->wus;
					//---------------------------
					foreach ($attributes as $name => $value)
						echo "<tr class='admin-row'><td>$name</td><td><div style='overflow: auto;'>$value</div></td></tr>\n";
					//---------------------------
					echo "</table>\n";
					//---------------------------
					if ($Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
					{
						echo "<br/><div>Privileged:</div>\n<br/>";
						echo "<input type='submit' name='action' value='Delete' /><br/>\n";
						echo "<br/>\n";
						echo "Week Points: <input style='margin-bottom:4px;' id='selwp' type='text' name='selwp' value='".$selStat->weekPoints."' maxlength='10'/><br/>\n";
						echo "Total Points: <input style='margin-bottom:4px;' id='seltp' type='text' name='seltp' value='".$selStat->totPoints."' maxlength='10'/><br/>\n";
						echo "<input type='submit' name='action' value='Update Points' /><br/>\n";
						echo "<br/>\n";
					}
					//---------------------------
				?>
				<br/>
				<br/>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
		<?php
		//---------------------------
		} // if ($sidx !== false && $selStat !== false)
		//---------------------------
		if ($Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
		{
			//---------------------------
			?>
			<div class="col1 pad_left1" style="width:800px;"><!-- Start Right Column -->
			<div class="admin-edit" style="margin: auto;">
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<br/>
					<h4>Create Stat Manually:</h4>
					<br/>
					<div>Privileged:</div><br/>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $sidx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
					<input type="hidden" name="lmt" value="<?php echo ($limit === false ? 0 : $limit); ?>"/>
			<?php
			//---------------------------
			if ($fltRnd !== false)
				echo "<input type='hidden' name='fltRnd' value='$fltRnd' />";
			if ($fltWrk !== false)
				echo "<input type='hidden' name='fltWrk' value='$fltWrk' />";
			//---------------------------
			echo "<input type='submit' name='action' value='Delete All From Round' />\n";
			echo "Round index: <input id='dridx' type='text' name='dridx' value='' maxlength='5'/><br/>\n";
			echo "<br/>\n";
			echo "Round <input style='margin-bottom:4px;' id='ridx' type='text' name='ridx' value='' maxlength='5'/><br/>\n";
			echo "Worker index: <input style='margin-bottom:4px;' id='widx' type='text' name='widx' value='' maxlength='5'/><br/>\n";
			echo "Work/Week Points: <input style='margin-bottom:4px;' id='wpnt' type='text' name='wpnt' value='0' maxlength='10'/><br/>\n";
			echo "Total Points: <input style='margin-bottom:4px;' id='tpnt' type='text' name='tpnt' value='0' maxlength='10'/><br/>\n";
			echo "Work Units: <input style='margin-bottom:4px;' id='wus' type='text' name='wus' value='0' maxlength='10'/><br/>\n";
			echo "<br/>\n";
			echo "Duplicate count: <input id='dcnt' type='text' name='dcnt' value='1' maxlength='5'/><br/>\n";
			echo "<input type='submit' name='action' value='Add' /><br/>\n";
			echo "<br/>\n";
			echo "<br/>\n";
			//---------------------------
			?>
				</fieldset>
			</form>
			</div>
			<br/>
			</div><!-- End Right Column -->
			<?php
			//---------------------------
		} // if ($Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
		//---------------------------
		?>
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
