<?php
//---------------------------------
/*
 * include/pages/admin/Monitor.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Monitor = new Monitor() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$Rounds = new Rounds() or die("Create object failed");
//---------------------------------
$pagenum = XGetPost('p');
$action = XPost('action');
$defFilters = ($action === false || $action == "" ? true : false);
//---------------------------------
$selID = XGetPost('id');
if (!is_numeric($selID))
	$selID = false;
//---------------------------------
$fltRnd = XGetPost('fltRnd');
if (!is_numeric($fltRnd))
	$fltRnd = false;
else
	$defFilters = false;
//---------------------------------
$colSort = XGetPost('srt');
if (!is_numeric($colSort) || $colSort == 0 || abs($colSort) > 5)
	$colSort = -1;
//---------------------------------
$limit = XGetPost("lmt");
if ($limit === false || $limit == "" || !is_numeric($limit))
	$limit = 200;
else 
{
	$defFilters = false;
	if ($limit == 0)
		$limit = false;
}
//---------------------------------
$locId = XGetPost("locId");
if ($locId == "" || !is_numeric($locId))
	$locId = false;
else
	$defFilters = false;
//---------------------------------
$selEntry = false;
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="width: 98%;"><!-- Start Left Column -->
		<div>
			<h1>Payouts:</h1>
			<p>
			Folding @ Home Payout list.
			</p>
			<br/>
		</div>
		<?php
			//------------------------
			// action handling
			//$action == "List Update"  do nothing
			if ($selID !== false && ($action == "User Fix" || $action == "User Bypass" || $action == "UnResolve"))
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					if ($action == "User Fix")
						$resolveFlag = MONEVENT_FLAG_USER_FIX;
					else // if ($action == "User Bypass")
						$resolveFlag = MONEVENT_FLAG_USER_BYPASS;
					//------------------------
					$event = $Monitor->getEvent($selID);
					//------------------------
					if ($event === false)
					{
						XLogNotify("Monitor admin page - $action validate selected monitor event failed. id: ".XVarDump($selID));
						echo "Monitor event id $selID not found.<br/><br/>\n";
					}
					else if ($action != 'UnResolve' && !$event->resolve($resolveFlag, false /*pushRoundAutomation*/))
					{
						XLogNotify("Monitor admin page - $action event resolve failed");
						echo "Monitor event resolve failed.<br/><br/>\n";
					}
					else if ($action == 'UnResolve' && !$event->unresolve())
					{
						XLogNotify("Monitor admin page - $action event unresolve failed");
						echo "monitor event UnResolve failed.<br/><br/>\n";
					}
					else 
						echo "Monitor event set resolved to '$action'.<br/><br/>\n";
					//------------------------
				} // HasPrivilege
			} // action: User Fix, User Bypass, UnResolve
			//------------------------	
			$Columns = array(
								array(	70, 	"ID",		1),
								array(	60,		"Rnd",		2),
								array(	200,	"Date",		3),
								array(  150,    "Type",     4),
								array(  420,    "Issue",    5)
							);
			//------------------------
			echo "<script>\nfunction selRow(eid){\n document.location.href='./Admin.php?p=$pagenum&id=' + eid + '&srt=$colSort&lmt=".($limit === false ? "" : $limit).($fltRnd === false ? "" : "&fltRnd=$fltRnd")."&".SecToken()."'; }\n</script>\n";
			echo "<script>\nfunction selRound(obj, eid){\n var id = obj.innerHTML; window.open('./Admin.php?p=6&rid=' + id + '&meid=' + eid + '&".SecToken()."');\n }\n</script>\n";
			echo "<script>\nfunction selCol(id, sort=$colSort){\n document.location.href='./Admin.php?p=$pagenum&id=' + id + '&srt=' + sort + '&lmt=".($limit === false ? "" : $limit).($fltRnd === false ? "" : "&fltRnd=$fltRnd")."&".SecToken()."'; }\n</script>\n";
			echo "<table class='admin-table' style='width:915px;'>\n<thead class='admin-scrolltable-header'>\n<tr>\n";
			//------------------------	
			foreach ($Columns as $col)
			{
				$style = "";
				if ($col[0] !== false)
					$style .= "width:$col[0]px;";
				if (abs($colSort) == $col[2])
					$style .= "background:#EEEEEE;";
				echo "\t<th".($style != "" ? " style='$style'" : "");
				echo " onclick=\"selCol(".($selID === false ? "''" : $selID).", ".(abs($colSort) == $col[2] ? (-1 * $colSort) : $col[2]).");\">$col[1]</th>\n";
			}
			//------------------------	
			echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
			//------------------------
			$sqlSort = '';
			$acolSort = abs($colSort);
			if ($acolSort == 1)
				$sqlSort = DB_MONEVENT_ID;
			else if ($acolSort == 2)
				$sqlSort = DB_MONEVENT_ROUND;
			else if ($acolSort == 3)
				$sqlSort = DB_MONEVENT_DATE;
			else if ($acolSort == 4)
				$sqlSort = DB_MONEVENT_TYPE;
			else if ($acolSort == 5)
				$sqlSort = DB_MONEVENT_ISSUE;
			if ($colSort < 0)
				$sqlSort .= " DESC";
			//------------------------	
			if ($locId !== false || ($defFilters === true && $selID !== false))
			{ // need selected one
				if ($locId !== false) // locID is locate ID
					$selID = $locId;
				$singleEntry = $Monitor->getEvent($selID);
				$entryList = array($singleEntry);
			}
			else
			{
				$where = false;
				if ($fltRnd !== false && is_numeric($fltRnd))
					$where = DB_MONEVENT_ROUND."=$fltRnd";
				$entryList = $Monitor->getEvents($where, $sqlSort /*orderBy*/, $limit);
			}
			//------------------------	
			if ($entryList === false)
				XLogError("Monitor admin page - getEvent/getEvents failed");
			if ($entryList === false || sizeof($entryList) == 0)
			{
				//------------------------
				echo "<tr class='admin-row'>";
				foreach ($Columns as $col)
					echo "<td style='width:".$col[0]."px;'>&nbsp;</td>";
				echo "</tr>\n";
				//------------------------
			}
			else
			{
				//------------------------
				$rowIdx = 0;
				foreach ($entryList as $entry)
				{
					//------------------------
					$strFlags = "";
					if ($entry->hasFlag(MONEVENT_FLAG_RESOLVED))
					{
						$strFlags = "(";
						if ($entry->hasFlag(MONEVENT_FLAG_SELF_FIX))
							$strFlags .= "Self-fixed ";
						if ($entry->hasFlag(MONEVENT_FLAG_USER_FIX))
							$strFlags .= "User-fixed ";
						if ($entry->hasFlag(MONEVENT_FLAG_USER_BYPASS))
							$strFlags .= "User-Bypassed";
						$strFlags .= ")";
					}
					//------------------------
					if ($entry->ridx == 0)
						$htmlRound = "(none)";
					else
						$htmlRound = "<a href='javascript:void(0);' onclick='selRound(this, $entry->id);'>$entry->ridx</a>";
					//------------------------
					if ($selID !== false && $selID == $entry->id)
					{
						$rowClass = 'admin-row-sel';
						$selEntry = $entry;
					}
					else $rowClass = 'admin-row';
					//------------------------
					echo "<tr class='$rowClass' onclick='selRow($entry->id);' \">\n";
					//------------------------
					$colData = array(	$entry->id,
										$htmlRound,
										$Display->htmlLocalDate($entry->date),
										$Monitor->typeName($entry->type)."<br/>".$strFlags,
										$Monitor->issueName($entry->issue)
										);
					//------------------------
					for ($i = 0;$i < sizeof($colData);$i++)
					{
						$style = "";
						if ($Columns[$i][0] !== false && $rowIdx == 0)
							$style .= "width:".$Columns[$i][0]."px;";
						/*
						if ($Columns[$i][0] !== false) // $rowIdx == 0 && 
							$style .= "width:".$Columns[$i][0]."px;";
						if ($Columns[$i][1] == "Payment")
							$style .= "text-align:right;";
						if ($Columns[$i][1] == "Worker")
							$style .= "display: block;overflow: hidden;white-space:nowrap;";
						*/
						if ($style != "")
							$style = " style='$style'";
						echo "<td$style>$colData[$i]</td>";
					}
					//------------------------
					echo "</tr>\n";
					//------------------------
					$rowIdx++;
					//------------------------
				} // foreach
			}
			//------------------------
			echo "</tbody></table>\n";		
			//------------------------	
		?>

		<div class="admin-edit pad_left1" style="width:550px;">
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<?php
						//---------------------------
						// filter, limit, and locate controls
						//---------------------------
						echo "<div style='float:left;padding-right:40px;margin-right:40px;border-right: 1px solid black;'>\n";
						echo "Locate Event ID: <input style='width:80px;' id='locId' type='text' name='locId' value='".($locId === false ? "" : $locId)."' maxlength='6'/><br/>\n";
						//echo "<br/>List Store Paid<br/>By Payout ID: <input style='width:80px;' id='fltSp' type='text' name='fltSp' value='".($fltSp === false ? "" : $fltSp)."' maxlength='6'/><br/>\n";
						echo "</div>\n";
						echo "<div style='margin:auto;'>\n";
						echo "&nbsp;Limit rows: <input style='width:80px;' id='lmt' type='text' name='lmt' value='".($limit === false ? 0 : $limit)."' maxlength='6'/><br/><br/>\n";
						echo "Filter Round: <input style='width:80px;' id='fltRnd' type='text' name='fltRnd' value='".($fltRnd === false ? "" : $fltRnd)."' maxlength='6'/><br/>\n";
						echo "</div>\n";
						echo "<div style='text-align:center;'><input style='margin-bottom:4px;' type='submit' name='action' value='List Update' /></div>\n";
						//---------------------------
						if ($selID !== false)
							echo "<input type='hidden' name='id' value='$selID' />";
						//---------------------------
					?>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>" />
					<input type="hidden" name="srt" value="<?php echo $colSort; ?>" />
				</fieldset>
			</form>
		</div>
		</div><!-- End Left Column -->
		<?php
		//---------------------------
		if ($selID !== false && $selEntry !== false)
		{
			//---------------------------
		?>
			<div class="col1 pad_left1" style="width:865px;"><!-- Start Right Column -->
			<div class="admin-edit" style="margin-right: 40px;" >
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<legend style="font-weight:bold;font-size:120%;">Monitor Event Details:</legend>
					<br/>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="id" value="<?php echo $selID; ?>" />
					<input type="hidden" name="srt" value="<?php echo $colSort; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
					<?php
						//---------------------------
						if ($locId !== false)
							echo "<input type='hidden' name='locId' value='$locId' />";
						if ($selID !== false)
							echo "<input type='hidden' name='fltRnd' value='$fltRnd' />";
						if ($limit !== false)
							echo "<input type='hidden' name='lmt' value='$limit' />";
						//---------------------------
						
						// Display selected Entry
						//---------------------------
						$strType = XEncodeHTML($Monitor->typeName($selEntry->type));
						//------------------------
						$isResolved = $selEntry->isResolved();
						if ($isResolved)
						{
							$strResolved = '';
							if ($selEntry->hasFlag(MONEVENT_FLAG_SELF_FIX))
								$strResolved .= 'Self-Fixed';
							if ($selEntry->hasFlag(MONEVENT_FLAG_USER_FIX))
								$strResolved .= 'User Fixed';
							if ($selEntry->hasFlag(MONEVENT_FLAG_USER_BYPASS))
								$strResolved .= 'User Bypass';
						}
						else $strResolved = 'No';
						$strResolved = XEncodeHTML($strResolved);
						$strIssue = XEncodeHTML($Monitor->issueName($selEntry->issue));
						//------------------------
						$actionNames = $Rounds->getActionTextNames();
						//------------------------
						$strDetails = '';
						$curstep = $curAction = false;
						$oldStep = $oldAction = false;
						$prog = $progMax = false;
						$detailsList = array();
						foreach ($selEntry->details as $key => $value)
						{
							if ($key == MONEVENT_DETAIL_ROUND_STEP || $key == MONEVENT_DETAIL_ROUND_ACTION)
							{
								if ($key == MONEVENT_DETAIL_ROUND_STEP)
									$curstep = $value;
								else
									$curAction = (isset($actionNames[$value]) ? $actionNames[$value] : "[Unknown $value]");
								if ($curstep !== false && $curAction !== false)
								{
									$detailsList[] = "Step=($curstep)'$curAction'";
									$curstep = $curAction = false;
								}
							}
							else if ($key == MONEVENT_DETAIL_ROUND_OLD_STEP || $key == MONEVENT_DETAIL_ROUND_OLD_ACTION)
							{
								if ($key == MONEVENT_DETAIL_ROUND_OLD_STEP)
									$oldStep = $value;
								else
									$oldAction = (isset($actionNames[$value]) ? $actionNames[$value] : "[Unknown $value]");
								if ($oldStep !== false && $oldAction !== false)
								{
									$detailsList[] = "Old Step=($oldStep)'$oldAction'";
									$oldStep = $oldAction = false;
								}
							}
							else if ($key == MONEVENT_DETAIL_ROUND_PROG || $key == MONEVENT_DETAIL_ROUND_PROG_MAX)
							{
								if ($key == MONEVENT_DETAIL_ROUND_PROG)
									$prog = $value;
								else
									$progMax = $value;
								if ($prog !== false && $progMax !== false)
								{
									if ($prog != 0 || $progMax != 0)
										$detailsList[] = "Prog=$prog/$progMax";
									$prog = $progMax = false;
								}
							}
							else
								$detailsList[] = $Monitor->detailName($key)."=$value";
						}
						//------------------------
						if ($selEntry->type == MONEVENT_TYPE_NOTIFY && $selEntry->issue == MONEVENT_ISSUE_ROUNDSTEP_COMPLETED && isset($selEntry->details[MONEVENT_DETAIL_ROUND_OLD_STEP]) && isset($selEntry->details[MONEVENT_DETAIL_ROUND_OLD_ACTION]) && isset($selEntry->details[MONEVENT_DETAIL_ELAPSED]) )
						{
							//------------------------
							$step = $selEntry->details[MONEVENT_DETAIL_ROUND_OLD_STEP];
							$actionValue = $selEntry->details[MONEVENT_DETAIL_ROUND_OLD_ACTION];
							$action = (isset($actionNames[$actionValue]) ? $actionNames[$actionValue] : "[Unknown $actionValue]");
							$elapsed = (isset($selEntry->details[MONEVENT_DETAIL_ELAPSED_FULL]) ? $selEntry->details[MONEVENT_DETAIL_ELAPSED_FULL] : $selEntry->details[MONEVENT_DETAIL_ELAPSED]);
							$stepCount = (isset($selEntry->details[MONEVENT_DETAIL_ROUND_STEP_COUNT]) && $selEntry->details[MONEVENT_DETAIL_ROUND_STEP_COUNT] > 1 ? ' count '.$selEntry->details[MONEVENT_DETAIL_ROUND_STEP_COUNT] : '');
							$strDetails = XEncodeHTML("Step ($step)'$action' took $elapsed sec$stepCount");
							//------------------------
						}
						else
							$strDetails = XEncodeHTML(implode(', ', $detailsList));
						//------------------------
						echo "<div style='border:1px solid black;margin:15px;padding:20px;width:620px;'><h4>Monitor Event $selEntry->id</h4>\n";
						echo "<table class='admin-table' style='width:600px;'>\n";
						echo "<tbody class='admin-scrolltable-body' style='max-height:500px;'>\n"; // override max-height:200px from admin-scrolltable-bod
						//------------------------
						echo "<tr class='admin-row'><td style='min-width:100px;'>Round</td><td style='width:100%;'>".
									($selEntry->ridx == 0 ? "(none)" :
										"<a href='./Admin.php?p=6&rid=$selEntry->ridx&meid=$selEntry->id&".SecToken()."'>$selEntry->ridx</a>").
									"</td></tr>\n";
						//------------------------
						echo "<tr class='admin-row'><td style='min-width:200px;'>Type</td><td style='width:100%;'>$strType</td></tr>\n";
						echo "<tr class='admin-row'><td>Issue</td><td>$strIssue</td></tr>\n";
						//------------------------
						if ($selEntry->type != MONEVENT_TYPE_NOTIFY)
						{
							echo "<tr class='admin-row'><td>Resolved</td><td>$strResolved&nbsp;";
							if (!$isResolved)
							{
								echo "\n<input name='action' type='submit' value='User Fix' />\n";
								echo "<input name='action' type='submit' value='User Bypass' />\n";
							}
							else 
								echo "<input name='action' type='submit' value='UnResolve' />\n";
							echo "</td></tr>\n";
						}
						//------------------------
						$strDate = XEncodeHTML($Display->htmlLocalDate($selEntry->date));
						echo "<tr class='admin-row'><td>Start Date</td><td>$strDate</td></tr>\n";
						if (is_numeric($selEntry->endDate) && $selEntry->date != $selEntry->endDate)
						{
							$strDate = XEncodeHTML($Display->htmlLocalDate($selEntry->endDate));
							echo "<tr class='admin-row'><td>End Date</td><td>$strDate</td></tr>\n";
							$strDiff = XEncodeHTML(XDateDiffFuzzy($selEntry->date, $selEntry->endDate));
							if ($strDiff !== false)
								echo "<tr class='admin-row'><td>Duration</td><td>$strDiff</td></tr>\n";							
						}
						//------------------------
						foreach ($detailsList as $detail)
						{
							$detailParts = explode('=', $detail);
							$name = XEncodeHTML($detailParts[0]);
							$value = XEncodeHTML($detailParts[1]);
							echo "<tr class='admin-row' style='background-color:#c4aead;'><td>$name</td><td>$value</td></tr>\n";
						}
						//------------------------
						echo "</tbody></table></div>\n";
						//------------------------
					?>
					<br/>
					<br/>
				</fieldset>
			</form>
			</div>
			<br/>
			</div><!-- End Right Column -->
		<?php
			//---------------------------
		} // if selected
		//---------------------------
		?>
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
