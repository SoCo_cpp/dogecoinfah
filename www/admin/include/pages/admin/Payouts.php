<?php
//---------------------------------
/*
 * include/pages/admin/Payouts.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Payouts = new Payouts() or die("Create object failed");
$Workers = new Workers() or die("Create object failed");
$Rounds  = new Rounds() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XPost('action');
$pidx = XGetPost('idx');
$selPayout = false;
$pSort = XGetPost('srt');
if (!is_numeric($pSort) || $pSort == 0 || abs($pSort) > 4)
	$pSort = -1;
$defFilters = ($action === false || $action == "" ? true : false);
if ($pidx == "")
	$pidx = false;	
$limit = XGetPost("lmt");
if ($limit === false || $limit == "" || !is_numeric($limit))
	$limit = 500;
else 
{
	$defFilters = false;
	if ($limit == 0)
		$limit = false;
}
$fltRnd = XGetPost("fltRnd");
if ($fltRnd == "" || !is_numeric($fltRnd))
	$fltRnd = false;
else
	$defFilters = false;
$fltWrk = XGetPost("fltWrk");
if ($fltWrk == "" || !is_numeric($fltWrk))
	$fltWrk = false;
else
	$defFilters = false;
$fltSp = XGetPost("fltSp");
if ($fltSp == "" || !is_numeric($fltSp))
	$fltSp = false;
else
	$defFilters = false;
$locId = XGetPost("locId");
if ($locId == "" || !is_numeric($locId))
	$locId = false;
else
{
	$fltSp = false; // overridden
	$defFilters = false;
}
$selRoundStr = "";
$selWorkerStr = "";
$dridx = XPost('dridx', false); // delete all from round Idx
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1" style="width: 98%;"><!-- Start Left Column -->
		<div>
			<h1>Payouts:</h1>
			<p>
			Folding @ Home Payout list.
			</p>
			<br/>
		</div>
		<?php
			//------------------------	
			if ($action == "Delete" && $pidx !== false)
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					XLogNotify("User $Login->User is deleting payout: $pidx");
					if (!$Payouts->deletePayout($pidx))
					{
						XLogNotify("Payouts admin page - Payouts failed to deletePayout $pidx");
						echo "Failed to delete payout.<br/><br/>\n";
					}
					else
					{
						echo "Payout successfully deleted.<br/><br/>\n";
						$pidx = false;
					}
				}
				else 
					echo "Deleting historical data required 'D' delete privileges.<br/><br/>\n";
			}
			else if ($action == "Delete All From Round")
			{
				//------------------------	
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					if ($dridx !== false && is_numeric($dridx))
					{
						XLogNotify("User $Login->User is deleting all payouts from round: $dridx");
						if (!$Payouts->deleteAllRound($dridx))
						{
							XLogNotify("Payout admin page - payouts failed to delete all from round $dridx");
							echo "Failed to delete all from round $dridx.<br/><br/>\n";
						}
						else
						{
							echo "Payouts successfully deleted.<br/><br/>\n";
							$pidx = false;
						}
						
					}
					else "Deleting all from round, validate round index failed.<br/><br/>\n";
				}
				else 
					echo "Deleting historical data required 'D' delete privileges.<br/><br/>\n";
				//------------------------	
			}
			else if ($action == "Unpaid payout report")
			{
				//------------------------	
				if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
				{
					//------------------------	
					XLogNotify("User $Login->User is generating an unpaid payout report");
					//------------------------
					$payoutList = $Payouts->loadStoredPayouts(false /*$onlyPaid*/, true /*$onlyUnpaid*/);
					//------------------------
					if ($payoutList === false)
					{
						XLogNotify("Payout admin page - $action Payouts failed to loadStoredPayouts");
						echo "$action failed.<br/><br/>\n";
					}
					else
					{
						//------------------------	
						$workerPayouts = array();
						//------------------------	
						foreach ($payoutList as $payout)
						{
							if (!isset($workerPayouts[$payout->workerIdx]))
								$workerPayouts[$payout->workerIdx] = array();
							$workerPayouts[$payout->workerIdx][] = $payout;
						}
						//------------------------
						$fullTotal = "0";
						$summary = array();
						foreach ($workerPayouts as $widx => $workerPayoutList)
						{
							$total = "0";
							$address = "";
							foreach ($workerPayoutList as $payout)
							{
								$total = bcadd($total, $payout->statPay, 8);
								if ($address == "")
									$address = $payout->address;
								else if ($address !== false && $address != $payout->address)
									$address = false; // oddity, covers more than one address								
							}
							$workerTotals[$widx] = $total;
							$fullTotal = bcadd($fullTotal, $total, 8);
							$summary[] = array('widx' => $widx, 'total' => $total, 'address' => $address, 'payouts' => $workerPayoutList);
						}
						//------------------------	
						$done = false;
						while (!$done)
						{
							$done = true;
							for ($i = 1;$i < sizeof($summary);$i++)
								if (bccomp($summary[$i-1]['total'], $summary[$i]['total'], 8) < 0)
								{
									$tmp = $summary[$i-1];
									$summary[$i-1] = $summary[$i];
									$summary[$i] = $tmp;
									$done = false;
								}
						}
						//------------------------	
						echo "<div>\n";
						echo "<p class='admin-row' style='margin-left:20px;width:500px;'>Unpaid Worker Summary:<br/>".sizeof($summary)." unpaid workers, totaling $fullTotal ".BRAND_UNIT."</p>\n";
						$simplCSV = XGetPost('rptcsv');
						if ($simplCSV == "")
						{
							$cols = array(	array("Worker", 200), 
											array(BRAND_UNIT, 80),
											array("Payout", 75),
											array("Round", 75),
											array("Date", 100),
											array("Notes", 100));
											
							echo "<script>\nfunction selWorker(obj){\n var id = obj.innerHTML; window.open('./Admin.php?p=5&idx=' + id + '&".SecToken()."');\n }\n</script>\n";
							echo "<script>\nfunction selRound(obj){\n var id = obj.innerHTML; window.open('./Admin.php?p=6&rid=' + id + '&".SecToken()."');\n }\n</script>\n";
							echo "<script>\nfunction selPayout(obj){\n var id = obj.innerHTML; window.open('./Admin.php?p=7&idx=' + id + '&".SecToken()."');\n }\n</script>\n";
							echo "<table class='admin-table' style='width:640px;'>\n<thead class='admin-scrolltable-header'>\n<tr class='admin-row'>\n";
							echo "<script>\nvar prevOl=window.onload;\nwindow.onload=function(){\n";
							echo "if(prevOl!=null) prevOl();\n";
							echo "var l=document.getElementsByName('tsw');\nfor(var i=0;i<l.length;i++)\n {l[i].href='javascript:void(0);';l[i].onclick=function(){selWorker(this);};}\n";
							echo "var l=document.getElementsByName('tsr');\nfor(var i=0;i<l.length;i++)\n {l[i].href='javascript:void(0);';l[i].onclick=function(){selRound(this);};}\n";
							echo "var l=document.getElementsByName('tsp');\nfor(var i=0;i<l.length;i++)\n {l[i].href='javascript:void(0);';l[i].onclick=function(){selPayout(this);};}\n";
							echo "}\n</script>\n";
							foreach ($cols as $col)
								echo "<th".($col[1] !== false ? " style='width:".$col[1]."px;'" : "").">".$col[0]."</th>";
							echo "</tr></thead><tbody class='admin-scrolltable-body'>\n";
							$firstRow = true;
							foreach ($summary as $entry)
							{
								echo "<tr class='admin-row'><td".($firstRow && $cols[0][1] !== false ? " style='width:".$cols[0][1]."px;'" : "").(sizeof($entry['payouts']) > 1 ? " rowspan='".sizeof($entry['payouts'])."'" : "").">";
								echo "(<a name='tsw'>".$entry['widx']."</a>) ".($entry['address'] === false ? "[multiple addresses]" : substr($entry['address'], 0, 7)."...");
								echo "</td>";
								$firstPayout = true;
								foreach ($entry['payouts'] as $payout)
								{
									if (!$firstPayout)
										echo "<tr class='admin-row'>";
									else 
										$firstPayout = false;
									echo "<td".($firstRow && $cols[1][1] !== false ? " style='width:".$cols[1][1]."px;'>" : ">").$payout->statPay."</td>\n";
									echo "<td".($firstRow && $cols[2][1] !== false ? " style='width:".$cols[2][1]."px;'>" : ">")."<a name='tsp'>".$payout->id."</a></td>\n";
									echo "<td".($firstRow && $cols[3][1] !== false ? " style='width:".$cols[3][1]."px;'>" : ">")."<a name='tsr'>".$payout->roundIdx."</a></td>\n";
									echo "<td".($firstRow && $cols[4][1] !== false ? " style='width:".$cols[4][1]."px;'>" : ">").$Display->htmlLocalDate($payout->dtcreated)."</td>\n";

									$notes = "";
									if ($payout->dtPaid !== false)
										$notes .= " [dtPaid set ".$Display->htmlLocalDate($payout->dtPaid)."]";
									if ($payout->storePayout !== false)
										$notes .= " [storePayout set <a name='tsp'>".$payout->storePayout."</a> ".($Payouts->loadPayout($payout->storePayout) === false ? "(doesn't exist)" : "")."]";
									if ($notes == "")
										$notes = "&nbsp;";
									echo "<td".($firstRow && $cols[5][1] !== false ? " style='width:".$cols[5][1]."px;'>" : ">")."$notes</td>\n";
									echo "</tr>\n";
								}
								if ($firstRow)
									$firstRow = false;
							}
							echo "</tbody></table>\n";
							echo "</div><br/><br/>\n";
						}
						else // if ($simplCSV == "")
						{
							echo "<div class='admin-row' style='margin-left:20px;padding:10px;width:615px;'>\n";
							$txt = "WorkerID,Address,PayoutID,RoundID,DateTime,Value\n";
							foreach ($summary as $entry)
								foreach ($entry['payouts'] as $payout)
									$txt .= $entry['widx'].",".($entry['address'] === false ? "[multiple addresses]" : $entry['address']).",$payout->id,$payout->roundIdx,$payout->dtcreated,$payout->statPay\n";
							echo "<textarea rows='15' cols='80' readonly='readonly'>$txt</textarea>\n";
							echo "</div><br/><br/>\n";
						}
						//------------------------	
					}	
					//------------------------	
				}
				else 
					echo "Unpaid payout reporting requires wallet manage privileges.<br/><br/>\n";
				//------------------------	
			}
			else if ($action == "Paid by missing payouts report" || $action == "Un-pay paid by missing payouts")
			{
				//------------------------	
				if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
				{
					//------------------------	
					XLogNotify("User $Login->User is $action");
					//------------------------
					$payoutList = $Payouts->findPaidByMissingPayouts();
					//------------------------
					if ($payoutList === false)
					{
						XLogNotify("Payout admin page - $action Payouts failed to findDeletePaidByMissingPayouts");
						echo "$action failed.<br/><br/>\n";
					}
					else
					{
						//------------------------	
						echo "<div><div>Paid by missing payouts found: ".sizeof($payoutList)." </div>\n<ul>";
						foreach ($payoutList as $payout)
							echo "<li>Payout id $payout->id, Round $payout->roundIdx, dtPaid '".($payout->dtPaid === false ? '(not paid)' : $Display->htmlLocalDate($payout->dtPaid))."', by now missing payout id".XVarDump($payout->storePayout)."</li>";
						echo "</ul></div><br/>\n";
						//------------------------	
						$succes = true;
						if ($action == "Un-pay paid by missing payouts")
						{
							//------------------------	
							foreach ($payoutList as $payout)
							{
								$payout->dtPaid = false;
								$payout->storePayout = false;
								if (!$payout->Update($payout->id))
								{
									XLogNotify("Payout admin page - $action payout id $payout->id failed to Update");
									echo "$action failed.<br/><br/>\n";
									$succes = false;
									break;
								}
							}
							//------------------------	
						}
						//------------------------	
						if ($succes)
						{
							XLogNotify("Payout admin page - $action completed by $Login->User");
							echo "$action successfully.<br/><br/>\n";
						}
						//------------------------	
					}
					//------------------------	
				}
				else 
					echo "Paid by missing payouts reporting requires wallet manage privileges.<br/><br/>\n";
				//------------------------	
			}
			else if ($action == "Forfeit report")
			{
				//------------------------	
				if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
				{
					//------------------------	
					XLogNotify("User $Login->User is $action");
					//------------------------
					$summary = $Rounds->getForfeitPayoutSummary(/*dtNow default now*/ /*forfeitDays default configured*/ /*forfeitIdleDays default configured*/);
					//------------------------
					if ($summary === false)
					{
						XLogNotify("Payout admin page - $action Rounds failed to getForfeitPayoutSummary");
						echo "$action failed.<br/><br/>\n";
					}
					else
					{
						//------------------------	
						echo "<div style='margin-left:40px;'>\n";
						echo "<div>Totals:<br/>\n<ul style='margin-left:60px;'>";
						echo "<li>Value:&nbsp;".$summary['totals']['value']."</li>\n";
						echo "<li>Workers:&nbsp;".$summary['totals']['workers']."</li>\n";
						echo "<li>Payouts:&nbsp;".$summary['totals']['payouts']."</li>\n";
						echo "</ul></div><br/>\n";
						//------------------------	
						echo "<div>Tests:<br/>\n<ul style='margin-left:60px;'>";
						foreach ($summary['tests'] as $testName => $testData)
						{
							echo "<li>$testName:&nbsp;";
							if ($testData['enabled'] !== true)
								echo "(disabled)";
							else
							{
								$strNow = ($testData['dtNow'] === false ? '(none)' : gmdate(MYSQL_DATETIME_FORMAT, $testData['dtNow']));
								$strLimit = ($testData['dtLimit'] === false ? '(none)' : gmdate(MYSQL_DATETIME_FORMAT, $testData['dtLimit']));
								echo 'limit '.$testData['limit'].' from '.$strNow.', cutoff '.$strLimit." found ".$testData['workerCount']." workers with ".$testData['payoutCount']." payouts";
							}
							echo "</li>\n";
						}						
						echo "</ul></div><br/>\n";
						//------------------------	
						echo "<div><div>Forfeit report:</div><br/>\n<ul style='margin-left:60px;'>";
						foreach ($summary as $workerIdx => $data)
							if (is_numeric($workerIdx))
							{
								echo "<li>Worker ID $workerIdx, total value to forfeit ".$data['total']."<br/>";
								echo "<ul style='margin-left:80px;'>";
								foreach ($data['payouts'] as $forfeitPayout)
								{
									$payout = $forfeitPayout['payout'];
									$reasons = $forfeitPayout['reasons'];
									$txtReasons = "";
									foreach ($forfeitPayout['reasons'] as $reasonData)
										$txtReasons .= "(".$reasonData['test'].": ".$reasonData['details'].")";
									echo "<li>Payout $payout->id (Rnd $payout->roundIdx) value $payout->statPay, reasons: $txtReasons</li>";
								}
								echo "</ul>";
							}
						echo "</ul></div></div><br/>\n";
						//------------------------	
						XLogNotify("Payout admin page - $action completed by $Login->User");
						//------------------------	
						$txt = $Rounds->printForfeitPayoutSummary($summary);
						if ($txt === false)
							XLogError("Rounds printForfeitPayoutSummary failed");
						else
							XLogDebug($txt);
						echo "$action successfully.<br/><br/>\n";
						//------------------------	
					}
					//------------------------	
				}
				else 
					echo "Forfeit reporting requires wallet manage privileges.<br/><br/>\n";
				//------------------------	
			}
			//------------------------	
			$Columns = array(	array(	100,	"ID",		1),
								array(	150,	"Round",	2),
								array(	450,	"Worker",	3),
								array(	140,	"Payment",	4)
								);
			//------------------------	
			echo "<script>\nfunction selPay(id, sort=$pSort){\n document.location.href='./Admin.php?p=$pagenum&idx=' + id + '&srt=' + sort + '&lmt=".($limit === false ? "" : $limit).($fltRnd === false ? "" : "&fltRnd=$fltRnd").($fltWrk === false ? "" : "&fltWrk=$fltWrk")."&".SecToken()."'; }\n</script>\n";
			//------------------------	
			echo "<table class='admin-table' style='width:900px;'>\n<thead class='admin-scrolltable-header'>\n<tr>\n";
			//------------------------	
			foreach ($Columns as $col)
			{
				$style = "";
				if ($col[0] !== false)
					$style .= "width:$col[0]px;";
				if (abs($pSort) == $col[2])
					$style .= "background:#EEEEEE;";
				echo "\t<th".($style != "" ? " style='$style'" : "");
				echo " onclick=\"selPay(".($pidx === false ? "''" : $pidx).", ".(abs($pSort) == $col[2] ? (-1 * $pSort) : $col[2]).");\">$col[1]</th>\n";
			}
			//------------------------	
			echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
			//------------------------
			$aSort = abs($pSort);
			if ($aSort == 1)
				$sort = DB_PAYOUT_ID;
			else if ($aSort == 2)
				$sort = DB_PAYOUT_ROUND;
			else if ($aSort == 3)
				$sort = DB_PAYOUT_ADDRESS;
			else if ($aSort == 4)
				$sort = DB_PAYOUT_STAT_PAY;
			if ($pSort < 0)
				$sort .= " DESC";
			//------------------------	
			if ($action == "Unpaid payout report") // skip normal display
				$plist = array();
			else if ($locId !== false || ($defFilters === true && $pidx !== false))
			{ // need selected one
				if ($locId !== false)
					$pidx = $locId;
				$singlePayout = $Payouts->loadPayout($pidx);
				$plist = array($singlePayout);
			}
			else
			{
				$where = false;
				$where = false;
				if ($fltRnd !== false && is_numeric($fltRnd))
					$where = DB_PAYOUT_ROUND."=$fltRnd";
				if ($fltWrk !== false && is_numeric($fltWrk))
					$where = ($where !== false ? "$where AND " : "").DB_PAYOUT_WORKER."=$fltWrk";
				if ($fltSp !== false && is_numeric($fltSp))
					$where = ($where !== false ? "$where AND " : "").DB_PAYOUT_STORE_PAYOUT."=$fltSp";
				$plist = $Payouts->getPayouts($limit, $sort /*orderBy*/, $where);
			}
			//------------------------	
			if ($plist === false)
				XLogError("Payouts admin page - findRoundPayouts/getPayouts failed");
			if ($plist === false || sizeof($plist) == 0)
			{
				//------------------------
				echo "<tr class='admin-row'>";
				foreach ($Columns as $col)
					echo "<td style='width:".$col[0]."px;'>&nbsp;</td>";
				echo "</tr>\n";
				//------------------------
			}
			else
			{
				//------------------------
				$rowIdx = 0;
				foreach ($plist as $p)
				{
					//------------------------
					$strRoundDate = $Rounds->getRoundDate($p->roundIdx);
					if ($strRoundDate === false || $strRoundDate == "")
					{
						$strRound = $p->roundIdx;
						$strRoundDate = "";
					}
					else
					{
						$strRoundDate = $Display->htmlLocalDate($strRoundDate);
						$strRound = "($p->roundIdx) $strRoundDate";
					}
					//------------------------
					$strWorker = "($p->workerIdx) ";
					if ($p->workerIdx < 0)
					{
						$worker = false;
						if ($p->workerIdx == WORKER_SPECIAL_MAIN)
							$strWorker .= "(Special: Main/Forfeit)";
						else if ($p->workerIdx == WORKER_SPECIAL_STORE)
							$strWorker .= "(Special: Store)";
						else
							$strWorker .= "[Invalid Value]";
					}
					else
					{
						$worker = $Workers->getWorker($p->workerIdx);
						if ($worker === false)
							$strWorker .= "[Not Found]";
						else
							$strWorker .= XEncodeHTML($worker->address == "" ? $worker->uname : $worker->address);
					}
					//------------------------
					$strPay = ($p->pay != 0 ? $p->pay : "($p->statPay)");
					//------------------------
					if ($pidx !== false && $pidx == $p->id)
					{
						$rowClass = 'admin-row-sel';
						$selRoundDate = $strRoundDate;
						$selPayout = $p;
						$selWorker = $worker;
					}
					else $rowClass = 'admin-row';
					//------------------------
					echo "<tr class='$rowClass' onclick=\"document.location.href='./Admin.php?p=$pagenum&amp;idx=$p->id&amp;srt=$pSort&amp;".($fltRnd === false ? "" : "fltRnd=$fltRnd&amp;").($fltWrk === false ? "" : "fltWrk=$fltWrk&amp;").($limit === false ? "" : "lmt=$limit&amp;").SecToken()."';\">\n";
					//------------------------
					$colData = array(	$p->id,
										$strRound,
										$strWorker,
										$strPay
										);
					//------------------------
					for ($i = 0;$i < sizeof($colData);$i++)
					{
						$style = "";
						if ($Columns[$i][0] !== false) // $rowIdx == 0 && 
							$style .= "width:".$Columns[$i][0]."px;";
						if ($Columns[$i][1] == "Payment")
							$style .= "text-align:right;";
						if ($Columns[$i][1] == "Worker")
							$style .= "display: block;overflow: hidden;white-space:nowrap;";
						if ($style != "")
							$style = " style='$style'";
						echo "<td$style>$colData[$i]</td>";
					}
					//------------------------
					echo "</tr>\n";
					//------------------------
					$rowIdx++;
					//------------------------
				} // foreach
				//------------------------
			}
			//------------------------
			echo "</tbody></table>\n";		
			//------------------------	
		//</div>
		?>
		<div class="admin-edit pad_left1" style="width:550px;">
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<?php
						//---------------------------
						echo "<div style='float:left;padding-right:40px;margin-right:40px;border-right: 1px solid black;'>\n";
						echo "Locate Payout ID: <input style='width:80px;' id='locId' type='text' name='locId' value='".($locId === false ? "" : $locId)."' maxlength='6'/><br/>\n";
						echo "<br/>List Store Paid<br/>By Payout ID: <input style='width:80px;' id='fltSp' type='text' name='fltSp' value='".($fltSp === false ? "" : $fltSp)."' maxlength='6'/><br/>\n";
						echo "</div>\n";
						echo "<div style='margin:auto;'>\n";
						echo "&nbsp;Limit rows: <input style='width:80px;' id='lmt' type='text' name='lmt' value='".($limit === false ? 0 : $limit)."' maxlength='6'/><br/><br/>\n";
						echo "Filter Round: <input style='width:80px;' id='fltRnd' type='text' name='fltRnd' value='".($fltRnd === false ? "" : $fltRnd)."' maxlength='6'/><br/>\n";
						echo "Filter Worker: <input style='width:80px;' id='fltWrk' type='text' name='fltWrk' value='".($fltWrk === false ? "" : $fltWrk)."' maxlength='6'/><br/>\n";
						echo "</div>\n";
						echo "<div style='text-align:center;'><input style='margin-bottom:4px;' type='submit' name='action' value='List Update' /></div>\n";
						//---------------------------
					?>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $pidx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>" /><!-- Page -->
					<input type="hidden" name="srt" value="<?php echo $pSort; ?>" />
				</fieldset>
			</form>
		</div>
		</div><!-- End Left Column -->
		<?php
		//---------------------------
		if ($pidx !== false && $selPayout !== false)
		{
		?>
		<div class="col1 pad_left1" style="width:865px;"><!-- Start Right Column -->
		<div class="admin-edit" style="margin-right: 40px;" >
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;">Payout Details:</legend>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="idx" value="<?php echo $pidx; ?>" />
				<input type="hidden" name="srt" value="<?php echo $pSort; ?>" />
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
				<table class='admin-table'>
				<tr><th style='width:120px;'>Attribute</th><th >Value</th></tr>
				<?php
					//---------------------------
					if ($limit !== false)
						echo "<input type='hidden' name='lmt' value='$limit' />";
					if ($fltRnd !== false)
						echo "<input type='hidden' name='fltRnd' value='$fltRnd' />";
					if ($fltWrk !== false)
						echo "<input type='hidden' name='fltWrk' value='$fltWrk' />";
					if ($locId !== false)
						echo "<input type='hidden' name='locId' value='$locId' />";
					if ($fltSp !== false)
						echo "<input type='hidden' name='fltSp' value='$fltSp' />";
					//---------------------------
					if ($selPayout->workerIdx < 0) // special
					{
						$selWorkerStr = "($selPayout->workerIdx) ";
						if ($selPayout->workerIdx == WORKER_SPECIAL_MAIN)
							$selWorkerStr .= "(Special: Main/Forfeit) ";
						else if ($selPayout->workerIdx == WORKER_SPECIAL_STORE)
							$selWorkerStr .= "(Special: Store) ";
						else
							$selWorkerStr .= "[Invalid ID] ";
						if (strlen($selPayout->address) < 5)
							$selWorkerStr .= "address invalid: ".XVarDump($selPayout->address);
						else
							$selWorkerStr .= "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selPayout->address)."'>".XEncodeHTML($selPayout->address)."</a>";
					}
					else if ($selWorker === false)
						$selWorkerStr = "($selPayout->workerIdx) [not found]";
					else
					{
						$selWorkerStr = "(<a href='./Admin.php?p=5&amp;idx=$selPayout->workerIdx&amp;".SecToken()."'>$selPayout->workerIdx</a>) ";
						if ($selWorker->address != $selWorker->uname)
							$selWorkerStr .= $selWorker->uname." (".($selWorker->address == "" ? "not set" : "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selWorker->address)."'>".XEncodeHTML($selWorker->address)."</a>").")";
						else
							$selWorkerStr .= "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($selWorker->address)."'>".XEncodeHTML($selWorker->address)."</a>";
					}
					//------------------------
					$textPayout = ($selPayout->dtStored === false ? "" : "Stored at ".$Display->htmlLocalDate($selPayout->dtStored)." ");
					$textPayout .= ($selPayout->dtPaid === false ? "Not yet paid" : "Paid at ".$Display->htmlLocalDate($selPayout->dtPaid));
					if ($selPayout->storePayout !== false)
						$textPayout .= " (stored value paid by payout <a href='./Admin.php?p=$pagenum&amp;idx=$selPayout->storePayout&amp;".SecToken()."'>".$selPayout->storePayout."</a>)";
					//------------------------
					$strRound = "(<a href='./Admin.php?p=6&amp;rid=$selPayout->roundIdx&amp;".SecToken()."'>$selPayout->roundIdx</a>) $selRoundDate";
					//------------------------
					$attributes = array(	'ID' => $selPayout->id,
											'Created' => $Display->htmlLocalDate($selPayout->dtcreated),
											'Round' => $strRound,
											'Worker' => $selWorkerStr,
											'Payout' => $textPayout,
											'Payment' => "Pay $selPayout->pay (Stat Pay $selPayout->statPay) ".($selPayout->storePay != 0 ? "(".$selPayout->storePay." from stored)" : "")."&nbsp;".BRAND_UNIT.($selPayout->dtStored !== false ? ", Stored pay of ".$selPayout->statPay."&nbsp;".BRAND_UNIT : ""),
											'Payment TxID' => ($selPayout->txid != "" ? (strpos($selPayout->txid, "[") !== false ? XEncodeHTML($selPayout->txid) : "<a href='".BRAND_TX_LINK.XEncodeHTML($selPayout->txid)."'>".XEncodeHTML($selPayout->txid)."</a>") : "&nbsp;")
											);
					//---------------------------style='text-overflow: none;'
					foreach ($attributes as $name => $value)
						echo "<tr class='admin-row'><td>$name</td><td><div style='overflow: auto;'>$value</div></td></tr>\n";
					//---------------------------
					echo "</table>\n";
					//---------------------------
					if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
						echo "<br/><div>Privileged:</div><br/><input type='submit' name='action' value='Delete' /><br/>\n";
					//---------------------------
				?>
				<br/>
				<br/>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	<?php
	} // if selected
	else
	{
		//---------------------------
			//---------------------------
			?>
			<div class="col1 pad_left1" style="width:800px;"><!-- Start Right Column -->
			<div class="admin-edit" style="margin: auto;">
			<form action="./Admin.php" method="post" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<div>Privileged:</div><br/>
					<br/>
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $pidx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>" /><!-- Page -->
					<input type="hidden" name="srt" value="<?php echo $pSort; ?>" />
			<?php
			//---------------------------
			if ($limit !== false)
				echo "<input type='hidden' name='lmt' value='$limit' />";
			if ($fltRnd !== false)
				echo "<input type='hidden' name='fltRnd' value='$fltRnd' />";
			if ($fltWrk !== false)
				echo "<input type='hidden' name='fltWrk' value='$fltWrk' />";
			//---------------------------
			if ($Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
			{
				echo "<input type='submit' name='action' value='Delete All From Round' />\n";
				echo "Round index: <input id='dridx' type='text' name='dridx' value='' maxlength='5'/><br/>\n";
				echo "<br/>\n";
			}
			//---------------------------
			if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
			{
				echo "<input type='submit' name='action' value='Unpaid payout report' />\n";
				echo "<input id='rptcsv' type='checkbox' name='rptcsv' />Simple CSV<br/>\n";
			}
			//---------------------------
			if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
			{
				echo "<br/><input type='submit' name='action' value='Paid by missing payouts report' /><br/>\n";
				if ($action == "Paid by missing payouts report")
					echo "<br/><input type='submit' name='action' value='Un-pay paid by missing payouts' />\n";
			}
			//---------------------------
			if ($Login->HasPrivilege(PRIV_WALLET_MANAGE))
			{
				echo "<br/><input type='submit' name='action' value='Forfeit report' />\n";
			}
			//---------------------------
			echo "<br/>\n";
			//---------------------------
			?>
				</fieldset>
			</form>
			</div>
			<br/>
			</div><!-- End Right Column -->
			<?php
			//---------------------------
		//---------------------------
	}
	//----------------------------------
	?>
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
