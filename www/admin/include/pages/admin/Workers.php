<?php
//---------------------------------
/*
 * include/pages/admin/Workers.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Workers = new Workers() or die("Create object failed");
$Wallet = new Wallet() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$Payouts = new Payouts() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XGetPost('action');
$widx = XGetPost('idx'); // latter checked against "" to see if set
$payDetails = XGetPost('payd');
$payDetails = ($payDetails != "" ? true : false);
$payDetailsMax = XGetPost('paydMax');
$payDetailsMax = (is_numeric($payDetailsMax) ? $payDetailsMax : 5);

$payDetailsOnlyPaid = (XGetPost('paydOnlyP') != "" ? true : false);
$payDetailsOnlyUnpaid = (XGetPost('paydOnlyU') != "" ? true : false);
if ($payDetailsOnlyPaid && $payDetailsOnlyUnpaid)
	$payDetailsOnlyPaid = $payDetailsOnlyUnpaid = false;
//---------------------------------
$listSort = XGetPost('sort');
if (!is_numeric($listSort) || $listSort == 0 || abs($listSort) > 2)
	$listSort = 1;
//---------------------------------
$searchActive = (XGetPost('sAct') != "" ? true : false);
$searchType = XGetPost('sType');
if (!is_numeric($searchType))
	$searchType = 0;
$searchText = XGetPost('sText');
$searchExact = (XGetPost('sExact') != "" ? true : false);
//---------------------------------
$wLimit = XGetPost('lmt');
if (!is_numeric($wLimit))
{
	if ($widx == "")
		$wLimit = 50;
	else 
		$wLimit = false;
}
//---------------------------------
$filterDisabled = (XGetPost('fdis') != "" ? true : false);
$filterInvalid = (XGetPost('finv') != "" ? true : false);
$onlyDisabled = (XGetPost('odis') != "" ? true : false);
$onlyInvalid = (XGetPost('oinv') != "" ? true : false);
//---------------------------------
if ($filterDisabled && $onlyDisabled)
	$filterDisabled = $onlyDisabled = false;
//---------------------------------
if ($filterInvalid && $onlyInvalid)
	$filterInvalid = $onlyInvalid = false;
//---------------------------------
$wID = "";
$wUserName = "";
$wAddress = "";
$wActivity = 0;
$wdtCreated = "";
$wdtUpdated = "";
$wUpdatedBy = "";
$wDisabled = false;
$wStatus = "";
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div style="width: 600px;" ><!-- Start Left Column -->
		<div>
			<h1>Workers:</h1>
			<p>
			Folding @ Home worker list.
			</p>
			<br/>
		</div>
		<?php
			//------------------------
			if ($action == "List Payouts")
			{
				$payDetails = true;
			}
			else if ($action == "Hide Payouts")
			{
				$payDetails = false;
			}
			else if ($action == "Search")
			{
				//------------------------
				if ($searchText == "")
				{
					XLogNotify("Admin Workers $action search query empty");
					echo "$action skipped empty query.<br/>";
					$searchActive = false;
				}
				if (!is_numeric($searchType) || ($searchType != 0 && $searchType != 1))
				{
					XLogNotify("Admin Workers $action invalid search type: ".XVarDump($searchType));
					echo "$action failed. Invalid search type.<br/>";
					$searchType = 0;
					$searchActive = false;
				}
				else if ($searchType == 1 /*ID*/ && !is_numeric($searchText))
				{
					XLogNotify("Admin Workers $action validate ID search is numeric failed");
					echo "$action failed. ID search must specify a numeric search query.<br/>";
					$searchText = "";
					$searchActive = false;
				}
				else if ($searchType == 0 /*name/address*/ && strlen($searchText) > max(C_MAX_WALLET_ADDRESS_LENGTH, C_MAX_NAME_TEXT_LENGTH))
				{
					XLogNotify("Admin Workers $action name/address search query exceeds maximum length: ".strlen($searchText));
					echo "$action failed. Search query is too long.<br/>";
					$searchText = substr($searchText, 0, max(C_MAX_WALLET_ADDRESS_LENGTH, C_MAX_NAME_TEXT_LENGTH));
				}
				else
				{
					$searchActive = true;
					echo "Searching for ".($searchType == 0 ? "Name/Address" : "ID").": '".XEncodeHTML($searchText)."'".($searchType == 0 && $searchExact ? " (exact match)" : "")."<br/>\n";
				}
				//------------------------
			}
			else if ($action == "Clear Search")
			{
				//------------------------
				$searchActive = false;
				echo "Search cleared.<br/>";
				//------------------------
			}
			else if ($action == "Add")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_ADD.PRIV_WORKER_MANAGE))
				{
					//------------------------
					$wUserName 		= XPost('wName');
					$wAddress		= XPost('wAddr');
					//------------------------
					if ($wUserName == "" || strlen($wUserName) > C_MAX_NAME_TEXT_LENGTH)
					{
						XLogNotify("Admin Workers Add worker name is invalid");
						echo "Add worker failed. Worker name invalid or is too long.<br/>";
					}
					else if ($wAddress != "" && !$Wallet->isValidAddress($wAddress))
					{
						XLogNotify("Admin Workers Add worker wallet address '$wAddress' is invalid");
						echo "Add worker failed. The wallet address is not valid (it can be blank for now).<br/>";
					}
					else
					{
						//------------------------
						if (!$Workers->addWorker($wUserName, $wAddress, "(".$Login->UserID.")".$Login->UserName))
						{
							$widx = "";
							XLogWarn("Admin Workers Add Workers addWorker failed");
							echo "Add worker failed.<br/>";
						}
						else
						{
							$widx = "";
							XLogNotify("Worker admin page - adding worker (uname: $wUserName, address: $wAddress) by $Login->UserName");
							echo "Added worker successfully.</br></br>\n";
						}
						//------------------------
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Add worker doesn't have proper privileges: $Login->UserName");
					echo "Add worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Update")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					$wUserName 		= XPost('wName');
					$wAddress		= XPost('wAddr');
					$wActivity		= XPost('wAct');
					//------------------------
					if ($widx == "")
					{
						XLogNotify("Admin Workers Update no selected worker.");
						echo "Update worker failed. Select a worker.<br/>";
					}
					else if (strlen($wUserName) > C_MAX_NAME_TEXT_LENGTH)
					{
						XLogNotify("Admin Workers Update worker name is invalid. (".strlen($wUserName)." of ".C_MAX_NAME_TEXT_LENGTH."): ".var_export($wUserName));
						echo "Update worker failed. Worker name contains invalid characters or is too long.<br/>";
					}
					else if ($wAddress != "" && !$Wallet->isValidAddress($wAddress))
					{
						XLogNotify("Admin Workers Update worker wallet address '$wAddress' is invalid");
						echo "Update worker failed. The wallet address is not valid (it can be blank for now).<br/>";
					}
					else if (!is_numeric($wActivity) || $wActivity < -4 || $wActivity > 4)
					{
						XLogNotify("Admin Workers Update worker wallet Activity Score '$wActivity' is invalid.");
						echo "Update worker failed. The wallet activity score is not valid (Integer inclusively between -4 and 4 required).<br/>";
					}
					else
					{
						//------------------------
						$w = $Workers->getWorker($widx);
						//------------------------
						if ($w === false)
						{
							XLogWarn("Admin Workers Update Worker idx $widx not found failed");
							echo "Update worker failed. Worker not found.<br/>";
							$widx = "";
						}
						else
						{
							$w->uname = $wUserName;
							$w->address = $wAddress;
							$w->activity = $wActivity;
							if (!$w->Update("(".$Login->UserID.")".$Login->UserName))
							{

								XLogNotify("Worker admin page - Worker Update failed (idx: $widx, uname: $wUserName, address: $wAddress)");
								echo "Update worker failed.<br/></br>\n";
							}
							else
							{
								XLogNotify("Worker admin page - updating worker (idx: $widx, uname: $wUserName, address: $wAddress) by $Login->UserName");
								echo "Updated worker successfully.<br/></br>\n";
							}
						}
						//------------------------
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Update worker doesn't have proper privileges: $Login->UserName");
					echo "Update worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Delete")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					if (!$Workers->deleteWorker($widx))
					{
						$widx = "";
						XLogWarn("Admin Workers Delete Workers deleteWorker $widx failed");
						echo "Delete worker failed.<br/>";
					}
					else
					{
						$widx = "";
						XLogNotify("Worker admin page - deleting worker ($widx) by $Login->UserName");
						echo "Deleted worker successfully.<br/></br>\n";
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Delete worker doesn't have proper privileges: $Login->UserName");
					echo "Delete worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Enable" || $action == "Disable")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					$w = $Workers->getWorker($widx);
					//------------------------
					if ($w === false)
					{
						$widx = 0;
						XLogWarn("Admin Workers $action Worker idx $widx not found failed");
						echo "$action worker failed. Worker not found.<br/>";
					}
					else
					{
						$w->disabled = ($action == "Disable" ? true : false);
						if (!$w->Update("(".$Login->UserID.")".$Login->UserName))
						{

							XLogNotify("Worker admin page - $action update worker failed (idx: $widx)");
							echo "$action worker failed.<br/></br>\n";
						}
						else
						{
							XLogNotify("Worker admin page - $action Updated worker (idx: $widx) by $Login->UserName");
							echo "Updated worker successfully.<br/></br>\n";
						}
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers $actione worker doesn't have proper privileges: $Login->UserName");
					echo "$action worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Verbose Validate") // single worker
			{
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					$w = $Workers->getWorker($widx);
					//------------------------
					if ($w === false)
					{
						XLogWarn("Admin Workers $action Worker idx $widx not found failed");
						echo "$action worker failed. Worker not found.<br/>";
						$widx = "";
					}
					else
					{
						$username = $w->uname;
						if (!is_string($username))
							$username = "";
						$address = $w->address;
						if (!is_string($address))
							$address = "";
						if ($address == "")
							$address = $username;
						$strLen = strlen($address);
						$quickResult = $Wallet->isValidAddressQuick($address);
						$fullResult = $Wallet->isValidAddress($address);
						$result = "<div class='admin-edit'><h2>Re-validating user idx $widx, name '$address':</h2>";
						$result .= "<p>uname: ".XVarDump($w->uname)."</p>";
						$result .= "<p>address: ".XVarDump($w->address)."</p>";
						$result .= "<p>ValidKnown: ".XVarDump($w->validAddressKnown)."</p>";
						$result .= "<p>test address (len $strLen/".C_MAX_WALLET_ADDRESS_LENGTH."): '$address'</p>";
						$result .= "<p>Quick Result: ".XVarDump($quickResult)."</p>";
						$result .= "<p>Full Result: ".XVarDump($quickResult)." (valid: ".($fullResult === $address ? "Yes" : "No").")</p>";
						$result .= "</div>";
						echo $result;
						XLogNotify("Worker admin page - $action worker (idx: $widx) by $Login->UserName uname: ".XVarDump($w->uname)."(".XVarDump($w->address)."), quick result: ".XVarDump($quickResult).", full result: ".XVarDump($quickResult));
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers $actione worker doesn't have proper privileges: $Login->UserName");
					echo "$action worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Validate and Save")
			{
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					$w = $Workers->getWorker($widx);
					//------------------------
					if ($w === false)
					{
						$widx = "";
						XLogWarn("Admin Workers $action Worker idx $widx not found failed");
						echo "$action worker failed. Worker not found.<br/>";
					}
					else
					{
						if (!$w->validateAddress())
						{
							XLogError("Admin Workers $action Worker idx $widx validateAddress failed");
							echo "$action worker failed. Validation process error.<br/>";
						}
						else
						{
							$isValidAddress = ($w->validAddress && $w->address != "");
							XLogNotify("Worker admin page - $action worker (idx: $widx) by $Login->UserName uname: ".XVarDump($w->uname)."(".XVarDump($w->address)."), result isValidAddress: ".XVarDump($isValidAddress));
							echo "$action completed. Worker idx $widx uname ".XVarDump($w->uname)."(".XVarDump($w->address)."), result isValidAddress: ".XVarDump($isValidAddress)."<br/>";
						}
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers $actione worker doesn't have proper privileges: $Login->UserName");
					echo "$action worker failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}			
			else if ($action == "Reset All Address Validations")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					if (!$Workers->resetAllAddressValidations())
					{

						XLogNotify("Worker admin page - Reset All Address Validations failed");
						echo "Reset All Address Validations failed.<br/></br>\n";
					}
					else
					{
						XLogNotify("Worker admin page - Reset All Address Validations by $Login->UserName");
						echo "Reset All Address Validations successfully.<br/></br>\n";
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Reset All Address Validations doesn't have proper privileges: $Login->UserName");
					echo "Reset All Address Validations for workers failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Re-test All Invalid Addresses")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					if (!$Workers->retestInvalidAddresses("(".$Login->UserID.")".$Login->UserName))
					{

						XLogNotify("Worker admin page - Workers retestInvalidAddresses failed");
						echo "Retest Invalid Addresses failed.<br/></br>\n";
					}
					else
					{
						XLogNotify("Worker admin page - Retest invalid Addresses by $Login->UserName");
						echo "Retested Invalid Addresses successfully.<br/></br>\n";
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Retest Invalid Addresses doesn't have proper privileges: $Login->UserName");
					echo "Retest Invalid Addresses for workers failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Invalid all username address mismatches")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					if (!$Workers->invalidateAllUsernameAddressMismatches("(".$Login->UserID.")".$Login->UserName))
					{

						XLogNotify("Worker admin page - Workers invalidateAllUsernameAddressMismatches failed");
						echo "Retest Invalid Addresses failed.<br/></br>\n";
					}
					else
					{
						XLogNotify("Worker admin page - Invalidated missmateched username/addresses by $Login->UserName");
						echo "Invalidated missmateched username/addresses successfully.<br/></br>\n";
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers Invalidated missmateched username/addresses doesn't have proper privileges: $Login->UserName");
					echo "Invalidated missmateched username/addresses failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Dump all invalid addresses")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
				{
					//------------------------
					$filter = array();
					$filter['disabled'] = true;
					$filter['valid'] = false;
					//------------------------
					$invalidWorkers = $Workers->loadWorkers(DB_WORKER_USER_NAME/*sort*/, $filter);
					if ($invalidWorkers === false)
					{

						XLogNotify("Worker admin page - $action, Workers loadWorkers failed");
						echo "$action failed.<br/></br>\n";
					}
					else
					{
						//------------------------
						$result = "";
						foreach ($invalidWorkers as $w)
						{
							$address = $w->uname;
							if (is_string($address) && strlen($address) > 10)
								$result .= $address."\n";
						}
						//------------------------
						echo "<div class='admin-edit'><h2>Invalid addresses (not disabled):</h2><TEXTAREA ROWS='20' COLS='100'>$result</TEXTAREA></div>";
						//------------------------
						XLogNotify("Worker admin page - $action by $Login->UserName");
						echo "$action successfully.<br/></br>\n";
					}
					//------------------------
				} 
				else
				{
					XLogNotify("Admin Workers $action doesn't have proper privileges: $Login->UserName");
					echo "$action failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "List duplicates")
			{
				//------------------------
				$dupList =  $Workers->findDuplicates();
				//------------------------
				if ($dupList === false)
				{
					XLogNotify("Admin Workers $action Workers findDuplicates failed");
					echo "$action failed. Action failed.<br/>";
				}
				else if (!sizeof($dupList))
				{
					echo "$action found no duplicates.<br/>";
				}
				else
				{
					//------------------------
					echo "<div><h4>Duplicates</h4><br/>";					
					foreach ($dupList as $name => $idList)
					{
						echo "<p>$name - ";
						foreach ($idList as $id)
							echo "$id, ";
						echo "</p>";						
					}
					echo "</div><br/>";
					//------------------------
					
				}				
				//------------------------
			}
			//------------------------
			echo "<script>\nfunction selWorker(id, sort=$listSort){\n document.location.href='./Admin.php?p=$pagenum&idx=' + id + '&sort=' + sort + '&paydMax=$payDetailsMax&";
			echo "lmt=".($wLimit === false ? "" : $wLimit)."&";
			if ($filterDisabled) echo "fdis=1&";
			if ($filterInvalid) echo "finv=1&";
			if ($onlyDisabled) echo "odis=1&";
			if ($onlyInvalid) echo "oinv=1&";
			if ($payDetails) echo "payd=1&";
			if ($payDetailsOnlyPaid) echo "paydOnlyP=1&";
			if ($payDetailsOnlyUnpaid) echo "paydOnlyU=1&";
			if ($searchActive) echo "sAct=1&";
			if ($searchExact) echo "sExact=1&";
			echo "sType=$searchType&sText=".XEncodeHTML($searchText)."&";
			echo SecToken()."';\n}\n</script>\n";
			//------------------------
			$Columns = array(	array(	"100px",	"ID", 				1),
								array(	"450px",	"Name", 			2),
								array(	"120px",	"Valid Address",	3)
								);
			//------------------------	
			?>
			<form action="./Admin.php" method="get" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $widx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
					<input type='hidden' name='sort' value='<?php echo $listSort; ?>' />
					<input type='hidden' name='payd' value='<?php echo ($payDetails ? "1" : ""); ?>' />
					<input type='hidden' name='paydOnlyP' value='<?php echo ($payDetailsOnlyPaid ? "1" : ""); ?>' />
					<input type='hidden' name='paydOnlyU' value='<?php echo ($payDetailsOnlyUnpaid ? "1" : ""); ?>' />
					<input type='hidden' name='paydMax' value='<?php echo $payDetailsMax; ?>' />
					<input type='hidden' name='sAct' value='<?php echo ($searchActive ? "1" : ""); ?>' />
					<input type='hidden' name='lmt' value='<?php echo ($wLimit === false ? "" : $wLimit); ?>' />
					<div style="padding: 5px;background:#AAAAAA;width:445px;">
					<div style="font-weight:bold;">Search:</div>
					<div style="margin-left:5px;">
						<div>
							<label class='loginLabel' for='sType'>Search Type:</label>
							<select name='sType' id='sType'>
								<option value='0'<?php echo ($searchType == 0 ? " selected='selected'" : ""); ?>>Name/Address</option>
								<option value='1'<?php echo ($searchType == 1 ? " selected='selected'" : ""); ?>>ID</option>
							</select>
							<br/>						
							<label class="loginLabel" for="sText">Search For:</label>
							<input style="width: 270px;" id="sText" type="text" name="sText" value="<?php echo ($searchText === false ? "" : XEncodeHTML($searchText));?>" maxlength="<?php echo max(C_MAX_NAME_TEXT_LENGTH, C_MAX_WALLET_ADDRESS_LENGTH); ?>" />
							<br/>
							<input id='sExact' type='checkbox' name='sExact' value='1' <?php echo ($searchExact ?  " checked='checked'" : ""); ?>/>Exact match&nbsp;
							<input type='submit' name='action' value='Search' />
							<?php
								if ($searchActive)
									echo "<input type='submit' name='action' value='Clear Search' />";
							?>
						</div>
					</div>
					</div>
				</fieldset>
			</form>
			<br/>
			<?php			
			//------------------------	
			if (abs($listSort) == 1)
				$sort = DB_WORKER_ID;
			else if (abs($listSort) == 2)
				$sort = DB_WORKER_USER_NAME;
			else
				$sort = false;
			if ($sort !== false && $listSort < 0)
				$sort .= " DESC";
			//------------------------	
			if ($filterDisabled || $filterInvalid || $onlyDisabled || $onlyInvalid || $searchActive)
			{
				$filter = array();
				if ($filterDisabled) $filter['disabled'] = false;
				if ($onlyDisabled)  $filter['disabled'] = true;
				if ($filterInvalid) $filter['valid'] = true;
				if ($onlyInvalid) $filter['valid'] = false;
				if ($searchActive)
				{
					 $filter['search'] = true;
					 $filter['search_type'] = $searchType;
					 $filter['search_text'] = $searchText;
					 $filter['search_exact'] = $searchExact;
				}
			}
			else $filter = false;
			//------------------------	
			if ($wLimit === false && $widx != "")
			{ // need selected one
				$singleWorker = $Workers->loadWorker($widx);
				if ($singleWorker === false)
				{
					XLogError("Workers admin page - Workers loadWorker failed: ".XVarDump($widx));
					$wlist = array();
					$widx = "";
				}
				else $wlist = array($singleWorker);
			}
			else
			{
				$wlist = $Workers->getWorkers($sort, $filter, $wLimit);
				if ($wlist === false)
					XLogError("Workers admin page - Workers getWorkers failed");
				else if (sizeof($wlist) == 1)
					$widx = $wlist[0]->id;
			}
			//------------------------	
			echo "<div>(".($wlist === false ? "0" : sizeof($wlist))." rows displayed. Click row to select for editing. Click a header to sort.)</div>";
			echo "<table class='admin-table' style='width:670px;'>\n<thead class='admin-scrolltable-header'>\n<tr class='admin-row'>\n";
			//------------------------	
			foreach ($Columns as $col)
			{
				$style = "";
				if ($col[0] !== false)
					$style .= "width:$col[0];";
				if (abs($listSort) == $col[2])
					$style .= "background:#EEEEEE;";
				echo "\t<th".($style != "" ? " style='$style'" : "");
				echo " onclick=\"selWorker(".($widx == "" ? "''" : $widx).", ".(abs($listSort) == $col[2] ? (-1 * $listSort) : $col[2]).");\">$col[1]</th>\n";
			}
			//------------------------	
			echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
			//------------------------	
			if ($wlist === false || sizeof($wlist) == 0)
			{
				//------------------------
				echo "<tr class='admin-row'>";
				foreach ($Columns as $col)
					echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
				echo "</tr>\n";
				//------------------------
			}
			else
			{
				//------------------------
				$rowIdx = 0;
				//------------------------
				foreach ($wlist as $w)
				{
					//------------------------
					if ( ($action == "Enable All" || $action == "Disable All") && $Login->HasPrivilege(PRIV_WORKER_MANAGE))
					{
						//------------------------
						$w->disabled = ($action == "Disable All" ? true : false);
						if (!$w->Update("(".$Login->UserID.")".$Login->UserName))
						{

							XLogNotify("Worker admin page - $action update worker failed (idx: $w->id)");
							echo "<br/></br>$action worker failed.<br/></br>\n";
						}
						//------------------------
					}
					//------------------------
					if ($widx != "" && $w->id == $widx)
					{
						$rowClass = 'admin-row-sel';
						$wID = $w->id;
						$wUserName = $w->uname;
						$wAddress = $w->address;
						$wActivity = $w->activity;
						$wdtCreated = $Display->htmlLocalDate($w->dtCreated);
						$wdtUpdated = $Display->htmlLocalDate($w->dtUpdated);
						$wUpdatedBy = $w->updatedBy;
						$wDisabled = $w->disabled;
						if ($w->disabled)
							$wStatus = "Disabled";
						else
							$wStatus = "Ready";
						if ($w->validAddress)
							$wStatus .= ", address validated";
					}
					else 
						$rowClass = 'admin-row';
					//------------------------
					$txtValid = "";
					if ($w->disabled)
						$txtValid = "(Disabled) ";
					if ($w->validAddress)
						$txtValid .= "Valid";
					else if ($w->validAddressKnown)
						$txtValid .= "Invalid";
					else 
						$txtValid .= "Unknown";
					//------------------------
					$colData = array(	$w->id,
										XEncodeHTML(substr($w->uname, 0, C_MAX_WALLET_ADDRESS_LENGTH)),
										$txtValid
									);
					//------------------------
					echo "<tr class='$rowClass' onclick=\"selWorker($w->id);\">";
					//------------------------
					for ($i = 0;$i < sizeof($colData);$i++)
					{
						$style = "";
						if ($rowIdx == 0 && $Columns[$i][0] !== false)
							$style .= "width:".$Columns[$i][0].";";
						if ($style != "")
							$style = " style='$style'";
						echo "<td$style>".$colData[$i]."</td>";
					}
					//------------------------
					echo "</tr>\n";
					//------------------------
					$rowIdx++;
					//------------------------
				}
				//------------------------
			}
			//------------------------
			echo "</tbody></table>\n";		
			//------------------------	
		?>
			<form action="./Admin.php" method="get" enctype="multipart/form-data">
				<fieldset class="loginBox">
					<?php PrintSecTokenInput(); ?>
					<input type="hidden" name="idx" value="<?php echo $widx; ?>" />
					<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
					<input type='hidden' name='sort' value='<?php echo $listSort; ?>' />
					<input type='hidden' name='payd' value='<?php echo ($payDetails ? "1" : ""); ?>' />
					<input type='hidden' name='paydOnlyP' value='<?php echo ($payDetailsOnlyPaid ? "1" : ""); ?>' />
					<input type='hidden' name='paydOnlyU' value='<?php echo ($payDetailsOnlyUnpaid ? "1" : ""); ?>' />
					<input type='hidden' name='paydMax' value='<?php echo $payDetailsMax; ?>' />
					<input type='hidden' name='sAct' value='<?php echo ($searchActive ? "1" : ""); ?>' />
					<input type='hidden' name='sType' value='<?php echo $searchType; ?>' />
					<input type='hidden' name='sText' value='<?php echo XEncodeHTML($searchText); ?>' />
					<input type='hidden' name='sExact' value='<?php echo ($searchExact ? "1" : ""); ?>' />

					<div style="padding: 5px;background:#AAAAAA;width:445px;">
					<div style="font-weight:bold;">List filters:</div>
					<div style="margin-left:5px;">
						<div>
							<label class="loginLabel" for="lmt">Limit rows:</label>
							<input style="width:90px;" id="lmt" type="text" name="lmt" value="<?php echo ($wLimit === false ? "" : $wLimit);?>" maxlength="6" />
						</div>
						<div style="float:left;margin-right: 10px;">
							<input id='fdis' type='checkbox' name='fdis' value='1' <?php echo ($filterDisabled ?  " checked='checked'" : ""); ?>/>Filter Disabled<br/>
							<input id='finv' type='checkbox' name='finv' value='1' <?php echo ($filterInvalid ?  " checked='checked'" : ""); ?>/>Filter Invalid Address
						</div>
						<div style="margin:auto;">
							<input id='odis' type='checkbox' name='odis' value='1' <?php echo ($onlyDisabled ?  " checked='checked'" : ""); ?>/>Only Disabled<br/>
							<input id='oinv' type='checkbox' name='oinv' value='1' <?php echo ($onlyInvalid ?  " checked='checked'" : ""); ?>/>Only Invalid Address
						</div>
					</div>
					<input type='submit' name='noaction' value='Update' />
					</div>
				</fieldset>
			</form>
		</div><!-- End Left Column -->
		<br/>
		<div class="pad_left1" <?php echo "style='width: ".($widx != "" && $payDetails === true ? "950px;" : "600px"); ?>;' ><!-- Start Right Column -->
		<div class="admin-edit" style="width:90%;">
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<legend style="font-weight:bold;font-size:120%;"><?php echo ($widx != "" ? "Worker Details:" : "Add Worker:") ?></legend>
				<br/>
				<?php PrintSecTokenInput(); ?>
				<input type="hidden" name="idx" value="<?php echo $widx; ?>" />
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/>
				<input type='hidden' name='sAct' value='<?php echo ($searchActive ? "1" : ""); ?>' />
				<input type='hidden' name='sType' value='<?php echo $searchType; ?>' />
				<input type='hidden' name='sText' value='<?php echo XEncodeHTML($searchText); ?>' />
				<input type='hidden' name='sExact' value='<?php echo ($searchExact ? "1" : ""); ?>' />
				<input type='hidden' name='lmt' value='<?php echo ($wLimit === false ? "" : $wLimit); ?>' />
				<input type='hidden' name='sort' value='<?php echo $listSort; ?>' />
				<?php 
					//---------------------------
					if ($payDetails) echo "<input type='hidden' name='payd' value='1' />\n";
					if ($filterDisabled) echo "<input type='hidden' name='fdis' value='1' />\n";
					if ($filterInvalid) echo "<input type='hidden' name='finv' value='1' />\n";
					if ($onlyDisabled) echo "<input type='hidden' name='odis' value='1' />\n";
					if ($onlyInvalid) echo "<input type='hidden' name='oinv' value='1' />\n";
					//---------------------------
					if ($widx != "")
					{
						//---------------------------
						$attributes = array(	'ID' => $wID,
												'User Name' => XEncodeHTML($wUserName),
												'Created' => $wdtCreated,
												'Updated' => $wdtUpdated.($wUpdatedBy != "" ? " by ".XEncodeHTML($wUpdatedBy) : ""),
												'Payout Address' => ($wAddress == "" || $wAddress == "x" ? $wAddress : "<a href='".BRAND_ADDRESS_LINK.XEncodeHTML($wAddress)."'>".XEncodeHTML($wAddress)."</a>"),
												'Status'  => $wStatus,
												'Activity Score' => $wActivity
											);
						//---------------------------
						echo "<table class='admin-table'>\n<tr><th style='width:120px;'>Attribute</th><th >Value</th></tr>\n";
						//---------------------------
						foreach ($attributes as $name => $value)
							echo "<tr class='admin-row'><td>$name</td><td><div style='overflow: auto;'>$value</div></td></tr>\n";
						//---------------------------
						echo "</table>\n";
						//---------------------------
					} // if ($widx != "")
					//---------------------------
					if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
					{
					?>
					<br/>
					<legend style="font-weight:bold;font-size:120%;">Add/Modify:</legend>
					<br/>
					<div>
						<label class="loginLabel" for="wName">User Name:</label>
						<input style="width:270px;" id="wName" type="text" name="wName" value="<?php echo XEncodeHTML($wUserName);?>" maxlength="<?php echo C_MAX_NAME_TEXT_LENGTH;?>" />
					</div>
					<br/>
					<div>
						<label class="loginLabel" for="wAddr">Payout Address:</label>
						<input style="width:270px;" id="wAddr" type="text" name="wAddr" value="<?php echo XEncodeHTML($wAddress);?>" maxlength="<? echo C_MAX_WALLET_ADDRESS_LENGTH;?>" />
					</div>
					<br/>
				<?php 
					} // if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
					//---------------------------
					if ($widx != "" && $Login->HasPrivilege(PRIV_WORKER_MANAGE))
						echo "<input type='submit' name='action' value='".($wDisabled ? "Enable" : "Disable")."' />&nbsp;\n<br/>\n<br/>\n";
					if ($Login->HasPrivilege(PRIV_WORKER_ADD.PRIV_WORKER_MANAGE))
						echo "<input type='submit' name='action' value='Add' />&nbsp;\n";
					if ($widx != "" && $Login->HasPrivilege(PRIV_WORKER_MANAGE))
					{
						echo "<input type='submit' name='action' value='Update' />&nbsp;";
						echo "<input type='submit' name='action' value='Delete' />\n";
						echo "<br/><br/><input type='submit' name='action' value='Verbose Validate' />&nbsp;(no-save, single worker)";
						echo "<br/><br/><input type='submit' name='action' value='Validate and Save' />&nbsp;(single worker)<br/>";
					}
					//---------------------------
					echo "<hr style='margin-top:8px;height:8px;border-top: 1px solid black;text-align: center;'>\n<br/>\n";
					//---------------------------
					if ($widx != "" && $payDetails === true)
					{
						//---------------------------
						echo "<div>\n";
						//---------------------------
						$workerPayouts = $Payouts->getWorkerPayouts($widx, $payDetailsOnlyPaid /*onlyPaid*/, $payDetailsOnlyUnpaid /*onlyUnpaid*/, DB_PAYOUT_ROUND." DESC" /*sort*/, ($payDetailsMax <= 0 ? false : $payDetailsMax) /*limit*/);
						if ($workerPayouts === false)
						{
							XLogNotify("Worker admin page - Payouts failed for workerIdx ".XVarDump($widx));
							echo "<br/></br>Error - Get worker payout details failed.<br/></br>\n";
						}
						else
						{
							//---------------------------
							$Columns = array(	array(	"80px",		"ID", 				0),
												array(	"100px",	"Round", 			1),
												array(	"230px",	"Pay", 				2),
												array(	"400px",	"Status",			3)
												);
						
							//---------------------------
							echo "<table class='admin-table'>\n<thead class='admin-scrolltable-header'>\n<tr>\n";
							//------------------------	
							foreach ($Columns as $col)
							{
								$style = "";
								if ($col[0] !== false)
									$style .= "width:$col[0];max-width:$col[0];";
								echo "\t<th".($style != "" ? " style='$style'" : "").">$col[1]</th>\n";
							}
							//------------------------	
							echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
							//---------------------------
							if (sizeof($workerPayouts) == 0)
							{
								//---------------------------
								echo "<tr class='admin-row'>";
								foreach ($Columns as $col)
									echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
								echo "</tr>\n";
								//---------------------------
							}
							else
							{
								//---------------------------
								foreach ($workerPayouts as $payout)
								{
									//---------------------------
									$txtPay = $payout->statPay." ";
									//---------------------------
									if ($payout->storePay != 0)
										$txtPay .= "(+  $payout->storePay) ";
									//---------------------------
									$txtPay .= BRAND_UNIT;
									if ($payout->dtStored !== false)
										$txtPay .= " (stored) ";
									//---------------------------
									$txtStatus = "";
									if ($payout->dtStored !== false)
										$txtStatus .= "Stored ".$Display->htmlLocalDate($payout->dtStored)."<br/>";
									if ($payout->dtPaid !== false)
										$txtStatus .= "Paid ".$Display->htmlLocalDate($payout->dtPaid);
									else
										$txtStatus .= "Not Paid ";
									if ($payout->storePayout !== false)
										$txtStatus .= ", by payout (<a href='./Admin.php?p=7&amp;idx=$payout->storePayout&amp;".SecToken()."'>$payout->storePayout</a>)";
									if ($payout->txid !== false && $payout->txid != "")
										$txtStatus .= "<br/>Txid ".(strpos($payout->txid, "[") !== false ? XEncodeHTML($payout->txid) : "<a href='".BRAND_TX_LINK.XEncodeHTML($payout->txid)."'>".XEncodeHTML(substr($payout->txid, 0, 15))."...</a>");
									//---------------------------
									$colData = array( "<a href='./Admin.php?p=7&amp;idx=$payout->id&amp;".SecToken()."'>$payout->id</a>",
													"<a href='./Admin.php?p=6&amp;rid=$payout->roundIdx&amp;".SecToken()."'>($payout->roundIdx)</a> $payout->dtcreated",
													$txtPay,
													$txtStatus									
													);
									//---------------------------
									echo "<tr class='admin-row'>";
									//---------------------------
									for ($i = 0;$i < sizeof($colData);$i++)
									{
										$style = "";
										if ($Columns[$i][0] !== false)
											$style .= "width:".$Columns[$i][0].";max-width: ".$Columns[$i][0].";";
										if ($style != "")
											$style = " style='$style'";
										echo "<td$style>".$colData[$i]."</td>";
									}
									//------------------------
									echo "</tr>\n";
									//------------------------
								} // foreach ($workerPayouts as $payout)
								//---------------------------
							}  // else // if (sizeof($workerPayouts) == 0)
							//---------------------------
							echo "</tbody></table>\n";		
							//---------------------------
						} // else // if ($workerPayouts === false)
						//---------------------------
						echo "</div>";
						echo "<label class='loginLabel' for='paydMax'>Maximum payout history:</label>";
						echo "<input style='width:70px;' id='paydMax' type='text' name='paydMax' value='$payDetailsMax' maxlength=5 /> ";
						echo "<input id='paydOnlyP' type='checkbox' name='paydOnlyP' value='1'".($payDetailsOnlyPaid ? " checked='checked'" : "")." />Only Paid ";
						echo "<input id='paydOnlyU' type='checkbox' name='paydOnlyU' value='1'".($payDetailsOnlyUnpaid ? " checked='checked'" : "")." />Only Unpaid&nbsp;";
						echo "<input type='submit' name='action' value='Update Payout Filter' /><br/><br/>\n";
						echo "<input type='submit' name='action' value='Hide Payouts' /><br/><br/>\n";
						//---------------------------
					} // if ($widx != "" && $payDetails === true)
					//---------------------------
					if ($widx != "" && $payDetails !== true)
						echo "<input type='submit' name='action' value='List Payouts' /><br/><br/>\n";
					//---------------------------
					if ($Login->HasPrivilege(PRIV_WORKER_MANAGE))
					{
						if ($widx != "")
							echo "<hr style='margin-top:8px;height:8px;border-top: 1px solid black;text-align: center;'>\n";
						echo "<hr/><br/><br/><input type='submit' name='action' value='Reset All Address Validations' />&nbsp;\n<br/>\n<br/>\n";
						echo "<br/><input type='submit' name='action' value='Re-test All Invalid Addresses' /> (not disabled, includes incorrectly valid blank addresses)<br/>";
						echo "<br/>\n<div>(All listed)<br/>\n";
						echo "<input type='submit' name='action' value='Enable All' />&nbsp;";
						echo "<input type='submit' name='action' value='Disable All' /><br/>\n";
						echo "<br/>\n";
						echo "<input type='submit' name='action' value='Dump all invalid addresses' /><br/>\n";
						echo "<br/>\n";
						echo "<input type='submit' name='action' value='Invalid all username address mismatches' /><br/>\n";
						echo "<br/>\n";
						echo "<input type='submit' name='action' value='List duplicates' /><br/>\n";
						echo "</div>\n";
					}
					//---------------------------
				?>
				<br/>
			</fieldset>
		</form>
		</div>
		<br/>
		</div><!-- End Right Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
