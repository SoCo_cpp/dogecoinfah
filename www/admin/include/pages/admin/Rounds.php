<?php
//---------------------------------
/*
 * include/pages/admin/Rounds.php
 * 
 * 
*/
//---------------------------------
define('ROUND_ADMIN_PAGE_AUTO_SELECT', true);
//---------------------------------
global $Login;
$Rounds = new Rounds() or die("Create object failed");
$Config = new Config() or die("Create object failed");
$Contributions = new Contributions() or die("Create object failed");
$Payouts = new Payouts() or die("Create object failed");
$Stats = new Stats() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$Workers = new Workers() or die("Create object failed");
$Monitor = new Monitor() or die("Create object failed"); 
$MarketHistory = new MarketHistory() or die("Create object failed");
//---------------------------------
$pagenum = XGetPost('p');
$action = XPost('action');
$showDetails = (XGetPost('det', false) !== false ? true : false);
$showZeroDetails = (XGetPost('zdet', false) !== false ? true : false);
//---------------------------------
$rAppr = "";
$rActive = false;
$rFunded = "";
$rStarted = "";
$rStatus = "";
$rStats = "";
$rPayment = "";
$rTeamId = "";
$rTotWork = "";
$rTotPay = "";
$rStorePay = "";
$rPayStored = "";
$countSummary = false;
//------------------------
$rid = XGetPost('rid');
if (!is_numeric($rid))
	$rid = false;
//------------------------
$meid = XGetPost('meid');
if (!is_numeric($meid))
	$meid = false;
//------------------------
$startAuto = XPost('stauto', false);
//------------------------
$rListSort = XGetPost('rsort', -1);
$wListSort = XGetPost('wsort', 2);
$meListSort = XGetPost('mesort', -1);
//------------------------
$rLimit = XGetPost('lmt');
$defFilters = ($rLimit == "" ? true : false);
if ($rLimit == "" || !is_numeric($rLimit))
	$rLimit = 2;
else if ($rLimit == 0)
	$rLimit = false;
//------------------------
if ($defFilters && $rid !== false)
	$rLimit = "";
//------------------------
echo "<script src='js/dfahTable.js?t=".rand()."'></script>\n";
//------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1 admin-edit" style='max-width:810px;'><!-- Start Container 2 -->
	<br/>
		<div>
			<h1>Reward Rounds:</h1>
		</div>
		<?php
			//------------------------
			if (!defined('BRAND_MARKET_ASSET_ID'))
				echo "<p>Warning: Settings.php field BRAND_MARKET_ASSET_ID not detected.</p>\n";
			//------------------------
			if ($action == "Add")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_ROUND_ADD)) 
				{
					//------------------------
					$teamId = XGetPost('teamid');
					$flagStoreValues = (XGetPost('nrfsv', false) !== false ? true : false);
					$flagForfeitStore = (XGetPost('nrffs', false) !== false ? true : false);
					$flagHidden = (XGetPost('nrfht', false) !== false ? true : false);
					$flagDryRun = (XGetPost('nrfdr', false) !== false ? true : false);
					$flagActive = (XGetPost('nrfact', false) !== false ? true : false);
					//---
					$flags = ROUND_FLAG_NONE;
					if ($flagStoreValues)
						$flags |= ROUND_FLAG_STORE_VALUES;
					if ($flagForfeitStore)
						$flags |= ROUND_FLAG_FORFEIT_STORE;
					if ($flagHidden)
						$flags |= ROUND_FLAG_HIDDEN;
					if ($flagDryRun)
						$flags |= ROUND_FLAG_DRYRUN;
					if ($flagActive)
						$flags |= ROUND_FLAG_ACTIVE;
					//------------------------
					if (!is_numeric($teamId))
					{
						XLogWarn("Admin Rounds Add Round validate parameters failed. Team ID: ".XVarDump($teamId));
						echo "Add round failed. Invalid params.<br/>";
					}
					else if (!$Rounds->addRound($teamId, $flags, $startAuto))
					{
						XLogWarn("Admin Rounds Add Round addRound failed");
						echo "Add round failed.<br/>";
					}
					else
					{
						$rid = false;
						XLogNotify("Round admin page - adding round");
						echo "Added round successfully.<br/><br/>\n";
					}
					//------------------------
				}
				else
					echo "Manually adding rounds require round add privileges.<br/><br/>\n";
				//------------------------
			}
			else if ($rid !== false && $action == "Get Market History")
			{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else
					{
						$hist = $MarketHistory->getHistory($r->dtStarted, true /*pollIfNeeded*/, false /*$provider*/, false /*$asset*/, 0 /*notFoundValue*/, 0 /*clientPollFailValue*/);
						echo "History reply: ".XVarDump($hist)."<br/>\n";
					}
					//------------------------
				
			}
			else if ($rid !== false && ($action == "Approve" || $action == "Unapprove"))
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_ROUND_APPROVE))
				{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else if (!$r->isActive())
					{
						XLogNotify("Round admin page - $action round id $rid is not active");
						echo "Round is no longer active.<br/><br/>\n";
					}
					else
					{
						if ($action == "Approve")
							$ret = $r->setApproved(true /*pushRoundAutomation*/);
						else
							$ret = $r->clearApproved();
						if (!$ret)
						{
							XLogNotify("Round admin page - $action Round setApproved/clearApproved failed.");
							echo "Round $action failed.<br/><br/>\n";
						}
						else
						{
							XLogNotify("Round admin page - round $rid set to $action by $Login->UserName");
							echo "Round successfully $action.<br/><br/>\n";
						}
					}
					//------------------------
				}
				else
					echo "Approving/Unapproving rounds require approval privileges.<br/><br/>\n";
				//------------------------
			}
			else if ($action == "Save As Default") // New Round Flags
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_ROUND_ADD) && $Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
				{
					//------------------------
					$teamId = XGetPost('teamid');
					$flagStoreValues = (XGetPost('nrfsv', false) !== false ? true : false);
					$flagForfeitStore = (XGetPost('nrffs', false) !== false ? true : false);
					$flagHidden = (XGetPost('nrfht', false) !== false ? true : false);
					$flagDryRun = (XGetPost('nrfdr', false) !== false ? true : false);
					$flagActive = (XGetPost('nrfact', false) !== false ? true : false);
					//---
					$flags = ROUND_FLAG_NONE;
					if ($flagStoreValues)
						$flags |= ROUND_FLAG_STORE_VALUES;
					if ($flagForfeitStore)
						$flags |= ROUND_FLAG_FORFEIT_STORE;
					if ($flagHidden)
						$flags |= ROUND_FLAG_HIDDEN;
					if ($flagDryRun)
						$flags |= ROUND_FLAG_DRYRUN;
					if ($flagActive)
						$flags |= ROUND_FLAG_ACTIVE;
					//------------------------
					if (!is_numeric($teamId))
					{
						XLogWarn("Admin Rounds  saved default round options validate parameters failed. Team ID: ".XVarDump($teamId));
						echo "Changing default round options failed. Invalid params.<br/>";
					}
					else if (!$Config->Set(CFG_ROUND_TEAM_ID, $teamId) || !$Config->Set(CFG_ROUND_FLAGS, $flags))
					{
						XLogWarn("Admin Rounds  saved default round options set config failed");
						echo "Changing default round options failed.<br/>";
					}
					else
					{
						XLogNotify("Round admin page - saved default round options");
						echo "Saved round default options successfully.<br/><br/>\n";
					}
					//------------------------
				}
				else
					echo "Changing default round options requires round add and delete history privileges.<br/><br/>\n";
				//------------------------
			}
			else if ($rid !== false && ($action == "Unmark Hidden" || $action == "Mark Hidden" || $action == "Unmark Dry Run" || $action == "Mark Dry Run" || 
							$action == "Unmark Active" || $action == "Mark Active" ||  $action == "Unmark Done" || $action == "Mark Done" ||  
							$action == "Disable Stored Rewards" || $action == "Enable Stored Rewards" ||  
							$action == "Disable Store Forfeit" || $action == "Enable Store Forfeit"))
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else
					{
						//------------------------
						if (strncmp($action, "Mark ", 5) == 0)
						{
							$modeAdd = true;
							$txtMode = substr($action, 5);
						}
						else if (strncmp($action, "Enable ", 7) == 0)
						{
							$modeAdd = true;
							$txtMode = substr($action, 7);
						}
						if (strncmp($action, "Unmark ", 7) == 0)
						{
							$modeAdd = false;
							$txtMode = substr($action, 7);
						}
						else if (strncmp($action, "Disable ", 8) == 0)
						{
							$modeAdd = false;
							$txtMode = substr($action, 8);
						}
						//------------------------
						if ($txtMode == "Hidden")
							$valFlag = ROUND_FLAG_HIDDEN;
						else if ($txtMode == "Dry Run")
							$valFlag = ROUND_FLAG_DRYRUN;
						else if ($txtMode == "Active")
							$valFlag = ROUND_FLAG_ACTIVE;
						else if ($txtMode == "Done")
							$valFlag = ROUND_FLAG_DONE;
						else if ($txtMode == "Stored Rewards")
							$valFlag = ROUND_FLAG_STORE_VALUES;
						else if ($txtMode == "Store Forfeit")
							$valFlag = ROUND_FLAG_FORFEIT_STORE;
						else 
							$valFlag = false;
						//------------------------
						if ($valFlag === false)
						{
							XLogNotify("Round admin page - Invalid mode action: $action, flag not found in txtMode: ".XVarDump($txtMode));
							echo "Round update failed.<br/><br/>\n";							
						}
						else 
						{
							//------------------------
							if ($modeAdd === true)
								$ret = $r->setFlag($valFlag);
							else
								$ret = $r->clearFlag($valFlag);
							//------------------------
							if (!$ret)
							{
								XLogNotify("Round admin page - $action update round failed");
								echo "Round update failed.<br/><br/>\n";
							}
							else
							{
								XLogNotify("Round admin page - round $rid set to '$action' by $Login->UserName");
								echo "Round successfully $action.<br/><br/>\n";
							}
							//------------------------
						}
						//------------------------
					}
					//------------------------
				}
				else
					echo "Marking/Unmarking rounds as hidden require delete privileges.<br/><br/>\n";
				//------------------------
			}
			else if ($rid !== false && $action == "Update Comment")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$comment = XPost('comment', "");
					$comment = ($comment == "" ? false : $comment);
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else
					{
						if (!$r->setComment($comment))
						{
							XLogNotify("Round admin page - $action update round failed");
							echo "Round update failed.<br/><br/>\n";
						}
						else
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName to ".XVarDump($comment));
							echo "Round successfully $action.<br/><br/>\n";
						}
					}
					//------------------------
				}
				else
					echo "Setting round comments require delete privileges.<br/><br/>\n";
				//------------------------
			}
			else if ($rid !== false && $action == "Delete")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					XLogNotify("User $Login->User is deleting round: $rid");
					if (!$Rounds->deleteRound($rid))
					{
						XLogNotify("Round admin page - rounds failed to deleteRound $rid");
						echo "Failed to delete round.<br/><br/>\n";
					}
					else
					{
						echo "Round successfully deleted.<br/><br/>\n";
						$rid = false;
					}
				}
				else
					echo "Deleting historical data required delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Delete Round All Data")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					$includeStats = (XGetPost('dds', false) !== false ? true : false);
					XLogNotify("User $Login->User is deleting all data for round: $rid");
					if (!$Payouts->deleteAllRound($rid))
					{
						XLogNotify("Round admin page - Payouts failed to deleteAllRound $rid");
						echo "Failed to delete round data.<br/><br/>\n";
					}
					else if (!$Contributions->deleteAllRound($rid))
					{
						XLogNotify("Round admin page - Contributions failed to deleteAllRound $rid");
						echo "Failed to delete round data.<br/><br/>\n";
					}
					else if (!$Monitor->PurgeResetRound($rid))
					{
						XLogNotify("Round admin page - Monitor failed to PurgeResetRound $rid");
						echo "Failed to delete round data.<br/><br/>\n";
					}
					else if ($includeStats && !$Stats->deleteAllRound($rid))
					{
						XLogNotify("Round admin page - Stats failed to deleteAllRound $rid");
						echo "Failed to delete round data.<br/><br/>\n";
					}
					else
						echo "Round successfully deleted all data for round: $rid.<br/><br/>\n";
				}
				else
					echo "Deleting historical data required delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "UnAdd Round Data Stats")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					$NewRoundData = new NewRoundData() or die ("Create object failed");
					$retVal = $NewRoundData->unaddStats();
					if ($retVal === false )
					{
						XLogNotify("Round admin page - $action failed by $Login->UserName");
						echo "Failed to $action round data.<br/><br/>\n";
					}
					else
					{
						XLogNotify("Round admin page - $action preformed to $retVal values by $Login->UserName");
						echo "Round successfully $action to $retVal values.<br/><br/>\n";
					}
				}
				else
					echo "$action requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Unpay Round Payouts")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					$includeSpecial = (XGetPost('upsp', false) !== false ? true : false);
					XLogNotify("User $Login->User is $action for round: $rid");
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Failed to $action. Round not found.<br/><br/>\n";
					}
					else if (!$r->unpayPayouts($includeSpecial))
					{
						XLogNotify("Round admin page - $action round unpayPayouts failed, round id $rid");
						echo "Failed to $action.<br/><br/>\n";
					}
					else
						echo "Round successfully $action for round: $rid.<br/><br/>\n";
				}
				else
					echo "Deleting historical data required delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Set Step")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$step = XPost('step', false);
					if (!is_numeric($step))
						$step = false;
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else if ($step === false)
					{
						XLogNotify("Round admin page - $action specified step value is invalid");
						echo "Invalid step value.<br/><br/>\n";
					}
					else
					{
						//------------------------
						$r->step = $step;
						//------------------------
						if (XPost('clrprogress', false) !== false && !$r->clearProgress())
						{
							XLogNotify("Round admin page - $action round clearProgress failed");
							echo "Round force state failed.<br/><br/>\n";
						}
						else if (!$r->Update())
						{
							XLogNotify("Round admin page - $action round Update failed");
							echo "Round force state failed.<br/><br/>\n";
						}
						else
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName to ".XVarDump($step));
							echo "Round successfully $action.<br/><br/>\n";
						}
						//------------------------
					}
					//------------------------
				}
				else
					echo "Manually setting round step requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Check for Worker Duplicates")
			{
				//------------------------
				$statDupes = $Stats->findRoundWorkersDuplicates($rid);
				if ($statDupes === false)
				{
					XLogError("Round admin page - Stats findRoundWorkersDuplicates failed");
					echo "Error getting Stats worker duplicates.<br/><br/>\n";
				}
				else if (sizeof($statDupes) != 0)
				{
					//------------------
					echo "<br/><div>Round $rid, Worker duplicate Stats found (".sizeof($statDupes)."):<br/>\n";
					echo "Worker - Stat<br/><hr>\n";
					foreach ($statDupes as $widx => $sidx)
						echo "&nbsp;$widx - $sidx<br/>\n";
					//------------------
					echo "</div><br/>\n";
				}
				else echo "<br/><div>Round $rid, no Worker duplicate Stats detected.</div><br/>\n";
				//------------------
				$payoutDupes = $Payouts->findRoundWorkersDuplicates($rid);
				if ($payoutDupes === false)
				{
					XLogError("Round admin page - Payouts findRoundWorkersDuplicates failed");
					echo "Error getting Payout worker duplicates.<br/><br/>\n";
				}
				else if (sizeof($payoutDupes) != 0)
				{
					//------------------
					echo "<div>Round $rid, Worker duplicate Payouts found (".sizeof($payoutDupes)."):<br/>\n";
					echo "Worker - Payout<br/><hr>\n";
					foreach ($payoutDupes as $widx => $pidx)
						echo "&nbsp;$widx - $pidx<br/>\n";
					//------------------
					echo "</div><br/>\n";
				}
				else echo "<div>Round $rid, no Worker duplicate Payouts detected.</div><br/>\n";
				//------------------
			}			
			else if ($rid !== false && $action == "Reparse FahClient Data From File")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$data = "";
					$FahClient = new FahClient() or die("Create object failed");
					if (!$FahClient->reparseDataFromFile($rid, "./reparse_fahclient_data.json"))
					{
						XLogNotify("Round admin page - $action FahClient reparseDataFromFile failed");
						echo "Round $action failed.<br/><br/>\n";
					}
					else
					{
						XLogNotify("Round admin page - round $rid $action by $Login->UserName to ".XVarDump($state));
						echo "Round successfully $action.<br/><br/>\n";
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Reparse FahAppClient Data From File")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action FahClient getRound $rid failed");
						echo "Round $action failed.<br/><br/>\n";
					}
					else
					{
						$order = XPost('rporder', false);
						$grep = XPost('rpgrep', false);
						$FahAppClient = new FahAppClient() or die("Create object failed");
						$reparseMode = ($grep ? 2 : 1);
						if ($order !== "newest")
							$reparseMode *= -1;
						if (!$r->requestFahAppStats($reparseMode))
						{
							XLogNotify("Round admin page - $action round requestFahAppStats (reparse) failed");
							echo "Round $action failed.<br/><br/>\n";
						}
						else
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName");
							echo "Round successfully $action.<br/><strong>Auto marked inactive for safety. Please review state/action and re-mark active.</strong><br/>\n";
							$r->clearFlag(ROUND_FLAG_ACTIVE);
						}
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Reparse FahClient CheckStats")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$FahClient = new FahClient() or die("Create object failed");
					if (!$FahClient->checkStats($rid, false /*countLimit*/, true /*reparse*/))
					{
						XLogNotify("Round admin page - $action FahClient requestPayouts(reparse) failed");
						echo "Round $action failed.<br/><br/>\n";
					}
					else
					{
						XLogNotify("Round admin page - round $rid $action by $Login->UserName to ".XVarDump($state));
						echo "Round successfully $action.<br/><br/>\n";
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Set Test Balance")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else
					{
						$testBal = XGetPost('sbal');
						if (!is_numeric($testBal))
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName, test balance not valid: ".XVarDump($testBal));
							echo "Round not found.<br/><br/>\n";
						}
						else
						{
							$r->totalPay = $testBal;
							$r->setFlag(ROUND_FLAG_TEST_BALANCE, false /*updateChange*/);
							if (!$r->Update())
							{
								XLogNotify("Round admin page - round $rid $action by $Login->UserName, round Update failed");
								echo "Round $action failed.<br/><br/>\n";
							}
							else
							{
								XLogNotify("Round admin page - round $rid $action by $Login->UserName to ".XVarDump($testBal));
								echo "Round successfully $action.<br/><br/>\n";
							}
						}
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && $action == "Clear Test Balance")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$r = $Rounds->getRound($rid);
					if ($r === false)
					{
						XLogNotify("Round admin page - $action getRound id $rid failed");
						echo "Round not found.<br/><br/>\n";
					}
					else
					{
						if (!$r->clearFlag(ROUND_FLAG_TEST_BALANCE))
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName, round clearFlag failed");
							echo "Round $action failed.<br/><br/>\n";
						}
						else
						{
							XLogNotify("Round admin page - round $rid $action by $Login->UserName");
							echo "Round successfully $action.<br/><br/>\n";
						}
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($action == "Force Next Index")
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					$nridx = XGetPost("nridx", false);
					//------------------------
					if ($nridx === false || !is_numeric($nridx))
					{
						XLogNotify("Round admin page - $action validate new next round index failed: ".XVarDump($nridx));
						echo "New round index invalid.<br/><br/>\n";
					}
					else if ($Rounds->getRound($nridx) !== false)
					{
						XLogNotify("Round admin page - $action new next round index already exists: ".XVarDump($nridx));
						echo "New round index already exists.<br/><br/>\n";
					}
					else
					{
						XLogNotify("User $Login->User is forcing the next round index to: $nridx");
						if (!$Rounds->setAutoIncrement($nridx))
						{
							XLogNotify("Round admin page - $action XLogNotify setAutoIncrement failed, new idx was: ".XVarDump($nridx));
							echo "Set new round index failed.<br/><br/>\n";
						}
						else
							echo "Forced next round index to $nridx.<br/><br/>\n";
					}
					//------------------------
				}
				else
					echo "$action round requires delete history privileges.<br/><br/>\n";
			}
			else if ($rid !== false && ($action == "Load Worker and Payout Details" || $action == "Unload Worker and Payout Details"))
			{
				$showDetails = ($action == "Load Worker and Payout Details" ? true : false);
			}			
			else if ($rid !== false && ($action == "Load Idle Worker Details" || $action == "Unload Idle Worker Details"))
			{
				$showZeroDetails = ($action == "Load Idle Worker Details" ? true : false);
			}
			else if ($rid !== false && ($action == "User Fix" || $action == "User Bypass" || $action == "UnResolve"))
			{
				if ($Login->HasPrivilege(PRIV_DELETE_HISTORY))
				{
					//------------------------
					if ($meid === false || !is_numeric($meid))
					{
						XLogNotify("Round admin page - $action validate selected monitor event id failed: ".XVarDump($meid));
						echo "Round monitor event id invalid.<br/><br/>\n";
					}
					else
					{
						//------------------------
						if ($action == "User Fix")
							$resolveFlag = MONEVENT_FLAG_USER_FIX;
						else // if ($action == "User Bypass")
							$resolveFlag = MONEVENT_FLAG_USER_BYPASS;
						//------------------------
						$event = $Monitor->getEvent($meid);
						//------------------------
						if ($event === false)
						{
							XLogNotify("Round admin page - $action selected monitor event not found failed. id: ".XVarDump($meid));
							echo "Round monitor event not found.<br/><br/>\n";
						}
						else if ($action != 'UnResolve' && !$event->resolve($resolveFlag, true /*pushRoundAutomation*/))
						{
							XLogNotify("Round admin page - $action Monitor event resolve failed");
							echo "Round monitor event resolve failed.<br/><br/>\n";
						}
						else if ($action == 'UnResolve' && !$event->unresolve())
						{
							XLogNotify("Round admin page - $action Monitor event unresolve failed");
							echo "Round monitor event UnResolve failed.<br/><br/>\n";
						}
						else 
							echo "Round monitor event resolved set to '$action'.<br/><br/>\n";
						//------------------------
					} // if $meid
				} // HasPrivilege
			}
			//------------------------
			$defTeamId = $Config->Get(CFG_ROUND_TEAM_ID, DEFAULT_ROUND_TEAM_ID);
			$defRoundFlags =  $Config->Get(CFG_ROUND_FLAGS, DEFAULT_ROUND_FLAGS);
			//------------------------
			$nridx = $Rounds->getAutoIncrement();
			if ($nridx === false)
				$nridx = '';
			//------------------------
		?>
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
			<fieldset class="loginBox">
				<?php 
					PrintSecTokenInput(); 
					if ($showDetails) echo "<input type='hidden' name='det' value='1' />\n";
					if ($showZeroDetails) echo "<input type='hidden' name='zdet' value='1' />\n";
				?>
				<input type="hidden" name="p" value="<?php echo $pagenum; ?>" />
				<input type="hidden" name="meid" value="<?php echo ($meid === false ? "" : $meid); ?>" />
				<input type="hidden" name='rsort' value='<?php echo $rListSort; ?>' />
				<input type="hidden" name='wsort' value='<?php echo $wListSort; ?>' />
				<input type="hidden" name='mesort' value='<?php echo $meListSort; ?>' />
			<?php
			//------------------------
			echo "<script>\nfunction selRound(id,rsort=$rListSort){\n document.location.href='./Admin.php?p=$pagenum&rid=' + id + '&rsort=' + rsort + '&wsort=$wListSort".($meid !== false ? "&meid=$meid" : "")."&mesort=$meListSort&lmt=".($rLimit === false ? 0 : $rLimit)."&".($showDetails ? 'det=1&' : '')."&".($showZeroDetails ? 'zdet=1&' : '');
			echo SecToken()."';\n}\n</script>\n";
			//------------------------
			# Round List Columns (<Width/Col style>, <Header Title>, <Field for sorting or false>)
			$Columns = array(	array(	"35px",		"ID",			DB_ROUND_ID),
								array(	"200px",	"Started",		DB_ROUND_DATE_STARTED),
								array(	"220px",	"Status",		false),
								array(	"150px",	"Total Work",	DB_ROUND_TOTAL_WORK),
								array(	"170px",	"Total Pay",	DB_ROUND_TOTAL_PAY)
								);
			//------------------------	
			echo "<table class='admin-table' style='width:800px;'>\n<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'>\n";
			//------------------------	
			$colIdx = 1;
			foreach ($Columns as $col)
			{
				$style = "";
				if ($colIdx == abs($rListSort))
					$style .= "background:#EEEEEE;";
				if ($col[0] !== false)
					$style .= "width:$col[0];";
				echo "\t<th".($style !== "" ? " style='$style'" : "");
				if ($col[2] !== false)
					echo " onclick=\"selRound(".($rid === false ? "''" : $rid).",".($colIdx == abs($rListSort) ? ($rListSort * -1) : $colIdx).");\"";
				echo ">$col[1]</th>\n";
				$colIdx++;
			}
			//------------------------	
			echo "</tr></thead>\n<tbody class='admin-scrolltable-body'>\n";
			//------------------------	
			$scidx = ($rListSort < 0 ? ($rListSort * -1) : $rListSort);
			if ($scidx === false || $scidx < 1 || $scidx >= sizeof($Columns))
				$scidx = 1;
			$sort = $Columns[$scidx - 1][2];
			if ($sort !== false && $rListSort < 0)
				$sort .= " DESC";				
			//------------------------	
			if ($defFilters && $rid !== false)
			{
				$singleRound = $Rounds->getRound($rid);
				if ($singleRound === false)
				{
					XLogError("Rounds admin page - Rounds getRound failed: ".XVarDump($rid));
					$rlist = array();
					$rid = false;
				}
				else $rlist = array($singleRound);
			}
			else $rlist = $Rounds->getRounds(false /*OnlyDone*/, false /*onlyNotPaid*/, $rLimit, $sort);
			if ($rlist === false)
				XLogError("Rounds admin page - getRounds failed");
			if ($rlist === false || sizeof($rlist) == 0)
			{
				//------------------------
				echo "<tr class='admin-row'>";
				$colIdx = 0;
				foreach ($Columns as $col)
				{
					echo "<td".($col[0] !== false ? " style='width:".$col[0].";'" : "").">&nbsp;</td>";
					$colIdx++;
				}
				echo "</tr>\n";
				//------------------------
			}
			else
			{
				//------------------------
				if ($rid === false && ROUND_ADMIN_PAGE_AUTO_SELECT === true) // none selected, try to select newest
					foreach ($rlist as $r)
						if ($rid === false || $r->id > $rid)
							$rid = $r->id;
				//------------------------
				$rSelRoundFound = false;
				$rowIdx = 0;
				foreach ($rlist as $r)
				{
					//------------------------
					$statusText = '';
					if (!$r->isDone())
						$statusText .= "($r->step) ";
					$statusText .= XEncodeHTML($r->actionName());
					$startedText = XEncodeHTML($Display->htmlLocalDate($r->dtStarted));
					//------------------------
					if ($r->hasFlag(ROUND_FLAG_DRYRUN)) $statusText .= " (Dry Run/No Pay)";
					if ($r->hasFlag(ROUND_FLAG_HIDDEN)) $statusText .= " (hidden test)";
					//------------------------
					if ($r->isActive() && ($progData = $r->getProgress()))
						if (sizeof($progData) >= 2 && $progData[1] != 0)
						{
							$percent = (int)(($progData[0] / $progData[1]) * 100.0);
							$count = (sizeof($progData) >= 3 && $progData[2] != 0 ? " [".$progData[2]."]" : "");
							$statusText .= " ($percent%$count)";
						}
					//------------------------
					if ($rid == $r->id)
					{
						//-----------
						$rSelRoundFound = true;
						$countSummary = $r->getCountSummary(true /*includeTxId*/);
						if ($countSummary === false)
							XLogError("Admin Page selected round getCountSummary failed");
						//-----------
						$rIsDryRun = $r->hasFlag(ROUND_FLAG_DRYRUN);
						$rIsHidden = $r->hasFlag(ROUND_FLAG_HIDDEN);
						$rFlagStorepay = $r->hasFlag(ROUND_FLAG_STORE_VALUES);
						$rFlagForfeit = $r->hasFlag(ROUND_FLAG_FORFEIT_STORE);
						$rIsTestBal = $r->hasFlag(ROUND_FLAG_TEST_BALANCE);
						//-----------
						$rFlags = '';
						if ($rIsDryRun)
							$rFlags .= 'DryRun ';
						if ($rIsHidden)
							$rFlags .= 'Hidden ';
						if ($rIsTestBal)
							$rFlags .= 'Test-Balance ';
						if ($rFlagStorepay)
							$rFlags .= 'StorePay ';
						if ($rFlagForfeit)
							$rFlags .= 'Forfeit ';
						$rFlags .= "($r->flags)";
						//-----------
						if ($r->dtStarted === false)
							$rHtmlMarket = '[Not started]';
						else
						{
							if ($r->dtPaid === false || !$r->isDone())
								$result = 0;
							else
								$result = $MarketHistory->getHistory($r->dtStarted, false /*pollIfNeeded*/);
							//-----------
							if ($result === false)
								$rHtmlMarket = '[Failed]';
							else if ($result === 0)
								$rHtmlMarket = '[Has No History]';
							else if (!is_array($result) || !isset($result['found']))
								$rHtmlMarket = '[Invalid]';
							else if (!$result['found'])
								$rHtmlMarket = '[Not Found]';
							else if (!isset($result['asset_values']))
								$rHtmlMarket = '[Missing values]';
							else 
							{
								$rHtmlMarket = ($r->dtPaid === false ? '(Started) ' : '(Paid) ');
								$values = $MarketHistory->getAssestValuesForDisplay($result['asset_values'], array('value_trim' => 'large', 'value_symbol' => 'fiat', 'space' => '&nbsp;', 'result_empty_value' => '[empty list]', 'unknown_asset' => 'fail') /*options*/, '?'/*null/bad value*/, '[Invalid values]', '[Invalid value entry]', '[Unknown asset]');
								//XLogDebug("Admin Page selected round getAssestValuesForDisplay:\n\tasset values: ".XVarDump($result['asset_values'])."\n\tdisplay results: ".XVarDump($values));
								if (!is_array($values))
									$rHtmlMarket .= $values;
								else
								{
									$selMvIdx = false;
									$rHtmlMarket .= "<select name='mktvals' id='mktvals'>\n";
									for ($mvIdx = 0;$mvIdx < sizeof($values);$mvIdx++)
										$rHtmlMarket .= "<option value='$mvIdx'".($selMvIdx !== false && $selMvIdx == $mvIdx ? " selected='selected'" : "").">".$values[$mvIdx]."</option>\n";
									$rHtmlMarket .= "</select>\n";
								}
							}
						}
						
						//------------------------
						$rowClass = 'admin-row-sel';
						$rComment = $r->getComment('' /*default*/);
						$rComment = ($rComment === false ? "&lt;error&gt;" : XEncodeHTML($rComment));
						$rActive = $r->isActive();
						$rDone = $r->isDone();
						$rStep = $r->step;
						$rStatus = $statusText;
						$rAppr = $r->hasFlag(ROUND_FLAG_APPROVED);
						$rFunded = $r->hasFlag(ROUND_FLAG_FUNDED);
						$rTeamId = ($r->teamId === false ? "" : $r->teamId);
						$rStarted = $startedText;
						$rStats = ($r->dtStats === false ? '(not recorded)' : '(recorded) '.$Display->htmlLocalDate($r->dtStats));
						$rPaid = ($r->dtPaid === false ? '(not paid)' : '(paid) '.$Display->htmlLocalDate($r->dtPaid));
						$rTotWork = $r->totalWork;
						$rTotPay = sprintf("%f", $r->totalPay);
						$rStorePay = sprintf("%f", $r->storePay);
						$rPayStored = sprintf("%f", $r->payStored);
					}
					else $rowClass = 'admin-row';
					//------------------------
					echo "<tr class='$rowClass' onclick=\"selRound($r->id);\">\n";
					//------------------------
					$colData = array(	$r->id,
										$startedText,
										$statusText,
										$r->totalWork,
										sprintf("%0.f", $r->totalPay)
									);
					//------------------------
					for ($i = 0;$i < sizeof($colData);$i++)
					{
						$style = "";
						if ($rowIdx == 0 && $Columns[$i][0] !== false)
							$style .= "width:".$Columns[$i][0].";";
						if ($Columns[$i][1] == "Status" || $Columns[$i][1] == "Total Work" || $Columns[$i][1] == "Total Pay")
							$style .= "text-align:right;";
						if ($style != "")
							$style = " style='$style'";
						echo "<td$style>".$colData[$i]."</td>";
					}
					//------------------------
					echo "</tr>\n";
					//------------------------
					$rowIdx++;
					//------------------------
				}
				//------------------------
				if (!$rSelRoundFound && $rid !== false)
					$rid = false;
				//------------------------
			}
			//------------------------
			echo "</tbody></table>\n";		
			//------------------------	
		?>
				Round List Limit: <input style='max-width:80px;' id='lmt' type='text' name='lmt' value='<?php echo ($rLimit === false ? 0 : $rLimit); ?>' maxlength='6' />
				<input type='submit' name='action' value='Update Limit' /><br/>
				<br/>
				<hr/>
				<br/>
				<input type="hidden" name="rid" value="<?php echo ($rid === false ? "" : $rid); ?>" />
		<?php
		if ($rid !== false)
		{
			//------------------------
			echo "<script>\nfunction selAdd(id){\n window.open('./AdminEL.php?t=wa&i='+id+'&".SecToken()."');\n }\n";
			echo "function selTxid(id){\n window.open('./AdminEL.php?t=pt&i='+id+'&".SecToken()."');\n }\n";
			echo "function selWkr(id){\n window.open('./Admin.php?p=5&idx=' + id + '&".SecToken()."');\n }\n";
			echo "function selSt(id){\n window.open('./Admin.php?p=9&idx=' + id + '&".SecToken()."');\n }\n";
			echo "function selP(id){\n window.open('./Admin.php?p=7&idx=' + id + '&".SecToken()."');\n }\n</script>\n";
			//------------------------
		?>
				<h2>Selected Round:</h2>
				<div style="width:100%;"><!-- Round Details -->
				<h3>Round Details</h3>
				<table class='admin-table' style='width:500px;'>
				<thead class='admin-scrolltable-header'>
					<tr style='width:100%;'><th style='min-width:200px;'>Attribute</th><th style='width:100%;'>Value</th></tr>
				</thead>
				<tbody class='admin-scrolltable-body' style='max-height:none;'>
				<?php 
					//------------------------
					$rTxid = "&nbsp;";
					if ($countSummary['txid'] !== false)
					{
						if (strpos($countSummary['txid'], "[") !== false)
							$rTxid = XEncodeHTML($countSummary['txid']);
						else
							$rTxid = "<a href='".BRAND_TX_LINK.XEncodeHTML($countSummary['txid'])."'>".XEncodeHTML(substr($countSummary['txid'], 0, 9))."...</a>";
					}
					//------------------------
					$attributes = array( 	array("ID", "$rid"),
											array("Comment", $rComment),
											array("Active", ($rActive === true ? "Yes" : "No")),
											array("Started", $rStarted),
											array("Status", $rStatus),
											array("Flags", $rFlags), 
											array("Stats", $rStats),
											array("Payment", $rPayment),
											array("Team", $rTeamId),
											array("Total Work", $rTotWork),
											array("Total Pay", $rTotPay),
											array("Pay to Store", $rPayStored),
											array("Pay from Store", $rStorePay),
											array("Approved", ($rAppr === true ? "Yes" : "No")),
											array("Funded", ($rFunded === true  ? "Yes" : "No")),
											array("Market Values", $rHtmlMarket),
											array("TxID", $rTxid));
					//------------------------
					$rowIdx = 0;
					foreach ($attributes as $attr)
					{
						echo "<tr class='admin-row'><td".($rowIdx == 0 ? " style='min-width:200px;'" : "").">$attr[0]</td><td style='text-align:right;".($rowIdx == 0 ? "width:100%;" : "")."'>$attr[1]</td></tr>\n";
						$rowIdx++;
					}
					//------------------------
					echo "</tbody></table>\n</div><!-- Round Details -->\n"; // end of Round details
					//------------------------
				
					echo "<input type='submit' name='action' value='Get Market History' /><br/>";
					//------------------------ Start of comment/approve
					$hasDeletePriv = $Login->HasPrivilege(PRIV_DELETE_HISTORY);
					$hasApprovePriv = $Login->HasPrivilege(PRIV_ROUND_APPROVE);
					//------------------------
					if ($hasDeletePriv || ($rActive === true && $hasApprovePriv))
					{
						//------------------------
						echo "<hr/><br/>\n";
						echo "<h4>Common Privileged Actions:</h4>\n";
						//------------------------
						if ($rActive === true && $hasApprovePriv) 
							echo "<label class='loginLabel'>Round Approval:</label><br/><input type='submit' name='action' value='".($rAppr === true ? "Unapprove" : "Approve" )."' /><br/>\n";
						//------------------------
						if ($hasDeletePriv)
						{
							echo "<label class='loginLabel' for='comment'>Round Comment:</label><br/>";
							echo "<input style='width:90%;' id='comment' type='text' name='comment' value='$rComment' maxlength='".C_MAX_COMMENT_TEXT_LENGTH."' ".(!$hasDeletePriv ? "disabled='disabled' " : "")." /><br/>\n";
							echo "<input type='submit' name='action' value='Update Comment' />\n";
						}
						//------------------------
					} // comment set / approval
					//------------------------
				?>	
				<br/>
				<hr/>
				<br/>
				<div style="width: 500px; "><!-- Mon Events -->
				<h3>Round Monitor Events:</h3>
				<?php 
					//------------------------
					echo "<script>\nfunction selMonEvent(id,mesort=$meListSort){\n document.location.href='./Admin.php?p=$pagenum".($rid !== false ? "&rid=$rid" : "")."&rsort=$rListSort&wsort=$wListSort&meid=' + id + '&mesort=' + mesort + '&lmt=".($rLimit === false ? 0 : $rLimit)."&".($showDetails ? 'det=1&' : '')."&".($showZeroDetails ? 'zdet=1&' : '');
					echo SecToken()."';\n}\n</script>\n";
					//------------------------
					$actionNames = $Rounds->getActionTextNames();
					//------------------------
					$sort = false;
					$absSort = abs($meListSort);
					if ($absSort == 1)
						$sort = DB_MONEVENT_ID;
					if ($sort !== false && $meListSort < 0)
						$sort .= " DESC";
					//---------------------------
					$blockingZeroEventList = $Monitor->RoundEventsBlocking(0/*ridx auto (include 0)*/, true /*alwaysIncludeRoundZero*/);
					if ($blockingZeroEventList === false)
					{
						XLogError("Admin Page Monitor RoundEventsBlocking blockingZeroEventList failed");
						$blockingZeroEventList = array();
					}
					//---------------------------
					$monEventList = $Monitor->getRoundEvents($rid, false /*where*/, $sort, false /*limit*/);
					//---------------------------
					if (sizeof($blockingZeroEventList) != 0)
						$monEventList = XArrayConcat($blockingZeroEventList, $monEventList);
					//---------------------------
					$selMonEvent = false;
					$rows = array();
					if ($monEventList === false)
						$rows[] = array('Error', 'Error', 'Error', 'Error');
					else if (sizeof($monEventList) == 0)
						$rows[] = array('&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;');
					else
					{
						//------------------------
						foreach ($monEventList as $event)
						{
							//------------------------
							if ($meid !== false && $meid == $event->id)
								$selMonEvent = $event;
							//------------------------
							$strIssue = $Monitor->issueName($event->issue);
							$isResolved = $event->isResolved();
							//------------------------
							$strDetails = '';
							$curstep = $curAction = false;
							$oldStep = $oldAction = false;
							$prog = $progMax = false;
							$detailsList = array();
							foreach ($event->details as $key => $value)
							{
								if ($key == MONEVENT_DETAIL_ROUND_STEP || $key == MONEVENT_DETAIL_ROUND_ACTION)
								{
									if ($key == MONEVENT_DETAIL_ROUND_STEP)
										$curstep = $value;
									else
										$curAction = (isset($actionNames[$value]) ? $actionNames[$value] : "[Unknown $value]");
									if ($curstep !== false && $curAction !== false)
									{
										$detailsList[] = "Step=($curstep)'$curAction'";
										$curstep = $curAction = false;
									}
								}
								else if ($key == MONEVENT_DETAIL_ROUND_OLD_STEP || $key == MONEVENT_DETAIL_ROUND_OLD_ACTION)
								{
									if ($key == MONEVENT_DETAIL_ROUND_OLD_STEP)
										$oldStep = $value;
									else
										$oldAction = (isset($actionNames[$value]) ? $actionNames[$value] : "[Unknown $value]");
									if ($oldStep !== false && $oldAction !== false)
									{
										$detailsList[] = "Old Step=($oldStep)'$oldAction'";
										$oldStep = $oldAction = false;
									}
								}
								else if ($key == MONEVENT_DETAIL_ROUND_PROG || $key == MONEVENT_DETAIL_ROUND_PROG_MAX)
								{
									if ($key == MONEVENT_DETAIL_ROUND_PROG)
										$prog = $value;
									else
										$progMax = $value;
									if ($prog !== false && $progMax !== false)
									{
										if ($prog != 0 || $progMax != 0)
											$detailsList[] = "Prog=$prog/$progMax";
										$prog = $progMax = false;
									}
								}
								else
									$detailsList[] = $Monitor->detailName($key)."=$value";
							}
							//------------------------
							if ($event->type == MONEVENT_TYPE_NOTIFY && $event->issue == MONEVENT_ISSUE_ROUNDSTEP_COMPLETED && isset($event->details[MONEVENT_DETAIL_ROUND_OLD_STEP]) && isset($event->details[MONEVENT_DETAIL_ROUND_OLD_ACTION]) && isset($event->details[MONEVENT_DETAIL_ELAPSED]) )
							{
								//------------------------
								$step = $event->details[MONEVENT_DETAIL_ROUND_OLD_STEP];
								$actionValue = $event->details[MONEVENT_DETAIL_ROUND_OLD_ACTION];
								$action = (isset($actionNames[$actionValue]) ? $actionNames[$actionValue] : "[Unknown $actionValue]");
								$elapsed = (isset($event->details[MONEVENT_DETAIL_ELAPSED_FULL]) ? $event->details[MONEVENT_DETAIL_ELAPSED_FULL] : $event->details[MONEVENT_DETAIL_ELAPSED]);
								$stepCount = (isset($event->details[MONEVENT_DETAIL_ROUND_STEP_COUNT]) && $event->details[MONEVENT_DETAIL_ROUND_STEP_COUNT] > 1 ? ' count '.$event->details[MONEVENT_DETAIL_ROUND_STEP_COUNT] : '');
								$strDetails = XEncodeHTML("Step ($step)'$action' took $elapsed sec$stepCount");
								//------------------------
							}
							else
								$strDetails = XEncodeHTML(implode(', ', $detailsList));
							//------------------------
							$strStatus = "";
							$colorStatus = false;
							if ($event->type == MONEVENT_TYPE_NONE)
							{
								$strStatus .= '[Type NONE]';
								$colorStatus = 'red';
							}
							else if ($event->type == MONEVENT_TYPE_NOTIFY)
								$strStatus .= '[N]';
							else if ($event->type == MONEVENT_TYPE_WARN)
							{
								$strStatus .= '[W]';
								$colorStatus = ($isResolved ? '#ffa700' : '#f94d00');
							}
							else if ($event->type == MONEVENT_TYPE_ALARM)
							{
								$strStatus .= '[A]';
								$colorStatus = ($isResolved ? '#ff9999' : '#ff0000');
							}
							else if ($event->type == MONEVENT_TYPE_BLOCK)
							{
								$strStatus .= '[B]';
								$colorStatus = ($isResolved ? '#dda0dd' : '#9933cc');
							}
							$strStatus .= "&nbsp;$strIssue";
							$strStatus = XEncodeHTML($strStatus);
							if ($colorStatus !== false)
								$strStatus = "<div style='background-color:$colorStatus;'>$strStatus</div>";
							//------------------------
							$rows[] = array($event->id, $strStatus, "<span style='font-size:80%;'>".XEncodeHTML($strDetails)."</span>", XEncodeHTML($strIssue) /*hidden*/, $detailsList /*hidden*/);
							//------------------------
						} // foreach monEventList
						//------------------------
					}
					//------------------------
					$eventCols = array(	array("ID", 		'50px',  1, false),
										array("Issue",		'280px', 2, false),
										array("Details", 	'430px',  3, "width:430px;max-width:430px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;")
									);
					//------------------------
					echo "<table class='admin-table' style='width:800px;'>\n";
					echo "<thead class='admin-scrolltable-header'>\n";
					echo "<tr style='width:100%;'>\n";
					$colIdx = 1;
					foreach ($eventCols as $col)
					{
						$style = "";
						if ($colIdx == abs($meListSort))
						{
							$sort = $meListSort * -1;
							$style .= "background:#EEEEEE;";
						}
						else
							$sort = $colIdx;
						if ($col[1] !== false)
							$style .= "min-width:".$col[1].";";
						echo "<th".($style == "" ? "" : " style='$style'");
						if ($col[2] !== false)
							echo " onclick=\"selMonEvent(".($meid === false ? "''" : $meid).",$sort);\"";
						echo ">$col[0]</th>\n";
						$colIdx++;
					}
					echo "</tr>\n</thead>\n";
					echo "<tbody class='admin-scrolltable-body' style='width:800px;'>\n"; //  style='height:200px;'
					//------------------------
					$rowIdx = 0;
					$selMonEventRow = false;
					foreach ($rows as $row)
					{
						if ($meid !== false && $meid == $row[0])
						{
							$rowClass = 'admin-row-sel';
							$selMonEventRow = $row;
						}
						else $rowClass = 'admin-row';
						echo "<tr class='$rowClass' onclick=\"selMonEvent(".$row[0].");\" style='width:100%;'>";
						for ($c = 0;$c < sizeof($eventCols);$c++)
						{
							$style = "";
							$text = $row[$c];
							if ($eventCols[$c][3] !== false)
								$text = "<div style='".$eventCols[$c][3]."'>$text</div>";
							else if ($rowIdx == 0 && $eventCols[$c][1] !== false)
								$style .= 'min-width:'.$eventCols[$c][1].';';
							echo "<td".($style != "" ? " style='$style'" : "" ).">$text</td>";
						}
						echo "</tr>\n";
						$rowIdx++;
					}
					//------------------------
					echo "</tbody></table>";
					//------------------------
					if ($meid !== false && $selMonEventRow !== false && $selMonEvent != false)
					{
						//------------------------
						$strType = XEncodeHTML($Monitor->typeName($selMonEvent->type));
						//------------------------
						$isResolved = $selMonEvent->isResolved();
						if ($isResolved)
						{
							$strResolved = '';
							if ($selMonEvent->hasFlag(MONEVENT_FLAG_SELF_FIX))
								$strResolved .= 'Self-Fixed';
							if ($selMonEvent->hasFlag(MONEVENT_FLAG_USER_FIX))
								$strResolved .= 'User Fixed';
							if ($selMonEvent->hasFlag(MONEVENT_FLAG_USER_BYPASS))
								$strResolved .= 'User Bypass';
						}
						else $strResolved = 'No';
						$strResolved = XEncodeHTML($strResolved);
						$detailsList = $selMonEventRow[4];
						//------------------------
						echo "<div style='border:1px solid black;margin:15px;padding:20px;width:620px;'><h4>Monitor Event $meid</h4>\n";
						echo "<table class='admin-table' style='width:600px;'>\n";
						echo "<tbody class='admin-scrolltable-body' style='max-height:500px;'>\n"; // override max-height:200px from admin-scrolltable-bod
						echo "<tr class='admin-row'><td style='min-width:200px;'>Type</td><td style='width:100%;'>$strType</td></tr>\n";
						echo "<tr class='admin-row'><td>Issue</td><td>$selMonEventRow[3]</td></tr>\n";
						//------------------------
						if ($selMonEvent->type != MONEVENT_TYPE_NOTIFY)
						{
							echo "<tr class='admin-row'><td>Resolved</td><td>$strResolved&nbsp;";
							if (!$isResolved)
							{
								echo "\n<input name='action' type='submit' value='User Fix' />\n";
								echo "<input name='action' type='submit' value='User Bypass' />\n";
							}
							else 
								echo "<input name='action' type='submit' value='UnResolve' />\n";
							echo "</td></tr>\n";
						}
						//------------------------
						$strDate = XEncodeHTML($Display->htmlLocalDate($selMonEvent->date));
						echo "<tr class='admin-row'><td>Start Date</td><td>$strDate</td></tr>\n";
						if (is_numeric($selMonEvent->endDate) && $selMonEvent->date != $selMonEvent->endDate)
						{
							$strDate = XEncodeHTML($Display->htmlLocalDate($selMonEvent->endDate));
							echo "<tr class='admin-row'><td>End Date</td><td>$strDate</td></tr>\n";
							$strDiff = XEncodeHTML(XDateDiffFuzzy($selMonEvent->date, $selMonEvent->endDate));
							if ($strDiff !== false)
								echo "<tr class='admin-row'><td>Duration</td><td>$strDiff</td></tr>\n";							
						}
						//------------------------
						foreach ($detailsList as $detail)
						{
							$detailParts = explode('=', $detail);
							$name = XEncodeHTML($detailParts[0]);
							$value = XEncodeHTML($detailParts[1]);
							echo "<tr class='admin-row' style='background-color:#c4aead;'><td>$name</td><td>$value</td></tr>\n";
						}
						//------------------------
						echo "</tbody></table></div>\n";
						//------------------------
					} // if selected Monitor event
					//------------------------
				?>				
				</div><!-- Mon Events -->
				<hr/>
				<br/>
				<div><!-- Contributions -->
				<h3>Round Contributions:</h3>
				<?php 
					//------------------------
					$contForfeit = false;
					$contStorepay = false;
					$contModeNamesShort = $Contributions->getModeNames(true /*short*/, true /*includeSpecial*/);
					//------------------------
					$contList = $Contributions->findRoundContributions($rid);
					$rows = array();
					if ($contList === false)
						$rows[] = array("Error", "Error", "Error", "Error", "Error");
					else if (sizeof($contList) == 0)
						$rows[] = array("&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;");
					else
					{
						//------------------------
						$contOutcomeNames = $Contributions->getOutcomeNames();
						//------------------------
						foreach ($contList as $cont)
						{
							//------------------------
							$strCont = "(<a href='./Admin.php?p=8&amp;cidx=$cont->id&amp;".SecToken()."'>$cont->id</a>) <span style='font-size:80%'>".XEncodeHTML($cont->name)."</span>";
							//------------------------
							$value = $cont->value;
							if ($cont->hasFlag(CONT_FLAG_FULL_BALANCE))
								$value = '(all)';
							//------------------------
							if ($cont->total !== false)
								$value .= ' ('.$cont->total.')';
							$strRate = $value.' '.(isset($contModeNamesShort[$cont->mode]) ? $contModeNamesShort[$cont->mode] : '(Invalid Mode)');
							//------------------------
							if ($cont->mode == CONT_MODE_SPECIAL_STORE_PAY)
								$contStorepay = $cont;
							else if ($cont->mode == CONT_MODE_SPECIAL_STORE_FORFEIT)
								$contForfeit = $cont;								
							//------------------------
							$strOutcomeName = (is_numeric($cont->outcome) && isset($contOutcomeNames[$cont->outcome]) ? $contOutcomeNames[$cont->outcome] : "(Unknown ".$cont->outcome.")");
							if ($cont->isDone())
							{
								$strDone = "<span style='font-size:80%'>".(!$cont->isDone() ? " (done date not set)" : " ".$Display->htmlLocalDate($cont->dtDone))."</span>";
								if ($cont->hasFlag(CONT_FLAG_MANUALLY_BYPASSED))
								{
									$strOutcomeName = "Manually bypassed";
									$strTxid = XEncodeHTML($cont->txid);
								}
								$strOutcome = "$strOutcomeName $strDone";
							}
							else $strOutcome = $strOutcomeName;
							//------------------------
							if ($cont->txid != "")
							{
								if (strpos($cont->txid, "[") !== false)
									$strTxid = XEncodeHTML( strlen($cont->txid) <= 9 ? $cont->txid : substr($cont->txid, 0, 9)."...");
								else
									$strTxid = "<a style='font-size:80%;' href='".BRAND_TX_LINK.XEncodeHTML($cont->txid)."'>".XEncodeHTML(substr($cont->txid, 0, 9))."...</a>";
							}
							else
								$strTxid = "&nbsp;";
							//------------------------
							if ($cont->address === false)
								$strAddr = "[address unknown]";
							else
								$strAddr = "<a style='font-size:80%;' href='".BRAND_ADDRESS_LINK.XEncodeHTML($cont->address)."'>".XEncodeHTML(substr($cont->address, 0, 7))."...</a>";
							//------------------------
							$rows[] = array($strCont, $strAddr, $strRate, $strOutcome, $strTxid);
							//------------------------
						}
						//------------------------
					}
					//------------------------
					$contCols = array(	array("Contribution", 	170, 1),
										array("Address", 		80, 2),
										array("Rate", 			215, 3),
										array("Outcome", 		185, 4),
										array("Txid", 			90, 5)
									);
					//------------------------
					echo "<table class='admin-table' style='width:800px;'>\n";
					echo "<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'>\n";
					foreach ($contCols as $col)
						echo "<th".($col[1] === false ? '' : " style='width:".$col[1]."px;'").">$col[0]</th>\n";
					echo "</tr>\n</thead>\n";
					echo "<tbody class='admin-scrolltable-body' style='height:100px;'>\n";
					//------------------------
					$rowIdx = 0;
					foreach ($rows as $row)
					{
						echo "<tr class='admin-row'>";
						for ($c = 0;$c < sizeof($contCols);$c++)
						{
							$style = "";
							if ($rowIdx == 0 && $contCols[$c][1] !== false)
								$style = 'width:'.$contCols[$c][1].'px;';
							echo "<td".($style != "" ? " style='$style'" : "" ).">$row[$c]</td>";
						}
						echo "</tr>\n";
						$rowIdx++;
					} // contributions
					//------------------------
					echo "</tbody></table>\n";
					//------------------------
					if ($rActive === true && $rid !== false)
					{
						//------------------------
						echo '<br/><h4>Totals Summary:</h4>';
						$contStepData = $Contributions->getContributionStepData($rid, false /*isTest*/, true /*peekOnly*/);
						if ($contStepData === false)
						{
							XLogError("Rounds admin page - Contributions getContributionStepData for round $rid failed");
							echo "<br/><div>Contributions getContributionStepData failed!</div><br/>\n";
						}
						else
						{
							//------------------------
							echo "<div>\n";
							echo "Original Balance Full: ".$contStepData['fullOriginalBalance']." / After Est. Fee: ".$contStepData['originalBalance']."<br/>\n";
							echo "Balance Full: ".$contStepData['fullBalance']." / After Est. Fee: ".$contStepData['balance']."<br/>\n";
							echo "Balance Used: ".$contStepData['balanceUsed']." / Balance Added: ".$contStepData['balanceAdded']."<br/>\n";
							echo "Balance Side-Use: ".$contStepData['balanceSideUse']."<br/>\n";
							echo "Est. Fee Payout: ".$contStepData['estFee']." / Est. Fee Contribution: ".$contStepData['estContFee']."<br/>\n";
							echo "Minimum Pay: ".$contStepData['payMinimum']." / Minimum Reward: ".$contStepData['rewardMinimum']."<br/>\n";
							echo "</div><br/>\n";
							//------------------------
						}
						//------------------------
					}
					//------------------------
					echo '<div> <!-- cont summary storePay/Forfeit -->';
					//------------------------
					$payStoredSummary = $Payouts->findRoundPayStoredSummary($rid);
					if ($payStoredSummary === false)
						XLogError("Rounds admin page - Payouts findRoundPayStoredSummary failed");
					//------------------------
					$txt = '';
					if ($payStoredSummary === false || !is_array($payStoredSummary))
						$txt = '[Error]';
					else
					{
						if ($payStoredSummary['countToPay'] > 0)
							$txt .= "paid/paying ".((float)$payStoredSummary['storedToPay'])." for ".$payStoredSummary['countToPay']." workers, ";
						$txt .= "stored/storing ".((float)$payStoredSummary['storedNoPay'])." for ".$payStoredSummary['countNoPay']." workers";
						if ($payStoredSummary['anomalyCount'] > 0)
							$txt .= ". <strong>Warning: ".$payStoredSummary['anomalyCount']." anomalies detected!</strong>";
					}
					echo "<div>Pay to store: $txt</div>";
					//------------------------
					if ($contStorepay === false)
						echo '<div>No pay from store</div>';
					else if (!$contStorepay->isDone())
						echo '<div>Pay from store: (not done)</div>';
					else if ($contStorepay->outcome == CONT_OUTCOME_TOO_LOW_SKIPPED)
						echo '<div>Pay from store: (too low, skipped)</div>';
					else if ($contStorepay->outcome != CONT_OUTCOME_PAID)
						echo '<div>Pay from store: (unsuccessful outcome)</div>';
					else 
						echo '<div>Pay from store: Total '.($contStorepay->total !== false ? $contStorepay->total : '[not set]').', Count '.($contStorepay->count !== false ? $contStorepay->count : '[not set]').'</div>';
					//------------------------
					if ($contForfeit === false)
						echo '<div>No forfeits</div>';
					else if (!$contForfeit->isDone())
						echo '<div>Forfeit: (not done)</div>';
					else if ($contForfeit->outcome == CONT_OUTCOME_TOO_LOW_SKIPPED)
						echo '<div>Forfeit: (too low, skipped)</div>';
					else if ($contForfeit->outcome != CONT_OUTCOME_PAID)
						echo '<div>Forfeit: (unsuccessful outcome)</div>';
					else 
						echo '<div>Forfeit: total '.($contForfeit->total !== false ? $contForfeit->total : '(not set)').' from '.($contForfeit->count !== false ? $contForfeit->count : '(not set)').' workers</div>';
					//------------------------
					echo '</div> <!-- cont summary storePay/Forfeit -->';
					//------------------------
				?>	
				</div><!-- Contributions -->
				<?php
				//------------------------
				echo "<hr/>\n";
				//------------------------
				if (!$showDetails)
				{
					//------------------------
					echo "<input type='submit' name='action' value='Load Worker and Payout Details' /><br/>\n";
					//------------------------
				}
				else
				{
					//------------------------
					echo "<h3>Worker and Payout Details:</h3>\n";
					echo "<input type='submit' name='action' value='Unload Worker and Payout Details' />&nbsp;";
					echo "<input type='submit' name='action' value='".($showZeroDetails ? "Unload Idle Worker Details" : "Load Idle Worker Details")."' /><br/>\n";
					//------------------------
					if ($contForfeit !== false && $contForfeit->isDone() && $contForfeit->outcome == CONT_OUTCOME_PAID && $contForfeit->txid !== false && strlen($contForfeit->txid) != 0)
					{
						//------------------------
						$ffPayouts = $Payouts->getRoundPayoutsWithTXID($rid, $contForfeit->txid);
						//------------------------
						$rows = array();
						if ($ffPayouts === false)
							$rows[] = array('Error', 'Error', 'Error');
						else if (sizeof($ffPayouts) == 0)
							$rows[] = array('&nbsp;', '&nbsp;', '&nbsp;');
						else
						{
							$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*$validOnly*/);
							if ($workerNameList === false)
								$workerNameList = array();
							foreach ($ffPayouts as $fpay)
							{
								if ($fpay->statPay == 0)
									$wrkIdx = $Payouts->findWorkerOfStorePayout($fpay->id); // zero on not found
								else
									$wrkIdx = $fpay->statPay;
								
								if ($wrkIdx === false || $wrkIdx == 0 || !isset($workerNameList[$wrkIdx]))
									$strWorker = "[not found]";
								else
								{
									$hwidx = dechex($wrkIdx);
									$strWorker = "(<a  href='javascript:selWkr(0x$hwidx);'>$hwidx</a>) ";
									$strWorker .= "<a style='font-size:80%' href='javascript:selAdd(0x$hwidx);'>".XEncodeHTML(substr($workerNameList[$wrkIdx], 0, 7))."...</a>";
								}			
								$hpidx = dechex($fpay->id);
								$strPayout = "(<a href='javascript:selP(0x$hpidx);'>$fpay->id</a>)";
								$rows[] = array($strPayout, $strWorker, XEncodeHTML($fpay->pay));
							}									
						}							
						//------------------------
						$ffCols = array(	array("Forfeit",		'90px', 	1),
											array("Worker", 		'200px', 	2),
											array("Total", 			'100%', 	3)
										);
						//------------------------
						echo "<br/><div> <!-- Forfeit payouts -->\n";
						echo "<h3>Forfeits:</h3>\n";
						echo "<table class='admin-table' style='width:450px;'>\n";
						echo "<thead class='admin-scrolltable-header'>\n<tr style='width:100%;'>\n";
						foreach ($ffCols as $col)
							echo "<th".($col[1] === false ? '' : " style='min-width:".$col[1].";'").">$col[0]</th>\n";
						echo "</tr>\n</thead>\n";
						echo "<tbody class='admin-scrolltable-body' style='min-height:40px;max-height:200px;'>\n";
						//------------------------
						$rowIdx = 0;
						foreach ($rows as $row)
						{
							echo "<tr class='admin-row'>";
							for ($c = 0;$c < sizeof($ffCols);$c++)
							{
								$style = "";
								if ($rowIdx == 0 && $ffCols[$c][1] !== false)
									$style = 'min-width:'.$ffCols[$c][1].';';
								echo "<td".($style != "" ? " style='$style'" : "" ).">$row[$c]</td>";
							}
							echo "</tr>\n";
							$rowIdx++;
						} // forfeit payouts
						//------------------------
						echo "</tbody></table>\n</div> <!-- Forfeit payouts -->\n<br/>\n";
						//------------------------
					}
					//------------------------
					$timer = new XTimer();
					$timer2 = new XTimer();
					//------------------------
					//			Columns 	(<Header Title>, <style>)
					$Columns = array( 	array("Worker", "width:160px;"),
										array("Points", "width:170px;"),
										array("Payout","width:130px;text-align:left;"),
										array("Reward",	"width:175px;text-align:right;"),
										array("Paid",	"width:100px;text-align:right;"),
										);
					//------------------------
					echo "<style>\n";
					for ($cidx = 0;$cidx < sizeof($Columns);$cidx++)
						if ($Columns[$cidx][1] != "")
							echo "#tblDet tbody tr td:nth-child(".($cidx + 1).") { ".$Columns[$cidx][1]." }\n";
					echo "</style>\n";
					//------------------------
					echo "<script>\nvar prevOl=window.onload;\nwindow.onload=function(){\n";
					echo "var ret;\n";
					echo "if(prevOl!=null) prevOl();\n";
					echo "ret = dfahTableInit('tblDet');\n";
					echo "if (ret !== true)\n\tconsole.log('dfahTableInit failed: '+ret);\n";
					echo "else {\n ret = addData();\n";
					echo "if (ret !== true)\n\tconsole.log('addData failed: '+ret);\n";
					echo "else {\n ret = dfahTableDisplay();\n";
					echo "if (ret !== true)\n\tconsole.log('dfahTableDisplay failed: '+ret);\n";
					echo "}}};\n";
					//------------------------
					echo "function addData(){\nvar ret;\n";
					$rowCount = 0;
					//------------------------
					$rows = array();
					//------------------------
					$cid = $wListSort;
					if ($cid < 0)
						$cid *= -1;
					//------------------------
					if ($cid >= 3 /*Payout*/)
					{
						$ssort = false;
						if ($cid == 3 /*pid*/)
							$psort = DB_PAYOUT_ID.($wListSort < 0 ? ' DESC' : '');
						else if ($cid == 4 /*Reward*/)
							$psort = DB_PAYOUT_STAT_PAY.($wListSort > 0 ? ' DESC' : '').','.DB_PAYOUT_PAY.($wListSort > 0 ? ' DESC' : '');
						else /*Paid*/
							$psort = DB_PAYOUT_PAY.($wListSort > 0 ? ' DESC' : '').','.DB_PAYOUT_STAT_PAY.($wListSort > 0 ? ' DESC' : '');
					}
					else
					{
						$psort = false;
						if ($cid == 2)
							$ssort = DB_STATS_WEEK_POINTS;
						else // $wListSort == 1
							$ssort = DB_STATS_WORKER;
						if ( ($cid == 2 && $wListSort > 0) || ($cid == 1 && $wListSort < 0) )
							$ssort .= " DESC";
					}
					//------------------------
					$timer->start();
					$payoutList = $Payouts->findRoundPayouts($rid, $psort);
					$statList = $Stats->findRoundStats($rid, $ssort);
					$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*$validOnly*/);
					$statNotZeroCount = 0;
					$rewardPayoutCount = 0;
					//------------------------
					$timeLookupPayout = "0";
					$timeLookupWorker = "0";
					$timeGetWork = "0";
					//------------------------
					if ($payoutList === false || $statList === false || $workerNameList === false)
						$rows[] = array("Error", "Error", "Error");
					else if (sizeof($statList) == 0)
						$rows[] = array("&nbsp;", "&nbsp;", "&nbsp;");
					else
					{
						//------------------------
						$timer2->start();
						$sortedStatPayoutList = array();
						if ($ssort !== false)
						{
							$workerPayoutList = array();
							foreach ($payoutList as $p)
								$workerPayoutList[$p->workerIdx] = $p;
							foreach ($statList as $s)
								$sortedStatPayoutList[] = array($s, (isset($workerPayoutList[$s->workerIdx]) ? $workerPayoutList[$s->workerIdx] : false));
						}
						else
						{
							$workerStatList = array();
							foreach ($statList as $s)
								$workerStatList[$s->workerIdx] = $s;
							foreach ($payoutList as $p)
								$sortedStatPayoutList[] = array( (isset($workerStatList[$p->workerIdx]) ? $workerStatList[$p->workerIdx] : false), $p);
						}
							
						//XLogDebug("Round admin details built payout lookup lists took ".$timer2->restartMs(true));
						//------------------------
						foreach ($sortedStatPayoutList as $statPayout)
						if ($statPayout[0] !== false && $statPayout[0]->work() != 0 || $showZeroDetails)
						{
							//------------------------
							$timer2->start();
							//------------------------
							$stat = $statPayout[0];
							$payout = $statPayout[1];
							//------------------------
							$widx = $stat->workerIdx;
							//------------------------
							$timeLookupPayout = bcadd($timeLookupPayout, $timer2->elapsedMs());
							if (!isset($workerNameList[$widx]))
								$strWorker = "($widx) [not found]";
							else
							{
								$hwidx = dechex($widx);
								$strWorker = "(<a  href='javascript:selWkr(0x$hwidx);'>$hwidx</a>) ";
								$strWorker .= "<a style='font-size:80%' href='javascript:selAdd(0x$hwidx);'>".XEncodeHTML(substr($workerNameList[$widx], 0, 7))."...</a>"; // tsa
									
							}
							$timeLookupWorker = bcadd($timeLookupWorker, $timer2->restartMs());
							//------------------------
							$work =  $stat->work();
							$timeGetWork = bcadd($timeGetWork, $timer2->restartMs());
							if ($work === false)
								$work = "none/error";
							else if ($work > 0)
								$statNotZeroCount++;
							//------------------------
							$strPoints = $work;
							$hsidx = dechex($stat->id);
							$strPoints .= " (<a href='javascript:selSt(0x$hsidx);'>$hsidx</a>)";
							//------------------------
							if ($payout === false)
							{
								$strPayout = '&nbsp;';
								$strPReward = '&nbsp;';
								$strPPay = '&nbsp;';
							}
							else
							{
								if ($payout->pay > 0 || $payout->storePay > 0) $rewardPayoutCount++;
								$hpidx = dechex($payout->id);
								$strPayout = "(<a href='javascript:selP(0x$hpidx);'>$hpidx</a>)".($payout->dtStored !== false ? ' Stored' : ($payout->dtPaid !== false ? ' Paid' : ''));
								$strPReward = ($payout->storePay != 0.0 ? "(+$payout->storePay) " : '').$payout->statPay;
								$strPPay = ($payout->dtStored !== false && $payout->pay == 0.0 ? '&nbsp;' : $payout->pay);
							}
							//------------------------
							if ($rowCount != 0)
								echo ",\n";
							else
								echo "ret = dfahTableAppendData([\n";
							echo "[\"$strWorker\",\"$strPoints\",\"$strPayout\",\"$strPReward\",\"$strPPay\"]";
							//------------------------
							$rowCount++;
							//------------------------
							/*
							if ($rowCount > 100)
							{
								echo "\n]);\nif (ret !== true) return 'tbl append data failed: '+ret;\n";
								$rowCount = 0;
								break;
							}*/
							//------------------------
						} // foreach statList
						//------------------------
					} // if payoutList statList workerNameList and sizeof(statList) != 0
					//------------------------
					//XLogDebug("admin build rows took ".$timer->restartMs(true).", with looking up payouts $timeLookupPayout lookup worker $timeLookupWorker getting work $timeGetWork");
					//------------------------
					if ($rowCount != 0)
						echo "\n]);\nif (ret !== true) return 'tbl append data failed: '+ret;\n";
					echo "return true;\n}\n</script>\n";
					//------------------------
					echo '<h4>Worker Points and Payouts:</h4>';
					echo "<div style='overflow:hidden;height:270px;width:100%;'>\n";
					echo "<table id='tblDet' name='tblDet' class='admin-table' style='width:800px;'>\n";
					//------------------------
					echo "<thead class='admin-scrolltable-header'>\n";
					echo "<tr style='width:100%;'>";
					//------------------------
					$colIdx = 1;
					foreach ($Columns as $col)
					{
						$style = "";
						if ($col[1] !== false)
							$style .= $col[1];
						if ($colIdx == abs($wListSort))
						{
							$sort = $wListSort * -1;
							$style .= "background:#EEEEEE;";
						}
						else
							$sort = $colIdx;
						echo "<th".($style != "" ? " style='$style'" : "");
						echo " onclick=\"document.location.href='./Admin.php?p=$pagenum&amp;lmt=$rLimit&amp;rsort=$rListSort&amp;wsort=$sort&amp;mesort=$meListSort&amp;".($meid !== false ? "meid=$meid&amp;" : "")."rid=".($rid === false ? "" : $rid)."&amp;".($showDetails ? "det=1&amp;" : "").($showZeroDetails ? "zdet=1&amp;" : "").SecToken()."';\"";
						echo ">$col[0]</th>";
						$colIdx++;
					}
					//------------------------
					echo "</tr></thead>\n<tbody class='admin-scrolltable-body round-details'>\n";
					echo "</tbody></table></div>\n";
					//------------------------
					$storePayoutList = $Payouts->findRoundPayouts($rid, false /*orderBy*/, true /*includePaid*/, 1 /*limit*/, WORKER_SPECIAL_STORE /*workerIdx*/);
					if ($storePayoutList === false)
					{
						XLogError("Rounds admin page - Payouts findRoundPayouts failed looking for pay to store payout");
						$storePayoutList = array();
					}
					//------------------------
					//XLogDebug("admin findRoundPayouts (special) took ".$timer->restartMs(true));
					//------------------------
					if (sizeof($storePayoutList) != 0)
					{
						$storePayout = $storePayoutList[0];
						if ($storePayout->txid == "")
							$strTxid = "&nbsp;";
						else if (strpos($storePayout->txid, "[") !== false)
							$strTxid = XEncodeHTML($storePayout->txid);
						else 
							$strTxid = "<a href='".BRAND_TX_LINK.XEncodeHTML($storePayout->txid)."'>".XEncodeHTML(substr($storePayout->txid, 0, 7))."...</a>";
						echo "<p class='admin-row' style='margin-left: 15px; width: 75%;'><a href='./Admin.php?p=7&amp;idx=$storePayout->id&amp;".SecToken()."'>($storePayout->id)</a> Stored $storePayout->pay txid: $strTxid</p><br/>\n";
					}
					//------------------------
				} // if ($showDetails)
					
					//------------------------
					if ($countSummary !== false)
						echo "<div>Stat count: ".$countSummary['stats']." (".$countSummary['nonzero']." non-zero), Reward count: ".$countSummary['rewards'].", Payout count: ".$countSummary['payouts']."</div>\n";
					//------------------------
					if ($hasDeletePriv) 
					{
						//------------------------
						echo "<hr/><br/>\n";
						echo "<h4>Destructive Privileged Actions:</h4>\n";
						echo "<p>Selected Round: $rid</p>\n";
						//------------------------
						echo "<input type='submit' name='action' value='Delete' /><br/>\n";
						echo "<input type='submit' name='action' value='Unpay Round Payouts' />&nbsp;<input id='upsp' type='checkbox' name='upsp' checked='checked' />Including special (forfeit/store pay)<br/>\n";
						echo "<input type='submit' name='action' value='Delete Round All Data' />&nbsp;(Payouts/Contributions)&nbsp;<input id='dds' type='checkbox' name='dds' checked='checked' />Including Stats<br/>\n";
						echo "<input type='submit' name='action' value='UnAdd Round Data Stats' /><br/>\n";
						echo "<hr/><br/>\n";
						echo "<h4>Round Flags:</h4>\n";
						echo "<p>(Options)</p>\n";
						echo "<input type='submit' name='action' value='".($rFlagStorepay ? "Disable Stored Rewards" : "Enable Stored Rewards")."' /><br>\n";
						echo "<input type='submit' name='action' value='".($rFlagForfeit ? "Disable Store Forfeit" : "Enable Store Forfeit")."' /><br>\n";
						echo "<p>(State)</p>\n";
						echo "<input type='submit' name='action' value='".($rActive ? "Unmark Active" : "Mark Active")."' /><br>\n";
						echo "<input type='submit' name='action' value='".($rDone ? "Unmark Done" : "Mark Done")."' /><br>\n";
						echo "<p>(Debug and Testing)</p>\n";
						echo "<input type='submit' name='action' value='".($rIsHidden ? "Unmark Hidden" : "Mark Hidden")."' /><br>\n";
						echo "<input type='submit' name='action' value='".($rIsDryRun ? "Unmark Dry Run" : "Mark Dry Run")."' /><br>\n";
						//------------------------
						echo "<input style='margin-top: 8px; width:80px;' id='sbal' type='text' name='sbal' value='$rTotPay' maxlength='12' />&nbsp;\n";
						echo "<input type='submit' name='action' value='".($rIsTestBal ? "Clear Test Balance" : "Set Test Balance")."' /><br>\n";
						//------------------------

						echo "<hr/><br/>\n";
						echo "<h4>Manual Recovery and Testing:</h4>\n";
						echo "<p>(advanced)</p>\n";
						//------------------------
						echo "<label class='loginLabel' for='state'>Set Round Step:</label>&nbsp;";
						$actionNames = $Rounds->getActionTextNames();
						$actionStack = $Rounds->getRoundActionStack();
						if ($actionStack === false)
						{
							XLogError("Rounds Admin - Rounds getRoundActionStack failed");
							echo "<label class='loginLabel' for='step'>Set Round Step:</label>&nbsp;";
							echo "<input style='margin-top: 8px; width:80px;' id='step' type='text' name='step' value='$rStep' maxlength='2' ".(!$hasDeletePriv ? "disabled='disabled' " : "")." />\n";
						}
						else
						{
							echo "<select name='step' id='step'".(!$hasDeletePriv ? " disabled='disabled' " : "").">\n";
							for ($step = 0;$step < sizeof($actionStack) + 1;$step++)
							{
								$action = $Rounds->getActionFromStep($step, $actionStack);
								if ($action === false)
								{
									XLogError("Rounds Admin - Rounds getActionFromStep failed, step ".XVarDump($step).", actionStack ".XVarDump($actionStack));
									$actionName = XEncodeHTML("<Error>");
								}
								else
									$actionName = XEncodeHTML( (isset($actionNames[$action]) ? $actionNames[$action] : "(unknown action ID $action)") );

								echo "<option value='$step'".($rid !== false && $rStep == $step ? " selected='selected'" : "").">($step) $actionName</option>\n";
							}
							echo "</select>\n";
						}
						//------------------------
						echo "<input type='submit' name='action' value='Set Step' />\n";
						echo "<input id='clrprogress' type='checkbox' name='clrprogress' />Clear progress<br/><br/>\n";
						//------------------------
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Check for Worker Duplicates' /><br/><br/>\n";
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Reparse FahAppClient Data From File' /><br/>\n";
						echo "(expeced at .../log/archive/fah_stats_round_X_team_X_[date].txt.bz2)<br/>\n";
						echo "<input id='rpgrep' type='checkbox' name='rpgrep' />Full daily user summary (needs grepped)<br/>\n";
						echo "<div><input id='rporder' type='radio' name='rporder' value='oldest' checked='checked' /> Oldest <input id='rporder' type='radio' name='rporder' value='newest' /> Newest</div><br/>\n";
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Reparse FahClient Data From File' /> expected at ./(admin)/reparse_fahclient_data.json<br/>\n";
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Reparse FahClient CheckStats' /><br/>\n";
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Reparse Request Payouts' /><br/>\n";
						echo "&nbsp;&nbsp;<input type='submit' name='action' value='Reparse Send Payout' /><br/>\n";
						//------------------------
					}
					//------------------------
				?>
				<br/>
		<?php
		} // if ($rid !== false)
		?>
		<br/>
		<br/>
		<div class="admin-edit" style="width:420px;"><!-- New Round -->
		<?php 
					if ($showDetails) echo "<input type='hidden' name='det' value='1' />\n";
					if ($showZeroDetails) echo "<input type='hidden' name='zdet' value='1' />\n";
					if ($Login->HasPrivilege(PRIV_ROUND_ADD)) 
					{
						echo "<h3>New round:</h3>\n";
						echo "<p>(Options)</p>\n";
						echo "<input id='nrfsv' type='checkbox' name='nrfsv'".(XMaskContains($defRoundFlags, ROUND_FLAG_STORE_VALUES) ? " checked='checked'" : "")." />Stored Values<br/>\n";
						echo "<input id='nrffs' type='checkbox' name='nrffs'".(XMaskContains($defRoundFlags, ROUND_FLAG_FORFEIT_STORE) ? " checked='checked'" : "")." />Forfeit Stored<br/>\n";
						echo "<p>(Initial State)</p>\n";
						echo "<input id='nrfact' type='checkbox' name='nrfact'".(XMaskContains($defRoundFlags, ROUND_FLAG_ACTIVE) ? " checked='checked'" : "")." />Start Active<br/>\n";
						echo "<p>(Debug and Testing)</p>\n";
						echo "<input id='nrfht' type='checkbox' name='nrfht'".(XMaskContains($defRoundFlags, ROUND_FLAG_HIDDEN) ? " checked='checked'" : "")." />Test, hidden from public<br/>\n";
						echo "<input id='nrfdr' type='checkbox' name='nrfdr'".(XMaskContains($defRoundFlags, ROUND_FLAG_DRYRUN) ? " checked='checked'" : "")." />Faked dry run (no Payout)<br/>\n";
						echo "<input id='stauto' type='checkbox' name='stauto' checked='checked' />Start Round Automation<br/>\n";
						echo "Team ID:&nbsp;<input style='width:100px;' id='teamid' type='text' name='teamid' value='".XEncodeHTML($defTeamId)."' maxlength='12' /><br/>";
						echo "<input type='submit' name='action' value='Add' />&nbsp;<input type='submit' name='action' value='Save As Default' /><br/>";
					}
					if ($Login->HasPrivilege(PRIV_DELETE_HISTORY)) 
					{
						echo "<hr/>\n";
						echo "<label class='loginLabel' for='nridx'>Force Next Round Index:</label>&nbsp;";
						echo "<input style='margin-top:8px;width:80px;' id='nridx' type='text' name='nridx' value='$nridx' maxlength='6' />\n";
						echo "<input type='submit' name='action' value='Force Next Index' />\n";
					}
					?>
				<br/>
				<br/>
		</div><!-- New Round -->
		<br/>
			</fieldset>
		</form>
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->
