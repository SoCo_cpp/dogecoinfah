<?php
//---------------------------------
/*
 * include/pages/admin/Automation.php
 * 
 * 
*/
//---------------------------------
global $Login;
$Automation = new Automation() or die("Create object failed");
$Config = new Config() or die("Create object failed");
$Display = new Display() or die("Create object failed");
$pagenum = XGetPost('p');
$action = XPost('action');
//---------------------------------
function getRow($canManage, $idx, $name, $active, $defRate, $strLastRan, $strCurRate)
{
	//------------------------
	$col = array();
	//------------------------
	$col[] = $name; // Name
	$col[] = ($active ? "Active" : "Idle"); // Active
	//------------------------
	if ($canManage)
	{
		//------------------------
		$action = ($active ? "Stop" : "Start");
		//------------------------
		$btnExec = "<button style='padding:3px;margin:3px;' name='action' value='Exec_$idx' type='submit'>Execute</button>";
		$btnSS = "<button style='padding:3px;margin:3px;' name='action' value='".$action."_$idx' type='submit'>$action</button>";
		$col[] = $btnExec." ".$btnSS; // Control
		//------------------------
	}
	else
	{
		//------------------------
		$col[] = "&nbsp;"; // Control
		//------------------------
	}
	//------------------------
	if ($idx != 0)
	{
		$col[] = ($defRate === false ? "&nbsp;" : "<div style='padding:1px;text-align:center;'><input style='padding: 1px; width: 35px;' id='Rate_$idx' type='text' name='Rate_$idx' value='".($active ? $strCurRate."' maxlength='5' disabled" : $defRate."' maxlength='3'")." /> minutes</div>"); // Rate
	}
	else  // handled specially
	{
		//------------------------
		$weekdays = array( 	array('*', '(any)'),
							array('0', 'Sunday'),
							array('1', 'Monday'),
							array('2', 'Tuesday'),
							array('3', 'Wednesday'),
							array('4', 'Thursday'),
							array('5', 'Friday'),
							array('6', 'Saturday')
						);
		//------------------------
		$Automation = new Automation() or die("Create object failed");
		$tz = $Automation->getTimeZoneName();
		if ($tz === false)
			$tz = "?";
		$tz = XEncodeHTML($tz);
		//------------------------
		$hourValue = ($active ? $strCurRate[0] : $defRate[0]);
		$minValue = ($active ? $strCurRate[1] : $defRate[1]);
		//------------------------
		$hourValue = str_pad($hourValue, 2, STR_PAD_LEFT);
		$minValue = str_pad($minValue, 2, STR_PAD_LEFT);
		//------------------------
		$str = "<div style='padding:1px;text-align:center;'><div style='margin: 1px;'>\n";
		$str .= "<input style='padding: 1px; width: 25px;' id='Hour_$idx' type='text' name='Hour_$idx' value='$hourValue' maxlength='2' />";
		$str .= ":<input style='padding: 1px; width: 25px;' id='Min_$idx' type='text' name='Min_$idx' value='$minValue' maxlength='2' />";
		$str .= "</div><div>24&nbsp;hour&nbsp;-&nbsp;$tz</div>";
		//------------------------
		$str .= "<div>Day&nbsp;<select id='Weekday_$idx' name='Weekday_$idx'".($active || !$canManage ? " disabled" : "").">\n";
		//------------------------
		foreach ($weekdays as $day)
		{
			$str .= "<option value='$day[0]'";
			if ( ($active && $strCurRate[2] == $day[0]) || (!$active && $defRate[2] == $day[0]) )
				$str .= " selected";
			$str .= ">$day[1]</option>\n";
		}
		//------------------------
		$str .= "</select></div></div>\n";
		//------------------------
		$col[] = $str;
		//------------------------
	}
	//------------------------
	$col[] = ($strLastRan === false ? "&nbsp;" : $strLastRan); // Last Ran
	//------------------------
	$out = "<tr class='admin-row'>";
	//------------------------
	foreach ($col as $c)
		$out .= "<td>$c</td>\n";
	//------------------------
	$out .= "</tr>\n";
	//------------------------
	return $out;
}
//---------------------------------
?>
<div id="content"><!-- Start Container 1 -->
	<div class="under pad_bot1"><!-- Start Container 2 -->
	<br/>
	<div class="line1 wrapper pad_bot2"><!-- Start Container 3 -->
		<div class="col1"  style="width: 720px;"><!-- Start Left Column -->
		<div>
			<h1>Automation:</h1>
			<p>
			Folding @ Home automation.
			</p>
			<br/>
		</div>
		<?php
			//------------------------
			if ($action == "Start_0") // New Round - Start
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$hour = XPost('Hour_0');
					$min = XPost('Min_0');
					$weekday = XPost('Weekday_0');
					//------------------------
					if (!is_numeric($hour) || !is_numeric($min) || $hour < 0 || $hour >= 24 || $min < 0 || $min >= 60 || ($weekday != '*' && !is_numeric($weekday)))
					{
						XLogNotify("Admin Automation hour/min is invalid, hour: ".XVarDump($hour).", min: ".XVarDump($min).", weekday: ".XVarDump($weekday));
						echo "Start automation failed. Rate 'at time' is invalid.<br/>";
					}
					else
					{
						//------------------------
						$hour = (int)$hour; // try to trim preceding zeros, ect
						$min = (int)$min; // try to trim preceding zeros, ect
						if ($weekday != '*')
							$weekday = (int)$weekday; // try to trim preceding zeros, ect
						//------------------------
						if (!$Automation->StartNewRoundPage($min, $hour, $weekday))
						{
							XLogNotify("Admin Automation Automation StartNewRoundPage failed new round");
							echo "Start new round automation failed.<br/>";
						}
						else
						{
							XLogNotify("Admin Automation started new round Automation");
							echo "Start new round automation succeeded.<br/>";
						}
						//------------------------
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation start new round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Start new round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Stop_0") // New Round - Stop
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Automation->StopNewRoundPage())
					{
						XLogNotify("Admin Automation Automation StopNewRoundPage failed new round");
						echo "Stop new round automation failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation stopped new round Automation");
						echo "Stop new round automation succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation stop new round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Stop new round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Exec_0") // New Round - Exec
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$result = $Automation->ExecuteNewRoundPage();
					if ($result === false)
					{
						XLogNotify("Admin Automation Automation ExecuteNewRoundPage failed new round");
						echo "Execute new round failed.<br/>";
					}
					else
					{
						$value = $result[0];
						$output = $result[1];
						XLogNotify("Admin Automation executed new round result value: ".XVarDump($value).", output: ".XVarDump($output));
						echo "Execute new round ".($value === 0 ? "succeeded" : "failed");
						if (sizeof($output) == 0)
							echo " with no output.<br/>\n";
						else
						{
							echo " with output:<br/>\n";
							echo "<div class='admin-rawtext'>\n";
							echo join("<br/>", $output);
							echo "</div>\n";
						}
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation execute new round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Execute new round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Start_1") // Round Check - Start
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$rate = XPost('Rate_1');
					if (!is_numeric($rate))
					{
						XLogNotify("Admin Automation rate is invalid");
						echo "Start automation failed. Rate is invalid.<br/>";
					}
					else 
					{
						//------------------------
						if (!$Automation->StartRoundCheckPage($rate))
						{
							XLogNotify("Admin Automation Automation StartRoundCheckPage failed");
							echo "Start monitor round automation failed.<br/>";
						}
						else
						{
							XLogNotify("Admin Automation started monitor round Automation");
							echo "Start monitor round automation succeeded.<br/>";
						}
						//------------------------
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation start monitor round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Start monitor round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Stop_1") // Round Check - Stop
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Automation->StopRoundCheckPage())
					{
						XLogNotify("Admin Automation Automation StopRoundCheckPage failed monitor round");
						echo "Stop monitor round automation failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation stopped monitor round Automation");
						echo "Stop monitor round automation succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation stop monitor round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Stop monitor round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Exec_1") // Round Check - Exec
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$timer = new XTimer();
					$result = $Automation->ExecuteRoundCheckPage();
					if ($result === false)
					{
						XLogNotify("Admin Automation Automation ExecuteRoundCheckPage failed monitor round");
						echo "Execute monitor round failed.<br/>";
					}
					else
					{
						$value = $result[0];
						$output = $result[1];
						XLogNotify("Admin Automation executed monitor round result value: ".XVarDump($value).", output: ".XVarDump($output));
						echo "Execute monitor round took ".$timer->restartMs(true)." and ".($value === 0 ? "succeeded" : "failed");
						if (sizeof($output) == 0)
							echo " with no output.<br/>\n";
						else
						{
							echo " with output:<br/>\n";
							echo "<div class='admin-rawtext'>\n";
							echo join("<br/>", $output);
							echo "</div>\n";
						}
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation execute monitor round doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Execute monitor round automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			//------------------------
			else if ($action == "Exec_2") // Public Data - Exec
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$timer = new XTimer();
					$result = $Automation->ExecutePublicData();
					if ($result === false)
					{
						XLogNotify("Admin Automation Automation ExecutePublicData failed public data");
						echo "Execute public data failed.<br/>";
					}
					else
					{
						$value = $result[0];
						$output = $result[1];
						XLogNotify("Admin Automation executed public data result value: ".XVarDump($value).", output: ".XVarDump($output));
						echo "Execute public data took ".$timer->restartMs(true)." and ".($value === 0 ? "succeeded" : "failed");
						if (sizeof($output) == 0)
							echo " with no output.<br/>\n";
						else
						{
							echo " with output:<br/>\n";
							echo "<div class='admin-rawtext'>\n";
							echo join("<br/>", $output);
							echo "</div>\n";
						}
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation execute public data automation doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Execute public data automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Start_2")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					$rate = XPost('Rate_2');
					if (!is_numeric($rate))
					{
						XLogNotify("Admin Automation rate is invalid");
						echo "Start automation failed. Rate is invalid.<br/>";
					}
					else 
					{
						//------------------------
						if (!$Automation->StartPublicData($rate))
						{
							XLogNotify("Admin Automation Automation StartPublicData failed public data");
							echo "Start public data automation failed.<br/>";
						}
						else
						{
							XLogNotify("Admin Automation started public data Automation");
							echo "Start public data automation succeeded.<br/>";
						}
						//------------------------
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation start public data automation doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Start public data automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "Stop_2")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Automation->StopPublicData())
					{
						XLogNotify("Admin Automation Automation StopPublicData failed public data");
						echo "Stop public data automation failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation stopped public data Automation");
						echo "Stop public data automation succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation Automation stop public data automation doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Stop public data automation failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "unlock_auto")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_PUBLIC_AUTOMATION_LOCK))
					{
						XLogNotify("Admin Automation Config Clear public data automation lock failed");
						echo "Clear automation lock failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation cleared public data automation lock");
						echo "Clear public data automation lock succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation clear automation lock doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Clear public data automation lock failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "rebuild_data")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->ClearMany(array(CFG_PUBLIC_STEP, CFG_PUBLIC_SUBSTEP, CFG_PUBLIC_LAST_ROUND, CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX, CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX)))
					{
						XLogNotify("Admin Automation Config ClearMany to force rebuild public data failed");
						echo "Force rebuild public data failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation forced public data to rebuild");
						echo "Force rebuild public data succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation force rebuild public data doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Force rebuild public data failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "force_balance")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_PUBLIC_LAST_BALANCE_UPDATE))
					{
						XLogNotify("Admin Automation Config Clear to force balance update public data failed");
						echo "Force balance update public data failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation forced balance update public data");
						echo "Force balance update public data succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation force balance update public data doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Force balance update public data failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "clear_poll_fails")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_FAH_APP_POLL_FAILS))
					{
						XLogNotify("Admin Automation Config Clear public data poll fails failed");
						echo "Clear public data poll fails failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation cleared public data poll fails");
						echo "Clear public data poll fails succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation clear public data poll fails doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Clear update public poll fails failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "clear_round_poll")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_LAST_FAH_APP_POLL))
					{
						XLogNotify("Admin Automation Config Clear public data round poll failed");
						echo "Clear public data round poll failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation cleared public data round poll");
						echo "Clear public data round poll succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation clear public data round poll doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Clear update public round poll failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "clear_stat_poll")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_LAST_FAH_APP_STATS_POLL))
					{
						XLogNotify("Admin Automation Config Clear public data stat poll failed");
						echo "Clear public data stat poll failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation cleared public data stat poll");
						echo "Clear public data stat poll succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation clear public data stat poll doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Clear update public stat poll failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			else if ($action == "clear_team_poll")
			{
				//------------------------
				if ($Login->HasPrivilege(PRIV_AUTO_MANAGE))
				{
					//------------------------
					if (!$Config->Clear(CFG_PUBLIC_LAST_POLL_TEAM))
					{
						XLogNotify("Admin Automation Config Clear public data team poll failed");
						echo "Clear public data team poll failed.<br/>";
					}
					else
					{
						XLogNotify("Admin Automation cleared public data team poll");
						echo "Clear public data team poll succeeded.<br/>";
					}
					//------------------------
				}
				else
				{
					XLogNotify("Admin Automation clear public data team poll doesn't have proper privileges: ($Login->User) $Login->UserName");
					echo "Clear update public team poll failed. Action requires proper privileges.<br/>";
				}
				//------------------------
			}
			//------------------------
			$cmdDateFormat = '%Y-%m-%d %H:%M:%S %Z';
			$reply = array();
			//------------------------
			$retCode = XExecSimple("date +'$cmdDateFormat'", $reply);
			if ($retCode === false)
				$serverDate = "(exec failed)";
			else if ($retCode != 0)
				$serverDate = "(date failed with code: ".XVarDump($retCode).")";
			else if (sizeof($reply) != 1)
			{
				$serverDate = "(expected one line reply, but got ".sizeof($reply)." lines)";
				XLogWarn("Admin Automation date exec replied with wrong line count: ".XVarDump($reply));
			}
			else $serverDate = $reply[0];
			//------------------------
			$dtFormat = MYSQL_DATETIME_FORMAT.' T';
			$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
			$strUtc = $nowUtc->format($dtFormat);
			$displayDate = $Display->htmlLocalDate($nowUtc, $dtFormat);
			//------------------------
			echo "<style>\n#dtServer {\n\nwidth:300px;margin-left:5px;\nborder:1px solid black;}\n#dtServer p {\nborder-bottom:1px solid black;\n}\n#dtServer table tr td {\nfont-size:80%;\npadding: 0px 2px;\n}\n #dtServer td:first-child {\padding-left:10px;\ntext-align:right;\n}\n</style>\n";
			//------------------------
			echo "<div>\n";
			//------------------------
			echo "<div id='dtServer' name='dtServer'><p>Server date/time (at page load):</p>\n";
			echo "<table><tr><td>Native:</td><td>$serverDate</td></tr>\n";
			echo "<tr><td>UTC:</td><td>$strUtc</td></tr>\n";
			echo "<tr><td>Local:</td><td>$displayDate</td></tr>\n";
			echo "</table></div>\n";
			//------------------------
			echo "<form action='./Admin.php' method='post' enctype='multipart/form-data'>\n";
			echo "<fieldset class='loginBox'>\n";
			PrintSecTokenInput();
			echo "<input type='hidden' name='p' value='$pagenum' />\n";
			echo "<div class='admin-edit' id='PubData' name='PubData'>\n";
			//------------------------
			$val = $Config->GetMany(array(	CFG_PUBLIC_AUTOMATION_LOCK,
											CFG_PUBLIC_STEP, CFG_PUBLIC_SUBSTEP,
											CFG_AUTO_LAST_PUBDATA, CFG_AUTO_PUBDATA_CURRENT_RATE, CFG_AUTO_PUBDATA_START_RATE,
											CFG_PUBLIC_LAST_BALANCE_UPDATE,
											CFG_PUBLIC_LAST_ROUND, CFG_ROUND_LAST_DONE,
											CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX, CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX,
											CFG_PUBLIC_LAST_POLL_TEAM, CFG_PUBLIC_POLL_TEAM_RATE,
											CFG_FAH_APP_POLL_FAILS, CFG_LAST_FAH_APP_POLL, CFG_LAST_FAH_APP_STATS_POLL,
											CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA,
											CFG_PUBLIC_STEPS_ENABLED, CFG_PUBLIC_IDLE_STEPS_ENABLED
											), false);
			//------------------------
			if ($val === false)
			{
				XLogNotify("Admin Automation Config GetMany (PublicData) failed");
				echo "<p>Database failure. Get Public Data status failed.</p>";
			}
			else
			{
				//------------------------
				$canManage = $Login->HasPrivilege(PRIV_AUTO_MANAGE);
				//------------------------
				$dtLock = $val[CFG_PUBLIC_AUTOMATION_LOCK];
				if ($dtLock === false || $dtLock === '')
					$strAutoLock = false;
				else
				{
					$staleAge = PUBLIC_STALE_MUTEX_AGE;
					$diff = XDateTimeDiff($dtLock, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
					$strAutoLock = '<p>Automation locked '.$Display->htmlLocalDate($dtLock).' ('.round($diff, 1).' mins';
					if (is_numeric($staleAge))
						$strAutoLock .= ' / '.$staleAge.' until stale';
					$strAutoLock .= ")";
					if ($canManage)
						$strAutoLock .=" <button style='padding:3px;margin:3px;' name='action' value='unlock_auto' type='submit'>Force Unlocked</button>";
					$strAutoLock .= "</p>\n";
				}
				//------------------------
				$pubStep = (is_numeric($val[CFG_PUBLIC_STEP]) ? $val[CFG_PUBLIC_STEP] : 0);
				$pubSubStep = (is_numeric($val[CFG_PUBLIC_SUBSTEP]) ? $val[CFG_PUBLIC_SUBSTEP] : 0);
				//------------------------
				$pdStepNames = array(   PUBDATA_STEP_NONE  => 'None',
										PUBDATA_STEP_START => 'Start',
										PUBDATA_STEP_TXIDLIST => 'Transaction IDs',
										PUBDATA_STEP_WORKERLIST => 'Folder Names',
										PUBDATA_STEP_ROUNDCHART => 'History Chart',
										PUBDATA_STEP_ROUNDHISTORY => 'History Table',
										PUBDATA_STEP_WORKERDATA => 'Folder History',
										PUBDATA_STEP_DONE => 'Done');
				//------------------------
				$pdSubStepNames = array(   	PUBDATA_IDLE_SUBSTEP_NONE  => 'None',
											PUBDATA_IDLE_SUBSTEP_START => 'Start',
											PUBDATA_IDLE_SUBSTEP_TEAMSUMMARY => 'Team Summary',
											PUBDATA_IDLE_SUBSTEP_TEAMDATA => 'Team Data',
											PUBDATA_IDLE_SUBSTEP_CALCTEAMSTATS => 'Calc Team Stats',
											PUBDATA_IDLE_SUBSTEP_DONE => 'Done');
				//------------------------
				$strStep = '<p>Step ('.$pubStep.') '.(isset($pdStepNames[$pubStep]) ? $pdStepNames[$pubStep] : '[Unknown]').' / ';
				$strStep .= ($pubStep == PUBDATA_STEP_WORKERDATA ? 'block step '.$pubSubStep : '('.$pubSubStep.') '.(isset($pdSubStepNames[$pubSubStep]) ? $pdSubStepNames[$pubSubStep] : '[Unknown]'));
				$strStep .= "</p>\n";
				//------------------------
				$lastDate = ($val[CFG_AUTO_LAST_PUBDATA] !== false && $val[CFG_AUTO_LAST_PUBDATA] !== '' ? $Display->htmlLocalDate($val[CFG_AUTO_LAST_PUBDATA]) : '[Not set]');
				$curRate = (is_numeric($val[CFG_AUTO_PUBDATA_CURRENT_RATE]) ? $val[CFG_AUTO_PUBDATA_CURRENT_RATE] : 0);
				$stRate = (is_numeric($val[CFG_AUTO_PUBDATA_START_RATE]) ? $val[CFG_AUTO_PUBDATA_START_RATE] : 0);
				$strLastAuto = "<p>Last checked: $lastDate @ $curRate ($stRate) mins</p>\n";
				//------------------------
				$stepsEnabledMask = $val[CFG_PUBLIC_STEPS_ENABLED];
				$idleStepsEnabledMask = $val[CFG_PUBLIC_IDLE_STEPS_ENABLED];
				//------------------------
				$strStepsDisabled = "";
				if (!is_numeric($stepsEnabledMask) || $stepsEnabledMask < 0)
					$strStepsDisabled .= "<p>Steps disabled: [value invalid] (see ".CFG_PUBLIC_STEPS_ENABLED.")</p>\n";
				else
				{
					$strDisabled = "";
					for ($s = PUBDATA_STEP_NONE + 1;$s < PUBDATA_STEP_DONE;$s++)
						if ( ($stepsEnabledMask & (1 << $s)) == 0)
							$strDisabled .= " ($s) ".(isset($pdStepNames[$s]) ? $pdStepNames[$s] : "[Unknown]").",";
					if ($strDisabled != "")
						$strStepsDisabled .= "<p>Steps disabled:$strDisabled</p><p>&nbsp;(see ".CFG_PUBLIC_STEPS_ENABLED.")</p>";
				}
				//--------
				if (!is_numeric($idleStepsEnabledMask) || $idleStepsEnabledMask < 0)
					$strStepsDisabled .= "<p>Idle steps disabled: [value invalid] (see ".CFG_PUBLIC_IDLE_STEPS_ENABLED.")</p>\n";
				else
				{
					$strDisabled = "";
					for ($s = PUBDATA_IDLE_SUBSTEP_NONE + 1;$s < PUBDATA_IDLE_SUBSTEP_DONE;$s++)
						if ( ($idleStepsEnabledMask & (1 << $s)) == 0)
							$strDisabled .= " ($s) ".(isset($pdSubStepNames[$s]) ? $pdSubStepNames[$s] : "[Unknown]").",";
					if ($strDisabled != "")
						$strStepsDisabled .= "<p>Idle steps disabled:$strDisabled</p><p>&nbsp;(see ".CFG_PUBLIC_IDLE_STEPS_ENABLED.")</p>\n";
				}
				//------------------------
				$dt = $val[CFG_PUBLIC_LAST_BALANCE_UPDATE];
				if ($dt !== false && $dt != '')
				{
					$diff = round(XDateTimeDiff($dt, false/*now*/, false/*UTC*/, 'n'/*minutes*/), 1);
					$dt = $Display->htmlLocalDate($dt);
				}
				else $dt = $diff = '[Not set]';
				$rate = (is_numeric(PUBLIC_BALANCE_UPDATE_RATE) ? PUBLIC_BALANCE_UPDATE_RATE : '[Not set]');
				$strLastBalance = "<p>Balance updated: $dt @ $diff / $rate mins";
				if ($canManage)
					$strLastBalance .=" <button style='padding:3px;margin:3px;' name='action' value='force_balance' type='submit'>Update</button>";
				$strLastBalance .= "</p>\n";
				//------------------------
				$lastRoundID = (is_numeric($val[CFG_ROUND_LAST_DONE]) ? $val[CFG_ROUND_LAST_DONE] : '[Not set]');
				$strRoundProcessed = '<p>Processed round '.(is_numeric($val[CFG_PUBLIC_LAST_ROUND]) ? $val[CFG_PUBLIC_LAST_ROUND] : '[Not set]')." / $lastRoundID</p>\n";
				//------------------------
				if ($pubStep != PUBDATA_STEP_ROUNDHISTORY)
					$strRoundHistory = false;
				else
				{
					$strRoundHistory = '<p>Round History Progress: block '.(is_numeric($val[CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX]) ? $val[CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX] : '[Not set]');
					$strRoundHistory .= ' (round id '.(is_numeric($val[CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX]) ? $val[CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX] : '[Not set]')." / $lastRoundID)</p>\n";
				}
				//------------------------
				$fails = (is_numeric($val[CFG_FAH_APP_POLL_FAILS]) ? $val[CFG_FAH_APP_POLL_FAILS] : 0);
				$rate = (is_numeric(FAH_APP_POLL_RATE_LIMIT) ? FAH_APP_POLL_RATE_LIMIT : 'Not set]');
				$dtApp = $val[CFG_LAST_FAH_APP_POLL];
				$dtStat = $val[CFG_LAST_FAH_APP_STATS_POLL];
				$dtTeam = $val[CFG_PUBLIC_LAST_POLL_TEAM];
				$rateTeam = (is_numeric($val[CFG_PUBLIC_POLL_TEAM_RATE]) ? $val[CFG_PUBLIC_POLL_TEAM_RATE] : '[Not set]');
				if ($dtApp !== false && $dtApp != '')
				{
					$diffApp = round(XDateTimeDiff($dtApp, false/*now*/, false/*UTC*/, 'n'/*minutes*/));
					$dtApp = $Display->htmlLocalDate($dtApp);
				}
				else $dtApp = $diffApp = '[Not set]';
				if ($dtStat !== false && $dtStat != '')
				{
					$diffStat = round(XDateTimeDiff($dtStat, false/*now*/, false/*UTC*/, 'n'/*minutes*/));
					$dtStat = $Display->htmlLocalDate($dtStat);
				}
				else $dtStat = $diffStat = '[Not set]';
				if ($dtTeam !== false && $dtTeam != '')
				{
					$diffTeam = round(XDateTimeDiff($dtTeam, false/*now*/, false/*UTC*/, 'n'/*minutes*/));
					$dtTeam = $Display->htmlLocalDate($dtTeam);
				}
				else $dtTeam = $diffTeam = '[Not set]';
				//------------------------
				$strFahPoll = '';
				if ($fails != 0)
				{
					$strFahPoll .= "<p>Fah app poll fail count $fails / 2";
					if ($canManage)
						$strFahPoll .= " <button style='padding:3px;margin:3px;' name='action' value='clear_poll_fails' type='submit'>Clear Fails</button>";
					$strFahPoll .= "</p>\n";
				}
				$strFahPoll .= "<p>Last round fah app poll: $dtApp</p>";
				$strFahPoll .= "<p style='margin-left:100px;'>($diffApp mins / minimum $rate mins)";
				if ($canManage)
					$strFahPoll .= " <button style='padding:3px;margin:3px;' name='action' value='clear_round_poll' type='submit'>Clear</button>";
				$strFahPoll .= "</p>\n";
				$strFahPoll .= "<p>Last pub data fah app poll: $dtStat</p>";
				$strFahPoll .= "<p style='margin-left:100px;'>($diffStat mins / minimum $rate mins)";
				if ($canManage)
					$strFahPoll .= " <button style='padding:3px;margin:3px;' name='action' value='clear_stat_poll' type='submit'>Clear</button>";
				$strFahPoll .= "</p>\n";
				$strFahPoll .= "<p>Last team poll: $dtTeam</p>";
				$strFahPoll .= "<p style='margin-left:100px;'>($diffTeam mins / minimum $rateTeam mins)";
				if ($canManage)
					$strFahPoll .= " <button style='padding:3px;margin:3px;' name='action' value='clear_team_poll' type='submit'>Clear</button>";
				$strFahPoll .= "</p>\n";
				//------------------------
				$strWithoutMarket = "<p>Without market data: ".(isset($val[CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA]) && is_numeric($val[CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA]) ? $val[CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA] : '[Not set]')."</p>";
				//------------------------
				echo "<legend style='font-weight:bold;font-size:120%;margin-bottom:4px;'>Public Data Status:</legend>\n";
				if ($canManage)
					echo "<button style='padding:3px;margin:5px;' name='action' value='rebuild_data' type='submit'>Rebuild All Data</button>";
				echo $strWithoutMarket;
				echo $strLastAuto;
				if ($strAutoLock !== false)
					echo $strAutoLock;
				echo $strStep;
				echo $strStepsDisabled;
				echo $strRoundProcessed;
				echo $strLastBalance;
				if ($strRoundHistory !== false)
					echo $strRoundHistory;
				echo $strFahPoll;
				//------------------------
				//------------------------
			}
			//------------------------
			echo "</div>\n";
			echo "</fieldset></form>\n";
			//------------------------
			echo "</div>\n";
			//------------------------
		?>
		<form action="./Admin.php" method="post" enctype="multipart/form-data">
		<fieldset class="loginBox">
			<?php PrintSecTokenInput(); ?>
			<input type="hidden" name="p" value="<?php echo $pagenum; ?>"/><!-- Page -->
			<table class='admin-table'>
			<tr><th style='width:57px;'>Name</th><th style='width:45px;'>Active</th><th style='width:85px;'>Control</th><th style='width:100px;'>Rate</th><th style='width:100px;'>Last ran</th></tr>
			<?php
				//------------------------
				$pages = array(AUTOMATION_PAGE_NEWROUND, AUTOMATION_PAGE_ROUND, AUTOMATION_PAGE_PUBLIC_DATA);
				$pagesActive = $Automation->hasCrontabs($pages);
				if ($pagesActive === false)
				{
					XLogNotify("Admin Automation Automation hasCrontabs failed");
					echo "<tr class='admin-row'>Error<td><td>Error</td><td>Error</td><td>Error</td><td>Error</td><td>Error</td></tr>\n";
				}
				else
				{
					//------------------------	
					$canManage = $Login->HasPrivilege(PRIV_AUTO_MANAGE);
					//------------------------	
					$rateH = $Config->Get(CFG_AUTO_NEWROUND_RATE_H, false);
					$rateM = $Config->Get(CFG_AUTO_NEWROUND_RATE_M, false);
					$rateW = $Config->Get(CFG_AUTO_NEWROUND_RATE_W, false);
					//------------------------
					if (!is_numeric($rateH)) $rateH = 22; // 9pm (24 hour)
					if (!is_numeric($rateM)) $rateM = 0; // 00
					if (!is_numeric($rateW)) $rateW = 6; // Saturday
					//------------------------
					$autos = array();
					//------------------------
					$autos[] = array(	'idx'	=> 0,
										'name'	=> "New Round",
										'last' 	=> CFG_AUTO_LAST_NEWROUND,
										'cur'	=> array($rateH, $rateM, $rateW), // handled specially
										'start'	=> false, 
										'drate' => array( 22 /*hour 9pm*/, 0 /*min*/, 6 /*week saturday*/) // handled specially
										);
					//------------------------
					$autos[] = array(	'idx'	=> 1,
										'name'	=> "Round Action",
										'last' 	=> CFG_AUTO_LAST_ROUND,
										'cur'	=> CFG_AUTO_ROUND_CURRENT_RATE,
										'start'	=> CFG_AUTO_ROUND_START_RATE,
										'drate' => 1
										);
					//------------------------
					$autos[] = array(	'idx'	=> 2,
										'name'	=> "Public Data Update",
										'last' 	=> CFG_AUTO_LAST_PUBDATA,
										'cur'	=> CFG_AUTO_PUBDATA_CURRENT_RATE,
										'start'	=> CFG_AUTO_PUBDATA_START_RATE,
										'drate' => 1
										);
					//------------------------
					foreach ($autos as $auto)
					{
						//------------------------
						if ($auto['cur'] !== false && $auto['start'] !== false)
						{
							//------------------------
							$curRate = $Config->Get($auto['cur']);
							$startRate = $Config->Get($auto['start']);
							//------------------------
							if ($curRate === false || $startRate === false)
								$strCurRate = false;
							else 
								$strCurRate = ($curRate != $startRate ? "$curRate ($startRate)" : $startRate);
							//------------------------
						}
						else if ($auto['idx'] == 0) // handled specially
							$strCurRate = $auto['cur'];
						else
							$strCurRate = false;
						//------------------------
						$lastRan = $Config->Get($auto['last']);
						if ($lastRan !== false)
							$lastRan = $Display->htmlLocalDate($lastRan);
						//------------------------
						echo getRow($canManage, $auto['idx'], $auto['name'], $pagesActive[$auto['idx']], $auto['drate'], $lastRan, $strCurRate);
						//------------------------
					}

					//------------------------	
				}
				echo "</table>\n";		
				//------------------------	
			?>
			</fieldset>
		</form>
		<br/>
		<div>
			<h2>Raw crontabs:</h2>
			<div>(minute hour day month week-day command)</div>
		</div>		
		<div class='admin-rawtext'>
		<?php
			//------------------------	
			$crontabsRaw = $Automation->getCrontabsRaw();
			if ($crontabsRaw === false)
			{
				XLogNotify("Admin Automation Automation getCrontabsRaw failed");
				echo "Failed to get raw cron tabs.";
			}
			else if (sizeof($crontabsRaw) == 0)
				echo "No crontabs.";
			else
				echo join("<br/>", $crontabsRaw);
			//------------------------	
		?>
		</div>
		</div><!-- End Left Column -->
	</div><!-- End Container 3 -->
	</div><!-- End Container 2 -->
</div><!-- End Container 1 -->

