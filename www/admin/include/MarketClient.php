<?php
//-------------------------------------------------------------
/*
*	MarketClient.php
*
* 
*/
//-------------------------------------------------------------
class MarketClient
{
	var $defaultProvider = false;
	var $defaultAsset = false;
	var $sessionCallCount = 0; // since this instance of the class was created
	//------------------
	function __construct($defaultProvider = false, $defaultAsset = false)
	{
		global $MarketConfig;
		$this->defaultProvider = ($defaultProvider !== false ? $defaultProvider : $MarketConfig['defaults']['provider']);
		$this->defaultAsset = ($defaultAsset !== false ? $defaultAsset : $MarketConfig['defaults']['asset']);
	}
	//------------------
	function getCurrentValue($cAsset, $cOtherAsset = false, $provider = false)
	{
		global $MarketConfig;
		if ($provider === false)
			$provider = $this->defaultProvider;
		if (!isset($MarketConfig['assets'][$cAsset]))
		{
			XLogError('MarketClient::getCurrentValue unknown asset ID: '.XVarDump($cAsset));
			return false;
		}
		if (!isset($MarketConfig['assets'][$cOtherAsset]))
		{
			XLogError('MarketClient::getCurrentValue unknown otherAsset ID: '.XVarDump($cOtherAsset));
			return false;
		}
		if ($provider == MARKET_CLIENT_PROVIDER_COINGECKO)
			return $this->getCurrentValue_coingecko($cAsset, $cOtherAsset);
		XLogError('MarketClient::getCurrentValue method for provider not found. provider: '.XVarDump($provider));
		return false;
	}
	//------------------
	function getHistoricValues($cAsset, $mixedDate, $provider = false)
	{
		global $MarketConfig;
		if ($provider === false)
			$provider = $this->defaultProvider;
		if (!isset($MarketConfig['assets'][$cAsset]))
		{
			XLogError('MarketClient::getHistoricValues unknown asset ID: '.XVarDump($cAsset));
			return false;
		}
		if ($provider == MARKET_CLIENT_PROVIDER_COINGECKO)
			return $this->getHistoricValues_coingecko($cAsset, $mixedDate);
		XLogError('MarketClient::getHistoricValues method for provider not found. provider: '.XVarDump($provider));
		return false;
	}
	//------------------
	function getCurrentValue_coingecko($cAsset, $cOtherAsset = MARKET_CLIENT_ASSET_BITCOIN)
	{
		global $MarketConfig;
		//------------------
		if (!isset($MarketConfig['assets'][$cAsset]['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO]))
		{
			XLogError('MarketClient::getCurrentValue_coingecko asset '.$cAsset.' not supported by provider coingecko ('.MARKET_CLIENT_PROVIDER_COINGECKO.')');
			return false;
		}
		//------------------
		if (!isset($MarketConfig['assets'][$cOtherAsset]['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO]))
		{
			XLogError('MarketClient::getCurrentValue_coingecko OtherAsset '.$cOtherAsset.' not supported by provider coingecko ('.MARKET_CLIENT_PROVIDER_COINGECKO.')');
			return false;
		}
		//------------------
		$otherAssetName = $MarketConfig['assets'][$cOtherAsset]['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO];
		//------------------
		$providerConfig = $MarketConfig['providers'][MARKET_CLIENT_PROVIDER_COINGECKO];
		$host = $providerConfig['host'];
		$api = $providerConfig['currentvalue'];
		$pathquery = str_replace('%ASSET_OTHER%', $otherAssetName, str_replace('%ASSET%', $assetName, $api));
		//------------------
		if ($this->sessionCallCount >= $providerConfig['rate_limit_per_minute'])
		{
			XLogWarn('MarketClient::getCurrentValue_coingecko per session rate limit exceeded');
			return false;
		}
		//------------------
		$opts = array('http'=>array('header'=>"Host: $host\r\nAccept: application/json\r\n"));
		//------------------
		$this->sessionCallCount++;
		//------------------
		$context = stream_context_create($opts);
		$httpReply = @file_get_contents('https://'.$host.$pathquery, false, $context);
		if ($httpReply === false)
		{
			XLogError('MarketClient::getCurrentValue_coingecko file_get_contents failed: '.XVarDump($http_response_header));
			return false;
		}
		//------------------
		$jsonReply = json_decode($httpReply, true);
		XLogDebug('MarketClient::getCurrentValue_coingecko httpReply:'."\r\n".XVarDump($httpReply, false)."\r\njsonReply: ".XVarDump($jsonReply, false));
		//------------------
		if (!is_array($jsonReply) || !isset($jsonReply[$assetName]))
		{
			XLogError('MarketClient::getCurrentValue_coingecko validate httpReply and coin asset failed:'."\r\n".XVarDump($jsonReply, false));
			return false;
		}
		//------------------
		if (!is_array($jsonReply[$assetName]) || !isset($jsonReply[$assetName][$otherAssetName]))
		{
			XLogError('MarketClient::getCurrentValue_coingecko validate httpReply other asset failed:'."\r\n".XVarDump($jsonReply, false));
			return false;
		}
		//------------------
		$value = $jsonReply[$assetName][$otherAssetName];
		//------------------
		return $value;
	}
	//------------------
	function getHistoricValues_coingecko($cAsset, $mixedDate)
	{
		global $MarketConfig;
		//------------------
		if (!isset($MarketConfig['assets'][$cAsset]['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO]))
		{
			XLogError('MarketClient::getHistoricValues_coingecko asset '.$cAsset.' not supported by provider coingecko ('.MARKET_CLIENT_PROVIDER_COINGECKO.')');
			return false;
		}
		//------------------
		$assetName = $MarketConfig['assets'][$cAsset]['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO];
		//------------------
		$providerConfig = $MarketConfig['providers'][MARKET_CLIENT_PROVIDER_COINGECKO];
		$host = $providerConfig['host'];
		$api = $providerConfig['historicvalues'];
		//------------------
		if ($this->sessionCallCount >= $providerConfig['rate_limit_per_minute'])
			return array('ok' => false, 'error' => 'session rate limit', 'count' => $this->sessionCallCount, 'max' => $providerConfig['rate_limit_per_minute'], 'provider' => MARKET_CLIENT_PROVIDER_COINGECKO);
		//------------------
		$dtDate = XMixedToDate($mixedDate);
		if ($dtDate === false)
		{
			XLogError('MarketClient::getHistoricValues_coingecko XMixedToDate failed. param mixedDate: '.XVarDump($mixedDate));
			return false;
		}
		//------------------
		$dtDate->setTime(0, 0); // clear time portion of date, Coingecko historic values are only by day precision
		$pathquery = str_replace('%DATE%', $dtDate->format('d-m-Y'), str_replace('%ASSET%', $assetName, $api));
		//------------------
		$opts = array('http'=>array('header'=>"Host: $host\r\nAccept: application/json\r\n"));
		//------------------
		$this->sessionCallCount++;
		//------------------
		//return 'MarketClient::getHistoricValues_coingecko host '.XVarDump($host).', pathquery '.XVarDump($pathquery).', api '.XVarDump($api).', opts '.XVarDump($opts);
		$context = stream_context_create($opts);
		$httpReply = @file_get_contents('https://'.$host.$pathquery, false, $context);
		if ($httpReply === false)
		{
			XLogWarn('MarketClient::getHistoricValues_coingecko file_get_contents failed: '.XVarDump($http_response_header));
			$httpCode = 0;
			$httpMsg = '';
			if (isset($http_response_header) && is_array($http_response_header) && sizeof($http_response_header) > 1)
			{
				$parts = explode(' ', $http_response_header[0]);
				if (sizeof($parts) >= 3)
				{
					if (!is_numeric($parts[1]))
						$httpMsg = $http_response_header[0];
					else
					{
						$httpCode = (int)$parts[1];
						$httpMsg = $parts[2];
					}
				}
			}
			return array('ok' => false, 'error' => 'request',  'httpCode' => $httpCode, 'httpMsg' => $httpMsg, 'asset' => $cAsset);
		}
		//------------------
		$jsonReply = json_decode($httpReply, true);
		//XLogDebug('MarketClient::getHistoricValues_coingecko httpReply:'."\r\n".XVarDump($httpReply, false)."\r\njsonReply: ".XVarDump($jsonReply, false));
		//------------------
		if (!is_array($jsonReply) || !isset($jsonReply['market_data']) || !is_array($jsonReply['market_data']) || !isset($jsonReply['market_data']['current_price']) || !is_array($jsonReply['market_data']['current_price']))
		{
			XLogError('MarketClient::getHistoricValues_coingecko validate httpReply failed:'."\r\n".XVarDump($jsonReply, false));
			return false;
		}
		//------------------
		$assetIdsByName = array();
		foreach ($MarketConfig['assets'] as $assetId => $assetDetails)
			if (isset($assetDetails['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO]))
				$assetIdsByName[ $assetDetails['assetname'][MARKET_CLIENT_PROVIDER_COINGECKO] ] = $assetId;
		//------------------
		$tsStart = $dtDate->getTimeStamp();
		$tsEnd = $tsStart + $providerConfig['historicvalue_period_sec'];
		//------------------
		$result = array('ok' => true, 'asset' => $cAsset, 'date_start' => $tsStart, 'date_end' => $tsEnd, 'asset_values' => array());
		//------------------
		foreach ($jsonReply['market_data']['current_price'] as $assetName => $value)
			if (isset($assetIdsByName[$assetName]))
				$result['asset_values'][$assetIdsByName[$assetName]] = $value;
		//------------------
		return $result;
	}
	//------------------
} // class MarketClient
//---------------
?>
