<?php
//---------------------------------
/*
 * include/pages/admin/include/SodiumMessenger.php
 * 
 * Notes: 
 *    see https://www.php.net/manual/en/book.sodium.php
 * 
*/
//---------------------------------
function SVarDump(&$Var, $flatten = true)
{
	//-----------------------------------
	ob_start();
	var_dump($Var);
	$s = ob_get_contents();
	ob_end_clean();
	//-----------------------------------
	if ($flatten === true)
	{
		$lines = explode("\n", $s);
		$s = '';
		foreach ($lines as $l)
			$s .= trim($l)." ";
	}
	//-----------------------------------
	return $s;
}
//---------------------------------
class SodiumMessenger
{
	var $debugMode = false;
	var $lastError = false;
	var $authUser = false;
	var $authSignPub = false;
	var $authSignPriv = false;
	var $authCryptPub = false;
	var $authCryptPriv = false;
	var $remoteAuth = array(); // [username] => array(signPub, cryptPub)

    function __construct($debugMode = false) 
    {
		$this->debugMode = $debugMode;
    }

	function setDebug($mode = true)
	{
		$this->debugMode = ($mode === true ? true : false);
	}
	
	function lastError($defaultValue = '')
	{
		if ($this->lastError === false)
			return $defaultValue;
		return $this->lastError;
	}
	
	function clearError($debugMsg = false)
	{
		if ($this->debugMode)
			$this->lastError = $debugMsg;
		else
			$this->lastError = false;		
	}
	
	function appendError($msg)
	{
		if ($this->lastError === false)
			$this->lastError = $msg;
		else 
			$this->lastError .= "\r\n".$msg;
	}
	
	function appendDebug($debugMsg)
	{
		if ($this->debugMode)
			$this->appendError($debugMsg);
	}
	
	function generateKeypair()
	{
		$keypair = sodium_crypto_sign_keypair();
		return array('pub' => sodium_crypto_sign_publickey($keypair), 'priv' => sodium_crypto_sign_secretkey($keypair));
	}
	
	function encode64($data)
	{
		return sodium_bin2base64($data, SODIUM_BASE64_VARIANT_ORIGINAL);
	}
	
	function decode64($data)
	{
		return sodium_base642bin($data, SODIUM_BASE64_VARIANT_ORIGINAL);
	}
	
	function getNonce()
	{
		return random_bytes(SODIUM_CRYPTO_BOX_NONCEBYTES);
	}
	
    function setAuth($user = false, $pubLocalKey = false, $privLocalKey = false)
    {
		// just some ball-park rational length checking
		if (($user !== false && strlen($user) < 2) || ($pubLocalKey !== false && strlen($pubLocalKey) < 10) || ($privLocalKey !== false && strlen($privLocalKey) < 10))
		{
			$this->appendError('SodiumMessenger::setAuth validate parameters failed');
			return false;
		}
		if ($user !== false)
			$this->authUser = $user;
		if ($pubLocalKey !== false)
			$this->authSignPub = $pubLocalKey;
		if ($privLocalKey !== false)
			$this->authSignPriv = $privLocalKey;
		try
		{
			if ($this->authSignPub !== false)
				$this->authCryptPub = sodium_crypto_sign_ed25519_pk_to_curve25519($this->authSignPub);
			if ($this->authSignPriv !== false)
				$this->authCryptPriv = sodium_crypto_sign_ed25519_sk_to_curve25519($this->authSignPriv);
		}
		catch (SodiumException $e)
		{
			$this->appendError('SodiumMessenger::setAuth one or more auth keys are invalid. Convert keys exception: '.$e->getMessage());
			return false;
		}
		return true;
	}
	
	function addRemoteAuth($user, $pubKey)
	{
		// just some ball-park rational length checking
		if (($user !== false && strlen($user) < 2) || ($pubKey !== false && strlen($pubKey) < 10))
		{
			$this->appendError('SodiumMessenger::addRemoteAuth validate parameters failed');
			return false;
		}
		try
		{
			$cryptPub = sodium_crypto_sign_ed25519_pk_to_curve25519($pubKey);
		}
		catch (SodiumException $e)
		{
			$this->appendError('SodiumMessenger::addRemoteAuth auth key is invalid. Convert keys exception: '.$e->getMessage());
			return false;
		}
		$this->remoteAuth[$user] = array($pubKey, $cryptPub);
		return true;
	}
	
	function removeRemoteAuth($user)
	{
		if (isset($this->remoteAuth[$user]))
			unset($this->remoteAuth[$user]);
	}
	
	function authReady()
	{
		if ($this->authUser === false || $this->authSignPub === false || $this->authSignPriv === false || $this->authCryptPub === false || $this->authCryptPriv === false)
			return false;
		return true;
	}
	
	function buildMsg($remoteUser, $message)
	{
		$this->clearError('SodiumMessenger::buildMsg');
		$nonce = $this->getNonce();
		$binCipher = $this->encrypt($remoteUser, $nonce, $message);
		if ($binCipher === false)
		{
			$this->appendError('SodiumMessenger::buildMsg encrypt failed');
			return false;
		}
		$pkt = $this->buildPacket($nonce, $binCipher);
		if ($pkt === false)
		{
			$this->appendError('SodiumMessenger::buildMsg buildPacket failed');
			return false;
		}
		return $pkt;
	}
	
	function buildEmptyMsg()
	{
		$this->clearError('SodiumMessenger::buildEmptyMsg');
		$pkt = $this->buildPacket('', '');
		if ($pkt === false)
		{
			$this->appendError('SodiumMessenger::buildEmptyMsg buildPacket failed');
			return false;
		}
		return $pkt;
	}
	
	function parseMsg($message, $includeUser = false)
	{
		$this->clearError('SodiumMessenger::parseMsg');
		$pkt = $this->parsePacket($message);
		if ($pkt === false)
		{
			$this->appendError('SodiumMessenger::parseMsg parsePacket failed');
			return false;
		}
		if ($pkt['c'] == '' && $pkt['n'] == '')
			$content = ''; // empty Msg
		else
			$content = $this->decrypt($pkt['u'], $pkt['n'], $pkt['c']);
		if ($content === false)
		{
			$this->appendError('SodiumMessenger::parseMsg decrypt content failed');
			return false;
		}
		return (!$includeUser ? $content : array($pkt['u'], $content));
	}
	
	function buildPacket($nonce, $binContent)
	{
		$this->clearError('SodiumMessenger::buildPacket');
		if (!$this->authReady())
		{
			$this->appendError('SodiumMessenger::buildPacket authReady failed');
			return false;
		}
		$msg = implode("\n", array('SM' /*Message ID*/, 1 /*Message Version*/, $this->encode64($this->authUser), $this->encode64($nonce), $this->encode64($binContent), '' /*placeholder for signature*/));
		$msg .= $this->encode64(sodium_crypto_sign_detached($msg, $this->authSignPriv));
		return $msg;
	}
	
	function parsePacket($content)
	{
		$base64RegEx = '/^[A-Za-z0-9+\\\\\/]+={0,2}$/';
		$this->clearError('SodiumMessenger::parsePacket');
		$ret = preg_match('/^[A-Za-z0-9+\\\\\/\n\+\=]+$/', $content);
		if ($ret === 0 || $ret === false)
		{
			$this->appendError('SodiumMessenger::parsePacket dissallowed character check failed. check result: '.SVarDump($ret));
			$this->appendDebug('SodiumMessenger::parsePacket dissallowed character found in this content: '.SVarDump($content));
			$dbout = '';
			$l = strlen($content);
			for ($i = 0; $i < $l;$i++)
			{
				$v = ord($content[$i]);
				if (!  ( ($v >= ord('0') && $v <= ord('9')) ||
						 ($v >= ord('A') && $v <= ord('Z')) ||
						 ($v >= ord('a') && $v <= ord('z')) ||
						 $v == ord('=') || $v == ord('/') || $v == ord('\\') || 
						 $v == ord('{') || $v == ord('}') || $v == ord(',') ||
						 $v == ord(':') || $v == ord('"') || $v == ord('+')
						) )
					$dbout .= "[$i] ($v) '".$content[$i]."'\n";
			}
			$this->appendDebug("Bad char dump (len $l):\n$dbout\n");
			return false;
		}
		if (strncmp($content, "SM\n", 3) != 0)
 		{
			$this->appendError('SodiumMessenger::parsePacket reply quickcheck header bad. reply: '.SVarDump($content));		
			$this->appendDebug('SodiumMessenger::parsePacket bad header in this content: '.SVarDump($content));
			return false;
		}
		$verEnd = strpos($content, "\n", 3);
		if ($verEnd > 6)
		{
			$this->appendError('SodiumMessenger::parsePacket find version failed');
			$this->appendDebug('SodiumMessenger::parsePacket find version failed in this content: '.SVarDump($content));
			return false;
		}
		$ver = substr($content, 3, ($verEnd - 3));
		$ret = preg_match('/^[0-9]+$/', $ver);
		if ($ret === 0 || $ret === false || !is_numeric($ver))
		{
			$this->appendError('SodiumMessenger::parsePacket validate version failed');
			$this->appendDebug('SodiumMessenger::parsePacket validate version failed in this content: '.SVarDump($content));
			return false;
		}
		if ($ver != 1)
		{
			$this->appendError('SodiumMessenger::parsePacket reply unsupported version. ver: '.SVarDump($ver).', reply: '.SVarDump($content));
			return false;
		}

		$userEnd = strpos($content, "\n", $verEnd + 1);
		if (($userEnd - $verEnd) > 200)
		{
			$this->appendError('SodiumMessenger::parsePacket find user failed');
			$this->appendDebug('SodiumMessenger::parsePacket find user failed in this content: '.SVarDump($content));
			return false;
		}
		$user = substr($content, $verEnd + 1, ($userEnd - $verEnd - 1));
		$ret = preg_match($base64RegEx, $user);
		if ($ret === 0 || $ret === false || strlen($user) < 5)
		{
			$this->appendError('SodiumMessenger::parsePacket validate user failed: ');
			$this->appendDebug('SodiumMessenger::parsePacket validate user failed in this content: '.SVarDump($content));
			return false;
		}
		$nonceEnd = strpos($content, "\n", $userEnd + 1);
		if (($nonceEnd - $userEnd) > 200)
		{
			$this->appendError('SodiumMessenger::parsePacket find nonce failed');
			$this->appendDebug('SodiumMessenger::parsePacket find nonce failed in this content: '.SVarDump($content));
			return false;
		}
		$nonce = substr($content, $userEnd + 1, ($nonceEnd - $userEnd - 1));
		$nonceLen = strlen($nonce);
		$ret = preg_match($base64RegEx, $nonce);
		if ($ret === false || ($nonceLen != 0 && $ret === 0) || ($nonceLen !== 0 && $nonceLen < SODIUM_CRYPTO_BOX_NONCEBYTES)) // nonce can be non base64 blank in special HELO message
		{
			$this->appendError('SodiumMessenger::parsePacket validate nonce failed');
			$this->appendDebug('SodiumMessenger::parsePacket validate nonce failed in this content: '.SVarDump($content));
			return false;
		}
		
		$conEnd = strpos($content, "\n", $nonceEnd + 1);
		if (($conEnd - $nonceEnd) > 5000000)
		{
			$this->appendError('SodiumMessenger::parsePacket find cont failed');
			$this->appendDebug('SodiumMessenger::parsePacket find cont failed in this content: '.SVarDump($content));
			return false;
		}
		$cont = substr($content, $nonceEnd + 1, ($conEnd - $nonceEnd - 1));
		$contLen = strlen($cont);
		$ret = preg_match($base64RegEx, $cont);
		if (($contLen != 0 && $ret === 0) || $ret === false) // cont can be non base64 blank in special HELO message
		{
			$this->appendError('SodiumMessenger::parsePacket validate cont failed');
			$this->appendDebug('SodiumMessenger::parsePacket validate cont failed in this content: '.SVarDump($content));
			return false;
		}

		$sigEnd = strpos($content, "\n", $conEnd + 1);
		if ($sigEnd !== false)
		{
			$this->appendError('SodiumMessenger::parsePacket find sig failed');
			$this->appendDebug('SodiumMessenger::parsePacket find sig failed in this content: '.SVarDump($content));
			return false;
		}
		$sig = substr($content, $conEnd + 1); // to end
		$sigLen = strlen($sig);
		$ret = preg_match($base64RegEx, $sig);
		if ($ret === 0 || $ret === false || $sigLen > 200 || $sigLen < 5)
		{
			$this->appendError('SodiumMessenger::parsePacket validate sig failed'.SVarDump($sig));
			$this->appendDebug('SodiumMessenger::parsePacket validate sig failed in this content: '.SVarDump($content));
			return false;
		}

		$user = $this->decode64($user);
		$sig  = $this->decode64($sig);
		
		if (strlen($user) < 3 || strlen($sig) < 5)
		{
			$this->appendError('SodiumMessenger::parsePacket validate decoded user/sig failed');
			$this->appendDebug('SodiumMessenger::parsePacket validate decoded user/sig failed in this content: '.SVarDump($content));
			return false;
		}
		if (!isset($this->remoteAuth[$user]))
		{
			$this->appendError('SodiumMessenger::parsePacket remoteAuth for user \''.$user.'\' not found');
			return false;
		}

		$pubKey = $this->remoteAuth[$user][0 /*signPub*/];
		if ($pubKey === false)
		{
			$this->appendError('SodiumMessenger::parsePacket validate remoteAuth for user \''.$user.'\' failed');
			return false;
		}
			
		$contentWihtoutSignature = substr($content, 0, $conEnd + 1); // included conEnd delimiter
		if (!sodium_crypto_sign_verify_detached($sig, $contentWihtoutSignature, $pubKey))
		{
			$this->appendError('SodiumMessenger::parsePacket verify reply signature failed. reply: '.SVarDump($contentWihtoutSignature));
			$this->appendDebug('SodiumMessenger::parsePacket verify reply signature failed in this content: '.SVarDump($content));
			return false;
		}
		
		if ($nonce !== '' || $cont !== '') // skip on special case HELO message, with both blank nounce and content
		{
			$nonce = $this->decode64($nonce);
			$nonceLen = strlen($nonce);
			$cont = $this->decode64($cont);
			$contLen = strlen($cont);
			
			if ($nonceLen < SODIUM_CRYPTO_BOX_NONCEBYTES)
			{
				$this->appendError('SodiumMessenger::parsePacket verify reply decoded nonce length failed. '.$nonceLen.'/'.SODIUM_CRYPTO_BOX_NONCEBYTES);
				return false;
			}
		}

		return array( 'id' => 'SM', 'v' => (int)$ver, 'u' => $user, 'n' => $nonce, 'c' => $cont, 's' => $sig);
	}
	
	function encrypt($remoteUser, $nonce, $content)
	{
		$this->clearError('SodiumMessenger::encrypt');
		if (!$this->authReady())
		{
			$this->appendError('SodiumMessenger::encrypt authReady failed');
			return false;
		}
		if (!isset($this->remoteAuth[$remoteUser]))
		{
			$this->appendError('SodiumMessenger::encrypt remoteAuth for user \''.$remoteUser.'\' not found');
			return false;
		}
		$pubKey = $this->remoteAuth[$remoteUser][1 /*cryptPub*/];
		if ($pubKey === false)
		{
			$this->appendError('SodiumMessenger::encrypt validate remoteAuth for user \''.$remoteUser.'\' failed');
			return false;
		}
		$keyPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($this->authCryptPriv, $pubKey);
		$binCipher = sodium_crypto_box($content, $nonce, $keyPair);
		return $binCipher;
	}

	function decrypt($remoteUser, $nonce, $binCipherContent)
	{
		$this->clearError('SodiumMessenger::decrypt');
		if (!$this->authReady())
		{
			$this->appendError('SodiumMessenger::decrypt authReady failed');
			return false;
		}
		if (!isset($this->remoteAuth[$remoteUser]))
		{
			$this->appendError('SodiumMessenger::decrypt remoteAuth for user \''.$remoteUser.'\' not found');
			return false;
		}
		$pubKey = $this->remoteAuth[$remoteUser][1 /*cryptPub*/];
		if ($pubKey === false)
		{
			$this->appendError('SodiumMessenger::decrypt validate remoteAuth for user \''.$remoteUser.'\' failed');
			return false;
		}
		$keyPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($this->authCryptPriv, $pubKey);
		$content = sodium_crypto_box_open($binCipherContent, $nonce, $keyPair);
		if ($content === false)
		{
			$this->appendError('SodiumMessenger::decrypt sodium_crypto_box_open failed');
			return false;
		}
		return $content;
	}
} // class SodiumMessenger
//---------------------------------
?>
