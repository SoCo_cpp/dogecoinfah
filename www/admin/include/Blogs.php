<?php
/*
 *	www/include/Blogs.php
 * 
 * 
* 
*/
//---------------
define('BLOG_SLUG_DIRECTORY', '../blog/'); // Current dir will be www/admin/
define('BLOG_SLUG_PREPEND', 'blog_'); // Current dir will be www/admin/
define('BLOG_INDEX_FILE_NAME', '../blog/index.html'); // Current dir will be www/admin/
define('BLOG_TEMPLATE_FILE', './include/BlogTemplate.html'); // Current dir will be www/admin/
define('BLOG_TEMPLATE_TITLE_KEY', '<!--$TITLE$-->');
define('BLOG_TEMPLATE_KEYWORDS_KEY', '<!--$KEYWORDS$-->');
define('BLOG_TEMPLATE_CONTENT_KEY', '<!--$CONTENT$-->');
define('BLOG_INDEX_TEMPLATE_FILE', './include/BlogIndexTemplate.html'); // Current dir will be www/admin/
//---------------
define('BLOG_FLAG_NONE', 		0);
define('BLOG_FLAG_PUBLISHED', 	1);
define('BLOG_FLAG_HIDDEN', 		2);
//---------------
class Blog
{
	var $id = -1;
	var $flags = 0;
	var $dtCreated = "";
	var $dtPublished = false;
	var $slug = false;
	var $userID = false;
	var $author = "";
	var $title = "";
	var $keywords = "";
	var $tags = "";
	var $content = false;
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id   		= $row[DB_BLOG_ID];
		$this->flags 		= (isset($row[DB_BLOG_FLAGS]) ? $row[DB_BLOG_FLAGS] : 0);
		$this->dtCreated 	= (isset($row[DB_BLOG_DATE_CREATED]) ? $row[DB_BLOG_DATE_CREATED] : "");
		$this->dtPublished 	= (isset($row[DB_BLOG_DATE_PUBLISHED]) ? $row[DB_BLOG_DATE_PUBLISHED] : false);
		$this->slug 	= (isset($row[DB_BLOG_SLUG]) ? $row[DB_BLOG_SLUG] : false);
		$this->userID 	= (isset($row[DB_BLOG_USER_ID]) ? $row[DB_BLOG_USER_ID] : false);
		$this->author 	= (isset($row[DB_BLOG_AUTHOR]) ? $row[DB_BLOG_AUTHOR] : "");
		$this->title 	= (isset($row[DB_BLOG_TITLE]) ? $row[DB_BLOG_TITLE] : "");
		$this->keywords	= (isset($row[DB_BLOG_KEYWORDS]) ? $row[DB_BLOG_KEYWORDS] : "");
		$this->tags	= (isset($row[DB_BLOG_TAGS]) ? $row[DB_BLOG_TAGS] : "");
		$this->content 	= (isset($row[DB_BLOG_CONTENT]) ? $row[DB_BLOG_CONTENT] : false);
		//------------------
		if ($this->dtCreated != "")
			if (strtotime($this->dtCreated) === false)
				$this->dtCreated = "";
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbBlog;
		//------------------
		$this->id		= -1;
		$this->flags = $dbBlog->GetMaxSize(DB_BLOG_FLAGS);
		$this->dtCreated = $dbBlog->GetMaxSize(DB_BLOG_DATE_CREATED);
		$this->dtPublished = $dbBlog->GetMaxSize(DB_BLOG_DATE_PUBLISHED);
		$this->slug = $dbBlog->GetMaxSize(DB_BLOG_SLUG);
		$this->userID = $dbBlog->GetMaxSize(DB_BLOG_USER_ID);
		$this->author = $dbBlog->GetMaxSize(DB_BLOG_AUTHOR);
		$this->title = $dbBlog->GetMaxSize(DB_BLOG_TITLE);
		$this->keywords = $dbBlog->GetMaxSize(DB_BLOG_KEYWORDS);
		$this->tags = $dbBlog->GetMaxSize(DB_BLOG_TAGS);
		$this->content = $dbBlog->GetMaxSize(DB_BLOG_CONTENT);
		//------------------
	}
	//------------------
	function generateSlug($slug = false)
	{
		//------------------
		if ($slug !== false)
			$slug = substr($slug, 0, 48);
		if ($this->slug !== false)
			$slug = $this->slug;
		elseif (strlen($this->title) > 20)
			$slug = BLOG_SLUG_PREPEND.substr($this->title, 0, 48).'.html';
		//------------------
		if ($slug === false || strlen($slug) < 10)
		{
			$slug = XRandHash(16);
			if ($slug === false)
				return false;
		}
		//------------------
		// Swap out Non "Letters" with a -
		$slug = preg_replace('/[^\\pL\d\.]+/u', '-', $slug); 
		// Trim out extra -'s
		$slug = trim($slug, '-');
		// Convert letters that we have left to the closest ASCII representation
		$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
		// Make text lowercase
		//$text = strtolower($text);
		// Strip out anything we haven't been able to convert
		$slug = preg_replace('/[^-\w]+\./', '', $slug);
		// enforce lenght limit
		$slug = substr($slug, 0, 48);
		//------------------
		$this->slug = urlencode($slug); // utf8_encode
		//------------------
		return $this->slug;
	}
	//------------------
	function fullSlugFileName()
	{
		//------------------
		if ($this->slug === false)
			return false;
		//------------------
		return BLOG_SLUG_DIRECTORY.$this->slug;
	}
	//------------------
	function setFlag($flag)
	{
		$this->flags |= $flag;
	}
	//------------------
	function clearFlag($flag)
	{
		if (($this->flags & $flag) != 0)
			$this->flags ^= $flag;
	}
	//------------------
	function hasFlag($flag)
	{
		return (($this->flags & $flag) != 0 ? true : false);
	}
	//------------------
	function isPublished()
	{
		//------------------
		if (!$this->hasFlag(BLOG_FLAG_PUBLISHED))
			return false;
		//------------------
		if ($this->dtPublished === false)
			return false;
		//------------------
		$fullFileName = $this->fullSlugFileName();
		if ($fullFileName === false || !file_exists($fullFileName))
			return false;
		//------------------
		return true;
	}
	//------------------
	function isVisible() // must also be published
	{
		//------------------
		return ($this->isPublished() && ($this->flags & BLOG_FLAG_HIDDEN) == 0);
	}
	//------------------
	function loadContent()
	{
		global $db, $dbBlog;
		//---------------------------------
		$dbBlog->ClearValues();
		$dbBlog->SetValue(DB_BLOG_CONTENT);
		//---------------------------------
		$sql = $dbBlog->scriptSelect(DB_BLOG_ID."=".$this->id, false /*orderby*/, 1 /*limit*/);
		//------------------
		$qr = $db->Query($sql);
		if ($qr === false)
		{
			XLogError("Blog::loadContent - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$row = $qr->GetRowArray();
		if ($row === false)
		{
			XLogError("Blog::loadContent - GetRowArray failed.");
			return false;
		}
		//------------------
		$this->content 	= (isset($row[DB_BLOG_CONTENT]) ? $row[DB_BLOG_CONTENT] : false);
		//---------------------------------
		return true;
	}	
	//------------------
	function deleteSlug()
	{
		//---------------------------------
		if ($this->dtPublished === false || ($this->flags & BLOG_FLAG_PUBLISHED) == 0)
			XLogWarn("Blog::deleteSlug - not published.");
		//------------------
		$fullFileName = $this->fullSlugFileName();
		if ($fullFileName === false || !file_exists($fullFileName))
		{
			XLogError("Blog::deleteSlug - publish slug file not found.");
			return false;
		}
		//------------------
		if (!unlink($fullFileName))
		{
			XLogError("Blog::deleteSlug - unlink slug file failed.");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function Unpublish()
	{
		//---------------------------------
		if (!$this->deleteSlug())
		{
			XLogError("Blog::Unpublish - deleteSlug failed.");
			return false;
		}
		//------------------
		$this->dtPublished = false;
		$this->flags ^= BLOG_FLAG_PUBLISHED;
		$this->dtPublished = false;
		//------------------
		if (!$this->Update())
		{
			XLogError("Blog::Unpublish - Update failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function createSlug()
	{
		//---------------------------------
		$fullFileName = $this->fullSlugFileName();
		if ($fullFileName === false)
		{
			XLogError("Blog::createSlug - fullSlugFileName failed.");
			return false;
		}
		//------------------
		if ($this->content === false)
		{
			XLogError("Blog::createSlug - no content or content not loaded.");
			return false;
		}
		//------------------
		$template = file_get_contents(BLOG_TEMPLATE_FILE);
		if ($template === false)
		{
			XLogError("Blog::createSlug - load blog template failed: ".BLOG_TEMPLATE_FILE);
			return false;
		}
		//---------------------------------
		$finalContent = str_replace(BLOG_TEMPLATE_TITLE_KEY, $this->title, $template);
		$finalContent = str_replace(BLOG_TEMPLATE_KEYWORDS_KEY, $this->keywords, $finalContent);
		//---------------------------------
		if (strpos($finalContent, BLOG_TEMPLATE_CONTENT_KEY) === false)
		{
			XLogError("Blog::createSlug - blog template content key not found. template: ".BLOG_TEMPLATE_FILE);
			return false;
		}
		//---------------------------------
		$finalContent = str_replace(BLOG_TEMPLATE_CONTENT_KEY, $this->content, $finalContent);
		//---------------------------------
		if (file_put_contents($fullFileName, $finalContent, LOCK_EX) === false)
		{
			$cwd = realpath("./");
			XLogError("Blog::createSlug - write failed: ($fullFileName) cwd: ".XVarDump($cwd));
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function Publish($forceDTPublish = false)
	{
		//---------------------------------
		if (!$this->generateSlug())
		{
			XLogError("Blog::Publish - generateSlug failed.");
			return false;
		}
		//---------------------------------
		if (!$this->createSlug())
		{
			XLogError("Blog::Publish - createSlug failed.");
			return false;
		}
		//------------------
		$this->setFlag(BLOG_FLAG_PUBLISHED);
		//------------------
		if ($forceDTPublish || $this->dtPublished === false)
		{
			$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
			$this->dtPublished = $nowUtc->format(MYSQL_DATETIME_FORMAT);
		}
		//------------------
		if (!$this->Update(false/*republish*/))
		{
			XLogError("Blog::Publish - Update failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function Update($republish = true)
	{
		//---------------------------------
		if ($republish && $this->isPublished())
			return $this->Publish();
		//---------------------------------
		global $db, $dbBlog;
		//---------------------------------
		$dbBlog->ClearValues();
		$dbBlog->SetValuePrepared(DB_BLOG_FLAGS,			$this->flags);
		$dbBlog->SetValuePrepared(DB_BLOG_DATE_CREATED,		$db->sanitize($this->dtCreated));
		$dbBlog->SetValuePrepared(DB_BLOG_DATE_PUBLISHED,	($this->dtPublished !== false ? $db->sanitize($this->dtPublished) : null));
		$dbBlog->SetValuePrepared(DB_BLOG_SLUG,				($this->slug !== false ? $db->sanitize($this->slug) : null));
		$dbBlog->SetValuePrepared(DB_BLOG_USER_ID,			($this->userID !== false ? $this->userID : null));
		$dbBlog->SetValuePrepared(DB_BLOG_AUTHOR,			$this->author);
		$dbBlog->SetValuePrepared(DB_BLOG_TITLE,			$this->title);
		$dbBlog->SetValuePrepared(DB_BLOG_KEYWORDS,			$this->keywords);
		$dbBlog->SetValuePrepared(DB_BLOG_TAGS,				$this->tags);
		$dbBlog->SetValuePrepared(DB_BLOG_CONTENT,			($this->content !== false ? $this->content : null));
		//---------------------------------
		$sql = $dbBlog->scriptUpdate(DB_BLOG_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Blog::Update - db Prepare failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbBlog->BindValues($dbs))
		{
			XLogError("Blog::Update - BindValues failed.");
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("Blog::Update - db prepared statement Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
} // class Blog
//---------------
class Blogs
{
	//------------------
	var $blogs = array();
	var $isLoaded = false;
	//------------------
	function Install()
	{
		global $db, $dbBlog;
		//------------------------------------
		$sql = $dbBlog->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Blogs::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbBlog;
		//------------------------------------
		$sql = $dbBlog->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Blogs::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbBlog;
		//------------------------------------
		switch ($oldTableVer)
		{
			case $dbBlog->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbBlog->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Blogs::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Blogs::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msize = new Blog();
		$msizet->setMaxSizes();
		//------------------------------------
		return $msize;		
	}
	//------------------
	function delete($idx)
	{
		global $db, $dbBlog;
		//---------------------------------
		$sql = $dbBlog->scriptDelete(DB_CONTRIBUTION_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Blogs::deleteContribution - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		if ($this->load() === false)
		{
			XLogError("Blogs::delete - load failed.");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbBlog;
		//---------------------------------
		$sql = $dbBlog->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Blogs::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->blogs = array();
		//---------------------------------
		$this->isLoaded = true;
		//------------------
		return true;
	}
	//---------------------------------	
	function loadRaw($idx)
	{
		global $db, $dbBlog;
		//------------------
		$dbBlog->SetValues();
		//------------------
		$sql = $dbBlog->scriptSelect(DB_BLOG_ID."=".$idx, false /*orderby*/, 1 /*limit*/);
		//------------------
		$qr = $db->Query($sql);
		if ($qr === false)
		{
			XLogError("Blogs::loadRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function load($idx)
	{
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Blogs::load - validate index failed");
			return false;
		}
		//------------------
		$qr = $this->loadRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Blogs::load - loadRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Blogs::load - index $idx not found.");
			return false;
		}
		//------------------
		return new Contribution($s);
	}
	//------------------
	function getBlog($idx)
	{
		//---------------------------------
		if (!is_numeric($idx))
		{
			XLogError("Blogs::getBlog - validate index failed");
			return false;
		}
		//------------------
		if ($this->isLoaded)
			foreach ($this->blogs as $c)
				if ($c->id == $idx)
					return $c;
		//---------------------------------
		return $this->load($idx);
	}
	//------------------
	function loadBlogsRaw($noContent = true)
	{
		global $db, $dbBlog;
		//------------------
		$dbBlog->SetValues();
		if ($noContent)
			$dbBlog->ClearValue(DB_BLOG_CONTENT);
		$sql = $dbBlog->scriptSelect(false /*where*/, DB_BLOG_ID /*orderby*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Blogs::loadBlogsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function loadBlogs($noContent = true)
	{
		$this->blogs = array();
		//------------------
		$qr = $this->loadBlogsRaw($noContent);
		//------------------
		if ($qr === false)
		{
			XLogError("Blogs::loadBlogs - loadBlogsRaw failed");
			return false;
		}
		//------------------
		while ($p = $qr->GetRowArray())
			$this->blogs[] = new Blog($p);
		//------------------
		$this->isLoaded = true;
		//------------------
		return $this->blogs;
	}
	//------------------
	function getBlogs($noContent = true)
	{
		//---------------------------------
		if ($this->isLoaded)
			return $this->blogs;
		//---------------------------------
		return $this->loadBlogs($noContent);
	}
	//------------------
	// dtPublished = dt:dt, true: now, false: null
	function addBlog($userID, $dtPublished = true, $author = "", $title = "", $keywords = "", $tags = "", $content = "")
	{
		global $db, $dbBlog;
		//------------------
		if (!is_numeric($userID))
		{
			XLogError("Blogs::addBlog validate userID is_numeric failed for '$userID'");
			return false;
		}
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		$nowUtcString = $nowUtc->format(MYSQL_DATETIME_FORMAT);
		//------------------
		$dbBlog->ClearValues();
		$dbBlog->SetValuePrepared(DB_BLOG_DATE_CREATED, $nowUtcString);
		$dbBlog->SetValuePrepared(DB_BLOG_DATE_PUBLISHED, ($dtPublished === false ? null : ($dtPublished === true ?  $nowUtcString : $dtPublished)));
		$dbBlog->SetValuePrepared(DB_BLOG_USER_ID, $userID);
		$dbBlog->SetValuePrepared(DB_BLOG_AUTHOR, $author);
		$dbBlog->SetValuePrepared(DB_BLOG_TITLE, $title);
		$dbBlog->SetValuePrepared(DB_BLOG_KEYWORDS, $keywords);
		$dbBlog->SetValuePrepared(DB_BLOG_TAGS, $tags);
		$dbBlog->SetValuePrepared(DB_BLOG_CONTENT, $content);
		//------------------
		$sql = $dbBlog->scriptInsert();
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Blogs::addBlog - db Prepare failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbBlog->BindValues($dbs))
		{
			XLogError("Blogs::addBlog - BindValues failed.");
			return false;
		}
		//---------------------------------
		if (!$db->ExecutePrepared($dbs))
		{
			XLogError("Blogs::addBlog - db ExecutePrepared failed.\nsql: $sql\nPrepare Dump: ".$db->DebugDumpStatement($dbs));
			return false;
		}
		//---------------------------------
		$this->blogs = array();
		$this->isLoaded = false; // list modified, set to reload
		//------------------
		$where = DB_BLOG_DATE_CREATED."='$nowUtcString'";
		//------------------
		$dbBlog->ClearValues();
		$dbBlog->SetValue(DB_BLOG_ID);
		//------------------
		$sql = $dbBlog->scriptSelect($where, DB_BLOG_ID /*orderby*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Blogs::addBlog - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$row = $qr->GetRowArray();
		//------------------
		if ($row === false)
		{
			XLogWarn("Blogs::addBlog - new payout not found (created $nowUtcString).");
			return false;
		}
		//------------------
		return (int)$row[DB_BLOG_ID];
	}
	//------------------
	function deleteBlog($idx)
	{
		global $db, $dbBlog;
		//------------------
		$sql = $dbBlog->scriptDelete(DB_BLOG_ID."=$idx");
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Blogs::deleteBlog - db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		$this->isLoaded = false;
		//---------------------------------
		return true;
	}
	//------------------
	function createIndexFile()
	{
		//---------------------------------
		$template = file_get_contents(BLOG_INDEX_TEMPLATE_FILE);
		if ($template === false)
		{
			XLogError("Blogs::createIndexFile - load blog template failed: ".BLOG_INDEX_TEMPLATE_FILE);
			return false;
		}
		//---------------------------------
		if (strpos($template, BLOG_TEMPLATE_CONTENT_KEY) === false)
		{
			XLogError("Blogs::createIndexFile - blog index template content key not found. template: ".BLOG_INDEX_TEMPLATE_FILE);
			return false;
		}
		//---------------------------------
		$blogList = $this->getBlogs();
		if ($blogList === false)
		{
			XLogError("Blogs::createIndexFile - getBlogs failed");
			return false;
		}
		//---------------------------------
		$data = "<script type='text/javascript'>\n//<![CDATA[\n";
		$data .= "var blogData = [\n";
		foreach ($blogList as $b)
			if ($b->isVisible())
				$data .= "\t{'id':'$b->id','pdt': '$b->dtPublished','a':'$b->author','t':'$b->title','s':'$b->slug','f':'$b->flags'},\n";
		//---------------------------------
		$data .= "];\n";
		$data .= "//]]>\n</script>\n";
		//---------------------------------
		$finalContent = str_replace(BLOG_TEMPLATE_CONTENT_KEY, $data, $template);
		//---------------------------------
		if (file_put_contents(BLOG_INDEX_FILE_NAME, $finalContent, LOCK_EX) === false)
		{
			XLogError("Blogs::createIndexFile - write failed: (".BLOG_INDEX_FILE_NAME.") ".XVarDump(realpath("./")));
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
} // class Blogs
//---------------
?>
