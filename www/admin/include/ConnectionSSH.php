<?php
//---------------------------------
/*
 * include/pages/admin/include/ConnectionSSH.php
 * 
 * Notes: 
 *    see https://www.php.net/manual/en/function.ssh2-connect.php
 * 
 *    Documentation says only old keys, cyphers, kex, and hmacs are supported.
 * 	  Keys must be RSA (old) or DSA (really old/bad).
 *    Privkey format must be PEM (ssh-keygen -t rsa -m pem).
 *    Password protected RSA key doesn't seem to work currently.
 * 	  Use of callbacks (debug,disconnect,hmacerror,ignore) causes PHP segfault on test system.
 *    Callbacks, when working, only report SSH packets, no debug info on auth failure anyways.
 * 	  Server finger print only comes in raw/hex and md5/sha1 (not the modern sha256).
 * 	  Get a key finger print in md5: ssh-keygen -l -E md5 -f ~/ssh/id_rsa.pub  | sed -r 's/^[0-9]{1,5}\s{1,}MD5:|://g'
*/
//---------------------------------
function ConSSH_VarDump(&$Var, $flatten = true)
{
	//-----------------------------------
	ob_start();
	var_dump($Var);
	$s = ob_get_contents();
	ob_end_clean();
	//-----------------------------------
	if ($flatten === true)
	{
		$lines = explode("\n", $s);
		$s = '';
		foreach ($lines as $l)
			$s .= trim($l)." ";
	}
	//-----------------------------------
	return $s;
}
//---------------------------------
class ConnectionSSH
{
	var $debugMode = false;
	var $lastError = false;
    var $host = false;
    var $port = 22;
    var $user = false;
    var $password = false; // optional, use pub/priv key otherwise
    var $serverFP = false;
    var $pubKey = false;
    var $privKey = false;
    var $privKeyPassphrase = false; // optional
    var $connection = false;
    Var $sshMethods = false;
   
    function __construct($debugMode = false) 
    {
		$this->debugMode = $debugMode;
		$this->sshMethods = array( 	'kex' => 'diffie-hellman-group-exchange-sha256,diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1',
									'hostkey' => 'ssh-rsa',
									'client_to_server' => array(	'crypt' => 'aes256-ctr,aes192-ctr,rijndael-cbc@lysator.liu.se,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour',
																	'comp' => 'zlib,none',
																	'mac' => 'hmac-sha2-512,hmac-sha2-256,hmac-sha1,hmac-sha1-96,hmac-ripemd160,hmac-ripemd160@openssh.com'),
									'server_to_client' => array(	'crypt' => 'aes256-ctr,aes192-ctr,rijndael-cbc@lysator.liu.se,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc,cast128-cbc,arcfour',
																	'comp' => 'zlib,none',
																	'mac' => 'hmac-sha2-512,hmac-sha2-256,hmac-sha1,hmac-sha1-96,hmac-ripemd160,hmac-ripemd160@openssh.com')
									);
    }

    function __destruct() 
    {
        $this->disconnect();
    }

	function setDebug($mode = true)
	{
		$this->debugMode = ($mode === true ? true : false);
	}
	
	function lastError($defaultValue = '')
	{
		if ($this->lastError === false)
			return $defaultValue;
		return $this->lastError;
	}
	
	function clearError($debugMsg = false)
	{
		if ($this->debugMode)
			$this->lastError = $debugMsg;
		else
			$this->lastError = false;		
	}
	
	function appendError($msg)
	{
		if ($this->lastError === false)
			$this->lastError = $msg;
		else 
			$this->lastError .= "\r\n".$msg;
	}
	
	function appendDebug($debugMsg)
	{
		if ($this->debugMode)
			$this->appendError($debugMsg);
	}
	
    function configure($host = false, $port = false, $user = false, $password = false, $serverFP = false, $pubKey = false, $privKey = false, $privKeyPassphrase = false, $sshMethods = false)
    {
		if ($host !== false)
			$this->host = $host;
		if ($port !== false)
			$this->port = $port;
		if ($user !== false)
			$this->user = $user;
		if ($password !== false)
			$this->password = $password;
		if ($serverFP !== false)
			$this->serverFP = $serverFP;
		if ($pubKey !== false)
			$this->pubKey = $pubKey;
		if ($privKey !== false)
			$this->privKey = $privKey;
		if ($privKeyPassphrase !== false)
			$this->privKeyPassphrase = $privKeyPassphrase;
		if ($sshMethods !== false)
			$this->sshMethods = $sshMethods;
		return true;
	}

    function connect($failIfConnected = true)
    {
		$this->clearError('ConnectionSSH::connect...'); 
		if (!function_exists('ssh2_connect'))
		{
			$this->appendError('ConnectionSSH::connect PHP ssh2 support not detected');
			return false;
		}
		if ($this->connection !== false)
		{
			if ($failIfConnected)
			{
				$this->appendError('ConnectionSSH::connect already connected');
				return false;
			}
			$this->appendDebug('ConnectionSSH::connect already connected, returning success');
			return true; 
		}
    	$this->connection = @ssh2_connect($this->host, $this->port, ($this->sshMethods === false ? null : $this->sshMethods));
		if ($this->connection === false)
		{
			$this->appendError('ConnectionSSH::connect ssh2_connect failed');
			return false;
		}
		$this->appendDebug('ConnectionSSH::connect...connect done...');
		if ($this->serverFP !== false)
		{
			$fingerprint = @ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 |  SSH2_FINGERPRINT_HEX);
			if (strcasecmp($this->serverFP, $fingerprint) !== 0) 
			{
				$this->appendError('ConnectionSSH::connect verify server fingerprint failed: '.ConSSH_VarDump($fingerprint));
				return false;
			}
        }
		$this->appendDebug('ConnectionSSH::connect...fingerprint done...');
        if ($this->password !== false)
        {
			if (!@ssh2_auth_password($this->connection, $this->user, $this->password))
			{
				$this->appendError('ConnectionSSH::connect ssh2_auth_password failed');
				return false;
			}
		}
		else
		{
			$methodsNegotiated = @ssh2_methods_negotiated($this->connection);
			$this->appendDebug('ConnectionSSH::connect methods negotiated: '.ConSSH_VarDump($methodsNegotiated));
			$ret = @ssh2_auth_pubkey_file($this->connection, $this->user, $this->pubKey, $this->privKey, ($this->privKeyPassphrase === false ? null : $this->privKeyPassphrase));
			if (!$ret)
			{
				$this->appendError('ConnectionSSH::connect ssh2_auth_pubkey_file failed. user '.ConSSH_VarDump($this->user).', pubKey '.ConSSH_VarDump($this->pubKey).', privKey '.ConSSH_VarDump($this->privKey).', has passphrase '.($this->privKeyPassphrase === false ? 'No' : 'Yes').', negotiated methods: '.ConSSH_VarDump($methodsNegotiated));
				return false;
			}
		}
		$this->appendDebug('ConnectionSSH::connect...done');
        return true;
    }

    function disconnect() 
    {
		$this->clearError('ConnectionSSH::disconnect...');
		if ($this->connection === false)
			return true; // no connection
		if (function_exists('ssh2_disconnect'))
		{
			if (!@ssh2_disconnect($this->connection))
			{
				$this->appendError('ConnectionSSH::disconnect ssh2_disconnect failed');
				return false;
			}
		}
		$this->connection = null;
		unset($this->connection);
		$this->connection = false;
		$this->appendDebug('ConnectionSSH::disconnect...done');
        return true;
    }
    
    function exec($cmd) 
    {
		$this->clearError('ConnectionSSH::exec...');
		if ($this->connection === false)
		{
			$this->appendError('ConnectionSSH::exec not connected');
			return false;
		}
		$stream = @ssh2_exec($this->connection, $cmd);
		if ($stream === false)
		{
			$this->appendError('ConnectionSSH::exec ssh2_exec failed');
			return false;
		}
		@stream_set_blocking($stream, true);
        $data = '';
        while ($buf = fread($stream, 4096))
            $data .= $buf;
        fclose($stream);
        $this->appendDebug('ConnectionSSH::exec...done');
        return $data;
    }

    function tunnel($tHost, $tPort) 
    {
		$this->clearError('ConnectionSSH::tunnel...');
		if ($this->connection === false)
		{
			$this->appendError('ConnectionSSH::tunnel not connected');
			return false;
		}
		$tunnel = @ssh2_tunnel($this->connection, $tHost, $tPort);
		if ($tunnel === false)
		{
			$this->appendError('ConnectionSSH::tunnel ssh2_tunnel failed');
			return false;
		}
		$this->appendDebug('ConnectionSSH::tunnel...done');
        return $tunnel;
    }
} // class ConnectionSSH
//---------------------------------
?>
