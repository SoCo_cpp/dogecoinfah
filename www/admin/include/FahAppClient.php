<?php
/*
 *	www/include/FahAppClient.php
 * 
 *  The FAH App Client, FahAppClient works
 *  with the older Folding@home 'app' flat files.
*/
//---------------
define('FAH_APP_TEST', false); // uses test data from file specified in FAH_APP_TEST_DATA
define('FAH_APP_DEBUG_OUTPUT_DATASOURCE', false); // may leak confidential proxy url to log
define('FAH_APP_PROXY_TEST', false); // uses last summary data
define('FAH_APP_DISABLE_WGET', true); 
define('FAH_APP_SIMPLE_EXEC', true); // use XExecSimple (exec) instead of XExecute (proc_open)
define('FAH_APP_PHP_STREAM_TIMEOUT_DIRECT', 50); // seconds, used with  downloadPHP, when connecting directly
define('FAH_APP_PHP_STREAM_TIMEOUT_PROXY', 900); // seconds, used with  downloadPHP, when using the API Proxy
define('FAH_APP_WGET_READ_TIMEOUT_DIRECT',  50); // seconds, used with downloadWget, when connecting directly (Wget default 900 sec/15 min)
define('FAH_APP_WGET_READ_TIMEOUT_PROXY',  900); // seconds, used with downloadWget, when using the API Proxy (Wget default 900 sec/15 min)
define('FAH_APP_TEST_DATA', './log/archive/fah_app_test_data.txt.bz2');
//---------------
define('FAH_APP_URL', "https://apps.foldingathome.org/daily_user_summary.txt.bz2");
define('FAH_APP_POLL_RATE_LIMIT', 60.0); // minutes (60 min = 1 hour)
//---------------
define('FAH_APP_ARCHIVE_FOLDER', './log/archive/');
define('FAH_APP_CMP_FILE_EXT', '.bz2');
define('FAH_APP_FILE_EXT', '.txt');
//---------------
define('CFG_LAST_FAH_APP_POLL', 'last-fah-app-poll');
define('CFG_LAST_FAH_APP_STATS_POLL', 'last-fah-app-stats-poll');
define('CFG_FAH_APP_POLL_FAILS', 'fah-app-poll-fails');
//---------------------------
class FahAppClient
{
	var $cmdWGet = false;
	var $cmdBZip2 = false;
	var $cmdGrep = false;
	//---------------------------
	// param reparse: (false not reparsing, <0 oldest, >0 newest, abs(1) no grep 3 column, abs(2) needs grepped, 4 column
	function pollTeam($teamId, $rIdx, $reparse = false, $noRateLimitFail = false) // use $rIdx = -1 for nonRoundStatsMode stats Team User poll
	{
		global $XLogInstance;
		//------------------
		$timer = new XTimer();
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		$nonRoundStatsMode = ($rIdx == -1 ? true : false);
		//------------------
		$failCount = (int)$Config->Get(CFG_FAH_APP_POLL_FAILS, 0);
		if (!$Config->Set(CFG_FAH_APP_POLL_FAILS, $failCount + 1)) // assume fail at start, then clear to success at completion
		{
			XLogError("FahAppClient::pollTeam config set new CFG_FAH_APP_POLL_FAILS failed");
			return false;
		}
		//------------------
		$this->cmdWGet = $Config->Get(CFG_WGET_CMD); // these commands are managed by Automation.php
		$this->cmdBZip2 = $Config->Get(CFG_BZIP2_CMD);
		$this->cmdGrep = $Config->Get(CFG_GREP_CMD);
		//------------------
		$now = new DateTime('now', ($XLogInstance->TimeZone !== false ? $XLogInstance->TimeZone : new DateTimeZone('UTC')));
		//------------------
		$outFileNameBase = FAH_APP_ARCHIVE_FOLDER."fah_stats_round_".$rIdx."_team_".$teamId."_".$now->format("m-d-Y_H-i");
		$outFileName = $outFileNameBase.FAH_APP_FILE_EXT;
		$outFileNameCmp = $outFileName.FAH_APP_CMP_FILE_EXT;
		//------------------
		$t = 0;
		while (file_exists($outFileNameCmp))
		{
			$outFileName = $outFileNameBase."_".$t.POLL_FAH_STATS_PAGE_FILE_EXT;
			$outFileNameCmp = $outFileName.FAH_APP_CMP_FILE_EXT;
			$t++;
		}
		//------------------
		if ($reparse !== false)
		{
			//------------------
			$baseFileName = "fah_stats_round_".$rIdx."_team_".$teamId."_";
			$dataFiles = scandir(FAH_APP_ARCHIVE_FOLDER);
			if ($dataFiles === false)
			{
				XLogError("FahAppClient::pollTeam (reparse) scandir failed");
				return false;
			}
			//------------------
			$baseFileNameLen = strlen($baseFileName);
			$cmpExtLen = strlen(FAH_APP_CMP_FILE_EXT);
			$selectedDataFile = false;
			$selectedDataFileTimestamp = false;
			XLogDebug(" reparse Mode ".XVarDump($reparse));
			XLogDebug(" baseFileName (".$baseFileNameLen .") '$baseFileName'");
			XLogDebug(" comp Ext     (".$cmpExtLen .") '".FAH_APP_CMP_FILE_EXT."'");
			foreach ($dataFiles as $fileName)
			{
				$msg = " * fn '$fileName': ";
				$fileNameFull = FAH_APP_ARCHIVE_FOLDER.$fileName;
				$msg .= $fileNameFull;
				XLogDebug($msg);
				if (substr_compare($fileName, $baseFileName, 0, $baseFileNameLen) === 0 && substr_compare($fileName, FAH_APP_CMP_FILE_EXT, -($cmpExtLen), $cmpExtLen) === 0)
				{
					$fileTimestamp = @filemtime($fileNameFull);
					if ($fileTimestamp === false)
					{
						XLogError("FahAppClient::pollTeam (reparse) filemtime failed for file '$fileNameFull'");
						return false;
					}
					XLogDebug(" accepted, ts ".XVarDump($fileTimestamp)." / ".XVarDump($selectedDataFileTimestamp));
					if  ($selectedDataFileTimestamp === false || ($reparse < 0 /*oldest*/ && $fileTimestamp < $selectedDataFileTimestamp)
							|| ($reparse > 0 /*newest*/ && $fileTimestamp > $selectedDataFileTimestamp))
					{
						$selectedDataFile = $fileNameFull;
						$selectedDataFileTimestamp = $fileTimestamp;					
					}
				}
			}
			//------------------
			XLogDebug(" selectedDataFile: ".XVarDump($selectedDataFile));
			if ($selectedDataFile === false)
			{
				XLogError("FahAppClient::pollTeam (reparse) couldn't find a data file to reparse, expected starts with: $baseFileName*, and ending with: ".FAH_APP_CMP_FILE_EXT);
				return false;
			}
			//------------------
			$reparseFileNameCmp = $selectedDataFile;
			$reparseFileName = substr($reparseFileNameCmp, 0, -strlen(FAH_APP_CMP_FILE_EXT));
			$tmpFileName = $reparseFileName.".reparse";
			$tmpFileNameCmp = $tmpFileName.FAH_APP_CMP_FILE_EXT;
			//------------------
			if (!copy($reparseFileNameCmp, $tmpFileNameCmp))
			{
				XLogError("FahAppClient::pollTeam copy file (reparse duplicate) failed");
				return false;
			}
			//------------------
			if (!file_exists($tmpFileNameCmp))
			{
				XLogError("FahAppClient::pollTeam verify reparse duplicated file exists failed");
				return false;
			}
			//------------------
			XLogDebug("FahAppClient::pollTeam (reparse) file names reparse ($reparseFileName + $reparseFileNameCmp), tmp ($tmpFileName + $tmpFileNameCmp), out ($outFileName + $outFileNameCmp)");
			//------------------
		}
		else // $reparse === false
		{
			//------------------
			if ($failCount > 2)
			{
				XLogWarn("FahAppClient::pollTeam fail count is at max (for bandwidth protection). Clear the config '".CFG_FAH_APP_POLL_FAILS."' to continue.");
				if ($rIdx != -1)
				{
					$Monitor = new Monitor() or die ('Create object failed');
					if (!$Monitor->RoundFahStatLocked('FahAppClient pollTeam', CFG_FAH_APP_POLL_FAILS, $failCount))
					{
						XLogError("FahAppClient::pollTeam Monitor RoundFahStatLocked (failCount) failed");
						return false;
					}
				}
				return 0;
			}
			//------------------
			$ret = $this->checkRateLimit(false /*nonRoundStatsMode*/, $noRateLimitFail);
			if ($ret === true && $nonRoundStatsMode)
				$ret = $this->checkRateLimit(true /*nonRoundStatsMode*/, $noRateLimitFail);
			//------------------
			if ($ret !== true)
			{
				if (!$Config->Set(CFG_FAH_APP_POLL_FAILS, $failCount)) // restore failCount, don't fail on passed due to rate limit
					XLogError("FahAppClient::pollTeam config set old CFG_FAH_APP_POLL_FAILS after rate limit failed");
				
				if ($ret === false)
					XLogWarn("FahAppClient::pollTeam checkRateLimit returned false");
				else if ($rIdx != -1)
				{
					$Monitor = new Monitor() or die ('Create object failed');
					if (!$Monitor->RoundFahStatLocked('FahAppClient pollTeam', CFG_LAST_FAH_APP_POLL, false /*failCount*/, ($ret * 60) /*elapsedSec*/, (FAH_APP_POLL_RATE_LIMIT * 60) /*elapsedMinSec*/))
					{
						XLogError("FahAppClient::pollTeam Monitor RoundFahStatLocked (rate limit) failed");
						return false;
					}
				}
				return ($ret === false || !$noRateLimitFail ? false : 0); // false or 0
			}
			//------------------
			$tmpFileName = $this->getTempFileName(FAH_APP_FILE_EXT);
			if ($tmpFileName === false)
			{
				XLogError("FahAppClient::pollTeam getTempFileName failed");
				return false;
			}
			//------------------
			$tmpFileNameCmp = $tmpFileName.FAH_APP_CMP_FILE_EXT;
			//------------------
			XLogDebug("FahAppClient::pollTeam file names tmp ($tmpFileName + $tmpFileNameCmp) out ($outFileName + $outFileNameCmp)");
			$timer->start();
			if (!$this->download($tmpFileNameCmp, $teamId))
			{
				XLogError("FahAppClient::pollTeam download failed");
				return false;
			}
			XLogDebug("FahAppClient::pollTeam downloaded in ".$timer->elapsedMs(true));
			//------------------
			if (!$this->updateRateLimit($nonRoundStatsMode)) // only update the active rate limit
			{
				XLogError("FahAppClient::pollTeam updateRateLimit failed");
				return false;
			}	
			//------------------
		} // else // $reparse === false
		//------------------
		$timer->start();
		if (!$this->extractBz2File($tmpFileNameCmp))
		{
			XLogError("FahAppClient::pollTeam extractBz2File failed");
			if (file_exists($tmpFileNameCmp))
				unlink($tmpFileNameCmp);
			if (file_exists($tmpFileName))
				unlink($tmpFileName);
			return false;
		}
		XLogDebug("FahAppClient::pollTeam data decompressed in ".$timer->restartMs(true));
		//------------------
		if (file_exists($tmpFileNameCmp)) // extraction should have already deleted this
			unlink($tmpFileNameCmp);
		//------------------
		if (!file_exists($tmpFileName))
		{
			XLogError("FahAppClient::pollTeam verify extracted file exists failed. filename: $tmpFileName");
			return false;
		}
		if (file_exists($outFileName))
			XLogDebug("FahAppClient::pollTeam outFileName exists already: $outFileName, is_writable: ".(is_writable($outFileName) ? "Y" : "N"));
		//------------------
		
		//------------------
		if ( ($reparse !== false && abs($reparse) == 2) || !(defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false)) // don't grep if proxy ver > 0 or reparsed needing grep
		{
			if (!$this->grepTeamMembers($teamId, $tmpFileName, $outFileName))
			{
				XLogError("FahAppClient::pollTeam grepTeamMembers failed");
				return false;
			}
			XLogDebug("FahAppClient::pollTeam team members grepped from data in ".$timer->restartMs(true));
		}
		else
		{
			if (!copy($tmpFileName, $outFileName))
			{
				XLogError("FahAppClient::pollTeam copy file (instead of grep) failed");
				return false;
			}
		}
		//------------------
		$data = $this->readData($teamId, $outFileName, $reparse);
		if ($data === false)
		{
			XLogError("FahAppClient::pollTeam readData failed");
			return false;
		}
		XLogDebug("FahAppClient::pollTeam data read in ".$timer->restartMs(true));
		//------------------
		if (!$this->compressBz2File($outFileName))
		{
			XLogError("FahAppClient::pollTeam compressBz2File failed");
			return false;
		}
		XLogDebug("FahAppClient::pollTeam data compressed for archiving in ".$timer->restartMs(true));
		//------------------
		if ($reparse !== false)
			if (!$this->compressBz2File($tmpFileName)) // only for reparse
			{
				XLogError("FahAppClient::pollTeam compressBz2File reparse source file failed");
				return false;
			}
		//------------------
		if (file_exists($outFileName)) // compression should have already deleted this
			unlink($outFileName);
		//------------------
		$timer->start();
		if ($nonRoundStatsMode) // for team stats data
		{
			//------------------
			if (file_exists($outFileNameCmp))
				unlink($outFileNameCmp); // remove archive of stats
			//------------------
			if (!$this->processStatsData($data, $teamId))
			{
				XLogError("FahAppClient::pollTeam processStatsData failed");
				return false;
			}
			//------------------
			XLogDebug("FahAppClient::pollTeam stats data processed in ".$timer->elapsedMs(true));
			//------------------
		}
		else // if $nonRoundStatsMode
		{
			//------------------
			if (!$this->processData($rIdx, $data))
			{
				XLogError("FahAppClient::pollTeam processData failed");
				return false;
			}
			//------------------
			XLogDebug("FahAppClient::pollTeam round data processed in ".$timer->elapsedMs(true));
			//------------------
		}
		//------------------
		if (!$Config->Set(CFG_FAH_APP_POLL_FAILS, 0))
		{
			XLogError("FahAppClient::pollTeam config set zero CFG_FAH_APP_POLL_FAILS failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function getTempFileName($appendExtention = "")
	{
		//------------------
		$tempDir = sys_get_temp_dir(); // shouldn't fail
		if ($tempDir === false || $tempDir == "")
		{
			XLogError("FahAppClient::getTempFileName validate sys_get_temp_dir failed, returned: ".XVarDump($tempDir));
			return false;
		}
		//------------------
		$tempFileName = tempnam($tempDir, "dfah_fah_stat_");
		if ($tempFileName === false || $tempFileName == "")
		{
			XLogError("FahAppClient::getTempFileName validate tempnam failed, returned: ".XVarDump($tempFileName));
			return false;
		}
		$tempFileName .= $appendExtention;
		//------------------
		return $tempFileName;
	}
	//---------------------------
	function download($outputFileName, $teamId)
	{
		//------------------
		$supportWget = ($this->cmdWGet === false || $this->cmdWGet === "" ? false : true);
		//------------------
		if (FAH_APP_TEST !== false)
		{
			$supportWget = false; // override
			$dataSource = FAH_APP_TEST_DATA;
		}
		else if (defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false)
			$dataSource = FOLD_API_PROXY."?key=".FOLD_API_PROXY_KEY."&team=$teamId&cmd=".(FAH_APP_PROXY_TEST === true ? "lastusersummary" : "usersummary")."&ver=1";
		else
			$dataSource = FAH_APP_URL;
		//------------------
		if (FAH_APP_TEST === false && defined('FOLD_API_PROXY') && FOLD_API_PROXY !== false) // proxy enabled, don't log $dataSource, it will leak FOLD_API_PROXY_KEY
			XLogDebug("FahAppClient::download downloading stats, is proxy test: ".(FAH_APP_PROXY_TEST ? "!YES!" : "no").", is proxy: YES, supports wget: ".XVarDump($supportWget).", wget disabled: ".(FAH_APP_DISABLE_WGET ? "!YES!" : "no")." data source: ".(FAH_APP_DEBUG_OUTPUT_DATASOURCE !== false ? FOLD_API_PROXY : "(hidden)").", output filename: $outputFileName");
		else
			XLogDebug("FahAppClient::download downloading stats, is test: ".(FAH_APP_TEST ? "!YES!" : "no").", is proxy: NO, supports wget: ".XVarDump($supportWget).", wget disabled: ".(FAH_APP_DISABLE_WGET ? "!YES!" : "no").", data source: ".(FAH_APP_DEBUG_OUTPUT_DATASOURCE !== false ? $dataSource : "(hidden)").", output filename: $outputFileName");
		//------------------
		//XLogDebug("FahAppClient::download PROXY: $dataSource"); // leaks proxy key
		//------------------
		if ($supportWget && !FAH_APP_DISABLE_WGET)
		{
			if (!$this->downloadWget($dataSource, $outputFileName))
			{
				XLogError("FahAppClient::downloading downloadWget failed");
				if (file_exists($outputFileName))
					unlink($outputFileName);
				return false;
			}
		}
		else
		{
			if (!$this->downloadPHP($dataSource, $outputFileName))
			{
				XLogError("FahAppClient::downloading downloadPHP failed");
				if (file_exists($outputFileName))
					unlink($outputFileName);
				return false;
			}
		}
		//------------------
		if (!file_exists($outputFileName))
		{
			XLogError("FahAppClient::download validate output file exists after download failed. output file name: '$outputFileName'");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function downloadWget($dataSource, $outputFileName)
	{
		//------------------
		if (!file_exists($this->cmdWGet))
		{
			XLogError("FahAppClient::downloadWget configured wget command not found: ".XVarDump($this->cmdWGet));
			return false;
		}
		//------------------
		if (defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false)
			$readTimeout = FAH_APP_WGET_READ_TIMEOUT_PROXY;
		else
			$readTimeout = FAH_APP_WGET_READ_TIMEOUT_DIRECT;
		//------------------
		// quotes required around dataSource to get proper return value (with XExecute)
		// -nv no verbose, -t 3 retries, -O output file, -a filename append output to logfile, -S print server response, --read-timeout default 900 sec (15 min)
		// 2>&1 redirects stderr to stdout
		$cmd = $this->cmdWGet." -nv --read-timeout=$readTimeout -S -O '$outputFileName' \"$dataSource\" 2>&1";
			//------------------
		if (FAH_APP_SIMPLE_EXEC === true)
		{
			//------------------
			$output = array();
			$rVal = XExecSimple($cmd, $output);
			if ($rVal !== 0)
			{
				XLogError("FahAppClient::downloadWget XExecSimple failed: val: ".XVarDump($rVal).", output: ".XVarDump($output));
				return false;
			}
			//------------------
		}
		else // not simple exec
		{
			//------------------
			$rData = XExecute($cmd);
			if ($rData[0] === false)
			{
				XLogError("FahAppClient::downloadWget XExecute failed: ".$rData[3]);
				return false;
			}
			//------------------
			if ($rData[0] !== 0)
			{
				$pos = strpos($rData[2], "\n"); // this is stderr, so carriage return may get stripped off
				$errorMsg = ($pos === false ? $rData[2] : trim(substr($rData[2], 0, $pos)));
				XLogWarn("FahAppClient::downloadWget XExecute command failed, code ".$rData[0].", error reply: $errorMsg");
				//XLogDebug("FahAppClient::downloadWget XExecute command failed (cont), source: $dataSource , outputFile: $outputFileName"); // leaks proxy key
				//XLogDebug("FahAppClient::downloadWget XExecute command failed (cont2), cmd: $cmd , full response: ".$rData[2]); // leaks proxy key
				return false;			
			}
			//------------------
		}
		//------------------
		return true;
	}
	//---------------------------
	function downloadPHP($dataSource, $outputFileName)
	{
		//------------------
		$timer = new XTimer();
		//------------------
		$hout = fopen($outputFileName, 'wb');
		if ($hout === false)
		{
			XLogError("FahAppClient::downloadPHP fopen failed to open output file: $outputFileName");
			return false;
		}
		//------------------
		$opts = array('http' =>
			array(
				'method' => 'GET',
				'max_redirects' => '0',
				'ignore_errors' => '1'
			)
		);
		//------------------
		$context = stream_context_create($opts);
		//------------------
		$hin = @fopen($dataSource, 'rb', false /*use include path*/, $context);
		if ($hin === false)
		{
			XLogError("FahAppClient::downloadPHP fopen failed to open remote page, response header: ".XVarDump($http_response_header[0])); // don't leak full $dataSource
			return false;
		}
		//------------------
		if (defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false)
			$streamTimeout = FAH_APP_PHP_STREAM_TIMEOUT_PROXY;
		else
			$streamTimeout = FAH_APP_PHP_STREAM_TIMEOUT_DIRECT;
		//------------------
		if (!stream_set_timeout($hin, $streamTimeout))
		{
			XLogError("FahAppClient::downloadPHP stream_set_timeout in failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::downloadPHP starting read with streamTimeout set to $streamTimeout...");
		//------------------
		$readFailed = false;
		$writeFailed = false;
		$streamFailed = false;
		//------------------
		while (!feof($hin) && !$readFailed && !$writeFailed) 
		{
			$data = fread($hin,  8192);
			if ($data === false)
				$readFailed = true;
			else if (false === fwrite($hout, $data))
				$writeFailed = true;
		}
		//------------------
		$streamResult = stream_get_meta_data($hin);
		if ($streamResult['timed_out'] === true)
		{
			XLogWarn("FahAppClient::downloadPHP the stream replied that it had timed out, considering download as failed");
			$streamFailed = true;
		}
		//------------------
		if (!fclose($hin))
			XLogWarn("FahAppClient::downloadPHP fclose remote handle failed");
		//------------------
		if (!fclose($hout))
		{
			XLogError("FahAppClient::downloadPHP fclose local file handle failed");
			return false;
		}
		//------------------
		if (isset($streamResult['uri']) && strpos($streamResult['uri'], '?') !== false)
			$streamResult['uri'] = substr($streamResult['uri'], 0, strpos($streamResult['uri'], '?')); // strip url arguments which could contain proxy key
		//------------------
		if ($readFailed || $writeFailed || $streamFailed)
		{
			XLogError("FahAppClient::downloadPHP read/write/stream failed. read: ".XVarDump($readFailed).", write: ".XVarDump($writeFailed).", streamFailed: ".XVarDump($streamFailed).", output file name: $outputFileName, stream results (scrubbed uri arguments):\n".XVarDump($streamResult, false)."\n");
			return false;
		}
		//------------------
		if (isset($http_response_header) && isset($http_response_header[0]) && substr($http_response_header[0], 0, 5) == "HTTP/")
		{
			$parts = explode(' ', $http_response_header[0], 100);
			$responseCode = (sizeof($parts) < 2 ? -1 : $parts[1]);
			if ($responseCode != 200)
			{
				$errorMessageContents = @file_get_contents($outputFileName, false /*use include path*/, null /*context*/, 0 /*offset*/, 800 /*length*/);
				XLogError("FahAppClient::downloadPHP server responded with failure: ".XVarDump($http_response_header[0]).", error message in content: ".XVarDump($errorMessageContents));
				return false;
			}			
		}
		//------------------
		XLogDebug("FahAppClient::downloadPHP read completed in ".$timer->restartMs(true)." streamResult: ".XVarDump($streamResult));
		//------------------
		return true;
	}
	//---------------------------
	function extractBz2File($inFile)
	{
		//------------------
		if (!file_exists($this->cmdBZip2))
		{
			XLogError("FahAppClient::extractBz2File configured bzip2 command not found: ".XVarDump($this->cmdBZip2));
			return false;
		}
		//------------------
		$cmd = $this->cmdBZip2." -d -f '$inFile' 2>&1"; // -d decompress, -f force overwrite existing output file
		//------------------
		if (FAH_APP_SIMPLE_EXEC === true)
		{
			//------------------
			$output = array();
			$rVal = XExecSimple($cmd, $output);
			if ($rVal !== 0)
			{
				XLogError("FahAppClient::extractBz2File XExecSimple failed: val: ".XVarDump($rVal).", output: ".XVarDump($output).", cmd: $cmd");
				return false;
			}
			//------------------
		}
		else // not simple exec
		{
			//------------------
			$rData = XExecute($cmd);
			if ($rData[0] === false)
			{
				XLogError("FahAppClient::extractBz2File XExecute failed: ".$rData[3]);
				return false;
			}
			//------------------
			if ($rData[0] !== 0)
			{
				XLogError("FahAppClient::extractBz2File XExecute command failed, code ".$rData[0].", error reply stdout: ".$rData[1].", reply sterr: ".$rData[2]);
				XLogDebug("FahAppClient::extractBz2File XExecute command failed (cont), inFile: $inFile , cmd: $cmd");
				return false;			
			}
			//------------------
		}
		//------------------
		return true;
	}
	//---------------------------
	function compressBz2File($inFile)
	{
		//------------------
		if (!file_exists($this->cmdBZip2))
		{
			XLogError("FahAppClient::compressBz2File configured bzip2 command not found: ".XVarDump($this->cmdBZip2));
			return false;
		}
		//------------------
		$cmd = $this->cmdBZip2." --best -f '$inFile' 2>&1"; // -f force overwrite existing output file
		//------------------
		if (FAH_APP_SIMPLE_EXEC === true)
		{
			//------------------
			$output = array();
			$rVal = XExecSimple($cmd, $output);
			if ($rVal !== 0)
			{
				XLogError("FahAppClient::compressBz2File XExecSimple failed: val: ".XVarDump($rVal).", output: ".XVarDump($output));
				return false;
			}
			//------------------
		}
		else // not simple exec
		{
			//------------------
			$rData = XExecute($cmd);
			if ($rData[0] === false)
			{
				XLogError("FahAppClient::compressBz2File XExecute failed: ".$rData[3]);
				return false;
			}
			//------------------
			if ($rData[0] !== 0)
			{
				XLogError("FahAppClient::compressBz2File XExecute command failed, code ".$rData[0].", error reply stdout: ".$rData[1].", reply sterr: ".$rData[2]);
				XLogDebug("FahAppClient::compressBz2File XExecute command failed (cont), inFile: $inFile , cmd: $cmd");
				return false;			
			}
			//------------------
		}
		//------------------
		return true;
	}
	//---------------------------
	function checkRateLimit($nonRoundStatsMode = false, $noRateLimitFail = false)
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$cfgLastPoll = ($nonRoundStatsMode === false ? CFG_LAST_FAH_APP_POLL : CFG_LAST_FAH_APP_STATS_POLL);
		//------------------
		$lastPoll = $Config->Get($cfgLastPoll);
		if ($lastPoll === false || $lastPoll === "")
		{
			 XLogWarn("FahAppClient::checkRateLimit No Rate limit info statsMode ".XVarDump($nonRoundStatsMode));
			 return true;
		}
		//------------------
		$d = XDateTimeDiff($lastPoll, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
		//------------------
		if ($d === false)
		{
			 XLogError("FahAppClient::checkRateLimit XDateTimeDiff failed. lastPoll: '$lastPoll'");
			 return false;
		}
		//------------------
		if ($d < FAH_APP_POLL_RATE_LIMIT)
		{
			if ($noRateLimitFail === false)
				XLogWarn("FahAppClient::checkRateLimit rate limited. ($cfgLastPoll) minutes since last fah poll: $d");
			return $d;
		}
		//------------------
		return true;
	}
	//---------------------------
	function updateRateLimit($nonRoundStatsMode = false)
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$cfgLastPoll = ($nonRoundStatsMode === false ? CFG_LAST_FAH_APP_POLL : CFG_LAST_FAH_APP_STATS_POLL);
		//------------------
		//XLogDebug("FahAppClient::updateRateLimit statsMode ".XVarDump($nonRoundStatsMode));
		$dtNow = new DateTime('now', new DateTimeZone('UTC'));
		if (!$Config->Set($cfgLastPoll, $dtNow->format(MYSQL_DATETIME_FORMAT)))
		{
			 XLogError("FahAppClient::updateRateLimit Config Set Last Fah App Poll failed. statsMode ".XVarDump($nonRoundStatsMode));
			 return true;
		}
		//------------------
		return true;
	}
	//---------------------------
	function grepTeamMembers($teamId, $inFile, $outFile)
	{
		//------------------
		if (!file_exists($this->cmdGrep))
		{
			XLogError("FahAppClient::grepTeamMembers configured grep command not found: ".XVarDump($this->cmdGrep));
			return false;
		}
		//------------------
		$cmd = $this->cmdGrep." \"\\s$teamId$\" $inFile 2>&1 > $outFile";  // redirects stderr to stdout before stdout is redirected to the file
		//------------------
		if (FAH_APP_SIMPLE_EXEC === true)
		{
			//------------------
			$output = array();
			$rVal = XExecSimple($cmd, $output);
			if ($rVal !== 0)
			{
				XLogError("FahAppClient::grepTeamMembers XExecSimple failed: val: ".XVarDump($rVal).", output: ".XVarDump($output));
				return false;
			}
			//------------------
		}
		else // not simple exec
		{
			//------------------
			$rData = XExecute($cmd);
			if ($rData[0] === false)
			{
				XLogError("FahAppClient::grepTeamMembers XExecute failed: ".$rData[3]);
				return false;
			}
			//------------------
			if ($rData[0] !== 0)
			{
				XLogError("FahAppClient::grepTeamMembers XExecute command failed, code ".$rData[0].", error reply stdout: ".$rData[1].", reply sterr: ".$rData[2]);
				XLogDebug("FahAppClient::grepTeamMembers XExecute command failed (cont), teamId: ".XVarDump($teamId).", inFile: '$inFile', outFile '$outFile', cmd: $cmd");
				return false;			
			}
			//------------------
		}
		//------------------
		return true;
	}
	//---------------------------
	function readData($teamId, $fileName, $reparseMode = false)
	{
		//------------------
		$data = array();
		//------------------
		$hf = fopen($fileName, 'r');
		if ($hf === false)
		{
			XLogError("FahAppClient::readData fopen failed to open input file: $fileName");
			return false;
		}
		//------------------
		if ( ($reparseMode === false || abs($reparseMode) != 2 /*not grepped 4 col*/) && (FAH_APP_TEST === false && defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false) )
			$ExpectedColumnCount = 3;
		else
			$ExpectedColumnCount = 4;
		//------------------
		$linenum = 0;
		$fail = false;
		while (!feof($hf) && !$fail)
		{
			//------------------
			$line = fgets($hf);
			if ($line !== false)
			{
				//------------------
				$cols = explode("\t", $line);
				//------------------
				if (sizeof($cols) != $ExpectedColumnCount)
				{
					XLogError("FahAppClient::readData invalid column count linenum: $linenum, col count: ".sizeof($cols)." (expected $ExpectedColumnCount reparseMode ".XVarDump($reparseMode)."), line: '$line', teamid: $teamId, cols: ".XVarDump($cols));
					$fail = true;
				}
				else $data[] = array(trim($cols[0]), trim($cols[1]), trim($cols[2])); // name, score, wu
				//------------------
			}
			else if (!feof($hf)) // $line was false
			{
				XLogError("FahAppClient::readData fgets failed");
				$fail = true;
			}
			//------------------
			$linenum++;
			//------------------
		} // while
		//------------------
		if (!fclose($hf))
			XLogWarn("FahAppClient::readData fclose input file handle failed");
		//------------------
		if ($fail !== false)
		{
			XLogError("FahAppClient::readData read data failed");
			return false;
		}
		//------------------
		return $data;
	}
	//---------------------------
	function processData($rIdx, $data, $reparse = false)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		$timer2 = new XTimer();
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		$Workers = new Workers() or die ("Create object failed");
		$Wallet = new Wallet() or die ("Create object failed");
		$Config = new Config() or die("Create object failed");
		//------------------
		$round = $Rounds->getRound($rIdx);
		if ($round === false)
		{
			XLogError("FahAppClient::processData - roundIdx not found: $rIdx");
			return false;
		}
		//---------------------------------
		if ($round->teamId === false || !is_numeric($round->teamId))
		{
			XLogError("FahAppClient::processData - validate round teamId failed: ".XVarDump($round->teamId));
			return false;
		}
		//---------------------------------
		$workerNameList = $Workers->getWorkerNameIDList();
		if ($workerNameList === false)
		{
			XLogError("FahAppClient::processData Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		if (!$NewRoundData->Clear())
		{
			XLogError("FahAppClient::processData NewRoundData Clear failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processData  prep/init in ".$timer->restartMs(true));
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processData validate db beginTransaction failed");
			return false;
		}
		//------------------
		$emptyUserNameCount = 0;
		//------------------
		foreach ($data as $dataEntry)
		{
			//------------------
			$userName  = $dataEntry[0];
			$userScore = $dataEntry[1];
			$userWU	   = $dataEntry[2];
			//------------------
			if (!strlen(trim($userName)))
			{
				XLogWarn("FahAppClient::processData ignoring dataEntry with empty user name: ".XVarDump($dataEntry));
				$emptyUserNameCount++;
				continue;
			}
			//------------------
			if (!is_numeric($userScore) || !is_numeric($userWU))
			{
				XLogError("FahAppClient::processData validate dataEntry values numeric failed: ".XVarDump($dataEntry));
				return false;					
			}
			//------------------
			if (!isset($workerNameList[$userName]))
			{
				if (!$NewRoundData->addDataEntry(false/*workerID*/, $userName, NEWDATA_WORKERSTATUS_NEW/*workerStatus*/, $userWU, $userScore,  NEWDATA_POINTSTATUS_NEW /*pointsStatus*/))
				{
					XLogError("FahAppClient::processData NewRoundData addDataEntry failed");
					return false;					
				} 
			}
			else
			{
				if (!$NewRoundData->addDataEntry($workerNameList[$userName], false /*workerName*/, NEWDATA_WORKERSTATUS_EXISTING /*workerStatus*/, $userWU, $userScore, NEWDATA_POINTSTATUS_NEW /*pointsStatus*/))
				{
					XLogError("FahAppClient::processData NewRoundData addDataEntry failed");
					return false;					
				} 
			}
			//------------------
		} // foreach
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processData validate db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processData : first pass, data entry count ".sizeof($data).", took ".$timer->restartMs(true));
		//------------------
		if ($emptyUserNameCount != 0)
		{
			$Monitor = new Monitor() or die ('Create object failed');
			if (!$Monitor->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUND_DATA_INVALID, $Monitor->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'FahAppClient processData', MONEVENT_DETAIL_FIELD => 'fah user name', MONEVENT_DETAIL_COUNT => $emptyUserNameCount, MONEVENT_DETAIL_NOTE => 'Found folder data for team members with empty user names'))))
			{
				XLogError("FahAppClient::processData Monitor addEvent (empty user names) failed");
				return false;
			}
		}
		//------------------
		return true;
	}
	//---------------------------
	function processStatsData($data, $teamId)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$Team = new Team() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		//------------------
		$timer = new XTimer();
		$workerNameList = $Workers->getWorkerNameIDList();
		if ($workerNameList === false)
		{
			XLogError("FahAppClient::processStatsData Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processStatsData Workers getWorkerNameIDList found ".sizeof($workerNameList)." took ".$timer->restartMs(true));
		//------------------
		$trimming = "0";
		$adding = "0";
		$timer2 = new XTimer();
		$strDate = new DateTime('now',  new DateTimeZone('UTC'));
		$strDate = $strDate->format(MYSQL_DATETIME_FORMAT);
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processStatsData beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($data as $dataEntry)
		{
			//------------------
			$userName  = $dataEntry[0];
			$userScore = $dataEntry[1];
			$userWU	   = $dataEntry[2];
			//------------------
			$timer2->start();
			if (!strlen(trim($userName)))
			{
				XLogWarn("FahAppClient::processStatsData ignoring dataEntry with empty user name: ".XVarDump($dataEntry));
				continue;
			}
			//------------------
			$trimming = bcadd($trimming, $timer2->elapsedMs(false));
			if (!is_numeric($userScore) || !is_numeric($userWU))
			{
				XLogError("FahAppClient::processStatsData validate dataEntry values numeric failed: ".XVarDump($dataEntry));
				return false;					
			}
			//------------------
			//------------------
			if (isset($workerNameList[$userName])) // ignore new workers for now
			{
				$timer2->start();
				if (!$Team->addUserDonor($workerNameList[$userName], $userScore, $userWU, $strDate))
				{
					XLogError("FahAppClient::processStatsData Team addUserDonor failed");
					return false;					
				}
				$adding = bcadd($adding, $timer2->elapsedMs(false));
			}
			//------------------
		} // foreach ($data as $dataEntry)
		//------------------
		XLogDebug("FahAppClient::processStatsData processed ".sizeof($data)." data entries took ".$timer->restartMs(true).", with $trimming trimming and $adding adding");
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processStatsData validate db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processStatsData commit took ".$timer->restartMs(true));
		//------------------
		return true;
	}
	function getProcessNewWorkersCount()
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		//------------------
		$count = $NewRoundData->getWorkerStatusCount(NEWDATA_WORKERSTATUS_NEW/*maxWorkerStatus*/);
		if ($count === false)
		{
			XLogError("FahAppClient::getProcessNewWorkersCount NewRoundData getWorkerStatusCount failed");
			return false;
		}
		//------------------
		return $count;
	}
	//---------------------------
	function processNewWorkers($countLimit)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Workers = new Workers() or die ("Create object failed");
		//------------------
		$newWorkers = $NewRoundData->getWorkerStatus(NEWDATA_WORKERSTATUS_NEW/*maxWorkerStatus*/, $countLimit);
		if ($newWorkers === false)
		{
			XLogError("FahAppClient::processNewWorkers NewRoundData getWorkerStatus failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processNewWorkers get limit of ".XVarDump($countLimit)." new data worker status took ".$timer->restartMs(true));
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processNewWorkers validate db beginTransaction failed");
			return false;
		}
		//------------------
		$newWorkersCount = sizeof($newWorkers);
		foreach ($newWorkers as $newWorker)
			if (!$Workers->addWorker($newWorker["wname"], false/*address*/, "<FAC>", false/*reloadWorkers*/))
			{
				XLogError("FahAppClient::processNewWorkers Workers addWorker failed for NewWorker: ".XVarDump($newWorker));
				return false;					
			}
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processNewWorkers validate db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processNewWorkers added $newWorkersCount new workers took ".$timer->restartMs(true));
		//------------------
		$workerNameList = $Workers->getWorkerNameIDList();
		if ($workerNameList === false)
		{
			XLogError("FahAppClient::processNewWorkers Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processNewWorkers get workerNameList took ".$timer->restartMs(true));
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processNewWorkers validate db beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($newWorkers as $newWorker)
		{
			//------------------
			if (!isset($workerNameList[$newWorker["wname"]]))
			{
				XLogError("FahAppClient::processNewWorkers newly added worker missing from workerNameList. NewWorker: ".XVarDump($newWorker));
				return false;
			}
			//------------------
			$workerID = $workerNameList[$newWorker["wname"]];
			//------------------
			if (!is_numeric($workerID))
			{
				XLogError("FahAppClient::processNewWorkers validate workerID from workerNameList failed. NewWorker: ".XVarDump($newWorker).", workerID: ".XVarDump($workerID));
				return false;
			}
			//------------------
			if (!$NewRoundData->updateNewWorker($newWorker["id"], $workerID, NEWDATA_WORKERSTATUS_EXISTNEW))
			{
				XLogError("FahAppClient::processNewWorkers NewRoundData updateNewWorker failed for NewWorker: ".XVarDump($newWorker));
				return false;					
			}
			//------------------
		}
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processNewWorkers validate db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processNewWorkers updated workerID and status of $newWorkersCount new data workers took ".$timer->restartMs(true));
		//------------------
		return $newWorkersCount;
	}
	//---------------------------
	function getProcessValidateWorkersCount()
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$count = $NewRoundData->getWorkerStatusCount(NEWDATA_WORKERSTATUS_NOT_VALIDATED/*maxWorkerStatus*/);
		if ($count === false)
		{
			XLogError("FahAppClient::getProcessValidateWorkersCount NewRoundData getWorkerStatusCount failed");
			return false;
		}
		//------------------
		return $count;
	}
	//---------------------------
	function processValidateWorkers($rIdx, $countLimit)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		$timer2 = new XTimer();
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Workers = new Workers() or die ("Create object failed");
		$Config = new Config() or die ("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		//------------------
		$validationMode = $Config->Get(CFG_WORKER_VALIDATION_MODE);
		if ($validationMode === false)
		{
			$validationMode = DEF_CFG_WORKER_VALIDATION_MODE;
			if (!$Config->Set(CFG_WORKER_VALIDATION_MODE, $validationMode))
			{
				XLogError("FahAppClient::processValidateWorkers Config Set default worker validation mode failed");
				return false;
			}
		}
		//------------------
		$round = $Rounds->getRound($rIdx);
		if ($round === false)
		{
			XLogError("FahAppClient::processValidateWorkers - roundIdx not found: $rIdx");
			return false;
		}
		//------------------
		$dataWorkers = $NewRoundData->getWorkerStatus(NEWDATA_WORKERSTATUS_NOT_VALIDATED/*maxWorkerStatus*/, $countLimit);
		if ($dataWorkers === false)
		{
			XLogError("FahAppClient::processValidateWorkers NewRoundData getWorkerStatus failed");
			return false;
		}
		$dataWorkerCount = sizeof($dataWorkers);
		//------------------
		$timer->start();
		$workersList = $Workers->getWorkers();
		if ($workersList === false)
		{
			XLogError("FahAppClient::processValidateWorkers Workers getWorkers failed");
			return false;
		}
		//------------------
		$workers = array();
		foreach ($workersList as $w)
			$workers[$w->id] = $w;
		//------------------
		XLogDebug("FahAppClient::processValidateWorkers get and list all workers took ".$timer->restartMs(true));
		//------------------
		$timeWorkerLookup = 0;
		$timeValidating = 0;
		$timeUpdating = 0;
		$dataWorkerUpdates = array();
		//------------------
		foreach ($dataWorkers as $dataWorker)
		{
			//------------------
			if (!isset($dataWorker["wid"]) || !is_numeric($dataWorker["wid"]))
			{
				XLogError("FahAppClient::processValidateWorkers validate data worker's workerID failed. New data name ".XVarDump($dataWorker["wname"]).", workerID: ".XVarDump($dataWorker["wid"]));
				return false;
			}
			//------------------
			$wid = $dataWorker["wid"];
			//------------------
			$timer2->start();
			//------------------
			if (!isset($workers[$wid]))
			{
				XLogError("FahAppClient::processValidateWorkers workerID not found in workers list. New data name ".XVarDump($dataWorker["wname"]).", workerID: $wid");
				return false;
			}
			//------------------
			$worker = $workers[$wid];
			$timeWorkerLookup += $timer2->restartMs();
			//------------------
			if (!$worker->validateAddress($validationMode))
			{
				XLogError("FahAppClient::processValidateWorkers - worker failed to validateAddress for: ".$worker->uname);
				return false;
			}
			//------------------
			$isValidAddress = ($worker->validAddress && $worker->address != "");
			$skip = false;
			//------------------
			if ($worker->disabled)
				$skip = 10; 
			//------------------
			if (!$isValidAddress)
				$skip = 1;
			//------------------
			if ($dataWorker["status"] == NEWDATA_WORKERSTATUS_EXISTNEW)
				$newStatus = ($isValidAddress ? NEWDATA_WORKERSTATUS_VALIDNEW : NEWDATA_WORKERSTATUS_INVALIDNEW);
			else
				$newStatus = ($isValidAddress ? NEWDATA_WORKERSTATUS_VALID : NEWDATA_WORKERSTATUS_INVALID);
			//------------------
			$timeValidating += $timer2->restartMs();
			//------------------
			$dataWorkerUpdates[] = array($wid, $newStatus /*workerStatus*/, ($skip !== false ? NEWDATA_POINTSTATUS_SKIPPED : false) /*pointsStatus*/);
			//------------------
		} // foreach
		//------------------
		$timer2->start();
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processValidateWorkers validate db beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($dataWorkerUpdates as $dwup)
			if (!$NewRoundData->updateStatuses($dwup[0], $dwup[1], $dwup[2]))
			{
				XLogError("FahAppClient::processValidateWorkers - NewRoundData updateWorkerStatus failed for data worker: ".XVarDump($dwup[0]));
				return false;
			}
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processValidateWorkers validate db commit failed");
			return false;
		}
		//------------------
		$timeUpdating = $timer2->elapsedMs();
		//------------------
		XLogDebug("FahAppClient::processValidateWorkers validating $dataWorkerCount addresses took ".$timer->elapsedMs(true).", with $timeWorkerLookup looking up workers, $timeValidating validating, and $timeUpdating updating");
		//------------------
		return $dataWorkerCount;
	}
	//---------------------------
	function getProcessSimpleWeekStatsCount()
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$count = $NewRoundData->getPointsStatusCount(NEWDATA_POINTSTATUS_NEW/*maxPointsStatus*/);
		if ($count === false)
		{
			XLogError("FahAppClient::getProcessSimpleWeekStatsCount NewRoundData getPointsStatusCount failed");
			return false;
		}
		//------------------
		return $count;
	}
	//---------------------------
	function processSimpleWeekStats($rIdx, $countLimit)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		//------------------
		$round = $Rounds->getRound($rIdx);
		if ($round === false)
		{
			XLogError("FahAppClient::processSimpleWeekStats - roundIdx not found: $rIdx");
			return false;
		}
		//------------------
		$dataWorkers = $NewRoundData->getPointsStatus(NEWDATA_POINTSTATUS_NEW/*maxPointsStatus*/, $countLimit);
		if ($dataWorkers === false)
		{
			XLogError("FahAppClient::processSimpleWeekStats NewRoundData getPointsStatus failed");
			return false;
		}
		$dataWorkerCount = sizeof($dataWorkers);
		//------------------
		$rIdxLast = $Rounds->getLastRoundIndex(true /*done*/, $rIdx /*beforeIdx*/); // returns false on fail, -1 for none, or round index
		if ($rIdxLast === false)
		{
			XLogError("FahAppClient::processSimpleWeekStats getLastRoundIndex failed for last rIdx: ".$rIdx);
			return false;
		}
		//------------------
		if ($rIdxLast === -1) // if there was no previous round, all deep search
		{
			//------------------
			$timer->start();
			//------------------
			foreach ($dataWorkers as $dataWorker)
				if (!$NewRoundData->updatePointsStatus($dataWorker["wid"], NEWDATA_POINTSTATUS_NEEDDEEP))
				{
					XLogError("FahAppClient::processSimpleWeekStats NewRoundData updatePointsStatus failed for (no prev round) dataWorker: ".XVarDump($dataWorker));
					return false;
				}
			//------------------
			XLogDebug("FahAppClient::processSimpleWeekStats updated all $dataWorkerCount new round data for deep search took ".$timer->restartMs(true));
			//------------------
			return $dataWorkerCount;
		}
		//------------------
		$roundLast = $Rounds->getRound($rIdxLast);
		if ($roundLast === false)
		{
			XLogError("FahAppClient::processSimpleWeekStats roundIdx not found: $rIdxLast");
			return false;
		}
		//------------------
		$timer->start();
		//------------------
		$lastStats = $Stats->findRoundStatsPointSummary($rIdxLast, true /*workerIdxAsIndex*/);
		if ($lastStats === false)
		{
			XLogError("FahAppClient::processSimpleWeekStats Stats findRoundStatsPointSummary failed for last rIdx: $rIdxLast");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processSimpleWeekStats comparing rounds $rIdx and $rIdxLast. Looked up ".sizeof($lastStats)." old stat points took ".$timer->restartMs(true));
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("FahAppClient::processSimpleWeekStats db beginTransaction failed");
			return false;
		}
		//------------------
		$foundCount = 0;
		$deepCount = 0;
		foreach ($dataWorkers as $dataWorker)
		{
			//------------------
			$wid = $dataWorker["wid"];
			$lastStat = (isset($lastStats[$wid]) ?  $lastStats[$wid] : false);
			//------------------
			if ($lastStat === false) // not found, deep search
			{
				//------------------
				if (!$NewRoundData->updatePointsStatus($wid, NEWDATA_POINTSTATUS_NEEDDEEP))
				{
					XLogError("FahAppClient::processSimpleWeekStats NewRoundData updatePointsStatus failed for (no lastStat) dataWorker: ".XVarDump($dataWorker));
					return false;
				}
				//------------------
				$deepCount++;
				//------------------
			}
			else // if $lastStat found
			{
				//------------------
				if (!is_numeric($dataWorker["points"]) || !is_numeric($lastStat["tp"]))
				{
					XLogError("FahAppClient::processSimpleWeekStats validate points numeric for lastStat and dataWorker failed. LastStat ".XVarDump($lastStat).", dataWorker ".XVarDump($dataWorker));
					return false;
				}
				//------------------
				$weekDiff = $dataWorker["points"] - $lastStat["tp"];
				//------------------
				if ($weekDiff < 0)
				{
					XLogError("FahAppClient::processSimpleWeekStats points anomaly for dataWorker: ".XVarDump($dataWorker).", and lastStat: ".XVarDump($lastStat).", $weekDiff = ".$dataWorker["points"]." - ".$lastStat["tp"]);
					return false;
				}
				//------------------
				if (!$NewRoundData->updatePointsStatus($wid, NEWDATA_POINTSTATUS_GOTWEEK, $weekDiff))
				{
					XLogError("FahAppClient::processSimpleWeekStats NewRoundData updatePointsStatus failed for (got week) dataWorker: ".XVarDump($dataWorker));
					return false;
				}
				//------------------
				$foundCount++;
				//------------------
			} // else // if $lastStat found
			//------------------
		} // foreach
		//------------------
		if (!$db->commit())
		{
			XLogError("FahAppClient::processSimpleWeekStats db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processSimpleWeekStats checked $dataWorkerCount data workers, finding points for $foundCount and marking $deepCount for deep search took ".$timer->restartMs(true));
		//------------------
		return $dataWorkerCount;
	}
	//---------------------------
	function getProcessDeepSearchStatsCount()
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$count = $NewRoundData->getPointsStatusCount(NEWDATA_POINTSTATUS_NEEDDEEP/*maxPointsStatus*/);
		if ($count === false)
		{
			XLogError("FahAppClient::getProcessDeepSearchStatsCount NewRoundData getPointsStatusCount failed");
			return false;
		}
		//------------------
		return $count;
	}
	//---------------------------
	function processDeepSearchStats($rIdx, $countLimit)
	{
		global $db; // for beginTransaction/commit
		//------------------
		$timer = new XTimer();
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		//------------------
		$dataWorkers = $NewRoundData->getPointsStatus(NEWDATA_POINTSTATUS_NEEDDEEP/*maxPointsStatus*/, $countLimit);
		if ($dataWorkers === false)
		{
			XLogError("FahAppClient::processDeepSearchStats NewRoundData getPointsStatus failed");
			return false;
		}
		$dataWorkerCount = sizeof($dataWorkers);
		//------------------
		XLogDebug("FahAppClient::processDeepSearchStats get limit of ".XVarDump($countLimit)." new data worker statuses took ".$timer->restartMs(true));
		//------------------
		$noStatCount = 0;
		$deepSearchCount = 0;
		//------------------
		foreach ($dataWorkers as $dataWorker)
		{
			//------------------
			$statIdx = $Stats->workerHasStats($dataWorker["wid"]); // returns first found stat ID or -1 for none
			//------------------
			if ($statIdx === false)
			{
				XLogError("FahAppClient::processDeepSearchStats Stats workerHasStats dataWorker failed: ".XVarDump($dataWorker));
				return false;
			}
			//------------------
			if (!is_numeric($statIdx))
			{
				XLogError("FahAppClient::processDeepSearchStats statIdx for dataWorker failed. deep result ".XVarDump($statIdx).", dataWorker ".XVarDump($dataWorker));
				return false;
			}
			//------------------
			if ($statIdx == -1)
			{
				$deepResult = 0;
				$noStatCount++;
			}
			else
			{
				//------------------
				$deepResult = $Stats->deepSearchLastTotalPoints($rIdx, $dataWorker["wid"]);
				if ($deepResult === false)
				{
					XLogError("FahAppClient::processDeepSearchStats Stats deepSearchLastTotalPoints dataWorker failed: ".XVarDump($dataWorker));
					return false;
				}
				//------------------
				if (!is_numeric($deepResult) || !is_numeric($dataWorker["points"]))
				{
					XLogError("FahAppClient::processDeepSearchStats validate points numeric for deep result and dataWorker failed. deep result ".XVarDump($deepResult).", dataWorker ".XVarDump($dataWorker));
					return false;
				}
				//------------------
				$deepSearchCount++;
				//------------------
			}
			//------------------
			$weekDiff = $dataWorker["points"] - $deepResult;
			//------------------
			if ($weekDiff < 0)
			{
				XLogError("FahAppClient::processDeepSearchStats points anomaly for dataWorker: ".XVarDump($dataWorker).", and deep result: ".XVarDump($deepResult).", statCount: $statCount, $weekDiff = ".$dataWorker["points"]." - ".$deepResult);
				return false;
			}
			//------------------
			if (!$NewRoundData->updatePointsStatus($dataWorker["wid"], NEWDATA_POINTSTATUS_GOTWEEK, $weekDiff))
			{
				XLogError("FahAppClient::processDeepSearchStats NewRoundData updatePointsStatus failed for dataWorker: ".XVarDump($dataWorker));
				return false;
			}
			//------------------
		} // foreach
		//------------------
		XLogDebug("FahAppClient::processDeepSearchStats $noStatCount with no stats and $deepSearchCount deep searchs of $dataWorkerCount data workers took ".$timer->restartMs(true));
		//------------------
		return $dataWorkerCount;
	}
	//---------------------------
	function getProcessAddStatsCount()
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$count = $NewRoundData->getPointsStatusCount(NEWDATA_POINTSTATUS_GOTWEEK/*maxPointsStatus*/);
		if ($count === false)
		{
			XLogError("FahAppClient::getProcessAddStatsCount NewRoundData getPointsStatusCount failed");
			return false;
		}
		//------------------
		return $count;
	}
	//---------------------------
	function processAddStats($rIdx, $countLimit)
	{
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		//------------------
		$addCount = $NewRoundData->addStatsWithWeek($rIdx, $countLimit);
		if ($addCount === false)
		{
			XLogError("FahAppClient::processAddStats NewRoundData addStatsWithWeek failed");
			return false;
		}
		//------------------
		return $addCount;
	}
	//---------------------------
} // class FahAppClient
//---------------------------
?>
