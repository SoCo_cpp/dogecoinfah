<?php
/*
 *	www/include/Wallet.php
 * 
 * 
* 
*/
//---------------
define('RPC_DEBUG_FAIL', true);
define('RPC_DEBUG_VERBOSE_CONNECT_FAILS', false); // normally false for minimal output on simple connection failures
define('RPC_DEBUG_SUCCESS', false);
define('RPC_DEBUG_CREDENTIALS', false);
//---------------
define('WALLET_HTTP_REPLY_TIMEOUT_SECS', 5);  
define('WALLET_HTTP_READ_TIMEOUT_USECS', 250000);  
//---------------
define('DEF_CFG_WALLET_USE_GETINFO', 0);
define('DEF_CFG_WALLET_MIN_CONF', 3);
define('DEF_CFG_WALLET_EST_FEE', 5);
define('DEF_CFG_WALLET_EST_CONT_FEE', 2);
define('DEF_CFG_WALLET_FEE_PER_KB',	1.0);
define('DEF_CFG_WALLET_MIN_FEE', 1.0);
define('DEF_CFG_WALLET_EXTRA_FEE', 0.0);
define('DEF_CFG_WALLET_MIN_OUTPUT', 1.0);
//---------------
define('CFG_WALLET_MAIN_ADDRESS',	'wallet-main-address');
define('CFG_WALLET_STORE_ADDRESS',	'wallet-store-address');
define('CFG_WALLET_USE_GETINFO',	'wallet-use-getinfo');
define('CFG_WALLET_MIN_CONF',		'wallet-min-conf');
define('CFG_WALLET_EST_FEE',		'wallet-est-fee');
define('CFG_WALLET_EST_CONT_FEE',	'wallet-est-cont-fee');
define('CFG_WALLET_FEE_PER_KB',		'wallet-fee-per-kb');
define('CFG_WALLET_MIN_FEE',		'wallet-min-fee');
define('CFG_WALLET_EXTRA_FEE',		'wallet-extra-fee');
define('CFG_WALLET_MIN_OUTPUT',		'wallet-min-output'); // (used with change)
//---------------
define('WALLET_AUTO_MODE_NONE', 		0);
define('WALLET_AUTO_MODE_FLAT', 		1);
define('WALLET_AUTO_MODE_PERCENT', 		2);
//---------------
define('WALLET_AUTO_FLAG_NONE', 		0);
define('WALLET_AUTO_FLAG_REQUIRED', 	1);
//------------------------ User defined special log type
function XLogRPC($msg)
{
	XLogSpecial($msg, 'RPC'); // from XLoggerInstance.php
}
//---------------
class Wallet
{
	var $Config = false;
	var $mainAddress = false;
	var $storeAddress = false;
	var $feePerKb = false;
	var $minFee = false;
	var $extraFee = false;
	var $minConf = false;
	var $minOutput = false;
	var $estfee = false;
	var $estcontfee = false;
	var $sendTxResult = false;
	var $sendTxLastStep = false;
	var $ssh = false;
	var $sodium = false;
	//------------------
	function __construct()
	{
		$this->Config = new Config() or die("Create object failed");
	}
	//------------------
	function getFeePerKb()
	{
		if ($this->feePerKb === false)
		{
			$this->feePerKb = $this->Config->GetSetDefault(CFG_WALLET_FEE_PER_KB, DEF_CFG_WALLET_FEE_PER_KB);
			if ($this->feePerKb === false)
				XLogError("Wallet::getFeePerKb GetSetDefault failed");
		}
		return $this->feePerKb;	
	}
	//------------------
	function getMinFee()
	{
		if ($this->minFee === false)
		{
			$this->minFee = $this->Config->GetSetDefault(CFG_WALLET_MIN_FEE, DEF_CFG_WALLET_MIN_FEE);
			if ($this->minFee === false)
				XLogError("Wallet::getMinFee GetSetDefault failed");
		}
		return $this->minFee;	
	}
	//------------------
	function getExtraFee()
	{
		if ($this->extraFee === false)
		{
			$this->extraFee = $this->Config->GetSetDefault(CFG_WALLET_EXTRA_FEE, DEF_CFG_WALLET_EXTRA_FEE);
			if ($this->extraFee === false)
				XLogError("Wallet::extraFee GetSetDefault failed");
		}
		return $this->extraFee;	
	}
	//------------------
	function getMinConf()
	{
		if ($this->minConf === false)
		{
			$this->minConf = $this->Config->GetSetDefault(CFG_WALLET_MIN_CONF, DEF_CFG_WALLET_MIN_CONF);
			if ($this->minConf === false)
				XLogError("Wallet::getMinConf GetSetDefault failed");
		}
		return $this->minConf;	
	}
	//------------------
	function getMinOutput()
	{
		if ($this->minOutput === false)
		{
			$this->minOutput = $this->Config->GetSetDefault(CFG_WALLET_MIN_OUTPUT, DEF_CFG_WALLET_MIN_OUTPUT);
			if ($this->minOutput === false)
				XLogError("Wallet::minOutput GetSetDefault failed");
		}
		return $this->minOutput;
	}
	//------------------
	function getEstFee()
	{
		if ($this->estfee === false)
		{
			$this->estfee = $this->Config->GetSetDefault(CFG_WALLET_EST_FEE, DEF_CFG_WALLET_EST_FEE);
			if ($this->estfee === false)
				XLogError("Wallet::getEstFee GetSetDefault failed");
		}
		return $this->estfee;	
	}
	//------------------
	function getEstContFee()
	{
		if ($this->estcontfee === false)
		{
			$this->estcontfee = $this->Config->GetSetDefault(CFG_WALLET_EST_CONT_FEE, DEF_CFG_WALLET_EST_CONT_FEE);
			if ($this->estcontfee === false)
				XLogError("Wallet::getEstContFee GetSetDefault failed");
		}
		return $this->estcontfee;	
	}
	//------------------
	function setEstFee($estFee)
	{
		//------------------
		if (!is_numeric($estFee))
		{
			XLogError("Wallet::setEstFee validate estfee is numeric failed");
			return false;
		}
		//------------------
		$this->estfee = $estFee;
		if (!$this->Config->Set(CFG_WALLET_EST_FEE, $this->estfee))
		{
			XLogError("Wallet::setEstFee Config Set estfee failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function setEstContFee($estContFee)
	{
		//------------------
		if (!is_numeric($estContFee))
		{
			XLogError("Wallet::setEstContFee validate estContFee is numeric failed");
			return false;
		}
		//------------------
		$this->estcontfee = $estContFee;
		if (!$this->Config->Set(CFG_WALLET_EST_CONT_FEE, $this->estcontfee))
		{
			XLogError("Wallet::setEsContFee Config Set estcontfee failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function setMinConf($minConf)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_MIN_CONF, $minConf))
		{
			XLogError("Wallet::setMinConf Config Set minConf failed");
			return false;
		}
		//------------------
		$this->minConf = $minConf;
		//------------------
		return true;
	}
	//------------------
	function setMinOutput($minOutput)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_MIN_OUTPUT, $minOutput))
		{
			XLogError("Wallet::setMinOutput Config Set minOutput failed");
			return false;
		}
		//------------------
		$this->minOutput = $minOutput;
		//------------------
		return true;
	}
	//------------------
	function setMinFee($minFee)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_MIN_FEE, $minFee))
		{
			XLogError("Wallet::setMinFee Config Set minFee failed");
			return false;
		}
		//------------------
		$this->minFee = $minFee;
		//------------------
		return true;
	}
	//------------------
	function setFeePerKb($feePerKb)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_FEE_PER_KB, $feePerKb))
		{
			XLogError("Wallet::setFeePerKb Config Set feePerKb failed");
			return false;
		}
		//------------------
		$this->feePerKb = $feePerKb;
		//------------------
		return true;
	}
	//------------------
	function setExtraFee($extraFee)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_EXTRA_FEE, $extraFee))
		{
			XLogError("Wallet::setExtraFee Config Set extraFee failed");
			return false;
		}
		//------------------
		$this->extraFee = $extraFee;
		//------------------
		return true;
	}
	//------------------
	function getUseGetInfo()
	{
		$value = $this->Config->GetSetDefault(CFG_WALLET_USE_GETINFO, DEF_CFG_WALLET_USE_GETINFO);
		if ($value === false)
			XLogError("Wallet::getUseGetInfo GetSetDefault failed");
		return ($value !== "0" ? true : false);	
	}
	//------------------
	function Init()
	{
		//------------------
		// ignore result, just ensure defaults are set
		$this->getFeePerKb();
		$this->getMinFee();
		$this->getMinConf();
		$this->getEstFee();
		$this->getEstContFee();
		$this->getUseGetInfo();
		//------------------
		return true;
	}
	//------------------
	function isValidAddressQuick($address)
	{        
		if (strlen($address) > C_MAX_WALLET_ADDRESS_LENGTH/*DatabaseDefines.php*/)
			return false;
		$addr = $this->decodeBase58($address);
		if (strlen($addr) != 50)
		  return false;
		$check = substr($addr, 0, strlen($addr) - 8);
		$check = pack("H*", $check);
		$check = strtoupper(hash("sha256", hash("sha256", $check, true)));
		$check = substr($check, 0, 8);
		return $check == substr($addr, strlen($addr) - 8);
	}
	//------------------
	function bcDecToHex($dec)
	{
		$hexchars = "0123456789ABCDEF";
		$val = "";
		while (bccomp($dec, 0) == 1)
		{
			$dv = (string) bcdiv($dec, "16", 0);
			$rem = (integer) bcmod($dec, "16");
			$dec = $dv;
			$val = $val.$hexchars[$rem];
		}
		return strrev($val);
	}
	//------------------
	function decodeBase58($base58)
	{
		$origbase58 = $base58;    
		$base58chars = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"; 
		$val = "0";
		for ($i = 0; $i < strlen($base58); $i++)
		{
		  $current = (string) strpos($base58chars, $base58[$i]);
		  $val = (string) bcmul($val, "58", 0);
		  $val = (string) bcadd($val, $current, 0);
		}
		$val = $this->bcDecToHex($val);
		//leading zeros
		for ($i = 0; $i < strlen($origbase58) && $origbase58[$i] == "1"; $i++)
		  $val = "00" . $val;
		if (strlen($val) % 2 != 0)
		  $val = "0" . $val;
		return $val;
	}
	//------------------
	function JSONtoAmount($value) 
	{
		// see https://en.bitcoin.it/wiki/Proper_Money_Handling_(JSON-RPC)#PHP
		return round($value * 1e8);
	}	
	//------------------
	function getSSH()
	{
		//------------------
		if (!defined('WALLET_SSH_TUNNEL') || WALLET_SSH_TUNNEL === false)
		{
			XLogError("Wallet::getSSH WALLET_SSH_TUNNEL is not enabled");
			return false;
		}
		//------------------
		if (!defined('WALLET_SSH_HOST') || !defined('WALLET_SSH_PORT') || !defined('WALLET_SSH_USER') || WALLET_SSH_HOST == '' || !is_numeric(WALLET_SSH_PORT) || WALLET_SSH_USER == '')
		{
			XLogError("Wallet::getSSH WALLET_SSH_TUNNEL is enabled but validate WALLET_SSH_HOST, WALLET_SSH_PORT, and WALLET_SSH_USER failed. Check Settings.php.");
			return false;
		}
		//------------------
		if (defined('WALLET_SSH_SERVER_FINGERPRINT') && WALLET_SSH_SERVER_FINGERPRINT !== false && WALLET_SSH_SERVER_FINGERPRINT != '')
			$serverFP = WALLET_SSH_SERVER_FINGERPRINT;
		else
			$serverFP = false;
		//------------------
		if (defined('WALLET_SSH_PASSWORD') && WALLET_SSH_PASSWORD !== false && WALLET_SSH_PASSWORD != '') // use basic user/pass SSH auth
		{
			//------------------
			$ssh = new ConnectionSSH(true /*debugMode*/) or die('Create object failed');
			if (!$ssh->configure(WALLET_SSH_HOST, WALLET_SSH_PORT, WALLET_SSH_USER, WALLET_SSH_PASSWORD, $serverFP))
			{
				XLogError("Wallet::getSSH SSH configure (user/pass) failed: ".$ssh->lastError());
				return false;
			}
			//------------------
		}
		else
		{
			//------------------
			if (!defined('WALLET_SSH_PUB_KEY_FILE') || !defined('WALLET_SSH_PRIV_KEY_FILE') || WALLET_SSH_PUB_KEY_FILE == '' || WALLET_SSH_PRIV_KEY_FILE == '')
			{
				XLogError("Wallet::getSSH basic user/pass auth not enabled, but validate WALLET_SSH_PUB_KEY_FILE and WALLET_SSH_PRIV_KEY_FILE failed. Check Settings.php.");
				return false;
			}
			//------------------
			if (defined('WALLET_SSH_PRIV_KEY_PASSWORD') && WALLET_SSH_PRIV_KEY_PASSWORD !== false && WALLET_SSH_PRIV_KEY_PASSWORD != '')
				$passphrase = WALLET_SSH_PRIV_KEY_PASSWORD;
			else
				$passphrase = false;
			//------------------
			$ssh = new ConnectionSSH(true /*debugMode*/) or die('Create object failed');
			if (!$ssh->configure(WALLET_SSH_HOST, WALLET_SSH_PORT, WALLET_SSH_USER, false /*password*/, $serverFP, WALLET_SSH_PUB_KEY_FILE, WALLET_SSH_PRIV_KEY_FILE, $passphrase))
			{
				XLogError("Wallet::getSSH SSH configure (pub/priv key) failed: ".$ssh->lastError());
				return false;
			}
			//------------------
		}
		//------------------
		return $ssh;
	}
	//------------------
	function getSodium()
	{
		//------------------
		if (!defined('WALLET_SODIUM_TUNNEL') || WALLET_SODIUM_TUNNEL === false)
		{
			XLogError("Wallet::getSodium WALLET_SODIUM_TUNNEL is not enabled");
			return false;
		}
		//------------------
		if (!defined('WALLET_SODIUM_HOST') || !defined('WALLET_SODIUM_USER') || !defined('WALLET_SODIUM_PUB_KEY') || !defined('WALLET_SODIUM_PRIV_KEY') || WALLET_SODIUM_HOST == '' || WALLET_SODIUM_USER == '' || WALLET_SODIUM_PUB_KEY == '' || WALLET_SODIUM_PRIV_KEY == '')
		{
			XLogError("Wallet::getSodium WALLET_SODIUM_TUNNEL is enabled but validate WALLET_SODIUM_HOST, WALLET_SODIUM_USER, WALLET_SODIUM_PUB_KEY, and WALLET_SODIUM_PRIV_KEY failed. Check Settings.php.");
			return false;
		}
		//------------------
		if (!defined('WALLET_SODIUM_SERVER_USER') || !defined('WALLET_SODIUM_SERVER_PUB_KEY') || WALLET_SODIUM_SERVER_USER == '' || WALLET_SODIUM_SERVER_PUB_KEY == '')
		{
			XLogError("Wallet::getSodium WALLET_SODIUM_TUNNEL is enabled but validate WALLET_SODIUM_SERVER_USER and WALLET_SODIUM_SERVER_PUB_KEY failed. Check Settings.php.");
			return false;
		}
		//------------------
		$sodium = new SodiumTunneler(true /*debugMode*/) or die('Create object failed');
		//------------------
		$sodium->setAddresses(WALLET_SODIUM_HOST, 'http://'.WALLET_RPC_HOST.':'.WALLET_RPC_PORT.'/');
		//------------------
		if (!$sodium->setAuth(WALLET_SODIUM_USER, $sodium->decode64(WALLET_SODIUM_PUB_KEY), $sodium->decode64(WALLET_SODIUM_PRIV_KEY)))
		{
			XLogError("Wallet::getSodium sodium setAuth failed: ".$sodium->lastError());
			return false;
		}
		//------------------
		if (!$sodium->addRemoteAuth(WALLET_SODIUM_SERVER_USER, $sodium->decode64(WALLET_SODIUM_SERVER_PUB_KEY)))
		{
			XLogError("Wallet::getSodium sodium addRemoteAuth failed: ".$sodium->lastError());
			return false;
		}
		//------------------
		return $sodium;
		
	}
	//------------------
	function execWallet($command, $paramArray = array(), $forceDebug = false, $suppressDebug = false)
	{
		$http = new HTTPHandler(true /*debug*/, true /*minimalLimits*/) or die ('Creae object failed');
		//------------------
		if (!is_array($paramArray) || !is_string($command)) 
		{
			XLogDebug("Wallet::execWallet validate params failed: cmd ".XVarDump($command).", params ".XVarDump($paramArray));
		}
		//------------------
		$debugFail = (!$suppressDebug && ($forceDebug || RPC_DEBUG_FAIL) ? true : false);
		$debugSuccess = (!$suppressDebug && ($forceDebug || RPC_DEBUG_SUCCESS) ? true : false);
		//$debugFail = true;
		//$debugSuccess = true;
		//------------------
		$paramArray = array_values($paramArray); // ensure no array keys
		//------------------
		$request = array(
						'method' => $command,
						'params' => $paramArray,
						'id' => 1
						);
		//------------------
		$request = json_encode($request);
		//------------------
		if ($debugSuccess)
			XLogDebug("Wallet::execWallet request: ".XVarDump($request));
		//------------------
		if (!defined('WALLET_RPC_AUTH') || WALLET_RPC_AUTH === false)
			$rpcAuth = false;
		else if (!defined('WALLET_RPC_USER') || !defined('WALLET_RPC_PASS') || WALLET_RPC_USER == '' || WALLET_RPC_PASS == '')
		{
			// always fail on bad setup
			XLogError("Wallet::execWallet WALLET_RPC_AUTH is enabled but validate WALLET_RPC_USER and WALLET_RPC_PASS failed. Check Settings.php.");
			return false;
		}
		else $rpcAuth = WALLET_RPC_USER.':'.WALLET_RPC_PASS;
		//------------------
		$headers = array('Host: '.WALLET_RPC_HOST.':'.WALLET_RPC_PORT);
		if ($rpcAuth !== false) 
			$headers[] = 'Authorization: Basic '.base64_encode($rpcAuth);
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Content-Length: '.strlen($request);
		//------------------
		$httpRequest = $http->buildPOST(false /*path*/, $request /*content*/, $headers);
		//------------------
		//XLogDebug("Wallet::execWallet httpRequest:\r\n".XVarDump($httpRequest));
		//------------------
		if (defined('WALLET_SODIUM_TUNNEL') && WALLET_SODIUM_TUNNEL !== false)
		{
			if ($this->sodium === false)
				$this->sodium = $this->getSodium();
			if ($this->sodium === false)
			{
				XLogError("Wallet::execWallet getSodium failed");
				return false;
			}
			//------------------
			$httpReply = $this->sodium->sendRequest($httpRequest);
			if ($httpReply === false)
			{
				if ($debugFail)
					XLogError("Wallet::execWallet sodium sendRequest failed: ".$this->sodium->lastError());
				return false;
			}
			//------------------			
			//XLogDebug("Wallet::execWallet (sodium) httpReply:\r\n".XVarDump($httpReply, false));
			//------------------
		}
		else if (defined('WALLET_SSH_TUNNEL') && WALLET_SSH_TUNNEL !== false)
		{
			$stream = false;
			if ($this->ssh !== false)
			{
				$stream = $this->ssh->tunnel(WALLET_RPC_HOST, WALLET_RPC_PORT);
				if ($stream === false) // can't re-make tunnel, assume ssh connection died, try from start
				{
					$this->ssh->disconnect();
					$this->ssh = null;
					unset($this->ssh);
					$this->ssh = false;
				}
			}
			if ($stream === false)
			{
				$ssh = $this->getSSH();
				if ($ssh === false)
				{
					XLogError("Wallet::execWallet getSSH failed");
					return false;
				}
				if (!$ssh->connect())
				{
					if ($debugFail)
						XLogError("Wallet::execWallet ssh connect failed: ".$ssh->lastError());
					return false;
				}
				$stream = $ssh->tunnel(WALLET_RPC_HOST, WALLET_RPC_PORT);
				if ($stream === false)
				{
					if ($debugFail)
						XLogError("Wallet::execWallet ssh tunnel failed: ".$ssh->lastError());
					return false;
				}
				$this->ssh = $ssh;
			}
			//------------------
			$httpReply = $http->requestReply($stream, $httpRequest);
			@fclose($stream);
			if ($httpReply === false)
			{
				if ($debugFail)
					XLogError("Wallet::execWallet (ssh tunnel) http requestReply failed: ".$http->lastError());
				return false;
			}
			//------------------
			//XLogDebug("Wallet::execWallet (ssh tunnel) httpReply:\r\n".XVarDump($httpReply, false));
			//------------------
		}
		else
		{
			$stream = @fsockopen(WALLET_RPC_HOST, WALLET_RPC_PORT, $rErrorCode, $rErrorString);
			if ($stream === false)
			{
				if ($debugFail)
					XLogError("Wallet::execWallet fsockopen failed with error ($rErrorCode): $rErrorString");
				return false;
			}
			//------------------
			$httpReply = $http->requestReply($stream, $httpRequest);
			@fclose($stream);
			if ($httpReply === false)
			{
				if ($debugFail)
					XLogError("Wallet::execWallet (no tunnel) http requestReply failed: ".$http->lastError());
				return false;
			}
			//------------------
			//XLogDebug("Wallet::execWallet (no tunnel) httpReply:\r\n".XVarDump($httpReply, false));
			//------------------
		}
		//------------------
		//XLogDebug("Wallet::execWallet httpReply:\r\n".XVarDump($httpReply, false));
		//------------------
		$replySummary = $http->parseReply($httpReply);
		if ($replySummary === false)
		{
			if ($debugFail)
				XLogError("Wallet::execWallet http parseReply failed: ".$http->lastError());
			return false;
		}
		if ($replySummary['status'] != 200)
		{
			if ($debugFail)
				XLogError("Wallet::execWallet reply returned a HTTP server fail code: ".XVarDump($replySummary['status']));
			return false;
		}
		$reply = $replySummary['content'];
		//------------------
		//XLogDebug("Wallet::execWallet http parsed reply:\r\n".XVarDump($reply));
		//------------------
		$json = json_decode($reply, true);
		if ($json === null)
		{
			if ($debugFail)
				XLogError("Wallet::execWallet json_decode response failed. response: ".XVarDump($reply)."\r\nhttpReply: ".XVarDump($httpReply, false));
			return false;
		}
		//------------------
		if ($debugSuccess)
			XLogDebug("Wallet::execWallet response: ".XVarDump($json));
		//------------------
		if (isset($json['error']) && !is_null($json['error']))
		{
			if ($debugFail)
				XLogError("Wallet::execWallet responded with error: ".XVarDump($json['error']));
			return false;
		}
		//------------------
		if (!isset($json['result']))
		{
			if ($debugFail)
				XLogError("Wallet::execWallet response missing result field: ".XVarDump($json));
			return false;
		}
		//------------------
		XLogRPC("Request: $command ".XVarDump($paramArray)." ## Result: ".XVarDump($json['result']));
		//------------------
		return $json['result'];		
	}
	//------------------
	function getInfo()
	{
		//------------------
		if ($this->getUseGetInfo() == true)
			return $this->execWallet("getinfo");
		//------------------
		$netInfo = $this->execWallet("getnetworkinfo");
		if ($netInfo === false)
		{
			XLogError("Wallet::getInfo execWallet getnetworkinfo failed");
			return false;
		}
		//------------------
		$walletInfo = $this->execWallet("getwalletinfo");
		if ($walletInfo === false)
		{
			XLogError("Wallet::getInfo execWallet getwalletinfo failed");
			return false;
		}
		//------------------
		$chainInfo = $this->execWallet("getblockchaininfo");
		if ($chainInfo === false)
		{
			XLogError("Wallet::getInfo execWallet getblockchaininfo failed");
			return false;
		}
		//------------------
		$bestBlockHash = XArray($chainInfo, 'bestblockhash', "");
		if ($bestBlockHash == "")
			$bestBlockTime = 0;
		else
		{
			$bestBlockInfo = $this->execWallet("getblock", array("".$bestBlockHash));
			if ($bestBlockInfo === false)
			{
				XLogWarn("Wallet::getInfo getblock of chain bestBlockHash failed");
				$bestBlockTime = 0;	
			}
			else 
			{
				$bestBlockTime = XArray($bestBlockInfo, 'time', false);
				if (!is_numeric($bestBlockTime))
				{
					XLogWarn("Wallet::getInfo bestBlockTime is not a number: ".XVarDump($bestBlockTime));
					$bestBlockTime = 0;	
				}
				else $bestBlockTime = (int)$bestBlockTime; // ensure int 
			}
		}
		if ($bestBlockTime == 0)
			$strChainSyncTime = "(unknown)";
		else 
			$strChainSyncTime = XDateDiffFuzzy($bestBlockTime, false/*NOW*/, false/*UTC*/, 'n'/*minUnits (minutes)*/, 12 /*min*/, 'ready'/*min reply*/);
		//------------------
		$info = array();
		$info['version']			= XArray($netInfo, 'version', "");
		$info['protocolversion']	= XArray($netInfo, 'protocolversion', "");
		$info['walletversion']		= XArray($walletInfo, 'walletversion', "");
		$info['balance']			= XArray($walletInfo, 'balance', "");
		$info['blocks']				= XArray($chainInfo, 'blocks', "");
		$info['until synced']		= $strChainSyncTime;
		$info['connections']		= XArray($netInfo, 'connections', "");
		$info['chain']				= XArray($chainInfo, 'chain', "");
		$info['unlocked_until']		= XArray($walletInfo, 'unlocked_until', "");
		$info['paytxfee']			= XArray($walletInfo, 'paytxfee', "");
		$info['relayfee']			= XArray($netInfo, 'relayfee', "");
		$info['errors']				= XArray($netInfo, 'warnings', "");
		//------------------
		return $info;
	}
	//------------------
	function getBalance($address = false, $minconf = false)
	{
		//------------------
		if ($address === false)
			$address = $this->getMainAddress();
		//------------------
		if ($minconf === false)
			$minconf = $this->getMinConf();
		//------------------
		$balances = $this->listUnspentTotals($address, $minconf);
		if ($balances === false)
		{
			XLogError("Wallet::getBalance no balance found, result:".XVarDump($balances));
			return false;
		}
		//------------------
		// no unspents for address
		if (!isset($balances[$address]))
			return 0.0;
		//------------------
		return $balances[$address];
	}
	//------------------
	function getLabelOfAddress($address = false)
	{
		//------------------
		if ($address === false)
			$address = $this->getMainAddress();
		//------------------
		return $this->execWallet("getaccount", array($address));
	}
	//------------------
	function generateAddresss($label = "")
	{
		//------------------
		return $this->execWallet("getnewaddress", array($label));
	}
	//------------------
	function listAccounts()
	{
		//------------------
		return $this->execWallet("listaccounts");
	}
	//------------------
	function send($fromAddress, $toAddress, $amount, $testOnly = false, $minconf = false, $comment = false)
	{
		//------------------
		if ($fromAddress === false)
			$mainAddress = $this->getMainAddress();
		//------------------
		if (!is_numeric($amount) || !is_string($fromAddress) || !is_string($toAddress))
		{
			XLogError("Wallet::send validate parameters failed. Amount ".XVarDump($amount).", fromAddress ".XVarDump($fromAddress).", toAddress ".XVarDump($toAddress));
			return false;
		}
		//------------------
		$amount = (float)$amount; // ensure float
		//------------------
		$outputs = array( $toAddress => (float)$amount );
		$result = $this->sendTransaction($fromAddress, $fromAddress, $outputs, $testOnly);
		if (!$result)
		{
			XLogError("Wallet::send sendTransaction failed");
			XLogWarn("Wallet::send sendTxResult: ".XVarDump($this->sendTxResult));
			return false;
		}
		//------------------
		return $result; // should be txid		
	}
	//------------------
	function sendMany($fromAccount, $addressValues, $minconf = false, $comment = false)
	{
		//------------------
		XLogWarn("Wallet::sendMany this operation is depreciated!");
		//------------------
		if ($fromAccount === false)
			$fromAccount = $this->getMainAddress();
		//------------------
		if (sizeof($addressValues) == 0)
		{
			XLogError("Wallet::sendMany addressValues is empty");
			return false;
		}
		//------------------
		$fromAccount .= ""; // ensure string type
		//------------------
		$result = $this->sendTransaction($fromAddress, $fromAddress, $addressValues);
		if (!$result)
		{
			XLogError("Wallet::sendMany sendTransaction failed");
			XLogWarn("Wallet::sendMany sendTxResult: ".XVarDump($this->sendTxResult));
			return false;
		}
		//------------------
		return $result; // should be txid
	}
	//------------------
	function getNewAddress($account = false)
	{
		//------------------
		if ($account === false)
			$params = array();
		else
		{
			$account .= ""; // ensure string type
			$params = array($account);
		}
		//------------------
		return $this->execWallet("getnewaddress", $params); 
	}
	//------------------
	function getAccountAddress($account = false)
	{
		//------------------
		if ($account === false)
			return $this->getMainAddress();
		//------------------
		$account .= ""; // ensure string type
		$params = array($account);
		//------------------
		return $this->execWallet("getaddressesbyaccount", $params);
	}
	//------------------
	function setFee($fee)
	{
		//------------------
		if (!is_numeric($fee))
		{
			XLogError("Wallet::setFee validate that fee is numberic failed: '$fee'");
			return false;
		}
		//------------------
		$params = array((double)$fee);
		//------------------
		return $this->execWallet("settxfee", $params);
	}
	//------------------
	function getFee()
	{
		//------------------
		$result = $this->getInfo();
		if ($result === false)
		{
			XLogError("Wallet::getFee getInfo failed");
			return false;
		}
		//------------------
		if (!isset($result["paytxfee"]))
		{
			XLogError("Wallet::getFee paytxfee not found in result");
			return false;
		}
		//------------------
		return $result["paytxfee"];
	}
	//------------------
	function getTotalFees()
	{
		//------------------
		$result = $this->getInfo();
		if ($result === false)
		{
			XLogError("Wallet::getTotalFees getInfo failed");
			return false;
		}
		//------------------
		if (!isset($result["paytxfee"]))
		{
			XLogError("Wallet::getTotalFees paytxfee not found in result");
			return false;
		}
		//------------------
		$rlFee = (isset($result["relayfee"]) ? $result["relayfee"] : "0.0");
		//------------------
		$txFee = $result["paytxfee"];
		$totFee = bcadd($txFee, $rlFee, 8);
		//------------------
		return $totFee;
	}
	//------------------
	function getMainAddress()
	{
		//------------------
		if ($this->mainAddress === false)
		{
			$this->mainAddress = $this->Config->Get(CFG_WALLET_MAIN_ADDRESS, ""/*default blank*/);
			if ($this->mainAddress === false)
				XLogError("Wallet::getMainAddress Config Get failed");
		}
		//------------------
		return $this->mainAddress;
	}
	//------------------
	function setMainAddress($address)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_MAIN_ADDRESS, $address))
		{
			XLogError("Wallet::setMainAddress Config set failed");
			return false;
		}
		//------------------
		$this->mainAddress = $address;
		//------------------
		return true;
	}
	//------------------
	function getStoreAddress()
	{
		//------------------
		if ($this->storeAddress === false)
		{
			$this->storeAddress = $this->Config->Get(CFG_WALLET_STORE_ADDRESS, ""/*default blank*/);
			if ($this->storeAddress === false)
				XLogError("Wallet::getStoreAddress Config set failed");
		}
		//------------------
		return $this->storeAddress;
	}
	//------------------
	function setStoreAddress($address)
	{
		//------------------
		if (!$this->Config->Set(CFG_WALLET_STORE_ADDRESS, $address))
		{
			XLogError("Wallet::setStoreAddress Config set failed");
			return false;
		}
		//------------------
		$this->storeAddress = $address;
		//------------------
		return true;
	}
	//------------------
 	function validateAddress($address)
	{        
		//------------------
		$address .= ""; // ensure string type
		$params = array($address);
		//------------------
		$result = $this->execWallet("validateaddress", $params, false/*forceDebug*/, true/*suppressDebug*/);
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::validateAddress execWallet failed");
			return false;
		}
		//------------------
		if (!isset($result["isvalid"]))
		{
			XLogError("Wallet::validateAddress isvalid not found in result");
			return false;
		}
		//------------------
		return ($result["isvalid"] == true ? $address : "");
	}
	//------------------
 	function isValidAddress($address)
	{        
		//------------------
		if (!$this->isValidAddressQuick($address))
			return "";
		//------------------
		$result = $this->validateAddress($address);
		//------------------
		return $result;
	}
	//------------------
	function listUnspent($address = false, $minConf = 3)
	{
		//------------------
		$listunspent = $this->execWallet("listunspent");
		//------------------
		if ($listunspent === false)
		{
			XLogError("Wallet::listUnspent execWallet failed");
			return false;
		}
		//------------------
		if (!is_array($listunspent))
		{
			XLogError("Wallet::listUnspent validate reply list failed");
			return false;
		}
		//------------------
		$unspents = array();
		foreach ($listunspent as $unspent)
		{
			//------------------
			if (!isset($unspent["txid"]) || !isset($unspent["vout"]) || !isset($unspent["address"]) || !isset($unspent["amount"]) || !isset($unspent["confirmations"]) || !isset($unspent["spendable"]))
			{
				XLogError("Wallet::listUnspent unspent is missing required field");
				return false;				
			}
			//------------------
			if (!is_numeric($unspent["vout"]) || !is_numeric($unspent["amount"]) || !is_numeric($unspent["confirmations"]) || !is_bool($unspent["spendable"]))
			{
				XLogError("Wallet::listUnspent verify fields failed");
				return false;				
			}
			//------------------
			//XLogDebug("Wallet::listUnspent - ".XVarDump($unspent));
			if ( ($address === false || $address == $unspent["address"]) && $unspent["confirmations"] >= $minConf && $unspent["spendable"] && $unspent["amount"] > 0.0)
				$unspents[] = array("address"=>$unspent["address"], "amount"=>$unspent["amount"], "txid"=>$unspent["txid"], "vout"=>$unspent["vout"]);
			//------------------
		}
		//------------------
		return $unspents;
	}
	//------------------
	function listUnspentTotals($address = false, $minConf = 3)
	{
		$totals = array();
		//------------------
		$unspents = $this->listUnspent($address, $minConf);
		if ($unspents === false)
		{
			XLogError("Wallet::listUnspentTotals verify fields failed");
			return false;				
		}
		//------------------
		foreach ($unspents as $unspent)
		{
			if (!isset($totals[$unspent["address"]]))
				$totals[$unspent["address"]] = 0.0;
			$totals[$unspent["address"]] += $unspent["amount"];
		}
		//------------------
		return $totals;
	}
	//------------------
	function rawTransaction($inputs, $outputs)
	{
		//------------------
		$params = array($inputs, $outputs);
		//------------------
		$result = $this->execWallet("createrawtransaction", $params);
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::rawTransaction execWallet failed");
			return false;
		}
		//------------------
		return $result;
	}
	//------------------
	function decodeRawTransaction($rawTransaction)
	{
		//------------------
		$result = $this->execWallet("decoderawtransaction", array($rawTransaction));
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::decodeRawTransaction execWallet failed");
			return false;
		}
		//------------------
		return $result;
	}
	//------------------
	function signRawTransaction($rawTransaction)
	{
		//------------------
		$result = $this->execWallet("signrawtransaction", array($rawTransaction));
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::signRawTransaction execWallet failed");
			return false;
		}
		//------------------
		return $result;
	}
	//------------------
	function sendRawTransaction($rawTransaction)
	{
		//------------------
		$result = $this->execWallet("sendrawtransaction", array($rawTransaction));
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::sendRawTransaction execWallet failed");
			return false;
		}
		//------------------
		return $result;
	}
	//------------------
	function chooseUnspents($fromAddress, $bcValue, $giveInsufficientList = false)
	{
		$chosen = array();
		$total = "0";
		XLogDebug("Wallet::chooseUnspent from $fromAddress, value $bcValue");
		//------------------
		$listUnspents = $this->listUnspent($fromAddress);
		if ($listUnspents === false)
		{
			XLogError("Wallet::chooseUnspents listUnspent failed");
			return false;
		}
		//------------------
		$i = 0;
		while ($i < sizeof($listUnspents) && bccomp($total, $bcValue, 8) == -1 /*total<bcValue*/)
		{
			$chosen[] = $listUnspents[$i];
			$total = bcadd($total, "".$listUnspents[$i]["amount"]/*make string*/, 8);
			//XLogDebug("Wallet::chooseUnspents listUnspents [$i/".sizeof($listUnspents)."] adding: ".substr($listUnspents[$i]["txid"], -5).":".$listUnspents[$i]["vout"]." amount: ".$listUnspents[$i]["amount"]." total: $total/$bcValue, chosen count: ".sizeof($chosen));
			$i++;
		}
		XLogDebug("Wallet::chooseUnspents listUnspents done [$i/".sizeof($listUnspents)."] total/value: $total/$bcValue, chosen count: ".sizeof($chosen));
		//------------------
		if ($giveInsufficientList === false && bccomp($total, $bcValue, 8) == -1 /*total<bcValue*/)
		{
			XLogNotify("Wallet::chooseUnspents not enough. Requested $bcValue, but only $total found available");
			return 0;
		}
		//------------------
		return $chosen;
	}
	//------------------
	// sendTransaction
	// (function recursively called adjusting fee and change)
	//-------
	// changeAddress - only used if needed
	// outputs[] = ["outAddress" => (float)value] 
	// fee - start with minimum fee (ajusted by minFee)
	// change - start at 0
	// depth - used internally for recusion safety
	function sendTransaction($inputAddress, $changeAddress, $outputs, $testOnly = false, $fee = "0", $change = "0", $extraFee = "0", $depth = 0)
	{
		XLogDebug("Wallet::sendTransaction ".($testOnly ? "(test only) " : "")."inpAddr: $inputAddress, chAddr: $changeAddress, change: $change, fee: $fee (extra $extraFee), depth: $depth, outputs: ".XVarDump($outputs));
		//------------------
		$minFee = "".$this->getMinFee(); // ensure string type for bc math
		if (bccomp($fee, $minFee, 8) == -1 /*fee < minFee*/)
		{
			$fee = $minFee;
			if ($extraFee !== "0")
				$fee = bcadd($fee, $extraFee, 8);
		}
		$change = "".$change; // ensure string type for bcd
		//------------------
		if ($depth == 0) // first step
			$this->sendTxLastStep = array('depth'=>$depth, 'action'=>'Initial start');
		//------------------
		$depth++;
		if ($depth > 10)
		{
			XLogError("Wallet::sendTransaction max depth failure");
			$this->sendTxResult = array('sent'=>false,'error'=>'max depth exceeded','testOnly'=>$testOnly,'hasFunds'=>true,'totalIn'=>false, 'totalOut'=>false,'fee'=>$fee, '$xtraFee'=>$extraFee, 'change'=>$change,'rawSize'=>false,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		$totalOut = "0";
		$txOutputs = array();
		foreach ($outputs as $addr => $value)
		{
			$txOutputs[$addr] = $value;
			$totalOut = bcadd($totalOut, "".$value/*make string*/, 8);
		}
		//------------------
		if (bccomp($change, "0", 8) != 0)
		{
			$txOutputs[$changeAddress] = (float)$change; // ensure float
			$totalOut = bcadd($totalOut, $change, 8);
			XLogDebug("Wallet::sendTransaction has change adding to output: change: $change, new total out: $totalOut, change addr: $changeAddress");
		}
		//------------------
		$totalOutWithFee = bcadd($totalOut, $fee, 8);
		$unspents = $this->chooseUnspents($inputAddress, $totalOutWithFee, true /*$giveInsufficientList*/);
		if ($unspents === false)
		{
			XLogError("Wallet::sendTransaction chooseUnspents failed");
			$this->sendTxResult = array('sent'=>false,'error'=>'wallet failure','action'=>'chooseUnspents','testOnly'=>$testOnly,'hasFunds'=>false,'totalIn'=>false, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>false,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		$totalIn = "0";
		$txInputs = array();
		foreach ($unspents as $unspent)
		{
			$txInputs[] = array("txid"=>$unspent["txid"], "vout"=>$unspent["vout"]);
			$totalIn = bcadd($totalIn, "".$unspent["amount"]/*make string*/, 8);
			XLogDebug("Wallet::sendTransaction adding unspent + ".$unspent["amount"]." = $totalIn txid ".substr($unspent["txid"],-5).":".$unspent["vout"]);
		}
		//------------------
		if (bccomp($totalIn, $totalOutWithFee) == -1 /* totalIn < totalOutWithFee*/ )
		{
			XLogError("Wallet::sendTransaction chooseUnspents didn't find enough input. Totals: In $totalIn, Out $totalOut, Attempted Fee $fee, extraFee $extraFee, Expected change $change");
			$this->sendTxResult = array('sent'=>false,'error'=>'not enough funds', 'testOnly'=>$testOnly, 'hasFunds'=>false,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>false,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		$oldChange = $change;
		$change = bcsub($totalIn, $totalOutWithFee, 8);
		if (bccomp($change, "0", 8) != 0)
		{
			//------------------
			$minOutput = "".$this->getMinOutput();
			$change = bcsub($totalIn, bcsub($totalOutWithFee, $oldChange, 8), 8);  // recalc change without old change in totalOutWithFee
			//------------------
			if (bccomp($change, $minOutput, 8) == -1 /*change < minOut*/) // Dogecoin does not allow outputs less than minOutput dust limit normally (requires extra fee)
			{
				XLogDebug("Wallet::sendTransaction change: $change too small, force increasing to $minOutput");
				$change = $minOutput;
			}
			//------------------
			if (bccomp($oldChange, $change, 8) != 0) // recalculate with original outputs and new change amount
			{
				XLogDebug("Wallet::sendTransaction change has changed ($oldChange -> $change, totalIn: $totalIn, totalOut: $totalOut, fee: $fee, extraFee: $extraFee, recalculating");
				$this->sendTxLastStep = array('depth'=>$depth, 'action'=>'Recalc change', 'oldChange'=>$oldChange, 'change'=>$change, 'totalIn'=>$totalIn, 'totalOut'=>$totalOut, 'fee'=>$fee, 'extraFee'=>$extraFee);
				return $this->sendTransaction($inputAddress, $changeAddress, $outputs, $testOnly, $fee, $change, $extraFee, $depth);
			}
			//------------------
		}
		//------------------
		XLogDebug("Wallet::sendTransaction rawTransaction fee: $fee, extraFee: $extraFee, totalIn: $totalIn, inCount: ".sizeof($txInputs).", totalOut: $totalOut, outCount ".sizeof($txOutputs).", change: $change");
		$rawtx = $this->rawTransaction($txInputs, $txOutputs); // $inputs, $outputs
		//------------------
		if ($rawtx === false)
		{
			XLogError("Wallet::sendTransaction rawTransaction failed, trying with higher fee(cur): $fee, extraFee: $extraFee, change: $change, totalIn: $totalIn, totalOut: $totalOut");
			$oldFee = $fee;
			$fee = bcadd($fee, $minFee, 8);
			$this->sendTxLastStep = array('depth'=>$depth, 'action'=>'Raw Trans failed, inc fee', 'change'=>$change, 'totalIn'=>$totalIn, 'totalOut'=>$totalOut, 'oldFee'=>$oldFee, 'fee'=>$fee, 'extraFee'=>$extraFee);
			return $this->sendTransaction($inputAddress, $changeAddress, $outputs, $testOnly, $fee, $change, $extraFee, $depth);
		}
		//------------------
		// check size add fee
		$oldFee = $fee;
		$txSize = strlen($rawtx);
		XLogDebug("Wallet::sendTransaction current transaction size: $txSize");
		$kbFeeCalc = $this->getFeePerKb() * ceil($txSize / 1000.0);
		$fee = "".max($this->getMinFee(), $kbFeeCalc); /*make string*/
		if ($extraFee !== "0")
			$fee = bcadd($fee, $extraFee, 8);
		if (bccomp($oldFee, $fee, 8) != 0) // recalculate with new fee
		{
			XLogDebug("Wallet::sendTransaction fee has changed ($oldFee -> $fee, extraFee: $extraFee, totalIn: $totalIn, totalOut: $totalOut, kbFeeCalc: $kbFeeCalc, txSize: $txSize), recalculating");
			$this->sendTxLastStep = array('depth'=>$depth, 'action'=>'Fee changed by size', 'change'=>$change, 'totalIn'=>$totalIn, 'totalOut'=>$totalOut, 'oldFee'=>$oldFee, 'fee'=>$fee, 'extraFee'=>$extraFee, 'rawSize'=>$txSize, 'kbFeeCalc'=>$kbFeeCalc, 'minFee'=>$this->getMinFee());
			return $this->sendTransaction($inputAddress, $changeAddress, $outputs, $testOnly, $fee, $change, $extraFee, $depth);
		}
		//------------------
		/*
		$decodedtx = $this->decodeRawTransaction($rawtx);
		//------------------
		if ($decodedtx === false)
		{
			XLogError("Wallet::sendTransaction decodeRawTransaction failed");
			return false;
		}
		XLogDebug("Wallet::sendTransaction decodedTx: ".json_encode($decodedtx, JSON_PRETTY_PRINT));
		*/
		//------------------
		XLogDebug("Wallet::sendTransaction fee: $fee, extraFee: $extraFee, totalIn: $totalIn, totalOut: $totalOut, rawTxSize: $txSize, rawTx: ".XVarDump($rawtx));
		$signedtx = $this->signRawTransaction($rawtx);
		//------------------
		if ($signedtx === false)
		{
			XLogError("Wallet::sendTransaction signRawTransaction failed");
			$this->sendTxResult = array('sent'=>false,'error'=>'wallet failed','action'=>'signRawTransaction','testOnly'=>$testOnly,'hasFunds'=>true,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>$txSize,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		if (!isset($signedtx["hex"]) || !isset($signedtx["complete"]) || $signedtx["complete"] !== true)
		{
			XLogError("Wallet::sendTransaction verify signed raw transaction failed");
			XLogWarn(" - ".XVarDump($signedtx));
			$this->sendTxResult = array('sent'=>false,'error'=>'wallet failed','action'=>'validate signed transaction','testOnly'=>$testOnly,'hasFunds'=>true,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>$txSize,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		XLogDebug("Wallet::sendTransaction signedtx: ".XVarDump($signedtx));
		//------------------
		if ($testOnly)
		{
			XLogDebug("Wallet::sendTransaction testOnly done, not sending");
			$this->sendTxResult = array('sent'=>false,'error'=>false,'testOnly'=>true,'hasFunds'=>true,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>$txSize,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return '[Test]';
		}
		//------------------
		$result = $this->sendRawTransaction($signedtx["hex"]);
		//------------------
		if ($result === false)
		{
			XLogError("Wallet::sendTransaction sendRawTransaction failed");
			$this->sendTxResult = array('sent'=>false,'error'=>'wallet failed','action'=>'sendRawTransaction','testOnly'=>false,'hasFunds'=>true,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>$txSize,'txid'=>false,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
			return false;
		}
		//------------------
		XLogDebug("Wallet::sendTransaction result: ".XVarDump($result));
		//------------------
		$this->sendTxResult = array('sent'=>true,'error'=>false,'testOnly'=>false,'hasFunds'=>true,'totalIn'=>$totalIn, 'totalOut'=>$totalOut,'fee'=>$fee,'extraFee'=>$extraFee,'change'=>$change,'rawSize'=>$txSize,'txid'=>$result,'inputAddr'=>$inputAddress,'changeAddr'=>$changeAddress,'depth'=>$depth);
		//------------------
		return $result; // should be txid
	}
	//------------------	
}// class Wallet
?>
