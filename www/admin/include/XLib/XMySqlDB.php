<?php
//-------------------------------------------------------------
// 
//
//-------------------------------------------------------------
if (!defined('XLOGGER_INSTANCE'))
	die('XSession - XLoggerInstance required');
//------------------------------------------------------------------------------
//##############################################################################
//------------------------------------------------------------------------------
class TXMySqlDBResult
{
	//---------------------------------------------------------
	function __construct($tHandle, $tPdo = false)
	{
		$this->pdo = $tPdo;
		$this->HasRowCount = false;
		if (!$tHandle) $this->Initted = false;
		else
		{
			$this->Handle = $tHandle;
			$this->Initted = true;
		}
	}
	//---------------------------------------------------------
	function Release()
	{
		if ($this->Initted)
		{
			if ($this->pdo)
				$this->Handle = NULL;
			else
				@mysql_free_result($this->Handle);
		}
		$this->HasRowCount = false;
		$this->Initted = false;
	}
	//---------------------------------------------------------
	function RowCount()
	{
		if (!$this->Initted)
			return 0;
		if (!$this->HasRowCount)
		{
			if ($this->pdo)
				$this->tRowCount = $this->Handle->rowCount();
			else
				$this->tRowCount = @mysql_num_rows($this->Handle);
		}
		return $this->tRowCount;
	}
	//---------------------------------------------------------
	function GetRow($emptyValue = false, $failValue = false)
	{
		if (!$this->Initted)
		{
			XLogError("TXMySqlDBResult::GetRow not initted");
			return $failValue;
		}
		if (!$this->pdo)
			$row = @mysql_fetch_row($this->Handle);
		else
			$row = $this->Handle->fetch(PDO::FETCH_NUM);
		if ($row === false)
		{
			if (!$this->pdo)
				$errorMsg = @mysql_error($this->Handle);
			else
				$errorMsg = $this->Handle->errorCode();
			
			if ($errorMsg !== null && $errorMsg !== "00000" && $errorMsg != "")
			{
				XLogError("TXMySqlDBResult::GetRow fetch num row failed: $errorMsg");
				return $failValue;
			}
			return $emptyValue;
		}
		if (sizeof($row) == 0)
			return $emptyValue;
		return $row;
	}
	//---------------------------------------------------------
	function GetRowArray($emptyValue = false, $failValue = false)
	{
		if (!$this->Initted)
		{
			XLogError("TXMySqlDBResult::GetRowArray not initted");
			return $failValue;
		}
		if (!$this->pdo)
			$row = @mysql_fetch_assoc($this->Handle);
		else
			$row = $this->Handle->fetch(PDO::FETCH_ASSOC);
		if ($row === false)
		{
			if (!$this->pdo)
				$errorMsg = @mysql_error($this->Handle);
			else
				$errorMsg = $this->Handle->errorCode();
			if ($errorMsg !== null && $errorMsg !== "00000" && $errorMsg != "")
			{
				XLogError("TXMySqlDBResult::GetRowArray fetch assoc failed: $errorMsg");
				return $failValue;
			}
			return $emptyValue;
		}
		if (sizeof($row) == 0)
			return $emptyValue;
		return $row;
	}
	//---------------------------------------------------------
	function GetOneValue($emptyValue = false, $failValue = false, $wantNulls = false)
	{
		$row = $this->GetRow($emptyValue, $failValue);
		if ($row === $failValue && $row !== $emptyValue)
		{
			XLogError("TXMySqlDBResult::GetOneValue GetRow failed");
			return $failValue;
		}
		if ($row === $emptyValue || ($wantNulls === false && $row[0] === null))
			return $emptyValue;
		return $row[0];
	}
	//---------------------------------------------------------
} // class TXMySQLDBResult
//------------------------------------------------------------------------------
//##############################################################################
//------------------------------------------------------------------------------
class TXMySqlDB
{
	//---------------------------------------------------------
	function __construct()
	{
		$this->pdo = false;
		$this->Connected = false;
		$this->Error = false;
		$this->DBSelected = false;
		$this->DBName = false;
	}
	//---------------------------------------------------------
	function sanitize($val) // This doesn't make anything safe, don't rely on this
	{
		if (!$this->Connected)
		{
			XLogError('TMySqlDB::sanitize database not connected');
			return '';
		}
		if ($this->pdo)
		{
			//XLogDebug('TMySqlDB::sanitize - PDO Support detected, using quote');
			return substr($this->Connection->quote($val), 1, -1); // remove quotes
		}
		else 
		{
			//XLogDebug('TMySqlDB::sanitize - falling back on mysql_real_escape_string, instead of PDO quote');
			try
			{
				$r = @mysql_real_escape_string(addslashes($val)); // was XDBSanitize
			}
			catch (Exception $e)
			{
				XLogError('TMySqlDB::sanitize - exception: '.XVarDump($e));
				return '';
			}
			return $r;
		}
	}
	//---------------------------------------------------------
	function errorString()
	{
		if ($this->pdo)
			return ($this->Error !== false ? $this->Error : '');
		return @mysql_error();
	}
	//---------------------------------------------------------
	function pdoErrorInfo($makeString = true, $baseObject = false)
	{
		if (!$this->Connected)
			$errorInfo = array(-1, -1, 'TXMySqlDB::pdoErrorInfo Database not connected');
		else if ($this->pdo)
			$errorInfo = array(-2, -2, 'TXMySqlDB::pdoErrorInfo Database not in PDO mode');
		else
		{
			if ($baseObject === false)
				$baseObject = $this->Connection;
			if ($baseObject === null or !is_object($baseObject))
				$errorInfo = array(-3, -3, 'TXMySqlDB::pdoErrorInfo validate baseObject failed (parameter of PDOStatement or default to PDO object expected');
			else
				$errorInfo = $baseObject->errorInfo();
		}
		if ($makeString)
			$errorInfo = implode(', ', $errorInfo);
		return $errorInfo;
	}
	//---------------------------------------------------------
	function Connect($tHost, $tUser, $tPass, $tDatabase = false)
	{
		$this->Error = false;
		if ($this->Connected)
			$this->Disconnect();
		$this->DBName = $tDatabase;
		if (defined('PDO::ATTR_DRIVER_NAME'))
		{
			$this->pdo = true;
			//XLogDebug('TMySqlDB::Connect - PDO Support detected');
			$conString = 'mysql:host='.$tHost;
			if ($this->DBName !== false)
				$conString .= ';dbname='.$this->DBName;
			try
			{
				$this->Connection = new PDO($conString, $tUser, $tPass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
				$this->Connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);  // To get maximum sql injection protection
			}
			catch (PDOException $e)
			{
				$this->Error = 'TXMySqlDB::Connect create pdo connection failed: '.$e->getMessage();
				XLogError('TXMySqlDB::Connect create pdo connection failed: '.$e->getMessage().', connection string: '.$conString);
				return false;
			}
			if ($this->DBName !== false)
				$this->DBSelected = true;
		}
		else
		{
			if (!function_exists('mysql_connect')) 
			{
				XLogError('TXMySqlDB::Connect - undefined function: mysql_connect().  Please install PDO or the MySQL Connector for PHP'); 
				return false;
			}
			XLogDebug('TMySqlDB::Connect - falling back on mysql_*, instead of PDO');
			$this->Connection = @mysql_connect($tHost, $tUser, $tPass);
			if (!$this->Connection)
			{
				XLogError('TXMySqlDB::Connect - mysql_connect failed: '.$this->errorString());
				return false;
			}
			if ($this->DBName !== false)
				if (!$this->SelectDatabase($this->DBName))
				{
					XLogError('TXMySqlDB::Connect - SelectDatabase failed');
					return false;
				}
		}
		$this->Connected = true;
		return true;
	}
	//---------------------------------------------------------
	function Disconnect()
	{
		if ($this->Connected && $this->Connection)
		{
			if (!$this->pdo)
				$this->Connection = NULL;
			else
			{
				if (!function_exists('mysql_close')) 
					@mysql_close($this->Connection);
			}
		}
		$this->Connected = false;
		$this->DBSelected = false;
		$this->DBName = false;
	}
	//---------------------------------------------------------
	function SelectDatabase($tDatabase)
	{
		$this->Error = false;
		if (!$this->pdo)
		{
			if (!$this->Connected)
			{
				$this->Error = 'XMySqlDB::SelectDatabase database not connected';
				return false;
			}
			if (@mysql_select_db($tDatabase)) 
			{
				$this->DBSelected = false;
				XLogError('TXMySqlDB::SelectDatabase - mysql_select_db failed: '.$this->errorString());
				return false;
			}
			$this->DBSelected = true;
			$this->DBName = $tDatabase;
			return true;
		}
		if (!$this->DBSelected || $this->DBName != $tDatabase)
		{
			$this->Error = 'TXMySqlDB::SelectDatabase not supported for PDO connections. Please specify database when calling TXMySqlDB::Connect instead. No previously selected database or not a match.';
			XLogError($this->Error);
			return false;
		}
		return true; // pdo tried, but it was already selected to the same one
	}	
	//---------------------------------------------------------
	function Execute($tQuery, $MustSelectDB = true, $QuietMode = false)
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::Execute - Not connected.';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		if ($MustSelectDB && !$this->DBSelected)
		{
			$this->Error = 'XMySqlDB::Execute - No database selected, but MustSelectDB was enabled.';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		$r = false;
		if (!$this->pdo)
		{
			$r = @mysql_query($tQuery);
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::Execute - mysql_query failed: '.$this->errorString();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}
		}
		else
		{
			try
			{
				$r = $this->Connection->query($tQuery);
			}
			catch (PDOException $e)
			{
				$this->Error = 'XMySqlDB::Execute - query failed: '.$e->getMessage();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}		
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::Execute - query failed: '.$this->pdoErrorInfo();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}
		}
		return true;
	}
	//---------------------------------------------------------
	function Query($tQuery, $MustSelectDB = true, $QuietMode = false)
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::Query - Not connected.';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		if ($MustSelectDB && !$this->DBSelected)
		{
			$this->Error = 'XMySqlDB::Query - No database selected, but MustSelectDB was enabled.';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		$r = false;
		if (!$this->pdo)
		{
			$r = @mysql_query($tQuery, $this->Connection);
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::Query - mysql_query failed: '.$this->errorString();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}
		}
		else
		{
			try
			{
				$r = $this->Connection->query($tQuery);
			}
			catch (PDOException $e)
			{
				$this->Error = 'XMySqlDB::Query - query failed: '.$e->getMessage();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::Query - query failed: '.$this->pdoErrorInfo();
				if (!$QuietMode)
					XLogError($this->Error);
				return false;
			}
		}
		$rr =  new TXMySqlDBResult($r, $this->pdo) or die('XMySqlDB::Query - Create object failed');
		return $rr;
	}
	//---------------------------------------------------------
	function Prepare($tQuery,  $driver_options = array())
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::Prepare - Database not connected';
			XLogError($this->Error);
			return false;
		}
		if (!$this->pdo)
		{
			$this->Error = 'XMySqlDB::Prepare - XMySqlDB support not implemented for non PDO PHP MySQL';
			XLogError($this->Error);
			return false;
		}
		$r = false;
		try
		{
			$r = $this->Connection->prepare($tQuery);
		}
		catch (PDOException $e)
		{
			$this->Error = $e->getMessage();
			XLogError('XMySqlDB::Prepare - prepare failed: '.$e->getMessage());
			return false;
		}
		if ($r === false)
		{
			$this->Error = 'XMySqlDB::Prepare - prepare failed: '.$this->pdoErrorInfo();
			XLogError($this->Error);
			return false;
		}
		return $r;
	}
	//---------------------------------------------------------
	function ExecutePrepared($PDOStatement, $QuietMode = false)
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::ExecutePrepared - Database not connected';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		if (!$this->pdo)
		{
			$this->Error = 'XMySqlDB::ExecutePrepared - XMySqlDB support not implemented for non PDO PHP MySQL';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		if ($PDOStatement == false || is_null($PDOStatement) || !is_object($PDOStatement))
		{
			$this->Error = 'XMySqlDB::ExecutePrepared - validate PDOStatement parameter failed';
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		$r = false;
		try
		{
			$r = @$PDOStatement->execute();
		}
		catch (PDOException $e)
		{
			$this->Error = 'XMySqlDB::ExecutePrepared - PDOStatement execute failed: '.$e->getMessage();
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		if ($r === false)
		{
			$this->Error = 'XMySqlDB::ExecutePrepared - PDOStatement execute failed: '.$this->pdoErrorInfo(true /*makeString*/, $PDOStatement /*baseObject*/);
			if (!$QuietMode)
				XLogError($this->Error);
			return false;
		}
		return true;
	}
	//---------------------------------------------------------
	function DebugDumpStatement($PDOStatement, $flatten = false)
	{
		if (!$this->pdo)
		{
			XLogError('XMySqlDB::DebugDumpStatement - Support not implemented for non PDO PHP MySQL');
			return false;
		}
		if ($PDOStatement == false || is_null($PDOStatement) || !is_object($PDOStatement))
		{
			XLogError('XMySqlDB::DebugDumpStatement - validate PDOStatement failed');
			return false;
		}
		
		ob_start();
		$PDOStatement->debugDumpParams();
		$s = ob_get_contents();
		ob_end_clean();
		if ($flatten === true)
		{
			$lines = explode("\n", $s);
			$s = '';
			foreach ($lines as $l)
				$s .= trim($l)." ";
		}
		return $s;
	}
	//---------------------------------------------------------
	function beginTransaction()
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::beginTransaction - Database not connected';
			XLogError($this->Error);
			return false;
		}
		if ($this->pdo)
		{
			$r = false;
			try
			{
				$r = $this->Connection->beginTransaction();
			}
			catch (PDOException $e)
			{
				$this->Error = 'XMySqlDB::beginTransaction - beginTransaction failed: '.$e->getMessage();
				XLogError($this->Error);
				return false;
			}
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::beginTransaction - beginTransaction failed: '.$this->pdoErrorInfo();
				XLogError($this->Error);
				return false;
			}
		}
		else // non-pdo
		{
			if (!@mysqli_begin_transaction($this->Connection))
			{
				$this->Error = 'XMySqlDB::beginTransaction - mysqli_begin_transaction failed'.$this->errorString();
				XLogError($this->Error);
				return false;
			}
		}
		return true;		
	}
	function commit()
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::commit - Database not connected';
			XLogError($this->Error);
			return false;
		}
		if ($this->pdo)
		{
			$r = false;
			try
			{
				$r = $this->Connection->commit();
			}
			catch (PDOException $e)
			{
				$this->Error = 'XMySqlDB::commit - commit failed: '.$e->getMessage();
				XLogError($this->Error);
				return false;
			}
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::commit - commit failed: '.$this->pdoErrorInfo();
				XLogError($this->Error);
				return false;
			}
		}
		else // non-pdo
		{
			if (!@mysqli_commit($this->Connection))
			{
				$this->Error = 'XMySqlDB::commit - mysqli_commit failed'.$this->errorString();
				XLogError($this->Error);
				return false;
			}
		}
		return true;		
	}
	function rollBack()
	{
		$this->Error = false;
		if (!$this->Connected)
		{
			$this->Error = 'XMySqlDB::rollBack - Database not connected';
			XLogError($this->Error);
			return false;
		}
		if ($this->pdo)
		{
			$r = false;
			try
			{
				$r = $this->Connection->rollBack();
			}
			catch (PDOException $e)
			{
				$this->Error = 'XMySqlDB::rollBack - rollBack failed: '.$e->getMessage();
				XLogError($this->Error);
				return false;
			}
			if ($r === false)
			{
				$this->Error = 'XMySqlDB::rollBack - rollBack failed: '.$this->pdoErrorInfo();
				XLogError($this->Error);
				return false;
			}
		}
		else // non-pdo
		{
			if (!@mysqli_rollback($this->Connection))
			{
				$this->Error = 'XMySqlDB::commit - mysqli_rollback failed'.$this->errorString();
				XLogError($this->Error);
				return false;
			}
		}
		return true;		
	}
	function lastInsertId()
	{
		$this->Error = false;
		if (!$this->pdo)
			return $this->Connection->insert_id;
		$r = false;
		try
		{
			$r = $this->Connection->lastInsertId();
		}
		catch (PDOException $e)
		{
			$this->Error = 'XMySqlDB::lastInsertId - lastInsertId failed: '.$e->getMessage();
			XLogError($this->Error);
			return false;
		}
		if ($r === false && $r = $this->Connection->errorCode() !== null)
		{
			$this->Error = 'XMySqlDB::lastInsertId - lastInsertId failed: '.$this->pdoErrorInfo();
			XLogError($this->Error);
			return false;
		}
		return $r; // this could still return false, while not failing
	}
	function tableExists($tableName, $existsResult = 1, $notExistsResult = 0, $failResult = false, $quietMode = false, $tDatabaseName = false)
	{
		$this->Error = false;
		if ($tDatabaseName === false && (!$this->DBSelected || $this->DBName === false))
		{
			$this->Error = 'XMySqlDB::tableExists - databaseName not specified and database not previously selected';
			XLogError($this->Error);
			return false;
		}
		if ($tDatabaseName === false)
			$tDatabaseName = $this->DBName;
		$qr = $this->Query("SELECT COUNT(*)	FROM information_schema.tables WHERE table_schema='$tDatabaseName' AND table_name='$tableName'");
		if ($qr === false)
		{
			$this->Error = 'XMySqlDB::tableExists - Query failed: '.$this->errorString();
			if (!$quietMode)
				XLogError($this->Error);
			return $failResult;
		}
		$value = $qr->GetOneValue();
		if ($value === false)
		{
			$this->Error = 'XMySqlDB::tableExists - GetOneValue failed';
			if (!$quietMode)
				XLogError($this->Error);
			return $failResult;
		}
		return (is_numeric($value) && $value > 0 ? $existsResult : $notExistsResult);
	}
	function duplicateTable($srcTable, $dstTable, $failExists = true, $clobber = true, $useTransaction = true)
	{
		$this->Error = false;
		if ($failExists)
		{
			$r = $this->tableExists($dstTable);
			if ($r !== 0)
			{
				if ($r === false)
					$this->Error = 'XMySqlDB::duplicateTable - tableExists failed: '.$this->errorString();
				else
					$this->Error = 'XMySqlDB::duplicateTable - destination table exists, failing';
				XLogError($this->Error);
				return false;
			}
		}
		else
		{
			if ($clobber)
				if (!$this->Execute('DROP TABLE IF EXISTS '.$dstTable))
				{
					$this->Error = 'XMySqlDB::duplicateTable - Execute (drop if exists) failed: '.$this->errorString();
					XLogError($this->Error);
					return false;
				}
		}
		if ($useTransaction && !$this->beginTransaction())
		{
			$this->Error = 'XMySqlDB::duplicateTable - beginTransaction failed: '.$this->errorString();
			XLogError($this->Error);
			return false;
		}
		if (!$this->Execute('CREATE TABLE '.$dstTable.' LIKE '.$srcTable))
		{
			$this->Error = 'XMySqlDB::duplicateTable - Execute (create) failed: '.$this->errorString();
			XLogError($this->Error);
			if ($useTransaction && !$this->rollBack())
				XLogError('XMySqlDB::duplicateTable - Execute (create) rollBack transaction also failed');
			return false;
		}
		if (!$this->Execute('INSERT INTO '.$dstTable.' SELECT * FROM '.$srcTable))
		{
			$this->Error = 'XMySqlDB::duplicateTable - Execute (insert) failed: '.$this->errorString();
			XLogError($this->Error);
			if ($useTransaction && !$this->rollBack())
				XLogError('XMySqlDB::duplicateTable - Execute (create) rollBack transaction also failed');
			return false;
		}
		if ($useTransaction && !$this->commit())
		{
			$this->Error = 'XMySqlDB::duplicateTable - commit transaction failed: '.$this->errorString();
			XLogError($this->Error);
			return false;
		}
		return true;
	}
	//---------------------------------------------------------
} // class TXMySQLDB
//------------------------------------------------------------------------------
?>
