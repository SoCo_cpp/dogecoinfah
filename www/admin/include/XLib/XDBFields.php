<?php
//-------------------------------------------------------------
/* 
 *	include/XDBFields.php
 * 
 * Global Option:
 * 
 * Used only for PDO prepared values:
 * define('XDBFIELDS_FORCE_BOOL_TO_INT', true); // default, sets bool values as PDO_INT type and converts value to 1 or 0
 * define('XDBFIELDS_FORCE_BOOL_TO_INT', false); // sets bool values as PDO:BOOL type, but converts value to 1 or 0
 * 
 * 
 * Examples:
 * 
 * Setup accompanying mysqli/PDO database object:
 * $xdb = new TXMySqlDB() or die ("Create XDB object failed");
 * if (!$xdb->Connect("127.0.0.1", "MyDBUserName", "MyDBPassword", "MyDatabase")) // Database parameter only needed for PDO, not legacy msqli
 * 		die("Connect to database failed.");
 * if (!$xdb->SelectDatabase("MyDatabase")) // only for legacy msqli, ignored when PDO detected
 * 		die("DB SelectDatabase failed"); * 
 * 
 * Setup table definition:
 * $dbF = new TXDBFields("myTableName", 1); // 2nd param, table version is stored, but versioning managed by user
 * $dbF->Add('id', 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
 * $dbF->Add('active', 'TINYINT(1)'); // treat as a Boolean
 * $dbF->Add('name', 'varchar(32)', 32); // 3rd param is max size/length when appropriate
 * 
 * Build Insert:
 * $dbF->ClearValues();
 * $dbF->SetValue('active', true);
 * $dbF->SetValue('name', 'MyName');
 * $sql = $dbF->scriptInsert(); // only sets SetValue's
 * // $sql: "INSERT INTO myTableName (active,name) VALUE (1,'MyName')" // bool normally converted to 1/0 int values, string auto ads quotes
 * if (!$xdb->Execute($sql))
 * 		die("DB Execute sql failed");
 * 
 * Build Select:
 * $dbF->ClearValues();
 * $dbF->SetValue('name'); // just set it as used, don't give it a value
 * $where = "active IN NOT NULL AND active != 0";
 * $orderBy = "name DESC";
 * $limitMax = 50;
 * $sql = $dbF->scriptSelect($where, $orderBy, $limitMax); // where/orderBy/limitMax are optional, by default only SetValue's included
 * // $sql: SELECT name FROM myTableName WHERE active IN NOT NULL AND active != 0 ORDER BY name DESC LIMIT 50
 * $result = $xdb->Query($sql); // returns TXMySqlDBResult object on success
 * if (!$result)
 *		die("DB query failed");
 * while ( ($row = $result->GetRowArray()) ) // TXMySqlDBResult object GetRowArray returns each row as array indexed by string field names
 * 		echo $row['name'];
 * 
 * Build Prepared (positional) Update:
 * $dbF->ClearValues();
 * $dbF->SetValuePrepared('active', true);
 * $dbF->SetValuePrepared('name', 'MyName');
 * $where = "id=1";
 * $sql = $dbF->scriptUpdate($where);
 * // $sql: "UPDATE myTableName SET active=?,name=? WHERE id=1"
 * $pdos = $xdb->Prepare($sql); // returns PDOStatement on success
 * if ($pdos === false)
 * 		die("DB Prepare PDO statement faield");
 * if (!$dbF->BindValues($pdos))
 * 		die("DB bind values to PDO statement failed");
 * if ($pdos->execute() === false) // execute the PDO statement, may return zero on no changes, so use type safe test for failure
 * 		die("PDO statement execute failed");
*/
//-------------------------------------------------------------
class TXDBField
{
	var $Index;
	var $Name;
	var $Types;
	var $MaxSize = 0;
	var $Value = NULL;
	var $ValueSet = false;
	var $Prepared = false;
	var $Tag = false;
	var $ValueTemp = false;
	function __construct($tIdx, $tName, $tTypes, $tMaxSize = 0)
	{
		$this->Index = $tIdx;
		$this->Name = $tName;
		$this->Types = $tTypes;
		$this->MaxSize = $tMaxSize;
	}
	function clear()
	{
		$this->Value = NULL;
		$this->ValueSet = false;
		$this->Prepared = false;
		$this->Tag = false;
		$this->ValueTemp = false;
	}
	function sqlValue()
	{
		if ($this->Prepared)
			return ($this->Tag === false || is_null($this->Tag) ? '?' : $this->Tag); // prepared placeholder or tag
		if ($this->ValueSet === false || is_null($this->Value))
			return 'NULL';
		if (is_bool($this->Value))
			return ($this->Value ? '1' : '0');
		if (is_numeric($this->Value))
			return ''.$this->Value;
		return '\''.$this->Value.'\'';
	}
	function bindValue($PDOStatement, $paramIndex = 0)
	{
		if ($PDOStatement === false || is_null($PDOStatement))
			return 'TXDBField::bindValue validate PDOStatement parameter failed';
		if (is_object($this->Value))
			return 'TXDBField::bindValue value is object type error';
		if ($this->Tag === false || is_null($this->Tag))
			$tag = $paramIndex;
		else
			$tag = $this->Tag;
		
		if (is_bool($this->Value))
		{
			$this->Value = ($this->Value === false ? 0 : 1);
			if (defined('XDBFIELDS_FORCE_BOOL_TO_INT') && XDBFIELDS_FORCE_BOOL_TO_INT === true)
				$type = PDO::PARAM_INT;
			else					
				$type = PDO::PARAM_BOOL;
		}
		else if (is_int($this->Value))
			$type = PDO::PARAM_INT;
		else if (is_null($this->Value))
			$type = PDO::PARAM_NULL;
		else
			$type = PDO::PARAM_STR;
		//echo "TXDBField::bindValue field '$this->Name', tag '$tag', type ".XVarDump($type).", value ".XVarDump($this->Value)."<br/>";
		$errorInfo = '';
		try
		{
			$ret = $PDOStatement->bindParam($tag, $this->Value, $type, $this->MaxSize);
		}
		catch (PDOException $e)
		{
			$ret = 'TXDBField::bindValue bindParam exception: '.XVarDump($e);
		}
		if ($ret !== true)
		{
			if ($type == PDO::PARAM_NULL)
				$typeName = 'Null';
			else if ($type == PDO::PARAM_INT)
				$typeName = 'Int';
			else if ($type == PDO::PARAM_BOOL)
				$typeName = 'Bool';
			else if ($type == PDO::PARAM_STR)
				$typeName = 'String';
			else
				$typeName = 'Unknown';
			$errorInfo = $PDOStatement->errorInfo();
			return "XDBFields::BindValues PDOStatement bindParam failed for field '$this->Name', tag '$tag', type ".XVarDump($type)."($typeName), value ".XVarDump($this->Value).", Error Info: ".XVarDump($errorInfo).", ret: ".XVarDump($ret);
		}
		return true;
	}
} // class TXDBField
//-------------------------------------------------------
class TXDBFields
{
	var $tableVersion = 0;
	var $tableName = '';
	var $lastError = '';
	var $Fields = array();
	function __construct($tTableName, $tTableVersion = 0)
	{
		$this->tableName = $tTableName;
		$this->tableVersion = $tTableVersion;
	}
	function Add($tName, $tTypes, $tMaxSize = 0)
	{
		$this->Fields[] = new TXDBField(sizeof($this->Fields), $tName, $tTypes, $tMaxSize) or die("Create object failed");
	}
	function ClearValues()
	{
		foreach ($this->Fields as $f)
			$f->clear();
	}
	function SetValues()
	{
		foreach ($this->Fields as $f)
		{
			$f->clear();
			$f->ValueSet = true;
			$f->Value = NULL;
		}
	}
	function SetValuesPrepared()
	{
		foreach ($this->Fields as $f)
		{
			$f->clear();
			$f->ValueSet = true;
			$f->Prepared = true;
		}
	}
	function ClearValue($tName)
	{
		$f = $this->GetByName($tName);
		if ($f === false)
			die("XDBFields.php - ClearValue GetByName, failed Name: $tName");
		$f->clear();
		return true;
	}
	function SetValue($tName, $tValue = NULL)
	{
		$f = $this->GetByName($tName);
		if ($f === false)
			die("XDBFields.php - SetValue GetByName, failed Name: $tName");
		$f->clear();
		$f->Value = $tValue;
		$f->ValueSet = true;
		return true;
	}
	function SetValuePrepared($tName, $tValue = NULL, $tPreparedTag = false)
	{
		$f = $this->GetByName($tName);
		if ($f === false)
			die("XDBFields.php - SetValuePrepared GetByName, failed Name: $tName");
		$f->clear();
		$f->ValueSet = true;
		$f->Prepared = true;
		$f->Value = $tValue;
		$f->Tag = $tPreparedTag;
		return true;
	}
	function GetByName($tName)
	{
		foreach ($this->Fields as $f)
			if ($tName == $f->Name)
				return $f;
		return false;
	}
	function GetValueCount($SkipNotSet = true)
	{
		$count = 0;
		foreach ($this->Fields as $f)
			if ($f->ValueSet || !$SkipNotSet)
				$count++;
		return $count;		
	}
	function GetMaxSize($tName)
	{
		foreach ($this->Fields as $f)
			if ($tName == $f->Name)
				return $f->MaxSize;
		return false;
	}
	function GetNameArray($SkipNotSet = false)
	{
		$a = array();
		if (!$SkipNotSet)
			foreach ($this->Fields as $f)
				$a[] = $f->Name;
		else
			foreach ($this->Fields as $f)
				if ($f->ValueSet) $a[] = $f->Name;
		return $a;
	}
	function GetNameListString($SkipNotSet = false) // for use with INSERT
	{
		return implode(',',$this->GetNameArray($SkipNotSet));
	}
	function GetValueArray($SkipNotSet = false)
	{
		$a = array();
		foreach ($this->Fields as $f)
		{
			if (is_object($f->Value))
			{
				die("XDBFields.php - GetValueArray value is object type error");
			}
			if ($f->ValueSet) $a[] = $f->sqlValue();
			elseif (!$SkipNotSet) $a[] = "''";
		}
		return $a;
	}
	function GetValueListString($SkipNotSet = false) // for use with UPDATE
	{
		return implode(',',$this->GetValueArray($SkipNotSet));
	}
	function GetValueAssignments($SkipNotSet = false)
	{
		$a = array();
		foreach ($this->Fields as $f)
			if ($SkipNotSet === false || $f->ValueSet)
				$a[] = $f->Name."=".$f->sqlValue();
		return implode(',', $a);
	}
	function GetFieldDeclarations()
	{
		$a = array();
		foreach ($this->Fields as $f)
			$a[] = $f->Name." ".$f->Types;
		return '('.implode(',', $a).')';
	}
	function BindValues($PDOStatement, $SkipNotSet = true)
	{
		if ($PDOStatement === false || is_null($PDOStatement))
		{
			$this->lastError = 'XDBFields.php BindValues validate PDO Statement parameter failed';
			return false;
		}
		$idx = 1;
		foreach ($this->Fields as $f)
			if ($f->ValueSet || !$SkipNotSet)
				if ( ($ret = $f->bindValue($PDOStatement, $idx++)) !== true)
				{
					$this->lastError = "XDBFields::BindValue field '$f->name' bindValue failed: ".XVarDump($ret);
					return false;
				}
		return true;
	}
	function scriptCreateTable($ifNotExists = true)
	{
		return "CREATE TABLE ".($ifNotExists === true ? "IF NOT EXISTS " : "").$this->tableName." ".$this->GetFieldDeclarations();
	}
	function scriptDropTable($ifNotExists = true)
	{
		return "DROP TABLE ".($ifNotExists == true ? "IF EXISTS " : "").$this->tableName;
	}
	function scriptInsert()
	{
		return "INSERT INTO ".$this->tableName." (".$this->GetNameListString(true/*SkipNotSet*/).") VALUES (".$this->GetValueListString(true/*SkipNotSet*/).")";
	}
	function scriptUpdate($where = false, $SkipNotSet = true)
	{
		return "UPDATE ".$this->tableName." SET ".$this->GetValueAssignments($SkipNotSet).($where !== false ? " WHERE ".$where : "");
	}
	function scriptSelect($where = false, $orderby = false, $limit = false, $SkipNotSet = true)
	{
		return "SELECT ".$this->GetNameListString(true/*SkipNotSet*/)." FROM ".$this->tableName.($where !== false ? " WHERE $where" : "").($orderby !== false ? " ORDER BY $orderby" : "").($limit !== false ? " LIMIT $limit" : "");
	}
	function scriptDelete($where = false)
	{
		return "DELETE FROM ".$this->tableName.($where !== false ? " WHERE $where" : "");
	}
} // class TXDBFields
//-------------------------------------------------------------
?>
