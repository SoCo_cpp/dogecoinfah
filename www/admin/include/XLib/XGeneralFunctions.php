<?php
//---------------------------------------------------------------------
define('XGeneralFunctionsPHP', true);
//---------------------------------------------------------------------
// htmlspecialchars();
// addslashes();
//---------------------------------------------------------------------
function Xnl2br($s)
{
	// cant seem to use str_replace to have array and replace with \n at same time
	//return str_replace(array("\r\n", "\n", "\r"), "<BR>\n", $s);
	return nl2br($s); // replaces with '<br />\n'
}
//---------------------------------------------------------------------
function XFalse2Null($value)
{
	return ($value === false ? null : $value);
}
//---------------------------------------------------------------------
function BoolYN($value)
{
	return ($value ? "Y" : "N");
}
//---------------------------------------------------------------------
function BoolTF($value)
{
	return ($value ? "T" : "F");
}
//---------------------------------------------------------------------
function FVal($value, $falseValue = false)
{
	return ($value !== false ? $value : $falseValue);
}
//---------------------------------------------------------------------
function NVal($value, $nullValue = false)
{
	return ($value !== null ? $value : $falseValue);
}
//---------------------------------------------------------------------
function FQVal($value, $falseValue = false)
{
	return ($value !== false ? "$value" : $falseValue);
}
//---------------------------------------------------------------------
function ZQVal($value, $zeroValue = 0)
{
	return ($value != 0 ? "$value" : $zeroValue);
}
//---------------------------------------------------------------------
function FZQVal($value, $falseValue = false, $zeroValue = 0)
{
	return ($value !== false ? ZQVal($value, $zeroValue) : $falseValue);
}
//---------------------------------------------------------------------
function XScanDirectory($baseDir, $includeDirs = true, $depthAllowed = 0, $seperator = '/')
{
	$reply = array();
	$baseDir = XEnsureEndSlash($baseDir, $seperator);
	if (!@file_exists($baseDir))
		return $reply;
	$files = @scandir($baseDir);
	if ($files === false)
		return $reply;
	foreach ($files as $fileName)
		if ($fileName != '.' && $fileName != '..')
		{
			$fileName = $baseDir.XEnsureNoEndSlash($fileName);
			if (!is_dir($fileName))
				$reply[] = $fileName;
			else
			{
				if ($includeDirs)
					$reply[] = $fileName;
				if ($depthAllowed > 0)
					foreach (XScanDirectory($fileName, $includeDirs, --$depthAllowed, $seperator) as $fileName)
						$reply[] = $fileName;
			}
		}
	return $reply;	
}
//---------------------------------------------------------------------
function XMaskContains($mask, $test)
{
	//-----------------------------------
	if (!is_numeric($mask) || !is_numeric($test))
		return false;
	//-----------------------------------
	return (($mask & $test) != 0 ? true : false);
}
//---------------------------------------------------------------------
function XArray($a, $i, $def = "")
{
	//-----------------------------------
	return (isset($a[$i]) ? $a[$i] : $def);
}
//---------------------------------------------------------------------
function XArrayConcat($srcA, $srcB) // doesn't modify sources
{
	//-----------------------------------
	$dest = array();
	//-----------------------------------
	foreach ($srcA as $v)
		$dest[] = $v;
	//-----------------------------------
	foreach ($srcB as $v)
		$dest[] = $v;
	//-----------------------------------
	return $dest;
}
//---------------------------------------------------------------------
function XArrayRemove($a, $pos, $count = 1) // doesn't modify source, doesn't preserve keys (not for AssArrays)
{
	$b = array();
	//-----------------------------------
	$posEnd = $pos + $count;
	$l = sizeof($a);
	for ($i = 0;$i < $l;$i++)
		if ($i < $pos || $i >= $posEnd)
			$b[] = $a[$i];
	//-----------------------------------
	return $b;
}
//---------------------------------------------------------------------
function XAssArrayRemove($a, $removeKey) // strict type compare, doesn't modify source, preserves keys
{
	$b = array();
	//-----------------------------------
	foreach ($a as $k => $v)
		if ($removeKey !== $k)
			$b[$k] = $v;
	//-----------------------------------
	return $b;
}
//---------------------------------------------------------------------
function XAssArrayRemoveMany($a, $arrayRemoveKeys) // strict type compare, doesn't modify source, preserves keys
{
	$b = array();
	//-----------------------------------
	foreach ($a as $k => $v)
		if (!in_array($k, $arrayRemoveKeys, true /*strict*/))
			$b[$k] = $v;
	//-----------------------------------
	return $b;
}
//---------------------------------------------------------------------
function XAssArrayMerge($dest, $src) // modifies only dest and returns it
{
	//-----------------------------------
	foreach ($src as $k => $v)
		$dest[$k] = $v;
	//-----------------------------------
	return $dest;
}
//---------------------------------------------------------------------
function XAssArrayConcat($srcA, $srcB) // doesn't modify sources
{
	//-----------------------------------
	$dest = array();
	//-----------------------------------
	foreach ($srcA as $k => $v)
		$dest[$k] = $v;
	//-----------------------------------
	foreach ($srcB as $k => $v)
		$dest[$k] = $v;
	//-----------------------------------
	return $dest;
}
//---------------------------------------------------------------------
function XAssArrayClone($src) // doesn't modify source, only reference values, not cloned
{
	//-----------------------------------
	$dest = array();
	//-----------------------------------
	foreach ($src as $k => $v)
		$dest[$k] = $v;
	//-----------------------------------
	return $dest;
}
//---------------------------------------------------------------------
function XVarDump(&$Var, $flatten = true)
{
	//-----------------------------------
	ob_start();
	var_dump($Var);
	$s = ob_get_contents();
	ob_end_clean();
	//-----------------------------------
	if ($flatten === true)
	{
		$lines = explode("\n", $s);
		$s = '';
		foreach ($lines as $l)
			$s .= trim($l)." ";
	}
	//-----------------------------------
	return $s;
}
//---------------------------------------------------------------------
function XStackTrace()
{
	//-----------------------------------
	$s = "";
	$t = debug_backtrace();
	//-----------------------------------
	foreach ($t as $c)
	{
		$l = XArray($c, 'line');
		$cls = XArray($c, 'class');
		$f = XArray($c, 'function');
		$s .= basename($c['file'])."\t\tLine: $l $cls::$f\n";
	}
	//-----------------------------------
	return $s;
}
//---------------------------------------------------------------------
function XPost($Name, $Def = "")
{
	//-----------------------------------
	return (isset($_POST[$Name]) ? $_POST[$Name] : $Def);
}
//---------------------------------------------------------------------
function XGet($Name, $Def = "")
{
	//-----------------------------------
	return (isset($_GET[$Name]) ? $_GET[$Name] : $Def);
}
//---------------------------------------------------------------------
function XGetPost($Name, $Def = "")
{
	//-----------------------------------
	$p = XPost($Name, false);
	if ($p !== false) 
		return $p;
	//-----------------------------------
	return XGet($Name, $Def);
}
//---------------------------------------------------------------------
function XServer($Name, $Def = "")
{
	//-----------------------------------
	return (isset($_SERVER[$Name]) ? $_SERVER[$Name] : $Def);
}
//---------------------------------------------------------------------
function XSession($Name, $Def = "")
{
	//-----------------------------------
	return (isset($_SESSION) && isset($_SESSION[$Name]) ? $_SESSION[$Name] : $Def);
}
//---------------------------------------------------------------------
function XFiles($filename, $part, $Def = "")
{
	//-----------------------------------
	return (isset($_FILES) && isset($_FILES[$filename]) && isset($_FILES[$filename][$part]) ? $_FILES[$filename][$part] : $Def);
}
//---------------------------------------------------------------------
function XRand($Min = false, $Max = false) // Min and Max are both inclusive. Default (0 - int max)
{
	//-----------------------------------
	if ($Max === false)
	{
		if ($Min !== false)
		{
			$Max = $Min;
			$Min =  0;
		}
		else
		{
			$Min =  0;
			$Max =  PHP_INT_MAX;
		}
	}
	else if ($Min >= $Max) return $Min;
	if (function_exists('random_int'))
		return random_int($Min, $Max);
	return mt_rand($Min, $Max);
}
//---------------------------------------------------------------------
function XRandData($lenght = 16)
{
    if (function_exists('random_bytes'))
        return random_bytes($lenght);
    if (function_exists("openssl_random_pseudo_bytes"))
        return openssl_random_pseudo_bytes($lenght);
    return false;
}	
//---------------------------------------------------------------------
function XRandHash($length = 16)
{
	$data = XRandData(ceil($length/2));
	if ($data === false)
		return false;
	return substr(bin2hex($data), 0, $length);
}
//---------------------------------------------------------------------
function XTestSecurityToken()
{
	//-----------------------------------
	if ( !isset($_SESSION['securityToken']) || !isset($_POST['securityToken']) )
		return false;
	//-----------------------------------
	if ( $_SESSION['securityToken'] !== $_POST['securityToken'] )
		return false;
	//-----------------------------------
	return true;
}
//-------------------------------------------------------
function XFileWritable($fileName) // will modify timestamp, will create the file then delete it, if it doesn't exist
{
	//-----------------------------------
	$writeable = true;
	$existed = file_exists($fileName);
	//-----------------------------------
	$fh = @fopen($fileName, 'a');
	if (!$fh)
		$writeable = false;
	else
		@fclose($fh);
	//-----------------------------------
	if (!$existed)
		@unlink($fileName);
	//-----------------------------------
	return ($writeable && (!$existed || is_writeable($fileName)));
}
//-------------------------------------------------------
function XQuickWriteFile($tFileName, $tText, $tAppend = false)
{
	//-----------------------------------
	if ($tAppend) $Attr = 'a';
	else          $Attr = 'w';
	//-----------------------------------
	$fh = fopen($tFileName, $Attr);
	if (!$fh) 
		return false;
	//-----------------------------------
	fwrite($fh, $tText);
	fclose($fh);
	//-----------------------------------
	return true;
}
//-------------------------------------------------------
function XListContains(&$List, &$Item)
{
	//-----------------------------------
	$cnt = sizeof($List);
	for ($i = 0;$i < $cnt;$i++)
		if ($Item == $List[$i])
			return true;
	//-----------------------------------
	return false;
}
//-------------------------------------------------------
function XEnsureBackslash($str)
{
	//-----------------------------------
	if (strrpos($str, "/") != strlen($str) - 1)
		$str .= "/";
	//-----------------------------------
	return $str;
}
//-------------------------------------------------------
function XEnsureEndSlash($str, $slash = '/')
{
	//-----------------------------------
	if (substr($str, -1, 1) != $slash)
		$str .= $slash;
	//-----------------------------------
	return $str;
}
//-------------------------------------------------------
function XEnsureNoEndSlash($str, $slash = '/')
{
	//-----------------------------------
	while (substr($str, -1, 1) == $slash)
		$str = substr($str, 0, strlen($str)-1);
	//-----------------------------------
	return $str;
}
//-------------------------------------------------------
function XEncodeHTML($txt, $doubleEncode = false)
{
	//-----------------------------------
	return htmlspecialchars($txt, ENT_QUOTES, 'UTF-8', $doubleEncode);
}
//------------------------
function XEncodeHTMLJS($txt, $doubleEncode = false)
{
	//-----------------------------------
	return htmlspecialchars(addcslashes($txt, "\"'"), ENT_QUOTES, 'UTF-8', $doubleEncode);
}
//------------------------
function XDefined($constName, $default = false)
{
	return (defined($constName) ? constant($constName) : $default);
}
//------------------------
function XMixedToDate($mixedDate, $timeZone = false, $noErrorLog = false)
{
	//-----------------------------------
	if ($mixedDate === false)
		$dt = XStringToDate('now', $timeZone, $noErrorLog);
	else if (is_object($mixedDate) && is_a($mixedDate, 'DateTime'))
		$dt = $mixedDate;
	else if (is_numeric($mixedDate) && is_int($mixedDate))
		$dt = XTimestampToDate($mixedDate, $timeZone, $noErrorLog);
	else if (is_string($mixedDate))
		$dt = XStringToDate($mixedDate, $timeZone, $noErrorLog);
	else
	{
		if (!$noErrorLog)
			XLogError("XMixedToDate mixedDate type not supported: ".XVarDump($mixedDate));
		return false;
	}
	//-----------------------------------
	if ($dt === false)
	{
		if (!$noErrorLog)
			XLogError("XMixedToDate mixedDate conversion failed: ".XVarDump($mixedDate));
		return false;
	}
	//-----------------------------------
	return $dt;
}
//------------------------
function XMixedToTimestamp($mixedDate, $timeZone = false, $noErrorLog = false)
{
	//-----------------------------------
	if ($mixedDate === false)
		$ts = time();
	if (is_object($mixedDate) && is_a($mixedDate, 'DateTime'))
		$ts = $mixedDate->getTimestamp();
	else if (is_numeric($mixedDate) && is_int($mixedDate))
		$ts = $mixedDate;
	else if (is_string($mixedDate))
		$ts = XStringToDate($mixedDate, $timeZone, $noErrorLog)->getTimestamp();
	else
	{
		if (!$noErrorLog)
			XLogError("XMixedToTimestamp mixedDate type not supported: ".XVarDump($mixedDate));
		return false;
	}
	//-----------------------------------
	if ($ts === false)
	{
		if (!$noErrorLog)
			XLogError("XMixedToTimestamp mixedDate conversion failed: ".XVarDump($mixedDate));
		return false;
	}
	//-----------------------------------
	return $ts;
}
//------------------------
function XTimestampToDate($timestamp, $timeZone = false, $noErrorLog = false)
{
	//-----------------------------------
	if (!is_int($timestamp) || $timestamp < 0)
	{
		if (!$noErrorLog )
			XLogError("XTimestampToDate validate timestamp parameter failed: ".XVarDump($timestamp));
		return false;
	}
	//-----------------------------------
	if ($timeZone === false)
		$timeZone = 'UTC';
	//-----------------------------------
	if (is_string($timeZone))
	{
		try{
			$timeZone = new DateTimeZone($timeZone);
		} catch (Exception $e) {
			if (!$noErrorLog )
				XLogError("XTimestampToDate create DateTimeZone from string timeZone: ".XVarDump($timeZone)." failed,  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	try{
		$dt = new DateTime("@$timestamp", $timeZone);
	} catch (Exception $e) {
		if (!$noErrorLog )
			XLogError("XTimestampToDate create DateTime failed timestamp : ".XVarDump($timestamp).",  exception: ".$e->getMessage());
		return false;
	}
	//-----------------------------------
	return $dt;
}
//------------------------
function XStringToDate($str, $timeZone = false, $noErrorLog = false)
{
	//-----------------------------------
	if (!is_string($str) || strlen(trim($str)) == 0)
	{
		if (!$noErrorLog )
			XLogError("XStringToDate validate str failed: ".XVarDump($str));
		return false;
	}
	//-----------------------------------
	if ($timeZone === false)
		$timeZone = 'UTC';
	//-----------------------------------
	if (is_string($timeZone))
	{
		try{
			$timeZone = new DateTimeZone($timeZone);
		} catch (Exception $e) {
			if (!$noErrorLog )
				XLogError("XStringToDate create DateTimeZone from string timeZone: ".XVarDump($timeZone)." failed,  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	try{
		$dt = new DateTime($str, $timeZone);
	} catch (Exception $e) {
		if (!$noErrorLog )
			XLogError("XStringToDate create DateTime failed str : ".XVarDump($str).",  exception: ".$e->getMessage());
		return false;
	}
	//-----------------------------------
	return $dt;
}
//------------------------
// returns difference in signed $units, with $overflow (def XDateTimeDiff_MAX 1 week max for seconds) on over flow
// before and later can be a DateTime object or a string
define('XDateTimeDiff_MAX', 604800); // 1 week worth of seconds
function XDateTimeDiff($before, $later = false/*now*/, $strTimeZone = false/*UTC*/, $units = false/*seconds (s,i/n,h,d,m,y)*/, $overflow = false/*XDateTimeDiff_MAX*/) 
{
	//-----------------------------------
	if ($units === false) $units = 's';
	if ($later === false) $later = 'now';
	if ($strTimeZone === false) $strTimeZone = 'UTC';
	if ($overflow === false) $overflow = XDateTimeDiff_MAX;
	if ($units === 'n') $units = 'i'; // alias i and n for minutes
	//-----------------------------------
	try{
		$timeZone = new DateTimeZone($strTimeZone);
	} catch (Exception $e) {
		XLogError("XDateTimeDiff create DateTimeZone strTimeZone: ".XVarDump($strTimeZone).",  exception: ".$e->getMessage());
		return false;
	}
	//-----------------------------------
	if ($timeZone === false){
		XLogError("XDateTimeDiff validate DateTimeZone failed, strTimeZone: ".XVarDump($strTimeZone));
		return false;
	}
	//-----------------------------------
	if (!is_string($before))
		$dtBefore = $before;
	else
	{
		try{
			$dtBefore = new DateTime($before, $timeZone);
		} catch (Exception $e) {
			XLogError("XDateTimeDiff create DateTime strBefore: ".XVarDump($before).",  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	if ($dtBefore === false){
		XLogError("XDateTimeDiff validate DateTime failed, strBefore: ".XVarDump($before));
		return false;
	}
	//-----------------------------------
	if (!is_string($later))
		$dtLater = $later;
	else
	{
		try{
			$dtLater = new DateTime($later, $timeZone);
		} catch (Exception $e) {
			XLogError("XDateTimeDiff create DateTime strLater: ".XVarDump($later).",  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	if ($dtLater === false){
		XLogError("XDateTimeDiff validate DateTime failed, strLater: ".XVarDump($later));
		return false;
	}
	//-----------------------------------
	$diff = $dtLater->diff($dtBefore);
	if ($diff === false){
		XLogError("XDateTimeDiff diff failed");
		return false;
	}
	//-----------------------------------
	$dy = ($diff->y !== false && $diff->y !== 0 ? $diff->y : 0);
	$dm = ($diff->m !== false && $diff->m !== 0 ? $diff->m : 0);
	$dd = ($diff->d !== false && $diff->d !== 0  && $diff->d !== -99999 /*PHP < 5.4.20/5.5.4*/ ? $diff->d : 0);
	$dh = ($diff->h !== false && $diff->h !== 0 ? $diff->h : 0);
	$di = ($diff->i !== false && $diff->i !== 0 ? $diff->i : 0);
	$ds = ($diff->s !== false && $diff->s !== 0 ? $diff->s : 0);
	//-----------------------------------
	if ($units === 's' && ($dy != 0 || $dm != 0 || $dd > 7))
		return $overflow;
	else if ($units === 'i' && ($dy > 1 || ($dy == 1 && $dm > 2) || $dm > 14 || $dd > 425))
		return $overflow;
	//-----------------------------------
	$val = 0.0;
	//-----------------------------------
	if ($units === 's') // seconds
	{
		$val += ($dd * 86400.0);
		$val += ($dh * 3600.0);
		$val += ($di * 60.0);
		$val += $ds;
	}
	else if ($units === 'i') // minutes
	{
		$val += ($dy * 525600.0);
		$val += ($dm * 43800.0);
		$val += ($dd * 1440.0);
		$val += ($dh * 60.0);
		$val += $di;
		if ($ds != 0) $val += ($ds / 60);
	}
	else if ($units === 'h') // hours
	{
		$val += ($dy * 8760.0);
		$val += ($dm * 730.001);
		$val += ($dd * 24.0);
		$val += $dh;
		if ($di != 0) $val += ($di / 60.0);
		if ($ds != 0) $val += ($ds / 3600.0); // (60 * 60)
	}
	else if ($units === 'd') // days
	{
		$val += ($dy * 365.25);
		$val += ($dm * 30.4167);
		$val += $dd;
		if ($dh != 0) $val += ($dh / 24.0);
		if ($di != 0) $val += ($di / 1440.0); // (60 * 24)
		if ($ds != 0) $val += ($ds / 86400.0); // (60 * 60 * 24)
	}
	else if ($units === 'm') // months
	{
		$val += ($dy * 12);
		$val += $dm;
		if ($dd != 0) $val += ($dd / 30.4167);
		if ($dh != 0) $val += ($dh / 730.0008); // (24 * 30.4167)
		if ($dh != 0) $val += ($di / 43800.048); // (60 * 24 * 30.4167)
		if ($dh != 0) $val += ($ds / 2628002.88); // (60 * 60 * 24 * 30.4167)
	}
	else if ($units === 'y') // years
	{
		$val += $dy;
		if ($dm != 0) $val += ($dm / 12.0);
		if ($dd != 0) $val += ($dd / 365.25);
		if ($dh != 0) $val += ($dh / 8766.0); // (24 * 365.25)
		if ($dh != 0) $val += ($di / 525960.0);  // (60 * 24 * 365.25)
	}
	//-----------------------------------
	//XLogDebug("XDateTimeDiff before: '$strBefore', later: '$strLater', tz: ' $strTimeZone', units: '$units', overflow: '$overflow', diff: ".XVarDump($diff));
	//-----------------------------------
	return $val;
}
//------------------------
function XDateDiffFuzzy($before, $later = false/*now*/, $strTimeZone = false/*UTC*/, $minUnits = 's', $minToOutOfMinUnits = 0, $belowMinToOutReply = "")
{
	//-----------------------------------
	if ($later === false) $later = 'now';
	if ($strTimeZone === false) $strTimeZone = 'UTC';
	//-----------------------------------
	if ($minUnits == 'n') $minUnits = 1;
	else if ($minUnits == 'h') $minUnits = 2;
	else if ($minUnits == 'd') $minUnits = 3;
	else if ($minUnits == 'm') $minUnits = 4;
	else if ($minUnits == 'y') $minUnits = 5;
	else $minUnits = 0; // default 's' seconds
	//-----------------------------------
	try{
		$timeZone = new DateTimeZone($strTimeZone);
	} catch (Exception $e) {
		XLogError("XDateDiffFuzzy create DateTimeZone strTimeZone: ".XVarDump($strTimeZone).",  exception: ".$e->getMessage());
		return false;
	}
	//-----------------------------------
	if ($timeZone === false){
		XLogError("XDateDiffFuzzy validate DateTimeZone failed, strTimeZone: ".XVarDump($strTimeZone));
		return false;
	}
	//-----------------------------------
	if (is_object($before)) // assume DateTime object
		$dtBefore = $before;
	else
	{
		try{
			if (is_numeric($before) && is_int($before)) // assume unix timestamp, ignore time zone
			{
				$dtBefore = new DateTime();
				$dtBefore->setTimestamp((int)$before);
			}
			else $dtBefore = new DateTime($before, $timeZone); // assume string
		} catch (Exception $e) {
			XLogError("XDateDiffFuzzy create DateTime strBefore: ".XVarDump($before).",  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	if ($dtBefore === false){
		XLogError("XDateDiffFuzzy validate DateTime failed, strBefore: ".XVarDump($before));
		return false;
	}
	//-----------------------------------
	if (is_object($later))
		$dtLater = $later;
	else
	{
		try{
			if (is_numeric($later) && is_int($later)) // assume unix timestamp, ignore time zone
			{
				$dtLater = new DateTime();
				$dtLater->setTimestamp((int)$later);
			}
			else $dtLater = new DateTime($later, $timeZone); // assume string
		} catch (Exception $e) {
			XLogError("XDateDiffFuzzy create DateTime strLater: ".XVarDump($later).",  exception: ".$e->getMessage());
			return false;
		}
	}
	//-----------------------------------
	if ($dtLater === false){
		XLogError("XDateDiffFuzzy validate DateTime failed, strLater: ".XVarDump($later));
		return false;
	}
	//-----------------------------------
	$diff = $dtLater->diff($dtBefore);
	if ($diff === false){
		XLogError("XDateDiffFuzzy diff failed");
		return false;
	}
	//-----------------------------------
	$units = array();
	$units[5] = 'year';
	$units[4] = 'month';
	$units[3] = 'day';
	$units[2] = 'hour';
	$units[1] = 'minute';
	$units[0] = 'second';
	//-----------------------------------
	$vals = array();
	$vals[5] = ($diff->y !== false && $diff->y !== 0 ? $diff->y : 0);
	$vals[4] = ($diff->m !== false && $diff->m !== 0 ? $diff->m : 0);
	$vals[3] = ($diff->d !== false && $diff->d !== 0  && $diff->d !== -99999 /*PHP < 5.4.20/5.5.4*/ ? $diff->d : 0);
	$vals[2] = ($diff->h !== false && $diff->h !== 0 ? $diff->h : 0);
	$vals[1] = ($diff->i !== false && $diff->i !== 0 ? $diff->i : 0);
	$vals[0] = ($diff->s !== false && $diff->s !== 0 ? $diff->s : 0);
	//-----------------------------------
	$idxUnit = 5;
	$rstr = "";
	while ($idxUnit >= $minUnits)
	{
		if ($minUnits == $idxUnit && $rstr == '' && $vals[$idxUnit] < $minToOutOfMinUnits)
			return $belowMinToOutReply; // value below min, done
		if ($vals[$idxUnit] != 0)
			$rstr .= ($rstr != '' ? ' ' : '').$vals[$idxUnit].' '.$units[$idxUnit].($vals[$idxUnit] > 1 ? 's' : '');
		$idxUnit--;
	}
	//-----------------------------------
	return $rstr;
}
//------------------------
// requires BC Math library
class XTimer
{
	var $supported = false;
	var $startMs = false;
	function __construct()
	{
		if (!function_exists("bcmul") || !function_exists("bcadd") || !function_exists("bcsub"))
			$this->supported = false;
		else
			$this->supported = true;
		$this->start();
	}
	function start()
	{
		$this->startMs = $this->currentMs();
	}
	function currentMs()
	{
		if (!$this->supported)
			return false;
		$parts = explode(" ", microtime());
		return bcadd( ($parts[0]*1000), bcmul($parts[1], 1000));
	}
	function elapsedMs($appendUnits = false)
	{
		if ($this->startMs === false)
			$elapsedMs = "0";
		else
			$elapsedMs = bcsub($this->currentMs(), $this->startMs);
		if ($appendUnits)
			$elapsedMs .= " ms";
		return $elapsedMs;
	}
	function restartMs($appendUnits = false)
	{
		$curMs = $this->currentMs();
		if ($this->startMs === false || $curMs === false)
			$elapsedMs = "0";
		else
			$elapsedMs = bcsub($curMs, $this->startMs);
		$this->startMs = $curMs;
		if ($appendUnits)
			$elapsedMs .= " ms";
		return $elapsedMs;
	}
}
//------------------------
function XExecute($cmd, $procMode = true)
{
	$descriptorspec = array(
		   0 => array("pipe", "w"), // stdin (needed for rVal)
		   1 => array("pipe", "w"), // stdout
		   2 => array("pipe", "w")  // stderr
		   );
	$p = @proc_open($cmd, $descriptorspec, $pipes);
	if (!is_resource($p))
	{
		XLogError("XExecute proc_open failed: ".XVarDump($p).", cmd: $cmd");
		return array(false, "", "", "proc_open failed");
	}
	$rData = array();
	$rData[1] = @stream_get_contents($pipes[1]);
	$rData[2] = @stream_get_contents($pipes[2]);
	@fclose($pipes[0]);
	@fclose($pipes[1]);
	@fclose($pipes[2]);
	$rVal = @proc_close($p);
	if ($rVal === false)
		return array(false, "", "", "proc_close failed");
	$rData[0] = $rVal;
	return $rData;
}
//------------------------
function XExecSimple($cmd, &$output = null /* should be array if used*/)
{
	$code = 0;
	$rVal = exec($cmd, $output, $code); 
	if ($rVal === false)
		return false;
	return $code;
}
//------------------------
?>
