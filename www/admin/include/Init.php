<?php
//-------------------------------------------------------------
/*
 *	www/admin/include/Init.php
 * 
 *  See www/admin/Settings.php for configurable settings.
 * 
*/
//-------------------------------------------------------------
function DFah_ErrorLogger($src, $e)
{
	//-------------
	$eTypeNames = array(	'e' => 'Exception',
							0 => '[error type not set]',
							E_ERROR => 'E_ERROR',
							E_WARNING => 'E_WARNING',
							E_PARSE => 'E_PARSE',
							E_NOTICE => 'E_NOTICE',
							E_CORE_ERROR => 'E_CORE_ERROR',
							E_CORE_WARNING => 'E_CORE_WARNING',
							E_COMPILE_ERROR => 'E_COMPILE_ERROR',
							E_COMPILE_WARNING => 'E_COMPILE_WARNING',
							E_USER_ERROR => 'E_USER_ERROR',
							E_USER_WARNING => 'E_USER_WARNING',
							E_USER_NOTICE => 'E_USER_NOTICE',
							E_STRICT => 'E_STRICT',
							E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
							E_DEPRECATED => 'E_DEPRECATED',
							E_USER_DEPRECATED => 'E_USER_DEPRECATED' );
	//-------------
	$eType = (isset($e['type']) ? $e['type'] : 0);
	$strType = (isset($eTypeNames[$eType]) ? $eTypeNames[$eType] : '[Unknown '.$eType.']');
	$strMessage = (isset($e['message']) ? $e['message'] : '[error message not set]');
	//-------------
	$eFile = (isset($e['file']) ? $e['file'] : '[error file not set]');
	$eLine = (isset($e['line']) ? $e['line'] : '[error line not set]');
	//-------------
	$posParams = strpos($eLine, '?');
	if ($posParams !== false)
		$eLine = substr($eLine, 0, $posParams);
	//-------------
	$strFileFull = $eFile.':'.$eLine;
	$strFile = basename(dirname($eFile, 3)).'/'.basename(dirname($eFile, 2)).'/'.basename(dirname($eFile, 1)).'/'.basename($eFile).':'.$eLine;
	//-------------
	try
	{
		$dtNow = new DateTime('now', new DateTimeZone('UTC'));
		$strDate = $dtNow->format('Y-m-d H:i:s T');
	}
	catch (Exception $e)
	{
		$strDate = '[failed to get date]';
	}
	//-------------
	$pos = strpos($strMessage, "\n");
	if ($pos !== false)
		$strMessage = substr($strMessage, 0, $pos);
	$strMessage = str_replace($strFileFull, '', $strMessage);
	//-------------
	$strOutputMessage = "$strDate [$src] $strType: $strMessage ($strFile)\n";
	//------------- Minimal error log meant for Monitor to read
	$outputFileBase = dirname(__FILE__, 2).'/log/error';
	if (false === @file_put_contents($outputFileBase.'_monitor.log', $strOutputMessage, FILE_APPEND))
	{
		$outputFileBase = './error';
		@file_put_contents($outputFileBase.'_monitor.log', $strOutputMessage, FILE_APPEND);
	}
	//------------- Full dump error log, test for size limit and archive if needed
	$outputFileFull = $outputFileBase.'.log';
	if (@file_exists($outputFileFull) === true)
	{
		$bytes = @filesize($outputFileFull);
		if ($bytes !== false && $bytes > 2500000 /*~2.5 MB*/)
		{
			$num = 1;
			while (@file_exists($outputFileFull.'.'.$num) === true)
				$num++;
			@rename($outputFileFull, $outputFileFull.'.'.$num);
		}
	}
	//------------- Collect full dump of last error and append it to the error.log
	ob_start();
	var_dump($e);
	$strDump = ob_get_contents();
	ob_end_clean();
	//-------------
	@file_put_contents($outputFileFull, $strDump."\n", FILE_APPEND);
	//-------------
}
//-------------------------------------------------------------
function DFah_Shutdown_Handler()
{
	//-------------
	$e = error_get_last();
	//-------------
	if (is_null($e) || !isset($e['type']))
		return; // no errors
	//-------------
	$errNo = $e['type'];
	//-------------
	if ($errNo == 0 /*prior to PHP 8*/ || (error_reporting() & $errNo) == 0) // test for @ error/warning suppression to ignore
		return;
	//-------------
	DFah_ErrorLogger('shutdown_handler', $e);
	//-------------
}
//-------------------------------------------------------------
function DFah_Error_Handler($errNo, $errStr, $errFile, $errLine, $errContext = 'Depreciated as of PHP 7.2.0')
{
	//-------------
	if ($errNo == 0 /*prior to PHP 8*/ || (error_reporting() & $errNo) == 0) // test for @ error/warning suppression to ignore
		return;
	//-------------
	DFah_ErrorLogger('error_handler', array('type' => $errNo, 'message' => $errStr, 'file' => $errFile, 'line' => $errLine));
	die();
	//-------------
}
//-------------------------------------------------------------
function DFah_Exception_Handler($e)
{
	//-------------
	$strFile = $e->getFile();
	$posParams = strpos($strFile, '?');
	if ($posParams !== false)
		$strFile = substr($strFile, 0, $posParams);
	$strMessage = '('.$e->getCode().') '.$e->getMessage()."\n".$e->getTraceAsString();
	DFah_ErrorLogger('exception_handler', array('type' => 'e', 'message' => $strMessage, 'file' => $strFile, 'line' => $e->getLine()));
	throw new ErrorException($strMessage, $e->getCode(), 0, $strFile, $e->getLine());
	//-------------
}
//-------------------------------------------------------------
register_shutdown_function('DFah_Shutdown_Handler');
set_error_handler('DFah_Error_Handler');
set_exception_handler('DFah_Exception_Handler');
//-------------------------------------------------------------
require('./include/MarketConfig.php');
require('./Settings.php');
//---------------------------
//define('XDB_USER_PREFIX', BRAND_DB_PREFIX.'_xlogin_'); // this auto sets the the table prefix XLogin.cpp
define('XINCLUDE_DIR', './include/XLib/'); // this sub-project's code comes bundled at this location
//---------------------------
if (!defined('XDATABASE_HOST') || !defined('XDATABASE_NAME') || !defined('XDATABASE_USER') || !defined('XDATABASE_PASS'))
{
	echo "<div style='color : red;'>Database credentials not specified in www/admin/Settings.php. XDATABASE_HOST, XDATABASE_NAME, XDATABASE_USER, XDATABASE_PASS are required.</div>\n";
	exit;
}
//------------------------
if (!defined('WALLET_RPC_HOST') || !defined('WALLET_RPC_PORT') || !defined('WALLET_RPC_USER') || !defined('WALLET_RPC_PASS'))
{
	echo "<div style='color : red;'>Coin wallet RPC credentials not specified in www/admin/Settings.php. WALLET_RPC_HOST, WALLET_RPC_PORT, WALLET_RPC_USER, WALLET_RPC_USER are required.</div>\n";
	exit;
}
//------------------------
if (!defined('BRAND_FOLDING_TEAM_ID') || !defined('BRAND_ADDRESS_LINK') || !defined('BRAND_NAME') || !defined('BRAND_UNIT') || !defined('BRAND_COPYRIGHT') || !defined('BRAND_TX_COMMENT') || !defined('BRAND_DB_PREFIX'))
{
	echo "<div style='color : red;'>Coin branding not specified in www/admin/Settings.php. BRAND_FOLDING_TEAM_ID, BRAND_ADDRESS_LINK, BRAND_NAME, BRAND_UNIT, BRAND_COPYRIGHT, BRAND_TX_COMMENT, BRAND_DB_PREFIX are required.</div>\n";
	exit;
}
//------------------------
if (defined('IS_INSTALL') && IS_INSTALL === true)
{
	$logPathInfo = pathinfo(XLOG_FILENAME);
	if (!is_writable($logPathInfo['dirname'])) 
	{
		echo "<div style='color : red;'>Logging directory is not writable. Please ensure write privileges to the directory containing ".$logPathInfo['dirname'].".</div>\n";
		exit;
	}
}
//------------------------ Guess limit constants if older version of PHP doesn't define them
if (!defined('PHP_INT_MAX'))
	define('PHP_INT_MAX', 2147483647);
//------------------------
if (!defined('PHP_INT_MIN'))
	define('PHP_INT_MIN', -2147483647);
//------------------------
require('./include/XLib/XInit.php');
//------------------------
require('./include/Version.php');
require('./include/DatabaseDefines.php');
require('./include/Config.php');
require('./include/Template.php');
require('./include/Workers.php');
require('./include/HTTPHandler.php');
require('./include/ConnectionSSH.php');
require('./include/SodiumMessenger.php');
require('./include/SodiumTunneler.php');
require('./include/Wallet.php');
require('./include/Rounds.php');
require('./include/Payouts.php');
require('./include/Stats.php');
require('./include/Automation.php');
require('./include/Privileges.php');
require('./include/Contributions.php');
require('./include/Ads.php');
require('./include/Display.php');
require('./include/FahClient.php');
require('./include/FahAppClient.php');
require('./include/NewRoundData.php');
require('./include/Team.php');
require('./include/Blogs.php');
require('./include/Monitor.php');
require('./include/PublicData.php');
require('./include/MarketClient.php');
require('./include/MarketHistory.php');
//------------------------
if (ini_get('allow_url_fopen') != '1')
{
	ini_set('allow_url_fopen', 1);
	if (ini_get('allow_url_fopen') != '1')
		XLogError("Init.php PHP ini option 'allow_url_fopen' could not be enabled. RPC wallet connectivity will not be permitted.");
}
//------------------------
if (version_compare(phpversion(), '7.1', '>=') && ini_get('serialize_precision') != '-1')
{
    ini_set( 'serialize_precision', -1);
	if (ini_get('serialize_precision') != '-1')
		XLogError("Init.php PHP ini option 'serialize_precision' could not be set to the special float value of -1. Transactions may fail and live data may have floating precision flaws.");
}
//------------------------
$Config = new Config() or die("Init.php Create Config object failed");
//------------------------
$lateErrorReporting = $Config->GetSetDefault('debug_late_error_reporting', '0');
if ( !($lateErrorReporting === '' || $lateErrorReporting === '0' || $lateErrorReporting === 'false') )
	error_reporting(E_ALL);
//------------------------
?>
