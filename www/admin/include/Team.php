<?php
/*
 *	www/adming/include/Team.php
 * 
 * 
* 
*/
//---------------
define('CFG_TEAM_NAME', 'team_name');
//---------------
class Team
{
	//------------------
	function Install()
	{
		global $db, $dbTeamFields;
		//------------------------------------
		$sql = $dbTeamFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Team::Install db Execute create team table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	} // Install
	//------------------
	function Uninstall()
	{
		global $db, $dbTeamFields;
		//------------------------------------
		$sql = $dbTeamFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Team::Uninstall db Execute drop team table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	} // Uninstall
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbTeamFields;
		//------------------------------------
		$newFields = false;
		$fieldsArray = false;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // 1->2 Updated credit/points fields to BIGINT, just do a copy
			case 2: // 2->3 renamed rank field that became a mysql keyword
				//---------------
				// fallthrough has fixup for ver 2->3
			case 3: // 3->4 changed date to string type
				//---------------
				if ($newFields === false && $fieldsArray === false)
				{
					//---------------
					$dbTeamFields->SetValues();
					$dbTeamFields->ClearValue(DB_TEAM_DATE);
					//---------------
					$newFields = $dbTeamFields->GetNameListString(true /*skipNotSet*/);
					$fieldsArray = $dbTeamFields->GetNameArray(true /*skipNotSet*/);
					//---------------
				}
				//---------------
				if ($oldTableVer < 3)
					for ($i=0;$i < sizeof($fieldsArray);$i++)
						if ($fieldsArray[$i] == DB_TEAM_RANK)
							$fieldsArray[$i] = "`rank`";
				//---------------
				$oldFields = implode(",", $fieldsArray);
				//---------------
				$sql = "INSERT INTO $dbTeamFields->tableName ($newFields,".DB_TEAM_DATE.") SELECT $oldFields, DATE_FORMAT(".DB_TEAM_DATE.",'".MYSQL_STR_TO_DATE_FORMAT."') AS ".DB_TEAM_DATE." FROM $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Team::Import db Execute table import failed (convert ver < 3) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbTeamFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbTeamFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Team::Import db Execute table team import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Team::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function AddFromOverview($overview)
	{
		global $db, $dbTeamFields;
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$dbTeamFields->ClearValues();
		$dbTeamFields->SetValue(DB_TEAM_DATE,   		$nowUtc->format(MYSQL_DATETIME_FORMAT));
		$dbTeamFields->SetValue(DB_TEAM_RANK,   		$overview["rank"]);
		$dbTeamFields->SetValue(DB_TEAM_TEAMS,   		$overview["total_teams"]);
		$dbTeamFields->SetValue(DB_TEAM_CREDIT,   		$overview["score"]);
		$dbTeamFields->SetValue(DB_TEAM_WUS,   			$overview["wus"]);
		//------------------
		$sql = $dbTeamFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Team::AddFromOverview - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	} // AddFromOverview
	//------------------
	function addUserDonor($userId, $points, $wus, $strDate = false)
	{
		global $db, $dbTeamUsersFields;
		//------------------
		if ($strDate === false)
		{
			$strDate = new DateTime('now',  new DateTimeZone('UTC'));
			$strDate = $strDate->format(MYSQL_DATETIME_FORMAT);
		}
		//------------------
		$dbTeamUsersFields->ClearValues();
		$dbTeamUsersFields->SetValuePrepared(DB_TEAM_USERS_DATE,   		$strDate);
		$dbTeamUsersFields->SetValuePrepared(DB_TEAM_USERS_USER_ID,   	$userId);
		$dbTeamUsersFields->SetValuePrepared(DB_TEAM_USERS_POINTS,   	$points);
		$dbTeamUsersFields->SetValuePrepared(DB_TEAM_USERS_WUS,   		$wus);
		//------------------
		$sql = $dbTeamUsersFields->scriptInsert();
		//------------------
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Team::addUserDonor - db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbTeamUsersFields->BindValues($dbs))
		{
			XLogError("Team::addUserDonor - BindValues failed");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Team::addUserDonor - dbs Execute scriptInsert (prepared) failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function calcTeamStats()
	{
		global $db, $dbTeamFields, $dbTeamUsersFields;
		//------------------
		$Config = new Config() or die("Create object failed");
		//------------------
		$teamId = $Config ->Get(CFG_ROUND_TEAM_ID);
		if ($teamId === false || !is_numeric($teamId))
		{
			XLogError("Team::calcTeamStats get team ID config failed");
			return false;
		}
		//------------------
		$utcTimeZone = new DateTimeZone('UTC');
		//------------------
		$nowDate = new DateTime('now', $utcTimeZone);
		$buffDate = clone $nowDate;
		$buffDate->modify("-1 day");
		//------------------
		$strLast = $this->getTeamMaxDate($buffDate, $nowDate, false /*fail if missing*/);
		if ($strLast === false)
		{
			XLogError("Team::calcTeamStats last getTeamMaxDate failed");
			return false;
		}
		//------------------
		$dtLast = XStringToDate($strLast, $utcTimeZone);
		if ($dtLast === false)
		{
			XLogError("Team::calcTeamStats last XStringToDate failed: ".XVarDump($strLast));
			return false;
		}
		//------------------
		$weekDate = clone $dtLast; // deep copy dtLast
		$weekDate->modify("-7 day");
		//------------------
		XLogDebug("Team::calcTeamStats - team data (last/week) ".$dtLast->format(MYSQL_DATETIME_FORMAT)." / ".$weekDate->format(MYSQL_DATETIME_FORMAT));
		//------------------
		// Query Last Week of Team Data
		$values = $this->getTeamValuesByMaxDate($weekDate->format(MYSQL_DATETIME_FORMAT));
		if ($values === false)
		{
			XLogError("Team::calcTeamStats - getTeamValuesByMaxDate failed.");
			return false;
		}
		//------------------
		$TotalWorkerCount = $this->getTeamUserCount();
		if ($TotalWorkerCount === false)
		{
			XLogDebug("Team::calcTeamStats team worker count failed");
			return false;
		}
		//------------------
		$dayCounter = clone $dtLast; // deep copy dttLast
		$dayCounter->modify("-1 day");
		$avgDay = array();
		$avgLast = false;
		$avgCount = 0;
		$weekValues = false;
		$nowValues = false;
		$dayValues = false;
		$oldestDate = false;
		//------------------
		foreach ($values as $row)
		{
			$date = XStringToDate($row[DB_TEAM_DATE], $utcTimeZone);
			if ($date === false)
			{
				XLogDebug("Team::calcTeamStats bad date detected in row: ".XVarDump($row));
				return false;
			}
			if ($nowValues === false)
				$nowValues = $row; // first record is now
			$weekValues = $row;
			$oldestDate = $date;
			if ($date < $dayCounter)
			{
				if ($dayValues === false)
					$dayValues = $row;
				if ($avgLast === false)
				{
					$avgLast = array();
					foreach ($row as $field => $value)
						$avgLast[$field] = $value;
				}
				else
				{
					foreach ($row as $field => $value)
						if (isset($avgLast[$field]) && is_numeric($avgLast[$field]) && is_numeric($value))
							$avgDay[$field] = $avgLast[$field] - $value; // we are iterating from the oldest to newest
					$avgCount++;
				}
				$avgLast = $row;
				//XLogDebug("Team:calcTeamStats date ".$date->format(MYSQL_DATETIME_FORMAT).", dayCounter ".$dayCounter->format(MYSQL_DATETIME_FORMAT)." avgCount: $avgCount");
				$dayCounter->modify("-1 day");
			}
			//else XLogDebug("Team::calcTeamStats date ".$date->format(MYSQL_DATETIME_FORMAT)." (".$row[DB_TEAM_DATE].") !< ".$dayCounter->format(MYSQL_DATETIME_FORMAT));
			$weekValues = $row; // very last record will be about one week ago
		} // foreach ($values as $row)
		//------------------
		if ($avgCount >= 2)
		{
			foreach ($avgDay as $field=>$value)
				$avgDay[$field] /= $avgCount;
		}
		else
		{
			XLogDebug("Team::calcTeamStats avgCount not enough data");
			$weekValues = false;
		}
		//------------------
		if ($nowValues === false)
		{
			XLogError("Team::calcTeamStats - no period values were found (now)");
			return false;
		}
		//------------------
		$userStats = $this->getTeamUserStats();
		XLogDebug("Team::calcTeamStats getTeamUserStats returned: ".XVarDump($userStats));
		if ($userStats === false)
		{
			XLogError("Team::calcTeamStats - getTeamUserStats failed");
			return false;
		}
		if ($userStats === 0)
		{
			XLogWarn("Team::calcTeamStats - getTeamUserStats failed to find needed data. Returning no data found.");
			return 0;
		}
		//------------------
		$actAll = $userStats[0];
		$actDay = ($userStats[1] !== false ? $userStats[1] : -9999);
		$actWeek = ($userStats[2] !== false ? $userStats[2] : -9999);
		//------------------
		if ($actDay === -9999)
			XLogWarn("Team::calcTeamStats - getTeamUserStats didn't find  enough data for day values");
		if ($actWeek === -9999)
			XLogWarn("Team::calcTeamStats - getTeamUserStats didn't find enough data for week values.");
		//------------------
		// members active/ x-inactive
		// rank / total teams (day / week change)
		// wus total
		// points / day (val/7)
		$teamCount = $nowValues[DB_TEAM_TEAMS];
		if ($dayValues !== false)
		{
			$rankDay = $dayValues[DB_TEAM_RANK] - $nowValues[DB_TEAM_RANK]; // lower rank is a positive increase
			$pointsDay = $nowValues[DB_TEAM_CREDIT] - $dayValues[DB_TEAM_CREDIT];
		}
		else
		{
			XLogWarn("Team::calcTeamStats - not all required period values were found (day)");
			$rankDay = -9999;
			$pointsDay = -9999;
		}
		//------------------
		if ($weekValues !== false)
		{
			$rankWeek = $weekValues[DB_TEAM_RANK] - $nowValues[DB_TEAM_RANK]; // lower rank is a positive increase
			$pointsWeek = $nowValues[DB_TEAM_CREDIT] - $weekValues[DB_TEAM_CREDIT];
		}
		else
		{
			XLogWarn("Team::calcTeamStats - not all required period values were found (week)");
			$rankWeek = -9999;
			$pointsWeek = -9999;
		}
		//------------------
		XLogDebug("Team::calcTeamStats rankDay $rankDay, rankWeek $rankWeek, pointsDay $pointsDay, pointsWeek $pointsWeek, avgDay team credit ".(!isset($avgDay[DB_TEAM_CREDIT]) ? "(not set, avgCount only $avgCount)" : $avgDay[DB_TEAM_CREDIT]));
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		//------------------
		$teamName = $Config->Get(CFG_TEAM_NAME);
		if ($teamName === false)
			$teamName = "[team name]";
		//------------------
		$json = array( 'UTC' => $nowUtc->format(DATE_ISO8601), 'name' => (string)$teamName, 'id' => (int)$teamId, 
					   'actusers' => (int)$actWeek, 'users' => (int)$TotalWorkerCount, 
					   'rank' => (int)$nowValues[DB_TEAM_RANK], 'rank24' => (int)$rankDay, 'rank7' => (int)$rankWeek, 
					   'points' => (int)$nowValues[DB_TEAM_CREDIT], 'points24' => $pointsDay, 'wu' => (int)$nowValues[DB_TEAM_WUS], 'teams' => (int)$teamCount);
		//------------------
		return $json;
	}
	//------------------
	function getTeamUserStats()
	{
		//------------------
		$utcTimeZone = new DateTimeZone('UTC');
		//------------------
		$dtNow = new DateTime('now',  $utcTimeZone);
		$dtBuff = clone $dtNow;
		$dtBuff->modify("-1 day");
		//------------------
		$strLast = $this->getTeamUserMaxDate($dtBuff, $dtNow, 0 /*missing*/);
		if ($strLast === false)
		{
			XLogError("Team::getTeamUserStats last getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		if ($strLast === 0 /*missing*/) // can't continue without at lest this one
		{
			XLogWarn("Team::getTeamUserStats last getTeamUserMaxDate failed. Returning no data found.");
			return 0;
		}			
		//------------------
		$dtLast = XStringToDate($strLast, $utcTimeZone);
		if ($dtLast === false)
		{
			XLogError("Team::getTeamUserStats XStringToDate dtLast from strLast failed : ".XVarDump($strLast));
			return false;
		}
		//------------------
		$dtDay = clone $dtLast; // needs to be deep copied
		$dtDay->modify("-1 day");
		//------------------
		$dtWeek = clone $dtLast; // needs to be deep copied
		$dtWeek->modify("-7 day");
		//------------------
		$dtBuff = clone $dtDay;
		$dtBuff->modify("-1 day");
		//------------------
		$strDay = $this->getTeamUserMaxDate($dtBuff, $dtDay, 0 /*missing*/);
		if ($strDay === false)
		{
			XLogError("Team::getTeamUserStats - day getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		$dtBuff = clone $dtWeek;
		$dtBuff->modify("-1 day");
		//------------------
		$strWeek = $this->getTeamUserMaxDate($dtBuff, $dtWeek, 0 /*missing*/);
		if ($strWeek === false)
		{
			XLogError("Team::getTeamUserStats - week getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		// always want now or fail
		$valuesNow = $this->getTeamUserValuesByDate($strLast);
		if ($valuesNow === false)
		{
			XLogError("Team::getTeamUserStats - now getTeamUserValuesByDate failed.");
			return false;
		}
		//------------------
		$valuesDay = false;
		if ($strDay !== 0 /*missing*/)
		{
			$valuesDay = $this->getTeamUserValuesByDate($strDay);
			if ($valuesDay === false)
			{
				XLogError("Team::getTeamUserStats - day getTeamUserValuesByDate failed.");
				return false;
			}
		}
		//------------------
		$valuesWeek = false;
		if ($strWeek !== 0 /*missing*/)
		{
			$valuesWeek = $this->getTeamUserValuesByDate($strWeek);
			if ($valuesWeek === false)
			{
				XLogError("Team::getTeamUserStats - week getTeamUserValuesByDate failed.");
				return false;
			}
		}
		//------------------
		XLogDebug("Team::getTeamUserStats strLast ".XVarDump($strLast)." valuesNow ".sizeof($valuesNow).", strDay ".XVarDump($strDay)." valuesDay ".($valuesDay === false ? 'false' : sizeof($valuesDay)).", strWeek ".XVarDump($strWeek)." valuesWeek ".($valuesWeek === false ? 'false' : sizeof($valuesWeek)));
		//XLogDebug("Team::getTeamUserStats valuesDay ".XVarDump($valuesDay).", strWeek ".XVarDump($strWeek).", valuesWeek ".XVarDump($valuesWeek));
		//------------------
		$actDay = 0;
		$actWeek = 0;
		foreach ($valuesNow as $id => $vNow)
		{
			//------------------
			if ($valuesDay !== false && isset($valuesDay[$id]))
			{
				if (($vNow - $valuesDay[$id]) > 0)
					$actDay++;
				else if ($vNow !== $valuesDay[$id])
					XLogDebug("Team::getTeamUserStats id $id weird no match (now/day) ".XVarDump($vNow)."/".XVarDump($valuesDay[$id]));

				if ($valuesWeek !== false && isset($valuesWeek[$id]))
				{
					if (($vNow - $valuesWeek[$id]) > 0)
						$actWeek++;
					else if ($vNow !== $valuesWeek[$id])
						XLogDebug("Team::getTeamUserStats id $id weird no match (now/week) ".XVarDump($vNow)."/".XVarDump($valuesWeek[$id]));
				}
			}
			//------------------
		} // foreach valuesNow
		//------------------
		return array(sizeof($valuesNow), ($valuesDay !== false ? $actDay : false), ($valuesWeek !== false ? $actWeek : false));
	}
	//------------------
	function getActiveUserCount($dtSince, $validOnly = false, $noDataValue = false)
	{
		$Workers = new Workers() or die("Create object failed");
		//------------------
		if ($validOnly !== false)
		{
			$validIDs = $Workers->getWorkerIDs(true /*onlyValid*/);
			if ($validIDs === false)
			{
				XLogError("Team::getActiveUserCount Workers getWorkerIDs failed.");
				return false;
			}
		}
		//------------------
		$utcTimeZone = new DateTimeZone('UTC');
		//------------------
		$dtNow = new DateTime('now',  $utcTimeZone);
		$dtBuff = clone $dtNow;
		$dtBuff->modify("-1 day");
		//------------------
		$strLast = $this->getTeamUserMaxDate($dtBuff, $dtNow, 0 /*missing*/);
		if ($strLast === false)
		{
			XLogError("Team::getActiveUserCount last getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		if ($strLast === 0) // can't continue with out last
			return $noDataValue;
		//------------------
		$dtLast = XStringToDate($strLast, $utcTimeZone);
		if ($dtLast === false)
		{
			XLogError("Team::getActiveUserCount XStringToDate dtLast from strLast failed : ".XVarDump($strLast));
			return false;
		}
		//------------------
		$dtBuff = clone $dtSince;
		$dtBuff->modify("-1 day");
		//------------------
		$strSince = $this->getTeamUserMaxDate($dtBuff, $dtSince, 0 /*missing*/);
		if ($strSince === false)
		{
			XLogError("Team::getActiveUserCount - since getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		if ($strSince === 0 /*missing*/)
			return -1; // missing
		//------------------
		// always want now or fail
		$valuesNow = $this->getTeamUserValuesByDate($strLast);
		if ($valuesNow === false)
		{
			XLogError("Team::getActiveUserCount - now getTeamUserValuesByDate failed.");
			return false;
		}
		//------------------
		$valuesSince = $this->getTeamUserValuesByDate($strSince);
		if ($valuesSince === false)
		{
			XLogError("Team::getActiveUserCount - since getTeamUserValuesByDate failed.");
			return false;
		}
		//------------------
		$actSince = 0;
		foreach ($valuesNow as $id => $vNow)
		if ($validOnly === false || in_array($id, $validIDs))
			if (isset($valuesSince[$id]))
			{
				if (($vNow - $valuesSince[$id]) > 0)
					$actSince++;
				else if ($vNow !== $valuesSince[$id])
					XLogDebug("Team::getActiveUserCount id $id weird no match (now/day) ".XVarDump($vNow)."/".XVarDump($valuesSince[$id]));
			}
		//------------------
		return $actSince;
	}
	//------------------
	function getTeamMaxDate($dtMinDate, $dtMaxDate, $default = false)
	{
		global $db, $dbTeamFields;
		//------------------
		$strMaxDate = $dtMaxDate->format(MYSQL_DATETIME_FORMAT);
		$strMinDate = $dtMinDate->format(MYSQL_DATETIME_FORMAT);
		$StrToDateFormat = MYSQL_STR_TO_DATE_FORMAT;
		$fDate = DB_TEAM_DATE;
		//------------------
		$sql = "SELECT MAX(DATE_FORMAT($fDate,'$StrToDateFormat')) FROM ".$dbTeamFields->tableName." WHERE DATE_FORMAT($fDate,'$StrToDateFormat')>STR_TO_DATE('$strMinDate','$StrToDateFormat') AND DATE_FORMAT($fDate,'$StrToDateFormat')<STR_TO_DATE('$strMaxDate','$StrToDateFormat')";
		//------------------
		//XLogDebug("Team::getTeamMaxDate sql: $sql");		
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Team::getTeamMaxDate - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$r = $qr->GetRow(); // returns one row, numerically indexed field array
		if (!$r || !isset($r[0]))
			return $default;
		//------------------
		return $r[0];
	}
	//------------------
	function getTeamValuesByMaxDate($strMaxDate)
	{
		global $db, $dbTeamFields;
		//------------------
		$dbTeamFields->SetValues();
		//------------------
		$StrToDateFormat = MYSQL_STR_TO_DATE_FORMAT;
		//------------------
		$where = "DATE_FORMAT(".DB_TEAM_DATE.",'$StrToDateFormat')>STR_TO_DATE('$strMaxDate','$StrToDateFormat')";
		$orderby = DB_TEAM_DATE." DESC"; // want DESC, newest to oldest
		//------------------
		$sql = $dbTeamFields->scriptSelect($where, $orderby);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Team::getTeamValuesByMaxDate - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$values = array();
		while ($u = $qr->GetRowArray())
			$values[] = $u;
		//------------------
		return $values;
	}
	//------------------
	function getTeamUserCount()
	{
		global $db, $dbTeamUsersFields;
		//------------------
	
		$sql = "SELECT COUNT(DISTINCT(".DB_TEAM_USERS_USER_ID.")) FROM ".$dbTeamUsersFields->tableName;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Team::getTeamUserCount - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$r = $qr->GetRow(); // returns one row, numerically indexed field array
			if (!$r || !isset($r[0]))
			{
				XLogDebug("Team::getTeamUserCount validate worker count row failed");
				return false;
			}
		//------------------
		return $r[0];
	}
	//------------------
	function getTeamUserMaxDate($dtMinDate, $dtMaxDate, $default = false)
	{
		global $db, $dbTeamUsersFields;
		//------------------
		$strMaxDate = $dtMaxDate->format(MYSQL_DATETIME_FORMAT);
		$strMinDate = $dtMinDate->format(MYSQL_DATETIME_FORMAT);
		$StrToDateFormat = MYSQL_STR_TO_DATE_FORMAT;
		$fDate = DB_TEAM_USERS_DATE;
		//------------------
		$sql = "SELECT MAX(DATE_FORMAT($fDate,'$StrToDateFormat')) FROM ".$dbTeamUsersFields->tableName." WHERE DATE_FORMAT($fDate,'$StrToDateFormat')>STR_TO_DATE('$strMinDate','$StrToDateFormat') AND DATE_FORMAT($fDate,'$StrToDateFormat')<STR_TO_DATE('$strMaxDate','$StrToDateFormat')";
		//------------------
		//XLogDebug("Team::getTeamUserMaxDate sql: $sql");		
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Team::getTeamUserMaxDate - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$r = $qr->GetRow(); // returns one row, numerically indexed field array
		if (!$r || !isset($r[0]))
			return $default;
		//------------------
		return $r[0];
	}
	//------------------
	function getTeamUserValuesByDate($strDate)
	{
		global $db, $dbTeamUsersFields;
		//------------------
		$dbTeamUsersFields->ClearValues();
		//------------------
		$dbTeamUsersFields->setValue(DB_TEAM_USERS_USER_ID);
		$dbTeamUsersFields->setValue(DB_TEAM_USERS_POINTS);
		//------------------
		$where = DB_TEAM_USERS_DATE."='".$strDate."'";
		//------------------
		$sql = $dbTeamUsersFields->scriptSelect($where);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Team::getTeamUserValuesByDate - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$values = array();
		while ($u = $qr->GetRowArray())
			if (isset($u[DB_TEAM_USERS_USER_ID]) && isset($u[DB_TEAM_USERS_POINTS]))
				$values[$u[DB_TEAM_USERS_USER_ID]] = $u[DB_TEAM_USERS_POINTS];
			else
				XLogDebug("Team::getTeamUserValuesByDate - not all fields set: ".XVarDump($u));
		//------------------
		return $values;
	}
	//------------------
	function trimOldData($ageDays = 20, $trimTeamUsers = false)
	{
		global $db, $dbTeamFields;
		//------------------
		if (!is_numeric($ageDays))
		{
			XLogError("Team::trimOldTeamData -validate ageDays parameter failed: ".XVarDump($ageDays));
			return false;
		}
		//------------------
		$dtMax = new DateTime('now',  new DateTimeZone('UTC')); 
		$dtMax->modify("-$ageDays day");
		//------------------
		$strMaxDate = $dtMax->format(MYSQL_DATETIME_FORMAT);
		$StrToDateFormat = MYSQL_STR_TO_DATE_FORMAT;
		$fDate = DB_TEAM_DATE;
		//------------------
		$where = "DATE_FORMAT($fDate,'$StrToDateFormat')<STR_TO_DATE('$strMaxDate','$StrToDateFormat')";
		//------------------
		$sql = $dbTeamFields->scriptDelete($where);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Team::trimOldTeamData - db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//------------------
		if ($trimTeamUsers === true)
		{
			//------------------
			$TeamUsers = new TeamUsers() or die("Create object failed");
			//------------------
			if (!$TeamUsers->trimOldData($ageDays))
			{
				XLogError("Team::trimOldTeamData - TeamUsers trimOldData failed");
				return false;
			}			
			//------------------
		}
		//------------------
		return true;
	}
	//------------------
	function lastFillNewRoundData()
	{
		global $db;
		//------------------
		$NewRoundData = new NewRoundData() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		if (!$NewRoundData->Clear())
		{
			XLogError("Team::lastFillNewRoundData NewRoundData Clear failed");
			return false;
		}
		//------------------
		$utcTimeZone = new DateTimeZone('UTC');
		//------------------
		$dtNow = new DateTime('now',  $utcTimeZone);
		$dtBuff = clone $dtNow;
		$dtBuff->modify("-1 day");
		//------------------
		$strLast = $this->getTeamUserMaxDate($dtBuff, $dtNow, false /*fail if missing*/);
		if ($strLast === false)
		{
			XLogError("Team::lastFillNewRoundData last getTeamUserMaxDate failed.");
			return false;
		}
		//------------------
		$dtLast = XStringToDate($strLast, $utcTimeZone);
		if ($dtLast === false)
		{
			XLogError("Team::lastFillNewRoundData XStringToDate dtLast from strLast failed : ".XVarDump($strLast));
			return false;
		}
		//------------------
		// always want now or fail
		$valuesNow = $this->getTeamUserValuesByDate($strLast);
		if ($valuesNow === false)
		{
			XLogError("Team::lastFillNewRoundData - now getTeamUserValuesByDate failed.");
			return false;
		}
		//------------------
		XLogDebug("Team::lastFillNewRoundData prep data took ".$timer->restartMs(true));
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("Team::lastFillNewRoundData db beginTransaction failed");
			return false;
		}
		//------------------
		foreach ($valuesNow as $id => $vNow)
			if (!$NewRoundData->addDataEntry($id, false /*workerName*/, NEWDATA_WORKERSTATUS_EXISTING /*workerStatus*/, 0 /*userWU*/, $vNow, NEWDATA_POINTSTATUS_NEW /*pointsStatus*/))
			{
				XLogError("Team::lastFillNewRoundData NewRoundData addDataEntry failed");
				return false;					
			} 
		//------------------
		if (!$db->commit())
		{
			XLogError("Team::lastFillNewRoundData db commit failed");
			return false;
		}
		//------------------
		XLogDebug("Team::lastFillNewRoundData added ".sizeof($valuesNow)." data entries took ".$timer->restartMs(true));
		//------------------
		return true;
	}
	//------------------
}// class Team
//---------------
//---------------
class TeamUsers
{
	//------------------
	function Install()
	{
		global $db, $dbTeamUsersFields;
		//------------------------------------
		$sql = $dbTeamUsersFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("TeamUsers::Install db Execute create users table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	} // Install
	//------------------
	function Uninstall()
	{
		global $db, $dbTeamUsersFields;
		//------------------------------------
		$sql = $dbTeamUsersFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("TeamUsers::Uninstall db Execute drop users table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	} // Uninstall
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbTeamUsersFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // 1->2 Updated credit/points fields to BIGINT, just do a copy
			case 2: // 2->3 changed date to string field
				//---------------
				$dbTeamUsersFields->SetValues();
				$dbTeamUsersFields->ClearValue(DB_TEAM_USERS_DATE);
				//---------------
				$sql = "INSERT INTO $dbTeamUsersFields->tableName (".$dbTeamUsersFields->GetNameListString(true /*skipNotSet*/).",".DB_TEAM_USERS_DATE." ) SELECT ".$dbTeamUsersFields->GetNameListString(true /*skipNotSet*/).",  DATE_FORMAT(".DB_TEAM_USERS_DATE.",'".MYSQL_STR_TO_DATE_FORMAT."') AS ".DB_TEAM_USERS_DATE." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("TeamUsers::Import db Execute table users import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbTeamUsersFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbTeamUsersFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("TeamUsers::Import db Execute table users import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("TeamUsers::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function trimOldData($ageDays = 20)
	{
		global $db, $dbTeamUsersFields;
		//------------------
		if (!is_numeric($ageDays))
		{
			XLogError("TeamUsers::trimOldData -validate ageDays parameter failed: ".XVarDump($ageDays));
			return false;
		}
		//------------------
		$dtMax = new DateTime('now',  new DateTimeZone('UTC')); 
		$dtMax->modify("-$ageDays day");
		//------------------
		$strMaxDate = $dtMax->format(MYSQL_DATETIME_FORMAT);
		$StrToDateFormat = MYSQL_STR_TO_DATE_FORMAT;
		$fDate = DB_TEAM_USERS_DATE;
		//------------------
		$where = "DATE_FORMAT($fDate,'$StrToDateFormat')<STR_TO_DATE('$strMaxDate','$StrToDateFormat')";
		//------------------
		$sql = $dbTeamUsersFields->scriptDelete($where);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("TeamUsers::trimOldData - db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
}// class TeamUsers
//---------------
?>
