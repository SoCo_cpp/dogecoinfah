<?php
/*
 *	www/include/Contributions.php
 * 
 * 
* 
*/
//---------------
define('CONT_OUTCOME_NONE',				0);
define('CONT_OUTCOME_PAID',				1);
define('CONT_OUTCOME_NOFUNDS_WAITING',	2);
define('CONT_OUTCOME_NOFUNDS_SKIPPED',	3);
define('CONT_OUTCOME_FAILED_WAITING',	4);
define('CONT_OUTCOME_FAILED_SKIPPING',	5);
define('CONT_OUTCOME_FAILED_WONTFIX',	6);
define('CONT_OUTCOME_FAILED_UPDATES',	7);
define('CONT_OUTCOME_TOO_LOW_SKIPPED',	8);
//---------------
define('CONT_MODE_NONE', 					0);
define('CONT_MODE_FLAT', 					1);
define('CONT_MODE_PERCENT', 				2);
define('CONT_MODE_ALL', 					3); // depreciated
define('CONT_MODE_EACH',					4);
define('CONT_MODE_PROPORTIONAL',			5);
define('CONT_MODE_RANDOM_DROP',				6);
define('CONT_MODE_PAY_INACTIVE_STORED',		7);
define('CONT_MODE_SPECIAL',					100);
define('CONT_MODE_SPECIAL_MAIN',			100); // not main for contribution.
define('CONT_MODE_SPECIAL_STORE_PAY',		101);
define('CONT_MODE_SPECIAL_STORE_FORFEIT',	102);
//---------------
define('CONT_FLAG_NONE', 			 		0x0);  // 0
define('CONT_FLAG_DISABLED', 		 		0x1);  // 1
define('CONT_FLAG_WAIT_FOR_FUNDS', 	 		0x2);  // 2
define('CONT_FLAG_FULL_BALANCE',     		0x4);  // 4
define('CONT_FLAG_MAX_TOTAL',    	 		0x8);  // 8
define('CONT_FLAG_MAX_COUNT',    			0x10); // 16
define('CONT_FLAG_MANUALLY_BYPASSED',  		0x20); // 32
define('CONT_FLAG_MAIN_CONTRIBUTION',  		0x4000); // 16384
//---------------
define('CONT_STEPDATA_CFGBASE_BALANCE_BASE', 'cont_stepdata_balance_%');
define('CONT_STEPDATA_CFGBASE_BALANCE_USED', 'cont_stepdata_balance_used_r');
define('CONT_STEPDATA_CFGBASE_BALANCE_ADDED', 'cont_stepdata_balance_added_r');
define('CONT_STEPDATA_CFGBASE_BALANCE_SIDEUSE', 'cont_stepdata_balance_sideuse_r');
define('CONT_STEPDATA_CFGBASE_BALANCE_ORIGINAL', 'cont_stepdata_balance_orig_r');
//---------------
class Contribution
{
	var $id = -1;
	var $dtCreated = false;
	var $dtDone = false;
	var $outcome = CONT_OUTCOME_NONE;
	var $roundIdx = false;
	var $order = false;
	var $name = "";
	var $trust = 0;
	var $mode = CONT_MODE_NONE;
	var $address = false;
	var $value = 0.0;
	var $subvalue = 0.0;
	var $countvalue = 0;
	var $flags = CONT_FLAG_DISABLED;
	var $txid = false;
	var $ad = false;
	var $total = false;
	var $count = false;
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id   		= $row[DB_CONTRIBUTION_ID];
		$this->dtCreated 	= $row[DB_CONTRIBUTION_DATE_CREATED];
		$this->dtDone 		= (isset($row[DB_CONTRIBUTION_DATE_DONE]) ? $row[DB_CONTRIBUTION_DATE_DONE] : false);
		$this->outcome 		= (isset($row[DB_CONTRIBUTION_OUTCOME]) ? $row[DB_CONTRIBUTION_OUTCOME] : CONT_OUTCOME_NONE);
		$this->roundIdx 	= (isset($row[DB_CONTRIBUTION_ROUND]) ? $row[DB_CONTRIBUTION_ROUND] : false);
		$this->order 		= (isset($row[DB_CONTRIBUTION_ORDER]) ? $row[DB_CONTRIBUTION_ORDER] : false);
		$this->name 		= (isset($row[DB_CONTRIBUTION_NAME]) ? $row[DB_CONTRIBUTION_NAME] : "");
		$this->trust 		= (isset($row[DB_CONTRIBUTION_TRUST]) ? $row[DB_CONTRIBUTION_TRUST] : 0);
		$this->mode 		= (isset($row[DB_CONTRIBUTION_MODE]) ? $row[DB_CONTRIBUTION_MODE] : CONT_MODE_NONE);
		$this->address 		= (isset($row[DB_CONTRIBUTION_ADDRESS]) ? $row[DB_CONTRIBUTION_ADDRESS] : false);
		$this->value 		= (isset($row[DB_CONTRIBUTION_VALUE]) ? $row[DB_CONTRIBUTION_VALUE] : 0.0);
		$this->subvalue		= (isset($row[DB_CONTRIBUTION_SUBVALUE]) ? $row[DB_CONTRIBUTION_SUBVALUE] : 0.0);
		$this->countvalue	= (isset($row[DB_CONTRIBUTION_COUNTVALUE]) ? $row[DB_CONTRIBUTION_COUNTVALUE] : 0);
		$this->flags 		= (isset($row[DB_CONTRIBUTION_FLAGS]) ? $row[DB_CONTRIBUTION_FLAGS] : CONT_FLAG_DISABLED);  
		$this->txid 		= (isset($row[DB_CONTRIBUTION_TXID]) ? $row[DB_CONTRIBUTION_TXID] : false);  
		$this->ad		 	= (isset($row[DB_CONTRIBUTION_AD]) && $row[DB_CONTRIBUTION_AD] !== -1 ? $row[DB_CONTRIBUTION_AD] : false);
		$this->total		= (isset($row[DB_CONTRIBUTION_TOTAL]) ? $row[DB_CONTRIBUTION_TOTAL] : false);
		$this->count		= (isset($row[DB_CONTRIBUTION_COUNT]) ? $row[DB_CONTRIBUTION_COUNT] : false);
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbContributionFields;
		//------------------
		$this->id			= -1;
		$this->dtCreated 	= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_DATE_CREATED);
		$this->dtDone		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_DATE_DONE);
		$this->outcome		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_OUTCOME);
		$this->roundIdx		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_ROUND);
		$this->order		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_ORDER);
		$this->name			= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_NAME);
		$this->trust		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_TRUST);
		$this->mode			= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_MODE);
		$this->address		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_ADDRESS);
		$this->value		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_VALUE);
		$this->subvalue		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_SUBVALUE);
		$this->countvalue	= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_COUNTVALUE);
		$this->flags		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_FLAGS);
		$this->txid			= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_TXID);
		$this->ad			= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_AD);
		$this->total		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_TOTAL);
		$this->count		= $dbContributionFields->GetMaxSize(DB_CONTRIBUTION_COUNT);
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbContributionFields;
		//---------------------------------
		$dbContributionFields->ClearValues();
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_DATE_CREATED,	$this->dtCreated);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_DATE_DONE,	XFalse2Null($this->dtDone));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_OUTCOME,	$this->outcome);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ROUND,		XFalse2Null($this->roundIdx));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ORDER,		XFalse2Null($this->order));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_NAME,		$this->name);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_TRUST,		$this->trust);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_MODE,		$this->mode);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ADDRESS,	XFalse2Null($this->address));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_VALUE,		$this->value);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_SUBVALUE,	$this->subvalue);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_COUNTVALUE,	$this->countvalue);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_FLAGS,		$this->flags);
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_TXID,		XFalse2Null($this->txid));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_AD,			XFalse2Null($this->ad));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_TOTAL,		XFalse2Null($this->total));
		$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_COUNT,		XFalse2Null($this->count));
		//---------------------------------
		$sql = $dbContributionFields->scriptUpdate(DB_CONTRIBUTION_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Contribution::Update db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbContributionFields->BindValues($dbs))
		{
			XLogError("Contribution::Update BindValues failed. sql: $sql");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Contribution::Update db prepared statement execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function isDisabled()
	{
		return $this->hasFlag(CONT_FLAG_DISABLED);
	}
	//------------------
	function isDone()
	{
		return ($this->dtDone !== false ? true : false);
	}
	//------------------
	function hasFlag($flagValue)
	{
		//------------------
		return XMaskContains($this->flags, $flagValue);
	}
	//------------------
	function setFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("Contribution::setFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		$this->flags |= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function clearFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("Contribution::clearFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		if ( ($this->flags & $flagValue) != 0)
			$this->flags ^= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function updateFlags($newValue = false)
	{
		//------------------
		if ( ($newValue !== false && !is_numeric($newValue)) || ($newValue === false && !is_numeric($this->flags)) )
		{
			XLogError("Contribution::updateFlags validate values failed");
			return false;
		}
		//------------------
		if ($newValue !== false)
			$this->flags = $newValue;
		//------------------
		if (!$this->updateValue(DB_CONTRIBUTION_FLAGS, $this->flags))
		{
			XLogError("Contribution::updateFlags updateValue failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function updateValue($field, $value, $useFalse2Null = true)
	{
		return $this->updateValues( array( $field => $value ), $useFalse2Null);
	}
	//------------------
	function updateValues($fieldValueArray, $useFalse2Null = true)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_array($fieldValueArray) || sizeof($fieldValueArray) == 0)
		{
			XLogError("Contribution::updateValues validate parameter failed: ".XVarDump($fieldValueArray));
			return false;
		}
		//------------------
		$dbContributionFields->ClearValues();
		foreach ($fieldValueArray as $field => $value)
			$dbContributionFields->SetValuePrepared($field, ($useFalse2Null === true ? XFalse2Null($value) : $value));
		//------------------
		$sql = $dbContributionFields->scriptUpdate(DB_CONTRIBUTION_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Contribution::updateValues db Prepare failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbContributionFields->BindValues($dbs))
		{
			XLogError("Contribution::updateValues BindValues failed. fieldValueArray: ".XVarDump($fieldValueArray));
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Contribution::updateValues db prepared statement execute failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function UpdateOutcome($outcome, $isDone = false, $txid = false, $total = false, $count = false)
	{
		//------------------
		if (!is_numeric($outcome) || ($total !== false && !is_numeric($total)) || ($count !== false && !is_numeric($count)))
		{
			XLogError("Contribution::UpdateOutcome validate parameters failed");
			return false;
		}
		//------------------
		$fieldValueArray = array();
		//------------------
		if ($isDone === true)
		{
			//------------------
			$this->txid = $txid;
			$this->dtDone = time();
			//------------------
			if ($total !== false)
				$this->total = $total;
			if ($count !== false)
				$this->count = $count;
			//------------------
			$fieldValueArray[DB_CONTRIBUTION_DATE_DONE] = $this->dtDone;
			$fieldValueArray[DB_CONTRIBUTION_TXID] = $this->txid;
			$fieldValueArray[DB_CONTRIBUTION_TOTAL] = ($this->total !== false ? $this->total : 0.0);
			$fieldValueArray[DB_CONTRIBUTION_COUNT] = ($this->count !== false ? $this->count : 0);
			//------------------
		}
		//------------------
		$this->outcome = $outcome;
		$fieldValueArray[DB_CONTRIBUTION_OUTCOME] = $this->outcome;
		//------------------
		if (!$this->updateValues($fieldValueArray))
		{
			XLogError("Contribution::UpdateOutcome updateValues failed.");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function manuallyBypass($note = '')
	{
		global $Login;
		$Contributions = new Contributions() or die('Create object failed');
		//------------------
		$txidNote = ($Login->LoggedIn && $Login->UserID !== null ? $Login->UserID : '-1');
		if ($note != '')
			$txidNote .= ','.$note;
		//------------------
		if (strlen($txidNote) > (C_MAX_TRANSACTION_ID_LENGTH - 2) )
			$txidNote = substr($txidNote, 0, (C_MAX_TRANSACTION_ID_LENGTH - 2));
		//------------------
		$this->dtDone = time();
		$this->flags |= CONT_FLAG_MANUALLY_BYPASSED;
		$this->txid = "[$txidNote]";
		//------------------
		if ($this->total === false)
			$this->total = 0.0;
		if ($this->count === false)
			$this->count = 0;
		//------------------
		$fieldValueArray = array( 	DB_CONTRIBUTION_DATE_DONE => $this->dtDone,
									DB_CONTRIBUTION_OUTCOME => $this->outcome,
									DB_CONTRIBUTION_FLAGS => $this->flags,
									DB_CONTRIBUTION_TXID => $this->txid,
									DB_CONTRIBUTION_TOTAL => ($this->total !== false ? $this->total : 0.0),
									DB_CONTRIBUTION_COUNT => ($this->count !== false ? $this->count : 0)
								);
		//------------------
		if (!$this->updateValues($fieldValueArray))
		{
			XLogError("Contribution::manuallyBypass updateValues failed.");
			return false;
		}
		//------------------
		$outcomeNames = $Contributions->getOutcomeNames();
		$strOutcome = "($this->outcome) '".(isset($outcomeNames[$this->outcome]) ? $outcomeNames[$this->outcome] : '[Unknown]')."'";
		//------------------
		if (!$Login->LoggedIn)
			$strUser = '[Not Logged In]';
		else
		{
			$uNum = ($Login->UserID !== null ? $Login->UserID : -1);
			$uName = ($Login->UserName !== null ? $Login->UserName : '[UserName Null]');
			$strUser = "($uNum) '$uName'";
		}
		//------------------
		XLogNotify("Contribution::manuallyBypass contribution $this->id '$this->name' manually bypased by user $strUser, with outcome $strOutcome, flags $this->flags, total $this->total, count $this->count, note '$note'");
		//------------------
		return true;
	}
	//------------------
	function step(&$stepData)
	{
		//------------------
		$outcome = false;
		$failure = false;
		//------------------
		if ($this->address === false || $this->address == "")
		{
			//------------------
			XLogWarn("Contribution::step contribution $this->id has a blank address, ignoring");
			$outcome = CONT_OUTCOME_NONE; 
			$this->total = 0;
			$this->txid = false;
			//------------------
		}
		else if ($this->mode == CONT_MODE_NONE)
		{
			//------------------
			$outcome = CONT_OUTCOME_NONE; 
			$this->total = 0;
			$this->txid = false;
			//------------------
		}
		else if ($this->isDisabled())
		{
			//------------------
			XLogWarn("Contribution::step contribution $this->id is disabled, ignoring");
			$outcome = CONT_OUTCOME_NONE; 
			$this->total = 0;
			$this->txid = false;
			//------------------
		}
		else if ($this->mode == CONT_MODE_SPECIAL_STORE_FORFEIT)
		{
			if (!$stepData['forfeitEnabled'])
			{
				//------------------
				$outcome = CONT_OUTCOME_NONE; 
				$this->total = 0;
				$this->txid = false;
				//------------------
			}
			else
			{
				//------------------
				if (!$this->stepForfeitedPayouts($stepData))
				{
					XLogError("Contribution::step stepForfeitedPayouts (forfeit mode) failed");
					$failure = true;
				}
				//------------------
			}
			//------------------
		}
		else if ($this->mode == CONT_MODE_SPECIAL_STORE_PAY)
		{
			//------------------
			if (!$stepData['storeEnabled'])
			{
				//------------------
				$outcome = CONT_OUTCOME_NONE; 
				$this->total = 0;
				$this->txid = false;
				//------------------
			}
			else if (!$this->stepFlatRate($stepData, $this->value))
			{
				XLogError("Contribution::step stepFlatRate mode (store pay) failed");
				$failure = true;
			}
			//------------------
		}
		else if ($this->mode == CONT_MODE_FLAT)
		{
			//------------------
			if (!$this->stepFlatRate($stepData, $this->value, true /*addToBalance*/))
			{
				XLogError("Contribution::step stepFlatRate mode (flat rate) failed");
				$failure = true;
			}
			//------------------
		}
		else if ($this->mode == CONT_MODE_ALL)
		{
			//------------------
			if (!$this->stepFlatRate($stepData, false /*value set to contBalance*/, true /*addToBalance*/))
			{
				XLogError("Contribution::step stepFlatRate (all) failed");
				$failure = true;
			}
			//------------------
		}
		else if ($this->mode == CONT_MODE_EACH)
		{
			//------------------
			$totalValueMax = false;
			$totalCountMax = false;
			//------------------
			if (XMaskContains($this->flags, CONT_FLAG_FULL_BALANCE))
			{
				if ($this->address == $stepData['mainAddress'])
					$totalValueMax = bcsub(bcadd($stepData['originalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
				else
				{
					$totalValueMax = $this->getBalance($stepData['estContFee']); // returns 0.0 if the address doesn't exist
					if ($totalValueMax === false)
					{
						XLogError("Contribution::step (each) cont getBalance failed");
						$failure = true;
					}
				}
			}
			if (XMaskContains($this->flags, CONT_FLAG_MAX_TOTAL))
				if ($totalValueMax === false || bccomp($this->subvalue, $totalValueMax, 8) < 0)
					$totalValueMax = $this->subvalue;
			if (XMaskContains($this->flags, CONT_FLAG_MAX_COUNT))
					$totalCountMax = $this->countvalue;
			//------------------
			if (!$this->stepEach($stepData, $this->value /*valueEach*/, $totalValueMax, $totalCountMax))
			{
				XLogError("Contribution::step stepEach failed");
				$failure = true;
			}
			//------------------
		}
		else if ($this->mode == CONT_MODE_PROPORTIONAL)
		{
			//------------------
			if (!XMaskContains($this->flags, CONT_FLAG_FULL_BALANCE))
				$valueTotal = $this->value;
			else
			{
				if ($this->address == $stepData['mainAddress'])
					$valueTotal = bcsub(bcadd($stepData['originalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
				else
				{
					$valueTotal = $this->getBalance($stepData['estContFee']); // returns 0.0 if the address doesn't exist
					if ($valueTotal === false)
					{
						XLogError("Contribution::step (proportional) cont getBalance failed");
						$failure = true;
					}
				}
			}
			//------------------
			if (!$failure)
				if (!$this->stepProportional($stepData, $valueTotal, false /*eachValueMax*/, false /*totalCountMax*/))
				{
					XLogError("Contribution::step stepProportional failed");
					$failure = true;
				}
			//------------------
		}
		else if ($this->mode == CONT_MODE_RANDOM_DROP)
		{
			//------------------
			$totalValueMax = false;
			$totalCountMax = false;
			//------------------
			if (XMaskContains($this->flags, CONT_FLAG_FULL_BALANCE))
			{
				if ($this->address == $stepData['mainAddress'])
					$totalValueMax = bcsub(bcadd($stepData['originalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
				else
				{
					$totalValueMax = $this->getBalance($stepData['estContFee']); // returns 0.0 if the address doesn't exist
					if ($totalValueMax === false)
					{
						XLogError("Contribution::step (each) cont getBalance failed");
						$failure = true;
					}
				}
			}
			if (XMaskContains($this->flags, CONT_FLAG_MAX_TOTAL))
				if ($totalValueMax === false || bccomp($this->subvalue, $totalValueMax, 8) < 0)
					$totalValueMax = $this->subvalue;
			if (XMaskContains($this->flags, CONT_FLAG_MAX_COUNT))
					$totalCountMax = $this->countvalue;
			//------------------
			if (!$failure)
				if (!$this->stepRandomDrop($stepData, $this->value /*valueEach*/, $totalValueMax, $totalCountMax))
				{
					XLogError("Contribution::step stepRandomDrop failed");
					$failure = true;
				}
			//------------------
		}
		else if ($this->mode == CONT_MODE_PAY_INACTIVE_STORED)
		{
			//------------------
			$totalValueMax = false;
			$totalCountMax = false;
			//------------------
			if (XMaskContains($this->flags, CONT_FLAG_FULL_BALANCE))
			{
				if ($this->address == $stepData['mainAddress'])
					$totalValueMax = bcsub(bcadd($stepData['originalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
				else
				{
					$totalValueMax = $this->getBalance($stepData['estContFee']); // returns 0.0 if the address doesn't exist
					if ($totalValueMax === false)
					{
						XLogError("Contribution::step (PayStored) cont getBalance failed");
						$failure = true;
					}
				}
			}
			if (XMaskContains($this->flags, CONT_FLAG_MAX_TOTAL))
				if ($totalValueMax === false || bccomp($this->subvalue, $totalValueMax, 8) < 0)
					$totalValueMax = $this->subvalue;
			if (XMaskContains($this->flags, CONT_FLAG_MAX_COUNT))
					$totalCountMax = $this->countvalue;
			//------------------
			if (!$failure)
				if (!$this->stepPayInactiveStored($stepData, $this->value /*valueEach*/, $totalValueMax, $totalCountMax))
				{
					XLogError("Contribution::step stepPayInactiveStored failed");
					$failure = true;
				}
			//------------------
		}
		else // $this->mode
		{
			XLogError("Contribution::step cont $this->id, unsupported mode: ".XVarDump($this->mode));
			$outcome = CONT_OUTCOME_FAILED_SKIPPING;
			$this->total = 0;
			$this->count = 0;
			$this->txid = false;
		}
		//------------------
		if ($failure || $outcome !== false)
		{
			//------------------
			if ($outcome !== false)
				$done = true;
			else
			{
				$outcome = CONT_OUTCOME_FAILED_WAITING;
				$done = false;
			}
			//------------------
			if (($this->dtDone === false && $done) || $this->outcome != $outcome)
				if (!$this->UpdateOutcome($outcome, $done, $this->txid, $this->total, $this->count))
				{
					XLogError("Contribution::step UpdateOutcome failed");
					return false;
				}
			//------------------
		} // if fail/outcome
		//------------------
		return true;
	}
	//------------------
	function getBalance($estContFee = false)
	{
		$Wallet = new Wallet() or die("Create object failed");
		//---------------------------------
		if ($estContFee === false)
		{
			$Config = new Config() or die("Create object failed");
			//---------------------------------
			$estContFee = $Config->Get(CFG_WALLET_EST_CONT_FEE);
			if ($estContFee === false)
			{
				$estContFee = DEF_CFG_WALLET_EST_CONT_FEE; // default
				if (!$Config->Set(CFG_WALLET_EST_CONT_FEE, $estContFee))
				{
					XLogError("Contribution::getBalance Config Set estcontfee failed");
					return false;
				}
			}
		}
		//------------------
		if (!is_numeric($estContFee))
		{
			XLogError("Contribution::getBalance validate parameters failed");
			return false;
		}
		//------------------
		$contBalance = $Wallet->getBalance($this->address); // returns 0.0 if the address doesn't exist
		if ($contBalance === false)
		{
			XLogError("Contribution::getBalance Wallet getBalance, failed. address: ".XVarDump($this->address));
			return false; 
		}
		//---------------------------------
		$contBalance = bcsub($contBalance, $estContFee, 8);
		//------------------
		return $contBalance;
	}
	//------------------
	function stepFlatRate(&$stepData, $value = false /*full balance*/, $addToBalance = false)
	{
		$Wallet = new Wallet() or die("Create object failed");
		//---------------------------------
		$ridx = $stepData['ridx'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_bool($testRun))
		{
			XLogError("Contribution::stepFlatRate validate statData ridx / testRun failed");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepFlatRate ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name, value ".($value === false ? '[Full]' : XVarDump($value)).", isMain ".($this->address == $stepData['mainAddress'] ? 'Yes' : 'No').", addToBalance ".($addToBalance ? 'Yes' : 'No'));
		XLogDebug("Contribution::stepFlatRate before totals: originalBalance ".$stepData['originalBalance'].", balance ".$stepData['balance'].", balanceAdded ".$stepData['balanceAdded'].", balanceUsed ".$stepData['balanceUsed'].", balanceSideUse ".$stepData['balanceSideUse']);
		//------------------
		$balanceValue = false;
		$outcome = CONT_OUTCOME_NONE;
		$done = false;
		//------------------
		if ($this->outcome != CONT_OUTCOME_NONE && $this->outcome != CONT_OUTCOME_NOFUNDS_WAITING)
		{
			XLogError("Contribution::stepFlatRate contribution outcome is already set, this process doesn't support re-entrance. Outcome: ".XVarDump($this->outcome));
			return false;
		}
		//------------------
		if ($this->address == $stepData['mainAddress'])
		{
			//---------------------------------
			$mainBalance = bcsub(bcadd($stepData['originalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
			//---------------------------------
			if ($value === false)
			{
				//---------------------------------
				$value = $mainBalance;
				$balanceValue = true;
				$this->total = $value;
				$outcome = CONT_OUTCOME_PAID;
				$done = true;
				//---------------------------------
			}
			else
			{
				//---------------------------------
				if (bccomp($value, $mainBalance, 8) > 0)
				{
					XLogWarn("Contribution::stepFlatRate from mainAddress of value $value exceeds the mainBalance of $mainBalance. originalBalance ".$stepData['originalBalance'].", balance ".$stepData['balance'].", balanceUsed ".$stepData['balanceUsed'].", balanceAdded ".$stepData['balanceAdded']);
					if ($this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
					{
						$fullMainBalance = bcsub(bcadd($stepData['fullOriginalBalance'], $stepData['balanceAdded'], 8), $stepData['balanceUsed'], 8);
						$outcome = CONT_OUTCOME_NOFUNDS_WAITING;
						$Monitor = new Monitor() or die('Create object failed');
						if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $value, 'Cont stepFlatRate' /*source*/, false /*isAlarm*/, $fullMainBalance, $stepData['estContFee'], $mainBalance, 'mainAddress value > mainBalance'/*note*/))
						{
							XLogError("Contribution::stepFlatRate Monitor ContributionWaitingForFunds (mainAddress) failed");
							return false; 
						}
					}
					else
					{
						$outcome = CONT_OUTCOME_NOFUNDS_SKIPPED;
						$this->total = 0;
						$done = true;
					}						
				}
				else
				{
					$this->total = $value;
					$outcome = CONT_OUTCOME_PAID;
					$done = true;
				}
				//---------------------------------
			}
			//---------------------------------
			if ($outcome == CONT_OUTCOME_PAID)
			{
				if ($addToBalance)
					$stepData['balanceAdded'] = bcadd($stepData['balanceAdded'], $value, 8);
				else
					$stepData['balanceUsed'] = bcadd($stepData['balanceUsed'], $value, 8);
			}
			//---------------------------------
			$this->txid = false; // no transaction here for mainAddress contributions, just record value, mark paid and update balances
			//---------------------------------
			if (!$this->UpdateOutcome($outcome, $done, false /*txid*/, $this->total))
			{
				XLogError("Contribution::stepFlatRate UpdateOutcome (mainAddress) failed");
				return false;
			}
			//---------------------------------
			XLogDebug("Contribution::stepFlatRate after totals: originalBalance ".$stepData['originalBalance'].", balance ".$stepData['balance'].", balanceAdded ".$stepData['balanceAdded'].", balanceUsed ".$stepData['balanceUsed'].", balanceSideUse ".$stepData['balanceSideUse']);
			//---------------------------------
			return true;
		} // if mainAddress
		//---------------------------------
		$fullContBalance = $this->getBalance(0 /*ignore fee*/); // returns 0.0 if the address doesn't exist
		if ($fullContBalance === false)
		{
			XLogError("Contribution::stepFlatRate cont getBalance, failed. address: ".XVarDump($this->address));
			return false; 
		}
		//---------------------------------
		$contBalance = bcsub($fullContBalance, $stepData['estContFee'], 8);
		//---------------------------------
		if ($value === false)
		{
			$balanceValue = true;
			$value = $contBalance;
		}
		//---------------------------------
		if ($stepData['dryRun'] === true) // dry run ignores balance checks
		{
			//------------------
			XLogDebug("Contribution::stepFlatRate cont $this->id faking dry run. cont address: ".XVarDump($this->address).", mainAddress: ".XVarDump($stepData['mainAddress']).", value: ".XVarDump($value).", prev conBalance: ".XVarDump($contBalance));
			//------------------
			if ($addToBalance)
				$stepData['balanceAdded'] = bcadd($stepData['balanceAdded'], $value, 8);
			else
				$stepData['balanceSideUse'] = bcadd($stepData['balanceSideUse'], $value, 8);
			//------------------
			$outcome = CONT_OUTCOME_PAID;
			$done = true;
			$this->total = $value;
			$this->txid = "[DryRun-".($testRun ? "Test-" : "")."Cont-$this->id]";
			//------------------
		}
		else if (bccomp($contBalance, $value, 8) < 0) // contBalance less than value
		{
			//------------------
			XLogNotify("Contribution::stepFlatRate cont $this->id insufficient funds. address: ".XVarDump($this->address).", fullContBalance: ".XVarDump($fullContBalance)." - estContFee: ".XVarDump($stepData['estContFee'])." = contBalance: ".XVarDump($contBalance).", needed value: ".XVarDump($value));
			if ($this->outcome == CONT_OUTCOME_NONE)
			{
				//------------------
				if ($this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
				{
					$outcome = CONT_OUTCOME_NOFUNDS_WAITING;
					$Monitor = new Monitor() or die('Create object failed');
					if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $value, 'Cont stepFlatRate' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'value < contBalance' /*note*/))
					{
						XLogError("Contribution::stepFlatRate Monitor ContributionWaitingForFunds failed");
						return false; 
					}
				}
				else
				{
					$outcome = CONT_OUTCOME_NOFUNDS_SKIPPED;
					$this->total = 0;
					$this->txid = false;
					$done = true;
				}
				//------------------
			}
			//------------------
		}
		else if (bccomp($value, "0.0", 8) != 1) // value not greater than zero
		{
			//------------------
			XLogNotify("Contribution::stepFlatRate cont $this->id value zero, marking paid. value: ".XVarDump($value));
			//------------------
			if ($balanceValue && $this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
			{
				$outcome = CONT_OUTCOME_NOFUNDS_WAITING;
				$Monitor = new Monitor() or die('Create object failed');
				if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $value, 'Cont stepFlatRate' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'value <= zero'))
				{
					XLogError("Contribution::stepFlatRate Monitor ContributionWaitingForFunds (value less eq zero) failed");
					return false; 
				}
			}
			else
			{
				$outcome = CONT_OUTCOME_TOO_LOW_SKIPPED;
				$this->total = 0;
				$this->txid = false;
				$done = true;
			}						
			//------------------
		}
		else if (bccomp($value, $stepData['payMinimum'], 8) < 0) 
		{
			//------------------
			XLogNotify("Contribution::stepFlatRate cont $this->id value $value below payMinimum ".$stepData['payMinimum']);
			//------------------
			if ($balanceValue && $this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
			{
				$outcome = CONT_OUTCOME_NOFUNDS_WAITING;
				$Monitor = new Monitor() or die('Create object failed');
				if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $value, 'Cont stepFlatRate' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'value < payMin'))
				{
					XLogError("Contribution::stepFlatRate Monitor ContributionWaitingForFunds (value less payMin) failed");
					return false; 
				}
			}
			else
			{
				$outcome = CONT_OUTCOME_TOO_LOW_SKIPPED;
				$this->total = 0;
				$this->txid = false;
				$done = true;
			}						
			//------------------
		}
		else
		{
			//------------------
			$value = bcadd($value, "0.0", 8); // trim to 8 decimal points by adding zero
			//------------------
			$txid = $Wallet->send($this->address, $stepData['mainAddress'], $value, $testRun);
			if ($txid === false) // tx failed
			{
				XLogNotify("Contribution::stepFlatRate cont $this->id Wallet send failed. cont address: ".XVarDump($this->address).", mainAddress: ".XVarDump($stepData['mainAddress']).", fullContBalance: ".XVarDump($fullContBalance)." - estContFee: ".XVarDump($stepData['estContFee'])." = contBalance: ".XVarDump($contBalance).", needed value: ".XVarDump($value).", sendTxResult: ".XVarDump($Wallet->sendTxResult));
				if (!$testRun)
				{
					$Monitor = new Monitor() or die('Create object failed');
					if (!$Monitor->TransactionFailed($Wallet->sendTxResult, $Wallet->sendTxLastStep, 'Contribution: '.$this->name, false /*isAlarm*/))
					{
						XLogError("Contribution::stepFlatRate Monitor TransactionFailed failed");
						return false;
					}
				}
				if ($Wallet->sendTxResult['hasFunds'] !== false)
					$outcome = CONT_OUTCOME_FAILED_WAITING; // just failed
				else if ($this->outcome == CONT_OUTCOME_NONE) // insufficient funds				
				{
					//------------------
					if ($this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
					{
						$outcome = CONT_OUTCOME_NOFUNDS_WAITING;
						$Monitor = new Monitor() or die('Create object failed');
						if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $value, 'Cont stepFlatRate' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'tx failed for no funds'))
						{
							XLogError("Contribution::stepFlatRate Monitor ContributionWaitingForFunds (tx failed no funds) failed");
							return false; 
						}
					}
					else
					{
						$outcome = CONT_OUTCOME_NOFUNDS_SKIPPED;
						$this->total = 0;
						$this->txid = false;
						$done = true;
					}						
					//------------------
				}
				//------------------
			}
			else // tx success
			{
				//------------------
				XLogDebug("Contribution::stepFlatRate cont $this->id send success. cont address: ".XVarDump($this->address).", mainAddress: ".XVarDump($stepData['mainAddress']).", value: ".XVarDump($value).", prev contBalance: ".XVarDump($contBalance));
				$outcome = CONT_OUTCOME_PAID;
				$done = true;
				$this->total = $value;
				$this->txid = $txid;
				//------------------
				if ($addToBalance)
					$stepData['balanceAdded'] = bcadd($stepData['balanceAdded'], $this->total, 8);
				else
					$stepData['balanceSideUse'] = bcadd($stepData['balanceSideUse'], $this->total, 8);
				//------------------
				$Config = new Config() or die("Create object failed");
				//------------------
				$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
				if (!$Config->Set(CFG_ROUND_LAST_TRANSACTION, $nowUtc->format(MYSQL_DATETIME_FORMAT)))
					XLogError("Contribution::stepFlatRate cont $this->id, Config failed to set last transaction time (continuing).");
				//------------------
			}
			//------------------
		}
		//------------------
		if ($outcome != CONT_OUTCOME_NONE || $done !== false)
			if (!$this->UpdateOutcome($outcome, $done, $this->txid, $this->total))
			{
				XLogError("Contribution::stepFlatRate UpdateOutcome failed");
				return false;
			}
		//------------------
		XLogDebug("Contribution::stepFlatRate after totals: originalBalance ".$stepData['originalBalance'].", balance ".$stepData['balance'].", balanceAdded ".$stepData['balanceAdded'].", balanceUsed ".$stepData['balanceUsed'].", balanceSideUse ".$stepData['balanceSideUse']);
		//---------------------------------
		return true;
	}
	//------------------
	function getStepLists($ridx)
	{
		$Stats = new Stats() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		//------------------
		if (!is_numeric($ridx))
		{
			XLogError("Contribution::getStepLists validate ridx failed");
			return false;
		}
		//------------------
		$timer = new XTimer();
		$statsList = $Stats->findRoundStats($ridx, DB_STATS_WEEK_POINTS." DESC" /*orderBy*/);
		if ($statsList === false)
		{
			XLogError("Contribution::getStepLists Stats failed to findRoundStats");
			return false;
		}
		//------------------
		$payoutList = $Payouts->findRoundPayouts($ridx);
		if ($payoutList === false)
		{
			XLogError("Contribution::getStepLists Payouts failed to findRoundPayouts");
			return false;
		}
		//------------------
		$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*validOnly*/);
		if ($workerNameList === false)
		{
			XLogError("Contribution::getStepLists Workers failed to getWorkerNameIDList");
			return false;
		}
		//------------------
		$payoutWorkerIdxList = array();
		foreach ($payoutList as $p)
			$payoutWorkerIdxList[$p->workerIdx] = $p;
		//------------------
		$tmpStatsList = array();
		foreach ($statsList as $stat)
		{
			$work = $stat->work();
			if ($work !== false && $work > 0)
				$tmpStatsList[] = array($stat->id, $work, $stat->workerIdx);
		}
		//------------------
		// ensure sorted by work (queried by week points desc)
		$statsList = $tmpStatsList;
		$sLen = sizeof($statsList);
		$done = false;
		while (!$done)
		{
			$done = true;
			for ($s = 1;$s < $sLen;$s++)
				if ($statsList[$s-1][1 /*work*/] < $statsList[$s][1 /*work*/])
				{
					$t = $statsList[$s-1];
					$statsList[$s-1] = $statsList[$s];
					$statsList[$s] = $t;
					$done = false;
				}
		}
		//------------------
		XLogDebug("Contribution::getStepLists prepared lists stats ".sizeof($statsList).", payouts ".sizeof($payoutList).", workerNames ".sizeof($workerNameList).", and payoutWorkerIdxList ".sizeof($payoutWorkerIdxList)." took ".$timer->restartMs(true));
		//------------------
		return array($statsList, $payoutList, $workerNameList, $payoutWorkerIdxList);
	}
	//------------------
	function stepRandomDrop(&$stepData, $valueEach, $totalValueMax = false, $totalCountMax = false)
	{
		global $db;
		$Stats = new Stats() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$timerFull = new XTimer();
		$timer = new XTimer();
		$timer2 = new XTimer();
		//------------------
		$ridx = $stepData['ridx'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_bool($testRun))
		{
			XLogError("Contribution::stepRandomDrop validate statData ridx / testRun failed");
			return false;
		}
		//------------------
		$valueEach = bcadd($valueEach, '0.0', $stepData['digits']); // trim to digits by adding zero
		//------------------
		XLogDebug("Contribution::stepRandomDrop ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name, valueEach $valueEach, totalValueMax ".XVarDump($totalValueMax).", totalCountMax ".XVarDump($totalCountMax));
		//------------------
		if ($this->outcome != CONT_OUTCOME_NONE && $this->outcome != CONT_OUTCOME_NOFUNDS_WAITING)
		{
			XLogError("Contribution::stepRandomDrop contribution outcome is already set, this process doesn't support re-entrance. Outcome: ".XVarDump($this->outcome));
			return false;
		}
		//------------------
		if (bccomp($valueEach, $stepData['rewardMinimum'], 8) < 0)
		{
			//------------------
			XLogWarn("Contribution::stepRandomDrop valueEach $valueEach is below the rewardMinimum ".$stepData['rewardMinimum']." skipping");
			//------------------
			if ($this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
			{
				$Monitor = new Monitor() or die('Create object failed');
				if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $valueEach, 'Cont stepRandomDrop' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'valueEach < rewardMinimum ('.$stepData['rewardMinimum'].')'))
				{
					XLogError("Contribution::stepRandomDrop Monitor ContributionWaitingForFunds failed");
					return false; 
				}
				if (!$this->UpdateOutcome(CONT_OUTCOME_NOFUNDS_WAITING, false /*isDone*/))
				{
					XLogError("Contribution::stepRandomDrop UpdateOutcome (valEa below rewMin) failed");
					return false;
				}
			}
			else if (!$this->UpdateOutcome(CONT_OUTCOME_TOO_LOW_SKIPPED, true /*isDone*/, 0 /*total*/))
			{
				XLogError("Contribution::stepRandomDrop UpdateOutcome (valEa below reMin) failed");
				return false;
			}
			//------------------
			return true;
		}
		//------------------
		$timer->start();
		//------------------
		$statsList = $Stats->findRoundStatsPointSummary($ridx, false /*workerIdxAsIndex*/, true /*weekPointsNotTotal*/, true /*onlyWithWeekPoints*/, DB_STATS_WEEK_POINTS." DESC" /*orderBy*/);
		if ($statsList === false)
		{
			XLogError("Contribution::stepRandomDrop Stats findRoundStatsPointSummary failed");
			return false;
		}
		//------------------
		$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*validOnly*/);
		if ($workerNameList === false)
		{
			XLogError("Contribution::stepRandomDrop Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		$payoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
		if ($payoutList === false)
		{
			XLogError("Contribution::stepRandomDrop Payouts findRoundPayouts failed ");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepRandomDrop lists found stats ".sizeof($statsList).", worker names ".sizeof($workerNameList).", payouts ".sizeof($payoutList).". took ".$timer->restartMs(true));
		//------------------
		$timer2->start();
		$rewardList = array();
		$rewardPayoutList = array();
		$createPayoutList = array();
		$failure = false;
		$totalValue = "0";
		$totalCount = 0;
		$statsAvailable = sizeof($statsList);
		//------------------
		while (!$failure) // should break out of loop when done
		{
			//------------------
			if ($totalCountMax !== false && $totalCount >= $totalCountMax)
			{
				XLogDebug("Contribution::stepRandomDrop totalCount hit totalCountMax, done. ".XVarDump($totalCount)." / ".XVarDump($totalCountMax));
				break;
			}
			//------------------
			if ($statsAvailable <= 0)
			{
				XLogDebug("Contribution::stepRandomDrop rand out of stats from workers available to choose from, done. statsAvailable ".XVarDump($statsAvailable).", statsCount ".XVarDump($statsCount).", curSizeofStatsList ".sizeof($statsList).", totalCount (picked already) ".XVarDump($totalCount));
				break;
			}
			//------------------
			$randStatIdx = XRand(1, $statsAvailable) - 1; // XRand min and max are inclusive so use 1 to count, then subtract one
			//------------------
			if ($randStatIdx < 0 || $randStatIdx >= $statsAvailable || !isset($statsList[$randStatIdx]))
			{
				XLogError("Contribution::stepRandomDrop validate random stat index failed. randStatIdx ".XVarDump($randStatIdx).", statsAvailable ".XVarDump($statsAvailable).", statsCount ".XVarDump($statsCount).", curSizeofStatsList ".sizeof($statsList).", totalCount (picked already) ".XVarDump($totalCount));
				$failure = true;
				break;
			}
			//------------------
			$statData = $statsList[$randStatIdx];
			//------------------
			$sidx = $statData['id'];
			// $work = $statData['wp'];
			$widx = $statData['wid'];
			//------------------
			if (!is_numeric($sidx) || !is_numeric($widx))
			{
				XLogError("Contribution::stepRandomDrop validate statData failed: ".XVarDump($statData));
				$failure = true;
				break;
			}
			//------------------
			if (!isset($workerNameList[$widx]))
			{
				XLogError("Contribution::stepRandomDrop stat $sidx workerIdx $widx not found in workerNameList");
				$failure = true;
				break;
			}
			//------------------
			$newTotalValue = bcadd($totalValue, $valueEach, 8);
			//------------------
			if ($totalValueMax !== false && bccomp($newTotalValue, $totalValueMax, 8) > 0)
			{
				XLogDebug("Contribution::stepRandomDrop newTotalValue exceeded totalValueMax, done. At $totalValue / ".XVarDump($totalValueMax).", and the next one would make it $newTotalValue");
				break;
			}
			//------------------
			$statsList = XArrayRemove($statsList, $randStatIdx);
			$statsAvailable--;
			$totalCount++;
			$totalValue = $newTotalValue;
			//------------------
			$rewardList[] = array($sidx, $widx);
			//------------------
			if (!isset($payoutList[$widx]))
				$createPayoutList[] = array($sidx, $ridx, $widx, $workerNameList[$widx]);
			//------------------
		} // foreach statList
		//------------------
		if ($failure !== false)
			return false;
		//------------------
		XLogDebug("Contribution::stepRandomDrop found ".sizeof($rewardList)." workers to reward and queued to create ".sizeof($createPayoutList)." new payouts for them. took ".$timer2->restartMs(true));
		//------------------
		if (sizeof($createPayoutList) != 0)
		{
			//------------------
			if (!$db->beginTransaction())
			{
				XLogError("Contribution::stepRandomDrop db beginTransaction failed");
				return false;
			}
			//------------------
			foreach ($createPayoutList as $payoutData)
				if (!$Payouts->addPayout($payoutData[1], $payoutData[2], $payoutData[3], 0.0 /*statPay*/))
				{
					XLogError("Contribution::stepRandomDrop  Payouts addPayout for stat ".$payoutData[1]." workerIdx ".$payoutData[2]." failed");
					$failure = true;
					break;
				}
			//------------------
			if ($failure !== false)
			{
				if (!$db->rollBack())
				{
					XLogError("Contribution::stepRandomDrop db rollBack failed");
					return false;
				}
				return false;
			}
			//------------------
			if (!$db->commit())
			{
				XLogError("Contribution::stepRandomDrop db commit failed");
				return false;
			}
			//------------------
			$timeCreating = $timer2->elapsedMs(true);
			//------------------
			$payoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
			if ($payoutList === false)
			{
				XLogError("Contribution::stepRandomDrop Payouts findRoundPayouts after creating new payouts failed ");
				return false;
			}
			//------------------
			XLogDebug("Contribution::stepRandomDrop created ".sizeof($createPayoutList)." new payouts, then read ".sizeof($payoutList)." updated round payouts. took ".$timer2->elapsedMs(true).", $timeCreating creating payouts");
			//------------------
		} // sizeof($createPayoutList) != 0
		//------------------
		if ($failure !== false)
			return false;
		//------------------
		foreach ($rewardList as $rewardData)
			if (!isset($payoutList[$rewardData[1 /*widx*/]]))
			{
				XLogError("Contribution::stepRandomDrop failed to find expected reward in payoutList after creating new payouts. widx ".XVarDump($rewardData[1 /*widx*/]));
				return false;
			}
			else $rewardPayoutList[] = $payoutList[$rewardData[1 /*widx*/]];
		//------------------
		$this->count = $totalCount;
		XLogDebug("Contribution::stepRandomDrop prepared payoutList for $valueEach each to $totalCount / ".sizeof($rewardPayoutList)." workers totaling $totalValue took ".$timer->restartMs(true));
		//------------------
		if (!$this->stepFlatRate($stepData, $totalValue))
		{
			XLogError("Contribution::stepRandomDrop stepFlatRate failed. totalValue was $totalValue");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepRandomDrop stepFlatRate done ".($this->outcome == CONT_OUTCOME_PAID ? "YES" : "NO")." took ".$timer->restartMs(true));
		//------------------
		if ($this->outcome == CONT_OUTCOME_PAID)
		{
			//------------------
			$notifyOutput = "Contribution::stepRandomDrop dropped $valueEach each on ".sizeof($payoutList)." random workers:\n";
			//------------------
			if (!$db->beginTransaction())
			{
				XLogError("Contribution::stepRandomDrop db beginTransaction failed");
				return false;
			}
			//------------------
			foreach ($rewardPayoutList as $payout)
			{
				if (!$payout->addUpdateStatPay($valueEach))
				{
					XLogError("Contribution::stepRandomDrop payout $payout->id failed to addUpdateStatPay");
					$failure = true;
					break;
				}
				$notifyOutput .= "\tWorker $payout->workerIdx, Payout $payout->id\n";
			}
			//------------------
			if (!$db->commit())
			{
				XLogError("Contribution::stepRandomDrop db commit failed");
				return false;
			}
			//------------------
			if ($testRun)
				XLogDebug("Contribution::stepRandomDrop [TestRun] dropped $valueEach each on ".sizeof($rewardPayoutList)." random workers to test, but the real payout will randomly pick different workers.");
			else
				XLogNotify($notifyOutput);
			//------------------
			if ($failure)
			{
				XLogWarn("Contribution::stepRandomDrop some update failures detecting, setting contribution outcome to failed updates.");
				if (!$this->UpdateOutcome(CONT_OUTCOME_FAILED_UPDATES, true /*isDone*/, $this->txid, $this->total))
					XLogError("Contribution::stepRandomDrop UpdateOutcome (updates failed) failed");
				return true;
			}
			//------------------
			XLogDebug("Contribution::stepRandomDrop addUpdateStatPay of ".sizeof($rewardPayoutList)." payoutList took ".$timer->restartMs(true));
			//------------------
		} // outcome paid
		//------------------
		XLogDebug("Contribution::stepRandomDrop full step took ".$timerFull->restartMs(true));
		//------------------
		return true;
	}
	//------------------
	function stepPayInactiveStored(&$stepData, $valueEach, $totalValueMax = false, $totalCountMax = false)
	{
		//------------------
		$Payouts = new Payouts() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$Config = new Config() or die("Create object failed");
		$timerFull = new XTimer();
		//------------------
		$ridx = $stepData['ridx'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_bool($testRun))
		{
			XLogError("Contribution::stepPayInactiveStored validate statData ridx / testRun failed");
			return false;
		}
		//------------------
		$payMinimum = $Config->GetSetDefault(CFG_ROUND_PAY_MINIMUM, DEFAULT_ROUND_PAY_MINIMUM);
		if ($payMinimum === false)
		{
			XLogError("Contribution::stepPayInactiveStored Config GetSetDefault pay minimum failed");
			return false;
		}
		//------------------
		$roundPayoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
		if ($roundPayoutList === false)
		{
			XLogError("Contribution::stepPayInactiveStored Payouts findRoundPayouts (roundPayoutList) failed ");
			return false;
		}
		//------------------
		$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*validOnly*/);
		if ($workerNameList === false)
		{
			XLogError("Contribution::stepPayInactiveStored Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		$valueEach = bcadd($valueEach, '0.0', $stepData['digits']); // trim to digits by adding zero
		//------------------
		XLogDebug("Contribution::stepPayInactiveStored ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name, valueEach $valueEach, totalValueMax ".XVarDump($totalValueMax).", totalCountMax ".XVarDump($totalCountMax));
		//------------------
		$storedPayoutList = $Payouts->loadStoredPayouts(false /*$onlyPaid*/, true /*$onlyUnpaid*/);
		if ($storedPayoutList === false)
		{
			XLogError("Contribution::stepPayInactiveStored - Payouts loadStoredPayouts failed");
			return false;
		}
		//------------------------	
		$workerPayouts = array();
		//------------------------	
		foreach ($storedPayoutList as $payout)
		{
			$valueStored = bcsub(bcsub($payout->statPay, $payout->storePay, 8), $payout->pay, 8);
			if (!isset($workerPayouts[$payout->workerIdx]))
				$workerPayouts[$payout->workerIdx] = $valueStored;
			else
				$workerPayouts[$payout->workerIdx] = bcadd($workerPayouts[$payout->workerIdx], $valueStored, 8);
		}
		//------------------------
		$fullTotal = "0";
		$fullCount = 0;
		//------------------
		foreach ($workerPayouts as $widx => $storedTotal)
			if (bccomp($storedTotal, $payMinimum, 8) >= 0 && !isset($roundPayoutList[$widx]))
			{
				$fullTotalNew = bcadd($fullTotal, $storedTotal, 8);
				if ( ($totalValueMax !== false && bccomp($fullTotalNew, $totalValueMax, 8) > 0) || ($totalCountMax !== false && $fullCount >= $totalCountMax) )
				{
					XLogDebug("Contribution::stepPayInactiveStored maximum hit, would be next total ".XVarDump($fullTotalNew)."/".XVarDump($totalValueMax).", count $fullCount/".XVarDump($totalCountMax));
					break;
				}
				else if (!isset($workerNameList[$widx]))
				{
					XLogError("Contribution::stepPayInactiveStored workerNameList doesn't contain worker idx: $widx, list size: ".sizeof($workerNameList));
					return false;
				}
				else if (!$Payouts->addPayout($ridx, $widx, $workerNameList[$widx], 0.0, 0.0))
				{
					XLogError("Contribution::stepPayInactiveStored Payouts addPayout failed");
					return false;
				}
				$fullTotal = $fullTotalNew;
				$fullCount++;
				XLogDebug("Contribution::stepPayInactiveStored added worker $widx ".substr($workerNameList[$widx], 0, 5)."... stored $storedTotal");
			}
		//------------------
		if (!$this->UpdateOutcome(CONT_OUTCOME_PAID /*outcome*/, true /*done*/, false /*txid*/, $fullTotal /*total*/, $fullCount /*count*/))
		{
			XLogError("Contribution::stepPayInactiveStored UpdateOutcome failed");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepPayInactiveStored added payouts for $fullCount workers totaling $fullTotal, full step took ".$timerFull->restartMs(true));
		//------------------
		return true;
	}
	//------------------
	function stepEach(&$stepData, $valueEach, $totalValueMax = false, $totalCountMax = false)
	{
		global $db;
		$Stats = new Stats() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$timer = new XTimer();
		$timer2 = new XTimer();
		$timerFull = new XTimer();
		//------------------
		$ridx = $stepData['ridx'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_bool($testRun))
		{
			XLogError("Contribution::stepEach validate statData ridx / testRun failed");
			return false;
		}
		//------------------
		$valueEach = bcadd($valueEach, '0.0', $stepData['digits']); // trim to digits by adding zero
		//------------------
		XLogDebug("Contribution::stepEach ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name, valueEach $valueEach, totalValueMax ".XVarDump($totalValueMax).", totalCountMax ".XVarDump($totalCountMax));
		//------------------
		if ($this->outcome != CONT_OUTCOME_NONE && $this->outcome != CONT_OUTCOME_NOFUNDS_WAITING)
		{
			XLogError("Contribution::stepEach contribution outcome is already set, this process doesn't support re-entrance. Outcome: ".XVarDump($this->outcome));
			return false;
		}
		//------------------
		if (bccomp($valueEach, $stepData['rewardMinimum'], 8) < 0)
		{
			//------------------
			XLogWarn("Contribution::stepEach valueEach $valueEach is below the rewardMinimum ".$stepData['rewardMinimum']." skipping");
			//------------------
			if ($this->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
			{
				$Monitor = new Monitor() or die('Create object failed');
				if (!$Monitor->ContributionWaitingForFunds(false /*round (auto detect)*/, $this->id, $valueEach, 'Cont stepEach' /*source*/, false /*isAlarm*/, $fullContBalance, $stepData['estContFee'], $contBalance, 'valueEach < rewardMinimum ('.$stepData['rewardMinimum'].')'))
				{
					XLogError("Contribution::stepEach Monitor ContributionWaitingForFunds failed");
					return false; 
				}
				if (!$this->UpdateOutcome(CONT_OUTCOME_NOFUNDS_WAITING, false /*isDone*/))
				{
					XLogError("Contribution::stepEach UpdateOutcome (valEa below rewMin) failed");
					return false;
				}
			}
			else	if (!$this->UpdateOutcome(CONT_OUTCOME_TOO_LOW_SKIPPED, true /*isDone*/, 0 /*total*/))
			{
				XLogError("Contribution::stepEach UpdateOutcome (valEa below reMin) failed");
				return false;
			}
			//------------------
			return true;
		}
		//------------------
		$timer->start();
		//------------------
		$statsList = $Stats->findRoundStatsPointSummary($ridx, false /*workerIdxAsIndex*/, true /*weekPointsNotTotal*/, true /*onlyWithWeekPoints*/, DB_STATS_WEEK_POINTS." DESC" /*orderBy*/);
		if ($statsList === false)
		{
			XLogError("Contribution::stepEach Stats findRoundStats failed");
			return false;
		}
		//------------------
		$workerNameList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*validOnly*/);
		if ($workerNameList === false)
		{
			XLogError("Contribution::stepEach Workers getWorkerNameIDList failed");
			return false;
		}
		//------------------
		$payoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
		if ($payoutList === false)
		{
			XLogError("Contribution::stepEach Payouts findRoundPayouts failed ");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepEach lists found stats ".sizeof($statsList).", worker names ".sizeof($workerNameList).", payouts ".sizeof($payoutList).". took ".$timer->restartMs(true));
		//------------------
		$timer2->start();
		$rewardList = array();
		$rewardPayoutList = array();
		$createPayoutList = array();
		$failure = false;
		$totalValue = "0";
		$totalCount = 0;
		foreach ($statsList as $statData)
		{
			//------------------
			$sidx = $statData['id'];
			// $work = $statData['wp'];
			$widx = $statData['wid'];
			//------------------
			if (!is_numeric($sidx) || !is_numeric($widx))
			{
				XLogError("Contribution::stepEach validate statData failed: ".XVarDump($statData));
				$failure = true;
				break;
			}
			//------------------
			if ($totalCountMax !== false && $totalCount >= $totalCountMax)
			{
				XLogDebug("Contribution::stepEach hit totalCountMax ".XVarDump($totalCountMax));
				break;
			}
			//------------------
			if (!isset($workerNameList[$widx]))
			{
				XLogError("Contribution::stepEach stat $sidx workerIdx $widx not found in workerNameList");
				$failure = true;
				break;
			}
			//------------------
			$newTotalValue = bcadd($totalValue, $valueEach, 8);
			//------------------
			if ($totalValueMax !== false)
				if (bccomp($newTotalValue, $totalValueMax, 8) >= 0)
				{
					XLogDebug("Contribution::stepEach hit totalValueMax $totalValue/".XVarDump($totalValueMax));
					break;
				}
			//------------------
			$totalCount++;
			$totalValue = $newTotalValue;
			//------------------
			$rewardList[] = array($sidx, $widx);
			//------------------
			if (!isset($payoutList[$widx]))
				$createPayoutList[] = array($sidx, $ridx, $widx, $workerNameList[$widx]);
			//------------------
		} // foreach statList
		//------------------
		if ($failure !== false)
			return false;
		//------------------
		XLogDebug("Contribution::stepEach found ".sizeof($rewardList)." workers to reward and queued to create ".sizeof($createPayoutList)." new payouts for them. took ".$timer2->restartMs(true));
		//------------------
		if (sizeof($createPayoutList) != 0)
		{
			//------------------
			if (!$db->beginTransaction())
			{
				XLogError("Contribution::stepEach db beginTransaction failed");
				return false;
			}
			//------------------
			foreach ($createPayoutList as $payoutData)
				if (!$Payouts->addPayout($payoutData[1], $payoutData[2], $payoutData[3], 0.0 /*statPay*/))
				{
					XLogError("Contribution::stepEach  Payouts addPayout for stat ".$payoutData[1]." workerIdx ".$payoutData[2]." failed");
					$failure = true;
					break;
				}
			//------------------
			if ($failure !== false)
			{
				if (!$db->rollBack())
				{
					XLogError("Contribution::stepEach db rollBack failed");
					return false;
				}
				return false;
			}
			//------------------
			if (!$db->commit())
			{
				XLogError("Contribution::stepEach db commit failed");
				return false;
			}
			//------------------
			$timeCreating = $timer2->elapsedMs(true);
			//------------------
			$payoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
			if ($payoutList === false)
			{
				XLogError("Contribution::stepEach Payouts findRoundPayouts failed ");
				return false;
			}
			//------------------
			XLogDebug("Contribution::stepEach created ".sizeof($createPayoutList)." new payouts, then read ".sizeof($payoutList)." updated round payouts. took ".$timer2->elapsedMs(true).", $timeCreating creating payouts");
			//------------------
		} // sizeof($createPayoutList) != 0
		//------------------
		if ($failure !== false)
			return false;
		//------------------
		foreach ($rewardList as $rewardData)
			if (!isset($payoutList[$rewardData[1 /*widx*/]]))
			{
				XLogError("Contribution::stepEach failed to find expected reward in payoutList after creating new payouts. widx ".XVarDump($rewardData[1 /*widx*/]));
				return false;
			}
			else $rewardPayoutList[] = $payoutList[$rewardData[1 /*widx*/]];
		//------------------
		$this->count = $totalCount;
		XLogDebug("Contribution::stepEach prepared payoutList for $valueEach each to $totalCount / ".sizeof($rewardList)." workers totaling $totalValue took ".$timer->restartMs(true));
		//------------------
		if (!$this->stepFlatRate($stepData, $totalValue))
		{
			XLogError("Contribution::stepEach stepFlatRate failed. totalValue was $totalValue");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepEach stepFlatRate done ".($this->outcome == CONT_OUTCOME_PAID ? "YES" : "NO")." took ".$timer->restartMs(true));
		//------------------
		if ($this->outcome == CONT_OUTCOME_PAID)
		{
			//------------------
			if (!$db->beginTransaction())
			{
				XLogError("Contribution::stepEach db beginTransaction failed");
				return false;
			}
			//------------------
			foreach ($rewardPayoutList as $payout)
				if (!$payout->addUpdateStatPay($valueEach))
				{
					XLogError("Contribution::stepEach payout $payout->id failed to addUpdateStatPay");
					$failure = true;
					break;
				}
			//------------------
			if (!$db->commit())
			{
				XLogError("Contribution::stepEach db commit failed");
				return false;
			}
			//------------------
			if ($failure)
			{
				XLogWarn("Contribution::stepEach some update failures detecting, setting contribution outcome to failed updates.");
				if (!$this->UpdateOutcome(CONT_OUTCOME_FAILED_UPDATES, true /*isDone*/, $this->txid, $this->total))
					XLogError("Contribution::stepEach UpdateOutcome (updates failed) failed");
				return true;
			}
			//------------------
			XLogDebug("Contribution::stepEach addUpdateStatPay of ".sizeof($rewardPayoutList)." payoutList took ".$timer->restartMs(true));
			//------------------
		} // outcome paid
		//------------------
		XLogDebug("Contribution::stepEach full step took ".$timerFull->restartMs(true));
		//------------------
		return true;
	}
	//------------------
	function stepForfeitedPayouts(&$stepData)
	{
		//------------------
		$Payouts = new Payouts() or die("Create object failed");
		//------------------
		$timer = new XTimer();
		$timerFull = new XTimer();
		$updatesFailed = false;
		//------------------
		$ridx = $stepData['ridx'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_bool($testRun))
		{
			XLogError("Contribution::stepForfeitedPayouts validate statData ridx / testRun failed");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepForfeitedPayouts ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name");
		//------------------
		if ($this->outcome != CONT_OUTCOME_NONE && $this->outcome != CONT_OUTCOME_NOFUNDS_WAITING)
		{
			XLogError("Contribution::stepForfeitedPayouts contribution outcome is already set, this process doesn't support re-entrance. Outcome: ".XVarDump($this->outcome));
			return false;
		}
		//------------------
		if (!$this->stepFlatRate($stepData, $this->value, true /*addToBalance*/))
		{
			XLogError("Contribution::stepForfeitedPayouts stepFlatRate failed. value was ".XVarDump($this->value));
			$failure = true;
		}
		//------------------
		XLogDebug("Contribution::stepForfeitedPayouts stepFlatRate done ".($this->outcome == CONT_OUTCOME_PAID ? "YES" : "NO")." took ".$timer->restartMs(true));
		//------------------
		if ($this->outcome == CONT_OUTCOME_PAID)
		{
			//------------------
			$payoutList = $Payouts->findRoundPayouts($ridx, false /*orderBy*/, false /*includePaid*/, false /*limit*/, WORKER_SPECIAL_MAIN /*workerIdx for forfeit to main*/);
			if ($payoutList === false)
			{
				XLogError("Contribution::stepForfeitedPayouts Payouts findRoundPayouts failed");
				$updatesFailed = true;
			}
			else
			{
				//------------------
				XLogDebug("Contribution::stepForfeitedPayouts Payouts findRoundPayouts found ".sizeof($payoutList)." payouts took ".$timer->restartMs(true));
				XLogNotify("Contribution::stepForfeitedPayouts ".($testRun ? "TEST " : "")."forfeiting ".sizeof($payoutList)." payouts for round $ridx");
				//------------------
				$forfeitedPayoutIDs = array();
				//------------------
				if (!$testRun)
					foreach ($payoutList as $payout)
					{
						//------------------
						$payout->dtPaid = $this->dtDone;
						$payout->txid = $this->txid;
						//------------------
						if (!$payout->Update())
						{
							XLogError("Contribution::stepForfeitedPayouts payout idx: ".XVarDump($payout->id)." Update failed, setting dtPaid: ".XVarDump($payout->dtPaid)." and txid: ".XVarDump($payout->txid));
							$updatesFailed = true;
						}
						else if (!$testRun)
						{
							//------------------
							$forfeitedPayoutIDs[] = $payout->id;
							//------------------
							if (!$Payouts->updateStorePaidPayouts($payout->id, $payout->dtPaid))
							{
								XLogError("Contribution::stepForfeitedPayouts Payouts updateStorePaidPayouts failed for payout id: ".XVarDump($payout->id).", dtPaid: ".XVarDump($payout->dtPaid));
								$updatesFailed = true;
							}
							//------------------
						}
						//------------------
					} // foreach $payoutList
				//------------------
				if (sizeof($forfeitedPayoutIDs) != 0)
					XLogNotify("Contribution::stepForfeitedPayouts payouts forfeited (".sizeof($forfeitedPayoutIDs)."): ".implode(',', $forfeitedPayoutIDs));
				//------------------
				XLogDebug("Contribution::stepForfeitedPayouts ".($testRun ? "TEST skipped updating " : "updated ").sizeof($payoutList)." store payouts forfeited as paid took ".$timer->restartMs(true));
				//------------------
			}
			//------------------
			if ($updatesFailed)
			{
				XLogWarn("Contribution::stepForfeitedPayouts some update failures detecting, setting contribution outcome to failed updates.");
				if (!$this->UpdateOutcome(CONT_OUTCOME_FAILED_UPDATES, true /*isDone*/, $this->txid, $this->total))
					XLogError("Contribution::stepForfeitedPayouts UpdateOutcome (updates failed) failed");
				return true;
			}
			//------------------
		} // outcome paid
		//------------------
		XLogDebug("Contribution::stepForfeitedPayouts full step took ".$timerFull->restartMs(true));
		//------------------
		return true;
	}
	//------------------
	function stepProportional(&$stepData, $valueTotal, $eachValueMax = false, $totalCountMax = false)
	{
		global $db;
		$Payouts = new Payouts() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$ridx = $stepData['ridx'];
		$rewardMinimum = $stepData['rewardMinimum'];
		$testRun = $stepData['testRun'];
		if (!is_numeric($ridx) || !is_numeric($rewardMinimum) || !is_bool($testRun))
		{
			XLogError("Contribution::stepProportional validate statData values failed");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepProportional ".($testRun ? '[TestRun] ' : '')."ridx $ridx, contribution $this->id, name $this->name, valueTotal $valueTotal, eachValueMax ".XVarDump($eachValueMax).", totalCountMax ".XVarDump($totalCountMax).", reward minimum $rewardMinimum");
		//------------------
		if ($this->outcome != CONT_OUTCOME_NONE && $this->outcome != CONT_OUTCOME_NOFUNDS_WAITING)
		{
			XLogError("Contribution::stepProportional contribution outcome is already set, this process doesn't support re-entrance. Outcome: ".XVarDump($this->outcome));
			return false;
		}
		//------------------
		$stepLists = $this->getStepLists($ridx);
		if ($stepLists === false)
		{
			XLogError("Contribution::stepProportional getStepLists failed");
			return false;
		}
		//------------------
		$statsList = $stepLists[0];
		$payoutList = $stepLists[1];
		$workerNameList = $stepLists[2];
		//------------------
		$fullPayCount = sizeof($statsList);
		$payCount = bcdiv($valueTotal, $rewardMinimum, 0); // round to whole number, max possible to pay
		if ($payCount < $fullPayCount)
			XLogDebug("Contribution::stepProportional adjusted start payCount to max possible of $payCount / $fullPayCount, as balance $valueTotal can be only split into this many parts of rewardMinimum $rewardMinimum");
		else
		{
			XLogDebug("Contribution::stepProportional starting payCount with fullPayCount, $fullPayCount, due to sufficient valueTotal, $valueTotal");
			$payCount = $fullPayCount;
		}
		//------------------
		// statsList [sidx, work, widx] + [address, pay, percent]
		$fullTotalWork = 0;
		$sLen = sizeof($statsList);
		for ($idx = 0;$idx < $sLen;$idx++)
		{
			$fullTotalWork += $statsList[$idx][1 /*work*/];
			if (!isset($workerNameList[$statsList[$idx][2 /*widx*/]]))
			{
				XLogError("Contribution::stepProportional find stat ".$statsList[$idx][1 /*sidx*/]."'s widx ".$statsList[$idx][2 /*widx*/]." in workerNameList failed");
				return false;
			}
			$statsList[$idx][3 /*address*/] = $workerNameList[$statsList[$idx][2 /*widx*/]];
			$statsList[$idx][4 /*pay*/] = 0;
			$statsList[$idx][5 /*percent*/] = 0;
		} // for statsList
		//------------------
		$timeoutTimer = new XTimer();
		$totPay = "0.0";
		$totWork = 0;
		$stepPayCount = max(floor($fullPayCount * 0.05), 5);
		$testPayCount = $payCount;
		$passCount = 0;
		$done = false;
		while (!$done && $payCount > 0)
		{
			if ($timeoutTimer->elapsedMs() > 15000)
			{
				XLogError("Contribution::stepProportional timeout timer hit on testPayCount: $testPayCount, passCount $passCount");
				return false;
			}
			$done = true;
			$totPay = "0.0";
			$totWork = 0;
			$scrapCount = 0;
			for ($idx = 0;$idx < $payCount;$idx++)
				$totWork += $statsList[$idx][1 /*work*/];
			if ($totWork <= 0)
			{
				XLogError("Contribution::stepProportional total work hit zero ($totWork), pass $passCount, testPayCount $testPayCount / $fullPayCount");
				return false;
			}
			for ($idx = 0;$idx < $testPayCount;$idx++)
			{
				$percent = bcdiv($statsList[$idx][1 /*work*/], $totWork, 8);
				$pay = bcmul($valueTotal, $percent, $stepData['digits']);
				if (bccomp($pay, $rewardMinimum, 8) >= 0) //  meets minimum
				{
					$totPay = bcadd($totPay, $pay, 8);
					$statsList[$idx][4 /*pay*/] = $pay;
					$statsList[$idx][5 /*percent*/] = $percent;
				}
				else 
				{
					$statsList[$idx][4 /*pay*/] = "0.0";
					$payCount = $testPayCount - 1;
					if ($payCount <= 0)
					{
						XLogError("Contribution::stepProportional payCount exhausted, pass $passCount");
						return false;
					}
					$testPayCount -= $stepPayCount;
					$done = false;
					break;
				}
			}
			if ($done && $payCount != $testPayCount)
			{
				XLogDebug("Contribution::stepProportional done found when jumping ahead from payCount $payCount, to testPayCount $testPayCount. Backtracking.");
				$testPayCount = $payCount;
				$stepPayCount = 1;
				$done = false;
			}
			$passCount++;
		}
		//------------------
		$this->count = $payCount;
		XLogDebug("Contribution::stepProportional spread rewards to $payCount / $fullPayCount worker stats paying on $totWork / $fullTotalWork points, paying $totPay / $valueTotal, in $passCount passes, took ".$timer->restartMs(true)); 
		//------------------
		if (!$this->stepFlatRate($stepData, $totPay))
		{
			XLogError("Contribution::stepProportional stepFlatRate failed. totPay was $totPay");
			return false;
		}
		//------------------
		XLogDebug("Contribution::stepProportional stepFlatRate done ".($this->outcome == CONT_OUTCOME_PAID ? "YES" : "NO")." took ".$timer->restartMs(true));
		//------------------
		if ($this->outcome == CONT_OUTCOME_PAID)
		{
			//------------------
			$workerPayoutIDList = array();
			foreach ($payoutList as $payout)
				$workerPayoutIDList[$payout->workerIdx] = $payout;
			//------------------
			$failure = false;
			$output = "\r\nwidx,address,points,percent,pay\r\n";
			//------------------
			if (!$db->beginTransaction())
			{
				XLogError("Contribution::stepProportional db beginTransaction failed");
				return false;
			}
			//------------------
			for ($idx = 0;$idx < $sLen;$idx++)
			{
				//------------------
				$statData = $statsList[$idx];
				//------------------
				$widx = $statData[2 /*widx*/];
				$address = $statData[3 /*address*/];
				$pay = $statData[4 /*pay*/];
				//------------------
				if ($idx < $payCount)
				{
					//------------------
					if (isset($workerPayoutIDList[$widx]))
					{
						if (!$workerPayoutIDList[$widx]->addUpdateStatPay($pay /*add stat pay*/))
						{
							XLogError("Contribution::stepProportional existing payout addUpdateStatPay failed at index $idx / $payCount, for statData entry: ".XVarDump($statData));
							$failure = true;
							break;
						}
					}
					else if (!$Payouts->addPayout($ridx, $widx, $address, $pay /*statPay*/))
					{
						XLogError("Contribution::stepProportional payouts failed to addPayout at index $idx / $payCount, for statData entry: ".XVarDump($statData));
						$failure = true;
						break;
					}
					//------------------
				} // if payCount not exceeded
				//------------------
				if (!$testRun)
				{
					//------------------
					$output .= $widx.','.substr($address, 0, 7).','.$statData[1 /*work*/].','.$statData[5 /*percent*/].','.$pay."\r\n";
					//------------------
					if (($idx % 100) == 0)
					{
						XLogRaw(XLOG_LEVEL_NOTIFY, $output, true /*leaveopen*/);
						$output = "";
					}
					//------------------
				} // if (!$testRun)
				//------------------
			} // for statData
			//------------------
			$output .= "\r\n";
			//------------------
			if (!$db->commit())
			{
				XLogError("Contribution::stepProportional db commit failed");
				return false;
			}
			//------------------
			if (!$testRun)
			{
				XLogRaw(XLOG_LEVEL_NOTIFY, $output, false /*leaveopen*/);
				XLogDebug("Contribution::stepProportional addUpdateStatPay/addPayout and debug dump for $payCount stats took ".$timer->restartMs(true)); 
			}
			else XLogDebug("Contribution::stepProportional [testRun] addUpdateStatPay/addPayout for $payCount stats took ".$timer->restartMs(true));
			//------------------
			if ($failure)
			{
				XLogWarn("Contribution::stepProportional some update failures detecting, setting contribution outcome to failed updates.");
				if (!$this->UpdateOutcome(CONT_OUTCOME_FAILED_UPDATES, true /*isDone*/, $this->txid, $this->total))
					XLogError("Contribution::stepProportional UpdateOutcome (updates failed) failed");
				return true;
			}
			//------------------
		} // outcome paid
		//------------------
		return true;
	}
	//------------------
} // class Contribution
//---------------
class Contributions
{
	//------------------
	function Install()
	{
		global $db, $dbContributionFields;
		//------------------------------------
		$sql = $dbContributionFields->scriptCreateTable();
		XLogDebug("Contributions::Install sql: $sql");
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbContributionFields;
		//------------------------------------
		$sql = $dbContributionFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbContributionFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1:
			case 2:
				//---------------
				$dbContributionFields->SetValues();
				$dbContributionFields->ClearValue(DB_CONTRIBUTION_TXID);
				$dbContributionFields->ClearValue(DB_CONTRIBUTION_AD);
				//---------------
				$sql = "INSERT INTO $dbContributionFields->tableName (".$dbContributionFields->GetNameListString(true/*SkipNotSet*/).") SELECT ".$dbContributionFields->GetNameListString(true/*SkipNotSet*/)." FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Contributions::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 3: // 3->4 renamed mode field that became a mysql keyword and added total field
				//---------------
				$dbContributionFields->SetValues();
				$dbContributionFields->ClearValue(DB_CONTRIBUTION_TOTAL); // total is a new field
				//---------------
				$newFields = $dbContributionFields->GetNameListString(true/*SkipNotSet*/);
				$fieldsArray = $dbContributionFields->GetNameArray(true/*SkipNotSet*/);
				//---------------
				for ($i=0;$i < sizeof($fieldsArray);$i++)
					if ($fieldsArray[$i] == DB_CONTRIBUTION_MODE)
						$fieldsArray[$i] = "`mode`";
				//---------------
				$oldFields = implode(",", $fieldsArray);
				//---------------
				$sql = "INSERT INTO $dbContributionFields->tableName ($newFields) SELECT $oldFields FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Contributions::Import db Execute table import failed (convert ver < 4) .\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case 4: // 4->5 changed DB_CONTRIBUTION_DATE_CREATED / DB_CONTRIBUTION_DATE_DONE from string to timestamp (unsigned int)
					// renamed DB_CONTRIBUTION_NUMBER 'num' to DB_CONTRIBUTION_ORDER 'ord'
					// added DB_CONTRIBUTION_TRUST 'trust', DB_CONTRIBUTION_SUBVALUE 'subval', DB_CONTRIBUTION_COUNTVALUE 'cntval', DB_CONTRIBUTION_COUNT 'cnt'
					// set all round contribs (non -1 ridx) with mainAddress with CONT_FLAG_MAIN_CONTRIBUTION flag
				//---------------
				$Wallet = new Wallet() or die("Create object failed");
				$mainAddress = $Wallet->getMainAddress();
				//---------------
				$sql = "SELECT * FROM $oldTableName";
				//---------------
				if ( !($qr = $db->Query($sql)) )
				{
					XLogError("Contributions::Import db Query select failed (convert ver < 5) .\nsql:\n$sql");
					return false;
				}
				//---------------
				if (!$db->beginTransaction())
				{
					XLogError("Contributions::Import db beginTransaction failed");
					return false;
				}
				//---------------
				while ( ($row = $qr->GetRowArray()) !== false)
				{
					//---------------
					if (isset($row[DB_CONTRIBUTION_ROUND]) && $row[DB_CONTRIBUTION_ROUND] >= 0 && isset($row[DB_CONTRIBUTION_ADDRESS]) && $row[DB_CONTRIBUTION_ADDRESS] == $mainAddress)
						$row[DB_CONTRIBUTION_FLAGS] = (isset($row[DB_CONTRIBUTION_FLAGS]) && $row[DB_CONTRIBUTION_FLAGS] !== null ? $row[DB_CONTRIBUTION_FLAGS] : CONT_FLAG_NONE) | CONT_FLAG_MAIN_CONTRIBUTION;
					//---------------					
					$dbContributionFields->ClearValues();
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ID, $row[DB_CONTRIBUTION_ID]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_OUTCOME, $row[DB_CONTRIBUTION_OUTCOME]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ROUND, $row[DB_CONTRIBUTION_ROUND]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ORDER, (isset($row['num']) ?  $row['num'] : (isset($row[DB_CONTRIBUTION_ORDER]) ? $row[DB_CONTRIBUTION_ORDER] : null)) );
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_NAME, $row[DB_CONTRIBUTION_NAME]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_MODE, $row[DB_CONTRIBUTION_MODE]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_ADDRESS, $row[DB_CONTRIBUTION_ADDRESS]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_VALUE, $row[DB_CONTRIBUTION_VALUE]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_FLAGS, $row[DB_CONTRIBUTION_FLAGS]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_TXID, $row[DB_CONTRIBUTION_TXID]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_AD, $row[DB_CONTRIBUTION_AD]);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_TOTAL, (isset($row[DB_CONTRIBUTION_TOTAL]) ? $row[DB_CONTRIBUTION_TOTAL] : null));
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_COUNT, (isset($row[DB_CONTRIBUTION_COUNT]) ? $row[DB_CONTRIBUTION_COUNT] : null));
					//---------------
					$strCreated = $row[DB_CONTRIBUTION_DATE_CREATED];
					$strDone = $row[DB_CONTRIBUTION_DATE_DONE];
					$tsCreated = XMixedToTimestamp($strCreated);
					if ($tsCreated === false)
					{
						XLogError("Contributions::Import convert dtCreated failed (convert ver < 5), cont id ".$row[DB_CONTRIBUTION_ID].", old dtCreated ".XVarDump($strCreated));
						if (!$db->rollBack())
							XLogError("Contributions::Import db rollBack after failure failed");
						return false;
					}
					if ($strDone === '' || $strDone === null)
						$tsDone = null;
					else
						$tsDone = XMixedToTimestamp($strDone);
					if ($tsDone === false)
					{
						XLogError("Contributions::Import convert dtDone failed (convert ver < 5), cont id ".$row[DB_CONTRIBUTION_ID].", old dtDone ".XVarDump($strDone));
						if (!$db->rollBack())
							XLogError("Contributions::Import db rollBack after failure failed");
						return false;
					}
					//---------------
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_DATE_CREATED, $tsCreated);
					$dbContributionFields->SetValuePrepared(DB_CONTRIBUTION_DATE_DONE, $tsDone);
					//---------------
					$sql = $dbContributionFields->scriptInsert();
					$dbs = $db->Prepare($sql);
					if ($dbs === false)
					{
						XLogError("Contributions::Import db Prepare failed.\nsql: $sql");
						if (!$db->rollBack())
							XLogError("Contributions::Import db rollBack after failure failed");
						return false;
					}
					//------------------
					if (!$dbContributionFields->BindValues($dbs))
					{
						XLogError("Contributions::Import BindValues failed. sql: $sql");
						if (!$db->rollBack())
							XLogError("Contributions::Import db rollBack after failure failed");
						return false;
					}
					//------------------
					if (!$dbs->execute())
					{
						XLogError("Contributions::Import prepared statement Execute failed.\nsql: $sql");
						if (!$db->rollBack())
							XLogError("Contributions::Import db rollBack after failure failed");
						return false;
					}
					//---------------
				} // while row
				//---------------
				if (!$db->commit())
				{
					XLogError("Contributions::Import db commit failed");
					return false;
				}
				//---------------
				break;
			case $dbContributionFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbContributionFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Contributions::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Contributions::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msize = new Contribution();
		$msizet->setMaxSizes();
		//------------------------------------
		return $msize;		
	}
	//------------------
	function getModeNames($short = false, $includeSpecial = true)
	{
		//------------------
		$names = array();
		//------------------
		$names[CONT_MODE_NONE]			= ($short ? 'None' 	: 'None / Disabled');
		$names[CONT_MODE_FLAT]			= ($short ? 'Flat' 	: 'Flat Amount');
		$names[CONT_MODE_PERCENT]		= ($short ? 'Perc.' : 'Percent');
		$names[CONT_MODE_ALL]			= ($short ? 'All' 	: 'Flat Amount (All)');
		$names[CONT_MODE_EACH]			= 'Each';
		$names[CONT_MODE_PROPORTIONAL]	= ($short ? 'Prop.' : 'Proportional');
		$names[CONT_MODE_RANDOM_DROP]	= ($short ? 'Rand.' : 'Random Drop');
		$names[CONT_MODE_PAY_INACTIVE_STORED] = ($short ? 'PayStored' : 'Pay Inactive Stored Rewards');
		//------------------
		if ($includeSpecial !== false)
		{
			//CONT_MODE_SPECIAL Special mode cutoff, not a real mode
			$names[CONT_MODE_SPECIAL_MAIN]			= 'Special Main';
			$names[CONT_MODE_SPECIAL_STORE_PAY]		= 'Special Store Pay';
			$names[CONT_MODE_SPECIAL_STORE_FORFEIT]	= 'Special Store Forfeit';
		}
		//------------------
		return $names;
	}
	//------------------
	function getOutcomeNames()
	{
		//------------------
		$names = array();
		//------------------
		$names[CONT_OUTCOME_NONE] = 'None';
		$names[CONT_OUTCOME_PAID] = 'Paid';
		$names[CONT_OUTCOME_NOFUNDS_WAITING] = 'Waiting for funds';
		$names[CONT_OUTCOME_NOFUNDS_SKIPPED] = 'Skipped, lack of funds';
		$names[CONT_OUTCOME_FAILED_WAITING]  = 'Failed, waiting to retry';
		$names[CONT_OUTCOME_FAILED_SKIPPING] = 'Failed, skipped';
		$names[CONT_OUTCOME_FAILED_WONTFIX]  = 'Failed, won\'t fix, skipped';
		$names[CONT_OUTCOME_FAILED_UPDATES]  = 'Paid, some updates failed';
		$names[CONT_OUTCOME_TOO_LOW_SKIPPED] = 'Too low, skipped';
		//------------------
		return $names;
	}
	//------------------
	function deleteContribution($idx)
	{
		global $db, $dbContributionFields;
		//---------------------------------
		$sql = $dbContributionFields->scriptDelete(DB_CONTRIBUTION_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::deleteContribution - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbContributionFields;
		//---------------------------------
		$sql = $dbContributionFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function loadContributionRaw($idx)
	{
		global $db, $dbContributionFields;
		//------------------
		$dbContributionFields->SetValues();
		//------------------
		$sql = $dbContributionFields->scriptSelect(DB_CONTRIBUTION_ID."=".$idx, false /*orderby*/, 1 /*limit*/);
		//------------------
		$qr = $db->Query($sql);
		if ($qr === false)
		{
			XLogError("Contributions::loadContributionRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getContribution($idx)
	{
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Contributions::getContribution - validate index failed");
			return false;
		}
		//------------------
		$qr = $this->loadContributionRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Contributions::getContribution - loadContributionRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Contributions::getContribution - index $idx not found.");
			return false;
		}
		//------------------
		return new Contribution($s);
	}
	//------------------
	function loadContributionsRaw($roundIdx = false, $includeSpecial = false, $where = false, $sort = false, $limit = false)
	{
		global $db, $dbContributionFields;
		//------------------
		if ($roundIdx !== false)
		{
			if ($where === false)
				$where = "";
			else
				$where .= " AND ";
			$where .= DB_CONTRIBUTION_ROUND."=$roundIdx"; // non-round entries will be -1, test will be -2
		}
		else if ($includeSpecial === false)
		{
			if ($where === false)
				$where = "";
			else
				$where .= " AND ";
			$where .= DB_CONTRIBUTION_ROUND.">=0"; // non-round entries will be -1, test will be -2
		}
		//------------------
		if ($sort === false)
			$sort = DB_CONTRIBUTION_ID;
		//------------------
		$dbContributionFields->SetValues();
		$sql = $dbContributionFields->scriptSelect($where,  $sort, $limit);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Contributions::loadContributionsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getContributions($roundIdx = false, $includeSpecial = false, $where = false, $sort = false, $limit = false)
	{
		//------------------
		$qr = $this->loadContributionsRaw($roundIdx, $includeSpecial, $where, $sort, $limit);
		//------------------
		if ($qr === false)
		{
			XLogError("Contributions::getContributions - loadContributionsRaw failed");
			return false;
		}
		//------------------
		$contList = array();
		while ($r = $qr->GetRowArray())
			$contList[] = new Contribution($r);
		//------------------
		return $contList;
	}
	//------------------
	function findRoundContributions($roundIdx, $orderValue = false, $specialMode = false)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_numeric($roundIdx))
		{
			XLogError("Contributions::findRoundContributions - validate roundIdx failed: ".XVarDump($roundIdx));
			return false;
		}
		//------------------
		$where = DB_CONTRIBUTION_ROUND."=$roundIdx";
		if ($orderValue !== false)
			$where .= " AND ".DB_CONTRIBUTION_ORDER."=$orderValue";
		if ($specialMode !== false)
			$where .= " AND ".DB_CONTRIBUTION_MODE.">=".CONT_MODE_SPECIAL;
		//------------------
		$dbContributionFields->SetValues();
		$sql = $dbContributionFields->scriptSelect($where, DB_CONTRIBUTION_ORDER.",".DB_CONTRIBUTION_ID /*orderby*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Contributions::findRoundContributions - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$contributions = array();
		while ($p = $qr->GetRowArray())
			$contributions[] = new Contribution($p);
		//------------------
		return $contributions;
	}
	//------------------
	function addContribution($roundIdx, $order, $name, $address, $mode, $flags, $value, $subvalue = 0.0, $countvalue = 0, $total = false, $count = false, $outcome = false, $returnID = false)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_numeric($roundIdx) || !is_numeric($order) || !is_numeric($mode) || !is_numeric($value) || !is_numeric($subvalue) || !is_numeric($countvalue)  || !is_numeric($flags) || ($total !== false && !is_numeric($total)) || ($count !== false && !is_numeric($count)) || ($outcome !== false && !is_numeric($outcome)) )
		{
			XLogError("Contributions::addContribution validate parameters failed: ridx ".XVarDump($roundIdx).", order ".XVarDump($order).", mode ".XVarDump($mode).", val ".XVarDump($value).", subval ".XVarDump($subvalue).", countvalue ".XVarDump($countvalue).", flags ".XVarDump($flags).", total ".XVarDump($total).", count ".XVarDump($count).", outcome ".XVarDump($outcome));
			return false;
		}
		//------------------
		$tsNow = time();
		//------------------
		$dbContributionFields->ClearValues();
		$dbContributionFields->SetValue(DB_CONTRIBUTION_DATE_CREATED, $tsNow);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_ROUND, $roundIdx);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_ORDER, $order);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_NAME, $name);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_ADDRESS, $address);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_MODE, $mode);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_FLAGS, $flags);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_VALUE, $value);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_SUBVALUE, $subvalue);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_COUNTVALUE, $countvalue);
		//------------------
		if ($total !== false)
			$dbContributionFields->SetValue(DB_CONTRIBUTION_TOTAL, $total);
		if ($count !== false)
			$dbContributionFields->SetValue(DB_CONTRIBUTION_COUNT, $count);
		if ($outcome !== false)
			$dbContributionFields->SetValue(DB_CONTRIBUTION_OUTCOME, $outcome);
		//------------------
		$sql = $dbContributionFields->scriptInsert();
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::addContribution - db Execute scriptInsert failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		if ($returnID === true)
		{
			//------------------
			$where = DB_CONTRIBUTION_DATE_CREATED."=$tsNow AND ".DB_CONTRIBUTION_ROUND."=$roundIdx";
			//------------------
			$dbContributionFields->ClearValues();
			$dbContributionFields->SetValue(DB_CONTRIBUTION_ID);
			//------------------
			$sql = $dbContributionFields->scriptSelect($where, DB_CONTRIBUTION_ID.' DESC' /*orderby*/, 1 /*limit*/);
			//------------------
			if (!($qr = $db->Query($sql)))
			{
				XLogError("Contributions::addContribution - db Query failed.\nsql: $sql");
				return false;
			}
			//------------------
			$value = $qr->GetOneValue();
			//------------------
			if ($value === false)
			{
				XLogWarn("Contributions::addContribution - new payout not found (created $tsNow round $roundIdx)");
				return false;
			}
			//------------------
			return (int)$value;
		}
		//------------------
		return true;
	}
	//------------------
	function deleteAllRound($ridx)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_numeric($ridx))
		{
			XLogError("Contributions::deleteAllRound validate parameters failed");
			return false;
		}
		//------------------
		$sql = $dbContributionFields->scriptDelete(DB_CONTRIBUTION_ROUND."=$ridx");
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::deleteAllRound db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function deleteRoundForfietContributions($ridx)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_numeric($ridx))
		{
			XLogError("Contributions::deleteRoundForfietContributions validate parameters failed");
			return false;
		}
		//------------------
		$sql = $dbContributionFields->scriptDelete(DB_CONTRIBUTION_ROUND."=$ridx AND ".DB_CONTRIBUTION_MODE."=".CONT_MODE_SPECIAL_STORE_FORFEIT);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Contributions::deleteRoundForfietContributions db Execute scriptDelete failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function resetRoundContributions($ridx, $includeSpecial = true)
	{
		global $db, $dbContributionFields;
		//------------------
		if (!is_numeric($ridx))
		{
			XLogError("Contributions::resetRoundContributions validate parameters failed");
			return false;
		}
		//------------------
		$dbContributionFields->ClearValues();
		$dbContributionFields->SetValue(DB_CONTRIBUTION_DATE_DONE, null);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_TXID, null);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_TOTAL, null);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_COUNT, null);
		$dbContributionFields->SetValue(DB_CONTRIBUTION_OUTCOME, CONT_OUTCOME_NONE);
		//------------------
		$where = DB_CONTRIBUTION_ROUND."=$ridx";
		if (!$includeSpecial)
			$where .= " AND ".DB_CONTRIBUTION_MODE."<".CONT_MODE_SPECIAL;
		//------------------
		$sql = $dbContributionFields->scriptUpdate($where);
		//------------------
		if ($db->Execute($sql) === false)
		{
			XLogError("Contributions::resetRoundContributions db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function getContributionTxidList()
	{
		global $db;
		//------------------
		$sql = "SELECT DISTINCT(".DB_CONTRIBUTION_TXID.") FROM ".DB_CONTRIBUTION." WHERE ".DB_CONTRIBUTION_TXID." IS NOT NULL AND ".DB_CONTRIBUTION_TXID."!=0 AND ".DB_CONTRIBUTION_TXID."!=''";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Contributions::getContributionTxidList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$txidList = array();
		while ($r = $qr->GetRow())
			if (isset($r[0]))
				$txidList[] = $r[0];
		//------------------
		return $txidList;
	}
	//------------------
	function getContributionStepData($ridx, $isTest = false, $peekOnly = false)
	{
		$Team = new Team() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$Wallet = new Wallet() or die("Create object failed");
		$Config = new Config() or die("Create object failed");
		//------------------
		$round = $Rounds->GetRound($ridx);
		if ($round === false)
		{
			XLogError("Contributions::getContributionStepData Rounds GetRound failed. ridx: ".XVarDump($ridx));
			return false;
		}
		//------------------
		$mainAddress = $Wallet->getMainAddress();
		//------------------
		$fullBalance = $Wallet->getBalance();
		if ($fullBalance === false)
		{
			XLogError("Contributions::getContributionStepData Wallet getBalance failed");
			return false;
		}
		//------------------
		$estFee = $Config->GetSetDefault(CFG_WALLET_EST_FEE, DEF_CFG_WALLET_EST_FEE);
		if ($estFee === false)
		{
			XLogError("Contributions::getContributionStepData Config GetSetDefault estfee failed");
			return false;
		}
		//------------------
		$balance = bcsub($fullBalance, $estFee, 8);
		//------------------		
		$estContFee = $Wallet->getEstContFee();
		if ($estContFee === false)
		{
			XLogError("Contributions::getContributionStepData Wallet getEstContFee failed");
			return false; 
		}
		//------------------
		$digits = $Config->GetSetDefault(CFG_ROUND_PAY_DIGITS, DEFAULT_ROUND_PAY_DIGITS);
		if ($digits === false)
		{
			XLogError("Contributions::getContributionStepData Config GetSetDefault pay digits failed");
			return false;
		}
		//------------------
		$payMinimum = $Config->GetSetDefault(CFG_ROUND_PAY_MINIMUM, DEFAULT_ROUND_PAY_MINIMUM);
		if ($payMinimum === false)
		{
			XLogError("Contributions::getContributionStepData Config GetSetDefault pay minimum failed");
			return false;
		}
		//------------------
		$storeMinimum = $Config->GetSetDefault(CFG_ROUND_STORE_MINIMUM, DEFAULT_ROUND_STORE_MINIMUM);
		if ($storeMinimum === false)
		{
			XLogError("Contributions::getContributionStepData Config GetSetDefault store minimum failed");
			return false;
		}
		//------------------
		$storeEnabled = $round->hasFlag(ROUND_FLAG_STORE_VALUES);
		$forfeitEnabled = $round->hasFlag(ROUND_FLAG_FORFEIT_STORE);
		$isDryRun = $round->hasFlag(ROUND_FLAG_DRYRUN);
		$isTestBalance  = ($round->hasFlag(ROUND_FLAG_TEST_BALANCE) ? false : $round->totalPay);
		//------------------
		if ($peekOnly) // don't set when they don't exist
		{
			$balanceUsed = $Config->Get(CONT_STEPDATA_CFGBASE_BALANCE_USED.$ridx, '');
			$balanceAdded = $Config->Get(CONT_STEPDATA_CFGBASE_BALANCE_ADDED.$ridx, '');
			$balanceSideUse = $Config->Get(CONT_STEPDATA_CFGBASE_BALANCE_SIDEUSE.$ridx, '');
			$balanceOrigFull = $Config->Get(CONT_STEPDATA_CFGBASE_BALANCE_ORIGINAL.$ridx, $fullBalance);
		}
		else
		{
			$balanceUsed = $Config->GetSetDefault(CONT_STEPDATA_CFGBASE_BALANCE_USED.$ridx, 0.0);
			$balanceAdded = $Config->GetSetDefault(CONT_STEPDATA_CFGBASE_BALANCE_ADDED.$ridx, 0.0);
			$balanceSideUse = $Config->GetSetDefault(CONT_STEPDATA_CFGBASE_BALANCE_SIDEUSE.$ridx, 0.0);
			$balanceOrigFull = $Config->GetSetDefault(CONT_STEPDATA_CFGBASE_BALANCE_ORIGINAL.$ridx, $fullBalance);
		}
		//------------------
		if ($balanceUsed === false || $balanceAdded === false)
		{
			XLogError("Contributions::getContributionStepData Config GetSetDefault of balanceUsed or balanceAdded failed");
			return false;
		}
		//------------------
		$balanceOrig = bcsub($balanceOrigFull, $estFee, 8);
		$rewardMinimum = ($storeEnabled ? $storeMinimum : $payMinimum);
		//------------------
		return array('ridx'=>$ridx, 'roundFlags'=>$round->flags, 'digits'=>$digits, 'fullOriginalBalance'=>$balanceOrigFull, 'originalBalance'=>$balanceOrig, 'fullBalance'=>$fullBalance, 'balance'=>$balance, 'balanceUsed'=>$balanceUsed, 'balanceAdded'=>$balanceAdded, 'balanceSideUse'=>$balanceSideUse, 'testRun'=>$isTest, 'dryRun'=>$isDryRun, 'testBalance'=>$isTestBalance, 'mainAddress'=>$mainAddress, 'estFee'=>$estFee, 'estContFee'=>$estContFee, 'payMinimum'=>$payMinimum, 'storeMinimum'=>$storeMinimum, 'rewardMinimum'=>$rewardMinimum , 'storeEnabled'=>$storeEnabled, 'forfeitEnabled'=>$forfeitEnabled);
	}
	//------------------
	function requestContributions($ridx)
	{
		$Wallet = new Wallet() or die("Create object failed");
		//------------------
		$mainAddress = $Wallet->getMainAddress();
		//------------------
		$contribs = $this->findRoundContributions(-1 /*(new) ridx*/,  false /*specific number*/, false /*only specialMode*/);
		if ($contribs === false)
		{
			XLogError("Contributions::requestContributions findRoundContributions failed");
			return false;
		}
		//------------------
		if (!$this->cleanupContributionsRoundStart())
		{
			XLogError("Contributions::requestContributions cleanupContributionsRoundStart failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::requestContributions found ".sizeof($contribs)." contributions");
		foreach ($contribs as $cont)
			if (!$cont->isDisabled())
			{
				//------------------
				$flags = $cont->flags;
				//------------------
				if ($cont->address == $mainAddress)
					$flags |= CONT_FLAG_MAIN_CONTRIBUTION;
				//------------------
				if (!$this->addContribution($ridx, $cont->order, $cont->name, $cont->address, $cont->mode, $flags, $cont->value, $cont->subvalue, $cont->countvalue))
				{
					XLogError("Contributions::requestContributions addContribution number $cont->order failed");
					return false; 
				}
				//------------------
				XLogDebug("Contributions::requestContributions adding contribution $cont->name, $cont->order, $cont->mode, $cont->address, $cont->value, to ridx $ridx");
				//------------------
			}
		//------------------
		XLogDebug("Contributions::requestContributions done");
		return true;
	}
	//------------------
	function stepContributions($ridx, $isTest = false, $maxStepTimeMs = false)
	{
		$Config = new Config() or die("Create object failed");
		$timer = new XTimer();
		$fullTimer = new XTimer();
		//------------------
		$stepData = $this->getContributionStepData($ridx, $isTest);
		if ($stepData === false)
		{
			XLogError("Contributions::stepContributions getContributionStepData failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::stepContributions found getContributionStepData took ".$timer->restartMs(true));
		//------------------
		$contribs = $this->findRoundContributions($ridx,  false /*specific number*/, false /*only specialMode*/);
		if ($contribs === false)
		{
			XLogError("Contributions::stepContributions findRoundContributions failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::stepContributions found ".sizeof($contribs)." contributions for ridx $ridx in ".$timer->restartMs(true));
		//------------------
		$countStepped = 0;
		$done = true;
		foreach ($contribs as $cont)
			if (!$cont->isDone())
			{
				$done = false;
				XLogDebug("Contributions::stepContributions stepping $cont->name ...");
				//XLogDebug("Contributions::stepContributions using stepData:\n".XVarDump($stepData));
				if (!$cont->step($stepData))
				{
					XLogError("Contributions::stepContributions contribution number $cont->order step failed");
					//------------------
					if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_USED.$ridx, $stepData['balanceUsed']))
					{
						XLogError("Contributions::stepContributions Config Set balanceUsed failed");
						return false; 
					}
					//------------------
					if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_ADDED.$ridx, $stepData['balanceAdded']))
					{
						XLogError("Contributions::stepContributions Config Set balanceAdded failed");
						return false; 
					}
					//------------------
					if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_SIDEUSE.$ridx, $stepData['balanceSideUse']))
					{
						XLogError("Contributions::stepContributions Config Set balanceSideUse failed");
						return false; 
					}
					//------------------
					return false; 
				}
				//------------------
				if ($cont->isDone())				
					$countStepped++;
				else if ($cont->hasFlag(CONT_FLAG_WAIT_FOR_FUNDS))
				{
					XLogDebug("Contributions::stepContributions ($cont->id)'$cont->name' stepped, but is not done and is flagged wait for funds. Waiting for funds or resolved non completion that didn't fail. ");
					break;
				}
				//------------------
				if ($maxStepTimeMs !== false && $fullTimer->elapsedMs() > $maxStepTimeMs)
				{
					XLogDebug("Contributions::stepContributions pass timed out after elapsed ".$fullTimer->elapsedMs(true));
					break;
				}
				//------------------
			} //foreach (...) if (!$cont->isDone())
		//------------------
		if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_USED.$ridx, $stepData['balanceUsed']))
		{
			XLogError("Contributions::stepContributions Config Set balanceUsed failed");
			return false; 
		}
		//------------------
		if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_ADDED.$ridx, $stepData['balanceAdded']))
		{
			XLogError("Contributions::stepContributions Config Set balanceAdded failed");
			return false; 
		}
		//------------------
		if (!$Config->Set(CONT_STEPDATA_CFGBASE_BALANCE_SIDEUSE.$ridx, $stepData['balanceSideUse']))
		{
			XLogError("Contributions::stepContributions Config Set balanceSideUse failed");
			return false; 
		}
		//------------------
		XLogDebug("Contributions::stepContributions done, stepped $countStepped contributions, with result done: ".XVarDump($done).", took ".$timer->restartMs(true));
		//------------------
		return ($done ? true : array($countStepped, sizeof($contribs)) /*not done*/);
	}
	//------------------
	function cleanupContributionsRoundStart()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		if (!$Config->DeleteLike(CONT_STEPDATA_CFGBASE_BALANCE_BASE))
		{
			XLogError("Contributions::cleanupContributionsRoundStart Config Delete balances failed.");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function cleanupRoundTestContributions($ridx)
	{
		$Config = new Config() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		if (!is_numeric($ridx))
		{
			XLogError("Contributions::cleanupRoundTestContributions validate parameters failed. ridx ".XVarDump($ridx));
			return false;
		}
		//------------------
		$round = $Rounds->getRound($ridx);
		if ($round === false)
		{
			XLogError("Contributions::cleanupRoundTestContributions Rounds getRound failed. ridx ".XVarDump($ridx));
			return false;
		}
		//------------------
		$timer->start();
		//------------------
		if (!$this->deleteAllRound($ridx))
		{
			XLogError("Contributions::cleanupRoundTestContributions deleteAllRound (contributions) failed");
			return false;
		}
		//------------------
		/*
		if (!$this->deleteRoundForfietContributions($ridx))
		{
			XLogError("Contributions::cleanupRoundTestContributions deleteRoundForfietContributions failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::cleanupRoundTestContributions deleteRoundForfietContributions payouts took ".$timer->restartMs(true));
		//------------------
		if (!$this->resetRoundContributions($ridx))
		{
			XLogError("Contributions::cleanupRoundTestContributions resetRoundContributions failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::cleanupRoundTestContributions resetAllRoundContributions took ".$timer->restartMs(true));
		*/
		//------------------
		XLogDebug("Contributions::cleanupRoundTestContributions deleteAllRound (contributions) took ".$timer->restartMs(true));
		//------------------
		if (!$Payouts->deleteAllRound($ridx))
		{
			XLogError("Contributions::cleanupRoundTestContributions Payouts deleteAllRound failed");
			return false;
		}
		//------------------
		XLogDebug("Contributions::cleanupRoundTestContributions Payouts deleteAllRound took ".$timer->restartMs(true));
		//------------------
		if (!$Config->DeleteLike(CONT_STEPDATA_CFGBASE_BALANCE_BASE))
		{
			XLogError("Contributions::cleanupRoundTestContributions Config DeleteLike balances failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
} // class Contributions
//---------------
?>
