<?php
/*
 *	www/include/NewRoundData.php
 * 
*/
//---------------
define('NEWDATA_WORKERSTATUS_NEW', 				0);
define('NEWDATA_WORKERSTATUS_EXISTING', 		1);
define('NEWDATA_WORKERSTATUS_EXISTNEW', 		2);
define('NEWDATA_WORKERSTATUS_NOT_VALIDATED', 	3);
define('NEWDATA_WORKERSTATUS_VALID', 			4);
define('NEWDATA_WORKERSTATUS_INVALID', 			5);
define('NEWDATA_WORKERSTATUS_VALIDNEW', 		6);
define('NEWDATA_WORKERSTATUS_INVALIDNEW', 		7);
//---------------------------
define('NEWDATA_POINTSTATUS_NEW', 		0);
define('NEWDATA_POINTSTATUS_NEEDDEEP', 	1);
define('NEWDATA_POINTSTATUS_GOTWEEK', 	2);
define('NEWDATA_POINTSTATUS_HASSTAT', 	3);
define('NEWDATA_POINTSTATUS_SKIPPED', 	4);
//---------------------------
class NewRoundData
{
	var $minIndex = false;
	var $maxIndex = false;
	function Install()
	{
		global $db, $dbNewDataFields;
		//------------------------------------
		$sql = $dbNewDataFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbNewDataFields;
		//------------------------------------
		$sql = $dbNewDataFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		// temporary table, no data kept
		//------------------------------------
		return true;
	}
	//------------------
	function Clear()
	{
		global $db, $dbNewDataFields;
		//---------------------------------
		$sql = $dbNewDataFields->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::Clear db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function addDataEntry($workerID, $workerName, $workerStatus, $wus, $points, $pointsStatus)
	{
		global $db, $dbNewDataFields;
		//------------------
		$dbNewDataFields->ClearValues();
		if ($workerID !== false)
			$dbNewDataFields->SetValuePrepared(DB_NEWDATA_WORKER_ID, 	$workerID);
		if ($workerName !== false)
			$dbNewDataFields->SetValuePrepared(DB_NEWDATA_WORKER_NAME, 	$workerName);
		$dbNewDataFields->SetValuePrepared(DB_NEWDATA_WORKER_STATUS, 	$workerStatus);
		$dbNewDataFields->SetValuePrepared(DB_NEWDATA_WUS, 				$wus);
		$dbNewDataFields->SetValuePrepared(DB_NEWDATA_POINTS, 			$points);
		$dbNewDataFields->SetValuePrepared(DB_NEWDATA_POINTS_STATUS, 	$pointsStatus);
		//------------------
		$sql = $dbNewDataFields->scriptInsert();
		//------------------
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("NewRoundData::addDataEntry db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbNewDataFields->BindValues($dbs))
		{
			XLogError("NewRoundData::addDataEntry BindValues failed");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("NewRoundData::addDataEntry dbs execute scriptInsert (prepared) failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;		
	}
	//---------------------------------	
	function getDuplicateWorkers()
	{
		global $db, $dbNewDataFields;
		//------------------
		$dups = array(); // [id](wid,wname,idCnt,nameCnt)
		//------------------
		$sql = 'SELECT a.'.DB_NEWDATA_ID.',a.'.DB_NEWDATA_WORKER_ID.',a.'.DB_NEWDATA_WORKER_NAME.',b.cnt FROM '.DB_NEWDATA.' a';
		$sql .= ' INNER JOIN ( SELECT '.DB_NEWDATA_WORKER_ID.',COUNT(*) AS cnt FROM '.DB_NEWDATA.' GROUP BY '.DB_NEWDATA_WORKER_ID.') b ON a.'.DB_NEWDATA_WORKER_ID.'=b.'.DB_NEWDATA_WORKER_ID;
		$sql .= ' WHERE b.cnt>1 AND b.'.DB_NEWDATA_WORKER_ID.' IS NOT NULL';
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getDuplicateWorkers db Query (id) failed.\nsql: $sql");
			return false;
		}
		//------------------
		while (($r = $qr->GetRow()))
			if (sizeof($r) == 4 && is_numeric($r[0]))
				$dups[$r[0]] = array($r[1], $r[2], $r[3], 0);
		//------------------
		$sql = 'SELECT a.'.DB_NEWDATA_ID.',a.'.DB_NEWDATA_WORKER_ID.',a.'.DB_NEWDATA_WORKER_NAME.',b.cnt FROM '.DB_NEWDATA.' a';
		$sql .= ' INNER JOIN ( SELECT '.DB_NEWDATA_WORKER_NAME.',COUNT(*) AS cnt FROM '.DB_NEWDATA.' GROUP BY '.DB_NEWDATA_WORKER_NAME.') b ON a.'.DB_NEWDATA_WORKER_NAME.'=b.'.DB_NEWDATA_WORKER_NAME;
		$sql .= ' WHERE b.cnt>1 AND b.'.DB_NEWDATA_WORKER_NAME.' IS NOT NULL';
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getDuplicateWorkers db Query (name) failed.\nsql: $sql");
			return false;
		}
		//------------------
		while (($r = $qr->GetRow()))
			if (sizeof($r) == 4 && is_numeric($r[0]))
				if (isset($dups[$r[0]]))
					$dups[$r[0]][3] = $r[3];
				else
					$dups[$r[0]] = array($r[1], $r[2], 0, $r[3]);
		//------------------
		return $dups; // [id](wid,wname,idCnt,nameCnt)	
	}
	//---------------------------------	
	function updateWorkerStatus($workerID, $workerStatus)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (!is_numeric($workerID) || !is_numeric($workerStatus))
		{
			XLogError("NewRoundData::updateWorkerStatus validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_STATUS, $workerStatus);		
		//------------------
		$sql = $dbNewDataFields->scriptUpdate(DB_NEWDATA_WORKER_ID."=".$workerID);
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::updateWorkerStatus - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function updateNewWorker($id, $workerID, $workerStatus)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (!is_numeric($id) || !is_numeric($workerID) || !is_numeric($workerStatus))
		{
			XLogError("NewRoundData::updateNewWorker validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_ID, $workerID);		
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_STATUS, $workerStatus);		
		//------------------
		$sql = $dbNewDataFields->scriptUpdate(DB_NEWDATA_ID."=".$id);
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::updateWorkerStatus - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;		
	}
	//---------------------------------	
	function updatePointsStatus($workerID, $pointsStatus, $weekPoints = false)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (!is_numeric($workerID) || !is_numeric($pointsStatus) || ($weekPoints !== false && !is_numeric($weekPoints)))
		{
			XLogError("NewRoundData::updatePointsStatus validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS_STATUS, $pointsStatus);
		if ($weekPoints !== false)
			$dbNewDataFields->SetValue(DB_NEWDATA_WEEK_POINTS, $weekPoints);		
		//------------------
		$sql = $dbNewDataFields->scriptUpdate(DB_NEWDATA_WORKER_ID."=".$workerID);
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::updatePointsStatus - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function updateStatuses($workerID, $workerStatus, $pointsStatus = false, $weekPoints = false)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (!is_numeric($workerID) || !is_numeric($workerStatus) || ($pointsStatus !== false && !is_numeric($pointsStatus)) || ($weekPoints !== false && !is_numeric($weekPoints)))
		{
			XLogError("NewRoundData::updateStatuses validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_STATUS, $workerStatus);		
		if ($pointsStatus !== false)
			$dbNewDataFields->SetValue(DB_NEWDATA_POINTS_STATUS, $pointsStatus);
		if ($weekPoints !== false)
			$dbNewDataFields->SetValue(DB_NEWDATA_WEEK_POINTS, $weekPoints);		
		//------------------
		$sql = $dbNewDataFields->scriptUpdate(DB_NEWDATA_WORKER_ID."=$workerID");
		if (!$db->Execute($sql))
		{
			XLogError("NewRoundData::updateStatuses - db Execute scriptUpdate failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function getEntryCount()
	{
		//---------------------------------
		$count = $this->getWorkerStatusCount(false /*maxWorkerStatus*/);
		if ($count === false)
		{
			XLogError("NewRoundData::getEntryCount getWorkerStatusCount failed");
			return false;
		}
		//---------------------------------
		return $count;
	}
	//---------------------------------	
	function getWorkerStatusCount($maxWorkerStatus)
	{
		global $db, $dbNewDataFields;
		//------------------
		if ($maxWorkerStatus !== false && !is_numeric($maxWorkerStatus))
		{
			XLogError("NewRoundData::getWorkerStatusCount validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$sql = "SELECT COUNT(".DB_NEWDATA_ID.") FROM ".DB_NEWDATA;
		//------------------
		if ($maxWorkerStatus !== false)
			$sql .= " WHERE ".DB_NEWDATA_WORKER_STATUS." <= ".$maxWorkerStatus;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getWorkerStatusCount - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue();
		if ($value === false)
		{
			XLogError("NewRoundData::getWorkerStatusCount db query result GetOneValue failed");
			return false;
		}
		//------------------
		return (int)$value;
	}
	//---------------------------------	
	function getWorkerStatus($maxWorkerStatus, $count)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (($maxWorkerStatus !== false && !is_numeric($maxWorkerStatus)) || ($count !== false && !is_numeric($count)))
		{
			XLogError("NewRoundData::getWorkerStatus validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_NAME);
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_STATUS);
		//------------------
		if ($maxWorkerStatus === false)
			$where = false;
		else
			$where = DB_NEWDATA_WORKER_STATUS." <= ".$maxWorkerStatus;
		//------------------
		$sql = $dbNewDataFields->scriptSelect($where,  DB_NEWDATA_ID/*orderby*/, $count /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getWorkerStatus - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ($row = $qr->GetRowArray())
		{
			//------------------
			if (!isset($row[DB_NEWDATA_ID]) || !isset($row[DB_NEWDATA_WORKER_STATUS]))
			{
				XLogError("NewRoundData::getWorkerStatus - db row is missing required values: ".XVarDump($row));
				return false;
			}
			//------------------
			$rData[] = array(
								"id" => $row[DB_NEWDATA_ID],
								"wid" => (isset($row[DB_NEWDATA_WORKER_ID]) ? $row[DB_NEWDATA_WORKER_ID] : false),
								"wname" => (isset($row[DB_NEWDATA_WORKER_NAME]) ? $row[DB_NEWDATA_WORKER_NAME] : false),
								"status" => $row[DB_NEWDATA_WORKER_STATUS]
							);
			//------------------
		}
		//---------------------------------
		return $rData;
	}
	//---------------------------------	
	function getPointsStatusCount($maxPointsStatus)
	{
		global $db, $dbNewDataFields;
		//------------------
		if ($maxPointsStatus !== false && !is_numeric($maxPointsStatus))
		{
			XLogError("NewRoundData::getPointsStatusCount validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$sql = "SELECT COUNT(".DB_NEWDATA_ID.") FROM ".DB_NEWDATA;
		//------------------
		if ($maxPointsStatus !== false)
			$sql .= " WHERE ".DB_NEWDATA_POINTS_STATUS." <= ".$maxPointsStatus;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getPointsStatusCount - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$value = $qr->GetOneValue(); // returns one row, numerically indexed field array
		if ($value === false)
		{
			XLogError("NewRoundData::getPointsStatusCount db query result GetOneValue failed");
			return false;
		}
		//------------------
		return (int)$value;
	}
	//---------------------------------	
	function getPointsStatus($maxPointsStatus, $count)
	{
		global $db, $dbNewDataFields;
		//------------------
		if (($maxPointsStatus !== false && !is_numeric($maxPointsStatus)) || ($count !== false && !is_numeric($count)))
		{
			XLogError("NewRoundData::getPointsStatus validate is_numeric parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WUS);
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS);
		$dbNewDataFields->SetValue(DB_NEWDATA_WEEK_POINTS);
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS_STATUS);
		//------------------
		if ($maxPointsStatus === false)
			$where = false;
		else
			$where = DB_NEWDATA_POINTS_STATUS." <= ".$maxPointsStatus;
		//------------------
		$sql = $dbNewDataFields->scriptSelect($where, DB_NEWDATA_ID /*orderby*/, $count /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getPointsStatus - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$rData = array();
		while ($row = $qr->GetRowArray())
		{
			$rData[] = array(
								"id" => (isset($row[DB_NEWDATA_ID]) ? $row[DB_NEWDATA_ID] : false),
								"wid" => (isset($row[DB_NEWDATA_WORKER_ID]) ? $row[DB_NEWDATA_WORKER_ID] : false),
								"wus" => (isset($row[DB_NEWDATA_WUS]) ? $row[DB_NEWDATA_WUS] : false),
								"points" => (isset($row[DB_NEWDATA_POINTS]) ? $row[DB_NEWDATA_POINTS] : false),
								"week" => (isset($row[DB_NEWDATA_WEEK_POINTS]) ? $row[DB_NEWDATA_WEEK_POINTS] : false),
								"status" => (isset($row[DB_NEWDATA_POINTS_STATUS]) ? $row[DB_NEWDATA_POINTS_STATUS] : false)
							);
		}
		//---------------------------------
		return $rData;
	}
	//---------------------------------	
	function addStatsWithWeek($rIdx, $limit = false)
	{
		global $db, $dbNewDataFields;
		//------------------
		$Stats = new Stats() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		if (!is_numeric($rIdx) || ($limit !== false && !is_numeric($limit)))
		{
			XLogError("NewRoundData::addStatsWithWeek validate parameters failed");
			return false;
		}
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WORKER_ID);
		$dbNewDataFields->SetValue(DB_NEWDATA_WUS);
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS);
		$dbNewDataFields->SetValue(DB_NEWDATA_WEEK_POINTS);
		//------------------
		$where = DB_NEWDATA_POINTS_STATUS."<=".NEWDATA_POINTSTATUS_GOTWEEK;
		//------------------
		$sql = $dbNewDataFields->scriptSelect($where, DB_NEWDATA_ID /*orderby*/, $limit /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::addStatsWithWeek - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError("NewRoundData::addStatsWithWeek db beginTransaction failed");
			return false;
		}
		//------------------
		$tsNow = time();
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS_STATUS, NEWDATA_POINTSTATUS_HASSTAT);
		//------------------
		$count = 0;
		while ($row = $qr->GetRowArray())
		{
			//------------------
			$id = $row[DB_NEWDATA_ID];
			$wid = $row[DB_NEWDATA_WORKER_ID];
			$week = $row[DB_NEWDATA_WEEK_POINTS];
			$points = $row[DB_NEWDATA_POINTS];
			$wus = $row[DB_NEWDATA_WUS];
			//------------------
			if (!is_numeric($id) || !is_numeric($wid) || !is_numeric($week) || !is_numeric($points) || !is_numeric($wus))
			{
				XLogError("NewRoundData::addStatsWithWeek validate newdata row fields failed: ".XVarDump($row));
				return false;
			}
			//------------------
			if (!$Stats->addCompleteStat($rIdx, $wid, $week, $points, $wus, $tsNow /*dtCreated*/))
			{
				XLogError("NewRoundData::addStatsWithWeek Stats addCompleteStat failed");
				return false;
			}
			//------------------
			$sql = $dbNewDataFields->scriptUpdate(DB_NEWDATA_ID."=".$id);
			if (!$db->Execute($sql))
			{
				XLogError("NewRoundData::addStatsWithWeek db Execute scriptUpdate point status failed.\nsql: $sql");
				return false;
			}
			//---------------------------------
			$count++;
			//------------------
		} // while row
		//---------------------------------
		if (!$db->commit())
		{
			XLogError("NewRoundData::addStatsWithWeek db commit failed");
			return false;
		}
		//------------------
		XLogDebug("FahAppClient::processAddStats add $count stats and update new round status took ".$timer->restartMs(true));
		//------------------
		return $count;
	}
	//---------------------------------	
	function getWeekPointsTotal()
	{
		global $db, $dbNewDataFields;
		//------------------
		$sql = "SELECT COUNT(".DB_NEWDATA_ID."), SUM(".DB_NEWDATA_WEEK_POINTS.") FROM ".DB_NEWDATA." WHERE ".DB_NEWDATA_POINTS_STATUS."=".NEWDATA_POINTSTATUS_GOTWEEK." OR ".DB_NEWDATA_POINTS_STATUS."=".NEWDATA_POINTSTATUS_HASSTAT;
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("NewRoundData::getWeekPointsTotal db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$row = $qr->GetRow();
		if ($row === false)
		{
			XLogError("NewRoundData::getWeekPointsTotal db Query result GetRow failed.");
			return false;
		}
		//------------------
		if (!is_array($row) || sizeof($row) != 2 || !is_numeric($row[0]) || !is_numeric($row[1]))
		{
			XLogError("NewRoundData::getWeekPointsTotal validate result failed: ".XVarDump($row));
			return false;
		}
		//------------------
		return $row;
	}
	//---------------------------------	
	function unaddStats()
	{
		global $db, $dbNewDataFields;
		//------------------
		$dbNewDataFields->ClearValues();
		$dbNewDataFields->SetValue(DB_NEWDATA_POINTS_STATUS, NEWDATA_POINTSTATUS_GOTWEEK);
		//------------------
		$where = DB_NEWDATA_POINTS_STATUS.'='.NEWDATA_POINTSTATUS_HASSTAT;
		//------------------
		$sql = $dbNewDataFields->scriptUpdate($where);
		//------------------
		$qr = $db->Query($sql);
		if ($qr === false)
		{
			XLogError("Stats::unaddStats db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr->RowCount();
	}
	//---------------------------------	
}; // class NewRoundData
//---------------------------
?>
