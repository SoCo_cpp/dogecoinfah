<?php
/*
 *	www/include/Monitor.php
 * 
 * 
* 
*/
//---------------
define('MONITOR_ERROR_LOG_FILE', 'log/error_monitor.log'); // relative to admin dir
//---------------
define('CFG_MONITOR_CUR_ROUND', 'monitor_ridx');
define('CFG_MONITOR_STEP_START', 'monitor_step_start');
define('CFG_MONITOR_STEP_BEGIN', 'monitor_step_begin');
define('CFG_MONITOR_STEP_STOP', 'monitor_step_stop');
define('CFG_MONITOR_STEP_FAILS', 'monitor_step_fails');
define('CFG_MONITOR_STEP_COUNT', 'monitor_step_count');
define('CFG_MONITOR_STEP', 'monitor_step');
define('CFG_MONITOR_ACTION', 'monitor_action');
//---------------
define('MONEVENT_TYPE_NONE',	0);
define('MONEVENT_TYPE_NOTIFY',	1);
define('MONEVENT_TYPE_WARN',	2);
define('MONEVENT_TYPE_ALARM',	3);
define('MONEVENT_TYPE_BLOCK',	4);
//---------------
define('MONEVENT_FLAG_NONE',		0);
define('MONEVENT_FLAG_RESOLVED',	1);
define('MONEVENT_FLAG_SELF_FIX',	2);
define('MONEVENT_FLAG_USER_FIX',	4);
define('MONEVENT_FLAG_USER_BYPASS',	8);
//---------------
define('MONEVENT_ISSUE_NONE',					0);
define('MONEVENT_ISSUE_MONITOR_FAIL',			10);
define('MONEVENT_ISSUE_PHP_ERROR',				11);

define('MONEVENT_ISSUE_ROUND_FAIL',				100);
define('MONEVENT_ISSUE_ROUND_CHANGED',			101);
define('MONEVENT_ISSUE_ROUND_NEW',				102);
define('MONEVENT_ISSUE_ROUND_START',			103);
define('MONEVENT_ISSUE_ROUND_DONE',				104);
define('MONEVENT_ISSUE_ROUND_LOCKED',			110);

define('MONEVENT_ISSUE_ROUND_FAHSTAT_LOCKED',	150);

define('MONEVENT_ISSUE_TRANSACTION_FAILED',		201);
define('MONEVENT_ISSUE_CONT_WAIT_FUNDS',		251);

define('MONEVENT_ISSUE_ROUND_INVALID',			310);
define('MONEVENT_ISSUE_ROUND_DATA_INVALID',		311);
define('MONEVENT_ISSUE_ROUND_DATA_SANITY',		312);
define('MONEVENT_ISSUE_ROUND_DATA_INFO',		313);

define('MONEVENT_ISSUE_ROUNDSTEP_FAIL',			410);
define('MONEVENT_ISSUE_ROUNDSTEP_COMPLETED',	411);
define('MONEVENT_ISSUE_ROUNDSTEP_INCOMPLETE',	412);
define('MONEVENT_ISSUE_ROUNDSTEP_TOO_LONG',		413);
define('MONEVENT_ISSUE_ROUNDSTEP_ORDER',		414);
//---------------
define('MONEVENT_DETAIL_NONE', 					0);
define('MONEVENT_DETAIL_NOTE', 					 1);  // x1
define('MONEVENT_DETAIL_SOURCE', 				10);  // xA
define('MONEVENT_DETAIL_FAIL', 					11);  // xB
define('MONEVENT_DETAIL_VALUE', 				12);  // xC
define('MONEVENT_DETAIL_VALUE2', 				13);  // xD
define('MONEVENT_DETAIL_VALUE_MIN',				14);  // xE
define('MONEVENT_DETAIL_VALUE_MAX',				15);  // xF
define('MONEVENT_DETAIL_NEW_VALUE', 			16);  // x10
define('MONEVENT_DETAIL_COUNT', 				17);  // x11
define('MONEVENT_DETAIL_FIELD',					18);  // x12
define('MONEVENT_DETAIL_BALANCE', 				19);  // x13
define('MONEVENT_DETAIL_BALANCE_LESS_FEE',		20);  // x14
define('MONEVENT_DETAIL_CUR_BALANCE',			21);  // x15
define('MONEVENT_DETAIL_CUR_BALANCE_LESS_FEE',	22);  // x16
define('MONEVENT_DETAIL_FEE', 					23);  // x17
define('MONEVENT_DETAIL_EST_FEE', 				24);  // x19
define('MONEVENT_DETAIL_ADDRESS_SRC',			25);  // x1A
define('MONEVENT_DETAIL_ADDRESS_DEST',			26);  // x1B

define('MONEVENT_DETAIL_ELAPSED', 				31);  // x1F
define('MONEVENT_DETAIL_ELAPSED_FULL', 			32);  // x20
define('MONEVENT_DETAIL_ELAPSED_MIN', 			33);  // x21
define('MONEVENT_DETAIL_ELAPSED_MAX', 			34);  // x22
define('MONEVENT_DETAIL_LOGIN_USER', 			50);  // x32
define('MONEVENT_DETAIL_RESOLVE_NOTE', 			51);  // x33
define('MONEVENT_DETAIL_ROUND_FLAGS', 			101); // x65
define('MONEVENT_DETAIL_ROUND_STEP', 			102); // x66
define('MONEVENT_DETAIL_ROUND_ACTION', 			103); // x67
define('MONEVENT_DETAIL_ROUND_OLD_STEP', 		104); // x68
define('MONEVENT_DETAIL_ROUND_OLD_ACTION', 		105); // x69
define('MONEVENT_DETAIL_ROUND_PROG', 			120); // x78
define('MONEVENT_DETAIL_ROUND_PROG_MAX', 		121); // x79
define('MONEVENT_DETAIL_ROUND_PROG_COUNT', 		122); // x7A
define('MONEVENT_DETAIL_ROUND_STEP_COUNT', 		140); // x8C
define('MONEVENT_DETAIL_ROUND_STEP_PASSES', 	141); // x8D
//---------------
class MonitorEvent
{
	var $id = -1;
	var $ridx = -1;
	var $date = 0;
	var $endDate = false;
	var $type = MONEVENT_TYPE_NONE;
	var $flags = MONEVENT_FLAG_NONE;
	var $issue = MONEVENT_ISSUE_NONE;
	var $details = array();
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		$Monitor = new Monitor() or die ('Create object failed');
		//------------------
		$this->id = $row[DB_MONEVENT_ID];
		$this->ridx = XArray($row, DB_MONEVENT_ROUND, -1);
		$this->date = XArray($row, DB_MONEVENT_DATE, 0);
		$this->endDate = XArray($row, DB_MONEVENT_END_DATE, false);
		$this->type = XArray($row, DB_MONEVENT_TYPE, MONEVENT_TYPE_NONE);
		$this->flags = XArray($row, DB_MONEVENT_FLAGS, MONEVENT_FLAG_NONE);
		$this->issue = XArray($row, DB_MONEVENT_ISSUE, MONEVENT_ISSUE_NONE);
		$this->details = $Monitor->unpackDetails(XArray($row, DB_MONEVENT_DETAILS, false));
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbMonEventFields;
		//------------------
		$Monitor = new Monitor() or die ('Create object failed');
		//------------------
		$dbMonEventFields->ClearValues();
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_ROUND,		$this->ridx);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_DATE,		$this->date);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_END_DATE,	XFalse2Null($this->endDate));
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_TYPE,		$this->type);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_FLAGS,		$this->flags);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_ISSUE,		$this->issue);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_DETAILS,	$Monitor->packDetails($this->details));
		//------------------
		$sql = $dbMonEventFields->scriptUpdate(DB_MONEVENT_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("MonitorEvent::Update db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbMonEventFields->BindValues($dbs))
		{
			XLogError("MonitorEvent::Update BindValues failed");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("MonitorEvent::Update dbs execute scriptUpdate (prepared) failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function updateValue($field, $value, $useFalse2Null = true)
	{
		return $this->updateValues( array( $field => $value ), $useFalse2Null);
	}
	//------------------
	function updateValues($fieldValueArray, $useFalse2Null = true)
	{
		global $db, $dbMonEventFields;
		//------------------
		if (!is_array($fieldValueArray) || sizeof($fieldValueArray) == 0)
		{
			XLogError("MonitorEvent::updateValues validate parameter failed: ".XVarDump($fieldValueArray));
			return false;
		}
		//------------------
		$dbMonEventFields->ClearValues();
		foreach ($fieldValueArray as $field => $value)
			$dbMonEventFields->SetValuePrepared($field, ($useFalse2Null === true ? XFalse2Null($value) : $value));
		//------------------
		$sql = $dbMonEventFields->scriptUpdate(DB_MONEVENT_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("MonitorEvent::updateValues db Prepare failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//---------------------------------
		if (!$dbMonEventFields->BindValues($dbs))
		{
			XLogError("MonitorEvent::updateValues BindValues failed. fieldValueArray: ".XVarDump($fieldValueArray));
			return false;
		}
		//---------------------------------
		if (!$dbs->execute())
		{
			XLogError("MonitorEvent::updateValues db prepared statement Execute failed. fieldValueArray: ".XVarDump($fieldValueArray).".\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function hasFlag($flagValue)
	{
		//------------------
		return XMaskContains($this->flags, $flagValue);
	}
	//------------------
	function setFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("MonitorEvent::setFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		$this->flags |= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function clearFlag($flagValue, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($this->flags) || !is_numeric($flagValue))
		{
			XLogError("MonitorEvent::clearFlag validate values failed");
			return false;
		}
		//------------------
		$wasFlags = $this->flags;
		if ( ($this->flags & $flagValue) != 0)
			$this->flags ^= $flagValue;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $wasFlags != $this->flags))
			return $this->updateFlags();
		//------------------
		return true;
	}
	//------------------
	function updateFlags($newValue = false)
	{
		//------------------
		if ( ($newValue !== false && !is_numeric($newValue)) || ($newValue === false && !is_numeric($this->flags)) )
		{
			XLogError("MonitorEvent::updateFlags validate values failed");
			return false;
		}
		//------------------
		if ($newValue !== false)
			$this->flags = $newValue;
		//------------------
		if (!$this->updateValue(DB_MONEVENT_FLAGS, $this->flags))
		{
			XLogError("MonitorEvent::updateFlags updateValue failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function getDetail($key, $default = false)
	{
		return XArray($this->details, $key, $default);
	}
	//------------------
	function clearDetail($key, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($key))
		{
			XLogError("MonitorEvent::clearDetail validate key parameter failed".XVarDump($key));
			return false;
		}
		//------------------
		$modified = (isset($this->details[$key]) ? true : false);
		unset($this->details[$key]);
		//------------------
		if ($updateAlways === true || ($updateChange === true && $modified))
			if (!$this->updateDetails())
			{
				XLogError("MonitorEvent::clearDetail updateDetails failed");
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function setDetail($key, $value, $updateChange = true, $updateAlways = false)
	{
		//------------------
		if (!is_numeric($key))
		{
			XLogError("MonitorEvent::setDetail validate key parameter failed".XVarDump($key));
			return false;
		}
		//------------------
		$modified = (!isset($this->details[$key]) || $this->details[$key] != $value ? true : false);
		$this->details[$key] = $value;
		//------------------
		if ($updateAlways === true || ($updateChange === true && $modified))
			if (!$this->updateDetails())
			{
				XLogError("MonitorEvent::setDetail updateDetails failed");
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function updateDetails($newDetailsArray = false, $newPackedDetails = false)
	{
		//------------------
		$Monitor = new Monitor() or die ('Create object failed');
		//------------------
		if ( ($newDetailsArray !== false && !is_array($newDetailsArray)) || ($newPackedDetails !== false && !is_string($newPackedDetails)) || ($newDetailsArray !== false && $newPackedDetails !== false) )
		{
			XLogError("MonitorEvent::updateDetails validate parameters failed");
			return false;
		}
		//------------------
		if ($newDetailsArray !== false)
			$this->details = $newDetailsArray;
		//------------------
		if ($newPackedDetails === false)
			$newPackedDetails = $Monitor->packDetails($this->details);
		//------------------
		if (!$this->updateValue(DB_MONEVENT_DETAILS, $newPackedDetails))
		{
			XLogError("MonitorEvent::updateDetails updateValue failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function isResolved()
	{
		return $this->hasFlag(MONEVENT_FLAG_RESOLVED);
	}
	//------------------
	function resolve($resolveFlag, $pushRoundAutomation = false, $resolveNote = false)
	{
		global $Login;
		//------------------
		if ($resolveFlag != MONEVENT_FLAG_SELF_FIX && $resolveFlag != MONEVENT_FLAG_USER_FIX && $resolveFlag != MONEVENT_FLAG_USER_BYPASS)
		{
			XLogError("MonitorEvent::resolve validate resolve flag parameter failed: ".XVarDump($resolveFlag));
			return false;
		}
		//------------------
		if ($resolveFlag == MONEVENT_FLAG_SELF_FIX)
		{
			//------------------
			XLogNotify("MonitorEvent::resolve self-resolving the MontiorEvent id $this->id for round $this->ridx with resolve note: ".XVarDump($resolveNote));
			//------------------
			// insert this important detail at the beginning of the array, so it can't be trimmed off if the string is too long when packed
			$this->details = XAssArrayConcat(array(MONEVENT_DETAIL_RESOLVE_NOTE => $resolveNote), $this->details);
			//------------------
		}
		else
		{
			//------------------
			if (!$Login->LoggedIn || !is_numeric($Login->UserID))
			{
				XLogError("MonitorEvent::resolve validate user is logged in for user resolve failed");
				return false;
			}
			//------------------
			if (!$Login->HasPrivilege(PRIV_ROUND_APPROVE))
			{
				XLogError("MonitorEvent::resolve validate user has round approval privileges for user resolve failed");
				return false;
			}
			//------------------
			XLogNotify("MonitorEvent::resolve user ($Login->UserID) '$Login->UserName' is resolving the MontiorEvent id $this->id for round $this->ridx with resolveFlag $resolveFlag");
			//------------------
			// insert this important detail at the beginning of the array, so it can't be trimmed off if the string is too long when packed
			$this->details = XAssArrayConcat(array(MONEVENT_DETAIL_LOGIN_USER => $Login->UserID), $this->details);
			//------------------
		}
		//------------------
		if (!$this->setFlag(MONEVENT_FLAG_RESOLVED, false /*updateChange*/))
		{
			XLogError("MonitorEvent::resolve setFlag for resolved flag failed");
			return false;
		}
		//------------------
		if (!$this->setFlag($resolveFlag, false /*updateChange*/))
		{
			XLogError("MonitorEvent::resolve setFlag for resolveFlag failed");
			return false;
		}
		//------------------
		$this->endDate = time();
		//------------------
		if (!$this->Update())
		{
			XLogError("MonitorEvent::resolve Update failed");
			return false;
		}
		//------------------
		if ($pushRoundAutomation !== false)
		{
			$Automation = new Automation() or die("Create object failed");
			if (!$Automation->ClearRoundCheckNoAction())
			{
				XLogError("MonitorEvent::resolve Automation ClearRoundCheckNoAction failed");
				return false;
			}
		}
		//------------------
		return true;
	}	
	//------------------
	function unresolve()
	{
		global $Login;
		//------------------
		$notifyUserInfo = (!$Login->LoggedIn || !is_numeric($Login->UserID) ? "(no user logged in)" : "user ($Login->UserID) '$Login->UserName'");
		$resolvedBy = $this->getDetail(MONEVENT_DETAIL_LOGIN_USER, '[not set]');
		$resolveNote = $this->getDetail(MONEVENT_DETAIL_RESOLVE_NOTE, '[not set]');
		//------------------
		XLogNotify("MonitorEvent::unresolve $notifyUserInfo is UNresolving the MontiorEvent id $this->id for round $this->ridx resolved on ".XVarDump($this->endDate).", with flags ".XVarDump($this->flags).", resolved by ".XVarDump($resolvedBy).", resolve note ".XVarDump($resolveNote));
		//------------------
		$this->clearFlag(MONEVENT_FLAG_RESOLVED, false /*updateChange*/);
		$this->clearFlag(MONEVENT_FLAG_SELF_FIX, false /*updateChange*/);
		$this->clearFlag(MONEVENT_FLAG_USER_FIX, false /*updateChange*/);
		$this->clearFlag(MONEVENT_FLAG_USER_FIX, false /*updateChange*/);
		//------------------
		$this->clearDetail(MONEVENT_DETAIL_LOGIN_USER, false /*updateChange*/);
		$this->clearDetail(MONEVENT_DETAIL_RESOLVE_NOTE, false /*updateChange*/);
		//------------------
		$this->endDate = false;
		//------------------
		if (!$this->Update())
		{
			XLogError("MonitorEvent::unresolve Update failed");
			return false;
		}
		//------------------
		return true;
	}	
	//------------------
	function updateEndDate()
	{
		//------------------
		if (!$this->updateValue(DB_MONEVENT_END_DATE, time()))
		{
			XLogError("MonitorEvent::updateEndDate updateValue failed");
			return false;
		}
		//------------------
		return true;
	}
} // class MonitorEvent
//---------------
class Monitor
{
	var $initted = false;
	var $ridx = -1;
	var $round = false;
	//------------------
	function __construct()
	{
		//------------------
		//------------------
	}
	//------------------
	function Install()
	{
		global $db, $dbMonEventFields;
		//------------------------------------
		$sql = $dbMonEventFields->scriptCreateTable();
		XLogDebug("Monitor::Install sql: $sql");
		if (!$db->Execute($sql))
		{
			XLogError("Monitor::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbMonEventFields;
		//------------------------------------
		$sql = $dbMonEventFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Monitor::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbMonEventFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case $dbMonEventFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbMonEventFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Monitor::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Monitor::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function typeName($type)
	{
		//------------------
		if (!is_numeric($type))
			return '[bad type parameter]';
		//------------------
		$names = array(
						MONEVENT_TYPE_NONE => 'None',
						MONEVENT_TYPE_NOTIFY => 'Notify',
						MONEVENT_TYPE_WARN => 'Warn',
						MONEVENT_TYPE_ALARM => 'Alarm',
						MONEVENT_TYPE_BLOCK => 'Block'
					  );
		//------------------
		if (!isset($names[$type]))
			return "[Unknown $type]";
		//------------------
		return $names[$type];
	}
	//------------------
	function issueName($issue)
	{
		//------------------
		if (!is_numeric($issue))
			return '[bad issue parameter]';
		//------------------
		$names = array(
						MONEVENT_ISSUE_NONE => 'None',
						MONEVENT_ISSUE_MONITOR_FAIL => 'Monitor Fail',
						MONEVENT_ISSUE_PHP_ERROR => 'PHP Error',
						MONEVENT_ISSUE_ROUND_FAIL => 'Round Fail',
						MONEVENT_ISSUE_ROUND_CHANGED => 'Round Changed',
						MONEVENT_ISSUE_ROUND_NEW => 'New Round',
						MONEVENT_ISSUE_ROUND_START => 'Round Start',
						MONEVENT_ISSUE_ROUND_DONE => 'Round Done',
						MONEVENT_ISSUE_ROUNDSTEP_FAIL => 'Round Step Fail',
						MONEVENT_ISSUE_ROUNDSTEP_COMPLETED => 'Round Step Completed',
						MONEVENT_ISSUE_ROUNDSTEP_INCOMPLETE => 'Round Step Incomplete',
						MONEVENT_ISSUE_ROUNDSTEP_TOO_LONG => 'Round Step Too Long',
						MONEVENT_ISSUE_ROUNDSTEP_ORDER => 'Round Step Order',
						MONEVENT_ISSUE_ROUND_LOCKED => 'Round Locked',
						MONEVENT_ISSUE_ROUND_INVALID => 'Round Invalid',
						MONEVENT_ISSUE_ROUND_DATA_INVALID => 'Round Data Invalid',
						MONEVENT_ISSUE_ROUND_DATA_SANITY => 'Round Data Sanity',
						MONEVENT_ISSUE_ROUND_DATA_INFO => 'Round Data Info',
						MONEVENT_ISSUE_ROUND_FAHSTAT_LOCKED => 'Round Fah Stat Locked',
						MONEVENT_ISSUE_TRANSACTION_FAILED => 'Transaction Failed',
						MONEVENT_ISSUE_CONT_WAIT_FUNDS => 'Contribution Waiting For Funds'
					);
		//------------------
		if (!isset($names[$issue]))
			return "[Unknown $issue]";
		//------------------
		return $names[$issue];
	}
	//------------------
	function detailName($detailKey)
	{
		//------------------
		if (!is_numeric($detailKey))
			return '[bad detail parameter]';
		//------------------
		$names = array(
						MONEVENT_DETAIL_NONE => 'None',
						MONEVENT_DETAIL_NOTE => 'Note',
						MONEVENT_DETAIL_SOURCE => 'Source',
						MONEVENT_DETAIL_FAIL => 'Fail',
						MONEVENT_DETAIL_VALUE => 'Value',
						MONEVENT_DETAIL_VALUE2 => 'Value 2',
						MONEVENT_DETAIL_VALUE_MIN => 'Value Min',
						MONEVENT_DETAIL_VALUE_MAX => 'Value Max',
						MONEVENT_DETAIL_NEW_VALUE => 'New Value',
						MONEVENT_DETAIL_COUNT => 'Count',
						MONEVENT_DETAIL_FIELD => 'Field',
						MONEVENT_DETAIL_BALANCE => 'Balance',
						MONEVENT_DETAIL_BALANCE_LESS_FEE => 'Balance Less Fee',
						MONEVENT_DETAIL_CUR_BALANCE => 'Current Balance',
						MONEVENT_DETAIL_CUR_BALANCE_LESS_FEE => 'Current Balance Less Fee',
						MONEVENT_DETAIL_FEE => 'Fee',
						MONEVENT_DETAIL_EST_FEE => 'Estimated Fee',
						MONEVENT_DETAIL_ADDRESS_SRC => 'Source Address',
						MONEVENT_DETAIL_ADDRESS_DEST => 'Dest. Address',
						MONEVENT_DETAIL_ELAPSED => 'Elapsed(sec)',
						MONEVENT_DETAIL_ELAPSED_FULL => 'Elapsed Full(sec)',
						MONEVENT_DETAIL_ELAPSED_MIN => 'Elapsed Min(sec)',
						MONEVENT_DETAIL_ELAPSED_MAX => 'Elapsed Max(sec)',
						MONEVENT_DETAIL_LOGIN_USER => 'Login User',
						MONEVENT_DETAIL_RESOLVE_NOTE => 'Resolve Note',
						MONEVENT_DETAIL_ROUND_FLAGS => 'Round Flags',
						MONEVENT_DETAIL_ROUND_STEP => 'Round Step',
						MONEVENT_DETAIL_ROUND_ACTION => 'Round Action',
						MONEVENT_DETAIL_ROUND_OLD_STEP => 'Old Round Step',
						MONEVENT_DETAIL_ROUND_OLD_ACTION => 'Old Round Action',
						MONEVENT_DETAIL_ROUND_PROG => 'Round Progress',
						MONEVENT_DETAIL_ROUND_PROG_MAX => 'Round Progress Max',
						MONEVENT_DETAIL_ROUND_PROG_COUNT => 'Round Count Same Progress',
						MONEVENT_DETAIL_ROUND_STEP_COUNT => 'Round Step Count',
						MONEVENT_DETAIL_ROUND_STEP_PASSES => 'Round Step Passes'
					);
		//------------------
		if (!isset($names[$detailKey]))
			return "[Unknown $detailKey]";
		//------------------
		return $names[$detailKey];
	}
	//---------------
	function packDetails($detailsArray)
	{
		//------------------
		if (!is_array($detailsArray))
			return '';
		//------------------
		$keyValList = array();
		$strPacked = '';
		foreach ($detailsArray as $key => $value)
			if ($value !== null)
				$keyValList[] = dechex($key).'='.$value;
		//------------------
		return substr(implode(',', $keyValList), 0,C_MAX_MONITOR_EVENT_DETAILS_LENGTH);
	}
	//------------------
	function unpackDetails($strDetails)
	{
		//------------------
		if ($strDetails === false || $strDetails === null)
			return $array();
		//------------------
		$detailArray = array();
		//------------------
		$keyValList = explode(',', $strDetails);
		//------------------
		foreach ($keyValList as $keyVal)
		{
			//------------------
			$parts = explode('=', $keyVal);
			if (sizeof($parts) == 2)
			{
				$key = hexdec($parts[0]);
				if (is_numeric($key))
					$detailArray[$key] = $parts[1];
			}	
			//------------------
		}
		//------------------
		return $detailArray;
	}
	//------------------
	function addEvent($type, $issue, $detailsArray = false, $flags = false, $ridx = false)
	{
		global $db, $dbMonEventFields;
		//------------------
		if ($ridx === false)
			$ridx = $this->ridx;
		//------------------
		if (!is_numeric($ridx) || !is_numeric($type))
		{
			XLogError("Monitor::addEvent validate parameters failed. ridx ".XVarDump($ridx).", type ".XVarDump($type));
			return false;
		}
		//------------------
		if ($ridx == -1)
			$ridx = 0;
		//------------------
		if (!is_numeric($type) || !is_numeric($issue) || ($flags !== false && !is_numeric($flags)) || ($detailsArray !== false && !is_array($detailsArray)))
		{
			XLogError("Monitor::addEvent validate parameters issues/flags/detailsArray failed");
			return false;
		}
		//------------------
		XLogDebug('Monitor::addEvent adding (r '.$ridx.') ['.$this->typeName($type).'] '.$this->issueName($issue).', details: '.XVarDump($detailsArray));
		//------------------
		$dbMonEventFields->ClearValues();
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_ROUND,		$ridx);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_DATE,		time());
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_TYPE,		$type);
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_ISSUE,		$issue);
		//------------------
		if ($flags === false)
			$flags = MONEVENT_FLAG_NONE;
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_FLAGS, $flags);
		//------------------
		if ($detailsArray === false)
			$detailsArray = array();
		//------------------
		$dbMonEventFields->SetValuePrepared(DB_MONEVENT_DETAILS, $this->packDetails($detailsArray));
		//------------------
		$sql = $dbMonEventFields->scriptInsert();
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Monitor::addEvent db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbMonEventFields->BindValues($dbs))
		{
			XLogError("Monitor::addEvent BindValues failed");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Monitor::addEvent dbs execute scriptInsert (prepared) failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function deleteEvent($id, $where = false)
	{
		global $db, $dbMonEventFields;
		//------------------
		if (!is_numeric($id) || $id === -1)
		{
			XLogError("Monitor::deleteEvent validate parameters failed. id ".XVarDump($id));
			return false;
		}
		//------------------
		$where = ($where === false ? '' : $where.' AND ').DB_MONEVENT_ID."=$id";	
		//------------------
		$sql = $dbMonEventFields->scriptDelete($where);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Monitor::deleteEvent db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function deleteRoundEvents($ridx, $where = false)
	{
		global $db, $dbMonEventFields;
		//------------------
		if (!is_numeric($ridx) || $ridx === -1)
		{
			XLogError("Monitor::deleteRoundEvents validate parameters failed. ridx ".XVarDump($ridx));
			return false;
		}
		//------------------
		$where = ($where === false ? '' : $where.' AND ').DB_MONEVENT_ROUND."=$ridx";	
		//------------------
		$sql = $dbMonEventFields->scriptDelete($where);
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError("Monitor::deleteRoundEvents db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//------------------
	function getEvent($id, $notFoundValue = false)
	{
		global $db, $dbMonEventFields;
		//------------------
		if (!is_numeric($id))
		{
			XLogError("Monitor::getEvent validate parameter failed");
			return false;
		}
		//------------------
		$dbMonEventFields->SetValues();
		$sql = $dbMonEventFields->scriptSelect(DB_MONEVENT_ID."=$id"/*where*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Monitor::getEvent db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$row = $qr->GetRowArray(null /*emptyValue*/);
		if ($row === false)
		{
			XLogError("Monitor::getEvent db query result GetRowArray failed.");
			return false;
		}
		if ($row === null)
		{
			XLogDebug("Monitor::getEvent event id '$id' not found.");
			return $notFoundValue;
		}
		//------------------
		return new MonitorEvent($row);
	}
	//------------------
	function getEvents($where = false, $orderBy = false, $limit = false)
	{
		global $db, $dbMonEventFields;
		//------------------
		if ( ($where !== false && !is_string($where)) || ($orderBy !== false && !is_string($orderBy)) || ($limit !== false && !is_numeric($limit)) )
		{
			XLogError("Monitor::getEvents validate parameters failed");
			return false;
		}
		//------------------
		if ($orderBy === false)
			$orderBy = DB_MONEVENT_ID;
		//------------------
		$dbMonEventFields->SetValues();
		$sql = $dbMonEventFields->scriptSelect($where, $orderBy, $limit);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Monitor::getEvents db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$eventList = array();
		while ( ($row = $qr->GetRowArray()) )
			$eventList[] = new MonitorEvent($row);
		//------------------
		return $eventList;
	}
	//------------------
	function getRoundEvents($ridx, $where = false, $orderBy = false, $limit = false)
	{
		//------------------
		if (!is_numeric($ridx) || ($where !== false && !is_string($where)) )
		{
			XLogError("Monitor::getRoundEvents validate parameters failed");
			return false;
		}
		//------------------
		$rndWhere = DB_MONEVENT_ROUND."=$ridx";
		if ($where !== false)
			$rndWhere .= " AND ($where)";
		//------------------
		$eventList = $this->getEvents($rndWhere, $orderBy, $limit);
		if ($eventList === false)
		{
			XLogError("Monitor::getRoundEvents getEvents failed");
			return false;
		}
		//------------------
		return $eventList;
	}
	//------------------
	function findEvents($ridx = false, $type = false, $issue = false, $onlyResolved = false, $onlyUnresolved = false, $requiredMatchDetails = false, $whenExistMatchDetails = false, $orderBy = false, $limit = false, $alwaysIncludeRoundZero = true)
	{
		global $db, $dbMonEventFields;
		//------------------
		if (($ridx !== false && !is_numeric($ridx)) || ($type !== false && !is_array($type) && !is_numeric($type)) || ($issue !== false && !is_numeric($issue)) || ($requiredMatchDetails !== false && !is_array($requiredMatchDetails)) || ($whenExistMatchDetails !== false && !is_array($whenExistMatchDetails)) || ($onlyResolved === true && $onlyUnresolved === true) )
		{
			XLogError("Monitor::findEvents validate parameters failed");
			return false;
		}
		//------------------
		$whereList = array();
		//------------------
		if ($ridx !== false)
		{
			if ($alwaysIncludeRoundZero)
				$whereList[] =  '('.DB_MONEVENT_ROUND."=$ridx OR ".DB_MONEVENT_ROUND.'=0)';
			else
				$whereList[] =  DB_MONEVENT_ROUND."=$ridx";
		}
		if ($type !== false)
		{
			if (is_array($type))
				$whereList[] = '('.DB_MONEVENT_TYPE.'='.implode(' OR '.DB_MONEVENT_TYPE.'=', $type).')';
			else
				$whereList[] =  DB_MONEVENT_TYPE."=$type";
		}
		if ($issue !== false)
			$whereList[] =  DB_MONEVENT_ISSUE."=$issue";
		if ($onlyResolved !== false)
			$whereList[] = '(CAST('.DB_MONEVENT_FLAGS.' AS UNSIGNED) & '.MONEVENT_FLAG_RESOLVED.') != 0';
		if ($onlyUnresolved !== false)
			$whereList[] = '(CAST('.DB_MONEVENT_FLAGS.' AS UNSIGNED) & '.MONEVENT_FLAG_RESOLVED.') = 0';
		//------------------
		$where = implode(' AND ', $whereList);
		//------------------
		$eventList = $this->getEvents($where, $orderBy, $limit);
		//XLogDebug("Monitor::findEvents where: ".XVarDump($where)."\n\teventList: ".XVarDump($eventList));
		if ($eventList === false)
		{
			XLogError("Monitor::findEvents getEvents failed. where: ".XVarDump($where));
			return false;
		}
		//------------------
		if ($requiredMatchDetails !== false || $whenExistMatchDetails !== false)
		{
			//------------------
			$filteredEventList = array();
			//------------------
			foreach ($eventList as $event)
			{
				//------------------
				$keep = true;
				//------------------
				if ($requiredMatchDetails !== false)
					foreach ($requiredMatchDetails as $matchKey => $matchValue)
						if (!isset($event->details[$matchKey]) || $event->details[$matchKey] != $matchValue)
						{
							//XLogDebug("Monitor::findEvents requiredMatchDetails mismatch on key: ".XVarDump($matchKey).", match-value: ".XVarDump($matchValue).", value: ".XVarDump($event->details[$matchKey]));
							$keep = false;
							break;
						}
				//------------------
				if ($keep && $whenExistMatchDetails !== false)
					foreach ($whenExistMatchDetails as $matchKey => $matchValue)
						if (isset($event->details[$matchKey]) && $event->details[$matchKey] != $matchValue)
						{
							//XLogDebug("Monitor::findEvents whenExistMatchDetails mismatch on key: ".XVarDump($matchKey).", match-value: ".XVarDump($matchValue).", value: ".XVarDump($event->details[$matchKey]));
							$keep = false;
							break;
						}
				//------------------
				if ($keep)
					$filteredEventList[] = $event;
				//------------------
			}
			//------------------
			$eventList = $filteredEventList;
			//------------------
		}
		//------------------
		return $eventList;
	}
	//------------------
	function findLastEvent($ridx = false, $type = false, $issue = false, $onlyResolved = false, $onlyUnresolved = false, $requiredMatchDetails = false, $whenExistMatchDetails = false, $alwaysIncludeRoundZero = true, $emptyValue = true, $failValue = false)
	{
		//------------------
		$orderBy = DB_MONEVENT_ID.' DESC';
		//------------------
		$eventList = $this->findEvents($ridx, $type, $issue, $onlyResolved, $onlyUnresolved, $requiredMatchDetails,$whenExistMatchDetails, $orderBy, false /*limit*/, $alwaysIncludeRoundZero);
		//------------------
		//XLogDebug("Monitor::findLastEvent findEvents result: ".XVarDump($eventList));
		if ($eventList === false)
		{
			XLogError("Monitor::findLastEvent findEvents failed");
			return $failValue;
		}
		//------------------
		if (sizeof($eventList) == 0)
			return $emptyValue;
		//------------------
		if (!isset($eventList[0]) || !is_object($eventList[0]) || !is_a($eventList[0], 'MonitorEvent'))
		{
			XLogError("Monitor::findLastEvent validate findEvents reply failed. eventList: ".XVarDump($eventList));
			return $failValue;
		}
		//------------------
		return $eventList[0];
	}
	//------------------
	function RoundEventsBlocking($ridx = false, $alwaysIncludeRoundZero = true)
	{
		//------------------
		if ($ridx === true)
		{
			if (!$this->TryInit('RoundEventsBlocking'))
				return false;
			$ridx = ($this->ridx != -1 ? $this->ridx : 0);
		}
		//------------------
		$eventList = $this->findEvents($ridx, MONEVENT_TYPE_BLOCK, false /*issue*/, false /*onlyResolved*/, true /*onlyUnresolved*/, false /*requiredMatchDetails*/, false /*whenExistMatchDetails*/, false /*orderBy*/, false /*limit*/, $alwaysIncludeRoundZero);
		//------------------
		if ($eventList === false)
		{
			XLogError("Monitor::RoundEventsBlocking findEvents failed");
			return false;
		}
		//------------------
		if ($eventList === true /*empty*/)
			$eventList = array();
		//------------------
		return $eventList;
	}
	//------------------
	function RoundEventsAlarming($ridx = false, $includeBlocking = true, $alwaysIncludeRoundZero = true)
	{
		//------------------
		if ($ridx === true)
		{
			if (!$this->TryInit('RoundEventsBlocking'))
				return false;
			$ridx = ($this->ridx != -1 ? $this->ridx : 0);
		}
		//------------------
		$eventList = $this->findEvents($ridx, MONEVENT_TYPE_ALARM, false /*issue*/, false /*onlyResolved*/, true /*onlyUnresolved*/, false /*requiredMatchDetails*/, false /*whenExistMatchDetails*/, false /*orderBy*/, false /*limit*/, $alwaysIncludeRoundZero);
		//------------------
		if ($eventList === false)
		{
			XLogError("Monitor::RoundEventsAlarming findEvents failed");
			return false;
		}
		//------------------
		if ($eventList === true /*empty*/)
			$eventList = array();
		//------------------
		if ($includeBlocking)
		{
			//------------------
			$blockList = $this->RoundEventsBlocking($ridx);
			if ($blockList === false)
			{
				XLogError("Monitor::RoundEventsAlarming RoundEventsBlocking failed");
				return false;
			}
			//------------------
			if ($blockList !== true /*empty*/)
				$eventList = array_merge($blockList, $eventList);
			//------------------
		}
		//------------------
		return $eventList;
	}
	//------------------
	function addUpdateEvent($type, $issue, $detailsArray = false, $requiredMatchDetailsArray = true /*false ignore, true use detailsArray*/, $whenExistMatchDetailsArray = false /*false ignore, true use detailsArray*/)
	{
		//------------------
		if (!is_numeric($type) || !is_numeric($issue) || ($detailsArray !== false && !is_array($detailsArray)) || ($requiredMatchDetailsArray !== false && $requiredMatchDetailsArray !== true && !is_array($requiredMatchDetailsArray)) || ($whenExistMatchDetailsArray !== false && $whenExistMatchDetailsArray !== true && !is_array($whenExistMatchDetailsArray)) )
		{
			XLogError("Monitor::addUpdateEvent validate parameters failed");
			return false;
		}
		//------------------
		if (!is_numeric($this->ridx) || $this->ridx == -1)
		{
			XLogError("Monitor::addUpdateEvent validate round is set failed");
			return false;
		}
		//------------------
		if ($requiredMatchDetailsArray === true)
			$requiredMatchDetailsArray = $detailsArray;
		if ($whenExistMatchDetailsArray === true)
			$whenExistMatchDetailsArray = $detailsArray;
		//------------------
		$event = $this->findLastEvent($this->ridx, $type, $issue, false /*onlyResolved*/, false /*onlyUnresolved*/, $requiredMatchDetailsArray, $whenExistMatchDetailsArray, false /*alwaysIncludeRoundZero*/);
		//------------------
		//XLogDebug("Monitor::addUpdateEvent type/issue $type/$issue required ".XVarDump($requiredMatchDetailsArray).", result ".XVarDump($event));
		if ($event === false)
		{
			XLogError("Monitor::addUpdateEvent findLastEvent failed");
			return false;
		}
		//------------------
		if ($event === true) // none found
		{
			//------------------
			if (!$this->addEvent($type, $issue, $detailsArray))
			{
				XLogError("Monitor::addUpdateEvent addEvent failed");
				return false;
			}
			//------------------
			return true; // done
		}
		//------------------
		if ($event->isResolved())
			return $event;
		//------------------
		if (!$event->updateDetails($detailsArray))
		{
			XLogError("Monitor::addUpdateEvent unresolved event updateDetails failed");
			return false;
		}
		//------------------
		if (!$event->updateEndDate())
		{
			XLogError("Monitor::addUpdateEvent unresolved event updateEndDate failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function Init()
	{
		global $Config;
		//------------------
		if ($this->initted)
			return true;
		//------------------
		$this->round = false; // clear, so ridx can't missmatch
		$this->ridx = $Config->Get(CFG_MONITOR_CUR_ROUND, -1);
		if (!is_numeric($this->ridx))
		{
			XLogError('Monitor::Init validate current round index Config value failed. See '.CFG_MONITOR_CUR_ROUND);
			return false;
		}
		//------------------
		$this->initted = true;
		//------------------
		return true;
	}	
	//------------------
	function TryInit($callingFunction = '[no caller]')
	{
		//------------------
		if (!$this->Init())
		{
			XLogError("Monitor::$callingFunction Init failed");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function TestRoundAndTryInit($round = false, $callingFunction = '[no caller]', $AllowNoRound = false)
	{
		global $Config;
		//------------------
		if (!$this->TryInit($callingFunction))
			return false;
		//------------------
		if ($round === false)
			$round = $this->round;
		//------------------
		if ($round === false)
		{
			//------------------
			if ($this->ridx == -1)
			{
				//------------------
				if ($AllowNoRound)
					return true; // no round selected 
				//------------------
				XLogError("Monitor::$callingFunction(TestRoundAndTryInit) no round given in parameter and Monitor has no round set");
				//------------------
				return false;
			}
			//------------------
			$Rounds = new Rounds() or die ('Create object failed');
			$round = $Rounds->getRound($this->ridx);
			if ($round === false)
			{
				XLogWarn("Monitor::$callingFunction(TestRoundAndTryInit) no round given in parameter and Rounds getRound failed to get Monitor's ridx: ".XVarDump($this->ridx));
				return false;
			}
			//------------------
		}
		//------------------
		if (! (is_object($round) && is_a($round, 'Round')) )
		{
			XLogError("Monitor::$callingFunction(TestRoundAndTryInit) validate round object parameter failed: ".XVarDump($round));
			return false;
		}
		//------------------
		if ($this->ridx != $round->id)
		{
			//------------------
			if ($this->ridx != -1) // not first-and-only, but changed
			{
				$oldId = $this->ridx;
				$this->ridx = $round->id;
				XLogDebug("Monitor::$callingFunction current round changed: $oldId -> $this->ridx");
				if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_CHANGED, array(MONEVENT_DETAIL_VALUE => $oldId, MONEVENT_DETAIL_NEW_VALUE => $this->ridx, MONEVENT_DETAIL_SOURCE => $callingFunction)))
				{
					XLogError("Monitor::$callingFunction(TestRoundAndTryInit) round change addEvent failed");
					return false;
				}
				//------------------
			}
			//------------------
			$this->ridx = $round->id;
			//------------------
			if (!$Config->Set(CFG_MONITOR_CUR_ROUND, $this->ridx))
			{
				XLogError("Monitor::$callingFunction(TestRoundAndTryInit) Config Set cur round failed");
				return false;
			}
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_NOTIFY, MONEVENT_ISSUE_ROUND_NEW, array(MONEVENT_DETAIL_SOURCE => $callingFunction)))
			{
				XLogError("Monitor::$callingFunction(TestRoundAndTryInit) new addEvent failed");
				return false;
			}
			//------------------
		}
		//------------------
		$this->round = $round;
		//------------------
		return true;
	}
	//------------------
	function Cleanup()
	{
		global $Config;
		//------------------
		if (!$this->TryInit('Cleanup'))
			return false;
		//------------------
		$configs = array(	CFG_MONITOR_CUR_ROUND => -1,
							CFG_MONITOR_STEP_START => -1,
							CFG_MONITOR_STEP_STOP => -1,
							CFG_MONITOR_STEP_BEGIN => -1,
							CFG_MONITOR_STEP => -1,
							CFG_MONITOR_ACTION => -1,
							CFG_MONITOR_STEP_FAILS => -1,
							CFG_MONITOR_STEP_COUNT => -1);
		//------------------
		if (!$Config->SetMany($configs))
		{
			XLogError('Monitor::Cleanup Config SetMany failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function PurgeResetRound($ridx)
	{
		//------------------
		if (!$this->deleteRoundEvents($ridx))
		{
			XLogError('Monitor::PurgeResetRound deleteRoundEvents failed');
			return false;
		}
		//------------------
		if (!$this->Cleanup())
		{
			XLogError('Monitor::PurgeResetRound Cleanup failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundStart($round, $source = 'none')
	{
		global $Config;
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundStart'))
			return false;
		//------------------
		if (!$this->addEvent(MONEVENT_TYPE_NOTIFY, MONEVENT_ISSUE_ROUND_START,array(MONEVENT_DETAIL_ROUND_FLAGS => $round->flags, MONEVENT_DETAIL_SOURCE => $source)))
		{
			XLogError('Monitor::RoundStart addEvent failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundDone($round, $detailsArray = array())
	{
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundDone'))
			return false;
		//------------------
		if (!$this->addEvent(MONEVENT_TYPE_NOTIFY, MONEVENT_ISSUE_ROUND_DONE, $detailsArray))
		{
			XLogError('Monitor::RoundDone addEvent failed');
			return false;
		}
		//------------------
		if (!$this->Cleanup())
		{
			XLogError('Monitor::RoundDone Cleanup failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function roundStepDetails($existingDetailsArray = false, $includeProgress = true, $round = false)
	{
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'roundStepDetails', true /*AllowNoRound*/))
		{
			//------------------
			$detArray = array(MONEVENT_DETAIL_SOURCE => 'roundStepDetails', MONEVENT_DETAIL_FAIL => 'validate round parameter failed');
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_MONITOR_FAIL, $detArray))
				XLogError('Monitor::roundStepDetails addEvent round validate fail failed');
			//------------------
			if (is_array($existingDetailsArray))
				XAssArrayMerge($detArray, $existingDetailsArray);
			//------------------
			return $detArray;
		}
		//------------------
		if ($this->round === false)
			return ($existingDetailsArray !== false ? $existingDetailsArray : array());
		//------------------
		$action = $this->round->currentAction();
		if ($action === false)
		{
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_FAIL, array(MONEVENT_DETAIL_SOURCE => 'roundStepDetails', MONEVENT_DETAIL_FAIL => 'round currentAction failed')))
				XLogError('Monitor::roundStepDetails addEvent round currentAction fail failed');
			//------------------
			$action = '[currentAction failed]';
			//------------------
		}
		//------------------
		$detArray = array();
		//------------------
		$detArray[MONEVENT_DETAIL_ROUND_STEP] = $this->round->step;
		$detArray[MONEVENT_DETAIL_ROUND_ACTION] = $action;
		//------------------
		$progArray = $this->round->getProgress();
		if ($includeProgress === true)
		{
			if ($progArray === false)
			{
				//------------------
				if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_FAIL, array(MONEVENT_DETAIL_SOURCE => 'roundStepDetails', MONEVENT_DETAIL_FAIL => 'round getProgress failed')))
					XLogError('Monitor::roundStepDetails addEvent round getProgress fail failed');
				//------------------
				$detArray[MONEVENT_DETAIL_ROUND_PROG] = $detArray[MONEVENT_DETAIL_ROUND_PROG_MAX] = '[round getProgress failed]';
				//------------------
			}
			else
			{
				//------------------
				$detArray[MONEVENT_DETAIL_ROUND_PROG] = $progArray[0];
				$detArray[MONEVENT_DETAIL_ROUND_PROG_MAX] = $progArray[1];
				if (sizeof($progArray) >= 3 && $progArray[2] != 0)
					$detArray[MONEVENT_DETAIL_ROUND_PROG_COUNT] = $progArray[2];
				//------------------
			}
		}
		//------------------
		if (is_array($existingDetailsArray))
			$detArray = XAssArrayMerge($detArray, $existingDetailsArray);
		//------------------
		return $detArray;
	}
	//------------------
	function RoundStepStart($round)
	{
		global $Config;
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundStepStart'))
			return false;
		//------------------
		$curRoundAction = $round->currentAction();
		if ($curRoundAction === false)
		{
			//------------------
			XLogError('Monitor::RoundStepStart round currentAction failed');
			//------------------
			$retVal = $this->addUpdateEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_FAIL, array(MONEVENT_DETAIL_ROUND_STEP => $round->step, MONEVENT_DETAIL_SOURCE => 'RoundStepStart', MONEVENT_DETAIL_FAIL => 'round currentAction failed'));
			//------------------
			if ($retVal === false)
			{
				XLogError('Monitor::RoundStepStart addUpdateEvent round currentAction fail failed');
				return false;
			}
			//------------------
			return false; // can't resolve
		}
		//------------------
		$nowTime = time();
		//------------------
		$values = $Config->GetMany(array(CFG_MONITOR_STEP_START, CFG_MONITOR_STEP_STOP, CFG_MONITOR_STEP_BEGIN, CFG_MONITOR_STEP, CFG_MONITOR_ACTION, CFG_MONITOR_STEP_FAILS, CFG_MONITOR_STEP_COUNT), -1);
		if ($values === false)
		{
			XLogError('Monitor::RoundStepStart Config GetMany failed');
			return false;
		}
		//------------------
		$beginDiff = ($values[CFG_MONITOR_STEP_BEGIN] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_BEGIN]));
		$startDiff = ($values[CFG_MONITOR_STEP_START] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_START]));
		$stopDiff = ($values[CFG_MONITOR_STEP_STOP] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_STOP]));
		//------------------
		if ($startDiff >= 0 && $stopDiff == -1)
		{
			//------------------ last step never finished MONEVENT_DETAIL_ELAPSED_FULL
			if (!$this->addUpdateEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUNDSTEP_ORDER, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStart', MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_FAIL => $values[CFG_MONITOR_STEP_FAILS], MONEVENT_DETAIL_ROUND_STEP_COUNT => $values[CFG_MONITOR_STEP_COUNT], MONEVENT_DETAIL_NOTE => 'Last step never finished'))))
			{
				XLogError('Monitor::RoundStepStop addUpdateEvent (last step never finished) failed');
				return false;
			}
			//------------------
		}
		if ($startDiff > 61)
		{
			// excessive delay between round stepping
		}
		if ($startDiff != -1 && $startDiff < 59)
		{
			// too quick of delay between round stepping
		}
		//------------------
		// check step/actions $values[$cfgStep] $values[$cfgAction]
		//------------------
		if ($round->step != $values[CFG_MONITOR_STEP]) // step changed
		{
			//------------------
			if ($values[CFG_MONITOR_STEP_FAILS] != -1 && !$Config->Set(CFG_MONITOR_STEP_FAILS, -1))
			{
				XLogError('Monitor::RoundStepStart Config Set (clear step fails) failed');
				return false;
			}
			//------------------
			if (!$Config->Set(CFG_MONITOR_STEP_BEGIN, $nowTime))
			{
				XLogError('Monitor::RoundStepStart Config Set (step begin) failed');
				return false;
			}
			//------------------
		}
		//------------------
		if (!$Config->SetMany(array(CFG_MONITOR_STEP_START => $nowTime, CFG_MONITOR_STEP_STOP => -1, CFG_MONITOR_STEP => $round->step, CFG_MONITOR_ACTION => $curRoundAction)))
		{
			XLogError('Monitor::RoundStepStart Config SetMany failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundStepStop($round)
	{
		global $Config;
		//------------------
		$Rounds = new Rounds() or die('Create object failed');
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundStepStop'))
			return false;
		//------------------
		$currentRoundAction = $round->currentAction();
		if ($currentRoundAction === false)
		{
			//------------------
			XLogError('Monitor::RoundStepStop round currentAction failed');
			//------------------
			$retVal = $this->addUpdateEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_FAIL, array(MONEVENT_DETAIL_ROUND_STEP => $round->step, MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_FAIL => 'round currentAction failed'));
			//------------------
			if ($retVal === false)
			{
				XLogError('Monitor::RoundStepStop addUpdateEvent round currentAction fail failed');
				return false;
			}
			//------------------
			return false; // can't resolve
		}
		//------------------
		$nowTime = time();
		//------------------
		$values = $Config->GetMany(array(CFG_MONITOR_STEP_START, CFG_MONITOR_STEP_STOP, CFG_MONITOR_STEP_BEGIN, CFG_MONITOR_STEP, CFG_MONITOR_ACTION, CFG_MONITOR_STEP_FAILS, CFG_MONITOR_STEP_COUNT), -1);
		if ($values === false)
		{
			XLogError('Monitor::RoundStepStop Config GetMany failed');
			return false;
		}
		//------------------
		$beginDiff = ($values[CFG_MONITOR_STEP_BEGIN] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_BEGIN]));
		$stepCount = ($values[CFG_MONITOR_STEP_COUNT] == -1 ? 0 : $values[CFG_MONITOR_STEP_COUNT]);
		$startDiff = ($values[CFG_MONITOR_STEP_START] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_START]));
		//------------------
		if ($values[CFG_MONITOR_STEP_STOP] != -1)
		{
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_ORDER, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_VALUE => $values[CFG_MONITOR_STEP_STOP], MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount, MONEVENT_DETAIL_NOTE => 'Round step already stopped'))))
			{
				XLogError('Monitor::RoundStepStop addEvent step already stopped failed');
				return false;
			}
			//------------------
		}
		if ($values[CFG_MONITOR_STEP_START] == -1)
		{
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_ORDER, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_NOTE => 'Step not started'))))
			{
				XLogError('Monitor::RoundStepStop addEvent step not started failed');
				return false;
			}
			//------------------
		}
		if ($startDiff > 61)
		{
			//------------------
			if (!$this->addEvent(($startDiff > 120 ? MONEVENT_TYPE_ALARM : MONEVENT_TYPE_WARN), MONEVENT_ISSUE_ROUNDSTEP_TOO_LONG, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount))))
			{
				XLogError('Monitor::RoundStepStop addEvent step too long failed');
				return false;
			}
			//------------------
		}
		//------------------
		if ($values[CFG_MONITOR_STEP] == $round->step) // step didn't change
		{
			//------------------
			$stepCount++;
			//------------------
			if (!$Rounds->isMultiPassAction($currentRoundAction))
			{
				//------------------
				$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop'));
				$matchDetails = XAssArrayClone($details);
				$details[MONEVENT_DETAIL_ROUND_STEP_COUNT] = $stepCount;
				$details[MONEVENT_DETAIL_ELAPSED] = $startDiff;
				$details[MONEVENT_DETAIL_ELAPSED_FULL] = $beginDiff;
				//------------------
				if (!$this->addUpdateEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_INCOMPLETE, $details, $matchDetails))
				{
					XLogError('Monitor::RoundStepStop addUpdateEvent round step incomplete failed');
					return false;
				}
				//------------------
			}
			//------------------
			$progArray = $this->round->getProgress();
			if ($progArray !== false && $progArray[0] > $progArray[1])
			{
				//------------------
				$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_FIELD => 'Step Progress', MONEVENT_DETAIL_NOTE => 'Round step progress exceeds progress max'), false /*$includeProgress*/);
				$detailsMatch = XAssArrayClone($details);
				$details = XAssArrayConcat($details, array(MONEVENT_DETAIL_ROUND_PROG => $progArray[0], MONEVENT_DETAIL_ROUND_PROG_MAX => $progArray[1], MONEVENT_DETAIL_ROUND_PROG_COUNT => (sizeof($progArray) >= 3 ? $progArray[2] : -1), MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_FAIL => ($values[CFG_MONITOR_STEP_FAILS] > 0 ? $values[CFG_MONITOR_STEP_FAILS] : null), MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff));
				if (!$this->addUpdateEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUND_INVALID, $details, $detailsMatch))
				{
					XLogError('Monitor::RoundStepStop addUpdateEvent step progress invalid failed');
					return false;
				}
				//------------------
			}
			//------------------
		}
		else // step done/changed
		{
			//------------------
			$lastTooLongEvents = $this->findEvents($this->ridx, array(MONEVENT_TYPE_ALARM, MONEVENT_TYPE_WARN), MONEVENT_ISSUE_ROUNDSTEP_TOO_LONG, false /*onlyResolved*/, true /*onlyUnresolved*/, array(MONEVENT_DETAIL_ROUND_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_ACTION => $values[CFG_MONITOR_ACTION]) /*requiredMatchDetails*/, false /*whenExistMatchDetails*/, DB_MONEVENT_ID.' DESC'/*orderBy*/, false /*limit*/, false /*alwaysIncludeRoundZero*/);
			if ($lastTooLongEvents === false)
			{
				XLogError('Monitor::RoundStepStop findEvents (too long) step complete failed');
				return false;
			}
			//------------------
			$lastRoundLockedEvents = $this->findEvents($this->ridx, MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_LOCKED, false /*onlyResolved*/, true /*onlyUnresolved*/, array(MONEVENT_DETAIL_ROUND_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_ACTION => $values[CFG_MONITOR_ACTION]) /*requiredMatchDetails*/, false /*whenExistMatchDetails*/, DB_MONEVENT_ID.' DESC' /*orderBy*/, false /*limit*/, false /*alwaysIncludeRoundZero*/);
			if ($lastRoundLockedEvents === false)
			{
				XLogError('Monitor::RoundStepStop findEvents (round locked) step complete failed');
				return false;
			}
			//------------------
			$lastStepIncompleteEvents = $this->findEvents($this->ridx, MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_INCOMPLETE, false /*onlyResolved*/, true /*onlyUnresolved*/, array(MONEVENT_DETAIL_ROUND_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_ACTION => $values[CFG_MONITOR_ACTION]) /*requiredMatchDetails*/, false /*whenExistMatchDetails*/, DB_MONEVENT_ID.' DESC' /*orderBy*/, false /*limit*/, false /*alwaysIncludeRoundZero*/);
			if ($lastStepIncompleteEvents === false)
			{
				XLogError('Monitor::RoundStepStop findEvents (step incomplete) step complete failed');
				return false;
			}
			//------------------
			$selfFixNotes = '';
			if (sizeof($lastTooLongEvents) != 0)
			{
				foreach ($lastTooLongEvents as $event)
				{
					if (!$event->resolve(MONEVENT_FLAG_SELF_FIX, false /*pushRoundAutomation*/, "Round step completed" /*resolveNote*/))
					{
						XLogError('Monitor::RoundStepStop (step complete) lastTooLongEvent resolve failed');
						return false;
					}
					XLogNotify('Monitor::RoundStepStop step complete resolved RoundStep Too Long event '.$event->id);
				}
				$selfFixNotes .= 'Self-fixed '.sizeof($lastTooLongEvents).' Round Step Too Long event(s) ';
			}
			//------------------
			if (sizeof($lastRoundLockedEvents) != 0)
			{
				foreach ($lastRoundLockedEvents as $event)
				{
					if (!$event->resolve(MONEVENT_FLAG_SELF_FIX, false /*pushRoundAutomation*/, "Round step completed" /*resolveNote*/))
					{
						XLogError('Monitor::RoundStepStop (step complete) lastRoundLockedEvent resolve failed');
						return false;
					}
					XLogNotify('Monitor::RoundStepStop step complete resolved Round Locked event '.$event->id);
				}
				$selfFixNotes .= 'Self-fixed '.sizeof($lastRoundLockedEvents).' Round Locked event(s)';
			}
			//------------------
			if (sizeof($lastStepIncompleteEvents) != 0)
			{
				foreach ($lastStepIncompleteEvents as $event)
				{
					if (!$event->resolve(MONEVENT_FLAG_SELF_FIX, false /*pushRoundAutomation*/, "Round step completed" /*resolveNote*/))
					{
						XLogError('Monitor::RoundStepStop (step complete) lastStepIncompleteEvents resolve failed');
						return false;
					}
					XLogNotify('Monitor::RoundStepStop step complete resolved Round Step Incomplete event '.$event->id);
				}
				$selfFixNotes .= 'Self-fixed '.sizeof($lastStepIncompleteEvents).' Step Incomplete event(s)';
			}
			//------------------
			if ($round->step === ROUND_STEP_DONE)
			{
				//------------------
				if (!$round->isDone() || $round->isActive())
					if (!$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_ORDER, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop', MONEVENT_DETAIL_NOTE => 'Round is done, but flags show active or not done', MONEVENT_DETAIL_ROUND_FLAGS => $round->flags, MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount, MONEVENT_DETAIL_FAIL => ($values[CFG_MONITOR_STEP_FAILS] > 0 ? $values[CFG_MONITOR_STEP_FAILS] : null)))))
					{
						XLogError('Monitor::RoundStepStop addEvent step complete failed');
						return false;
					}
				//------------------
				if (!$this->RoundDone($round, array(MONEVENT_DETAIL_SOURCE => 'RoundStepStop')))
				{
					XLogNotify('Monitor::RoundStepStop RoundDone failed');
					return false;
				}
				//------------------
				return true;
			}
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_NOTIFY, MONEVENT_ISSUE_ROUNDSTEP_COMPLETED, $this->roundStepDetails(array(MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff, MONEVENT_DETAIL_ROUND_OLD_STEP => $values[CFG_MONITOR_STEP], MONEVENT_DETAIL_ROUND_OLD_ACTION => $values[CFG_MONITOR_ACTION], MONEVENT_DETAIL_ROUND_STEP_COUNT => $stepCount, MONEVENT_DETAIL_FAIL => ($values[CFG_MONITOR_STEP_FAILS] > 0 ? $values[CFG_MONITOR_STEP_FAILS] : null), MONEVENT_DETAIL_RESOLVE_NOTE => ($selfFixNotes != '' ? $selfFixNotes : null)))))
			{
				XLogError('Monitor::RoundStepStop addEvent step complete failed');
				return false;
			}
			//------------------
			$stepCount = 0; // last step ended, new one hasn't started yet
			//------------------
		} // else // step done/changed
		//------------------
		if (!$Config->SetMany(array(CFG_MONITOR_STEP_STOP => $nowTime, CFG_MONITOR_STEP_COUNT => $stepCount)))
		{
			XLogError('Monitor::RoundStepStop Config Set step stop failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundStepBlocked($round, $source = '[no source specified]')
	{
		global $Config;
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundStepBlocked'))
			return false;
		//------------------
		// just cleanup step start time, so next step start doesn't trigger start without stop event
		//------------------
		if (!$Config->SetMany(array(CFG_MONITOR_STEP_START => -1)))
		{
			XLogError('Monitor::RoundStepBlocked Config Set step stop failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundStepFail($round)
	{
		global $Config;
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'RoundStepFail'))
			return false;
		//------------------
		$values = $Config->GetMany(array(CFG_MONITOR_STEP_START, CFG_MONITOR_STEP_STOP, CFG_MONITOR_STEP_BEGIN, CFG_MONITOR_STEP, CFG_MONITOR_STEP_FAILS, CFG_MONITOR_STEP_COUNT), -1);
		if ($values === false)
		{
			XLogError('Monitor::RoundStepFail Config GetMany failed');
			return false;
		}
		//------------------
		$nowTime = time();
		$beginDiff = ($values[CFG_MONITOR_STEP_BEGIN] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_BEGIN]));
		$startDiff = ($values[CFG_MONITOR_STEP_START] == -1 ? -1 : ($nowTime - $values[CFG_MONITOR_STEP_START]));
		$failCount = ($values[CFG_MONITOR_STEP_FAILS] == -1 ? 0 : $values[CFG_MONITOR_STEP_FAILS]);
		//------------------
		$failCount++;
		//------------------
		// special handling for contrib step and payout insufficent funds
		//------------------
		$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepFail', MONEVENT_DETAIL_FAIL => $failCount,  MONEVENT_DETAIL_COUNT => $values[CFG_MONITOR_STEP_COUNT], MONEVENT_DETAIL_ELAPSED => $startDiff, MONEVENT_DETAIL_ELAPSED_FULL => $beginDiff));
		$matchDetails = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'RoundStepFail'));
		//------------------
		if (!$this->addUpdateEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUNDSTEP_FAIL, $details, $matchDetails))
		{
			XLogError('Monitor::RoundStepFail addUpdateEvent failed');
			return false;
		}
		//------------------
		// update failCount and clear step start time so we avoid a resulting round step order alarm
		if (!$Config->SetMany(array(CFG_MONITOR_STEP_FAILS => $failCount, CFG_MONITOR_STEP_START => -1)))
		{
			XLogError('Monitor::RoundStepFail Config SetMany failCount failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundLocked($source, $cfgLockName = false, $elapsedSec = false, $elapsedMaxSec = false, $isIgnored = false)
	{
		//------------------
		if ( ($elapsedSec !== false && !is_numeric($elapsedSec)) || ($elapsedMaxSec !== false && !is_numeric($elapsedMaxSec)) )
		{
			XLogError('Monitor::RoundLocked validate parameters failed');
			return false;
		}
		//------------------
		if (!$this->TestRoundAndTryInit(false /*round*/, 'RoundLocked'))
		{
			XLogWarn('Monitor::RoundLocked TestRoundAndTryInitfailed failed. No round currently selected? Can not create event for source: '.XVarDump($source));
			return true; // don't fail, probably no current round found
		}
		//------------------
		$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => $source));
		//------------------
		if ($cfgLockName !== false)
			$details[MONEVENT_DETAIL_VALUE] = $cfgLockName;
		//------------------
		$matchDetails = XAssArrayClone($details);
		//------------------
		if ($elapsedSec !== false)
			$details[MONEVENT_DETAIL_ELAPSED] = $elapsedSec;
		if ($elapsedMaxSec !== false)
			$details[MONEVENT_DETAIL_ELAPSED_MAX] = $elapsedMaxSec;
		//------------------
		if ($isIgnored)
			$details[MONEVENT_DETAIL_NOTE] = 'Ignored';
		//------------------
		if (!$this->addUpdateEvent( (!$isIgnored ? MONEVENT_TYPE_ALARM : MONEVENT_TYPE_WARN), MONEVENT_ISSUE_ROUND_LOCKED, $details, $matchDetails))
		{
			XLogError('Monitor::RoundLocked addUpdateEvent failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function RoundFahStatLocked($source, $fieldName, $failCount = false, $elapsedSec = false, $elapsedMinSec = false)
	{
		//------------------
		if ( ($failCount !== false && !is_numeric($failCount)) || ($elapsedSec !== false && !is_numeric($elapsedSec)) || ($elapsedMinSec !== false && !is_numeric($elapsedMinSec)) )
		{
			XLogError('Monitor::RoundFahStatLocked validate parameters failed');
			return false;
		}
		//------------------
		if (!$this->TestRoundAndTryInit(false /*round*/, 'RoundFahStatLocked'))
		{
			XLogWarn('Monitor::RoundFahStatLocked TestRoundAndTryInitfailed failed. No round currently selected? Can not create event for source: '.XVarDump($source));
			return true; // don't fail, probably no current round found
		}
		//------------------
		$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => $source, MONEVENT_DETAIL_FIELD => $fieldName));
		$matchDetails = XAssArrayClone($details);
		//------------------
		if ($failCount !== false)
		{
			$details[MONEVENT_DETAIL_COUNT] = $failCount;
			$details[MONEVENT_DETAIL_NOTE] = 'Fah stats failed too many times';
		}
		else
		{
			if ($elapsedSec !== false)
				$details[MONEVENT_DETAIL_ELAPSED] = $elapsedSec;
			if ($elapsedMinSec !== false)
				$details[MONEVENT_DETAIL_ELAPSED_MIN] = $elapsedMinSec;
			$details[MONEVENT_DETAIL_NOTE] = 'Fah stats rate limitted';
		}
		//------------------
		if (!$this->addUpdateEvent( ($failCount !== false ? MONEVENT_TYPE_BLOCK : MONEVENT_TYPE_ALARM), MONEVENT_ISSUE_ROUND_FAHSTAT_LOCKED, $details, $matchDetails))
		{
			XLogError('Monitor::RoundFahStatLocked addUpdateEvent failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function PreValidate($round = false)
	{
		global $Config;
		$Contributions = new Contributions() or die('Create object failed');
		$Wallet = new Wallet() or die('Create object failed');
		$Rounds = new Rounds() or die('Create object failed');
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'PreValidate', true /*AllowNoRound*/))
			return false;
		//------------------
		if ($this->round !== false)
		{
			//------------------
			$roundFlags = $this->round->flags;
			//------------------
			$lastRoundIdx = $Rounds->getLastRoundIndex(false /*onlyPaid*/, $this->round->id /*beforeIdx*/);
			if ($lastRoundIdx === false)
			{
				XLogError('Monitor::PreValidate Rounds getLastRoundIndex failed');
				return false;
			}
			//------------------
			if ( ($lastRoundIdx === -1 /*none*/ && $this->round->id != 1) || ($lastRoundIdx !== -1 /*none*/ && ($lastRoundIdx + 1) != $this->round->id) )
				if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'Round Index', MONEVENT_DETAIL_VALUE => $lastRoundIdx, MONEVENT_DETAIL_NEW_VALUE => $this->round->id)))
				{
					XLogError('Monitor::PreValidate addEvent (round index) failed');
					return false;
				}
			//------------------
		}
		else // if ($this->round !== false)
		{
			//------------------
			$lastRoundIdx = $Rounds->getLastRoundIndex();
			if ($lastRoundIdx === false)
			{
				XLogError('Monitor::PreValidate Rounds getLastRoundIndex failed');
				return false;
			}
			//------------------
			$nextRoundIdx = $Rounds->getAutoIncrement();
			if ($nextRoundIdx === false)
			{
				XLogError('Monitor::PreValidate Rounds getAutoIncrement failed');
				return false;
			}
			//------------------
			$roundFlags =  $Config->Get(CFG_ROUND_FLAGS, DEFAULT_ROUND_FLAGS);
			if ($roundFlags === false)
			{
				XLogError('Monitor::PreValidate Config Get round flags failed');
				return false;
			}
			//------------------
			if ( ($lastRoundIdx == -1 /*none*/ && $nextRoundIdx != 1) || ($lastRoundIdx == -1 /*none*/ && ($lastRoundIdx + 1) != $nextRoundIdx) )
				if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'Next Round Index', MONEVENT_DETAIL_VALUE => $lastRoundIdx, MONEVENT_DETAIL_NEW_VALUE => $nextRoundIdx)))
				{
					XLogError('Monitor::PreValidate addEvent (next round index) failed');
					return false;
				}
			//------------------
		} // else // if ($round !== false)
		//------------------
		$flagStoreValues = XMaskContains($roundFlags, ROUND_FLAG_STORE_VALUES);
		$flagHidden = XMaskContains($roundFlags, ROUND_FLAG_HIDDEN);
		$flagDryRun = XMaskContains($roundFlags, ROUND_FLAG_DRYRUN);
		$flagTestBalance = XMaskContains($roundFlags, ROUND_FLAG_TEST_BALANCE);
		//------------------
		$mainAddress = $Wallet->getMainAddress();
		$storeAddress = $Wallet->getStoreAddress();
		//------------------
		if ($mainAddress == '' || !$Wallet->isValidAddressQuick($mainAddress))
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'MainAddress', MONEVENT_DETAIL_VALUE => $mainAddress))))
			{
				XLogError('Monitor::PreValidate addEvent (mainAddress) failed');
				return false;
			}
		if ($flagStoreValues && ($storeAddress == '' || !$Wallet->isValidAddressQuick($storeAddress)))
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'StoreAddress', MONEVENT_DETAIL_VALUE => $storeAddress, MONEVENT_DETAIL_NOTE => ($this->round !== false ? '' : 'Default ').'Round flag store values is enabled'))))
			{
				XLogError('Monitor::PreValidate addEvent (storeAddress) failed');
				return false;
			}
		//------------------
		if ($flagTestBalance)
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => ($this->round !== false ? '' : 'Default ').'Round Flags', MONEVENT_DETAIL_VALUE => 'Test Balance Enabled', MONEVENT_DETAIL_NOTE => ($this->round !== false ? 'Round flags '.$this->round->flags.' totalPay '.$this->round->totalPay : CFG_ROUND_FLAGS)))))
			{
				XLogError('Monitor::PreValidate addEvent (test balance) failed');
				return false;
			}
		//------------------
		if ($flagDryRun)
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => ($this->round !== false ? '' : 'Default ').'Round Flags', MONEVENT_DETAIL_VALUE => 'Dry Run Enabled', MONEVENT_DETAIL_NOTE => ($this->round !== false ? 'Round flags '.$this->round->flags : CFG_ROUND_FLAGS)))))
			{
				XLogError('Monitor::PreValidate addEvent (dry run) failed');
				return false;
			}
		//------------------
		$mainContFound = false;
		$ContList = $Contributions->findRoundContributions(-1 /*(new) ridx*/,  false /*specific number*/, false /*only specialMode*/);
		foreach ($ContList as $cont)
			if ($cont->address == $mainAddress)
			{
				$mainContFound = true;
				break;
			}
		//------------------
		if (!$mainContFound)
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'Contributions', MONEVENT_DETAIL_VALUE => 'Main Contribution Missing'))))
			{
				XLogError('Monitor::PreValidate addEvent (main contrib) failed');
				return false;
			}
		//------------------
		$useFahAppProxy = (defined('FOLD_API_PROXY') && defined('FOLD_API_PROXY_KEY') && FOLD_API_PROXY !== false ? true : false);
		if (FAH_APP_TEST !== false || ($useFahAppProxy && FAH_APP_PROXY_TEST === true))
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PreValidate', MONEVENT_DETAIL_FIELD => 'FahAppClient', MONEVENT_DETAIL_VALUE => 'Test mode activated', MONEVENT_DETAIL_NOTE => 'Folding stats are set to test mode with FAH_APP_TEST or FAH_APP_PROXY_TEST'))))
			{
				XLogError('Monitor::PreValidate addEvent (FahApp test mode) failed');
				return false;
			}
		//------------------
		return true;
	}
	//------------------
	function PostNewDataAddValidate($round)
	{
		//------------------
		$NewRoundData = new NewRoundData() or die('Create object failed');
		$Workers = new Workers() or die('Create object failed');
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'PostNewDataAddValidate'))
			return false;
		//------------------
		$timer = new XTimer();
		//------------------
		$duplicateWorkers = $NewRoundData->getDuplicateWorkers();
		//------------------
		if ($duplicateWorkers === false)
		{
			XLogError('Monitor::PostNewDataAddValidate NewRoundData getDuplicateWorkers failed');
			return false;
		}
		//------------------
		XLogDebug('Monitor::PostNewDataAddValidate NewRoundData getDuplicateWorkers took '.$timer->restartMs(true));
		//------------------
		if (sizeof($duplicateWorkers) != 0)
		{
			//------------------
			XLogError('Monitor::PostNewDataAddValidate '.sizeof($duplicateWorkers).' duplicated workers found in new round data:');
			//------------------
			foreach ($duplicateWorkers as $id => $data)
				XLogError("\t[$id] Worker ID ".($data[0] === null ? '(null)' : $data[0]).", Worker Name ".($data[1] === null ? '(null)' : "'".$data[1]."'").", wID Dupes ".$data[2].", wName Dupes ".$data[3]);
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostNewDataAddValidate', MONEVENT_DETAIL_FIELD => 'Duplicate Workers', MONEVENT_DETAIL_COUNT => sizeof($duplicateWorkers)))));
			{
				XLogError('Monitor::PostNewDataAddValidate addEvent (duplicate workers) failed');
				return false;
			}
			//------------------
		}
		//------------------
		$newDataCount = $NewRoundData->getEntryCount();
		if ($newDataCount === false)
		{
			XLogError('Monitor::PostNewDataAddValidate NewRoundData getEntryCount failed');
			return false;
		}
		//------------------
		$workerCount = $Workers->GetWorkerCount();
		if ($newDataCount === false)
		{
			XLogError('Monitor::PostNewDataAddValidate Workers GetWorkerCount failed');
			return false;
		}
		//------------------
		if ($newDataCount < $workerCount)
		{
			if (!$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostNewDataAddValidate', MONEVENT_DETAIL_FIELD => 'NewRoundData Count', MONEVENT_DETAIL_VALUE => $workerCount, MONEVENT_DETAIL_NEW_VALUE => $newDataCount, MONEVENT_DETAIL_NOTE => 'Less NewRoundData entries than worker count'))))
			{
				XLogError('Monitor::PostNewDataAddValidate addEvent (less data than workers) failed');
				return false;
			}
		}
		else
		{
			//------------------
			$diff = $newDataCount - $workerCount;
			//------------------
			if ($workerCount <= 10)
			{
				$warnMax = 10;
				$alarmMax = 20;
			}
			else if ($workerCount <= 50)
			{
				$warnMax = ($workerCount * 0.5);
				$alarmMax = $workerCount;
			}
			else if ($workerCount <= 200)
			{
				$warnMax = ($workerCount * 0.3);
				$alarmMax = ($workerCount * 0.5);
			}
			else if ($workerCount <= 500)
			{
				$warnMax = ($workerCount * 0.2);
				$alarmMax = ($workerCount * 0.4);
			}
			else if ($workerCount <= 2000)
			{
				$warnMax = ($workerCount * 0.1);
				$alarmMax = ($workerCount * 0.25);
			}
			else
			{
				$warnMax = ($workerCount * 0.05);
				$alarmMax = ($workerCount * 0.1);
			}
			//------------------
			if ($diff >= $warnMax)
				if (!$this->addEvent( ($diff >= $alarmMax ? MONEVENT_TYPE_ALARM : MONEVENT_TYPE_WARN), MONEVENT_ISSUE_ROUND_DATA_SANITY, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostNewDataAddValidate', MONEVENT_DETAIL_FIELD => 'New Worker Count', MONEVENT_DETAIL_VALUE => $workerCount, MONEVENT_DETAIL_NEW_VALUE => $newDataCount, MONEVENT_DETAIL_COUNT => $diff, MONEVENT_DETAIL_NOTE => 'Many more NewRoundData entries than worker count (new folders)'))))
				{
					XLogError('Monitor::PostNewDataAddValidate addEvent (many new workers) failed');
					return false;
				}
			//------------------
		}
		//------------------
		return true;
	}
	//------------------
	function PostNewDataAddStatsValidate($round)
	{
		$Stats = new Stats() or die('Create object failed');
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'PostNewDataAddStatsValidate'))
			return false;
		//------------------
		$statDupes = $Stats->findRoundWorkersDuplicates($this->ridx);
		if ($statDupes === false)
		{
			XLogError('Monitor::PostNewDataAddStatsValidate Stats findRoundWorkersDuplicates failed');
			return false;
		}
		//------------------
		if (sizeof($statDupes) != 0 )
		{
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostNewDataAddStatsValidate', MONEVENT_DETAIL_FIELD => 'Stat Duplicates', MONEVENT_DETAIL_COUNT => sizeof($statDupes), MONEVENT_DETAIL_NOTE => 'One or more workers had duplicate stats detected'))))
			XLogError('Monitor::PostNewDataAddStatsValidate addEvent (stat dupes) failed');
			return false;
			//------------------
			$msgList = "Worker - Stat\n--------------\n";
			foreach ($statDupes as $widx => $sidx)
				$msgList .= "\t$widx - $sidx\n";
			XLogWarn("Monitor::PostNewDataAddStatsValidate Round $this->ridx, Worker duplicate Stats found (".sizeof($statDupes)."):\n$msgList");
			//------------------
		}
		//------------------
		// sane stat count paired with valid worker count
		// stats with invalid workers getWorkerIDs($onlyValid = false)
		//------------------
		return true;
	}
	//------------------
	function PostContributionsValidate($round)
	{
		//------------------
		$Payouts = new Payouts() or die('Create object failed');
		$Contributions = new Contributions() or die('Create object failed');
		$Wallet = new Wallet() or die('Create object failed');
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'PostContributionsValidate'))
			return false;
		//------------------
		$mainAddress = $Wallet->getMainAddress();
		$storeAddress = $Wallet->getStoreAddress();
		//------------------
		if ($mainAddress == '' || !$Wallet->isValidAddressQuick($mainAddress))
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'MainAddress', MONEVENT_DETAIL_VALUE => $mainAddress, MONEVENT_DETAIL_NOTE => 'Validate main address failed'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (mainAddress) failed');
				return false;
			}
		//------------------
		$contList = $Contributions->getContributions($this->ridx);
		if ($contList === false)
		{
			XLogError('Monitor::PostContributionsValidate Contributions getContributions failed');
			return false;
		}
		//------------------
		$forfietStoreCount = 0;
		$storepayCount = 0;
		$unknownFromStoreCount = 0;
		$fromMainCount = 0;
		$doneCount = 0;
		$disabledCount = 0;
		$notDoneCount = 0;
		$noOutcomeCount = 0;
		$skippedCount = 0;
		$noFundsCount = 0;
		$failedCount = 0;
		$paidCount = 0;
		$stillWaiting = 0;
		//------------------
		foreach ($contList as $cont)
		{
			if ($cont->isDisabled())
				$disabledCount++;
			if ($cont->isDone())
				$doneCount++;
			else
				$notDoneCount++;
			if ($cont->outcome == CONT_OUTCOME_NONE)
				$noOutcomeCount++;
			if ($cont->outcome == CONT_OUTCOME_PAID)
				$paidCount++;
			if ($cont->outcome == CONT_OUTCOME_NOFUNDS_WAITING || $cont->outcome == CONT_OUTCOME_FAILED_WAITING)
				$stillWaiting++;
			if ($cont->outcome == CONT_OUTCOME_FAILED_WAITING || $cont->outcome == CONT_OUTCOME_FAILED_SKIPPING || $cont->outcome == CONT_OUTCOME_FAILED_WONTFIX || $cont->outcome == CONT_OUTCOME_FAILED_UPDATES)
				$failedCount++;
			if ($cont->outcome == CONT_OUTCOME_NOFUNDS_SKIPPED || $cont->outcome == CONT_OUTCOME_FAILED_SKIPPING || $cont->outcome == CONT_OUTCOME_TOO_LOW_SKIPPED)
				$skippedCount++;
			if ($cont->outcome == CONT_OUTCOME_NOFUNDS_SKIPPED)
				$noFundsCount++;
			if ($mainAddress != '' && $cont->address !== false && $cont->address == $mainAddress)
				$fromMainCount++;
			if ($storeAddress != '' && $cont->address !== false && $cont->address == $storeAddress)
			{
				if ($cont->mode == CONT_MODE_SPECIAL_STORE_PAY)
					$storepayCount++;
				else if ($cont->mode == CONT_MODE_SPECIAL_STORE_FORFEIT)
					$forfietStoreCount++;
				else if ($cont->mode != CONT_MODE_PAY_INACTIVE_STORED)
					$unknownFromStoreCount++;
			}
		}
		//------------------
		if ($notDoneCount != 0 && !$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions not done', MONEVENT_DETAIL_COUNT => $notDoneCount))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (cont not done) failed');
			return false;
		}
		else if ($disabledCount != 0 || $noOutcomeCount != 0)
		{
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions bad state', MONEVENT_DETAIL_COUNT => ($disabledCount + $noOutcomeCount), MONEVENT_DETAIL_NOTE => 'Disabled or no outcome found'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (disabled/no outcome) failed');
				return false;
			}
		}
		//------------------
		if ($fromMainCount != 1)
		{
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions sanity', MONEVENT_DETAIL_COUNT => $fromMainCount, MONEVENT_DETAIL_NOTE => 'Missing or multiple Main address contributions detected'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (main) failed');
				return false;
			}
		}
		if ($forfietStoreCount > 1)
		{
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions sanity', MONEVENT_DETAIL_COUNT => $forfietStoreCount, MONEVENT_DETAIL_NOTE => 'Multiple forfeit contributions detected'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (forfiet) failed');
				return false;
			}
		}
		if ($storepayCount > 1)
		{
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions sanity', MONEVENT_DETAIL_COUNT => $storepayCount, MONEVENT_DETAIL_NOTE => 'Multiple store pay contributions detected'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (storePay) failed');
				return false;
			}
		}
		if ($unknownFromStoreCount > 0)
		{
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions sanity', MONEVENT_DETAIL_COUNT => $unknownFromStoreCount, MONEVENT_DETAIL_NOTE => 'Unknown Store address contributions detected (not forfeit/storePay)'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (unknown store) failed');
				return false;
			}
		}
		//------------------
		if ($failedCount != 0 && !$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions failed', MONEVENT_DETAIL_COUNT => $failedCount, MONEVENT_DETAIL_NOTE => 'One or more contributions failed'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (failed) failed');
			return false;
		}
		//------------------
		if ($stillWaiting != 0 && !$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions waiting', MONEVENT_DETAIL_COUNT => $stillWaiting, MONEVENT_DETAIL_NOTE => 'One or more contributions still waiting'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (still waiting) failed');
			return false;
		}
		//------------------
		if (($paidCount == 0 || $doneCount == 0) && !$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions none complete', MONEVENT_DETAIL_NOTE => 'No paid/done contributions detected'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (none paid) failed');
			return false;
		}
		//------------------
		if ($skippedCount != 0 &&  !$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUND_DATA_INFO, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions skipped', MONEVENT_DETAIL_COUNT => $skippedCount, MONEVENT_DETAIL_NOTE => 'One or more contributions were skipped'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (skipped) failed');
			return false;
		}
		//------------------
		if ($noFundsCount != 0 && !$this->addEvent(MONEVENT_TYPE_WARN, MONEVENT_ISSUE_ROUND_DATA_INFO, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions no funds', MONEVENT_DETAIL_COUNT => $noFundsCount, MONEVENT_DETAIL_NOTE => 'One or more contributions did not have enough funds'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (funds) failed');
			return false;
		}
		//------------------
		if ($paidCount > 0 && !$this->addEvent(MONEVENT_TYPE_NOTIFY, MONEVENT_ISSUE_ROUND_DATA_INFO, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Contributions complete', MONEVENT_DETAIL_COUNT => $paidCount, MONEVENT_DETAIL_NOTE => 'One or more contributions were paid/done'))))
		{
			XLogError('Monitor::PostContributionsValidate addEvent (paid count) failed');
			return false;
		}
		//------------------
		$payoutDupes = $Payouts->findRoundWorkersDuplicates($this->ridx);
		if ($payoutDupes === false)
		{
			XLogError('Monitor::PostContributionsValidate Payouts findRoundWorkersDuplicates failed');
			return false;
		}
		//------------------
		if (sizeof($payoutDupes) != 0)
		{
			//------------------
			if (!$this->addEvent(MONEVENT_TYPE_ALARM, MONEVENT_ISSUE_ROUND_DATA_INVALID, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => 'PostContributionsValidate', MONEVENT_DETAIL_FIELD => 'Payout Duplicates', MONEVENT_DETAIL_COUNT => sizeof($payoutDupes), MONEVENT_DETAIL_NOTE => 'One or more workers had duplicate payouts detected'))))
			{
				XLogError('Monitor::PostContributionsValidate addEvent (payout dupes) failed');
				return false;
			}
			//------------------
			$msgList = "Worker - Payout\n--------------\n";
			foreach ($payoutDupes as $widx => $pidx)
				$msgList .= "\t$widx - $pidx\n";
			XLogWarn("Monitor::PostContributionsValidate Round $this->ridx, Worker duplicate Payouts found (".sizeof($payoutDupes)."):\n$msgList");
			//------------------
		}
		//------------------
		// missing stats/payouts
		// no balance for fee/statPay
		// too low percent of workers paid / pay counts rational
		// fee est too low
		// forfeit rational / warn or confirm
		//------------------
		return true;		
	}
	//------------------
	function checkErrorLog($source = '[no source specified]')
	{
		//------------------
		$errorLogFileName = dirname(__FILE__, 2).'/'.MONITOR_ERROR_LOG_FILE;
		//XLogDebug("Monitor::checkErrorLog checking errorLogFileName: $errorLogFileName");
		//------------------
		if (!@file_exists($errorLogFileName))
			return true; // no error log detected, done
		//------------------
		if (!$this->TestRoundAndTryInit(false /*round*/, 'checkErrorLog', true /*AllowNoRound*/))
			return false;
		//------------------
		$logs = @file_get_contents($errorLogFileName);
		if ($logs === false)
		{
			XLogError('Monitor::checkErrorLog file_get_contents errorLog failed');
			return false;
		}
		//------------------
		$logLines = explode("\n", $logs);
		//------------------
		if (sizeof($logLines) == 0)
		{
			if ($logs !== '')
				if (!$this->addEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_PHP_ERROR, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => $source, MONEVENT_DETAIL_VALUE => str_replace(',', '_', $logs)))))
				{
					XLogError('Monitor::checkErrorLog addEvent (single) failed. Dumping single log data: '.XVarDump($logs));
					return false;
			}
		}
		else
		{
			foreach ($logLines as $line)
				if (trim($line) !== '')
					if (!$this->addEvent(MONEVENT_TYPE_BLOCK, MONEVENT_ISSUE_PHP_ERROR, $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => $source, MONEVENT_DETAIL_VALUE => str_replace(',', '_', $line)))))
					{
						XLogError('Monitor::checkErrorLog addEvent failed. Dumping log data lines: '.XVarDump($logLines).', log data raw: '.XVarDump($logs));
						return false;
					}
		}
		//------------------
		if (!@unlink($errorLogFileName))
		{
			XLogError('Monitor::checkErrorLog unlink errorLog failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function ContributionWaitingForFunds($round, $contId, $value, $source = '[no source specified]', $isAlarm = false, $fullBal = false, $estFee = false, $balLessFee = false, $note = false)
	{
		global $Config;
		//------------------
		$Contributions = new Contributions() or die('Create object failed');
		$Wallet = new Wallet() or die('Create object failed');
		//------------------
		if (!is_numeric($contId) || !is_numeric($value) || ($fullBal !== false && !is_numeric($fullBal)) || ($estFee !== false && !is_numeric($estFee)) || ($balLessFee !== false && !is_numeric($balLessFee)))
		{
			XLogError('Monitor::ContributionWaitingForFunds validate parameters failed');
			return false;
		}
		//------------------
		if (!$this->TestRoundAndTryInit($round, 'PostContributionsValidate'))
			return false;
		//------------------
		$cont = $Contributions->getContribution($contId);
		if ($cont === false)
		{
			XLogError('Monitor::ContributionWaitingForFunds Contributions getContribution failed. id: '.XVarDump($contId));
			$curFullBal = '[Not found]';
			$address = '[Not found]';
		}
		else if (!$Wallet->isValidAddressQuick($cont->address))
		{
			$curFullBal = '[Invalid address]';
			$address = $cont->address.'[Invalid address]';
		}
		else
		{
			$address = $cont->address;
			$curFullBal = $cont->getBalance(0 /*ignore fee*/); // returns 0 on invalid address
			if ($curFullBal === false)
				$curFullBal = '[getBalance failed]';
		}
		//------------------
		if ($estFee === false)
			$estFee = $Config->Get(CFG_WALLET_EST_CONT_FEE, false);
		//------------------
		if ($estFee === false || !is_numeric($estFee))
		{
			$estFee = '[Not set]';
			$curBal = '[Est. Fee Unknown]';
		}
		else if (!is_numeric($curFullBal))
			$curBal = '[Balance Unknown]';
		else
			$curBal = bcsub($curFullBal, $estFee, 8);
		//------------------
		$mainAddress = $Wallet->getMainAddress();
		$storeAddress = $Wallet->getStoreAddress();
		//------------------
		if ($mainAddress !== false && $mainAddress != '' && $cont->address == $mainAddress)
			$address .= '(MainAddress)';
		if ($storeAddress !== false && $storeAddress != '' && $cont->address == $storeAddress)
			$address .= '(StoreAddress)';
		//------------------
		$strCont = "($contId) ".($cont !== false ? (strlen($cont->name) > 10 ? substr($cont->name, 0, 7).'...' : $cont->name) : '[Cont not found]');
		//------------------
		$mainDetails = array(MONEVENT_DETAIL_SOURCE => $source, MONEVENT_DETAIL_VALUE => $value, MONEVENT_DETAIL_FAIL => $strCont, MONEVENT_DETAIL_EST_FEE => $estFee, MONEVENT_DETAIL_CUR_BALANCE => $curFullBal, MONEVENT_DETAIL_CUR_BALANCE_LESS_FEE => $curBal, MONEVENT_DETAIL_ADDRESS_SRC => $address);
		if ($fullBal !== false)
			$mainDetails[MONEVENT_DETAIL_BALANCE] = $fullBal;
		if ($balLessFee !== false)
			$mainDetails[MONEVENT_DETAIL_BALANCE_LESS_FEE] = $balLessFee;
		//------------------
		$details =  $this->roundStepDetails($mainDetails, true /*includeProgress*/);
		$matchDetails =  $this->roundStepDetails($mainDetails, false /*includeProgress*/); // don't include progress, such as same progress count
		if ($note !== false)
			$details[MONEVENT_DETAIL_NOTE] = $note;
		//------------------
		if (!$this->addUpdateEvent( ($isAlarm ? MONEVENT_TYPE_ALARM : MONEVENT_TYPE_WARN), MONEVENT_ISSUE_CONT_WAIT_FUNDS, $details, $matchDetails))
		{
			XLogError('Monitor::ContributionWaitingForFunds addUpdateEvent (cont wait funds) failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function TransactionFailed($txResult, $txLastResult = false, $source = '[no source specified]', $isAlarm = true)
	{
		//------------------
		if (!is_array($txResult) || ($txLastResult !== false && !is_array($txLastResult)))
		{
			XLogError('Monitor::TransactionFailed validate parameters failed');
			return false;
		}
		//------------------
		if (!$this->TestRoundAndTryInit(false /*round*/, 'TransactionFailed', true /*AllowNoRound*/))
		{
			XLogError('Monitor::TransactionFailed TestRoundAndTryInit failed');
			return false;
		}
		//------------------
		$wError = XArray($txResult, 'error', false);
		$wAction = XArray($txResult, 'action', false);
		$wTestOnly = XArray($txResult, 'testOnly', false);
		$wIn = XArray($txResult, 'totalIn', false);
		$wOut = XArray($txResult, 'totalOut', false);
		$wFee = XArray($txResult, 'fee', false);
		$wChange = XArray($txResult, 'change', false);
		$wDepth = XArray($txResult, 'depth', false);
		$wSize = XArray($txResult, 'rawSize', false);
		//------------------
		$lastNote = '';
		if ($txLastResult !== false && ($wIn === false || $wOut === false || $wFee === false || $wChange === false || $wSize === false))
		{
			$lAction = XArray($txLastResult, 'action', '[not set]');
			$lDepth = XArray($txLastResult, 'depth', false);
			$lIn = XArray($txLastResult, 'totalIn', false);
			$lOut = XArray($txLastResult, 'totalIn', false);
			$lOldFee = XArray($txLastResult, 'oldFee', false);
			$lFee = XArray($txLastResult, 'fee', false);
			$lOldChange  = XArray($txLastResult, 'oldChange', false);
			$lChange = XArray($txLastResult, 'change', false);
			$lSize = XArray($txLastResult, 'rawSize', false);
			$lFeeCalc = XArray($txLastResult, 'kbFeeCalc', false);
			$lminFee = XArray($txLastResult, 'minFee', false);
			$modified = false;
			if ($wIn === false && $lIn !== false)
				{$wIn = "[$lIn]"; $modified = true;}
			if ($wOut === false && $lOut !== false)
				{$wOut = "[$lOut]"; $modified = true;}
			if ($wFee === false && $lFee !== false)
				{$wFee = "[$lFee]"; $modified = true;}
			if ($wChange === false && $lChange !== false)
				{$wChange = "[$lChange]"; $modified = true;}
			if ($wSize === false && $lSize !== false)
				{$wSize = "[$lSize]"; $modified = true;}
			if ($lOldChange !== false)
				{$wChange .= "(old $lOldChange)"; $modified = true;}
			if ($lOldFee !== false || $lFeeCalc !== false || $lminFee !== false)
			{
				$moreFee = '';
				if ($lOldFee !== false)
					$moreFee .= "old $lOldFee";
				if ($lFeeCalc !== false)
					$moreFee .= ($moreFee != '' ? ' ' : '')."by size $lFeeCalc";
				if ($lminFee !== false)
					$moreFee .= ($moreFee != '' ? ' ' : '')."min $lminFee";
				if ($moreFee != '')
					{$wFee .= "($moreFee)"; $modified = true;}
			}		
			if ($modified)		
				$lastNote = " (prev $lAction)";
		}
		//------------------
		if ($wError === false) $wError = '[error not set]';
		if ($wIn === false) $wIn = '*';
		if ($wOut === false) $wOut = '*';
		if ($wFee === false) $wFee = '*';
		if ($wChange === false) $wChange = '*';
		if ($wDepth === false) $wDepth = '*';
		if ($wSize === false) $wSize = '*';
		//------------------
		if ($wAction !== false)
			$wError .= ": $wAction";
		//------------------
		$note = "in/out $wIn/$wOut fee $wFee change $wChange depth $wDepth bytes $wSize$lastNote";
		if ($wTestOnly) $note = '[Test] '.$note;
		//------------------
		$details = $this->roundStepDetails(array(MONEVENT_DETAIL_SOURCE => $source, MONEVENT_DETAIL_FAIL => $wError));
		//------------------
		$matchDetails = XAssArrayClone($details);
		$details[MONEVENT_DETAIL_NOTE] = $note;
		//------------------
		if (!$this->addUpdateEvent( ($isAlarm ? MONEVENT_TYPE_ALARM : MONEVENT_TYPE_WARN), MONEVENT_ISSUE_TRANSACTION_FAILED, $details, $matchDetails))
		{
			XLogError('Monitor::TransactionFailed addUpdateEvent failed');
			return false;
		}
		//------------------
		return true;
	}	
} // class Monitor
//---------------
?>
