<?php
//-------------------------------------------------------------
/*
*	MarketConfig.php
*
* 
*/
//-------------------------------------------------------------
define('MARKET_CLIENT_ASSET_TYPE_FIAT',				1);
define('MARKET_CLIENT_ASSET_TYPE_CRYPTO',			2);
define('MARKET_CLIENT_ASSET_TYPE_STABLECOIN',		3);
define('MARKET_CLIENT_ASSET_TYPE_GENERIC_TOKEN',	4);
//---------------
define('MARKET_CLIENT_PROVIDER_COINGECKO', 0x20); // 32
//---------------
define('MARKET_CLIENT_ASSET_DOGECOIN',	0x201); // 513
define('MARKET_CLIENT_ASSET_BITCOIN',	0x202); // 514
define('MARKET_CLIENT_ASSET_ETHEREUM',	0x203); // 515
//---------------
define('MARKET_CLIENT_ASSET_USD', 	0x101); // 257
define('MARKET_CLIENT_ASSET_EURO',	0x102); // 258
define('MARKET_CLIENT_ASSET_CAD', 	0x103); // 259
define('MARKET_CLIENT_ASSET_GBP', 	0x104); // 260
define('MARKET_CLIENT_ASSET_AUD', 	0x105); // 261
//---------------
$MarketConfig = array(
	'defaults' => array(
					'asset' => MARKET_CLIENT_ASSET_DOGECOIN,
					'provider' => MARKET_CLIENT_PROVIDER_COINGECKO
					),
	'assets' => array(
			MARKET_CLIENT_ASSET_DOGECOIN => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_CRYPTO,
						'fullname' => 'Dogecoin',
						'unitname' => 'DOGE',
						'symbol' => '&ETH;', // \u{00D0}
						'preferred_decimal' => 4,
						'large_decimal' => 6,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'dogecoin'
								)
							),
			MARKET_CLIENT_ASSET_BITCOIN => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_CRYPTO,
						'fullname' => 'Bitcoin',
						'unitname' => 'BTC',
						'symbol' => '&#8383;', // \u{20BF}
						'preferred_decimal' => 5,
						'large_decimal' => 6,
						'min_decimal' => 0,
						'max_decimal' => 2,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'btc'
								)
							),							
			MARKET_CLIENT_ASSET_ETHEREUM => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_CRYPTO,
						'fullname' => 'Ethereum',
						'unitname' => 'ETH',
						'symbol' => '&Xi;', // \u{0926} (not official)
						'preferred_decimal' => 4,
						'large_decimal' => 6,
						'min_decimal' => 0,
						'max_decimal' => 18,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'eth'
								)
							),
			MARKET_CLIENT_ASSET_USD => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_FIAT,
						'fullname' => 'US Dollar',
						'unitname' => 'USD',
						'symbol' => '$', // \u{0036}
						'preferred_decimal' => 2,
						'large_decimal' => 4,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'usd'
								)
							),
			MARKET_CLIENT_ASSET_EURO => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_FIAT,
						'fullname' => 'Euro',
						'unitname' => 'EUR',
						'symbol' => '&euro;', // \u{8364}
						'preferred_decimal' => 2,
						'large_decimal' => 4,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'eur'
								)
							),
			MARKET_CLIENT_ASSET_CAD => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_FIAT,
						'fullname' => 'Canadian Dollar',
						'unitname' => 'CAD',
						'symbol' => 'CA$', // \u{0036}
						'preferred_decimal' => 2,
						'large_decimal' => 4,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'cad'
								)
							),
			MARKET_CLIENT_ASSET_GBP => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_FIAT,
						'fullname' => 'Pound Sterling',
						'unitname' => 'GBP',
						'symbol' => '&pound;', // \u{0163}
						'preferred_decimal' => 2,
						'large_decimal' => 4,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'gbp'
								)
							),
			MARKET_CLIENT_ASSET_AUD => array(
						'type' => MARKET_CLIENT_ASSET_TYPE_FIAT,
						'fullname' => 'Australian Dollar',
						'unitname' => 'AUD',
						'symbol' => 'AU$', // \u{0036}
						'preferred_decimal' => 2,
						'large_decimal' => 4,
						'min_decimal' => 0,
						'max_decimal' => 8,
						'assetname' => array(
								MARKET_CLIENT_PROVIDER_COINGECKO => 'aud'
								)
							)				
		),
	'providers' => array(
			MARKET_CLIENT_PROVIDER_COINGECKO => array(
						'host' => 'api.coingecko.com',
						'currentvalue' => '/api/v3/simple/price?ids=%ASSET%&vs_currencies=%ASSET_OTHER%',
						'historicvalues' => '/api/v3/coins/%ASSET%/history?date=%DATE%&localization=false',
						'historicvalue_period_sec' => 86400, // one day
						'rate_limit_per_minute' => 40, // 50 per minute but "varies"
						'default_history_assets' => array(
													MARKET_CLIENT_ASSET_USD,
													MARKET_CLIENT_ASSET_BITCOIN,
													MARKET_CLIENT_ASSET_ETHEREUM,
													MARKET_CLIENT_ASSET_EURO,
													MARKET_CLIENT_ASSET_CAD,
													MARKET_CLIENT_ASSET_GBP,
													MARKET_CLIENT_ASSET_AUD
													)

							)
		),
	'asset_values_display_defaults' => array(
											'space' => '', // optionally change to &nbsp; for html
											'result_empty_value' => array(),
											'result_associate' => 'none', // 'asset', 'unit', 'name'
											'value_symbol' => false, // true, false, 'crypto', 'fiat'
											'value_unit' => true, // true, false, 'crypto', 'fiat'
											'value_trim' => 'preferred', // 'none', 'preferred', 'large', 'max', 'min' (usually zero)
											'unknown_asset' => 'ignore', // 'ignore', 'keep', 'fail'
											'default_asset_config' => array(
														'type' => MARKET_CLIENT_ASSET_TYPE_CRYPTO,
														'fullname' => 'Unknown', // required if kept (no blank)
														'unitname' => 'Unknown', // required if kept (no blank)
														'symbol' => '', // '' (blank allowed)
														'preferred_decimal' => 2,  // false/'none' or numeric decimal places
														'large_decimal' => 4,  // false/'none' or numeric decimal places
														'min_decimal' => 0,  // false/'none' or numeric decimal places
														'max_decimal' => 8,  // false/'none' or numeric decimal places
													)
		)
	);
//---------------
?>
