<?php
//-------------------------------------------------------------
/*
*	MarketHistory.php
*
* 
*/
//-------------------------------------------------------------
define('MARKET_RATE_LIMIT_DELAY_SEC', 600); // 10 min
define('DEF_CFG_MARKET_POLL_MAX_COUNT', 20);
define('DEF_CFG_MARKET_POLL_MAX_AGE_SEC', 120);
//-------------------------------------------------------------
define('CFG_MARKET_RATE_LIMITED', 'market_rate_limited');
define('CFG_MARKET_POLL_COUNT', 'market_poll_count');
define('CFG_MARKET_POLL_LAST_DATE', 'market_poll_last_date');
define('CFG_MARKET_POLL_MAX_COUNT', 'market_poll_max_count');
define('CFG_MARKET_POLL_MAX_AGE_SEC', 'market_poll_max_age_sec');
define('CFG_MARKET_ASSET', 'market_asset');
define('CFG_MARKET_PROVIDER', 'market_provider');
define('CFG_MARKET_HISTORY_ASSETS', 'market_history_assets');
//-------------------------------------------------------------
class MarketHistory
{
	var $defaultProvider = false;
	var $defaultAsset = false;
	//------------------
	function __construct($defaultProvider = false, $defaultAsset = false)
	{
		global $Config, $MarketConfig;
		//------------------
		if ($this->defaultProvider === false)
		{
			$this->defaultProvider = $Config->GetSetDefault(CFG_MARKET_PROVIDER, $MarketConfig['defaults']['provider']);
			if ($this->defaultProvider === false)
			{
				XLogError('MarketHistory::construct Config GetSetDefault provider failed');
				$this->defaultProvider = false;
			}
		}
		//------------------
		if ($this->defaultAsset === false)
		{
			$this->defaultAsset = $Config->GetSetDefault(CFG_MARKET_ASSET, $MarketConfig['defaults']['asset']);
			if ($this->defaultAsset === false)
			{
				XLogError('MarketHistory::construct Config GetSetDefault asset failed');
				$this->defaultAsset = false;
			}
		}
		//------------------
	}
	//------------------
	function getAssetNameIdList($provider = false)
	{
		global $MarketConfig;
		//------------------
		if ($provider === false)
			$provider = $this->defaultProvider;
		//------------------
		if ($provider === false || !isset($MarketConfig['assets']))
			return array();
		//------------------
		$nameIdList = array();
		foreach ($MarketConfig['assets'] as $id => $assetData)
			if (isset($MarketConfig['assets'][$id]['assetname']) && isset($MarketConfig['assets'][$id]['assetname'][$provider]))
				$nameIdList[$MarketConfig['assets'][$id]['assetname'][$provider]] = $id;
		//------------------
		return $nameIdList;
	}
	//------------------
	function getAssetDetails($onlyAssets = false /*array assetIDs/single id/false all*/, $index = 'none' /* none,asset,name,unit*/, $typeCode = false, $fullName = false, $unitName = false, $symbol = false, $decimalSizes = false, $providerNames = false)
	{
		global $MarketConfig;
		//------------------
		$result = array();
		foreach ($MarketConfig['assets'] as $id => $assetData)
			if ($onlyAssets === false || (is_numeric($onlyAssets) && $onlyAssets == $id) || (is_array($onlyAssets) && in_array($id, $onlyAssets)))
			{
				$data = array();
				if ($typeCode)
					$data['type'] = $assetData['type'];
				if ($fullName)
					$data['fullname'] = $assetData['fullname'];
				if ($unitName)
					$data['unitname'] = $assetData['unitname'];
				if ($symbol)
					$data['symbol'] = $assetData['symbol'];
				if ($decimalSizes)
				{
					$data['preferred_decimal'] = $assetData['preferred_decimal'];
					$data['large_decimal'] = $assetData['large_decimal'];
					$data['min_decimal'] = $assetData['min_decimal'];
					$data['max_decimal'] = $assetData['max_decimal'];
				}
				if ($providerNames)
					$data['assetname'] = $assetData['assetname'];
				if ($index == 'asset' || $index == 'id')
					$result[$id] = $data;
				else if ($index == 'name')
					$result[$assetData['fullname']] = $data;
				else if ($index == 'unit')
					$result[$assetData['unitname']] = $data;
				else
					$result[] = $data;
			}
		//------------------
		return $result;
	}
	//------------------
	function getHistoryAssets($provider = false, $setDefaultIfEmpty = true)
	{
		global $Config, $MarketConfig;
		//------------------
		$strHistAssets = $Config->Get(CFG_MARKET_HISTORY_ASSETS, 0 /*defaultValue*/);
		if ($strHistAssets === false)
		{
			XLogError('MarketHistory::getHistoryAssets Config Get failed');
			return false;			
		}
		//------------------
		$histAssetArrayHex = ($strHistAssets !== 0 /*not set*/ || $strHistAssets != '' ? explode(',', $strHistAssets) : array());
		//------------------
		if (sizeof($histAssetArrayHex) == 0 /* wasn't set or was empty*/)
		{
			//------------------
			if ($provider === false)
				$provider = $this->defaultProvider;
			//------------------
			if ($provider === false)
			{
				XLogError('MarketHistory::getHistoryAssets not set, could not determine provider to get default, failed');
				return false;			
			}
			//------------------
			 if (!isset($MarketConfig['providers'][$provider]) || !isset($MarketConfig['providers'][$provider]['default_history_assets']))
			 {
				XLogError('MarketHistory::getHistoryAssets not set, validate MarketConfig for provider and its default_history_assets exist failed. determined provider '.XVarDump($provider));
				return false;			
			 }
			//------------------
			$histAssetsArrayDec = $MarketConfig['providers'][$provider]['default_history_assets'];
			if (!is_array($histAssetsArrayDec) || sizeof($histAssetsArrayDec) == 0)
			{
				XLogError('MarketHistory::getHistoryAssets not set, validate providers default_history_assets MarketConfig is valid failed. determined provider '.XVarDump($provider).', histAssetsArrayDec '.XVarDump($histAssetsArrayDec));
				return false;			
			}
			//------------------
			if ($setDefaultIfEmpty)
			{
				//------------------
				$histAssetsArrayHex = array();
				foreach ($histAssetsArrayDec as $histAssetDec)
					$histAssetsArrayHex[] = dechex($histAssetDec);
				//------------------
				$strHistAssets = implode(',', $histAssetsArrayHex);
				//------------------
				if (!$Config->Set(CFG_MARKET_HISTORY_ASSETS, $strHistAssets))
				{
					XLogError('MarketHistory::getHistoryAssets not set, Config Set history assets from default failed');
					return false;			
				}
				//------------------
			}
			//------------------
			return $histAssetsArrayDec;
		}
		//------------------
		$histAssetsArrayDec = array();
		foreach ($histAssetArrayHex as $histAssetHex)
			$histAssetsArrayDec[] = hexdec($histAssetHex);
		//------------------
		return $histAssetsArrayDec;
	}
	//------------------
	function getAssestValuesForDisplay($assetValuesArray, $DisplayOptionsArray = array(), $valueBad = '?', $invalidArrayValue = false, $invalidArrayEntryValue = false, $unknownAssetFailValue = false)
	{
		global $MarketConfig;
		//------------------
		if (!is_array($assetValuesArray))
		{
			XLogError('MarketHistory::getAssestValuesForDisplay validate assetValuesArray is array failed. assetValuesArray: '.XVarDump($assetValuesArray));
			return $invalidArrayValue;
		}
		//------------------
		$options = XAssArrayClone($MarketConfig['asset_values_display_defaults']);
		$options = array_replace($options, $DisplayOptionsArray); 
		//------------------
		$result = array();
		foreach ($assetValuesArray as $key => $value)
		{
			//------------------
			if (!is_numeric($key) || !is_numeric($value))
			{
				XLogError('MarketHistory::getAssestValuesForDisplay validate array entry failed. assetValuesArray: '.XVarDump($assetValuesArray)); 
				return $invalidArrayEntryValue;
			}
			//------------------
			if (isset($MarketConfig['assets'][$key]))
				$activeConfig = $MarketConfig['assets'][$key];
			else
			{
				if ($options['unknown_asset'] === 'fail')
				{
					XLogError('MarketHistory::getAssestValuesForDisplay array entry unknown asset '.XVarDump($key).'. assetValuesArray: '.XVarDump($assetValuesArray)); 
					return $unknownAssetFailValue;
				}
				else if ($options['unknown_asset'] === 'ignore')
					continue;
				// keep
				$activeConfig = $options['default_asset_config'];
			}
			//------------------
			if ($options['value_trim'] === 'preferred')
				$trimVal = $activeConfig['preferred_decimal'];
			else if  ($options['value_trim'] === 'large')
				$trimVal = $activeConfig['large_decimal'];
			else if  ($options['value_trim'] === 'min')
				$trimVal = $activeConfig['min_decimal'];
			else if  ($options['value_trim'] === 'max')
				$trimVal = $activeConfig['max_decimal'];
			else
				$trimVal = false; // none
			//------------------
			if ($activeConfig['symbol'] !== '' &&
					($options['value_symbol'] === true ||
					($options['value_symbol'] == 'crypto' && $activeConfig['type'] == MARKET_CLIENT_ASSET_TYPE_CRYPTO) || 
					($options['value_symbol'] == 'fiat'   && $activeConfig['type'] == MARKET_CLIENT_ASSET_TYPE_FIAT)) )
				$txtValue = $activeConfig['symbol'].$options['space'];
			else
				$txtValue = '';
			//------------------
			if (!is_numeric($value))
				$txtValue .= $valueBad;
			else if (is_numeric($trimVal) /*ie not false 'none'*/)
				$txtValue .= number_format($value, $trimVal, '.', ''); // @TODO add thousands comma option
			else
				$txtValue .= $value;
			//------------------
			if ($activeConfig['unitname'] !== '' &&
					($options['value_unit'] === true ||
					($options['value_unit'] == 'crypto' && $activeConfig['type'] == MARKET_CLIENT_ASSET_TYPE_CRYPTO) || 
					($options['value_unit'] == 'fiat'   && $activeConfig['type'] == MARKET_CLIENT_ASSET_TYPE_FIAT)) )
				$txtValue .= $options['space'].$activeConfig['unitname'];
			//------------------
			if ($options['result_associate'] == 'asset' || $options['result_associate'] == 'id')
					$result[$key] = $txtValue;
			else if ($options['result_associate'] == 'unit')
				$result[$activeConfig['unitname']] = $txtValue;
			else if ($options['result_associate'] == 'name')
				$result[$activeConfig['fullname']] = $txtValue;
			else // non associative array
				$result[] = $txtValue;
			//------------------
		} // foreach 
		//------------------
		if (sizeof($result) == 0)
			return $options['result_empty_value'];
		//------------------
		return $result;
	}
	//------------------
	function getHistory($date, $pollIfNeeded = true, $provider = false, $asset = false, $notFoundValue = 0, $clientPollFailValue = 0, $pollLocked = 0, $justPolled = false)
	{
		//------------------
		if ($provider === false)
			$provider = $this->defaultProvider;
		if ($asset === false)
			$asset = $this->defaultAsset;
		//------------------
		$result = $this->getSnapshot($date, $provider, $asset);
		if ($result === false || !is_array($result) || !isset($result['found']))
		{
			XLogError('MarketHistory::getHistory getSnapshot failed. result: '.XVarDump($result));
			return false;
		}
		//------------------
		if (!$result['found'])
		{
			//------------------
			if (!$pollIfNeeded)
			{
				//XLogDebug('MarketHistory::getHistory getSnapshot returned not found, pollIfNeeded is not set. Returning notFoundValue: '.XVarDump($notFoundValue).', getSnapshot result: '.XVarDump($result));
				return $notFoundValue;
			}
			//------------------
			$result = $this->pollHistory($date, $provider, $asset, true /*noTestNeeded*/, 0 /*clientFailValue*/, -1 /*pollLocked*/);
			if ($result === false)
			{
				XLogError('MarketHistory::getHistory pollHistory failed.');
				return false;
			}
			//------------------
			if ($result === 0)
			{
				XLogDebug('MarketHistory::getHistory pollHistory returned clientFailValue. Returning clientPollFailValue: '.XVarDump($clientPollFailValue));
				return $clientPollFailValue;
			}
			//------------------
			if ($result === -1)
				return $pollLocked;
			//------------------
			XLogDebug('MarketHistory::getHistory  poll done, calling getHistory recursively');
			//------------------
			return $this->getHistory($date, false /*pollIfNeeded*/, $provider, $asset, $notFoundValue, $clientPollFailValue, $pollLocked, true /*justPolled*/); 
		}
		//------------------
		$result['just_polled'] = $justPolled;
		//------------------
		//XLogDebug('MarketHistory::getHistory  returning result: '.XVarDump($result));
		//------------------
		return $result;
	}
	//------------------
	function referencePollLock($rateLimitedReplyValue = 0)
	{
		global $Config;
		//------------------
		$rateLimited = $Config->Get(CFG_MARKET_RATE_LIMITED, '');
		if ($rateLimited === false)
		{
			XLogError('MarketHistory::referencePollLock Config Get failed, could not check if rateLimited');
			return false;
		}
		//------------------
		if (is_numeric($rateLimited))
		{
			$tsDateLimited = XMixedToDate($rateLimited);
			if (is_numeric($tsDateLimited) && (time() - $tsDateLimited) < MARKET_RATE_LIMIT_DELAY_SEC)
			{
				XLogWarn('MarketHistory::referencePollLock rate limit active. Cannot poll until it expires: '.(time() - $tsDateLimited).'/'.MARKET_RATE_LIMIT_DELAY_SEC.' secs');
				return $rateLimitedReplyValue;
			}
			if (!$Config->Clear(CFG_MARKET_RATE_LIMITED))
			{
				XLogError('MarketHistory::referencePollLock rate limit expired, but clearing it failed. Config Clear failed.');
				return false;
			}
		}
		//------------------
		$pollCount = $Config->GetSetDefault(CFG_MARKET_POLL_COUNT, 0);
		$pollLastDate = $Config->GetSetDefault(CFG_MARKET_POLL_LAST_DATE, 0);
		$pollMaxCount = $Config->GetSetDefault(CFG_MARKET_POLL_MAX_COUNT, DEF_CFG_MARKET_POLL_MAX_COUNT);
		$pollMaxAge = $Config->GetSetDefault(CFG_MARKET_POLL_MAX_AGE_SEC, DEF_CFG_MARKET_POLL_MAX_AGE_SEC);
		//------------------
		if ($pollCount === false || $pollLastDate === false || $pollMaxCount === false || $pollMaxAge === false)
		{
			XLogError('MarketHistory::referencePollLock Config GetSetDefault poll values and limits failed');
			return false;
		}
		//------------------
		if (!is_numeric($pollLastDate))
			$pollLastDate = 0;
		//------------------
		if (!is_numeric($pollCount))
			$pollCount = 0;
		//------------------
		if ($pollLastDate == 0 || (time() - $pollLastDate) > $pollMaxAge)
			$pollCount = 0;
		//------------------
		if ($pollCount >= $pollMaxCount)
			return $rateLimitedReplyValue;
		//------------------
		$pollCount++;
		//------------------
		$tsNow = time();
		if (!$Config->SetMany(array(CFG_MARKET_POLL_COUNT => $pollCount, CFG_MARKET_POLL_LAST_DATE => $tsNow)))
		{
			XLogError('MarketHistory::referencePollLock Config SetMany poll reference update failed');
			return false;
		}
		//------------------
		return true;
	}

	function pollHistory($date, $provider = false, $asset = false, $noTestNeeded = false, $clientFailValue = false, $pollLocked = false)
	{
		global $Config;
		//------------------
		if ($provider === false)
			$provider = $this->defaultProvider;
		if ($asset === false)
			$asset = $this->defaultAsset;
		//------------------
		if (!$noTestNeeded)
		{
			//------------------
			$result = $this->hasSnapshot($date, $provider, $asset);
			//------------------
			if ($result === false)
			{
				XLogError('MarketHistory::pollHistory hasSnapshot failed');
				return false;
			}
			//------------------
			if ($result === true)
				return true;
			//------------------
		}
		//------------------
		$ref = $this->referencePollLock();
		if ($ref === false)
		{
			XLogError('MarketHistory::pollHistory referencePollLock failed');
			return false;
		}
		//------------------
		if ($ref === 0)
		{
			//XLogDebug('MarketHistory::pollHistory referencePollLock has limited polling');
			return $pollLocked;
		}
		//------------------
		$MarketClient = new MarketClient($provider) or die('Create object failed');
		//------------------
		$result = $MarketClient->getHistoricValues($asset, $date, $provider);
		if ($result === false)
		{
			XLogError('MarketHistory::pollHistory MarketClient getHistoricValues failed');
			return $clientFailValue;
		}
		//------------------
		if (!is_array($result) || !isset($result['ok']) || !isset($result['asset']) || $result['asset'] != $asset || !isset($result['date_start'])  || !isset($result['date_end']) || !isset($result['asset_values']))
		{
			XLogError('MarketHistory::pollHistory validate MarketClient getHistoricValues result failed: result '.XVarDump($result));
			return false;
		}
		//------------------
		if (!$result['ok'])
		{
			if (isset($result['error']) && $result['error'] == 'request' && isset($result['httpCode']) && $result['httpCode'] == 429 /*too many requests*/)
			{
				//@todo handle rate limiting
			}
			else if (isset($result['error']) && $result['error'] == 'session rate limit')
				XLogDebug('MarketHistory::pollHistory MarketClient getHistoricValues refused due to the session rate limit: result '.XVarDump($result));
			else
				XLogWarn('MarketHistory::pollHistory MarketClient getHistoricValues returned failure: result '.XVarDump($result));
			return $clientFailValue;
		}
		//------------------
		$histAssets = $this->getHistoryAssets($provider);
		if ($histAssets === false)
		{
			XLogError('MarketHistory::pollHistory getHistoryAssets failed');
			return false;
		}
		//------------------
		$assetValues = array();
		foreach ($result['asset_values'] as $otherAsset => $value)
			if (in_array($otherAsset, $histAssets))
				$assetValues[$otherAsset] = $value; // value can be NULL
		//------------------
		//XLogDebug('MarketHistory::pollHistory MarketClient getHistoricValues result: '.XVarDump($result).', histAssets '.XVarDump($histAssets).', assetValues '.XVarDump($assetValues));
		//------------------
		if (sizeof($assetValues) == 0)
		{
			XLogError('MarketHistory::pollHistory no history assets found in result, returning clientFailed');
			return $clientFailValue;
		}
		//------------------
		if (!$this->addSnapshot($result['date_start'], $result['date_end'], time() /*datePolled*/, $provider, $asset, $assetValues))
		{
			XLogError('MarketHistory::pollHistory addSnapshot failed');
			return $clientFailValue;
		}
		//------------------
		return true;
	}
	//------------------
	function addSnapshot($dateStart, $dateEnd, $datePolled, $provider, $asset, $assetValues)
	{
		global $db, $dbMarketSnapshotFields, $dbMarketDataFields;
		//------------------
		$tsDateStart = XMixedToTimestamp($dateStart);
		$tsDateEnd = XMixedToTimestamp($dateEnd);
		$tsPolled = XMixedToTimestamp($datePolled);
		//------------------
		if ($tsDateStart === false || $tsDateEnd === false || $tsPolled === false || !is_numeric($provider) || !is_numeric($asset) || !is_array($assetValues))
		{
			XLogError('MarketHistory::addSnapshot validate parameters failed');
			return false;
		}
		//------------------
		$dbMarketSnapshotFields->ClearValues();
		$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_DATE_START, $tsDateStart);
		$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_DATE_END, $tsDateEnd);
		$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_POLL_DATE, $tsPolled);
		$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_PROVIDER, $provider);
		$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_ASSET, $asset);
		//------------------
		$sql = $dbMarketSnapshotFields->scriptInsert();
		//------------------
		if (!$db->beginTransaction())
		{
			XLogError('MarketHistory::addSnapshot db beginTransaction failed');
			return false;
		}
		//------------------
		if (!$db->Execute($sql))
		{
			XLogError('MarketHistory::addSnapshot db Execute scriptInsert failed.'."\nsql: $sql");
			return false;
		}
		//------------------
		$snapshotId = $db->lastInsertId();
		//------------------
		if (!is_numeric($snapshotId))
		{
			XLogError('MarketHistory::addSnapshot db lastInsertId failed. result '.XVarDump($snapshotId));
			if (!$db->rollBack())
				XLogError('MarketHistory::addSnapshot db rollBack failed');
			return false;
		}
		//------------------
		foreach ($assetValues as $otherAsset => $value)
		{
			//------------------
			if (!is_numeric($otherAsset) || (!is_numeric($value) && $value !== NULL)) // values can be null
			{
				XLogError('MarketHistory::addSnapshot validate assetValues failed: '.XVarDump($assetValues));
				if (!$db->rollBack())
					XLogError('MarketHistory::addSnapshot db rollBack failed');
				return false;
			}
			//------------------
			$dbMarketDataFields->ClearValues();
			$dbMarketDataFields->SetValue(DB_MARKETDATA_SNAPSHOT, $snapshotId);
			$dbMarketDataFields->SetValue(DB_MARKETDATA_OTHER_ASSET, $otherAsset);
			$dbMarketDataFields->SetValue(DB_MARKETDATA_VALUE, $value);
			//------------------
			$sql = $dbMarketDataFields->scriptInsert();
			//------------------
			if (!$db->Execute($sql))
			{
				XLogError('MarketHistory::addSnapshot db Execute scriptInsert data failed.'."\nsql: $sql");
				if (!$db->rollBack())
					XLogError('MarketHistory::addSnapshot db rollBack failed');
				return false;
			}
			//------------------
		}
		//------------------
		if (!$db->commit())
		{
			XLogError('MarketHistory::addSnapshot db commit failed');
			return false;
		}
		//------------------
		return true;
	}
	//------------------
	function hasSnapshot($date, $provider = false, $asset = false, $notFoundValue = 0)
	{
		//------------------
		$tsDate = XMixedToTimestamp($date); // need time stamp for comparison
		//------------------
		$result = $this->getSnapshot($date, $provider, $asset, false /*wantAssetValues*/, true /*existTestOnly*/);
		//------------------
		if ($result === false)
		{
			XLogError('MarketHistory::hasSnapshot getSnapshot failed');
			return false;
		}
		//------------------
		if ($result['found'] === false)
			return $notFoundValue;
		//------------------
		return $result;
	}
	//------------------
	function getSnapshot($date, $provider = false, $asset = false, $wantAssetValues = true, $existTestOnly = false)
	{
		global $db, $dbMarketSnapshotFields, $dbMarketDataFields;
		//------------------
		$tsDate = XMixedToTimestamp($date);
		//------------------
		if ($tsDate === false || ($provider !== false && !is_numeric($provider)) || ($asset !== false && !is_numeric($asset)) || (!is_bool($wantAssetValues) && !is_array($wantAssetValues)))
		{
			XLogError('MarketHistory::getSnapshot validate parameters failed.');
			return false;
		}
		//------------------
		$where = DB_MARKETSNAPSHOT_DATE_START."<=$tsDate AND ".DB_MARKETSNAPSHOT_DATE_END.">= $tsDate";
		if ($asset !== false)
			$where .= ' AND '.DB_MARKETSNAPSHOT_ASSET."=$asset";
		if ($provider !== false)
			$where .= ' AND '.DB_MARKETSNAPSHOT_PROVIDER."=$provider";
		//------------------
		if (!$existTestOnly)
			$dbMarketSnapshotFields->SetValues();
		else
		{
			$dbMarketSnapshotFields->ClearValues();
			$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_DATE_START);
			$dbMarketSnapshotFields->SetValue(DB_MARKETSNAPSHOT_DATE_END);
		}
		//------------------
		$sql = $dbMarketSnapshotFields->scriptSelect($where, DB_MARKETSNAPSHOT_ID.' DESC' /*orderBy*/, 1 /*limit*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError('MarketHistory::getSnapshot db Query snapshot failed.'."\nsql: $sql");
			return false;
		}
		//------------------
		$snapshot = $qr->GetRowArray();
		//------------------
		if ($snapshot === false)
		{
			XLogDebug('MarketHistory::getSnapshot no row, returning not found for timestamp: '.$tsDate);
			return array('found' => false, 'req_date' => $tsDate);
		}
		//------------------
		if (!isset($snapshot[DB_MARKETSNAPSHOT_ID]) || !is_numeric($snapshot[DB_MARKETSNAPSHOT_ID]) || !isset($snapshot[DB_MARKETSNAPSHOT_DATE_START]) || !is_numeric($snapshot[DB_MARKETSNAPSHOT_DATE_START]) || !isset($snapshot[DB_MARKETSNAPSHOT_DATE_END]) || !is_numeric($snapshot[DB_MARKETSNAPSHOT_DATE_END]))
		{
			XLogError('MarketHistory::getSnapshot validate snapshot fields failed. '.XVarDump($snapshot));
			return false;
		}
		//------------------
		$result = array('found' => true, 'req_date' => $tsDate, 'snapshot_id' => (int)$snapshot[DB_MARKETSNAPSHOT_ID], 'date_start' => (int)$snapshot[DB_MARKETSNAPSHOT_DATE_START], 'date_end' => (int)$snapshot[DB_MARKETSNAPSHOT_DATE_END]);
		//------------------
		if ($existTestOnly)
			return $result;
		//------------------
		if (!isset($snapshot[DB_MARKETSNAPSHOT_POLL_DATE]) || !isset($snapshot[DB_MARKETSNAPSHOT_ASSET]) || !isset($snapshot[DB_MARKETSNAPSHOT_PROVIDER]) ||
			!is_numeric($snapshot[DB_MARKETSNAPSHOT_POLL_DATE]) || !is_numeric($snapshot[DB_MARKETSNAPSHOT_ASSET]) || !is_numeric($snapshot[DB_MARKETSNAPSHOT_PROVIDER]))
		{
			XLogError('MarketHistory::getSnapshot validate snapshot fields failed. '.XVarDump($snapshot));
			return false;
		}
		//------------------
		$result['poll_date'] = (int)$snapshot[DB_MARKETSNAPSHOT_POLL_DATE];
		$result['asset'] = (int)$snapshot[DB_MARKETSNAPSHOT_ASSET];
		$result['provider'] = (int)$snapshot[DB_MARKETSNAPSHOT_PROVIDER];
		//------------------
		if ($wantAssetValues === false || (is_array($wantAssetValues) && sizeof($wantAssetValues) == 0))
			return $result;
		//------------------
		$dbMarketDataFields->ClearValues();	
		$dbMarketDataFields->SetValue(DB_MARKETDATA_OTHER_ASSET);
		$dbMarketDataFields->SetValue(DB_MARKETDATA_VALUE);
		//------------------
		$where = DB_MARKETDATA_SNAPSHOT.'='.$snapshot[DB_MARKETSNAPSHOT_ID];
		//------------------
		if (is_array($wantAssetValues))
			$where .= ' AND '.DB_MARKETDATA_OTHER_ASSET.' IN ('.implode(',', $wantAssetValues).')';
		//------------------
		$sql = $dbMarketDataFields->scriptSelect($where, DB_MARKETDATA_OTHER_ASSET /*orderBy*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError('MarketHistory::getSnapshot db Query data failed.'."\nsql: $sql");
			return false;
		}
		//------------------
		$resultData = array();
		while ( ($data = $qr->GetRowArray()) !== false)
			if (isset($data[DB_MARKETDATA_OTHER_ASSET]) && isset($data[DB_MARKETDATA_VALUE]) && is_numeric($data[DB_MARKETDATA_OTHER_ASSET]) && is_numeric($data[DB_MARKETDATA_VALUE]))
				$resultData[(int)$data[DB_MARKETDATA_OTHER_ASSET]] = $data[DB_MARKETDATA_VALUE];
		//------------------
		$result['asset_values'] = $resultData;
		//------------------
		return $result;
	}
	//------------------
} // class MarketHistory
//---------------
class MarketSnapshotsTable
{
	//------------------
	function __construct()
	{
	}
	//------------------
	function Install()
	{
		global $db, $dbMarketSnapshotFields;
		//------------------------------------
		$sql = $dbMarketSnapshotFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("MarketSnapshotsTable::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbMarketSnapshotFields;
		//------------------------------------
		$sql = $dbMarketSnapshotFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("MarketSnapshotsTable::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbMarketSnapshotFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case $dbMarketSnapshotFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbMarketSnapshotFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("MarketSnapshotsTable::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("MarketSnapshotsTable::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
} // class MarketHistorySnapshotsTable
//---------------
class MarketDataTable
{
	//------------------
	function __construct()
	{		
	}
	//------------------
	function Install()
	{
		global $db, $dbMarketDataFields;
		//------------------------------------
		$sql = $dbMarketDataFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("MarketDataTable::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbMarketDataFields;
		//------------------------------------
		$sql = $dbMarketDataFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("MarketDataTable::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbMarketDataFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case $dbMarketDataFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbMarketDataFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("MarketDataTable::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("MarketDataTable::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
} // class MarketDataTable
//---------------
?>
