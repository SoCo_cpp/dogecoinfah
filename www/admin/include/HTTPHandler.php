<?php
//---------------------------------
/*
 * include/pages/admin/include/HTTPHandler.php
 * 
 * All of PHP's HTTP handling has problems.
 * This class manually encodes and decods
 * HTTP messages, with limitted features.
 *
*/
//---------------------------------
define('HTTPHandler_Def_Header_Len_Max', 1024);
define('HTTPHandler_Def_Header_Line_Max', 15);
define('HTTPHandler_Def_Read_Timeout_usec', 500000);
define('HTTPHandler_Def_Read_Reply_Timeout_sec', 7);
//---------------------------------
define('HTTPHandler_Minimal_Header_Len_Max', 256);
define('HTTPHandler_Minimal_Header_Line_Max', 10);
define('HTTPHandler_Minimal_Read_Timeout_usec', 250000);
define('HTTPHandler_Minimal_Read_Reply_Timeout_sec', 5);
//---------------------------------
class HTTPHandler
{
	var $debugMode = false;
	var $lastError = false;
	var $path = '/';
	var $headers = array();
	var $limits = array();
	
    function __construct($debugMode = false, $minimalLimits = false) 
    {
		$this->debugMode = $debugMode;
		if ($minimalLimits)
			$this->loadMinimalLimits();
		else
			$this->loadDefaultLimits();
    }

	function setDebug($mode = true)
	{
		$this->debugMode = ($mode === true ? true : false);
	}
	
	function lastError($defaultValue = '')
	{
		if ($this->lastError === false)
			return $defaultValue;
		return $this->lastError;
	}
	
	function clearError($debugMsg = false)
	{
		if ($this->debugMode)
			$this->lastError = $debugMsg;
		else
			$this->lastError = false;		
	}
	
	function appendError($msg)
	{
		if ($this->lastError === false)
			$this->lastError = $msg;
		else 
			$this->lastError .= "\r\n".$msg;
	}
	
	function appendDebug($debugMsg)
	{
		if ($this->debugMode)
			$this->appendError($debugMsg);
	}
	
	function setPath($path)
	{
		$this->path = $path;
	}
	
	function clearHeaders()
	{
		$this->headers = array();
	}
	
	function addHeader($header)
	{
		$this->headers[] = $header;
	}
	
	function loadDefaultLimits()
	{
		$this->limits = array();
		$this->limits['Header_Len_Max']  = HTTPHandler_Def_Header_Len_Max;
		$this->limits['Header_Line_Max'] = HTTPHandler_Def_Header_Line_Max;
		$this->limits['Read_Timeout_usec'] = HTTPHandler_Def_Read_Timeout_usec;
		$this->limits['Read_Reply_Timeout_sec'] = HTTPHandler_Def_Read_Reply_Timeout_sec;
		
	}
	
	function loadMinimalLimits()
	{
		$this->limits = array();
		$this->limits['Header_Len_Max']  = HTTPHandler_Minimal_Header_Len_Max;
		$this->limits['Header_Line_Max'] = HTTPHandler_Minimal_Header_Line_Max;
		$this->limits['Read_Timeout_usec'] = HTTPHandler_Minimal_Read_Timeout_usec;
		$this->limits['Read_Reply_Timeout_sec'] = HTTPHandler_Minimal_Read_Reply_Timeout_sec;
	}
	
	function setLimit($name, $value)
	{
		$this->limits[$name] = $value;
	}
	
	function buildGET($path = false, $additionalHeaderArray = array())
	{
		if ($path === false)
			$path = $this->path;
		$http = "GET $path HTTP/1.1\r\n";
		foreach ($this->headers as $h)
			$http .= "$h\r\n";
		foreach ($additionalHeaderArray as $h)
			$http .= "$h\r\n";
		$http .= "\r\n";
		return $http;
	}
	
	function buildPOST($path = false, $content = '', $additionalHeaderArray = array())
	{
		if ($path === false)
			$path = $this->path;
		$http = "POST $path HTTP/1.1\r\n";
		foreach ($this->headers as $h)
			$http .= "$h\r\n";
		foreach ($additionalHeaderArray as $h)
			$http .= "$h\r\n";
		$http .= "\r\n";
		$http .= $content;
		return $http;
	}

	function parseReply($httpReply)
	{
		$this->clearError('HTTPHandler::parseReply');
		$httpVer = substr($httpReply, 0, 9);
		if ($httpVer != 'HTTP/1.1 ')
		{
			$this->appendError("parseReply Validate HTTP response and version failed: ".XVarDump($httpVer));
			return false;
		}
		$posContentBreak = strpos($httpReply, "\r\n\r\n");
		if ($posContentBreak === false)
		{
			$httpHeaders = $httpReply;
			$httpContent = '';
		}
		else
		{
			if ($posContentBreak > $this->limits['Header_Len_Max'])
			{
				$this->appendError("HTTPHandler::parseReply Header length max exceeded. Content break found at $posContentBreak, but limit is ".$this->limits['Header_Len_Max']);
				return false;
			}
			$httpHeaders = substr($httpReply, 0, $posContentBreak);
			$httpContent = substr($httpReply, ($posContentBreak + 4));
		}
		$headerLines = explode("\r\n", $httpHeaders, ($this->limits['Header_Line_Max'] + 1));
		if (sizeof($headerLines) > $this->limits['Header_Line_Max'] || sizeof($headerLines) < 1)
		{
			$this->appendError('HTTPHandler::parseReply Header line count exceeded. Header lines count is '.sizeof($headerLines).', but limit is '.$this->limits['Header_Line_Max'].' '.XVarDump($headerLines));
			return false;
		}
		$statusCode = substr($headerLines[0], 9, 3);
		if (!is_numeric($statusCode))
		{
			$this->appendError('HTTPHandler::parseReply Validate status code is numeric failed. First header line is: '.XVarDump($headerLines[0]));
			return false;
		}
		$hidx = 0;
		$headers = array();
		foreach ($headerLines as $line)
		if ($hidx++ != 0) // not first line
		{
			$posSplit = strpos($line, ':');
			if ($posSplit === false)
			{
				$this->appendError("HTTPHandler::parseReply Header line found without colon split. Header line $hidx: ".XVarDump($line));
				return false;
			}
			$name = substr($line, 0, $posSplit);
			$value = substr($line, $posSplit+1);
			if ($name == '')
			{
				$this->appendError("HTTPHandler::parseReply Header line found with empty name. Header line $hidx: ".XVarDump($line));
				return false;
			}
			$headers[$name] = $value;
			if ($name == 'Content-Length')
			{
				if (!is_numeric($value))
				{
					$this->appendError("HTTPHandler::parseReply validate Content-Length header line's value is numeric failed. Header line $hidx, value ".XVarDump($value).", posSplit: $posSplit, line: ".XVarDump($line));
					return false;
				}
				if ($value != strlen($httpContent))
				{
					$this->appendError("HTTPHandler::parseReply validate Content-Length header line's value matches the size of the content failed. Content size: ".strlen($httpContent).", Header line: ".XVarDump($line));
					return false;
				}
			}
		}
		return array('status' => $statusCode, 'headers' => $headers, 'content' => $httpContent);
	}
	
	function requestReply($stream, $httpRequest)
	{
		$this->clearError('HTTPHandler::requestReply');
		if (@fwrite($stream, $httpRequest) === false)
		{
			$this->appendError('HTTPHandler::requestReply fwrite httpRequest failed');
			return false;
		}
		if (!@fflush($stream))
		{
			$this->appendError('HTTPHandler::requestReply fflush failed');
			return false;
		}
		$reply = $this->readReply($stream);
		if ($reply === false)
		{
			$this->appendError('HTTPHandler::requestReply readReply failed');
			return false;
		}
		return $reply;
	}
	
	function readReply($stream)
	{
		$this->clearError('HTTPHandler::readReply');
		if (!@stream_set_blocking($stream, true))
		{
			$this->appendError('HTTPHandler::readReply stream_set_timeout failed');
			return false;
		}
		if (!@stream_set_timeout($stream, 0 /*sec*/, $this->limits['Read_Timeout_usec'] /*microsec*/))
		{
			$this->appendError('HTTPHandler::readReply stream_set_timeout failed');
			return false;
		}
		$timeout = time() + $this->limits['Read_Reply_Timeout_sec']; // seconds
		$reply = '';
		do
		{
			$reply = @stream_get_contents($stream);
			if ($reply === false)
			{
				$this->appendError('HTTPHandler::readReply stream_get_contents failed');
				return false;
			}
		} while ($reply == '' && time() < $timeout);
		return $reply;
	}
	
} // class HTTPHandler
//---------------------------------
?>
