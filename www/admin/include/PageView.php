<?php
//---------------------------------
/*
 * include/PageView.php
 * 
 * 
*/
//---------------------------------
//-------------------------------------------------------------
class PageView
{
	var $views = array();
	//------------------
	function __construct()
	{
		//------------------------
		//------------------------
	}
	//------------------
	function getViewsDump()
	{
		//------------------------
		return XVarDump($this->views);
	}
	//------------------
	function getHeader()
	{
		//------------------------
		$html = <<<HEREDOC
				<script>
					function pageViewTabClick(viewName, tab){
						if (!tab)
						{
							console.log('pageViewTabClick  viewName '+viewName+' bad tab:');
							console.log(tab);
							return;
						}
						console.log('pageViewTabClick selected page: '+tab.value);
						var tabs = document.getElementById('pvTabs_'+viewName);
						if (!tabs)
						{
							console.log('pageViewTabClick tabs not found for viewName '+viewName);
							return;
						}
						var pages = document.querySelectorAll('[id^=\"pvPage_'+viewName+'_\"]');
						console.log(tabs);
						if (!pages)
						{
							console.log('pageViewTabClick pages not found for viewName '+viewName);
							return;
						}
						var selPageName = 'pvPage_'+viewName+'_'+tab.value;
						console.log('SelPageName: '+selPageName);
						for (var i = 0; i < pages.length; i++)
						{
							var page = pages.item(i);
							console.log('page ['+i+']:'+page.id);
							page.style.display = (page.id != selPageName ? 'none' : 'block');
						}
						tabs.value = tab.value;
					}
				</script>
				<style>
					.pageView {
						border: 1px solid black;
						background-color:#BBBBBB;
					}
					.pageViewTitle {
					}
					.pageViewTabs {
						display: inline-block;
						width:100%;
						overflow-x:scroll;
						background-color:#888888;
						height:2em;
					}
					.pageViewTab {
						background-color:#BBBBBB;
					}
					.pageViewTab:checked {
						background-color:#DDDDDD;
					}
					.pageViewCont {
						border: 1px solid black;
						height:100%;
					}
					.pageViewPage {
						height:100%;
					}
				</style>
HEREDOC;
		//------------------------
		return $html;
	}
	//------------------
	function createView($viewName, $viewTitle, $pagesList = false)
	{
		//------------------------
		$paramError = false;
		if (!is_string($viewTitle) && !is_numeric($viewTitle))
			$paramError = "viewTitle wrong type";
		else if (!is_string($viewName) && !is_numeric($viewTitle))
			$paramError = "viewName wrong type";
		else
		{
			$viewName = trim($viewName);
			if ($viewName == '')
				$paramError = "viewName cannot be blank";
		}
		//------------------------
		if ($paramError !== false)
			return "createView validate parameters failed, $paramError";
		//------------------------
		$this->views[$viewName] = array('title' => $viewTitle, 'selected' => false, 'pages' => array());
		//------------------------
		if (is_array($pagesList))
			foreach ($pagesList as $pageName => $pageData)
			{
				$ret = $this->addViewPage($viewName, $pageName, $pageData);
				if ($ret !== true)
					return "createView addTabPagesPage pageList parameter failed: $ret";
			}
		//------------------------
		return true;
	}
	//------------------
	// pageData ['title','content']
	function addViewPage($viewName, $pageName, $pageData, $selectThisPage = false, $allowOverwrite = false)
	{
		//------------------------
		$paramError = false;
		if (!is_string($viewName) && !is_numeric($viewTitle))
			$paramError = "viewName wrong type";
		else if (!isset($this->views[$viewName]))
			$paramError = "'$viewName' doesn't exist";
		else if (!is_array($pageData) || !isset($pageData['title']) || !isset($pageData['content']))
			$paramError = "pageData not an associative array with the required 'title' and 'content' fields.";
		else if (!is_string($pageData['title']) && !is_numeric($pageData['title']))
			$paramError = "page title wrong type";
		else if (!is_string($pageData['content']) && !is_numeric($pageData['content']))
			$paramError = "page content wrong type";
		else if ($allowOverwrite === false && isset($this->views[$viewName]['pages'][$pageName]))
			$paramError = "viewName '$viewName' already has a page named '$pageName'. Enable allowOverwrite parameter if this is desired.";
		//------------------------
		if ($paramError !== false)
			return "addViewPage validate parameters failed, $paramError";
		//------------------------
		$this->views[$viewName]['pages'][$pageName] = $pageData;
		//------------------------
		if ($selectThisPage !== false || $this->views[$viewName]['selected'] === false)
			$this->views[$viewName]['selected'] = $pageName;
		//------------------------
		return true;
	}
	//------------------
	function buildView($viewName, $selectPage = false)
	{
		//------------------------
		if (!is_string($viewName) && !is_numeric($viewTitle))
			return "buildView validate parameters failed, viewName wrong type";
		//------------------------
		if (!isset($this->views[$viewName]))
			return "buildView validate parameters failed, viewName '$viewName' doesn't exist";
		//------------------------ selection: set via param
		if ($selectPage !== false)
			$this->views[$viewName]['selected'] = $selectPage;
		//------------------------  selection: get current selection
		$selectedPage = $this->views[$viewName]['selected'];
		//------------------------ selection: clear invalid selection
		if ($this->views !== false && !isset($this->views[$viewName]['pages'][$selectedPage]))
			$selectedPage = false;
		//------------------------
		if ($selectedPage === false) // selection: select for entry (still false if empty)
			$selectedPage = reset($this->views[$viewName]['pages']); // reset gets first element, or false if empty
		//------------------------
		$title = $this->views[$viewName]['title'];
		if ($title == '')
			$title = '&nbsp;';
		//------------------------
		$htmlTabs = '';
		$htmlPages = '';
		//------------------------
		if (sizeof($this->views[$viewName]['pages']) == 0)
		{
			$htmlTabs .= "<input class='pageViewTab' type='radio' checked='checked' />&nbsp;\n";
		}
		else
			foreach ($this->views[$viewName]['pages'] as $pageName => $pageData)
			{
				//------------------------
				$pageTitle = $pageData['title'];
				if ($pageTitle == '')
					$pageTitle = '&nbsp;';
				//------------------------
				$pageContent = $pageData['content'];
				if ($pageContent == '')
					$pageContent = '&nbsp;';
				//------------------------
				if ($selectedPage === false)
					$selectedPage = $pageName; // set first page when no selection
				//------------------------
				if ($selectedPage == $pageName)
				{
					$tabSelText  = "checked='checked'";
					$pageSelText = '';
				}
				else
				{
					$tabSelText = '';
					$pageSelText = "style='display:none;'";
				}
				//------------------------
				$tabName  = "pvTab_".$viewName;
				//$tabId    = $tabName.'_'.$pageName;
				$contName = "pvPage_$viewName".'_'.$pageName;
				//------------------------
				$htmlTabs .= "<input class='pageViewTab' type='radio' name='$tabName' id='$tabName' value='$pageName' $tabSelText onclick='pageViewTabClick(\"$viewName\",this);' />\n<label class='pageViewTab' for='$tabName'>$pageTitle</label>\n";
				//------------------------
				$htmlPages .= "<div class='pageViewPage' name='$contName' id='$contName' $pageSelText>$pageContent</div>\n";
				//------------------------
			}
		//------------------------
		$html =  "<div class='pageView' id='pvView_$viewName'>\n";
		$html .= "\t<div class='pageViewHeader'>\n";
		$html .= "\t\t<p class='pageViewTitle'>$title</p>\n";
		$html .= "\t\t<div class='pageViewTabs' id='pvTabs_$viewName'>\n$htmlTabs</div>\n";
		$html .= "\t</div>\n";	
		$html .= "\t<div class='pageViewCont'>\n$htmlPages</div>\n";
		$html .= "</div>\n";
		//------------------------ // selection: store in case it was updated
		$this->views[$viewName]['selected'] = $selectedPage; 
		//------------------------
		return $html;
	}
} // class TabPageView
//---------------
?>
