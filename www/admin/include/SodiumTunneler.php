<?php
//---------------------------------
/*
 * include/pages/admin/include/SodiumTunneler.php
 * 
*/
//---------------------------------
class SodiumTunneler
{
	var $debugMode = false;
	var $lastError = false;
    var $hostAddress = false;
    var $tunnelAddress = false;
    var $remoteUser = false;
    var $lastReplySummary = false;
    
    function __construct($debugMode = false) 
    {
		$this->debugMode = $debugMode;
		$this->messenger = new SodiumMessenger($debugMode) or die('Create object failed');
    }

	function setDebug($mode = true)
	{
		$this->debugMode = ($mode === true ? true : false);
	}
	
	function lastError($defaultValue = '')
	{
		if ($this->lastError === false)
			return $defaultValue;
		return $this->lastError;
	}
	
	function clearError($debugMsg = false)
	{
		if ($this->debugMode)
			$this->lastError = $debugMsg;
		else
			$this->lastError = false;		
	}
	
	function appendError($msg)
	{
		if ($this->lastError === false)
			$this->lastError = $msg;
		else 
			$this->lastError .= "\r\n".$msg;
	}
	
	function appendDebug($debugMsg)
	{
		if ($this->debugMode)
			$this->appendError($debugMsg);
	}
	
	function generateKeypair()
	{
		return $this->messenger->generateKeypair();
	}

	function encode64($data)
	{
		return $this->messenger->encode64($data);
	}
	
	function decode64($data)
	{
		return $this->messenger->decode64($data);
	}
	
    function setAuth($user = false, $pubLocalKey = false, $privLocalKey = false)
    {
		if (!$this->messenger->setAuth($user, $pubLocalKey, $privLocalKey))
		{
			$this->lastError = 'SodiumTunneler::setAuth messenger setAuth failed: '.$this->messenger->lastError();
			return false;
		}
		return true;
	}
	
	function addRemoteAuth($user, $pubKey)
	{
		if (!$this->messenger->addRemoteAuth($user, $pubKey))
		{
			$this->lastError = 'SodiumTunneler::setAuth messenger setAuth failed: '.$this->messenger->lastError();
			return false;
		}
		return true;
	}
	
	function removeRemoteAuth($user)
	{
		$this->messenger->removeRemoteAuth($user);
	}
	
	function setAddresses($hostAddress = false, $tunnelAddress = false)
	{
		if ($hostAddress !== false)
			$this->hostAddress = $hostAddress;
		if ($tunnelAddress !== false)
			$this->tunnelAddress = $tunnelAddress;
	}
	
	function sendHelo()
	{
		$this->clearError('SodiumTunneler::sendHelo');
		$pkt = $this->messenger->buildEmptyMsg();
		if ($pkt === false)
		{
			$this->appendError('SodiumTunneler::sendHelo messenger buildEmptyMsg failed: '.$this->messenger->lastError());
			return false;
		}
		$reply = $this->sendRequestPacket($pkt);
		if ($reply === false)
		{
			$this->appendError('SodiumTunneler::sendHelo sendRequestPacket failed');
			return false;
		}
		$replyMessageData = $this->parseMessage($reply, true /*includeUser*/);
		if ($replyMessageData === false)
		{
			$this->appendError('SodiumTunneler::sendRequest parseMessage failed');
			return false;
		}
		$this->remoteUser = $replyMessageData[0];
		return $replyMessageData[1];
	}

	function sendRequest($message)
	{
		$this->clearError('SodiumTunneler::sendRequest');
		if ($this->remoteUser === false)
		{
			$ret = $this->sendHelo();
			if ($ret === false)
			{
				$this->appendError('SodiumTunneler::sendRequest sendHelo failed');
				return false;
			}
			$this->clearError('SodiumTunneler::sendRequest (after helo)');
		}
		$tunnelMessage = json_encode(array('t' => $this->tunnelAddress, 'c' => $message));
		$pkt = $this->messenger->buildMsg($this->remoteUser, $tunnelMessage);
		if ($pkt === false)
		{
			$this->appendError('SodiumTunneler::sendRequest messenger buildMsg failed: '.$this->messenger->lastError());
			return false;
		}
		$reply = $this->sendRequestPacket($pkt);
		if ($reply === false)
		{
			$this->appendError('SodiumTunneler::sendRequest sendRequestPacket failed');
			return false;
		}
		$replyMessage = $this->parseMessage($reply);
		if ($replyMessage === false)
		{
			$this->appendError('SodiumTunneler::sendRequest parseMessage failed');
			$this->appendDebug('SodiumTunneler::sendRequest parseMessage failed. lastReplySummary: '.SVarDump($this->lastReplySummary));
			return false;
		}
		return $replyMessage;
	}
	
	function sendRequestPacket($pkt)
	{
		$http = new HTTPHandler(true /*debug*/, false /*minimalLimits*/) or die ('Create object failed');
		$this->clearError('SodiumTunneler::sendRequestPacket');
		if ($this->hostAddress === false)
		{
			$this->appendError('SodiumTunneler::sendRequestPacket verify hostAddress failed');
			return false;
		}
		$parts = parse_url($this->hostAddress);
		if (!isset($parts['host']))
		{
			$this->appendError('SodiumTunneler::sendRequestPacket parsing URL failed to find the host. hostAddress: '.$this->hostAddress);
			return false;
		}
		$host = $parts['host'];
		$port = (isset($parts['port']) ? $parts['port'] : 80);
		$path = (isset($parts['path']) ? $parts['path'] : '/');

		$headers = array('Host: '.$host.($port != 80 ? ':'.$port : ''));
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Content-Length: '.strlen($pkt);
		
		$httpRequest = $http->buildPOST($path, $pkt /*content*/, $headers /*headers*/);
		//XLogDebug("SodiumTunneler::sendRequestPacket hostAddress: ".$this->hostAddress.", host: $host, port: $port, path: $path, parts: ".XVarDump($parts));
		$stream = @fsockopen($host, $port, $rErrorCode, $rErrorString);
		if ($stream === false)
		{
			$this->appendError('SodiumTunneler::sendRequestPacket fsockopen failed with error ('.$rErrorCode.'): '.$rErrorString);
			return false;
		}		
		$httpReply = $http->requestReply($stream, $httpRequest);
		@fclose($stream);
		if ($httpReply === false)
		{
			$this->appendError('SodiumTunneler::sendRequestPacket http requestReply failed: '.$http->lastError());
			$this->appendDebug('SodiumTunneler::sendRequestPacket Dump httpReply: '.SVarDump($httpReply, false));
			$this->appendDebug('SodiumTunneler::sendRequestPacket Dump httpRequest: '.SVarDump($httpRequest, false));
			return false;
		}
		$replySummary = $http->parseReply($httpReply);
		if ($replySummary === false)
		{
			$this->appendError('SodiumTunneler::sendRequestPacket http parseReply failed: '.$http->lastError());
			return false;
		}
		if ($replySummary['status'] != 200)
		{
			$this->appendError('SodiumTunneler::sendRequestPacket http server returned fail status code: '.$replySummary['status']);
			return false;
		}
		$reply = $replySummary['content'];
		$this->lastReplySummary = $replySummary;
		$this->lastReplySummary['httpReply'] = $httpReply;
		return $reply;
	}
	
	function parseMessage($content, $includeUser = false)
	{
		$this->clearError('SodiumTunneler::parseMessage');
		$message = $this->messenger->parseMsg($content, $includeUser);
		if ($message === false)
		{
			$this->appendError('SodiumTunneler::parseMessage messenger parseMsg failed: '.$this->messenger->lastError());
			return false;
		}
		return $message;
	}		
} // class SodiumTunneler
//---------------------------------
?>
