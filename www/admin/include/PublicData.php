<?php
//-------------------------------------------------------------
/*
*	PublicData.php
*
* 
*/
//-------------------------------------------------------------
define('PUBLIC_DATA_MANIFEST_OUTPUT', '../data/PublicDataManifest.json');
define('PUBLIC_BALANCE_DATA_OUTPUT', '../data/PublicData.json');
define('PUBLIC_ROUND_DATA_OUTPUT', '../data/PublicRoundData.json');
define('PUBLIC_ROUND_CHART_DATA_OUTPUT', '../data/PublicRoundChartData.json');
define('PUBLIC_TXID_LIST_OUTPUT', '../data/PublicTxidList.bin');
define('PUBLIC_WORKER_LIST_INDEX_OUTPUT', '../data/PublicWorkerList.bin');
define('PUBLIC_WORKER_LIST_VALID_OUTPUT', '../data/PublicWorkerListVal.bin');
define('PUBLIC_WORKER_LIST_INVALID_OUTPUT', '../data/PublicWorkerListInval.json');
define('PUBLIC_WORKER_DATA_OUTPUT', '../data/PublicWorkerData.json');
//-------------------------------------------------------------
define('PUBDATA_MANIFEST_ASSET_BALANCE',			1);
define('PUBDATA_MANIFEST_ASSET_CHART',				2);
define('PUBDATA_MANIFEST_ASSET_TEAM',				3);
define('PUBDATA_MANIFEST_ASSET_TXIDS',				4);
define('PUBDATA_MANIFEST_ASSET_WORKERLIST_INDEX',	5);
define('PUBDATA_MANIFEST_ASSET_WORKERLIST_INV',		6);
define('PUBDATA_MANIFEST_ASSET_WORKERLIST_VAL',		7);
define('PUBDATA_MANIFEST_ASSET_ROUNDS_INDEX',		8);
define('PUBDATA_MANIFEST_ASSET_ROUNDS',				9);
define('PUBDATA_MANIFEST_ASSET_WORKERDATA_INDEX',	10);
define('PUBDATA_MANIFEST_ASSET_WORKERDATA',			11);
define('PUBDATA_MANIFEST_ASSET_TEXT_MANAGER',		12);
//-------------------------------------------------------------
define('PUBLIC_STALE_MUTEX_AGE', 30.0); // minutes
define('PUBLIC_BALANCE_UPDATE_RATE', 8); // minutes
//-------------------------------------------------------------
define('DEFAULT_PUBLIC_ROUND_BLOCK_SIZE', 300000); // max bytes per block
define('DEFAULT_PUBLIC_ROUND_COUNT', 30); // max round count per block
define('DEFAULT_PUBLIC_ROUND_CHART_COUNT', 100); // last x rounds are displayed
define('DEFAULT_PUBLIC_ROUND_INCLUDE_IDLE', 0); // 0/1 include round workers with no tx (idle)
define('DEFAULT_PUBLIC_POLL_TEAM_RATE', 360); // 60 mins = 6 hours
define('DEFAULT_PUBLIC_WORKER_BLOCK_SIZE', 1000);
define('DEFAULT_PUBLIC_WORKER_PAYOUT_LIMIT', 12);
define('DEFAULT_PUBLIC_WORKER_LIST_BLOCK_SIZE', 4000);
//-------------------------------------------------------------
define('CFG_PUBLIC_DATA_MANIFEST', 'auto_pubdata_manifest');
define('CFG_PUBLIC_AUTOMATION_LOCK', 'auto_pubdata_lock');
define('CFG_PUBLIC_ROUND_BLOCK_SIZE', 'auto_pubdata_round_block_size');
define('CFG_PUBLIC_ROUND_COUNT', 'auto_pubdata_round_count');
define('CFG_PUBLIC_ROUND_CHART_COUNT', 'auto_pubdata_chart_count');
define('CFG_PUBLIC_ROUND_CHART_FULL_COUNT', 'auto_pubdata_fchart_count');
define('CFG_PUBLIC_ROUND_INCLUDE_IDLE', 'auto_pubdata_round_inc_idle');
define('CFG_PUBLIC_POLL_TEAM_RATE', 'auto_pubdata_poll_team_rate');
define('CFG_PUBLIC_WORKER_BLOCK_SIZE', 'auto_pubdata_worker_block_size');
define('CFG_PUBLIC_LAST_ROUND', 'auto_pubdata_last_round');
define('CFG_PUBLIC_WORKER_PAYOUT_LIMIT', 'auto_pubdata_worker_payout_limit');
define('CFG_PUBLIC_WORKER_LIST_BLOCK_SIZE', 'auto_pubdata_worker_list_block_size');
define('CFG_PUBLIC_WORKER_DATA_BLOCKS_STEP', 'auto_pubdata_worker_data_blocks_step');
define('CFG_PUBLIC_LAST_BALANCE_UPDATE', 'auto_pubdata_last_balance_update');
define('CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX', 'auto_pubdata_round_data_next_ridx');
define('CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX', 'auto_pubdata_round_data_next_bidx');
define('CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA', 'auto_pubdata_rounds_without_market');
//---------------------------
define('CFG_PUBLIC_STEP', 'auto_pubdata_step');
define('CFG_PUBLIC_SUBSTEP', 'auto_pubdata_substep');
//---------------------------
define('CFG_PUBLIC_STEPS_ENABLED', 'auto_pubdata_enabled_steps');
define('CFG_PUBLIC_IDLE_STEPS_ENABLED', 'auto_pubdata_enabled_idle_steps');
//---------------------------
define('DEF_CFG_PUBLIC_STEPS_ENABLED', '255');
define('DEF_CFG_PUBLIC_IDLE_STEPS_ENABLED', '255');
//---------------------------
define('LEGACY_UPDATE_TEAM_DATA_EO', false);
define('LEGACY_UPDATE_TEAM_DATA_FAHCLIENT', false);
//---------------------------
define('TEST_TEAMSTAT', false);
define('TEST_TEAMSUMMARY', false);
//-------------------------------------------------------------
define('POLL_TEAM_STATS_PAGE_EO', 'http://folding.extremeoverclocking.com/xml/team_summary.php?t=');
define('TEST_TEAM_STATS_PAGE_EO', 'test_team_summary.xml');
define('TEST_TEAM_STATS_PAGE', '../data/test_team_summary.json');
//---------------------------
define('PUBLIC_TEAM_DATA_OUTPUT', '../data/PublicTeamData.json');
define('CFG_PUBLIC_LAST_POLL_TEAM', 'last-poll-team-stat');
//---------------------------
define('PUBDATA_STEP_NONE',			0);
define('PUBDATA_STEP_START',		1);
define('PUBDATA_STEP_TXIDLIST',		2);
define('PUBDATA_STEP_WORKERLIST',	3);
define('PUBDATA_STEP_ROUNDHISTORY',	4);
define('PUBDATA_STEP_ROUNDCHART',	5);
define('PUBDATA_STEP_WORKERDATA',	6);
define('PUBDATA_STEP_DONE',			7);
//---------------------------
define('PUBDATA_IDLE_SUBSTEP_NONE',				0);
define('PUBDATA_IDLE_SUBSTEP_START',			1);
define('PUBDATA_IDLE_SUBSTEP_TEAMSUMMARY',		1);
define('PUBDATA_IDLE_SUBSTEP_TEAMDATA',			2);
define('PUBDATA_IDLE_SUBSTEP_CALCTEAMSTATS',	3);
define('PUBDATA_IDLE_SUBSTEP_DONE',				4);
//---------------------------
?>
