<?php
/*
 *	include/Config.php
 * 
 * 
* 
*/
//---------------
//---------------
class Config
{
	//------------------
	function Install()
	{
		global $db, $dbConfigFields;
		//------------------------------------
		$sql = $dbConfigFields->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Config::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbConfigFields;
		//------------------------------------
		$sql = $dbConfigFields->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Config::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Init()
	{
		//------------------------------------
		// set new database default entries here
		//------------------------------------
		$rounds = new Rounds() or die("Create object failed");
		if (!$rounds->Init())
			{XLogError("Config::Init rounds Init failed");return false;}
		//------------------------------------
		$wallet = new Wallet() or die("Create object failed");
		if (!$wallet->Init())
			{XLogError("Config::Init wallet Init failed");return false;}
		//------------------------------------
		$Display = new Display() or die("Create object failed");
		if (!$Display->Init())
			{XLogError("Config::Init Display Init failed");return false;}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbConfigFields;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1:
			case 2: // renamed name and value fields which are now mysql keywords, added comment, date modified, and modified user id fields
				//---------------
				$dbConfigFields->ClearValues();
				$dbConfigFields->SetValue(DB_CFG_NAME);
				$dbConfigFields->SetValue(DB_CFG_VALUE);
				//---------------
				$oldFieldNameList = $dbConfigFields->GetNameListString(true /*SkipNotSet*/);
				//---------------
				$sql = "INSERT INTO $dbConfigFields->tableName ($oldFieldNameList) SELECT `name`,`value` FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Config::Import db Execute table config ver 1 import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbConfigFields->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbTeamFields->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Config::Import db Execute table config import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Config::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		if (!$this->Init())
		{
			XLogError("Config::Import post import Init failed");
			return false;
		}
		//------------------------------------
		return true;
	} // Import
	//------------------
	function Set($name, $value, $comment = false)
	{
		global $db, $dbConfigFields;
		global $Login;
		//---------------------------------
		try
		{
			$value = $db->sanitize(''.$value); // ensure string type, sanitize uses PDO quote for escaping, then removes the quotes
		}
		catch (Exception $e)
		{
			XLogError("Config::Set - exception sanitizing value: ".XVarDump($value));
			return false;
		}
		//---------------------------------
		if (strlen($name) > C_MAX_NAME_TEXT_LENGTH || strlen(''.$value) > C_MAX_CONFIG_VALUE_LENGTH || ($comment !== false && strlen($comment) > C_MAX_COMMENT_TEXT_LENGTH))
		{
			XLogError("Config::Set - validate parameters failed. name: ".XVarDump($name).", value: ".XVarDump($value).", comment: ".XVarDump($comment));
			return false;
		}
		//---------------------------------
		$dbConfigFields->ClearValues();
		$dbConfigFields->SetValue(DB_CFG_NAME, $name);
		$dbConfigFields->SetValue(DB_CFG_VALUE, $value);
		//---------------------------------
		if ($comment !== false)
			$dbConfigFields->SetValue(DB_CFG_COMMENT, $comment);
		//---------------------------------
		if ($Login->LoggedIn && $Login->UserID !== null)
 		{
			$dbConfigFields->SetValue(DB_CFG_DATE_MODIFIED, time());
			$dbConfigFields->SetValue(DB_CFG_MODIFIED_USER_ID, $Login->UserID);
		}
		//---------------------------------
		$sql = $dbConfigFields->scriptInsert();
		//---------------------------------
		$dbConfigFields->ClearValue(DB_CFG_NAME); // don't want name in update value assignments
		$sql .= " ON DUPLICATE KEY UPDATE ".$dbConfigFields->GetValueAssignments(true /*SkipNotSet*/);
		//---------------------------------
		//XLogDebug("config set: $sql");
		if (!$db->Execute($sql))
		{
			XLogError("Config::Set - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function SetMany($nameValueArray)
	{
		//---------------------------------
		if (!is_array($nameValueArray))
		{
			XLogError("Config::SetMany validate parameter is array failed");
			return false;
		}
		//---------------------------------
		foreach ($nameValueArray as $name => $value)
		{
			//---------------------------------
			if (is_numeric($name) || !is_string($name))
			{
				XLogError("Config::SetMany numeric or invalid name array key found, fail: ".XVarDump($nameValueArray));
				return false;
			}
			//---------------------------------
			if (!$this->Set($name, $value))
			{
				XLogError("Config::SetMany Set value for '$name' failed");
				return false;
			}
			//---------------------------------
		} // foreach
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Get($name, $defaultValue = false, $failValue = false)
	{
		global $db, $dbConfigFields;
		//---------------------------------
		$sql = "SELECT ".DB_CFG_VALUE." FROM ".$dbConfigFields->tableName." WHERE ".DB_CFG_NAME."='$name'";
		//---------------------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Config::Get - db Query failed.\nsql: $sql");
			return $failValue;
		}
		//------------------
		$value = $qr->GetOneValue($defaultValue /*emptyValue*/, $failValue /*failValue*/);
		if ($value === $failValue)
		{
			XLogError("Config::Get - db query result GetOneValue failed.");
			return $failValue;
		}
		//------------------
		return $value;
	}
	//---------------------------------	
	function GetMany($nameArray, $default = false)
	{
		//------------------
		if (!is_array($nameArray))
		{
			XLogError("Config::GetMany validate parameter is array failed");
			return false;
		}
		//---------------------------------
		$nameValues = array();
		foreach ($nameArray as $name)
		{
			//---------------------------------
			if (is_numeric($name) || !is_string($name))
			{
				XLogError("Config::GetMany numeric or invalid name found in array, fail: ".XVarDump($nameArray));
				return false;
			}
			//---------------------------------
			$value = $this->Get($name, null /*(notset)defaultValue*/, false /*failValue*/);
			if ($value === false)
			{
				XLogError("Config::GetMany Get fail");
				return false;
			}
			//---------------------------------
			$nameValues[$name] = ($value === null ? $default : $value);
			//---------------------------------
		}
		//------------------
		return $nameValues;
	}
	//---------------------------------	
	function GetSetDefault($name, $defaultValue)
	{
		//------------------
		$value = $this->Get($name, null /*(notset)defaultValue*/, false /*failValue*/);
		//------------------
		if ($value === false /*failValue*/)
		{
			XLogError("Config::GetSetDefault Get failed");
			return false;
		}
		//------------------
		if ($value !== null /*(notset)defaultValue*/)
			return $value;
		//------------------
		if (!$this->Set($name, $defaultValue))
		{
			XLogError("Config::GetSetDefault Set default value failed");
			return false;
		}
		//------------------
		return $defaultValue;
	}
	//---------------------------------	
	function Clear($name)
	{
		//---------------------------------
		if (!$this->Set($name, ''))
		{
			XLogError("Config::Clear - Set name '$name' to blank failed");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function ClearMany($nameArray)
	{
		//---------------------------------
		if (!is_array($nameArray))
		{
			XLogError("Config::ClearMany validate parameter is array failed");
			return false;
		}
		//---------------------------------
		foreach ($nameArray as $name)
		{
			//---------------------------------
			if (is_numeric($name) || !is_string($name))
			{
				XLogError("Config::ClearMany numeric or invalid name found in array, fail: ".XVarDump($nameArray));
				return false;
			}
			//---------------------------------
			if (!$this->Clear($name))
			{
				XLogError("Config::ClearMany Clear value for '$name' failed");
				return false;
			}
			//---------------------------------
		} // foreach
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Delete($name)
	{
		global $db, $dbConfigFields;
		//---------------------------------
		$sql = $dbConfigFields->scriptDelete(DB_CFG_NAME."='$name'");
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Config::Delete - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function DeleteLike($likeName)
	{
		global $db, $dbConfigFields;
		//---------------------------------
		$sql = $dbConfigFields->scriptDelete(DB_CFG_NAME." LIKE '$likeName'");
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Config::DeleteLike - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function getListLike($likeName, $justNameValues = true, $default = false)
	{
		//---------------------------------
		$clist = $this->getList(DB_CFG_NAME." LIKE '$likeName'" /*where*/, $justNameValues, $default);
		if ($clist === false)
		{
			XLogError("Config::getListLike - getList failed");
			return false;
		}
		//---------------------------------
		return $clist;
	}
	//---------------------------------	
	function getList($where = false, $justNameValues = true, $default = false)
	{
		global $db, $dbConfigFields;
		//---------------------------------
		$dbConfigFields->ClearValues();
		$dbConfigFields->SetValue(DB_CFG_NAME);
		$dbConfigFields->SetValue(DB_CFG_VALUE);
		//---------------------------------
		if (!$justNameValues)
		{
			$dbConfigFields->SetValue(DB_CFG_COMMENT);
			$dbConfigFields->SetValue(DB_CFG_DATE_MODIFIED);
			$dbConfigFields->SetValue(DB_CFG_MODIFIED_USER_ID);
		}
		//---------------------------------
		$sql = $dbConfigFields->scriptSelect($where, DB_CFG_NAME /*orderby*/);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Config::getList - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		$clist = array();
		if ($justNameValues)
		{
			while ($s = $qr->GetRowArray())
				$clist[$s[DB_CFG_NAME]] = (isset($s[DB_CFG_VALUE]) ? $s[DB_CFG_VALUE] : $default);
		}
		else
		{
			while ($s = $qr->GetRowArray())
				$clist[$s[DB_CFG_NAME]] = array(	(isset($s[DB_CFG_VALUE]) ? $s[DB_CFG_VALUE] : $default),
													(isset($s[DB_CFG_COMMENT]) ? $s[DB_CFG_COMMENT] : ''),
													(isset($s[DB_CFG_DATE_MODIFIED]) ? $s[DB_CFG_DATE_MODIFIED] : -1),
													(isset($s[DB_CFG_MODIFIED_USER_ID]) ? $s[DB_CFG_MODIFIED_USER_ID] : -1) );
		}
		//------------------
		return $clist;
	}
	//---------------------------------	
	function getFull($name, $default = array("", "", 0, -1), $defaultUserID = -1)
	{
		global $db, $dbConfigFields;
		//---------------------------------
		if (!is_string($name) || strlen($name) == 0)
		{
			XLogError("Config::getFull - validate paramter failed: ".XVarDump($name));
			return false;
		}
		//---------------------------------
		$dbConfigFields->ClearValues();
		$dbConfigFields->SetValue(DB_CFG_NAME);
		$dbConfigFields->SetValue(DB_CFG_VALUE);
		$dbConfigFields->SetValue(DB_CFG_COMMENT);
		$dbConfigFields->SetValue(DB_CFG_DATE_MODIFIED);
		$dbConfigFields->SetValue(DB_CFG_MODIFIED_USER_ID);
		//---------------------------------
		$sql = $dbConfigFields->scriptSelect(DB_CFG_NAME."='$name'");
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Config::getFull - db Query failed.\nsql: $sql");
			return $default;
		}
		//------------------
		$row = $qr->GetRowArray();
		if (!is_array($row)  || !isset($row[DB_CFG_NAME])|| !isset($row[DB_CFG_VALUE]))
		{
			XLogError("Config::getFull GetRowArray failed or missing required. returned: ".XVarDump($row));
			return false;
		}
		//------------------
		$name = $row[DB_CFG_NAME]; // already checked
		$value = $row[DB_CFG_VALUE]; // already checked
		$comment = (!isset($row[DB_CFG_COMMENT]) || $row[DB_CFG_COMMENT] === null ? "" : $row[DB_CFG_COMMENT]);
		$modDate = (!isset($row[DB_CFG_DATE_MODIFIED]) || $row[DB_CFG_DATE_MODIFIED] === null ? 0 : $row[DB_CFG_DATE_MODIFIED]);
		$modBy = (!isset($row[DB_CFG_MODIFIED_USER_ID]) || $row[DB_CFG_MODIFIED_USER_ID] === null ? -1 : $row[DB_CFG_MODIFIED_USER_ID]);
		//------------------
		return array($name, $value, $comment, $modDate, $modBy);
	}
	//---------------------------------	
} // class
//---------------------------------	
?>
