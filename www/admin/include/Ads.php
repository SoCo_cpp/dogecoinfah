<?php
/*
 *	www/include/Ads.php
 * 
 * 
* 
*/
//---------------
define('AD_FLAG_NONE', 			0);
define('AD_FLAG_SHOW_ALWAYS', 	1);
define('AD_FLAG_SHOW_TITLE', 	2);
define('AD_FLAG_SHOW_VALUE', 	4);
define('AD_FLAG_SHOW_TEXT', 	8);
define('AD_FLAG_SHOW_IMAGE', 	16);
define('AD_FLAG_LINK_TITLE', 	32);
define('AD_FLAG_LINK_TEXT', 	64);
define('AD_FLAG_LINK_IMAGE', 	128);
//---------------
class Ad
{
	var $id = -1;
	var $title = "";
	var $flags = AD_FLAG_NONE;
	var $text = "";
	var $link = "";
	var $image = "";
	var $style = "";
	//------------------
	function __construct($row = false)
	{
		if ($row !== false)
			$this->set($row);
	}
	//------------------
	function set($row)
	{
		//------------------
		$this->id   	= $row[DB_ADS_ID];
		$this->title	= (isset($row[DB_ADS_TITLE]) ? $row[DB_ADS_TITLE] : "");
		$this->flags 	= (isset($row[DB_ADS_FLAGS]) ? $row[DB_ADS_FLAGS] : AD_FLAG_NONE);
		$this->text 	= (isset($row[DB_ADS_TEXT]) ? $row[DB_ADS_TEXT] : "");
		$this->link 	= (isset($row[DB_ADS_LINK]) ? $row[DB_ADS_LINK] : "");
		$this->image 	= (isset($row[DB_ADS_IMAGE]) ? $row[DB_ADS_IMAGE] : "");
		$this->style 	= (isset($row[DB_ADS_STYLE]) ? $row[DB_ADS_STYLE] : "");
		//------------------
	}
	//------------------
	function setMaxSizes()
	{
		global $dbAds;
		//------------------
		$this->id		= -1;
		$this->title	= $dbAds->GetMaxSize(DB_ADS_TITLE);
		$this->flags	= $dbAds->GetMaxSize(DB_ADS_FLAGS);
		$this->text		= $dbAds->GetMaxSize(DB_ADS_TEXT);
		$this->link		= $dbAds->GetMaxSize(DB_ADS_LINK);
		$this->image	= $dbAds->GetMaxSize(DB_ADS_IMAGE);
		$this->style	= $dbAds->GetMaxSize(DB_ADS_STYLE);
		//------------------
	}
	//------------------
	function Update()
	{
		global $db, $dbAds;
		//------------------
		$dbAds->ClearValues();
		//------------------
		$dbAds->SetValuePrepared(DB_ADS_TITLE,		$this->title);
		$dbAds->SetValuePrepared(DB_ADS_FLAGS,		$this->flags);
		$dbAds->SetValuePrepared(DB_ADS_TEXT,		$this->text);
		$dbAds->SetValuePrepared(DB_ADS_LINK,		$this->link);
		$dbAds->SetValuePrepared(DB_ADS_IMAGE,		$this->image);
		$dbAds->SetValuePrepared(DB_ADS_STYLE,		$this->style);
		//------------------
		$sql = $dbAds->scriptUpdate(DB_ADS_ID."=".$this->id);
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Ad::Update db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbAds->BindValues($dbs))
		{
			XLogError("Ad::Update BindValues failed.");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Ad::Update db prepared statement execute failed.\nsql: $sql");
			return false;
		}
		//------------------
		return true;
	}
	//------------------
} // class Ad
//---------------
class Ads
{
	//------------------
	function Install()
	{
		global $db, $dbAds;
		//------------------------------------
		$sql = $dbAds->scriptCreateTable();
		if (!$db->Execute($sql))
		{
			XLogError("Ads::Install db Execute create table failed.\nsql: $sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Uninstall()
	{
		global $db, $dbAds;
		//------------------------------------
		$sql = $dbAds->scriptDropTable();
		if (!$db->Execute($sql))
		{
			XLogError("Ads::Uninstall db Execute drop table failed.\nsql:\n$sql");
			return false;
		}
		//------------------------------------
		return true;
	}
	//------------------
	function Import($oldTableVer, $oldTableName)
	{
		global $db, $dbAds;
		//------------------------------------
		switch ($oldTableVer)
		{
			case 0: // fall through
			case 1: // 1->2 added title field and renamed most other fields
				//---------------
				$newFieldNames = DB_ADS_ID.','.DB_ADS_TITLE.','.DB_ADS_FLAGS.','.DB_ADS_STYLE.','.DB_ADS_LINK.','.DB_ADS_IMAGE.','.DB_ADS_TEXT;
				$oldFieldNames = "id,'' AS ".DB_ADS_TITLE.',admode,adstyle,adlink,adimage,adtext';
				//---------------
				$sql = "INSERT INTO $dbAds->tableName ($newFieldNames) SELECT $oldFieldNames FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Ads::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			case $dbAds->tableVersion: // same version, just do a copy
				//---------------
				$sql = "INSERT INTO $dbAds->tableName SELECT * FROM  $oldTableName";
				//---------------
				if (!$db->Execute($sql))
				{
					XLogError("Ads::Import db Execute table import failed.\nsql:\n$sql");
					return false;
				}
				//---------------
				break;
			default:
				XLogError("Ads::Import import from ver $oldTableVer not supported");
				return false;
		} // switch ($oldTableVer)
		//------------------------------------
		return true;
	} // Import
	//------------------
	function GetMaxSizes()
	{
		//------------------------------------
		$msize = new Contribution();
		$msizet->setMaxSizes();
		//------------------------------------
		return $msize;		
	}
	//------------------
	function deleteAd($idx)
	{
		global $db, $dbAds;
		//---------------------------------
		if (!is_numeric($idx))
		{
			XLogError("Ads::deleteAd validate parameters failed");
			return false;
		}
		//---------------------------------
		$sql = $dbAds->scriptDelete(DB_ADS_ID."=".$idx);
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Ads::deleteAd - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function Clear()
	{
		global $db, $dbAds;
		//---------------------------------
		$sql = $dbAds->scriptDelete();
		//---------------------------------
		if (!$db->Execute($sql))
		{
			XLogError("Ads::Clear - db Execute failed.\nsql: $sql");
			return false;
		}
		//---------------------------------
		return true;
	}
	//---------------------------------	
	function loadAdRaw($idx)
	{
		global $db, $dbAds;
		//------------------
		$dbAds->SetValues();
		//------------------
		$sql = $dbAds->scriptSelect(DB_ADS_ID."=".$idx, false /*orderby*/, 1 /*limit*/);
		//------------------
		$qr = $db->Query($sql);
		if ($qr === false)
		{
			XLogError("Ads::loadAdRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getAd($idx)
	{
		//------------------
		if (!is_numeric($idx))
		{
			XLogError("Ads::getAd - validate index failed");
			return false;
		}
		//------------------
		$qr = $this->loadAdRaw($idx);
		//------------------
		if ($qr === false)
		{
			XLogError("Ads::getAd - loadAdRaw failed");
			return false;
		}
		//------------------
		$s = $qr->GetRowArray();
		//------------------
		if ($s === false)
		{
			XLogWarn("Ads::getAd - index $idx not found.");
			return false;
		}
		//------------------
		return new Ad($s);
	}
	//------------------
	function loadAdsRaw($where = false, $order = false, $limit = false)
	{
		global $db, $dbAds;
		//------------------
		if ($order === false)
			$order = DB_ADS_ID;
		//------------------
		$dbAds->SetValues();
		$sql = $dbAds->scriptSelect($where, $order, $limit);
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Ads::loadAdsRaw - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		return $qr;
	}
	//------------------
	function getAds($where = false, $order = false, $limit = false)
	{
		//------------------
		if ( ($where !== false && !is_string($where)) || ($order !== false && !is_numeric($order) && !is_string($order)) || ($limit !== false && !is_numeric($limit)) )
		{
			XLogError("Ads::getAds validate parameters failed");
			return false;
		}
		//------------------
		$qr = $this->loadAdsRaw($where, $order, $limit);
		//------------------
		if ($qr === false)
		{
			XLogError("Ads::getAds loadAdsRaw failed");
			return false;
		}
		//------------------
		$adList = array();
		while ($a = $qr->GetRowArray())
			$adList[] = new Ad($a);
		//------------------
		return $adList;
	}
	//------------------
	function addAd($title = false)
	{
		global $db, $dbAds;
		//------------------
		$dbAds->ClearValues();
		if ($title !== false)
			$dbAds->SetValuePrepared(DB_ADS_TITLE, $title);
		//------------------
		$sql = $dbAds->scriptInsert();
		//------------------
		$dbs = $db->Prepare($sql);
		if ($dbs === false)
		{
			XLogError("Ads::addAd db Prepare failed.\nsql: $sql");
			return false;
		}
		//------------------
		if (!$dbAds->BindValues($dbs))
		{
			XLogError("Ads::addAd BindValues failed.");
			return false;
		}
		//------------------
		if (!$dbs->execute())
		{
			XLogError("Ads::addAd db prepared statement execute failed.\nsql: $sql");
			return false;
		}
		//------------------
		$this->ads = array();
		$this->isLoaded = false; // list modified, set to reload
		//------------------
		return true;
	}
	//------------------
} // class Ads
//---------------
?>
