<?php
//-------------------------------------------------------------
/*
 *	www/admin/Install.php
 * 
 *  See www/admin/Settings.php for configurable settings.
 * 
*/
//-------------------------------------------------------------
// Tell Init.php what modes to use
define('IS_LOGIN',		true);
define('IS_ADMIN',		true);
define('IS_INSTALL',	true);
//-------------------------------------------------------------
require('./include/Init.php');
//-------------------------------------------------------------
$CurTableVersions = array();
//-------------------------------------------------------------
$p = XGet('p');
if ($p != "" && is_numeric($p))
{
	switch ($p)
	{
		case 0: header("Location: ./Index.php"); break;
		case 1: header("Location: ./Admin.php"); break;
		case 2: header("Location: ./Install.php"); break;
	}
}
//-------------------------------------------------------------
function getTableList()
{
	global $Login;
	global $xdbUserFields;
	global $dbConfigFields;
	global $dbWorkerFields;
	global $dbRoundFields;
	global $dbPayoutFields;
	global $dbStatsFields;
	global $dbContributionFields;
	global $dbAds;
	global $dbTeamFields;
	global $dbTeamUsersFields;
	global $dbNewDataFields;
	global $dbBlog;
	global $dbMonEventFields;
	global $dbMarketSnapshotFields;
	global $dbMarketDataFields;
	
	$lst = array();
	//----------------------
	// class, dbFields, displayName, wantFastEngine(InnoDB), Indexes false/array(name => field)
	$lst[] = array($Login, 		 				$xdbUserFields,				"Login",			false,		false);
	$lst[] = array(new Config(), 				$dbConfigFields,			"Config",			false,		false);
	$lst[] = array(new Workers(), 				$dbWorkerFields,			"Workers",			true,		false);
	$lst[] = array(new Rounds(), 				$dbRoundFields,				"Rounds",			false,		false);
	$lst[] = array(new Payouts(), 				$dbPayoutFields,			"Payouts",			true,		array('RidIndex' => DB_PAYOUT_ROUND, 'WidIndex' => DB_PAYOUT_WORKER));
	$lst[] = array(new Stats(), 				$dbStatsFields,				"Stats",			true,		array('RidIndex' => DB_STATS_ROUND, 'WidIndex' => DB_STATS_WORKER));
	$lst[] = array(new Contributions(), 		$dbContributionFields,		"Contributions",	false,		array('RidIndex' => DB_CONTRIBUTION_ROUND));
	$lst[] = array(new Ads(),					$dbAds, 					"Ads",				false,		false);
	$lst[] = array(new Team(),					$dbTeamFields,				"Team",				true,		false);
	$lst[] = array(new TeamUsers(),				$dbTeamUsersFields,			"TeamUsers",		true,		false);
	$lst[] = array(new NewRoundData(),			$dbNewDataFields,			"NewRoundData",		true,		array('WidIndex' => DB_NEWDATA_WORKER_ID));
	$lst[] = array(new Blogs(),					$dbBlog,					"Blog",				false,		false);
	$lst[] = array(new Monitor(),				$dbMonEventFields,			"MonitorEvents",	false,		array('RidIndex' => DB_MONEVENT_ROUND));
	$lst[] = array(new MarketSnapshotsTable(),	$dbMarketSnapshotFields,	"MarketSnapshots",	false,		false);
	$lst[] = array(new MarketDataTable(),		$dbMarketDataFields,		"MarketData",		false,		array('SsIndex' => DB_MARKETDATA_SNAPSHOT));
	//----------------------
	return $lst;
}
//-------------------------------------------------------------
function checkDependencies()
{
	$results = "";
	//----------------------
	if (!function_exists("bcmul") || !function_exists("bcadd") || !function_exists("bccomp") || !function_exists("bcsub"))
		$results .= "Dependency missing: PHP BC Math module<br/>";
	//----------------------
	if (!defined('PDO::ATTR_DRIVER_NAME'))
		$results .= "Dependency missing: PHP PDO MySQL module<br/>";
	//----------------------
	if (ini_get('allow_url_fopen') != '1')
		$results .= "Dependency missing: PHP ini option 'allow_url_fopen' could not be enabled. It is required for wallet RPC connectivity and other purposes<br/>";
	//----------------------
	if (version_compare(phpversion(), '7.1', '>=') && ini_get('serialize_precision') != '-1')
		$results .= "Dependency missing: PHP ini option 'serialize_precision' could not be set to the special float value of -1. Transactions may fail and live data may have floating precision flaws.<br/>";
	//----------------------
	return $results;
}
//-------------------------------------------------------------
function redetectCommands()
{
	//----------------------
	$Automation = new Automation() or die("Create object failed");
	//----------------------
	if (!$Automation->ClearCommands())
	{
		XLogError("install.php - redetectCommands Automation ClearCommands failed");
		return false;		
	}
	//----------------------
	if (!$Automation->DetectCommands())
	{
		XLogError("install.php - redetectCommands Automation DetectCommands failed");
		return false;		
	}
	//----------------------
	return true;	
}
//-------------------------------------------------------------
function checkFilePerms()
{
	global $XLogInstance;
	$result = "";
	$lst = array();
	//----------------------
	$lst[] = "./log";
	$lst[] = "./log/archive";
	$lst[] = "../data";
	$lst[] = "../blog";
	//----------------------
	foreach ($lst as $fn)
		if (!is_writable($fn)) // another period to be up one level
			$result .= "Directory not writable: $fn<br/>";
	//----------------------
	$lst = array();
	$lst[] = "./log/automation.lock";
	$lst[] = "./log/ALL_log.txt";
	$lst[] = "../data/PublicData.json";
	$lst[] = "../data/PublicRoundData.json";
	$lst[] = "../data/PublicTeamData.json";
	$lst[] = "../data/PublicRoundChartData.json";
	//----------------------
	foreach ($lst as $fn)
		if (file_exists($fn) && !is_writable($fn)) // another period to be up one level
			$result .= "File exists, but not writable: $fn<br/>";
	//----------------------
	if (!$XLogInstance->TestLogsWritable())
		$result .= "Logger Modules reports not writable.<br/>";
	//----------------------
	if ($result != "")
		$result = "CWD: ".getcwd()."</br>".$result;
	//----------------------
	return $result;
}
//-------------------------------------------------------------
function checkTableVersions()
{
	$result = "";
	$config = new Config();
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		$fields = $tbl[1];
		$TableID = $tbl[2];
		$tblName = $config->Get("dbtablename:$TableID", "" /*defaultValue*/);
		$tblVersion = $config->Get("dbtablever:$TableID", "" /*defaultValue*/);
		if ($tblName === false || $tblVersion === false)
		{
			if (tableExists($fields->tableName))
				return "Database Config table exists, but reading it failed. The table may need updated.<br/>This prevented checking the rest of the database tables.<br/>";
			else
			{
				$existCount = 0;
				foreach ($tableList as $tblExist)
					if (tableExists($tblExist))
						$existCount++;
				if ($existCount == 0)
					return "[Database Not Installed]<br/>";
				return "Database Config table is missing, but $existCount expected tables exist.<br/>Database is broken and cannot know the table versions of the existing tables.<br/>You must recover the Config table by hand or reinstall all database tables<br/>";
			}	
		}
		//$result .= $fields->tableName." - ".$fields->tableVersion." ($tblName - $tblVersion)<br/>";
		if ($fields->tableName != $tblName)
		{
			$result .= "DB Table name doesn't match: new '".$fields->tableName."', installed '$tblName'<br/>";
			$result .= " - DB Table Detected [$TableID] '$tblName' ".(tableExists($tblName) ? "DOES" : "DOES NOT")." exist.<br/>";
			$result .= " - DB Table '".$fields->tableName."' ".(tableExists($fields->tableName) ? "DOES" : "DOES NOT")." exist.<br/>";
		}
		else
		{
			if ($fields->tableVersion != $tblVersion)
				$result .= "DB Table $tblName's version is old: new ".$fields->tableVersion.", installed $tblVersion<br/>";
			if (!tableExists($fields->tableName))
				$result .= "DB Table '".$fields->tableName."' DOES NOT exist.<br/>";
		}
	}	
	//----------------------
	return $result;
}
//-------------------------------------------------------------
function checkTableEngines($doSet = false)
{
	global $db;
	//----------------------
	$result = '';
	//----------------------
	$sql = "SELECT ENGINE FROM information_schema.ENGINES";
	if (!($qr = $db->Query($sql)))
	{
		XLogError("Install.php - checkTableEngines db Query failed.\nsql: $sql");
		return "Couldn't get MySQL engine list<br/>";
	}
	//------------------
	$rows = array();
	while (($row = $qr->GetRow()))
		if (sizeof($row) == 1)
			$rows[] = $row[0];
	//----------------------
	if (!in_array('InnoDB', $rows))
		return "MySQL engine list doesn't contain the desired 'InnoDB': ".XVarDump($rows)."<br/>";
	//----------------------
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
		if ($tbl[3] /*wantFastEngine*/)
		{
			//----------------------
			$fields = $tbl[1];
			$tableID = $tbl[2];
			//----------------------
			$sql = "SHOW TABLE STATUS LIKE '".$fields->tableName."'";
			if (!($qr = $db->Query($sql)))
			{
				XLogError("Install.php -checkTableEngines db Query failed.\nsql: $sql");
				$result .= "Couldn't get table status (query failed) for '".$fields->tableName."' ($tableID)<br/>";
			}
			else if (!($row = $qr->GetRowArray()))
				$result .= "Couldn't get table status (empty) for '".$fields->tableName."' ($tableID)<br/>";
			else if (!isset($row['Name']) || !isset($row['Engine']))
			{
				XLogError("Install.php - checkTableEngines table status for ".$fields->tableName." didn't return all the expected fields: ".XVarDump($row));
				$result .= "Table status returned bad result (missing fields) for '".$fields->tableName."' ($tableID)<br/>";
			}
			else if ($row['Name'] != $fields->tableName)
			{
				XLogError("Install.php - checkTableEngines table status for ".$fields->tableName." returned the wrong table: ".XVarDump($row));
				$result .= "Table status returned bad result (wrong table) for '".$fields->tableName."' ($tableID)<br/>";
			}
			else if ($row['Engine'] != 'InnoDB')
			{
				if (!$doSet)
					$result .= "Table $tableID engine '".$row['Engine']."' but wants 'InnoDB'<br/>";
				else if (XPost($tableID) != "")
				{
					//------------------
					$sql = "ALTER TABLE ".$fields->tableName." ENGINE='InnoDB'";
					if (!$db->Execute($sql))
					{
						XLogError("Install.php - checkTableEngines alter table for ".$fields->tableName." failed.\nsql: $sql");
						$result .= "Table $tableID is using engine '".$row['Engine']."', but set to 'InnoDB' failed<br/>";
					}
					else echo "Table $tableID was using engine '".$row['Engine']."', but set to 'InnoDB' successfully<br/>";
					//------------------
				}
			}
			//----------------------
		}
	//----------------------
	return $result;
}
//-------------------------------------------------------------
function checkTableIndexes($doSet = false)
{
	global $db;
	//----------------------
	$result = '';
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
		if (is_array($tbl[4]) && sizeof($tbl[4]) != 0 /*Indexes*/)
		{
			//----------------------
			$fields  = $tbl[1];
			$tableID = $tbl[2];
			$wantIndexes = $tbl[4];
			$haveIndexes = array();
			//----------------------
			$sql = "SHOW INDEXES FROM `".$fields->tableName."`"; //."` IN `".XDATABASE_NAME."`";
			if (!($qr = $db->Query($sql)))
			{
				XLogError("Install.php -checkTableIndexes db Query failed.\nsql: $sql");
				return $result."Couldn't get table index status (query failed) for '".$fields->tableName."' ($tableID)<br/>";
			}
			while (false !== ($row = $qr->GetRowArray()))
			{
				if (!isset($row['Table']) || !isset($row['Key_name']) || !isset($row['Column_name']))
				{
					XLogError("Install.php - checkTableIndexes table index status for ".$fields->tableName." didn't return all the expected fields: ".XVarDump($row));
					return $result."Table index status returned bad result (missing fields) for '".$fields->tableName."' ($tableID)<br/>";
				}
				else if ($row['Table'] != $fields->tableName)
				{
					XLogError("Install.php - checkTableIndexes table index status for ".$fields->tableName." returned the wrong table: ".XVarDump($row));
					return $result."Table index status returned bad result (wrong table) for '".$fields->tableName."' ($tableID)<br/>";
				}
				else if ($row['Key_name'] != 'PRIMARY') // ignore primary keys
					$haveIndexes[$row['Key_name']] = $row['Column_name'];
			}
			//----------------------
			foreach ($wantIndexes as $indexName => $fieldName)
			{
				//----------------------
				if (is_numeric($indexName) || !is_string($indexName) || !is_string($fieldName) || $indexName == '' ||  $fieldName == '')
				{
					XLogError("Install.php - checkTableIndexes validate table wanted index definition for ".$fields->tableName." failed: ".XVarDump($wantIndexes));
					return $result."Table wanted index definition is bad for '".$fields->tableName."' ($tableID)<br/>";
				}
				//----------------------
				$found = false;
				foreach ($haveIndexes as $haveIndexName => $haveFieldName)
					if ($haveIndexName == $indexName && $haveFieldName == $fieldName)
						$found = true;
				//----------------------
				if (!$found)
				{
					//----------------------
					if (!$doSet)
						$result .= "Table $tableID index not found: $indexName -> $fieldName<br/>";
					else if (XPost($tableID) != "")
					{
						//------------------
						$sql = "ALTER TABLE `".$fields->tableName."` ADD INDEX `$indexName` (`$fieldName`)";
						if (!$db->Execute($sql))
						{
							XLogError("Install.php - checkTableIndexes alter table add index for ".$fields->tableName." failed.\nsql: $sql");
							$result .= "Table $tableID add index failed: $indexName -> $fieldName<br/>";
						}
						else echo "Table $tableID added index successfully: $indexName -> $fieldName<br/>";
					}
					//----------------------
				}
				//----------------------
			}
			//----------------------
			foreach ($haveIndexes as $haveIndexName => $haveFieldName)
			{
				//----------------------
				$found = false;
				foreach ($wantIndexes as $indexName => $fieldName)
					if ($haveIndexName == $indexName && $haveFieldName == $fieldName)
						$found = true;
				//----------------------
				if (!$found)
				{
					//----------------------
					if (!$doSet)
						$result .= "Table $tableID found extra index: $haveIndexName -> $haveFieldName<br/>";
					else if (XPost($tableID) != "" && XPost('Indexes_Del_Extra') != "")
					{
						$sql = "DROP INDEX `$haveIndexName` ON `".$fields->tableName."`";
						if (!$db->Execute($sql))
						{
							XLogError("Install.php - checkTableIndexes drop extra index for table ".$fields->tableName." failed: $haveIndexName -> $haveFieldName\nsql: $sql");
							$result .= "Table $tableID drop extra index failed: $haveIndexName -> $haveFieldName<br/>";
						}
						else echo "Table $tableID dropped extra index successfully: $haveIndexName -> $haveFieldName<br/>";
					}
					//----------------------
				}
				//----------------------
			}
			//----------------------
		}
	//----------------------
	return $result;
}
//-------------------------------------------------------------
function checkCommands()
{
	$Automation = new Automation() or die("Create object failed");
	$result = "";
	//----------------------
	if (!file_exists($Automation->cron_cmd))
		$result .= "Required command 'cron' not found at: '".$Automation->cron_cmd."'<br/>";
	//----------------------
	if (!file_exists($Automation->php_cmd))
		$result .= "Required command 'cron' not found at: '".$Automation->php_cmd."'<br/>";
	//----------------------
	if ($Automation->phpcli_cmd !== false && $Automation->phpcli_cmd != "" && !file_exists($Automation->phpcli_cmd))
		$result .= "Non-Required command 'phpcli' not found at: '".$Automation->phpcli_cmd."'<br/>";
	//----------------------
	if (!file_exists($Automation->wget_cmd))
		$result .= "Required command 'wget' not found at: '".$Automation->wget_cmd."'<br/>";
	//----------------------
	if (!file_exists($Automation->bzip2_cmd))
		$result .= "Required command 'bzip2' not found at: '".$Automation->bzip2_cmd."'<br/>";
	//----------------------
	if (!file_exists($Automation->grep_cmd))
		$result .= "Required command 'grep' not found at: '".$Automation->grep_cmd."'<br/>";
	//----------------------
	return $result;
}
//-------------------------------------------------------------
function createDatabase()
{
	$lastObj = null;
	//----------------------
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		//----------------------
		echo "Installing $tblName DB table...<br/>\n";
		//----------------------
		if ($lastObj == $obj)
			echo "skipped<br/>\n";
		else if (!$obj->Install())
		{
			//----------------------
			XLogError("install.php - table '$tblName' failed to Install");
			echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
			//----------------------
			return false;
		}
		//----------------------
		$lastObj = $obj;
		echo "Installing $tblName DB table...DONE<br/>\n";
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function initDatabase()
{
	//----------------------
	global $Login;
	echo "<div>Initting Login DB...</div>\r";
	if (!$Login->AddUser(C_DEFAULT_USER_NAME, C_DEFAULT_USER_FULL_NAME, C_DEFAULT_USER_PRIV, C_DEFAULT_USER_PASS, false/*time zone*/, true/*bypasPrivCheck*/))
		{XLogError("Install.php - initDatabase - Add default admin user failed"); return false;}
	echo "<div>Initting Login DB...DONE</div>\r";
	//----------------------
	echo "<div>Initting Config DB...</div>\r";
	$config = new Config();
	//----------------------
	if (!$config->Init())
		{XLogError("Install.php - initDatabase - Config Init failed"); return false;}
	echo "<div>Initting Config DB...DONE</div>\r";
	//----------------------
	return true;
}
//-------------------------------------------------------------
function setTableVersions()
{
	$config = new Config();
	//----------------------
	echo "Setting DB table versions...<br/>\n";
	//----------------------
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		//----------------------
		if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
		{
			//----------------------
			XLogError("install.php - setTableVersions - setTableVersion failed");
			echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
			//----------------------
			return false;
		}
		//----------------------
	} // foreach
	//----------------------
	echo "Setting DB table versions...DONE<br/>\n";
	//----------------------
	return true;
}
//-------------------------------------------------------------
function dropDatabase()
{
	$r = true;
	$lastObj = null;
	//----------------------
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		//----------------------
		echo "Uninstalling $tblName DB table...<br/>\n";
		//----------------------
		if ($lastObj == $obj)
			echo "skipped<br/>\n";
		else if (!$obj->Uninstall())
		{
			//----------------------
			XLogError("install.php - dropDatabase - table '$tblName' failed to Uninstall");
			echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
			//----------------------
			$r = false;
			//----------------------
		}
		//----------------------
		$lastObj = $obj;
		echo "Uninstalling $tblName DB table...DONE<br/>\n";
		//----------------------
	} // foreach
	//----------------------
	return $r;
}
//-------------------------------------------------------------
function backupDatabase($prefix = 'bak_')
{
	global $db, $dbConfigFields;
	//----------------------
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		if (XPost($tblName) != "")
		{
			//----------------------
			$bakTableName = "$prefix$tblFields->tableName";
			$bakTableVer = getTableVersion($tblName);
			if ($bakTableVer === false)
			{
				XLogError("install.php - backupDatabase - getTableVersion failed for table '$tblName'");
				return false;
			}
			//----------------------
			echo "Backing up $tblName DB table...<br/>\n";
			//----------------------
			$sql = "DROP TABLE IF EXISTS $bakTableName";
			//----------------------
			if (!$db->Execute($sql))
			{
				XLogError("install.php - backupDatabase - drop table failed.\nsql: $sql\n".$db->errorString());
				return false;
			}
			//----------------------
			$sql = "CREATE TABLE $bakTableName LIKE $tblFields->tableName";
			//----------------------
			if (!$db->Execute($sql))
			{
				XLogError("install.php - backupDatabase - create table failed.\nsql: $sql\n".$db->errorString());
				return false;
			}
			//----------------------
			$sql = "INSERT INTO $bakTableName SELECT * FROM $tblFields->tableName";
			//----------------------
			if (!$db->Execute($sql))
			{
				XLogError("install.php - backupDatabase - copy table failed.\nsql: $sql\n".$db->errorString());
				return false;
			}
			//----------------------
			echo "Backing up $tblName DB table...DONE<br/>\n";
			//----------------------
			if (!setTableVersion($bakTableName, $bakTableName, $bakTableVer))
			{
				XLogError("install.php - backupDatabase - table '$bakTableName' failed to setTableVersion to $bakTableVer");
				echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
			} 
			else XLogNotify("install.php - backupDatabase - successfully backed up table '$tblName' to table '$bakTableName' at version $bakTableVer");
			//----------------------
		}
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function importDatabase($prefix = 'bak_')
{
	global $db, $dbConfigFields;
	//----------------------
	$tableList = getTableList();
	//----------------------
	if (!getTableVersions($prefix))
	{
		XLogError("install.php - importDatabase - getTableVersions failed");
		return false;
	}
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		if (XPost($tblName) != "")
		{
			//----------------------
			$oldTableName = "$prefix$tblFields->tableName";
			$oldTableVer = getTableVersion($oldTableName);
			if ($oldTableVer === false)
			{
				XLogError("install.php - importDatabase - getTableVersion for '$oldTableName' failed.");
				echo "<span style='font-weight:bold;color:red;'>Lookup back table version failed!</span><br/>\n";
				return false;
			}
			//----------------------
			XLogNotify("install.php - importDatabase - lookupTableVersion: $tblName, oldver: $oldTableVer, oldTableName: $oldTableName, prefix: $prefix, table name: ".$tblFields->tableName);
			//----------------------
			echo "Importing $tblName (v $tblFields->tableVersion) DB table from $oldTableName (v $oldTableVer)...<br/>\n";
			//----------------------
			$r = $obj->Import($oldTableVer, $oldTableName);
			//----------------------
			if (!$r)
			{
				XLogError("install.php - importDatabase - table '$tblName' failed to Import");
				echo "<span style='font-weight:bold;color:red;'>Import failed!</span><br/>\n";
				return false;
			}
			else echo "Importing $tblName (v $tblFields->tableVersion) DB table from $oldTableName (v $oldTableVer)...DONE<br/>\n";
			//----------------------
			if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
			{
				XLogError("install.php - importDatabase - table '$tblName' failed to setTableVersion to ".$tblFields->tableVersion);
				echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
			}
			else XLogNotify("install.php - importDatabase - successfully imported table '$oldTableName' ver $oldTableVer to '$tblName' cur ver ".$tblFields->tableVersion);
			//----------------------
		}// if xpost
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function uninstallTables($prefix = 'bak_')
{
	global $db, $dbConfigFields;
	//----------------------
	$tableList = getTableList();
	//----------------------
	if (!getTableVersions($prefix))
	{
		XLogError("install.php - uninstallTables - getTableVersions failed");
		return false;
	}
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		if (XPost($tblName) != "")
		{
			//----------------------
			list($oldTableVer, $oldTableName) = lookupTableVersion($tblName);
			//----------------------
			if (!$obj->Uninstall())
			{
				XLogError("install.php - uninstallTables - table '$tblName' failed to Uninstall");
				echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
				return false;
			}
			else echo "Uninstalled $tblName (v $tblFields->tableVersion) DB table from $oldTableName (v $oldTableVer).<br/>\n";
			//----------------------
		}// if xpost
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function installTables($prefix = 'bak_')
{
	global $db, $dbConfigFields;
	//----------------------
	$tableList = getTableList();
	//----------------------
	if (!getTableVersions($prefix))
	{
		XLogError("install.php - installTables - getTableVersions failed");
		return false;
	}
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		if (XPost($tblName) != "")
		{
			//----------------------
			list($oldTableVer, $oldTableName) = lookupTableVersion($tblName);
			//----------------------
			if (!$obj->Install())
			{
				XLogError("install.php - installTables - table '$tblName' failed to Uninstall");
				echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
				return false;
			}
			else echo "Installed $tblName (v $tblFields->tableVersion) DB table from $oldTableName (v $oldTableVer).<br/>\n";
			//----------------------
			if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
			{
				XLogError("install.php - installTables - table '$tblName' failed to setTableVersion to ".$tblFields->tableVersion);
				echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
			}
			//----------------------
			
		}// if xpost
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function tableExists($tableName)
{
	global $db;
	//----------------------
	$sql = "SELECT 1 FROM $tableName LIMIT 1";
	//----------------------
	if (!$db->Execute($sql, true/*MustSelectDB*/, true/*QuietMode*/))
	{
		//XLogWarn("install.php - tableExists - test table failed.\nsql: $sql\n".$db->errorString());
		return false;
	}
	//----------------------
	return true;
}
function duplicateTable($srcTable, $newTable, $clobber = true)
{
	global $db;
	//----------------------
	if ($clobber)
	{
		$sql = "DROP TABLE IF EXISTS $newTable";
		//----------------------
		if (!$db->Execute($sql))
		{
			XLogError("install.php - duplicateTable - drop table failed.\nsql: $sql\n".$db->errorString());
			return false;
		}
		//----------------------
	}
	//----------------------
	$sql = "CREATE TABLE $newTable LIKE $srcTable";
	//----------------------
	if (!$db->Execute($sql))
	{
		XLogError("install.php - duplicateTable - create table failed.\nsql: $sql\n".$db->errorString());
		return false;
	}
	//----------------------
	$sql = "INSERT INTO $newTable SELECT * FROM $srcTable";
	//----------------------
	if (!$db->Execute($sql))
	{
		XLogError("install.php - duplicateTable - copy table failed.\nsql: $sql\n".$db->errorString());
		return false;
	}
	//----------------------
	return true;
}
//-------------------------------------------------------------
function updateTables($prefix = 'bak_')
{
	global $db, $dbConfigFields;
	//----------------------
	$tableList = getTableList();
	//----------------------
	if (!getTableVersions($prefix))
	{
		XLogError("install.php - updateTables - getTableVersions failed");
		return false;
	}
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		if (XPost($tblName) != "")
		{
			//----------------------
			list($oldTableVer, $oldTableName) = lookupTableVersion($tblName);
			XLogDebug("install.php - updateTables: '$tblName', ver $oldTableVer, old table '$oldTableName'");
			//----------------------
			if ($oldTableName != $tblFields->tableName && !tableExists($oldTableName))
			{
				XLogWarn("install.php - updateTables - old table name differs and doesn't exist, trying current table name");
				$oldTableName = $tblFields->tableName; // in case table names changed, but config not up to date
			}
			//----------------------
			if (!tableExists($oldTableName))
			{
				//----------------------
				echo "Updating $tblName doesn't exist, installing...<br/>\n";
				XLogDebug("install.php - updateTables - Updating $tblName doesn't exist, installing...");
				if (!$obj->Install())
				{
					XLogError("install.php - updateTables - table '$tblName' failed to Install");
					echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
					return false;
				}
				//----------------------
				if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
				{
					XLogError("install.php - updateTables - table '$tblName' failed to setTableVersion to ".$tblFields->tableVersion);
					echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
				}
				//----------------------
			}
			else if ($oldTableName == $tblFields->tableName && $oldTableVer == $tblFields->tableVersion)
			{
				//----------------------
				echo "Updating $tblName up to date.<br/>\n";
				XLogDebug("install.php - updateTables - $tblName up to date.");
				//----------------------
				// ensure table version is up to date
				if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
				{
					XLogError("install.php - updateTables - table '$tblName' failed to setTableVersion to ".$tblFields->tableVersion);
					echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
				}
				//----------------------
			}
			else // table name or version changed
			{
				//----------------------
				$bakTableName = "$prefix$tblFields->tableName";
				//----------------------
				echo "Updating back up $oldTableName -> $bakTableName DB table...<br/>\n";
				XLogWarn("install.php - Updating back up $oldTableName -> $bakTableName DB table...");
				if (!duplicateTable($oldTableName, $bakTableName))
				{
					XLogError("install.php - updateTables - drop table failed.\nsql: $sql\n".$db->errorString());
					return false;
				}
				//----------------------
				echo "Updating $tblName uninstalling...<br/>\n";
				XLogWarn("install.php - Updating $tblName uninstalling...");
				//----------------------
				if (!$obj->Uninstall())
				{
					XLogError("install.php - updateTables - table '$tblName' failed to Uninstall");
					echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
					return false;
				}
				//----------------------
				echo "Updating $tblName (re)installing...<br/>\n";
				XLogWarn("install.php - Updating $tblName (re)installing...");
				if (!$obj->Install())
				{
					XLogError("install.php - updateTables - table '$tblName' failed to (re)Install");
					echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
					return false;
				}
				//----------------------
				if ($oldTableVer == -1)
					$oldTableVer = 0; // no recorded version, try to assume zero
				//----------------------
				if (!$obj->Import($oldTableVer, $bakTableName))
				{
					XLogError("install.php - updateTables - table '$tblName' failed to Import (old table ver $oldTableVer, bak table name $bakTableName)");
					echo "<span style='font-weight:bold;color:red;'>Failed!</span><br/>\n";
					return false;
				}
				//----------------------
				echo "Updating imported $tblName (v $tblFields->tableVersion) DB table from $bakTableName (v $oldTableVer).<br/>\n";
				XLogWarn("install.php - Updating imported $tblName (v $tblFields->tableVersion) DB table from $bakTableName (v $oldTableVer).");
				//----------------------
				if (!setTableVersion($tblName, $tblFields->tableName, $tblFields->tableVersion))
				{
					XLogError("install.php - updateTables - table '$tblName' failed to setTableVersion to ".$tblFields->tableVersion);
					echo "<span style='font-weight:bold;color:red;'>...but failed to update table version!</span><br/>\n";
				}
				//----------------------
				echo "Updating $tblName DONE<br/>\n";
				XLogWarn("install.php - Updating $tblName DONE");
				//----------------------
			} // if ($oldTableVer == $tblFields->tableVersion)
			//----------------------
		}// if xpost
		//----------------------
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function getTableVersions($prefix = 'bak_')
{
	global $db, $dbConfigFields, $CurTableVersions;
	//----------------------
	$config = new Config();
	$tableList = getTableList();
	//----------------------
	foreach ($tableList as $tbl)
	{
		list($obj, $tblFields, $tblName) = $tbl;
		$ver = $config->Get("dbtablever:$tblName");
		$name = $config->Get("dbtablename:$tblName");
		if ($ver === false && $name === false)
		{
			if ($tblName == $tblFields->tableName)
				XLogDebug("install.php - getTableVersions - no table version found for $tblName");
			else
			{
				// try to grab version from legacy id
				$ver = $config->Get("dbtablever:".$tblFields->tableName);
				$name = $config->Get("dbtablename:".$tblFields->tableName);
				if ($tblFields->tableName || ($tblVer === false && $tblName === false))
					XLogDebug("install.php - getTableVersions - no table version found for $tblName/".$tblFields->tableName);
				else
					XLogDebug("install.php - getTableVersions - '$tblName' not found, trying ".$tblFields->tableName.", got ver ".XVarDump($ver).", name ".XVarDump($name));
			}
		}
		$CurTableVersions[] = array($tblName, $ver, $name);
	} // foreach
	//----------------------
	return true;
}
//-------------------------------------------------------------
function setTableVersion($tblID, $tblName, $tblVersion)
{
	//----------------------
	$config = new Config();
	$config->set("dbtablename:$tblID", $tblName);
	$config->set("dbtablever:$tblID", "".$tblVersion);
	//----------------------
	return true;
}
//-------------------------------------------------------------
function getTableVersion($tblName)
{
	//----------------------
	$config = new Config();
	//----------------------
	return $config->get("dbtablever:$tblName");
}
//-------------------------------------------------------------
function lookupTableVersion($lookupTable)
{
	global $CurTableVersions;
	foreach ($CurTableVersions as $tver)
	{
		list($name, $ver, $tblName) = $tver;
		if ($name == $lookupTable)
			return array( ($ver === false || !is_numeric($ver) ? -1 : (int)$ver), ($tblName === false ? "" : $tblName));
	}		
	return array(-1, "");
}
//---------------------------------
$title = X_TITLE." - Administration - Installation";
//---------------------------------
echo htmlHeader($title);
echo htmlHeaderBar($title);
//---------------------------------
if (!defined('C_INSTALL_PASSWORD') || !defined('C_DEFAULT_USER_FULL_NAME') || !defined('C_DEFAULT_USER_PASS') || !defined('C_DEFAULT_USER_PRIV'))
{
	echo "<div style='color: red;'>Before continuing, you must edit the file www/admin/Settings.php. One of the following is missing: C_INSTALL_PASSWORD, C_DEFAULT_USER_FULL_NAME, C_DEFAULT_USER_PASS, and C_DEFAULT_USER_PRIV.</div>\n";
	echo htmlFooter();
	exit;
}
//---------------------------------
$fnMenuItems = array();
$fnMenuItems[] = "Home";
$fnMenuItems[] = "Admin";
$fnMenuItems[] = "Install";
//---------------
echo htmlMenu($fnMenuItems, "./Install.php", false/*params*/, 2 /*Selected Page Index*/);
//---------------------------------
echo "<!--content -->\n";
//-------------------------------------------------------------
if (!isset($_SESSION[SESS_SECURITY_TOKEN]))
{
	echo '<div class="loginError">Security token missing. Please refresh this page to continue.</div>'."\n";
	exit;
}
//-------------------------------------------------------------
if (XGet('error') == 'login_fail')
{
	echo '<div class="loginError">Login failed. Please check your user name and password, then try again.</div>'."\n";
}
//-------------------------------------------------------------
if (XPost('action') == 'Install')
{
	
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		echo "<div>Creating Database tables...</div>\n";
		if (!createDatabase())
			echo "<div style='color: red;'>Some create database failures occurred.</div>\n";
		else
		{
			echo "<div>Creating Database tables...DONE</div>\n";
			echo "<div style='color: green;'>All create database operations succeeded.</div>\n";
			echo "<div>Initting Database tables...</div>\n";
			if (!initDatabase())
				echo "<div style='color : red;'>Some init database failures occurred.</div>\n";
			else
			{
				echo "<div>Initting Database tables...DONE</div>\n";
				echo "<div style='color: green;'>All init database operations succeeded.</div>\n";
				echo "<div>Setting Database table versions...</div>\n";
				if (!setTableVersions())
					echo "<div style='color : red;'>Store database table versions in config failed.</div>\n";
				else
				{
					echo "<div>Setting Database table versions...DONE</div>\n";
					echo "<div style='color: green;'>All version operations succeeded.</div>\n";
					echo "<div>Initting Automation handler...</div>\n";
					$Automation = new Automation() or die("Create object failed");
					if (!$Automation->Install())
						echo "<div style='color: red;'>Install Automation failed.</div>\n";
					else
					{
						echo "<div>Initting Automation handler...DONE</div>\n";
						echo "<div style='color: green;'>Install Automation succeeded.</div>\n";
						echo "<div style='color: green;'>Complete installation process succeeded.</div>\n";
					}
				}
			}
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Uninstall')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		if (!dropDatabase())
		{
			echo "<div style='color: red;'>Some drop failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All Uninstallation operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Backup')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!backupDatabase($prefix))
		{
			echo "<div style='color: red;'>Some backup failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All backup operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Update')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!updatetables($prefix))
		{
			echo "<div style='color: red;'>Some update failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All update operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Import')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!importDatabase($prefix))
		{
			echo "<div style='color: red;'>Some import failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All import operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Uninstall Tables')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!uninstallTables($prefix))
		{
			echo "<div style='color: red;'>Some table uninstall failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All table uninstall operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Install Tables')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!installTables($prefix))
		{
			echo "<div style='color: red;'>Some table install failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All table install operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Update Tables')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!updateTables($prefix))
		{
			echo "<div style='color: red;'>Some table update failures occurred.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>All table update operations succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Redetect Commands')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$prefix = XPost("prefix");
		if ($prefix == "")
			$prefix = 'bak_';
		if (!redetectCommands($prefix))
		{
			echo "<div style='color: red;'>Redetect commands failed.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>Redetect commands succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Generate Sodium Keypair')
{
	//----------------------
	$tunneler = new SodiumTunneler(true /*debugMode*/) or die('Create object failed');
	$keys = $tunneler->generateKeypair();
	if ($keys === false)
		echo "<div style='color: red;'>Generated Sodium Keypair failed</div>";
	else
	{
		echo "<div>Generated Sodium Keypair (base64 ed25519 signing keys):<br/>\n";
		echo "&nbsp;Public Key:&nbsp;".$tunneler->encode64($keys['pub'])."<br/> \n";
		echo "&nbsp;Private Key:&nbsp;".$tunneler->encode64($keys['priv'])."<br/> \n";
		echo "</div>\n";
	}
	//----------------------
}
else if (XPost('action') == 'Set Engines')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$result = checkTableEngines(true /*doSet*/);
		if ($result != "")
		{
			echo "<div style='color: red;'>Some set MySQL table engine operations failed or not complete.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>Set MySQL table engines succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
else if (XPost('action') == 'Set Indexes')
{
	//----------------------
	if (XPost('pass') == C_INSTALL_PASSWORD)
	{
		$result = checkTableIndexes(true /*doSet*/);
		if ($result != "")
		{
			echo "<div style='color: red;'>Some set MySQL table index operations failed or not complete.</div>\n";
		}
		else
		{
			echo "<div style='color: green;'>Set MySQL table indexes succeeded.</div>\n";
		}
	}
	else
	{
		echo "Password Incorrect";
		sleep(3);
		exit;
	}
	//----------------------
}
//-------------------------------------------------------------
?>
<div>
<br/>
<?php
$filePermResults = checkFilePerms();
if ($filePermResults != "")
{
	echo "<div style='padding:10px;margin:20px;float:left;width:600px;border:1px solid black;'><span style='color:red;font-weight:bold;'>Warning, these file permissions are required:</span><br/><p>$filePermResults</p></div>";
}
?>
<br/>
<form action="./Install.php" method="post" enctype="multipart/form-data">
	<fieldset style="margin:auto; width:500px; padding: 10px; background-color:#AAAAAA;color:black;">
		<legend>Database Installation Manager</legend>
		<?php 
			PrintSecTokenInput(); 
			$result = checkDependencies();
			if ($result != "")
				echo "<div style='padding:10px;margin:20px;float:left;width:500px;border:1px solid black;'><span style='font-weight:bold;'>Missing Dependencies Detected:<br/></span><p>$result</p></div>";
			else
			{
				$result = checkTableVersions();
				if ($result != "")
					echo "<div style='padding:10px;margin:20px;float:left;width:500px;border:1px solid black;'><span style='font-weight:bold;'>Database Detected:<br/></span><p>$result</p></div>";
				else
				{
					$result = checkTableEngines();
					if ($result != "")
						echo "<div style='padding:10px;margin:20px;float:left;width:500px;border:1px solid black;'><span style='font-weight:bold;'>Database Engines Detected:<br/></span><p>$result</p></div>";
					else
					{
						$result = checkTableIndexes();
						if ($result != "")
							echo "<div style='padding:10px;margin:20px;float:left;width:500px;border:1px solid black;'><span style='font-weight:bold;'>Database Table Indexes Detected:<br/></span><p>$result</p></div>";
					}
				}
				$result = checkCommands();
				if ($result != "")
					echo "<div style='padding:10px;margin:20px;float:left;width:500px;border:1px solid black;'><span style='font-weight:bold;'>Commands Detected:<br/></span><p>$result</p></div>";
			}
		?>
		<br/>
		<div style="align:center;width:500px;">
			<div style="width:300px;">
				<label class="loginLabel" for="pass">Install Page Password:</label><br/>
				<input id="pass" type="password" name="pass" value="" /><br/>
			</div>
		</div>
		<br/>
		<div style="width:500px;">
			<div style="float:left;margin-top:10px;">
				<input type="submit" name="action" value="Uninstall" />&nbsp;
				<input type="submit" name="action" value="Install" /><br/>
				<br/>
				<input type="submit" name="action" value="Redetect Commands" /><br/>
				<input type="submit" name="action" value="Generate Sodium Keypair" /><br/>
				<br/>
			</div>
			<div style="float:right;width:45%;border;1px solid black;">
				<?php
					//----------------------
					$tableList = getTableList();
					//----------------------
					foreach ($tableList as $tbl)
					{
						list($obj, $tblFields, $tblName) = $tbl;
						if ($tblName != "Login")
							echo "<input type='checkbox' id='$tblName' name='$tblName' value='$tblName' />&nbsp;$tblName<br/>\n";
					}
					//----------------------
				?>
				<br/>
				<label style="margin-bottom:2px;" class="loginLabel" for="prefix">Backup/Import Prefix:</label><br/>
				<input style="margin-bottom:4px;" id="prefix" type="text" name="prefix" value="bak_" /><br/>
				<input style="margin-bottom:4px;" type="submit" name="action" value="Update" /><br/>
				<input style="margin-bottom:4px;" type="submit" name="action" value="Backup" />&nbsp;&nbsp;<input type="submit" name="action" value="Import" /><br/>
				<input style="margin-bottom:4px;" type="submit" name="action" value="Set Engines" /><br/>
				<input style="margin-bottom:4px;" type="submit" name="action" value="Set Indexes" />&nbsp;<input type='checkbox' id='Indexes_Del_Extra' name='Indexes_Del_Extra' value='Indexes_Del_Extra' />Drop Extra<br/>
				<input style="margin-bottom:4px;" type="submit" name="action" value="Install Tables" />&nbsp;&nbsp;<input type="submit" name="action" value="Uninstall Tables" /><br/>
			</div>
		</div>
	</fieldset>
</form>
</div>
<br/>
<?php
//---------------------------------
echo "<!--content end-->\n";
//---------------------------------
echo htmlFooter();
//---------------------------------
?>
