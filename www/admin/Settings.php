<?php
//-------------------------------------------------------------
/*
 *	www/admin/Settings.php
 * 
 *  This file contains user configurable values (SQL, RPC, web user).
 * 
 *  Some values must be set, noted as @CHANGEME.
 * 		* Install.php access password
 * 		* Default web user created when installing
 * 		* Wallet RPC access
 * 		* MYSQL database access
 * 
 *  Some values have defaults which you can change, noted as @CUSTOMIZE
 * 		* Branding
 * 		* Coin specifications
 * 		* Debugging options
 * 		
 * 	Other values adjust logging, debugging, and related features.
 * 
*/
//-------------------------------------------------------------
// The following user configuration values must be text/strings unless noted.
//--------------------------- Install
//define('C_INSTALL_PASSWORD', 		''); // @CHANGEME // password to execute www/admin/Install.php which you should protect by disabling when not in use
define('C_DEFAULT_USER_NAME', 		'admin'); // this is the one default admin user created by the installer
define('C_DEFAULT_USER_FULL_NAME', 	'Administrator'); // display name for default admin user created by the installer
//define('C_DEFAULT_USER_PASS', 		''); // @CHANGEME password for default admin user created by the installer
define('C_DEFAULT_USER_PRIV', 		'U'); // leave this as 'U' so the admin user can only create/manage users (then make another account with other privileges)
//--------------------------- Wallet
define('WALLET_RPC_HOST', 'localhost'); // the wallet's RPC hostname
define('WALLET_RPC_PORT', '22555'); // the wallet's RPC port (Dogecoin default 22555)
define('WALLET_RPC_AUTH', true); // true/false use wallet's RPC user/password authentication
define('WALLET_RPC_USER', 'dfahuser'); // @CHANGEME // the wallet's RPC user name
//define('WALLET_RPC_PASS', ''); // @CHANGEME // the wallet's RPC password
//---------------
define('WALLET_SSH_TUNNEL', false); // true/false connect wallet's RPC through a SSH tunnel using the PHP SSH2 module
define('WALLET_SSH_HOST', ''); // SSH hostname
define('WALLET_SSH_PORT', '22'); // SSH port
define('WALLET_SSH_USER', ''); // SSH user name used with both password and pub/priv key authentication
define('WALLET_SSH_PASSWORD', false); // string/false SSH password for username/password authentication, false for disabled (recomended, use pub/priv keys)
define('WALLET_SSH_PUB_KEY_FILE', ''); // file name of SSH public key on web server
define('WALLET_SSH_PRIV_KEY_FILE', ''); // file name of SSH private key on web server
define('WALLET_SSH_PRIV_KEY_PASSWORD', false); // string/false SSH private key passphrase, false for disabled (support may vary)
define('WALLET_SSH_SERVER_FINGERPRINT', false); // string/false SSH server fingerprint to verify, false for ignore
//---------------
define('WALLET_SODIUM_TUNNEL', false); // true/false connect wallet's RPC through a specialized SodiumTunnelServer. see see tools/SodiumTunnelServer/STunnel.php
define('WALLET_SODIUM_HOST', ''); // hostname to SodiumTunnelServer
define('WALLET_SODIUM_USER', ''); // this remote username for authenticating with SodiumTunnelServer
define('WALLET_SODIUM_PUB_KEY', ''); // ed25519 base64 encoded public key for this SodiumTunnelServer remote user's authentication
define('WALLET_SODIUM_PRIV_KEY', ''); // ed25519 base64 encoded public key for this SodiumTunnelServer remote user's authentication
define('WALLET_SODIUM_SERVER_USER', ''); // SodiumTunnelServer's authentication username (in stunnel.cfg)
define('WALLET_SODIUM_SERVER_PUB_KEY', '');  // ed25519 base64 encoded public key for SodiumTunnelServer's authentication
//--------------------------- Database
define('XDATABASE_HOST', 'localhost'); // the database's hostname
define('XDATABASE_NAME', 'dogecoinfah'); // @CHANGEME // the database name
define('XDATABASE_USER', 'dfahuser'); // @CHANGEME // the database user name
//define('XDATABASE_PASS', ''); // @CHANGEME // the database user password
//--------------------------- Folding API
define('FOLD_API_PROXY', false); // hostname or false for disabled, specialized proxy server for accessing folding api. see tools/FoldAPIProxy/FoldAPIProxy.php
define('FOLD_API_PROXY_KEY', ''); // Passkey for access to FoldAPIProxy
//--------------------------- Branding
define('BRAND_FOLDING_TEAM_ID', 226715); // [number] @CUSTOMIZE folding team ID
define('BRAND_ADDRESS_LINK', 'http://dogechain.info/address/'); // @CUSTOMIZE block explorer link for address (address is appended)
define('BRAND_TX_LINK', 'http://dogechain.info/tx/'); // @CUSTOMIZE block explorer link for transaction (txid is appended)
define('BRAND_MARKET_ASSET_ID', MARKET_CLIENT_ASSET_DOGECOIN); // [constant] @CUSTOMIZE identify your currency's characteristics in www/admin/include/MarketConfig.php's list
define('BRAND_NAME', 'Dogecoin'); // @CUSTOMIZE currency name
define('BRAND_UNIT', 'DOGE'); // @CUSTOMIZE currency unit
define('BRAND_COPYRIGHT', 'DogecoinFah (c) 2015'); // @CUSTOMIZE used in footer template
define('BRAND_TX_COMMENT', 'DogecoinFah round '); // @CUSTOMIZE comment added to transaction (not used any more)
define('BRAND_DB_PREFIX', 'dogefah_'); // @CUSTOMIZE database tables will be named starting with this prefix
define('X_TITLE', 'DogecoinFah'); // @CUSTOMIZE the title of the webpage (admin pages may append to this)
define('X_SESSION_NAME', 'Dogecoinfah'); // @CUSTOMIZE session id for session cookies (see PHP session_id)
//---------------------------
define('XDBFIELDS_FORCE_BOOL_TO_INT', true); // XDBFields to not just convert PDO prepared bool values to 1 or 0, but also to set the type as PDO::PARAM_INT not PDO::PARAM_BOOL
define('MYSQL_REAL_ESCAPE', true); // [bool] this enables detecting if extra SQL sanitation is needed for older PHP versions < 7 (see PHP mysql_real_escape_string)
define('XIS_DEBUG', false); // [bool] adds web cache disabling HTML to pages, enables all web server error reporting (see PHP error_reporting)
//---------------------------
define('XLOG_MAX_LEVEL', 4); // [number] 0 All, 1 Error, 2 Warn, 3 User, 4 Notify, 5 Debug 
//---------------------------
define('XLOG_FILENAME', './log/log.txt'); // This file name will be the base used for multiple log file names (text may be added to the final filename)
define('XLOG_TIMEZONE', 'America/Chicago'); // Time zone used for time-stamping log entries. 'UTC', /etc/timezone, or see PHP DateTimeZone
define('XLOG_COMBINED', true); // [bool] creates ALL log file with log entries from all levels combined
define('XLOG_SEPERATE', false); // [bool] creates separate log files for each debug level
//define('XLOG_LABEL', ''); // main label to put on every log entry after the timestamp if any, default none (level labels are automatic)
//define('XLOG_TIMESTAMP_FORMAT', 'm/d/Y G:i'); // default 'm/d/Y G:i' (see PHP  DateTime::format)
define('XLOG_ARCHIVE_FILESIZE', 2500000); // bytes [number] after this size a log file will be compressed to the archive folder and start over
//define('XLOG_ARCHIVE_DIR', './log/archive'); // defaults to 'archive' subdirectory of XLOG_FILENAME's directory
//define('XLOG_LINE_DELIMITTER', "\n"); // defaults to \n  (use double quotes)
//---------------------------
?>
