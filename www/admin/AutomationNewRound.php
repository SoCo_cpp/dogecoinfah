<?php
//-------------------------------------------------------------
/*
*	AutomationNewRound.php
*
* This page shouldn't be accessible to the web.
* This script should be executed every time a round
* ends, like once a week.
* 
* This script will simply create a new round.
* 
*/
//-------------------------------------------------------------
//---------------------------
require('./include/Init.php');
//---------------------------
XLogNotify("AutomationNewRound.php activated");
//---------------------------
class AutomationNewRound
{
	//---------------------------
	function Main()
	{
		$Rounds = new Rounds() or die("Create object failed");
		$Config = new Config() or die("Create object failed");
		//------------------
		$teamId = $Config->Get(CFG_ROUND_TEAM_ID);
		if ($teamId === false)
		{
			XLogError("AutomationNewRound::Main no round teamId set");
			return false;
		}
		//------------------
		if (!is_numeric($teamId))
		{
			XLogError("AutomationNewRound::Main validate teamId failed");
			return false;
		}
		//------------------
		$flags = $Config->GetSetDefault(CFG_ROUND_FLAGS, DEFAULT_ROUND_FLAGS);
		if ($flags === false)
		{
			XLogError("AutomationNewRound::Main Config GetSetDefault round flags failed");
			return false;
		}
		//------------------
		XLogNotify("AutomationNewRound::Main adding new round with pay flags: $flags");
		if (!$Rounds->addRound($teamId, $flags, true/*start round check automation*/))
		{
			XLogError("AutomationNewRound::Main rounds failed to addRound");
			return false;
		}
		//------------------
		$Automation = new Automation() or die("Create object failed");
		if (!$Automation->UpdateLastNewRound())
		{
			XLogError("AutomationNewRound::Main Automation UpdateLastNewRound failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
} // class AutomationNewRound
//---------------------------
$ar = new AutomationNewRound() or die("Create object failed");
if (!$ar->Main())
	echo "failed";
else
	echo "ok";
//---------------------------
?>
