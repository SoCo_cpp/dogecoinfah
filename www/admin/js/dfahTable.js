/*  dfahTable.js
 * 
 * 
 * 
 * 
 * 
*/

var dfahTable = {
			initted: false,
			domID: null,
			domBody: null,
			domTopRow: null,
			domBotRow: null,
			dataSet: false,
			colCount: 0,
			rows: [],
			display: {
				updateDelay: 50, // ms
				updating: false,
				isEmpty: true,
				defRowOffsetHeight: 0,
				defRowClientHeight: 0,				
				lastScrollTop: -1,
				updateTimeout: null
			}
		};
		
function dfahTableInit(domID)
{
	var eTable = document.getElementById(domID);
	if (!eTable)
		return 'tbl val tbl el failed';
	var eBodyList = eTable.getElementsByTagName('tbody');
	if (!eBodyList || eBodyList.length < 1 || !eBodyList[0])
		return 'tbl val body el  failed';
	dfahTable.domID = domID;
	dfahTable.domBody = eBodyList[0];
	
	dfahTable.domBody.addEventListener('scroll', dfahTableScroll);
	         
	dfahTable.initted = true;
	var ret = dfahTableDisplay();
	if (ret !== true)
		return 'tbl init dsp failed: '+ret;
	return true;
}

function dfahTableAppendData(data)
{
	if (!Array.isArray(data))
		return 'tbl app val data failed';
	var len = data.length;
	if (len == 0)
		return 'tbl app data is empty';
	var colCount = data[0].length;
	if (!dfahTable.dataSet)
		dfahTable.colCount = colCount;
	else if (dfahTable.blocks.colCount != colCount)
		return 'tbl app colCount mismatch';
	var rLen = dfahTable.rows.length;
	for (var r = 0;r<len;r++)
		if (data[r].length != colCount)
			return 'tbl app col mismatch, r '+r+', '+data[r].length+'/'+colCount;
		else
		{
			//data[r][0] = rLen; // this helps debugging
			dfahTable.rows[rLen++] = data[r];
		}
	dfahTable.dataSet = true;
	var ret = dfahTableDisplay();
	if (ret !== true)
		return 'tbl app dsp failed: '+ret;
	return true;
}

function dfahTableNewRow()
{
	var newRow = document.createElement('tr')
	if (!newRow)
	{
		console.log('tbl nr val nr failed');
		return null;
	}
	for (var c = 0;c < dfahTable.colCount;c++)
	{
		var newCol = document.createElement('td')
		if (!newCol)
		{
			console.log('tbl nr val nc failed');
			return null;
		}
		newCol.innerHTML = '&nbsp;';
		newRow.appendChild(newCol);
	}
	return newRow;
}

function dfahTableScroll(event)
{	
	if (dfahTable.display.updating || dfahTable.display.updateTimeout !== null)
		return;
	dfahTable.display.updateTimeout = setTimeout(function(){ var ret = dfahTableDisplay(); if (ret !== true) console.log('tbl t/o dsp failed: '+ret); }, dfahTable.display.updateDelay);
}

function dfahTableDisplay()
{
	if (!dfahTable.initted)
		return 'tbl dsp not initted';
	if (!dfahTable.domBody)
		return 'tbl dsp val domBody failed';
	if (dfahTable.display.updating)
		return true; // skip silently
	
	var rLen = dfahTable.rows.length;	
	if (!dfahTable.dataSet || rLen == 0)
	{
		dfahTable.display.isEmpty = true;
		//console.log('tbl dsp nothing to show');
		return true;
	}
	var ret = dfahTableUpdate();
	if (ret !== true)
		return 'tbl dsp up failed: '+ret;
	return true;
}

function dfahTableUpdate()
{
	if (dfahTable.display.updateTimeout !== null)
	{
		clearTimeout(dfahTable.display.updateTimeout);
		dfahTable.display.updateTimeout = null;
	}
	if (dfahTable.display.lastScrollTop == dfahTable.domBody.scrollTop)
		return true; // no change
	if (dfahTable.display.defRowOffsetHeight == 0 || dfahTable.display.defRowClientHeight == 0)
	{
		dfahTableFillRows(0, 1);
		dfahTable.display.defRowOffsetHeight = dfahTable.domBody.children[0].offsetHeight;
		dfahTable.display.defRowClientHeight = dfahTable.domBody.children[0].clientHeight;
		//console.log('defRowHeight set to '+dfahTable.display.defRowClientHeigh+'/'+dfahTable.display.defRowOffsetHeight);
	}		
	if (dfahTable.display.defRowOffsetHeight == 0 || dfahTable.display.defRowClientHeight == 0)
		return 'tlb up row height not set';
	dfahTable.display.lastScrollTop = dfahTable.domBody.scrollTop;
	var rowCount = dfahTable.rows.length;
	var curTopRow = dfahTable.domBody.scrollTop / dfahTable.display.defRowOffsetHeight;
	if (curTopRow > rowCount || curTopRow < 0)
		curTopRow = 0;
	var curBotRow = (dfahTable.domBody.clientHeight / dfahTable.display.defRowOffsetHeight) + curTopRow;
	if (curBotRow > rowCount)
		curBotRow = rowCount;
	dfahTable.display.updating = true;
	if (dfahTable.domTopRow !== null)
	{
		dfahTable.domTopRow.remove();
		dfahTable.domTopRow = null;
	}
	var ret = dfahTableFillRows(Math.floor(curTopRow), Math.ceil(curBotRow));
	if (ret !== true)
		return 'tbl up fr failed: '+ret;
	dfahTable.display.topOffset = (curTopRow * dfahTable.display.defRowOffsetHeight) - (dfahTable.display.defRowOffsetHeight-dfahTable.display.defRowClientHeight);
	if (dfahTable.display.topOffset != 0)
	{
		var newRow = dfahTableNewRow();
		if (newRow === null)
			return 'tbl up nr failed';
		newRow.style.height = dfahTable.display.topOffset.toFixed(0)+'px';
		dfahTable.domTopRow = newRow;
		var el = dfahTable.domBody.getElementsByTagName('tr');
		el = (el && el.length >= 1 ? el[0] : null);
		if (el === null)
			return 'tbl up first failed';
		dfahTable.domBody.insertBefore(newRow, el);
	}
	if (dfahTable.domBotRow !== null)
	{
		if (curBotRow >= rowCount)
			dfahTable.domBotRow.style.height = '0px';
		else
			dfahTable.domBotRow.style.height = ((rowCount - curBotRow) * dfahTable.display.defRowOffsetHeight).toFixed(0)+'px';
	}
	dfahTable.display.updating = false;
	//console.log('colCount '+dfahTable.colCount+' row height : '+dfahTable.display.defRowClientHeight+'/'+dfahTable.display.defRowOffsetHeight+' cur top row: '+curTopRow.toFixed(2) + ' cur bot row: '+ curBotRow.toFixed(2)+' / '+rowCount+', top ('+dfahTable.display.topOffset.toFixed(2)+') '+ (dfahTable.domTopRow === null ? 'null' : dfahTable.domTopRow.style.height)+' bot '+(dfahTable.domBotRow === null ? 'null' : dfahTable.domBotRow.style.height));
	return true;
}

function dfahTableFillRows(rStart, rStop)
{
	var rCount = rStop - rStart;
	var dataRowLen = dfahTable.rows.length;
	if (rStop < dataRowLen) rCount++; // not at end, add extra spacer row
	var domRowList = dfahTable.domBody.getElementsByTagName('tr');
	if (!domRowList)
		return 'tbl fr val domRowList failed';
	var rLen = domRowList.length;
	while (rLen < rCount)
	{
		var newRow = dfahTableNewRow();
		if (newRow === null)
			return 'tbl fr nr failed';
		dfahTable.domBody.appendChild(newRow);
		domRowList[rLen++] = newRow;
	}
	while (rLen > rCount && rLen > 1)
	{
		rLen--;
		if (domRowList[rLen] == dfahTable.domBotRow)
			dfahTable.domBotRow = null;		
		domRowList[rLen].remove();
	}
	if (rLen < 2 || dfahTable.domBotRow != domRowList[rLen-1])
	{
		if (dfahTable.domBotRow !== null)
			dfahTable.domBotRow.style.height = 'auto';
		if (rLen >= 2 && rStop < dataRowLen) // don't need spacer for last row
		{
			dfahTable.domBotRow = domRowList[rLen-1];
			dfahTable.domBotRow.style.height = '0px';
		}
		else dfahTable.domBotRow = null;
	}	
	var c;
	var dspRow;
	var row;
	var dspIdx = 0;
	for (var r = rStart;r < rStop;r++)
	{
		if (dspIdx >= rLen || r >= dataRowLen || dfahTable.rows[r] === undefined || domRowList[dspIdx] === undefined)
			return 'fr '+(dfahTable.rows[r] === undefined ? 'undef' : 'overflow')+': dspR '+dspIdx+'/'+rLen+', row '+r+' of ('+rStart+' - '+rStop+')='+rCount+' / '+dataRowLen;
		dspRow = domRowList[dspIdx++];
		row = dfahTable.rows[r];
		for (c = 0;c < dfahTable.colCount;c++)
			dspRow.children[c].innerHTML = row[c];
	}
	return true;
}
