<?php
//---------------------------------
define('IS_ADMIN', true);
//---------------------------------
require('./include/Init.php');
//---------------------------------
$type = XGetPost('t');
$id = XGetPost('i');
//---------------------------------
function failDie($msg)
{
	echo "<h2>DogecoinFah Admin External Launcher</h2>\n";
	echo "<p>$msg</p>\n";
	die();
}
//---------------------------------
if ($type === false || $type == '' || !is_numeric($id))
	failDie('Validate parameters failed');
//---------------------------------
if ($type == 'wa')
{
	$Workers = new Workers() or die("Create object failed");
	//---------------------------------
	$worker = $Workers->getWorker($id);
	if ($worker === false)
		failDie('(wa) Worker '.XEncodeHTML($id).' not found.');
	//---------------------------------
	header('Location: '.BRAND_ADDRESS_LINK.XEncodeHTML($worker->address));
	exit;
	//---------------------------------
}
else if ($type == 'pt')
{
	$Payouts = new Payouts() or die("Create object failed");
	//---------------------------------
	$payout = $Payouts->getPayout($id);
	if ($payout === false)
		failDie('(pt) Payout '.XEncodeHTML($id).' not found.');
	//---------------------------------
	header('Location: '.BRAND_TX_LINK.XEncodeHTML($payout->txid));
	exit;
	//---------------------------------
}
//---------------------------------
failDie('Unsupported type: '.XEncodeHTML($type));
//---------------------------------
?>
