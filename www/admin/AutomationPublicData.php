<?php
//-------------------------------------------------------------
/*
*	AutomationPublicData.php
*
* This page shouldn't be accessible to the web.
* 
* 
*/
//-------------------------------------------------------------
require('./include/Init.php');
//---------------------------
class AutomationPublicData
{
	//---------------------------
	function formatDate($mixedDate)
	{
		//---------------------------
		$dt = XMixedToDate($mixedDate);
		if ($dt === false)
		{
			XLogWarn("AutomationPublicData::formatDate XMixedToDate failed input: ".XVarDump($mixedDate));
			return false;
		}
		//---------------------------
		return $dt->format(DATE_ISO8601);
	}
	//---------------------------
	function Main()
	{
		//---------------------------
		$Automation = new Automation() or die("Create object failed");
		//---------------------------
		if (!$this->tryLock())
		{
			XLogWarn("AutomationPublicData::Main tryLock failed");
			return false;
		}
		//------------------
		$ret = $this->step();
		if ($ret === false)
			XLogError("AutomationPublicData::Main Automation step failed");
		//---------------------------
		if ($ret === true)
		{
			//---------------------------
			if (!$Automation->UpdateLastPublicData())
			{
				XLogError("AutomationPublicData::Main Automation UpdateLastPublicData failed");
				$ret = false;
			}
			//---------------------------
			if ($ret === true && !$Automation->ClearPublicDataNoAction())
			{
				XLogError("AutomationPublicData::Main Automation ClearPublicDataNoAction failed");
				return false;
			}
			//---------------------------
		}
		else
		{
			//---------------------------
			if (!$Automation->IncPublicDataNoAction())
			{
				XLogError("AutomationPublicData::Main Automation IncPublicDataNoAction failed");
				$ret == false;
			}
			//---------------------------
		}
		//---------------------------
		if (!$this->unlock())
		{
			XLogWarn("AutomationPublicData::Main unlock failed");
			$ret = false;
		}
		//---------------------------
		return $ret;
	}
	//---------------------------
	function tryLock()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		$MutexTime = $Config->Get(CFG_PUBLIC_AUTOMATION_LOCK);
		//---------------------------
		if ($MutexTime !== false && $MutexTime !== "")
		{
			//------------------
			$minDiff = XDateTimeDiff($MutexTime, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
			if ($minDiff === false)
			{
				XLogError("AutomationPublicData::tryLock XDateTimeDiff failed. Cannot verify MutexTime: '$MutexTime'");
				return false;
			}
			//------------------
			if ($minDiff < PUBLIC_STALE_MUTEX_AGE)
			{
				XLogNotify("AutomationPublicData::tryLock Mutex is still locked. Locked '".CFG_PUBLIC_AUTOMATION_LOCK."' at '$MutexTime', $minDiff minutes ago.");
				return false;
			}
			else XLogNotify("AutomationPublicData::tryLock ignoring stale mutex '".CFG_PUBLIC_AUTOMATION_LOCK."' that is $minDiff minutes old.");
			//------------------
		}
		//------------------
		$nowUtc = new DateTime('now',  new DateTimeZone('UTC'));
		$strNowUtc = $nowUtc->format(MYSQL_DATETIME_FORMAT);
		//------------------
		if (!$Config->Set(CFG_PUBLIC_AUTOMATION_LOCK, $strNowUtc))
		{
			XLogError("AutomationPublicData::tryLock Config set mutex failed");
			return false;
		}
		//------------------
		$MutexTime = $Config->Get(CFG_PUBLIC_AUTOMATION_LOCK);
		//------------------
		if ($MutexTime !== $strNowUtc)
		{
			XLogWarn("AutomationPublicData::tryLock verify set mutex failed. Lost the race? Current mutex '$MutexTime', but just set it to '$strNowUtc'");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function unlock()
	{
		//------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_AUTOMATION_LOCK, ""))
		{
			XLogError("AutomationPublicData::unlock Config set mutex to blank failed");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function incManifest($asset, $writeToFile = true)
	{
		//------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		$strData = $Config->Get(CFG_PUBLIC_DATA_MANIFEST, "");
		if ($strData === false)
		{
			XLogError("AutomationPublicData::incManifest Config Get manifest data failed");
			return false;
		}
		//---------------------------
		$strData = ltrim($strData, 'x');
		$strData = str_pad($strData, 26, '0');
		$mfData = str_split($strData, 2);
		//---------------------------
		$val = $mfData[$asset];

		if (!is_numeric($val))
			$val = 0;
		$val = hexdec($val);
		$val++;
		if ($val > 255)
			$val = 0;
		$val = dechex($val);
		$mfData[$asset] = str_pad($val, 2, '0', STR_PAD_LEFT);
		$strData = implode('', $mfData);
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_DATA_MANIFEST, 'x'.$strData))
		{
			XLogError("AutomationPublicData::incManifest Config set manifest data failed");
			return false;
		}
		//---------------------------
		if ($writeToFile && !$this->writeManifest($strData))
		{
			XLogError("AutomationPublicData::incManifest writeManifest failed");
			return false;
		}
		//---------------------------
		return true;
	}	
	//---------------------------
	function writeManifest($strData = false)
	{
		//---------------------------
		if ($strData === false)
		{
			//---------------------------
			$Config = new Config() or die("Create object failed");
			//---------------------------
			$strData = $Config->Get(CFG_PUBLIC_DATA_MANIFEST, "");
			if ($strData === false)
			{
				XLogError("AutomationPublicData::writeManifest Config Get manifest data failed");
				return false;
			}
			//---------------------------
			$strData = ltrim($strData, 'x');
			$strData = str_pad($strData, 26, '0');
			//---------------------------
		}
		else if (!is_string($strData) || strlen($strData) < 20)
		{
			XLogError("AutomationPublicData::writeManifest validate strData parameter failed: ".XVarDump($strData));
			return false;
		}
		//---------------------------
		$strData = time().','.$strData;
		if (!@file_put_contents(PUBLIC_DATA_MANIFEST_OUTPUT, $strData))
		{
			XLogError("AutomationPublicData::writeManifest file_put_contents failed");
			XLogWarn("AutomationPublicData::writeManifest failed to write text data file: ".PUBLIC_DATA_MANIFEST_OUTPUT." (".realpath(PUBLIC_DATA_MANIFEST_OUTPUT)."), is writable: ".(is_writable(PUBLIC_DATA_MANIFEST_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_DATA_MANIFEST_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_DATA_MANIFEST_OUTPUT))." bytes.");
			return false;
		} 
		//---------------------------
		return true;
	}
	//---------------------------
	function isPublicStepEnabled($step, $failValue = false, $enabledValue = 1, $disabledValue = 0)
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		if (!is_numeric($step) || $step < 0)
		{
			XLogError("AutomationPublicData::isPublicStepEnabled validate step parameter failed: ".XVarDump($step));
			return $failValue;
		}
		//---------------------------
		if ($step == PUBDATA_STEP_NONE || $step == PUBDATA_STEP_DONE)
			return $enabledValue; // these are always enabled
		//---------------------------
		$enabledMask = $Config->GetSetDefault(CFG_PUBLIC_STEPS_ENABLED, DEF_CFG_PUBLIC_STEPS_ENABLED);
		if ($enabledMask === false)
		{
			XLogError("AutomationPublicData::isPublicStepEnabled Config step GetSetDefault failed");
			return $failValue;
		}
		//---------------------------
		if (!is_numeric($enabledMask) || $enabledMask < 0)
		{
			XLogError("AutomationPublicData::isPublicStepEnabled validate configured enabledMask failed: ".XVarDump($enabledMask));
			return $failValue;
		}
		//---------------------------
		//XLogDebug("AutomationPublicData::isPublicStepEnabled mask $enabledMask, step $step, result value ".($enabledMask & (1 << $step)).", enabled value ".XVarDump($enabledValue).", disabled value ".XVarDump($disabledValue));
		return ( ($enabledMask & (1 << $step)) != 0 ? $enabledValue : $disabledValue);
	}
	//---------------------------
	function isPublicIdleStepEnabled($step, $failValue = false, $enabledValue = 1, $disabledValue = 0)
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		if (!is_numeric($step) || $step < 0)
		{
			XLogError("AutomationPublicData::isPublicIdleStepEnabled validate step parameter failed: ".XVarDump($step));
			return $failValue;
		}
		//---------------------------
		if ($step == PUBDATA_IDLE_SUBSTEP_NONE || $step == PUBDATA_IDLE_SUBSTEP_DONE)
			return $enabledValue; // these are always enabled
		//---------------------------
		$enabledMask = $Config->GetSetDefault(CFG_PUBLIC_IDLE_STEPS_ENABLED, DEF_CFG_PUBLIC_IDLE_STEPS_ENABLED);
		if ($enabledMask === false)
		{
			XLogError("AutomationPublicData::isPublicIdleStepEnabled Config step GetSetDefault failed");
			return $failValue;
		}
		//---------------------------
		if (!is_numeric($enabledMask) || $enabledMask < 0)
		{
			XLogError("AutomationPublicData::isPublicIdleStepEnabled validate configured enabledMask failed: ".XVarDump($enabledMask));
			return $failValue;
		}
		//---------------------------
		//XLogDebug("AutomationPublicData::isPublicIdleStepEnabled mask $enabledMask, step $step, result value ".($enabledMask & (1 << $step)).", enabled value ".XVarDump($enabledValue).", disabled value ".XVarDump($disabledValue));
		return ( ($enabledMask & (1 << $step)) != 0 ? $enabledValue : $disabledValue);
	}
	//---------------------------
	function step()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		if (!$this->udpateBalanceData())
		{
			XLogError("AutomationPublicData::step udpateBalanceData failed");
			return false;
		}
		//---------------------------
		$step = $Config->Get(CFG_PUBLIC_STEP);
		if ($step === false || !is_numeric($step) || $step == PUBDATA_STEP_NONE)
		{
			//---------------------------
			$generalLastRound = $Config->Get(CFG_PUBLIC_LAST_ROUND);
			if (!is_numeric($generalLastRound))
				$generalLastRound = false;
			//---------------------------
			$lastRound = $Config->Get(CFG_ROUND_LAST_DONE);
			if (!is_numeric($lastRound))
				$lastRound = false;
			//---------------------------
			if ($lastRound !== false && ($generalLastRound === false || $lastRound != $generalLastRound))
			{
				// new round
				//---------------------------
				$step = PUBDATA_STEP_START;
				if (!$Config->Set(CFG_PUBLIC_STEP, $step))
				{
					XLogError("AutomationPublicData::step Config Set step to start failed");
					return false;
				}
				//---------------------------
				if (!$Config->Set(CFG_PUBLIC_SUBSTEP, 0))
				{
					XLogError("AutomationPublicData::step Config Set substep to zero failed");
					return false;
				}
				//---------------------------
			}
			else 
			{
				//---------------------------
				$substep = $Config->Get(CFG_PUBLIC_SUBSTEP);
				if ($substep === false || !is_numeric($substep))
					$substep = PUBDATA_IDLE_SUBSTEP_NONE;
				//---------------------------
				if ($substep == PUBDATA_IDLE_SUBSTEP_NONE)
				{
					//---------------------------
					$ret = $this->checkTeamRateLimit();
					if ($ret === true)
					{
						//---------------------------
						$substep = PUBDATA_IDLE_SUBSTEP_START;
						if (!$Config->Set(CFG_PUBLIC_SUBSTEP, $substep))
						{
							XLogError("AutomationPublicData::step Config Set substep to idle start failed");
							return false;
						}
						//---------------------------
					}
					//---------------------------
					return true; // nothing else to do
				}
				//---------------------------
				XLogDebug("AutomationPublicData::step idle substep is $substep");
				$increment = false;
				//---------------------------
				$substepEnabled = $this->isPublicIdleStepEnabled($substep);
				if ($substepEnabled === false)
				{
					XLogError("AutomationPublicData::step isPublicIdleStepEnabled failed");
					return false;
				}
				//---------------------------
				if ($substepEnabled === 0)
				{
					//---------------------------
					XLogError("AutomationPublicData::step idle step $substep is disabled, skipping. (see ".CFG_PUBLIC_IDLE_STEPS_ENABLED.")");
					$increment = true;
					//---------------------------
				}
				else if ($substep == PUBDATA_IDLE_SUBSTEP_TEAMSUMMARY)
				{
					//---------------------------
					if (!$this->pollTeamOverview())
					{
						XLogError("AutomationPublicData::step idle teamsummary pollTeamOverview failed");
						return false;
					}
					//---------------------------
					$increment = true;
					//---------------------------
				}
				else if ($substep == PUBDATA_IDLE_SUBSTEP_TEAMDATA)
				{
					//---------------------------
					$ret = $this->updateTeamData(); // zero on rate limit, false on fail, true on success
					//---------------------------
					XLogDebug("AutomationPublicData::step idle teamdata updateTeamData returned ".XVarDump($ret));
					//---------------------------
					if ($ret === false) // does current and LEGACY_UPDATE_TEAM_DATA_FAHCLIENT
					{
						XLogError("AutomationPublicData::step updateTeamData failed");
						return false;
					}
					//---------------------------
					if ($ret === true) // otherwise ret is zero for rate-limitted, not done yet
						$increment = true;
					//---------------------------
				}
				else if ($substep == PUBDATA_IDLE_SUBSTEP_CALCTEAMSTATS)
				{
					//---------------------------
					$ret = $this->calcTeamStats();
					if ($ret === false)
					{
						XLogError("AutomationPublicData::step idle calcTeamStats failed");
						return false;
					}
					if ($ret === 0)
						XLogWarn("AutomationPublicData::step idle calcTeamStats returned no data found. Skipping and continuing...");
					//---------------------------
					$increment = true;
					//---------------------------
				}
				else if ($substep == PUBDATA_IDLE_SUBSTEP_DONE)
				{
					//---------------------------
					if (!$Config->Set(CFG_PUBLIC_SUBSTEP, PUBDATA_IDLE_SUBSTEP_NONE))
					{
						XLogError("AutomationPublicData::step Config Set substep to from done to none failed");
						return false;
					}
					//---------------------------
				}
				//---------------------------
				if ($increment)
					if (!$Config->Set(CFG_PUBLIC_SUBSTEP, ($substep+1) ))
					{
						XLogError("AutomationPublicData::step Config Set substep to idle increment failed");
						return false;
					}
				//---------------------------
				return true; // nothing else to do.
			} // no new round
			//---------------------------
		} // step none
		//---------------------------
		XLogDebug("AutomationPublicData::step step is $step");
		$increment = false;
		//---------------------------
		$stepEnabled = $this->isPublicStepEnabled($step);
		if ($stepEnabled === false)
		{
			XLogError("AutomationPublicData::step isPublicStepEnabled failed");
			return false;
		}
		//---------------------------
		if ($stepEnabled === 0)
		{
			//---------------------------
			XLogError("AutomationPublicData::step step $step is disabled, skipping. (see ".CFG_PUBLIC_STEPS_ENABLED.")");
			$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_START) // set initial values
		{
			//---------------------------
			if (!$Config->Clear(CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX))
			{
				XLogError("AutomationPublicData::step (start) Config Clear round data next ridx failed");
				return false;
			}
			//---------------------------
			if (!$Config->Clear(CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX))
			{
				XLogError("AutomationPublicData::step (start) Config Clear round data next bidx failed");
				return false;
			}
			//---------------------------
			$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_TXIDLIST)
		{
			//---------------------------
			if (!$this->updateTxidList())
			{
				XLogError("AutomationPublicData::step updateTxidList");
				return false;
			}
			//---------------------------
			$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_WORKERLIST)
		{
			//---------------------------
			if (!$this->updateWorkerLists())
			{
				XLogError("AutomationPublicData::step updateWorkerLists");
				return false;
			}
			//---------------------------
			$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_ROUNDCHART)
		{
			//---------------------------
			if (!$this->updateChartData())
			{
				XLogError("AutomationPublicData::step updateChartData failed");
				return false;
			}
			//---------------------------
			$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_ROUNDHISTORY)
		{
			//---------------------------
			$retVal = $this->updateRoundData();
			if ($retVal === false)
			{
				XLogError("AutomationPublicData::step updateRoundData failed");
				return false;
			}
			//---------------------------
			// multipass: true or 1 for done
			if ($retVal === 1 /*done*/)
				$increment = true;
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_WORKERDATA)
		{
			//---------------------------
			if (!$this->updateWorkerData())
			{
				XLogError("AutomationPublicData::step updateWorkerData failed");
				return false;
			}
			//---------------------------
			// multipass, don't increment
			//---------------------------
		}
		else if ($step == PUBDATA_STEP_DONE)
		{
			//---------------------------
			$lastRound = $Config->Get(CFG_ROUND_LAST_DONE);
			//---------------------------
			if (!is_numeric($lastRound))
			{
				XLogError("AutomationPublicData::step validate lastRound failed: ".XVarDump($lastRound));
				return false;
			}
			//---------------------------
			if (!$Config->Set(CFG_PUBLIC_LAST_ROUND, $lastRound))
			{
				XLogError("AutomationPublicData::step Config Set step to none failed");
				return false;
			}
			//---------------------------
			if (!$Config->Set(CFG_PUBLIC_STEP, PUBDATA_STEP_NONE))
			{
				XLogError("AutomationPublicData::step Config Set step to none failed");
				return false;
			}
			//---------------------------
		}
		//---------------------------
		if ($increment)
		{
			$step++;
			XLogDebug("AutomationPublicData::step incrementing step to $step");
			if (!$Config->Set(CFG_PUBLIC_STEP, $step))
			{
				XLogError("AutomationPublicData::step Config Set incremented step ($step) failed");
				return false;
			}
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function udpateBalanceData()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		//---------------------------
		$lastBalanceUpdate = $Config->Get(CFG_PUBLIC_LAST_BALANCE_UPDATE);
		//---------------------------
		if ($lastBalanceUpdate !== false && $lastBalanceUpdate != "") // has last update info
		{
			//---------------------------
			$d = XDateTimeDiff($lastBalanceUpdate, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
			//------------------
			if ($d === false)
			{
				 XLogError("AutomationPublicData::udpateBalanceData XDateTimeDiff failed. lastBalanceUpdate: '$lastBalanceUpdate'");
				 return false;
			}
			//---------------------------
			if ($d < PUBLIC_BALANCE_UPDATE_RATE)
			{
				//XLogDebug("AutomationPublicData::udpateBalanceData skipping due to rate limit");
				return true;
			}
			//---------------------------
		}
		//---------------------------
		$dtNow = new DateTime('now', new DateTimeZone('UTC'));
		if (!$Config->Set(CFG_PUBLIC_LAST_BALANCE_UPDATE, $dtNow->format(MYSQL_DATETIME_FORMAT)))
		{
			XLogError("AutomationPublicData::udpateBalanceData Config Set last balance update failed");
			return false;
		}
		//---------------------------
		$data = $this->getBalanceData();
		if ($data === false)
		{
			XLogError("AutomationPublicData::udpateBalanceData getBalanceData failed");
			return false;
		}
		//---------------------------
		if (!$this->saveBalanceData($data))
		{
			XLogError("AutomationPublicData::udpateBalanceData saveBalanceData failed");
			return false;
		}
		//---------------------------
		if (!$this->updateRoundsMarketData())
		{
			 XLogError("AutomationPublicData::udpateBalanceData updateRoundsMarketData failed.");
			 return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateChartData()
	{
		global $db;
		//---------------------------
		$MarketHistory = new MarketHistory or die("Create object failed");
		$Config = new Config() or die("Create object failed");
		$timer = new XTimer();
		//---------------------------
		$roundMax = $Config->GetSetDefault(CFG_PUBLIC_ROUND_CHART_COUNT, DEFAULT_PUBLIC_ROUND_CHART_COUNT);
		if ($roundMax === false || !is_numeric($roundMax))
		{
			XLogError("AutomationPublicData::updateChartData Config GetSetDefault round chat count failed");
			return false;
		}
		//---------------------------
		$digits = $Config->GetSetDefault(CFG_ROUND_PAY_DIGITS, DEFAULT_ROUND_PAY_DIGITS);
		if ($digits === false)
		{
			XLogError("AutomationPublicData::updateChartData Config GetSetDefault pay digits failed");
			return false;
		}
		//---------------------------
		$dbRoundTable = DB_ROUND;
		$dbfRoundId = DB_ROUND_ID;
		$dbfRoundDate = DB_ROUND_DATE_STARTED;
		$dbfRoundFlags = DB_ROUND_FLAGS;
		$dbfWork = DB_ROUND_TOTAL_WORK;
		$dbfReward = DB_ROUND_TOTAL_PAY;
		//---------
		$dbPayTable = DB_PAYOUT;
		$dbfPayRound = DB_PAYOUT_ROUND;
		$dbfPayWorker = DB_PAYOUT_WORKER;
		$dbfPayReward = DB_PAYOUT_STAT_PAY;
		//---------
		$dbStatTable = DB_STATS;
		$dbfStatRound = DB_STATS_ROUND;
		$dbfStatWork = DB_STATS_WEEK_POINTS;
		//---------------------------
		$sql  = "SELECT $dbfRoundId AS rid, $dbfRoundDate AS ts, $dbfRoundFlags AS fg, $dbfWork AS tw, $dbfReward AS tr, s.mnw, s.mxw, s.avw, p.mnr, p.mxr, p.avr, p.cnr FROM $dbRoundTable r ";
		$sql .= "INNER JOIN ";
		$sql .= "(SELECT $dbfStatRound, MIN($dbfStatWork) AS mnw, MAX($dbfStatWork) AS mxw, AVG($dbfStatWork) AS avw FROM $dbStatTable GROUP BY $dbfStatRound) s ON r.$dbfRoundId=s.$dbfStatRound ";
		$sql .= "INNER JOIN ";
		$sql .= "(SELECT $dbfPayRound, MIN($dbfPayReward) AS mnr, MAX($dbfPayReward) AS mxr, AVG($dbfPayReward) AS avr, COUNT(*) AS cnr FROM $dbPayTable WHERE $dbfPayReward>0 GROUP BY $dbfPayRound) p ON r.$dbfRoundId=p.$dbfPayRound ";
		$sql .= "ORDER BY r.$dbfRoundId DESC";
		//------------------
		if (!($qr = $db->Query($sql)))
		{
			XLogError("Payouts::updateChartData - db Query failed.\nsql: $sql");
			return false;
		}
		//------------------
		XLogDebug("AutomationPublicData::updateChartData query took ".$timer->restartMs(true));
		//---------------------------
		$rData = array();
		$roundsWithoutMarketData = 0;
		//------------------
		while ($r = $qr->GetRowArray())
			if (XMaskContains($r['fg'], ROUND_FLAG_DONE) && !XMaskContains($r['fg'], ROUND_FLAG_HIDDEN))
			{
				//---------------------------
				$outMktData = array();
				//---------------------------
				if (isset($r['ts']) && is_numeric($r['ts']) && $r['ts'] != 0)
				{
					//---------------------------
					$mktData = $MarketHistory->getHistory((int)$r['ts'], false /*pollIfNeeded*/);
					if ($mktData === false)
						XLogWarn("AutomationPublicData::updateChartData MarketHistory getHistory for round ".$r['rid']." failed, ignoring...");
					else if ($mktData === 0)
						$roundsWithoutMarketData++;
					else if (!is_array($mktData) || !isset($mktData['found']))
					{
						XLogError("AutomationPublicData::updateChartData validate MarketHistory getHistory result failed: ".XVarDump($mktData));
						return false;
					}
					else
					{
						//---------------------------
						$outMktData = array();
						if ($mktData !== false && $mktData['found'] && isset($mktData['asset_values']))
						{
							$values = $MarketHistory->getAssestValuesForDisplay($mktData['asset_values'], array('value_trim' => 'large', 'value_symbol' => false, 'value_unit' => false, 'result_associate' => 'asset', 'space' => '&nbsp;') /*options*/);
							if (is_array($values))
								$outMktData = $values;
						}
						//---------------------------
					}
					//---------------------------
				}
				//------------------
				$rData[] = array(	'id' => (int)$r['rid'],
									'tsst' => (int)NVal($r['ts'], 0),
									'tpt' => (int)NVal($r['tw'], 0),
									'apt' => (int)number_format($r['avw'], 0, '.', ''),
									'mxpt' => (int)$r['mxw'],
									'mnpt' => (int)$r['mnw'],
									'tpy' => (float)NVal($r['tr'], 0),
									'cpy' => (int)$r['cnr'],
									'apy' => ZQVal((float)number_format($r['avr'], $digits, '.', ''), 0),
									'mxpy' => ZQVal((float)$r['mxr'], 0),
									'mnpy' => ZQVal((float)$r['mnr'], 0),
									'mrkt' => $outMktData
										);
			}
		//------------------
		$marketAssets = $MarketHistory->getAssetDetails(false /*all*/, $index = 'asset', true /*type*/, true /*fulname*/, true /*unitname*/, true /*symbol*/, true /*decimalSizes*/);
		if (!is_array($marketAssets))
		{
			XLogError("AutomationPublicData::updateChartData validate MarketHistory getAssetDetails failed. result: ".XVarDump($marketAssets));
			return false;
		}
		//------------------
		$cData = array('ts' => time(), 'mktast' => $marketAssets, 'rounds' => $rData);
		//------------------
		XLogDebug("AutomationPublicData::updateChartData process query took ".$timer->restartMs(true));
		//---------------------------
		if (!$this->saveRoundChartData($cData))
		{
			XLogError("AutomationPublicData::updateChartData saveRoundChartData failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateChartData saveRoundChartData took ".$timer->restartMs(true));
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA, $roundsWithoutMarketData))
		{
			XLogError("AutomationPublicData::updateChartData  Set rounds without market data failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function getBalanceData()
	{
		//---------------------------
		$Rounds = new Rounds() or die("Create object failed");
		$Contributions = new Contributions() or die("Create object failed");
		$Ads = new Ads() or die("Create object failed");
		$Config = new Config() or die("Create object failed");
		//---------------------------
		$Wallet = new Wallet() or die("Create object failed");
		//---------------------------
		$digits = $Config->GetSetDefault(CFG_ROUND_PAY_DIGITS, DEFAULT_ROUND_PAY_DIGITS);
		if ($digits === false)
		{
			XLogError("AutomationPublicData::getBalanceData Config GetSetDefault pay digits failed");
			return false;
		}
		//---------------------------
		$hasActiveRounds = $Rounds->hasActiveRounds();
		//---------------------------
		$activeAddress = $Wallet->getMainAddress();
		if ($activeAddress === false)
		{
			XLogError("AutomationPublicData::getBalanceData Wallet failed to getAccountAddress");
			return false;
		}
		//---------------------------
		$activeBalance = $Wallet->getBalance(); // defaults to active account
		if ($activeBalance === false)
		{
			XLogError("AutomationPublicData::getBalanceData Wallet failed to getBalance. Wallet getMainAddress returned activeAddress: ".XVarDump($activeAddress).", in-objected mainAddress: ".XVarDump($Wallet->mainAddress));
			return false;
		}
		//---------------------------
		$contList = $Contributions->findRoundContributions(-1 /*roundIdx*/);
		if ($contList === false)
		{
			XLogError("AutomationPublicData::getBalanceData Contributions failed to findRoundContributions (default)");
			return false;
		}
		//---------------------------
		$jsonCont = array();
		foreach ($contList as $cont)
			if ($cont->address != $activeAddress) // skip main payout(s) from mainAddress
			{
				$ad = false;
				if ($cont->ad !== false && is_numeric($cont->ad))
					$ad = $Ads->getAd($cont->ad);
				if ($ad === false)
				{
					$adFlags  = AD_FLAG_NONE;
					$adTitle = "";
					$adStyle = "";
					$adLink  = "";
					$adText  = "";
					$adImage = "";
				}
				else
				{
					
					$adFlags = $ad->flags;
					$adTitle = $ad->title;
					$adStyle = $ad->style;
					$adLink  = $ad->link;
					$adText  = $ad->text;
					$adImage = $ad->image;
				}
				if (XMaskContains($adFlags, AD_FLAG_SHOW_ALWAYS) || ($cont->mode != CONT_FLAG_NONE && $cont->mode < CONT_MODE_SPECIAL && !$cont->hasFlag(CONT_FLAG_DISABLED)))
				{
					if ($cont->mode == CONT_MODE_ALL && $cont->address !== false && $cont->address !== "")
					{
						$cont->value = $Wallet->getBalance($cont->address);	
						if ($cont->value === false)
						{
							XLogWarn("AutomationPublicData::getBalanceData Wallet getBalance of ALL Contribution from address '".$cont->address."' failed");
							$cont->value = 0;
						}
					}
					$jsonCont[] = array( 'order' => $cont->order, 'name' => base64_encode($cont->name), 'mode' => $cont->mode, 
										'value' => FZQVal($cont->value), 'flags' => $cont->flags, 'subvalue' => FZQVal($cont->subvalue), 'countvalue' => $cont->countvalue,
										'adFlags' => $adFlags, 'adTitle' => base64_encode($adTitle), 'adStyle' => base64_encode($adStyle), 'adLink' => base64_encode($adLink), 'adText' => base64_encode($adText), 'adImage' => base64_encode($adImage));
				}
			}
		//---------------------------
		if ($hasActiveRounds)
			$activeBalance = 0.0;
		else
			$activeBalance = round($activeBalance);
		//---------------------------
		$data = array( 'updated' => time(), 
  					  'brand_tx_link' => BRAND_TX_LINK, 'brand_addr_link' => BRAND_ADDRESS_LINK, 'brand_name' => BRAND_NAME, 'brand_unit' => BRAND_UNIT,
  					  'pay_digits' => $digits, 'activeAddress' => $activeAddress, 'activeBalance' => $activeBalance, 'payoutActive' => ($hasActiveRounds ? 1 : 0),
					  'fee' => $Wallet->getEstFee(), 'contributions' => $jsonCont);
		//---------------------------
		if (defined('BRAND_MARKET_ASSET_ID'))
			$data['brand_asset'] = BRAND_MARKET_ASSET_ID;
		//---------------------------
		return $data;
	}
	//---------------------------
	function saveBalanceData($data)
	{
		//---------------------------
		$strJson = json_encode($data);
		if ($strJson === false)
		{
			XLogError("AutomationPublicData::saveBalanceData json_encode failed");
			return false;
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_BALANCE_DATA_OUTPUT, $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::saveBalanceData file_put_contents failed");
			XLogWarn("AutomationPublicData::saveBalanceData failed to write JSON data file: ".PUBLIC_BALANCE_DATA_OUTPUT." (".realpath(PUBLIC_BALANCE_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_BALANCE_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_BALANCE_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_BALANCE_DATA_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_BALANCE))
		{
			XLogError("AutomationPublicData::saveBalanceData incManifest failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateRoundsMarketData()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		$MarketHistory = new MarketHistory() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$timer = new XTimer();
		//---------------------------
		$withoutData = $Config->Get(CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA, '');
		if ($withoutData === false)
		{
			XLogError("AutomationPublicData::updateRoundsMarketData Config Get failed");
			return false;
		}
		if (!is_numeric($withoutData))
		{
			$withoutData = 0;
			if (!$Config->Set(CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA, $withoutData))
			{
				XLogError("AutomationPublicData::updateRoundsMarketData Config Set (default) failed");
				return false;
			}
		}
		//---------------------------
		if ($withoutData == 0)
			return true;
		//---------------------------
		$roundList = $Rounds->getRounds(true /*onlyDone*/, false /*onlyNotDone*/, false /*maxCount/limit*/, DB_ROUND_ID.' DESC' /*sort*/, false /*where*/);
		if ($roundList === false)
		{
			XLogError("AutomationPublicData::updateRoundsMarketData Rounds getRounds failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateRoundsMarketData checking ".sizeof($roundList)." rounds for market data, $withoutData was previously reported without");
		//---------------------------
		$polledCount = 0;
		$withoutData = 0;
		foreach ($roundList as $r)
		{
			//---------------------------
			$mktData = $MarketHistory->getHistory($r->dtStarted, true /*pollIfNeeded*/, false /*provider*/, false /*asset*/);
			if ($mktData === false)
			{
				XLogError("AutomationPublicData::updateRoundsMarketData MarketHistory getHistory for round $r->id failed. result: ".XVarDump($mktData));
				return false;
			}
			else if ($mktData === 0) // not found...and either client poll failed or is rate limited
				$withoutData++;
			else if (!is_array($mktData) || !isset($mktData['found']))
			{
				XLogError("AutomationPublicData::updateRoundsMarketData validate MarketHistory getHistory result failed: ".XVarDump($mktData));
				return false;
			}
			else if (!$mktData['found'])
				$withoutData++;
			else if (isset($mktData['just_polled']) && $mktData['just_polled'])
				$polledCount++;
			//---------------------------
		}
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_ROUNDS_WITHOUT_MARKET_DATA, $withoutData))
		{
			XLogError("AutomationPublicData::updateRoundsMarketData Config Set (finished) failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateRoundsMarketData polled $polledCount, left without data $withoutData, took ".$timer->restartMs(true));
		//---------------------------
		return true;
	}
	//---------------------------
	function updateRoundData()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$timer = new XTimer();
		$timer2 = new XTimer();
		//---------------------------
		$nextRoundIdx = $Config->GetSetDefault(CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX, '');
		if ($nextRoundIdx === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault nextRoundIdx failed");
			return false;
		}
		if (!is_numeric($nextRoundIdx))
			$nextRoundIdx = 0;
		//---------------------------
		$nextBlockIdx = $Config->GetSetDefault(CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX, '');
		if ($nextBlockIdx === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault nextBlockIdx failed");
			return false;
		}
		if (!is_numeric($nextBlockIdx))
			$nextBlockIdx = 0;
		//---------------------------
		$maxBlockSize = $Config->GetSetDefault(CFG_PUBLIC_ROUND_BLOCK_SIZE, DEFAULT_PUBLIC_ROUND_BLOCK_SIZE);
		if ($maxBlockSize === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault maxBlockSize failed");
			return false;
		}
		if ($maxBlockSize != 0 && $maxBlockSize < 1000)
		{
			XLogWarn("AutomationPublicData::getRoundData too low maxBlockSize detected: ".XVarDump($maxBlockSize).", assuming legacy count value and setting to default.");
			$maxBlockSize = DEFAULT_PUBLIC_ROUND_BLOCK_SIZE;
			if (!$Config->Set(CFG_PUBLIC_ROUND_BLOCK_SIZE, $maxBlockSize))
			{
				XLogError("AutomationPublicData::updateRoundData Config Set maxBlockSize default failed");
				return false;
			}
		}
		//---------------------------
		$maxBlockCount = $Config->GetSetDefault(CFG_PUBLIC_ROUND_COUNT, DEFAULT_PUBLIC_ROUND_COUNT);
		if ($maxBlockCount === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault maxBlockCount failed");
			return false;
		}
		if (!is_numeric($maxBlockCount))
		{
			XLogError("AutomationPublicData::updateRoundData validate maxBlockCount failed: ".XVarDump($maxBlockCount));
			return false;
		}
		//---------------------------
		$digits = $Config->GetSetDefault(CFG_ROUND_PAY_DIGITS, DEFAULT_ROUND_PAY_DIGITS);
		if ($digits === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault pay digits failed");
			return false;
		}
		//---------------------------
		$includeIdle = $Config->GetSetDefault(CFG_PUBLIC_ROUND_INCLUDE_IDLE, DEFAULT_PUBLIC_ROUND_INCLUDE_IDLE);
		if ($includeIdle === false)
		{
			XLogError("AutomationPublicData::updateRoundData Config GetSetDefault includeIdle failed");
			return false;
		}
		//---------------------------
		if ($nextBlockIdx == 0)
			$metaData =  false;
		else
		{
			//---------------------------
			$metaDataJson = @file_get_contents(PUBLIC_ROUND_DATA_OUTPUT.'.tmp');
			if ($metaDataJson === false)
			{
				XLogError("AutomationPublicData::updateRoundData file_get_contents (meta) failed. nextBlockIdx: ".XVarDump($nextBlockIdx));
				return false;
			}
			//---------------------------
			$metaData = json_decode($metaDataJson, true /*associated array*/);
			if ($metaData === false)
			{
				XLogError("AutomationPublicData::updateRoundData file_get_contents (meta) failed. nextBlockIdx: ".XVarDump($nextBlockIdx).", metaDataJson: ".XVarDump($metaDataJson));
				return false;
			}
			//---------------------------
			if (!isset($metaData['ver']) || !isset($metaData['block_count']) || !isset($metaData['round_count']) || !isset($metaData['block_index']))
			{
				XLogError("AutomationPublicData::updateRoundData validate meta fields read failed. nextBlockIdx: ".XVarDump($nextBlockIdx).", metaDataJson: ".XVarDump($metaDataJson));
				return false;
			}
			//---------------------------
			if ($metaData['ver'] != 2)
			{
				XLogError("AutomationPublicData::updateRoundData meta fields read is incorrect version. nextBlockIdx: ".XVarDump($nextBlockIdx).", metaDataJson: ".XVarDump($metaDataJson));
				return false;
			}
			//---------------------------
		}
		//---------------------------
		if ($metaData === false)
			$metaData = array('ver' => 2, 'updated' => 0 /*place holder*/, 'block_count' => 0, 'round_count' => 0, 'block_index' => array());
		//---------------------------
		$txidList = $this->getTxidList();
		if ($txidList === false)
		{
			XLogError("AutomationPublicData::updateRoundData getTxidList failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateRoundData prep took ".$timer->restartMs(true));
		//---------------------------
		$done = false;
		$blockIdx = $nextBlockIdx;
		$cacheData = array('blockIdx' => $blockIdx, 'nextRoundIdx' => $nextRoundIdx, 'maxBlockSize' => $maxBlockSize, 'maxBlockCount' => $maxBlockCount, 'digits' => $digits, 'includeIdle' => $includeIdle, 'txidList' => $txidList);
		$outputFileParts = pathinfo(PUBLIC_ROUND_DATA_OUTPUT);
		$roundCount = 0;
		//---------------------------
		$timeGetRoundData = 0;
		$timeEncodeBlock = 0;
		$timeSaveBlock = 0;
		//---------------------------
		while (!$done && $timer->elapsedMs() < 40000)
		{
			//---------------------------
			$blockFileName = XEnsureBackslash($outputFileParts['dirname']).$outputFileParts['filename'].'_'.$blockIdx.($outputFileParts['extension'] != '' ? '.'.$outputFileParts['extension'] : '');
			//XLogDebug("AutomationPublicData::updateRoundData looping blockIdx: ".XVarDump($blockIdx).", nextBlockIdx: ".XVarDump($nextBlockIdx).",  nextRoundIdx ".$cacheData['nextRoundIdx'].", roundCount ".$metaData['round_count']);
			//---------------------------
			$timer2->start();
			//---------------------------
			$result = $this->getRoundData($cacheData, true/*includeDetails*/);
			if ($result === false)
			{
				XLogError("AutomationPublicData::updateRoundData getRoundData failed");
				return false;
			}
			//---------------------------
			$timeGetRoundData += $timer2->elapsedMs();
			//---------------------------
			$done = $result['done'];
			$blockRoundCount = sizeof($result['ids']);
			//---------------------------
			if ($blockRoundCount != 0)
			{
				//---------------------------
				$timer2->start();
				//---------------------------
				$strJson = json_encode($result['data']);
				if ($strJson === false)
				{
					XLogError("AutomationPublicData::updateRoundData json_encode failed. result: ".XVarDump($result));
					return false;
				}
				//---------------------------
				$timeEncodeBlock += $timer2->restartMs();
				//---------------------------
				$retval = file_put_contents($blockFileName.'.tmp', $strJson);
				if ($retval === false)
				{
					XLogError("AutomationPublicData::updateRoundData file_put_contents failed");
					XLogWarn("AutomationPublicData::updateRoundData failed to write JSON data file: $blockFileName (".realpath($blockFileName)."), is writable: ".(is_writable($blockFileName) ? "Y" : "N").", file exists: ".(file_exists($blockFileName) ? "Y" : "N")." free disk space: ".disk_free_space(dirname($blockFileName))." bytes.");
					return false;
				}
				//---------------------------
				$timeSaveBlock += $timer2->elapsedMs();
				//---------------------------
				$cacheData['nextRoundIdx'] = ($blockRoundCount == 0 ? 0 : ($result['ids'][$blockRoundCount - 1] + 1));
				//---------------------------
				$metaData['round_count'] += $blockRoundCount;
				$metaData['block_index'][$blockIdx] = array('bfs' => strlen($strJson), 'rid' => $result['ids']);
				$metaData['block_count']++;
				//---------------------------
				$blockIdx++;
				$cacheData['blockIdx'] = $blockIdx;
				//---------------------------
			}
			//---------------------------
		} // while  !done or timedout
		//---------------------------
		XLogDebug("AutomationPublicData::updateRoundData loop done: isDone ".XVarDump($done).", blockIdx $blockIdx (has extra inc), nextRoundIdx ".$cacheData['nextRoundIdx'].", roundCount ".$metaData['round_count'].", loop took ".$timer->elapsedMs(true)." (getRoundData $timeGetRoundData, EncodeBlock $timeEncodeBlock, SaveBlock $timeSaveBlock)");
		//---------------------------
		$nextBlockIdx = $blockIdx; // should already have an extra trailing increment
		$nextRoundIdx = $cacheData['nextRoundIdx'];
		//---------------------------
		if ($done)
			$nextBlockIdx--; // always re-do the last block
		//---------------------------
		$metaData['updated'] = time();
		//---------------------------
		$strJson = json_encode($metaData);
		$retval = file_put_contents(PUBLIC_ROUND_DATA_OUTPUT.'.tmp', $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::updateRoundData file_put_contents (meta) failed");
			XLogWarn("AutomationPublicData::updateRoundData failed to write JSON data file: ".PUBLIC_ROUND_DATA_OUTPUT.", is writable: ".(is_writable(PUBLIC_ROUND_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_ROUND_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_ROUND_DATA_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		if ($done)
		{
			//---------------------------
			if (!@rename(PUBLIC_ROUND_DATA_OUTPUT.'.tmp', PUBLIC_ROUND_DATA_OUTPUT))
			{
				XLogError("AutomationPublicData::updateRoundData rename metaData failed");
				XLogWarn("AutomationPublicData::updateRoundData failed to rename metaData file to: ".PUBLIC_ROUND_DATA_OUTPUT." (".realpath(PUBLIC_ROUND_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_ROUND_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_ROUND_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_ROUND_DATA_OUTPUT))." bytes.");
				return false;
			}
			//---------------------------
			if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_ROUNDS_INDEX, false /*writeToFile*/))
			{
				XLogError("AutomationPublicData::updateRoundData incManifest index failed");
				return false;
			}
			//---------------------------
			for ($b = 0;$b < $metaData['block_count'];$b++)
			{
				//---------------------------
				$blockFileName = XEnsureBackslash($outputFileParts['dirname']).$outputFileParts['filename'].'_'.$b.($outputFileParts['extension'] != '' ? '.'.$outputFileParts['extension'] : '');
				$blockFileNameTmp = $blockFileName.'.tmp';
				//---------------------------
				if (!@rename($blockFileNameTmp, $blockFileName))
				{
					XLogError("AutomationPublicData::updateRoundData rename block failed");
					XLogWarn("AutomationPublicData::updateRoundData failed to rename data file to: $blockFileName (".realpath($blockFileName)."), is writable: ".(is_writable($blockFileName) ? "Y" : "N").", file exists: ".(file_exists($blockFileName) ? "Y" : "N")." free disk space: ".disk_free_space(dirname($blockFileName))." bytes.");
					return false;
				}
				//---------------------------
			} // for b
			//---------------------------
			if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_ROUNDS)) // allow writeToFile
			{
				XLogError("AutomationPublicData::updateRoundData incManifest data failed");
				return false;
			}
			//---------------------------
		}
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_ROUND_DATA_NEXT_ROUND_IDX, $nextRoundIdx))
		{
			XLogError("AutomationPublicData::updateRoundData Config Set nextRoundIdx failed");
			return false;
		}
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_ROUND_DATA_NEXT_BLOCK_IDX, $nextBlockIdx))
		{
			XLogError("AutomationPublicData::updateRoundData Config Set nextBlockIdx failed");
			return false;
		}
		//---------------------------
		return ($done ? 1 : true);
	}
	//---------------------------
	function getRoundData($cacheData, $includeDetails = false)
	{
		//---------------------------
		$MarketHistory = new MarketHistory() or die("Create object failed");
		$Contributions = new Contributions() or die("Create object failed");
		$Rounds = new Rounds() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$Stats = new Stats() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$Wallet = new Wallet() or die("Create object failed");
		$timer = new XTimer();
		$timerFull = new XTimer();
		//---------------------------
		if (!is_array($cacheData)
			|| !isset($cacheData['blockIdx']) || !isset($cacheData['nextRoundIdx']) || !isset($cacheData['maxBlockSize']) || !isset($cacheData['maxBlockCount']) || !isset($cacheData['digits']) || !isset($cacheData['includeIdle']) || !isset($cacheData['txidList'])
			|| !is_numeric($cacheData['blockIdx']) || !is_numeric($cacheData['nextRoundIdx']) || !is_numeric($cacheData['maxBlockSize']) || !is_numeric($cacheData['maxBlockCount']) || !is_numeric($cacheData['digits']) || !is_numeric($cacheData['includeIdle']) || ($cacheData['txidList'] !== false && !is_array($cacheData['txidList'])) )
		{
			XLogError("AutomationPublicData::getRoundData validate cacheData failed. cacheData: ".XVarDump($cacheData));
			return false;
		}
		//---------------------------
		//XLogDebug("AutomationPublicData::getRoundData cacheData: ".XVarDump($cacheData));
		//---------------------------
		if (is_numeric($cacheData['maxBlockCount']) && $cacheData['maxBlockCount'] != 0)
			$roundLimit = $cacheData['maxBlockCount'];
		else
			$roundLimit = false;
		//---------------------------
		$mainAddress = $Wallet->getMainAddress();
		if ($mainAddress === false)
		{
			XLogError("AutomationPublicData::getRoundData Wallet failed to mainAddress");
			return false;
		}
		//---------------------------
		$roundList = $Rounds->getRounds(true /*onlyDone*/, false /*onlyNotDone*/, $roundLimit /*maxCount/limit*/, DB_ROUND_ID /*sort*/, DB_ROUND_ID.'>='.$cacheData['nextRoundIdx'] /*where*/);
		if ($roundList === false)
		{
			XLogError("AutomationPublicData::getRoundData Rounds getRounds failed");
			return false;
		}
		//---------------------------
		//XLogDebug("AutomationPublicData::getRoundData roundList size: ".sizeof($roundList).", maxCount ".XVarDump($cacheData['maxBlockCount']).", nextRoundIdx ".$cacheData['nextRoundIdx'].", includeDetails ".($includeDetails ? 'Y' : 'N').". took ".$timer->restartMs(true));
		//---------------------------
		$timeGettingLists = 0;
		$timeLookupPayout = 0;
		$timeProcessing = 0;
		$timeSearchingTxid = 0;
		$timeBuildingConts = 0;
		$timeBuildingResults = 0;
		//---------------------------
		$workerList = array();
		$roundsData = array();
		$roundIDList = array();
		//---------------------------
		$sizeTestCounter = 0;
		$roundCount = 0;
		$processedCount = 0;
		$doneBlock = false;
		//---------------------------
		foreach ($roundList as $r)
		{
			//---------------------------
			$processedCount++;
			//---------------------------
			if ($r->isHidden())
				continue; // skip this round
			//---------------------------
			if ($cacheData['maxBlockSize'] != 0 && $sizeTestCounter++ > 1)
			{
				//---------------------------
				$sizeTestCounter = 0;
				//---------------------------
				$strJson = json_encode($roundsData);
				if ($strJson === false)
				{
					XLogError("AutomationPublicData::getRoundData json_encode failed. ".XVarDump($roundsData));
					return false;
				}
				//---------------------------
				$sizeBytes = strlen($strJson);
				if ($sizeBytes > $cacheData['maxBlockSize'])
				{
					XLogDebug("AutomationPublicData::getRoundData block ".$cacheData['blockIdx']." size limit exceeded with $roundCount rounds at ".XVarDump($sizeBytes)." bytes");
					$doneBlock = true;
					break; // foreach ($roundList as $r)
				}
				//---------------------------
			}
			//---------------------------
			$roundIDList[] = $r->id;
			//---------------------------
			$timer->start();
			//---------------------------
			$payoutList = $Payouts->findRoundPayouts($r->id, false /*orderBy*/, true /*includePaid*/, false /*limit*/, false /*workerIdx*/, false /*includeSpecialWorkers*/, false /*payoutIDIndexed*/, true /*workerIDIndexed*/);
			if ($payoutList === false)
			{
				XLogError("AutomationPublicData::getRoundData Rounds failed to getRounds ridx $r->id");
				return false;
			}
			//---------------------------
			$statList = $Stats->findRoundStats($r->id);
			if ($statList === false)
			{
				XLogError("AutomationPublicData::getRoundData Stats failed to findRoundStats ridx $r->id");
				return false;
			}
			//---------------------------
			$contList = $Contributions->getContributions($r->id, true /*includeSpecial*/, false /*where*/, DB_CONTRIBUTION_ORDER /*sort*/);
			if ($contList === false)
			{
				XLogError("AutomationPublicData::getRoundData Contributions contribList failed for ridx $r->id");
				return false;
			}
			//---------------------------
			$timeGettingLists += $timer->elapsedMs();
			//---------------------------
			$avgPoints = 0;
			$avgPay = 0.0;
			$maxPoints = 0;
			$maxPay = 0.0;
			$minPoints = PHP_INT_MAX;
			$minPay = PHP_INT_MAX;
			$countPoints = 0;
			$countPay = 0;
			//---------------------------
			$curTxId = false;
			//---------------------------
			$wData = array();
			foreach ($statList as $stat)
			{
				//------------------------
				$widx = $stat->workerIdx;
				//------------------------
				$timer->start();
				$payout = (isset($payoutList[$widx]) ? $payoutList[$widx] : false);
				$timeLookupPayout += $timer->restartMs();
				//------------------------
				$work = $stat->work();
				if ($work === false || $work <= 0)
					$outPoints = 0;
				else
				{
					$outPoints = $work;
					$avgPoints += $work;
					if ($work > $maxPoints)
						$maxPoints = (int)$work;
					if ($work < $minPoints)
						$minPoints = (int)$work;
					$countPoints++;
				}
				//------------------------
				if ($work === false)
					$work = -1;
				//------------------------
				if ($payout === false) // not included in payout due to minimums
				{
					$outStatPay = 0;
					$outPayout = 0;
					$strTx = '[none]';
				}
				else
				{
					$outStatPay = ($payout->statPay == 0 ? 0 : "$payout->statPay");
					$outPayout = ($payout->pay == 0 ? 0 : "$payout->pay");
					if ($payout->dtStored !== false)
						$strTx = '[stored]'; // round data should show stored, even if it was later paid
					else
						$strTx = ($payout->txid === false || $payout->txid == "" ? '[none]' : $payout->txid);
					if ($payout->statPay > 0)
					{
						$avgPay += $payout->statPay;
						if ($payout->statPay > $maxPay)
							$maxPay = (float)$payout->statPay;
						if ($payout->statPay < $minPay)
							$minPay = (float)$payout->statPay;
						$countPay++;
					}
				}
				//------------------------
				$timeProcessing += $timer->restartMs();
				//------------------------
				if ($cacheData['txidList'] === false)
					$curTxId = 0;
				else if ($curTxId === false || $strTx != $cacheData['txidList'][$curTxId])
				{
					//------------------------
					$curTxId = array_search($strTx, $cacheData['txidList']);
					if ($curTxId === false)
					{
						XLogError("AutomationPublicData::getRoundData Round $r->id txid not found: ('$strTx') txid: ".($payout !== false ? "(no payout)" : XVarDump($payout->txid)));
						return false;
					}
					//------------------------
				}
				//------------------------
				$timeSearchingTxid += $timer->restartMs();
				//------------------------
				//XLogDebug("Round ".$r->id." widx ".$widx." includeIdle ".XVarDump($includeIdle)." has payout ".($payout !== false ? "y" : "f")." tx ".$strTx);
				if ($payout !== false || ($roundCount < $cacheData['includeIdle'] && is_numeric($work) && $work > 0)) // has a payout or within include idle range
					$wData[] = array('w' => $widx, 'pt' => $outPoints, 'sp' => $outStatPay, 'py' => $outPayout, 'tx' => $curTxId);
				//------------------------
				$timeBuildingResults += $timer->elapsedMs();
				//------------------------
			} // foreach ($statList as $stat)
			//------------------------
			$timer->start();
			//------------------------
			if ($countPoints != 0)
				$avgPoints /= $countPoints;
			$avgPoints =  (int)floor($avgPoints);
			if ($countPay != 0)
				$avgPay /= $countPay;
			$avgPay = round($avgPay, $cacheData['digits']);
			//------------------------
			if ($minPoints === PHP_INT_MAX)
				$minPoints = 0;
			if ($minPay === PHP_INT_MAX)
				$minPay = 0.0;
			//------------------------
			$comment = $r->getComment();
			if ($comment === false)
				$comment = '[error]';
			//------------------------
			$strMode = '';
			if (XMaskContains($r->flags, ROUND_FLAG_DRYRUN)) $strMode .= ' dry-run/test';
			if (XMaskContains($r->flags, ROUND_FLAG_HIDDEN)) $strMode .= ' hidden/test';
			//------------------------
			// these are both expect done
			$strStats = ($r->dtStats === false ? '' : $this->formatDate($r->dtStats));
			$strPaid = ($r->dtPaid === false ? '' : $this->formatDate($r->dtPaid));
			if ($strStats === false || $strPaid === false)
			{
				XLogError("AutomationPublicData::getRoundData formatDate failed for dtStats or dtPaid. Round: ".XVarDump($r));
				return false;
			}
			//------------------------
			$timeBuildingResults += $timer->restartMs();
			//------------------------
			if ($includeDetails)
			{
				//------------------------
				$mainContName = "Main";
				foreach ($contList as $cont)
					if ($cont->address == $mainAddress)
						$mainContName = $cont->name;
				//------------------------
				$contModeNames = $Contributions->getModeNames();
				$cData = array();
				foreach ($contList as $cont)
					if ($cont->outcome == CONT_OUTCOME_PAID && $cont->mode != CONT_MODE_SPECIAL_STORE_PAY && $cont->mode != CONT_MODE_SPECIAL_MAIN)
					{
						//------------------------
						$flagFullBal = $cont->hasFlag(CONT_FLAG_FULL_BALANCE);
						$flagMaxTotal = $cont->hasFlag(CONT_FLAG_MAX_TOTAL);
						$flagMaxCount = $cont->hasFlag(CONT_FLAG_MAX_COUNT);
						//------------------------
						$outName = ($cont->name != '' ? $cont->name : 'Contribution '.$cont->id);
						$outMode = XArray($contModeNames, $cont->mode, '[unknown]');
						if ($flagFullBal)
							$outMode .= ' (all)';
						if (!$flagFullBal && $cont->value != 0 && $cont->mode < CONT_MODE_SPECIAL)
							$outMode .= " $cont->value ".BRAND_UNIT;
						if ($flagMaxTotal && $cont->subvalue != 0)
							$outMode .= " (max $cont->subvalue ".BRAND_UNIT.")";
						if ($flagMaxCount && $cont->countvalue != 0)
							$outMode .= " (max folders $cont->countvalue)";
						$outTotal = ($cont->total !== false ? ($cont->total == 0 ? 0 : ''.$cont->total) : -1);
						$outCount = ($cont->count !== false ? $cont->count : -1);
						if ($cont->mode == CONT_MODE_FLAT || $cont->mode == CONT_MODE_PERCENT || $cont->mode == CONT_MODE_ALL)
						{ // these go into main donation pot, instead of directly to workers
							if ($outCount == 0)
								$outCount = "(to $mainContName)"; // count n/a, goes into main donations
							if ($outTotal > 0)
								$outTotal = "($outTotal)";
							else if ($outTotal == 0)
								$outTotal = -1;								
						}
						$cd = array('nm' => $outName, 'md' => $outMode, 'tp' => $outTotal, 'cn' => $outCount);
						//------------------------
						$cData[] = $cd;
						//------------------------
					}
				//------------------------
			} // if includeDetails cont data
			//------------------------
			$timeBuildingConts += $timer->restartMs();
			//------------------------
			$outTotalPay = ($r->totalPay == 0 ? 0 : ''.$r->totalPay);
			$outMaxPay = ($maxPay == 0 ? 0 : ''.$maxPay);
			$outMinPay = ($minPay == 0 ? 0 : ''.$minPay);
			//------------------------
			$rData = array('id' => $r->id, 'tsst' => ($r->dtStarted === false ? 0 : $r->dtStarted), 'tspd' => ($r->dtPaid === false ? 0 : $r->dtPaid), 
						  'tpt' =>  $r->totalWork, 'apt' =>  $avgPoints, 'mxpt' =>  $maxPoints, 'mnpt' =>  $minPoints,
						  'tpy' => $outTotalPay, 'cpy' =>  $countPay, 'apy' => ''.$avgPay, 'mxpy' => $outMaxPay, 'mnpy' => $outMinPay);
			//------------------------
			if ($includeDetails)
			{
				$rData['tssp'] = ($r->dtStats === false ? 0 : $r->dtStats);
				$rData['md'] = $strMode;
				$rData['cm'] = $comment;
				$rData['wd'] = $wData;
				$rData['cd'] = $cData;
			}
			//---------------------------
			$mktData = $MarketHistory->getHistory($r->dtStarted, true /*pollIfNeeded*/);
			if ($mktData === false)
				XLogWarn("AutomationPublicData::getRoundData MarketHistory getHistory for round $r->id failed, ignoring...");
			else if ($mktData === 0) // market data not found, client failed or polling rate limited
			{
				$mktData = false;
				//XLogWarn("AutomationPublicData::getRoundData MarketHistory getHistory for round $r->id returned client failed, ignoring...");
			}
			else if (!is_array($mktData) || !isset($mktData['found']))
			{
				XLogError("AutomationPublicData::getRoundData validate MarketHistory getHistory result failed: ".XVarDump($mktData));
				return false;
			}
			//---------------------------
			$outMktData = array();
			if ($mktData !== false && $mktData['found'] && isset($mktData['asset_values']))
			{
				$values = $MarketHistory->getAssestValuesForDisplay($mktData['asset_values'], array('value_trim' => 'large', 'value_symbol' => 'fiat', 'space' => '&nbsp;') /*options*/);
				if (is_array($values))
					$outMktData = $values;
			}
			//---------------------------
			$rData['mkt'] = $outMktData;
			//---------------------------
			$roundsData[] = $rData;
			$roundCount++;
			//---------------------------
			if ($cacheData['maxBlockCount'] != 0 && $roundCount >= $cacheData['maxBlockCount'])
			{
				//XLogDebug("AutomationPublicData::getRoundData block round count reached at ".XVarDump($roundCount)."/".XVarDump($cacheData['maxBlockCount']));
				$doneBlock = true;
				break; // foreach ($roundList as $r)
			}
			//---------------------------
			$timeBuildingResults += $timer->elapsedMs();
			//------------------------
		} // foreach ($roundList as $r) if mode....
		//------------------------
		$roundListSize = sizeof($roundList); // some may be skipped in roundsData/roundCount, like hidden ones, but will still be counted in processedCount
		if (!$doneBlock && $roundListSize != 0 && $processedCount == $roundListSize && ($roundLimit === false || $roundLimit == $roundListSize))
			$doneBlock = true; // didn't max file size or round count (!$doneBlock), but processed all of round list. Also, roundList is full of roundLimit, not finishing last handful of rounds.
		//------------------------
		XLogDebug("AutomationPublicData::getRoundData processed $roundCount/".sizeof($roundList)." rounds. took ".$timerFull->elapsedMs(true)." ( getting lists $timeGettingLists, lookup payouts $timeLookupPayout, processing $timeProcessing, searching Txids $timeSearchingTxid, building contributions $timeBuildingConts, building results $timeBuildingResults)");
		//------------------------
		return array('done' => (!$doneBlock /*Done: out of rounds before block done*/), 'ids' => $roundIDList, 'data' => array( 'ts' => time(), 'rounds' => $roundsData));
	}
	//---------------------------
	function saveRoundChartData($data)
	{
		//---------------------------
		$strJson = json_encode($data);
		if ($strJson === false)
		{
			XLogError("AutomationPublicData::saveRoundChartData json_encode failed");
			return false;
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_ROUND_CHART_DATA_OUTPUT, $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::saveRoundChartData file_put_contents failed");
			XLogWarn("AutomationPublicData::saveRoundChartData failed to write JSON data file: ".PUBLIC_ROUND_CHART_DATA_OUTPUT." (".realpath(PUBLIC_ROUND_CHART_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_ROUND_CHART_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_ROUND_CHART_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_ROUND_CHART_DATA_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_CHART))
		{
			XLogError("AutomationPublicData::saveRoundChartData incManifest failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateWorkerLists()
	{
		//---------------------------
		$timer = new XTimer();
		$workerData = $this->updateValidWorkerLists();
		if ($workerData === false)
		{
			XLogError("AutomationPublicData::updateWorkerLists updateValidWorkerLists failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateWorkerLists updateValidWorkerLists took ".$timer->restartMs(true));
		//---------------------------
		$workerData = $this->updateInvalidWorkerLists($workerData);
		if ($workerData === false)
		{
			XLogError("AutomationPublicData::updateWorkerLists updateInvalidWorkerLists failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateWorkerLists updateInvalidWorkerLists took ".$timer->restartMs(true));
		//---------------------------
		if (!$this->saveWorkerLists($workerData))
		{
			XLogError("AutomationPublicData::updateWorkerLists saveWorkerLists failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateWorkerLists saveWorkerLists took ".$timer->restartMs(true));
		//---------------------------
		return true;
	}
	//---------------------------
	function updateValidWorkerLists()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$Wallet = new Wallet() or die("Create object failed");
		$timer = new XTimer();
		//---------------------------
		$workerListBlockSize = $Config->Get(CFG_PUBLIC_WORKER_LIST_BLOCK_SIZE);
		if ($workerListBlockSize === false || !is_numeric($workerListBlockSize))
		{
			$workerListBlockSize = DEFAULT_PUBLIC_WORKER_LIST_BLOCK_SIZE;
			if (!$Config->Set(CFG_PUBLIC_WORKER_LIST_BLOCK_SIZE, $workerListBlockSize))
			{
				XLogError("AutomationPublicData::updateValidWorkerLists Config failed to Set worker list block size to default");
				return false;
			}
		}
		//---------------------------
		$timer->start();
		// [](id,uname,disabled)
		$workerList = $Workers->getWorkerSummaryByActiveAge(true /*includeDetails*/, false /*indexedList*/);
		if ($workerList === false)
		{
			XLogError("AutomationPublicData::updateValidWorkerLists Workers getWorkerSummaryByActiveAge failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateValidWorkerLists Workers getWorkerSummaryByActiveAge found ".sizeof($workerList)." workers in ".$timer->restartMs(true));
		//---------------------------
		$valBlock = 0;
		$countBlock = 0;
		//---------------------------
		$data['index'] = array();
		$data['index']['valcount'] = 1;
		$data['index'][0]['count'] = 0;
		$data['index'][0]['data'] = b''; // binary string;
		//---------------------------
		$data['val'][0] = b''; // binary string
		//---------------------------
		$timer->start();
		//---------------------------
		foreach ($workerList as $wdata)
		{
			//---------------------------
			if (!is_array($wdata))
			{
				XLogError("AutomationPublicData::updateValidWorkerLists wdata isn't an array: ".XVarDump($wdata));
				return false;
			}
			if (!is_string($wdata[1]))
			{
				XLogError("AutomationPublicData::updateValidWorkerLists wdata[1] isn't a string: ".XVarDump($wdata));
				return false;
			}
			$data['val'][$valBlock] .= pack('H*', $Wallet->decodeBase58($wdata[1])); //without len 25 bytes 303.7kb,  with V $worker->id,  length is 29 bytes  352.3Kb
			$data['index'][$valBlock]['data'] .= pack('V', $wdata[0]);
			$countBlock++;
			if ($countBlock >= $workerListBlockSize)
			{
				$data['index'][$valBlock]['count'] = $countBlock;
				$valBlock++;
				$countBlock = 0;
				$data['val'][$valBlock] = b'';
				$data['index'][$valBlock]['count'] = 0;
				$data['index'][$valBlock]['data'] = b'';
			}
			//---------------------------
		} // foreach ($workerList as $worker)
		//---------------------------
		$data['index'][$valBlock]['count'] = $countBlock;
		if ($countBlock == 0)
			$valBlock--; // hanging empty block
		//---------------------------
		$data['index']['valcount'] = $valBlock + 1;
		//---------------------------
		XLogDebug("AutomationPublicData::updateValidWorkerLists process valid Workers took ".$timer->restartMs(true));
		//---------------------------
		return $data;
	}
	//---------------------------
	function updateInvalidWorkerLists($data)
	{
		//---------------------------
		$Workers = new Workers() or die("Create object failed");
		//---------------------------
		if (!isset($data['index']) || !is_array($data['index']))
		{
			XLogError("AutomationPublicData::updateInvalidWorkerLists validate data index is array failed");
			return false;
		}
		//---------------------------
		$timer = new XTimer();
		$workerList = $Workers->getWorkerNameIDList(false /*indexIsName*/, false /*validOnly*/, true /*invalidOnly*/);
		//---------------------------
		if ($workerList === false)
		{
			XLogError("AutomationPublicData::updateInvalidWorkerLists Workers getWorkerNameIDList invalid only failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::updateInvalidWorkerLists Workers getWorkerNameIDList found ".sizeof($workerList)." workers in ".$timer->restartMs(true));
		//---------------------------
		$data['index']['invcount'] = sizeof($workerList);
		$data['index']['inv'] = b'';
		$data['inval'] = array();
		foreach ($workerList as $widx => $wname)
		{
			$data['index']['inv'] .= pack('V', $widx);
			$data['inval'][] = $wname;
		}
		//---------------------------
		return $data;
	}
	//---------------------------
	function saveWorkerLists($data)
	{
		//---------------------------
		$delemiter = "--BREAK--";
		//---------------------------
		$strJson = json_encode($data['inval']);
		if ($strJson === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists json_encode inv failed");
			return false;
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_WORKER_LIST_INVALID_OUTPUT, $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists file_put_contents inv failed");
			XLogWarn("AutomationPublicData::saveWorkerLists failed to write JSON inv data file: ".PUBLIC_WORKER_LIST_INVALID_OUTPUT." (".realpath(PUBLIC_WORKER_LIST_INVALID_OUTPUT)."), is writable: ".(is_writable(PUBLIC_WORKER_LIST_INVALID_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_WORKER_LIST_INVALID_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_WORKER_LIST_INVALID_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_WORKERLIST_INV))
		{
			XLogError("AutomationPublicData::saveWorkerLists incManifest (inv) failed");
			return false;
		}
		//---------------------------
		$outputFileParts = pathinfo(PUBLIC_WORKER_LIST_VALID_OUTPUT);
		//---------------------------
		$blockCount = $data['index']['valcount'];
		$fhIndex = @fopen(PUBLIC_WORKER_LIST_INDEX_OUTPUT, 'w+b');
		if ($fhIndex === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists fopen index for writing failed");
			XLogWarn("AutomationPublicData::saveWorkerLists fopen index for writing failed file: ".PUBLIC_WORKER_LIST_INDEX_OUTPUT." (".realpath(PUBLIC_WORKER_LIST_INDEX_OUTPUT)."), is writable: ".(is_writable(PUBLIC_WORKER_LIST_INDEX_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_WORKER_LIST_INDEX_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_WORKER_LIST_INDEX_OUTPUT))." bytes.");
		}
		//---------------------------
		if (@fwrite($fhIndex, "ValBlocks-$blockCount,InvCount-".$data['index']['invcount'].$delemiter) === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists fwrite writing index first counts failed");
			fclose($fhIndex);
			return false;
		}
		//---------------------------
		for ($bIdx = 0;$bIdx < $blockCount;$bIdx++)
		{
			//---------------------------
			$blockFileName = XEnsureBackslash($outputFileParts['dirname']).$outputFileParts['filename']."_$bIdx".($outputFileParts['extension'] != '' ? '.'.$outputFileParts['extension'] : '');
			//---------------------------
			if (@fwrite($fhIndex, "B-$bIdx,C-".$data['index'][$bIdx]['count'].$delemiter) === false || 
				@fwrite($fhIndex, $data['index'][$bIdx]['data'].$delemiter) === false)
			{
				XLogError("AutomationPublicData::saveWorkerLists fwrite writing index block $bIdx failed");
				fclose($fhIndex);
				return false;
			}
			//---------------------------
			$retval = file_put_contents($blockFileName, "B-$bIdx,C-".$data['index'][$bIdx]['count'].$delemiter.$data['val'][$bIdx].$delemiter);
			if ($retval === false)
			{
				XLogError("AutomationPublicData::saveWorkerLists file_put_contents val block $bIdx failed");
				XLogWarn("AutomationPublicData::saveWorkerLists failed to file_put_contents val block $bIdx file: ".$blockFileName." (".realpath($blockFileName)."), is writable: ".(is_writable($blockFileName) ? "Y" : "N").", file exists: ".(file_exists($blockFileName) ? "Y" : "N")." free disk space: ".disk_free_space(dirname($blockFileName))." bytes.");
				fclose($fhIndex);
				return false;
			}
			//---------------------------
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_WORKERLIST_VAL))
		{
			XLogError("AutomationPublicData::saveWorkerLists incManifest (val blocks) failed");
			return false;
		}
		//---------------------------
		if (@fwrite($fhIndex, "Inv-".$data['index']['invcount'].$delemiter) === false || 
			@fwrite($fhIndex, $data['index']['inv'].$delemiter) === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists fwrite writing index invalid section failed");
			fclose($fhIndex);
			return false;
		}
		//---------------------------
		if (@fwrite($fhIndex, "-END-".$delemiter) === false)
		{
			XLogError("AutomationPublicData::saveWorkerLists fwrite writing index end failed");
			fclose($fhIndex);
			return false;
		}
		//---------------------------
		fclose($fhIndex);
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_WORKERLIST_INDEX))
		{
			XLogError("AutomationPublicData::saveWorkerLists incManifest (idx) failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateTxidList()
	{
		//---------------------------
		$txidList = $this->getTxidList();
		if ($txidList === false)
		{
			XLogError("AutomationPublicData::updateTxidList getTxidList failed");
			return false;
		}
		//---------------------------
		$binOut = "";
		foreach ($txidList as $txid)
		{
			//---------------------------
			$len = strlen($txid);
			if ($len == 0 || $len > 64)
			{
				XLogError("AutomationPublicData::updateTxidList invalid txid length $len detected: ".XVarDump($txid));
				return false;
			}
			if ($txid[0] == "[")
				$binOut .= pack('a32', $txid);
			else if ($len < 64)
			{
				XLogError("AutomationPublicData::updateTxidList txid too short: ".XVarDump($txid));
				return false;
			}
			else $binOut .= pack('H64', $txid);
			//---------------------------
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_TXID_LIST_OUTPUT, "C-".sizeof($txidList)."\r\n".$binOut."--END--");
		if ($retval === false)
		{
			XLogError("AutomationPublicData::updateTxidList file_put_contents failed");
			XLogWarn("AutomationPublicData::updateTxidList failed to file_put_contents file: ".PUBLIC_TXID_LIST_OUTPUT." (".realpath(PUBLIC_TXID_LIST_OUTPUT)."), is writable: ".(is_writable(PUBLIC_TXID_LIST_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_TXID_LIST_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_TXID_LIST_OUTPUT))." bytes.");
			fclose($fhIndex);
			return false;
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_TXIDS))
		{
			XLogError("AutomationPublicData::updateTxidList incManifest failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function getWorkerData($payoutLimit = 4, $txidList, $workerIDs = false)
	{
		//---------------------------
		$Workers = new Workers() or die("Create object failed");
		$Payouts = new Payouts() or die("Create object failed");
		$timer = new XTimer();
		$timer2 = new XTimer();
		//---------------------------
		$wdata = array();
		$wdata[] = array(-1, array());
		//---------------------------
		if (sizeof($txidList) == 0)
		{
			XLogError("AutomationPublicData::getWorkerData txidList cannot be empty");
			return false;
		}
		//---------------------------
		$txidNone = 0;
		if (strtolower($txidList[$txidNone]) != '[none]')
		{
			XLogError("AutomationPublicData::getWorkerData txidList first entry must be [none]");
			return false;
		}
		//---------------------------
		if ($workerIDs === false)
		{
			$workerList = $Workers->getWorkerNameIDList(false /*indexIsName*/, true /*validOnly*/);
			//---------------------------
			if ($workerList === false)
			{
				XLogError("AutomationPublicData::getWorkerData Workers getWorkers failed");
				return false;
			}
			//---------------------------
			$workerIDs = array();
			foreach ($workerList as $widx => $wname)
				$workerIDs[] = $widx;
			//---------------------------
		}
		else if (!is_array($workerIDs))
		{
			XLogError("AutomationPublicData::getWorkerData validate workerIDs is array failed: ".XVarDump($workerIDs));
			return false;
		}
		//---------------------------
		$timeGetWorkerPayouts = 0;
		$timeLookupStorePayoutWorker = 0;
		$timeGetWorkerStoredBalance = 0;
		//---------------------------
		foreach ($workerIDs as $widx)
		{
			//---------------------------
			$timer2->start();
			$workerPayouts = $Payouts->getWorkerPayouts($widx, false /*onlyPaid*/, false /*onlyUnpaid*/, DB_PAYOUT_ROUND." DESC" /*sort*/, $payoutLimit /*limit*/);
			if ($workerPayouts === false)
			{
				XLogError("AutomationPublicData::getWorkerData Payouts getWorkerPayouts failed");
				return false;
			}
			//---------------------------
			$timeGetWorkerPayouts += $timer2->restartMs();
			//---------------------------
			$payoutData = array();
			$payoutData['pd'] = array();
			$payoutData['upd'] = array();
			$payoutData['ff'] = array();
			//---------------------------
			foreach ($workerPayouts as $payout)
			{
				//---------------------------
				if ($payout->dtPaid === false)
					$payoutData['upd'][] = array($payout->id, FZQVal($payout->statPay, 0), FVal($payout->dtStored, 0));
				else
				{
					//--------------------------- Don't record pay to store txid's
					if ($payout->txid === false || $payout->txid == "")
						$txid = $txidNone; // none
					else 
					{
						$txid = array_search($payout->txid, $txidList);
						if ($txid === false)
						{
							XLogError("AutomationPublicData::getWorkerData payout $payout->id txid not found in list: ".XVarDump($payout->txid));
							return false;
						}
					}
					//---------------------------
					$storePayout = -1; // none
					if ($payout->storePayout !== false && is_numeric($payout->storePayout))
					{
						//---------------------------
						$timer2->start();
						$storePayoutWorkerIdx = $Payouts->getPayoutWorkerIdx($payout->storePayout);
						if ($storePayoutWorkerIdx === false)
						{
							XLogError("AutomationPublicData::getWorkerData Payouts getPayoutWorkerIdx for storePayout failed: ".XVarDump($payout->storePayout));
							return false;
						}
						//---------------------------
						$timeLookupStorePayoutWorker += $timer2->elapsedMs();
						//---------------------------
						if ($storePayoutWorkerIdx === true /*not found*/)
						{
							XLogWarn("AutomationPublicData::getWorkerData Payouts getPayoutWorkerIdx for storePayout not found: ".XVarDump($payout->storePayout));
							$storePayout = $payout->storePayout;
						}
						else if ($storePayoutWorkerIdx == WORKER_SPECIAL_MAIN /*forfeit to main*/)
							$storePayout = -2; // forfeit
						else
							$storePayout = $payout->storePayout;
						//---------------------------
					}
					//---------------------------
					if ($storePayout == -2) // forfeit
						$payoutData['ff'][] = array($payout->id, FZQVal($payout->statPay, 0), FVal($payout->dtStored, 0), FVal($payout->dtPaid, 0));
					else
						$payoutData['pd'][] = array($payout->id, FZQVal($payout->pay, 0), FZQVal($payout->statPay, 0), FZQVal($payout->storePay, 0), FVal($payout->dtStored, 0), FVal($payout->dtPaid, 0), FVal($storePayout, 0), FVal($txid, 0));
					//---------------------------
				}
				//---------------------------
			} // foreach ($workerPayouts as $payout)
			//---------------------------
			if (sizeof($payoutData['pd']) == 0)
				unset($payoutData['pd']);
			if (sizeof($payoutData['upd']) == 0)
				unset($payoutData['upd']);
			if (sizeof($payoutData['ff']) == 0)
				unset($payoutData['ff']);
			if (sizeof($payoutData) == 0) // all empty
				$payoutData = 0;
			//---------------------------
			$timer2->start();
			$balance = $Payouts->getWorkerStoredBalance($widx);
			if ($balance === false)
			{
				XLogError("AutomationPublicData::getWorkerData Payouts getWorkerStoredBalance failed");
				return false;
			}
			//---------------------------
			$timeGetWorkerStoredBalance += $timer2->elapsedMs();
			//---------------------------
			if ($payoutData === 0 and $balance == 0)
				$wdata[0][1][] = $widx;
			else
				$wdata[] = array($widx, FZQVal($balance, 0), $payoutData);
			//---------------------------
		} // foreach ($workerList as $worker)
		//---------------------------
		//XLogDebug("AutomationPublicData::getWorkerData for ".sizeof($workerIDs)." workers took ".$timer->restartMs(true)." getting: ($timeGetWorkerPayouts worker payouts, $timeLookupStorePayoutWorker store payout worker, $timeGetWorkerStoredBalance worker store balance)");
		//---------------------------
		return $wdata;
	}
	//---------------------------
	function updateWorkerData()
	{
		//---------------------------
		$Config = new Config() or die("Create object failed");
		$Workers = new Workers() or die("Create object failed");
		$timer = new XTimer();
		$timeoutTimer = new XTimer();
		//---------------------------
		$payoutLimit = $Config->GetSetDefault(CFG_PUBLIC_WORKER_PAYOUT_LIMIT, DEFAULT_PUBLIC_WORKER_PAYOUT_LIMIT);
		if ($payoutLimit === false || !is_numeric($payoutLimit))
		{
			XLogError("AutomationPublicData::updateWorkerData Config GetSetDefault payoutLimit failed");
			return false;
		}
		//---------------------------
		$blockIndex = $Config->Get(CFG_PUBLIC_SUBSTEP);
			if ($blockIndex === false || !is_numeric($blockIndex))
				$blockIndex = 0;
		//---------------------------
		$blockSize = $Config->GetSetDefault(CFG_PUBLIC_WORKER_BLOCK_SIZE, DEFAULT_PUBLIC_WORKER_BLOCK_SIZE);
		if ($blockSize === false || !is_numeric($blockSize))
		{
			XLogError("AutomationPublicData::updateWorkerData Config GetSetDefault blocksize failed");
			return false;
		}
		//---------------------------
		$timer->start();
		$txidList = $this->getTxidList();
		if ($txidList === false)
		{
			XLogError("AutomationPublicData::updateWorkerData getTxidList failed");
			return false;
		}
		//XLogDebug("AutomationPublicData::updateWorkerData getTxidList took ".$timer->restartMs(true));
		//---------------------------
		$workerIDs = $Workers->getWorkerIDs(true /*onlyValid*/);
		if ($workerIDs === false)
		{
			XLogError("AutomationPublicData::updateWorkerData Workers getWorkerIDs failed");
			return false;
		}
		//---------------------------
		$wrkCount = sizeof($workerIDs);
		$maxBlocks = ceil( ((float)$wrkCount) / ((float)$blockSize) );
		//---------------------------
		if ($blockIndex >= $maxBlocks)
		{
			XLogError("AutomationPublicData::updateWorkerData left off blockIndex $blockIndex exceeds maxBlocks $maxBlocks");
			return false;
		}
		//---------------------------
		$outputFileParts = pathinfo(PUBLIC_WORKER_DATA_OUTPUT);
		//---------------------------
		XLogDebug("AutomationPublicData::updateWorkerData wrkCount $wrkCount, maxBlocks $maxBlocks, blockIndex $blockIndex");
		//---------------------------
		while ($blockIndex < $maxBlocks && $timeoutTimer->elapsedMs() < 40000)
		{
			//---------------------------
			$bStartIdx = $blockIndex * $blockSize;
			$bEndIdx = $bStartIdx + $blockSize - 1;
			$bCountIdx = $blockSize;
			if ($bEndIdx >= $wrkCount)
			{
				$bEndIdx = $wrkCount - 1;
				$bCountIdx = $bEndIdx - $bStartIdx + 1;
			}
			//---------------------------
			$blockFileName = XEnsureBackslash($outputFileParts['dirname']).$outputFileParts['filename']."_$blockIndex".($outputFileParts['extension'] != '' ? '.'.$outputFileParts['extension'] : '');
			$blockFileNameTmp = $blockFileName.'.tmp';
			//---------------------------
			$timer->start();
			$wdata = $this->getWorkerData($payoutLimit, $txidList, array_slice($workerIDs, $bStartIdx, $bCountIdx) );
			if ($wdata === false)
			{
				XLogError("AutomationPublicData::updateWorkerData getWorkerData failed");
				return false;
			}
			//---------------------------
			XLogDebug("AutomationPublicData::updateWorkerData getWorkerData block $blockIndex / $maxBlocks ($bStartIdx [$workerIDs[$bStartIdx]] - $bEndIdx [$workerIDs[$bEndIdx]], $bCountIdx) took ".$timer->restartMs(true));
			//---------------------------
			$strJson = json_encode($wdata);
			if ($strJson === false)
			{
				XLogError("AutomationPublicData::updateWorkerData json_encode failed");
				return false;
			}
			//---------------------------
			$retval = file_put_contents($blockFileNameTmp, $strJson);
			if ($retval === false)
			{
				XLogError("AutomationPublicData::updateWorkerData file_put_contents failed");
				XLogWarn("AutomationPublicData::updateWorkerData failed to write JSON data file: $blockFileNameTmp (".realpath($blockFileNameTmp)."), is writable: ".(is_writable($blockFileNameTmp) ? "Y" : "N").", file exists: ".(file_exists($blockFileNameTmp) ? "Y" : "N")." free disk space: ".disk_free_space(dirname($blockFileNameTmp))." bytes.");
				return false;
			}
			//---------------------------
			$blockIndex++;
			//---------------------------
		} // while
		//---------------------------
		if ($blockIndex >= $maxBlocks) // last pass
		{
			//---------------------------
			XLogDebug("AutomationPublicData::updateWorkerData last pass. Writing metaData index.");
			//---------------------------
			$blockIndexes = array();
			for ($b = 0;$b < $maxBlocks;$b++)
			{
				$bStartIdx = $b * $blockSize;
				$bEndIdx = $bStartIdx + $blockSize - 1;
				if ($bEndIdx >= $wrkCount)
					$bEndIdx = $wrkCount - 1;
				$blockIndexes[] = array($workerIDs[$bStartIdx], $workerIDs[$bEndIdx]);
			}
			//---------------------------
			$metaData = array('ver' => 1, 'block_count' => (int)$maxBlocks, 'worker_count' => (int)$wrkCount, 'workers_per_block' => (int)$blockSize, 'payout_limit' => (int)$payoutLimit, 'blocks' => $blockIndexes);
			$strJson = json_encode($metaData);
			$retval = file_put_contents(PUBLIC_WORKER_DATA_OUTPUT.'.tmp', $strJson);
			if ($retval === false)
			{
				XLogError("AutomationPublicData::updateWorkerData file_put_contents metaData failed");
				XLogWarn("AutomationPublicData::updateWorkerData failed to file_put_contents metaData file to: ".PUBLIC_WORKER_DATA_OUTPUT." (".realpath(PUBLIC_WORKER_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_WORKER_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_WORKER_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_WORKER_DATA_OUTPUT))." bytes.");
			}
			//---------------------------
			if (!@rename(PUBLIC_WORKER_DATA_OUTPUT.'.tmp', PUBLIC_WORKER_DATA_OUTPUT))
			{
				XLogError("AutomationPublicData::updateWorkerData rename metaData failed");
				XLogWarn("AutomationPublicData::updateWorkerData failed to rename data file to: ".PUBLIC_WORKER_DATA_OUTPUT." (".realpath(PUBLIC_WORKER_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_WORKER_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_WORKER_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_WORKER_DATA_OUTPUT))." bytes.");
				return false;
			}
			//---------------------------
			if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_WORKERDATA_INDEX, false /*writeToFile*/))
			{
				XLogError("AutomationPublicData::updateWorkerData incManifest failed");
				return false;
			}
			//---------------------------
			for ($b = 0;$b < $maxBlocks;$b++)
			{
				$blockFileName = XEnsureBackslash($outputFileParts['dirname']).$outputFileParts['filename']."_$b".($outputFileParts['extension'] != '' ? '.'.$outputFileParts['extension'] : '');
				$blockFileNameTmp = $blockFileName.'.tmp';
				//---------------------------
				if (!@rename($blockFileNameTmp, $blockFileName))
				{
					XLogError("AutomationPublicData::updateWorkerData rename block failed");
					XLogWarn("AutomationPublicData::updateWorkerData failed to rename data file to: $blockFileName (".realpath($blockFileName)."), is writable: ".(is_writable($blockFileName) ? "Y" : "N").", file exists: ".(file_exists($blockFileName) ? "Y" : "N")." free disk space: ".disk_free_space(dirname($blockFileName))." bytes.");
					return false;
				}
				//---------------------------
			} // for b
			//---------------------------
			if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_WORKERDATA)) // allow writeToFile
			{
				XLogError("AutomationPublicData::updateWorkerData incManifest failed");
				return false;
			}
			//---------------------------
			$step = PUBDATA_STEP_WORKERDATA + 1;
			if (!$Config->Set(CFG_PUBLIC_STEP, $step))
			{
				XLogError("AutomationPublicData::updateWorkerData Config Set step (increment) failed");
				return false;
			}
			//---------------------------
			$blockIndex = 0;
			//---------------------------
		}
		//---------------------------
		if (!$Config->Set(CFG_PUBLIC_SUBSTEP, $blockIndex))
		{
			XLogError("AutomationPublicData::updateWorkerData Config Set blockStep (pubstep) failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateTeamDataEO()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$teamNum = $Config ->Get(CFG_ROUND_TEAM_ID);
		if ($teamNum === false)
		{
			XLogError("AutomationPublicData::updateTeamDataEO get team ID config failed");
			return false;
		}
		//------------------
		if (!is_numeric($teamNum))
		{
			XLogError("AutomationPublicData::updateTeamDataEO validate team ID is numeric failed: ".XVarDump($teamNum));
			return false;
		}
		//------------------
		if (TEST_TEAMSTAT === true)
			$url = TEST_TEAM_STATS_PAGE_EO;
		else
		{
			$url = POLL_TEAM_STATS_PAGE_EO.$teamNum;
		}
		//---------------------------------
		$stats = @simplexml_load_file($url);
		if ($stats === false)
		{
			$error = libxml_get_last_error();
			if ($error !== false && strpos($error->message, "failed to load external entity") === 0)
			{
				//---------------------------------
				XLogWarn("AutomationPublicData::updateTeamDataEO - team stats not found team: $teamNum");
				//---------------------------------
				return "Not Found";
			}
			XLogError("AutomationPublicData::updateTeamDataEO - simplexml_load_file failed. Error: ".($error === false ? "<no error info>" : "[$error->code] $error->message").", Url: $url");
			return false;
		}
		//---------------------------------
		if (!$this->updateTeamRateLimit())
		{
			XLogError("AutomationPublicData::updateTeamDataEO updateTeamRateLimit failed");
			return false;
		}
		//------------------
		if (!isset($stats->team))
		{
			XLogError("AutomationPublicData::updateTeamDataEO - verify data structure failed");
			return false;
		}
		//------------------
		$stats = $stats->team;
		//------------------
		if (!isset($stats->TeamID) || $stats->TeamID != $teamNum)
		{
			XLogError("AutomationPublicData::updateTeamDataEO - verify team number failed. Expected: $teamNum, got: ".XVarDump($stats->TeamID));
			return false;
		}
		//---------------------------------
		if (!isset($stats->Team_Name) || !isset($stats->Rank) || !isset($stats->Users_Active) || !isset($stats->Users) || !isset($stats->Change_Rank_24hr) || !isset($stats->Change_Rank_7days) || !isset($stats->Points_24hr_Avg) || !isset($stats->Points) || !isset($stats->WUs))
		{
			XLogError("AutomationPublicData::updateTeamDataEO - not all expected fields were found");
			return false;
		}
		//---------------------------------
		$data = array( 'UTC' => $this->formatDate("now"), 'name' => (string)$stats->Team_Name, 'id' => (int)$stats->TeamID, 'actusers' => (int)$stats->Users_Active, 'users' => (int)$stats->Users, 'rank' => (int)$stats->Rank, 'rank24' => (int)$stats->Change_Rank_24hr, 'rank7' => (int)$stats->Change_Rank_7days, 'points' => (int)$stats->Points, 'points24' => (int)$stats->Points_24hr_Avg, 'wu' => (int)$stats->WUs);
		//---------------------------------
		$strJson = json_encode($data);
		if ($strJson === false)
		{
			XLogError("AutomationPublicData::updateTeamDataEO json_encode failed");
			return false;
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_TEAM_DATA_OUTPUT, $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::updateTeamDataEO file_put_contents failed");
			XLogWarn("AutomationPublicData::updateTeamDataEO failed to write JSON data file: ".PUBLIC_TEAM_DATA_OUTPUT." (".realpath(PUBLIC_TEAM_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_TEAM_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_TEAM_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_TEAM_DATA_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_TEAM))
		{
			XLogError("AutomationPublicData::updateTeamDataEO incManifest failed");
			return false;
		}
		//---------------------------
		return true;
	}
	//---------------------------
	function updateTeamData()
	{
		$Team = new Team() or die("Create object failed");
		$FahClient = new FahClient() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		if (LEGACY_UPDATE_TEAM_DATA_FAHCLIENT === true)
		{
				//FahClient pollTeamMembers
		}
		else
		{
			//------------------
			$FahAppClient = new FahAppClient() or die("Create object failed");
			$Config = new Config() or die("Create object failed");
			//------------------
			$teamId = $Config->Get(CFG_ROUND_TEAM_ID);
			if ($teamId === false || !is_numeric($teamId))
			{
				XLogError("AutomationPublicData::updateTeamData get team ID config failed");
				return false;
			}
			//------------------
			$ret = $FahAppClient->pollTeam($teamId, -1 /*special rIdx*/, false /*reparse*/, true /*noRateLimitFail*/); // has it's own rateLimit, returns 0 if rate limitted
			if ($ret === false) 
			{
				XLogError("AutomationPublicData::updateTeamData FahAppClient pollTeam failed");
				return false;
			}
			//------------------
			if ($ret === 0)
				return 0;
			//------------------
		}
		//------------------
		XLogDebug("AutomationPublicData::updateTeamData poll data took ".$timer->restartMs(true));
		//------------------
		return true;
	}
	//---------------------------
	function calcTeamStats()
	{
		$Team = new Team() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		$stats = $Team->calcTeamStats();
		//------------------
		if ($stats === false)
		{
			XLogError("AutomationPublicData::calcTeamStats - Team calcTeamStats failed");
			return false;
		}
		if ($stats === 0)
		{
			XLogWarn("AutomationPublicData::calcTeamStats - Team calcTeamStats returned no data found. Returning no data found.");
			return 0;
		}
		//---------------------------------
		XLogDebug("AutomationPublicData::calcTeamStats Team calcTeamStats took ".$timer->restartMs(true));
		//------------------
		$strJson = json_encode($stats);
		if ($strJson === false)
		{
			XLogError("AutomationPublicData::calcTeamStats json_encode failed");
			return false;
		}
		//---------------------------
		$retval = file_put_contents(PUBLIC_TEAM_DATA_OUTPUT, $strJson);
		if ($retval === false)
		{
			XLogError("AutomationPublicData::calcTeamStats file_put_contents failed");
			XLogWarn("AutomationPublicData::calcTeamStats failed to write JSON data file: ".PUBLIC_TEAM_DATA_OUTPUT." (".realpath(PUBLIC_TEAM_DATA_OUTPUT)."), is writable: ".(is_writable(PUBLIC_TEAM_DATA_OUTPUT) ? "Y" : "N").", file exists: ".(file_exists(PUBLIC_TEAM_DATA_OUTPUT) ? "Y" : "N")." free disk space: ".disk_free_space(dirname(PUBLIC_TEAM_DATA_OUTPUT))." bytes.");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::calcTeamStats encode and save took ".$timer->restartMs(true));
		//------------------
		if (!$this->incManifest(PUBDATA_MANIFEST_ASSET_TEAM))
		{
			XLogError("AutomationPublicData::calcTeamStats incManifest failed");
			return false;
		}
		//---------------------------
		if (!$Team->trimOldData(60 /*ageDays*/, true /*trimTeamUsers*/))
		{
			XLogError("AutomationPublicData::calcTeamStats Team trimOldData failed");
			return false;
		}
		//---------------------------
		XLogDebug("AutomationPublicData::calcTeamStats Team trimOldData took ".$timer->restartMs(true));
		//------------------
		return true;
	}
	//---------------------------
	function pollTeamOverview()
	{
		$FahClient = new FahClient() or die("Create object failed");
		$timer = new XTimer();
		//------------------
		XLogDebug("AutomationPublicData::pollTeamOverview");
		//------------------
		if (!$FahClient->pollTeamOverview())
		{
			XLogError("AutomationPublicData::pollTeamOverview FahClient pollTeamOverview failed");
			return false;
		}
		//------------------
		XLogDebug("AutomationPublicData::pollTeamOverview FahClient pollTeamOverview took ".$timer->restartMs(true));
		//------------------
		if (!$this->updateTeamRateLimit())
		{
			XLogError("AutomationPublicData::pollTeamOverview updateTeamRateLimit failed");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function checkTeamRateLimit()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		$pollRate = $Config->Get(CFG_PUBLIC_POLL_TEAM_RATE);
		if ($pollRate === false || !is_numeric($pollRate))
		{
			$pollRate = DEFAULT_PUBLIC_POLL_TEAM_RATE;
			if (!$Config->Set(CFG_PUBLIC_POLL_TEAM_RATE, $pollRate ))
			{
				XLogError("AutomationPublicData::checkTeamRateLimit Config set default poll rate failed");
				return true;
			}
		}
		//------------------
		$lastPoll = $Config->Get(CFG_PUBLIC_LAST_POLL_TEAM);
		if ($lastPoll === false || $lastPoll === "")
		{
			 XLogWarn("AutomationPublicData::checkTeamRateLimit No Rate limit info");
			 return true;
		}
		//------------------
		$d = XDateTimeDiff($lastPoll, false/*now*/, false/*UTC*/, 'n'/*minutes*/);
		//------------------
		if ($d === false)
		{
			 XLogError("AutomationPublicData::checkTeamRateLimit XDateTimeDiff failed. lastPoll: '$lastPoll'");
			 return false;
		}
		//------------------
		if ($d < $pollRate) // maximum rate, def once every 360 mins = 6 hours
		{
			//XLogDebug("AutomationPublicData::checkTeamRateLimit rate limitted. minutes since last team stats: $d");
			return false;
		}
		//------------------
		return true;
	}
	//---------------------------
	function updateTeamRateLimit()
	{
		$Config = new Config() or die("Create object failed");
		//------------------
		//XLogNotify("AutomationPublicData::updateTeamRateLimit");
		$dtNow = new DateTime('now', new DateTimeZone('UTC'));
		if (!$Config->Set(CFG_PUBLIC_LAST_POLL_TEAM, $dtNow->format(MYSQL_DATETIME_FORMAT)))
		{
			 XLogError("AutomationPublicData::updateTeamRateLimit Config Set Last Team Stat failed");
			 return true;
		}
		//------------------
		return true;
	}
	//---------------------------
	function getTxidList()
	{
		//------------------
		$Payouts = new Payouts() or die("Create object failed");
		$Contributions = new Contributions() or die("Create object failed");
		//------------------
		$pTxidList = $Payouts->getPayoutTxidList();
		if ($pTxidList === false)
		{
			 XLogError("AutomationPublicData::getTxidList Payouts getPayoutTxidList failed");
			 return false;
		}
		//------------------
		$cTxidList = $Contributions->getContributionTxidList();
		if ($cTxidList === false)
		{
			 XLogError("AutomationPublicData::getTxidList Contributions getContributionTxidList failed");
			 return false;
		}
		//------------------
		return array_merge(array('[none]', '[stored]'), $pTxidList, $cTxidList);
	}
	//---------------------------
} // class AutomationPublicData
//---------------------------
$startAutoPubUtc = new DateTime('now',  new DateTimeZone('UTC'));
$apd = new AutomationPublicData() or die("Create object failed");
if (!$apd->Main())
{
	XLogNotify("AutomationPublicData Main failed");
	echo "failed";
}
else
{
	//XLogNotify("AutomationPublicData done successfully");
	echo "ok";
}
$secDiff = XDateTimeDiff($startAutoPubUtc); // default against now in UTC returning seconds
if ($secDiff > 1)
	XLogDebug("AutomationPublicData full automation took $secDiff seconds");
//---------------------------
?>
