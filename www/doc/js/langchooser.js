window.onload = function()
{
	var lang = document.getElementById('lang');
	var langSel = document.getElementById('langsel');
	if (!lang || !langSel)
		return;
	langSel.value = document.documentElement.getAttribute("lang");
	langSel.style.display = '';
	lang.addEventListener('change', function(event) {
			var ls = document.getElementsByTagName('link');
			for (var i = 0;i < ls.length;i++)
				if (ls[i].getAttribute('rel') == 'alternate' && ls[i].getAttribute('hreflang') == event.target.value)
					window.location.href=ls[i].getAttribute('href');
		});
};
