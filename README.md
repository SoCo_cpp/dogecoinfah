# Dogecoin Folding at Home #

DogecoinFah is a LAMP web application and public website to automate spreading donated Dogecoin cryptocurrency to members of our Folding @ Home team. The Folding @ Home distributed computing project gives points to users and teams for running distributed computing software that makes calculations for medical research.

More technically, DogecoinFah is an automated LAMP (Linux/Apache/MySQL/PHP) web application for calculating and scheduling the sending of cryptocurrency transactions to users of a third party system, in customize proportions related to user scoring value reported by that system's public API.

Current use of the system automates weekly reward rounds, which distribute donated Dogecoin cryptocurrency funds to our Folding @ Home team members who have a valid Dogecoin address as their folding username. Team members are rewarded various portions of the weekly donations, based on their portion of the team's folding points for the week. While each user's weekly points are used to verify active participation and their level of participation proportional to the team, the reward for each team member is calculated from a combination of multiple separate donation accounts with a variety of configured strategies for sharing.

### Status ###

This system has been active and running live for several years. It has adapted many times to changes and new challenges.

### Latest Focus ###

 * Added multi-language support to JavaScript generated content, but still testing/reviewing.
  - (This feature is live, but only has English content and won't seek translations until a few other content changes are considered.)
 * Improve performance, display, and clarity of Round History and Folder Search History.
 * Provide clear and detailed information about contributions, sponsorship, and rewards.
 * Revised and update all web page wording and documentation.
 * Decide what to do about the antiquated and poor quality blog feature.
 * Improve web page SEO, keywords, and a few web best-practices.
 * Consider an improved web page layout system that flows/scales better and can support various browsers/devices.

The big long-term goal:

 * Just getting started on support for self-served sponsorship automated contributions.
     (I've been considering aspects of the following for quite some time)
       - The ability to create and configure a contribution on the public facing web site without administration involvement.
       - Specify contribution reward distribution settings.
          * Mode: Proportional, match donations percent, reward random active folders, reward each active, etc.
          * Value and limits: specify weekly value for mode, also with optional maximum total and maximum rewarded folder count per round.
          * Consider that some contributions should be private, funded by one entity, and some should be shared and collectively funded.
          * Add more fun and useful modes and options. Simple stuff like one that covers transaction fees, matches the donations by percentage, gives extra to brand new active folders, etc.
       - Be provided a hot wallet address for funding your contribution account, without requiring weekly on-time deposits, or sharing to collectively fund.
       - Specify and configure a sponsorship ad for showing on the website, advertising your sponsorship.
          * Dependent on many feasibility challenges.       
          * Would be limited to semi-significant sponsorship and may require contact information for admin communication.
          * Will need moderation or admin approval features, security considerations and possibly image upload feature.
          * Current web page design may not support properly displaying many of these.
          * Fun options like showing only the ad of the highest funded contribution in a competing group. This would allow voting by donating to the contribution+ad of the group you support.
       - Intelligent display of user created contributions in "This Week's Rewards"
          * Monitoring if the pending contribution is funded yet.
          * Display scheduled contributions separately/distinctly for already funded versus not.
          * Possible rating system to show scheduled, but not yet funded, contributions with a recent reliability score.
       - Possible ability to later edit, adjust, or refund your contribution.
          * (The other option is a one-shot, that you cease funding and abandon, to create another with different options.)
          * Creation should require a mandatory refund address.
          * Creation should not require registration and identification. Providing a contact method should be only optional.

Recent improvements and additions at the end of 2021...

 * User contributed translations have added full website and documentation translations for six languages: English, German, French, Russia, Spanish, and Portuguese. 
   (Includes documents and public facing static web pages, but excludes JavaScript generated text and the antiquated blog section.)
 * Support for multiple language translations of user facing web pages and documentation.
 * Web page and documentation templating system for management of versioning and translations of content for many pages.
 * Expansion of sponsorship contribution support for better reliability and later expansion of features.
 * Better handling for downloading the ever-growing size of the folding stats flat file from Folding @ Home (daily_user_summary.txt.bz2).
 * Expanded support for configurable transaction fees, output minimums, and reward/payout minimums.
 * The addition of historic market value recording and display in the History and History Chart pages.

A little farther back in 2021....

 * Better handling of web caching for generated data.
 * Folder history and balance search by folding username/address.
 * Improved installer testing for dependencies, file privileges, and database schema.
 * Added support for SSH and Sodium tunneling to secure access to the cryptocurrency wallet's RPC server.
 * Added Monitor system to detect, report, and sometimes manage common problems, issues, and failures.
 * Refactored most code to optimize for speed and handling of large amounts of data.
 * Total round payout and contribution refactor for improved reliability, verification, and contribution flexibility.
 * Support for storing reward balances for users until a threshold is accumulated to send a payout.
 * Revised most branding to be configurable allowing this system to be re-branded and used by another folding team, with a different name, and different cryptocurrency.

### Requirements ###

Linux, MySql, PHP, a web server, and a Dogecoin wallet accessible via RPC.

The system makes use of Linux's cron scheduling system, with crontabs, to actively manage the periodic execution of a couple PHP scripts. This requirement may limit server platform support.

The supported Apache, MySql, and PHP versions are pretty wide. Last tried, pretty old versions are supported, but use of more modern versions would avoid unexpected problems.

PHP Requirements:

 * Support for PHP's BC Math module is required
 * PHP MySql PDO support is now required
 * PHP permissions for a couple features like allow_url_fopen and serialize_precision may be required (automatically checked and set by installer/maintenance page if able to).
 * Other PHP.ini options may need manually adjusted. max_execution_time must be at least 60 seconds, but should be 120 seconds or more, especially if your server is a slower machine.

Cryptocurrency wallet requirements:

 * Only Bitcoin-like wallet RPC servers are supported currently. There is no current support for alternative RPC APIs, such as Electrum or various web wallets/services/exchanges.
 * Currently only username/password RPC authentication is supported. I expect this to be depreciated, requiring some sort of certificate/priv-key based authentication sometime soon by many coin wallets for RPC access.
 * Naked RPC connections, even with username/password, are very insecure and should only be used when the wallet is running on the same machine as the web application, with as many restrictions applied as possible. One of the following tunneling options or some reasonable security considerations should be utilized for remote wallet RPC access.
 * SSH tunneling is supported, making a public-private key SSH connection, then using the tunnel feature to connect to the wallet RPC endpoint. This takes some configuration, plenty of setup on both ends, and many web servers only have older, insecure, and poorly working libssh2 PHP modules available.
 * Sodium tunneling is also supported. Sodium's public-private key Curve encryption is used to connect to the remote Sodium Tunnel Server, using the well supported libsodium PHP module. Only a custom tunneling protocol is supported, which works with a helper PHP module you host on the remote server, that then relays the tunneled connection to the wallet RPC endpoint.

Folding @ Home points and statistics requirements:

 * Folding @ Home has a great set of APIs, but they have specific limitations that make them unusable for the main points collection with larger folding teams.
 * Instead, we've reverted back to using the flat-file, daily_user_summary.txt.bz2. It lists the name, team, and total points of every Folding @ Home member.
 * Especially since Covid's increase in folding interest, this file has became quite large (27MB now). This presents some extra challenges, as it is not only needed for periodic (weekly) rounds, but also for general statistics, which are typically configured to be polled about every 6 hours.
 * This bandwidth usage will crush many budget web hosting plans quickly.
 * Support was added to connect to a proxy tool, which greatly diminishes this strain. Requests are direct to a FoldAPIProxy PHP page hosted on a remote web server, without strict bandwidth limitations. When queried, the FoldAPIProxy downloads the big flat-file, processes, compresses, and replies with just its data for your team members. Querying the proxy requires a pre-shared a basic passphrase to prevent abuse from those who stumble upon it on the web.

### Installation ###

Note: This is a brief run down that does not cover comprehensive options, best practices, or security.

Create a MySql database and a user with privileges to it.

Prepare a cryptocurrency client set to run as an RPC server.

Copy all of the www folder and its sub-folders to your published web server folder. 

Password protect your www/admin/ directory and its sub-directories using your web-server, such as with a properly configured .htaccess server file. DogecoinFah's humble home-brew admin login and privileges system should not be trusted with the security of your system.

Modify the www/admin/Settings.php file, filling the Install password, SQL credentials, RPC credentials, and other required fields.

Open www/admin/Install.php in your browser. You will be notified of directories or files that may need write permissions, detected dependency or server configuration issues, about the database not being installed yet, and more.

On the Install page, enter the install password you specified in Settings.php (C_INSTALL_PASSWORD), then click the Install button. This installs and initializes the SQL database schema.

When completed, disable www/admin/Install.php from being accessed through your web server for security. (Consider deleting it or rename it to www/admin/Install.php.disabled)


### First Steps In Administration ###

Note: This is a brief run down that does not cover comprehensive options, best practices, or security.

Enter the administration section by opening the hosted version of www/admin/Admin.php in your browser. Enter the default admin web user credentials you provided in Settings.php (C_DEFAULT_USER_NAME / C_DEFAULT_USER_PASS) to log into the administration section for the first time.


You may have no privileges other than creating new users, until you give yourself more privileges in the Admin Users tab. It is suggested that you change the password then keep this super user as it is, with only user management privileges. Then you can use it to make a second account for daily use, with many privileges, except for user management. Log out and back in with this daily administration account, using it from here on.


From the Admin Wallet tab, confirm connectivity to your wallet's RPC server as configured in Settings.php. Once connectivity is working, first you should set a main address. This will be the main round hot wallet. Also set a store address, to hold rewards not large enough to send yet (even if you won't be using one), as well as some of the cryptocurrency minimum and fee configurations.


Go to the Contributions tab and added your main donations contribution, which is typically set to proportional mode, using the entire balance, and using the main address. More detailed contributions can be added later.


Go to the Automation tab and click the start button for start the Public Data Update automation. This will start the continuous background process, which will collect your team's statistics and prepare information shown on the public areas of the website.


In the Automation tab, click the execute button the Round Automation to start a new round. The first round output will have zero values and be a dud, not paying out anything. The system normally compares the current folding points to a previous round's folding points, so the first one is special. Plan your launch accordingly or use a hidden/test round modes to prepare for the first round with a payout and to test to ensure everything is ready. Configure the Round Automation to Start a new round weekly, when ready, by setting the rate options then clicking start.


Monitor the round's payout progress in the Rounds tab. The default is to require each round to be approved before paying, by clicking the Approve button of the round twice. The Monitor system will warn of concerns or issues, track performance, and halt the progress if problems are detected. The contribution, payout, and transaction process is fully completed in a test mode first, without real transactions and requiring approval, then those are reset and the process is repeated again, this time for real. The contributions are made, sending actual transactions from contribution accounts and recalculating all the rewords. Then approval is required again, before sending the final funds from the main hot-wallet address to the users.


After your first dud round, you should consider refining and adding your desired account payouts in the Contributions tab. Each contribution should have at least a name, mode, value, and source address specified. You can create an Ad, which to associate with a Contribution. This Ad will show on the website in the sponsors list and the contributions show in the payout tally.


For troubleshooting, see www/admin/log/ALL_log.txt


### System Design and Terminology ###

The DogecoinFah system is a PHP state machine, where most tasks are run in small steps. These steps target completing in under 60 seconds, but a max_execution_time (in PHP.ini or equivalent) of double that or more is recommended. A locking mechanism ensures proper operation if a step takes an excessive amount of time and the Monitor system will report if this has problems, including the event of PHP cutting the step off early due to exceeding the execution time limit.


These steps form tasks which are executed by cron jobs, who are managed by the "Automation" system. There are three automations, to start a new round once per week, to continuously step an active round until it completes, and another to step updating of public data displayed by the public facing web site.


No PHP is utilized outside of the administration section and con jobs. Most of DogecoinFah's "tools" should ideally not be used or present on a production server. All public facing data, public data, is generated into static binary and JSon data files, which client side JavaScript parses to display for users.


The Wallet system manages wallet related activities, configurations, and executes all RPC calls. Wallet also validates that folding user names are valid cryptocurrency addresses.


The Config system holds a hash-table of key and value pairs, which represent all configurations and some states used by various parts of the system. Some configs are manged with nice administration user interfaces and some might require manual editing in the Config administration tab.


Most of the DogecoinFah system relies on a design of lists of objects, where each list is a SQL table and each record is a single object. Generally, a class represents each singular object/table record, providing functions to manipulate it. Another class represents the list of that object, being the entire table, and provides functions for manipulate the list its self, like retrieving the table's contents, interpreted as a list of classes representing each object in the list results.


The round payout process uses several of these with:


 * Workers - Information about each Folding@home username on your team, including if their folding username was determined to be a valid crypto wallet address.
 * Rounds - The state and information about each weekly round
 * Stats - A Round's calculated points data for each Worker.
 * Payouts - A Round's calculated reward for each Worker, as well as state of payment, used to hold and record store rewards for layer payout.
 * Contributions - A Round's sources of funding, how they are to be dispersed, and their state of payment. Contributions configurations are stored with their round id set to -1. A round will duplicate all those configured contributions with its round id set, then use those with its round id for recording and keeping track of the state payment for that round.
 * NewRoundData - Temporary working data for the current round's raw data from Folding@Home, with an entry for each team member, along with the progress for matching team member usernames to Workers, validating workers, and calculating each team members points effective for just this round's week.

Cron Automation System:

 * New Round - Creates a new round with the configured defaults. This is typicaly set to run once per week, late in the eveing, with  rounds set with 'start round automation', which starts the Round Action automation.
 * Round Action - Calls step on the active round repeatidly until is is completed, then shuts its self off.
 * Public Data Update - Regularly checks various timers to update data when needed that is shown on the public website. This includes donation balances, contributions, and team statistics. When a new round has finished, it will build data related to showing the round history, chart, and folder search information on the website.

Further notes about automations:

When Round Step is called, it determines what round action should be done for the current step in the round's progression from the flexible action stack. Some actions are designed to be called many times until completed, while other's are actions re-used multiple times at various steps in the round processing, such as pausing for detected problems from the Monitor system if there is any. Others logical tasks in the round processing have multiple actions to complete, such as an action for preparing for a task, an action for doing the task, then an action verifying the results.

Folding@Home points collection:

The FahAppClient system downloads the current Folding@Home points stats from the flat file https:\//apps.foldingathome.org/daily_user_summary.txt.bz2

...uncompleted
