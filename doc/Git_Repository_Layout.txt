﻿DogecoinFah
Git Repository Layout Overview
7-17-2022

(Document uncompleted! Mostly formatting is needed.)

reference
doc
doc/diagrams
doc/diagrams/diagrams-src
doc/diagrams/single-pages

tools
tools/FoldAPIProxy
tools/FoldAPIProxy/data
tools/SodiumTunnelServer
tools/SodiumTunnelServer/data
tools/SodiumTunnelServer/include
tools/TemplateAssembler
tools/TemplateAssembler/data
tools/TemplateAssembler/data/content
tools/TemplateAssembler/data/content/doc
tools/TemplateAssembler/data/input
tools/TemplateAssembler/data/input/data
tools/TemplateAssembler/data/input/doc
tools/TemplateAssembler/data/output
tools/TemplateAssembler/data/output/data
tools/TemplateAssembler/data/output/doc
tools/TemplateAssembler/data/release
tools/TemplateAssembler/data/templates
tools/TemplateAssembler/data/templates/doc
tools/TemplateAssembler/include

www
www/js
www/css
www/fonts
www/images
www/data

www/doc
www/doc/js
www/doc/css
www/doc/images
www/doc/de
www/doc/en
www/doc/es
www/doc/fr
www/doc/pt
www/doc/ru

www/blog
www/blog/css
www/blog/fonts
www/blog/images
www/blog/js


www/admin
www/admin/css
www/admin/images
www/admin/js
www/admin/log
www/admin/log/archive
www/admin/include
www/admin/include/XLib
www/admin/include/pages
www/admin/include/pages/admin



Terminology and short-hand:

DFah - DogecoinFah, particularly the PHP+MySQL web application and the the main web server hosting it.
F@H – Folding@Home, the distributed computing project ran by a consortium of universities and researchers.
Wallet - A full node cryptocurrency client software, such as
		dogecoind, dogecoin-qt, or a Bitcoin-like equivalent.
		Typically the Wallet runs on a different server from DFah due to disk space, processor, and bandwidth limits of paid hosting for the DFah web server. This also separates the wallet for security, in the even the DFah web server becomes compromised.

Wallet RPC - A feature you can enable in most Bitcoin-like wallet clients is the Remote Procedure Call server. This causes the Wallet to listen for network connections and provide remote access to query balances and send transactions, with properly crafted messages. This feature can be enabled and configured with command line arguments to the Wallet or through the Wallet’s configuration file.

F@H stats flat-file - A large (27MB+) file from F@H, that contains the information about all users donating processing to the F@H project. It contains one comma-delimited line for each user, with the username, team number, total folding points,  and total count of completed work units.
					  This is the flat file:  https://apps.foldingathome.org/daily_user_summary.txt.bz2


Main Overview:

doc - documents for the source code, installation, setup, etc
reference - old and no longer relevant data and information for reference
tools - a collection of optional tools to work with the project
www - the root of the project to be hosted on a website
www/admin - the start of the website's administration interface



The main public facing website:

www - The root of the website contains the public facing HTML pages. Each file starts with the page name. The default language ends there, but translations append an underscore and the two character language code to the filename. These pages are based off of the Transit template by TEMPLATED, which contains some images by Unsplash. The template uses JQuery and a JavaScript layered skeleton system to scale with page/screen size.
www/css - The CSS files supporting public pages are here. style.css and style-history.css are custom to DFah, while the rest are part of the template and Font Awesome.
www/fonts - The font files supporting the public pages are here. They are included by the template, but had some adjustments made while optimizing bandwidth and load time.
www/images - Images that get used in the public web pages are here. Currently, images associated with ad/recognition plaques are in here too.
www/js - JavaScript files for the public web pages. These include the JQuery library, the template's init and skeleton layers scripts. DFah adds loangchooser.js for the language selection box, and several other that start with "dfah". These work together to load and display live data on the public web pages, such as the team statistics, balance information, contributions list, ads/recognition plaques, history tables, history chart, etc.
www/data - This is where the DFah system's PHP backend will put static data files it generates. These are mostly JSon files, but a couple raw data files are used for bandwidth and page load optimization. These data files are loaded by the DFah JavaScript files to display live data, history, and statistics.


The public facing documentation:

www/doc - This is the website's documentation folder. It holds the How-To's, Getting Started, FAQ, and other documentation in minimized HTML format. They do not use the website's main template and attempt to be minimal, aside from the language chooser. All documents are in a sub directory of their two character language code, including the default language.
www/doc/css - The minimized CSS files supporting these document pages.
www/doc/images - The images used in these document pages, including screenshots.
www/doc/js - The JavaScript files supporting these document pages. Currently only for supporting the language chooser.
www/doc/de - The various language specific document folders... Dutch/German
www/doc/en - English
www/doc/es - Spanish
www/doc/fr - French
www/doc/pt - Portuguese
www/doc/ru - Russian

The simple public facing blog section:

www/blog - This section of the website displays simple block, starting with an index list, and having blog entries, which are created in the DFah administration section. This whole blog system was poorly put together and leaves much to be desired. It generates the index with inline JavaScript in an odd way, as well. This section does use the template the main web pages use.
www/blog/css - The CSS files to support the blog section. This is just duplicates of the main webpage's fonts, including its template's files.
www/blog/fonts - The font files to support the blog section. This is just duplicates of the main webpage's fonts, including its template's files.
www/blog/images - The images to support the blog section.
www/blog/js - The JavaScript files to support the blog section. This is just duplicates of the main template's files.

The protected administration section:

www/admin - This is the base of the administration web interface. Folders within it just be protects by security measures. See security documentation. Accessing this folder should redirect you to www/admin/Login.php if you haven't already, and allow navigating the PHP administration section. Again, this login and session system is not secure and additional security is needed (like your web server's password protection for directories). This folder also contains PHP files that get executed in the background DFah's "automation" cron job system.
www/admin/css - Minimal CSS for supporting the administration web pages.
www/admin/images - Minimal images for supporting the administration web pages.
www/admin/js - Minimal JavaScript for supporting the administration web pages. Currently just a script for displaying huge tables of data on the fly, by only showing the visible rows.

The protected logging output directory:

www/admin/log - This is the base of the logging folder. It should also be protected by security measures. Log files will be generated and stored here. The logging levels are somewhat configurable, but some log entries are quite detailed and frequently will divulge the IP of administrators and other sensitive information.
www/admin/log/archive - Log files that exceed a certain threshold, such as 2.5M, are moved here, compressed with GZip (as in tar.gz), and they are renamed to contain a unique filename with the date and time archived.

The protected PHP support files:

www/admin/include - This begins the PHP files which are included by others administration interfaces PHP entry files. These form the base of the DFah system and its functionality.
www/admin/include/XLib - This contains additional included PHP files, but started as a separate generic collection of PHP tools. It contains the login, session, and SQL database, as well as others.
www/admin/include/pages - This contains the PHP snippets for displaying the content particular interface pages. These files are included by other PHP files, when the corresponding page is selected.
www/admin/include/pages/admin - Currently, the only PHP interfaces are int he administration section. When accessing www/admin/Admin.php, it will include the PHP file here that corresponds to the administration page you are on.



tools - A collection of optional helper tools for running and administrating a DFah system.

The FoldAPIProxy for retrieving minimized F@H flat-file statics through a remote server without metered-bandwidth:

tools/FoldAPIProxy - The web root of the tool, containing the single PHP file it consists of, FoldAPIProxy.php.
tools/FoldAPIProxy/data - The protected folder where logging output files will be generated, which likely contain sensitive information. This tool utilizes the web server's temporary file locations during process, but also stores the last downloaded raw and processed F@H flat-file folding points data here.

The SodiumTunnelServer for encrypted tunnel access to a remote wallet's RPC:

tools/SodiumTunnelServer - The web root of the tool, containing the main entry PHP file, STunnel.php.
tools/SodiumTunnelServer/data - The protected folder where a sensitive configuration file is expected and where logging output files will be generated, which likely contain sensitive information.
tools/SodiumTunnelServer/include - A couple PHP files included by the main entry PHP file.

The somewhat clunky and ill-conceived HTML building template system:

tools/TemplateAssembler - The ta.sh Bash script file runs the configured set of templates and file lists, by building the proper arguments and using them with the command-line PHP execution of ta.phpsh, which is a PHP file. It takes arguments and preforms short routines with TemplateAssembler.php in tool's include directory. These take a template file and various content files, to build HTML files. It can also take an HTML file and decompile it into a content file. This was the hard way going around to decompile tranlated old versions of web pages, them compile them into many translations of newer versions of the webpages in an automated way. In practice, the template files snowballed into a complicated time consuming mess, but it works once you get the templates worked out.
tools/TemplateAssembler/include
tools/TemplateAssembler/data
