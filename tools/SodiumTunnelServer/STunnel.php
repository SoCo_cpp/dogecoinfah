<?php
//---------------------------------
/*
 * STunnel.php
 * 
*/
//---------------------------------
define('STUNNEL_DEBUG_MODE', true);
define('STUNNEL_LOG_FILENAME', './data/stunnel.log');
define('STUNNEL_CFG_FILENAME', './data/stunnel.cfg');
//---------------------------------
require('./include/SodiumMessenger.php');
require('./include/SodiumTunnelServer.php');
//---------------------------------
error_reporting(E_ALL); // E_ALL for all or 0 (zero) for none
//---------------------------------
function debugLog($msg)
{
	$fh = @fopen(STUNNEL_LOG_FILENAME, 'a+');
	if ($fh === false)
		return;
	@fwrite($fh, date('Y-m-d H:i:s')." $msg\r\n");
	@fclose($fh);
}
//---------------------------------
function faildie($msg)
{
	header('HTTP/1.1 500 Internal Server Error');
	debugLog($msg);
	die();
}
//---------------------------------
function writeConfigSkeleton()
{
	$config = array('authUser' => '', 'authPub' => '', 'authPriv' => '', 'remoteAuth' =>  array('<user name>', '<pub key>'), 'tunnelPriv' => array('<remote user name>' => array('simpleAllow' => array('host' => '192.168.0.100', 'port' => 80))));
	$txtConfig = json_encode($config);
	if (@file_put_contents(STUNNEL_CFG_FILENAME, $txtConfig) === false)
		return 'readConfig Write config skeleton failed';
	return true;
}
//---------------------------------
function readConfig($tserver)
{
	$txtConfig = @file_get_contents(STUNNEL_CFG_FILENAME);
	if ($txtConfig === false)
	{
		if (!@file_exists(STUNNEL_CFG_FILENAME))
		{
			debugLog('Config file doesnt exist, writing skeleton...');
			$ret = writeConfigSkeleton();
			if ($ret !== true)
				return 'readConfig writeConfigSkeleton (file doesnt exist) failed: '.$ret;
			return 'readConfig Config skeleton wrote, quitting';
		}
		return 'readConfig Read config file failed';
	}
	$config = json_decode($txtConfig, true /*associated array*/);
	//debugLog('config loaded: '.SVarDump($config).', txt '.SVarDump($txtConfig));
	if ($config === false)
		return 'readConfig json_decode config failed';
	if ($config === null)
	{
		if (strlen($txtConfig) != 0)
			return 'readConfig json_decode config failed';
		debugLog('Empty config detected, writing skeleton...');
		$ret = writeConfigSkeleton();
		if ($ret !== true)
			return 'readConfig writeConfigSkeleton (empty config) failed: '.$ret;
		return 'readConfig Config skeleton wrote, quitting';
	}
	if (!is_array($config) || !isset($config['authUser']) || !isset($config['authPub']) || !isset($config['authPriv']) || !isset($config['remoteAuth']) || !is_array($config['remoteAuth']))
		return 'readConfig validate Config structure and fields failed';
	if (!$tserver->setAuth($config['authUser'], $tserver->decode64($config['authPub']), $tserver->decode64($config['authPriv'])))
		return 'readConfig setAuth failed. lastError: '.$tserver->messenger->lastError();
	foreach ($config['remoteAuth'] as $name => $key)
		if (preg_match('/[<>]+/', $name) !== 1 && preg_match('/[<>]+/', $key) !== 1) // skip default placeholders
			if (!$tserver->messenger->addRemoteAuth($name, $tserver->decode64($key)))
				return 'readConfig addRemoteAuth for remoteUser \''.$name.'\' failed. lastError: '.$tserver->messenger->lastError();
	if (isset($config['tunnelPriv']) && is_array($config['tunnelPriv']))
	{
		foreach ($config['tunnelPriv'] as $userName => $rule)
			if (preg_match('/[<>]+/', $userName) !== 1 && is_array($rule))
				foreach ($rule as $ruleType => $ruleArgs)
					if ($ruleType === 'simpleAllow')
					{
						if (!is_array($ruleArgs) || !isset($ruleArgs['host']) || !is_string($ruleArgs['host']) || !isset($ruleArgs['port']) || ($ruleArgs['port'] !== '*' && !is_numeric($ruleArgs['port'])))
							return 'readConfig tunnelPriv simpleAllow verify args failed';
						if (!$tserver->addTunnelRuleSimpleAllow($userName, $ruleArgs['host'], $ruleArgs['port']))
							return 'readConfig tserver addTunnelRuleSimpleAllow failed: '.$tserver->lastError();
					}
					else
						return 'readConfig unknown ruleType for tunnelPriv';
	} // tunnelPriv
	return true;
}
//---------------------------------
if (ini_get('allow_url_fopen') != '1')
{
	ini_set('allow_url_fopen', 1);
	if (ini_get('allow_url_fopen') != '1')
		faildie("STunnel.php PHP ini option 'allow_url_fopen' could not be enabled. fsockopen connectivity will not be permitted.");
}
//---------------------------------
$tserver = new SodiumTunnelServer(STUNNEL_DEBUG_MODE /*debugMode*/) or faildie('Create SodiumTunnelServer object failed');
//---------------------------------
$ret = readConfig($tserver);
if ($ret !== true)
	faildie('readConfig failed: '.$ret);
//---------------------------------
$request = file_get_contents('php://input');
if ($request === false)
	faildie('Get php/input request contents failed');
//---------------------------------
$reply = $tserver->handleRequest($request);
if ($reply === false)
	faildie('tserver handleRequest failed: '.$tserver->lastError());
//---------------------------------
header('Content-length: '.strlen($reply));
header('Content-Type: application/octet-stream');
//---------------------------------
//debugLog('Reply: '.$reply."\n"); // very verbose logging of all replies
//---------------------------------
echo $reply;
//---------------------------------
?>
