<?php
//---------------------------------
/*
 * SodiumTunnelServer.php
 * 
*/
//---------------------------------
define('SODIUMTSERVER_HTTP_REPLY_TIMEOUT_SECS', 5);
define('SODIUMTSERVER_HTTP_READ_TIMEOUT_USECS', 250000);
//---------------------------------
class SodiumTunnelServer
{
	var $debugMode = false;
	var $lastError = false;
    var $messenger = false;
    var $remoteUser = false;
    var $tunnelRules = array('simpleAllow' => array());
    
    function __construct($debugMode = false) 
    {
		$this->debugMode = $debugMode;
		$this->messenger = new SodiumMessenger($debugMode) or die('Create object failed');
    }

	function setDebug($mode = true)
	{
		$this->debugMode = ($mode === true ? true : false);
	}
	
	function lastError($defaultValue = '')
	{
		if ($this->lastError === false)
			return $defaultValue;
		return $this->lastError;
	}
	
	function clearError($debugMsg = false)
	{
		if ($this->debugMode)
			$this->lastError = $debugMsg;
		else
			$this->lastError = false;		
	}
	
	function appendError($msg)
	{
		if ($this->lastError === false)
			$this->lastError = $msg;
		else 
			$this->lastError .= "\r\n".$msg;
	}
	
	function appendDebug($debugMsg)
	{
		if ($this->debugMode)
			$this->appendError($debugMsg);
	}
	
	function generateKeypair()
	{
		return $this->messenger->generateKeypair();
	}
	
	function encode64($data)
	{
		return $this->messenger->encode64($data);
	}
	
	function decode64($data)
	{
		return $this->messenger->decode64($data);
	}

    function setAuth($user = false, $pubLocalKey = false, $privLocalKey = false)
    {
		return $this->messenger->setAuth($user, $pubLocalKey, $privLocalKey);
	}
	
	function addRemoteAuth($user, $pubKey)
	{
		return $this->messenger->addRemoteAuth($user, $pubKey);
	}
	
	function removeRemoteAuth($user)
	{
		$this->messenger->removeRemoteAuth($user);
	}
	
	function addTunnelRuleSimpleAllow($user, $host, $port)
	{
		if (strlen($user) < 2 || strlen($host) < 3 || ($port !== '*' && (!is_numeric($port) || $port <= 0 || $port > 65535)))
		{
			$this->appendError('SodiumTunnelServer::addTunnelRuleSimpleAllow validate prameters failed');
			return false;
		}
		if (!isset($this->tunnelRules['simpleAllow']))
			$this->tunnelRules['simpleAllow'] = array();
		if (!isset($this->tunnelRules['simpleAllow'][$user]))
			$this->tunnelRules['simpleAllow'][$user] = array();
		$this->tunnelRules['simpleAllow'][$user][] = array('host' => $host, 'port' => $port);
		return true;
	}

	function checkTunnelRules($user, $host, $port)
	{
		if (strlen($user) < 2 || strlen($host) < 3 || !is_numeric($port) || $port <= 0 || $port > 65535)
			return false; // parameters invalid
		foreach ($this->tunnelRules as $ruleType => $ruleArgs)
			if ($ruleType == 'simpleAllow' && isset($ruleArgs[$user]))
			{
				foreach ($ruleArgs[$user] as $simpleRule)
					if (is_array($simpleRule) && isset($simpleRule['host']) && isset($simpleRule['port']))
						if ($simpleRule['host'] === $host && ($simpleRule['port'] === '*' || $simpleRule['port'] === $port))
							return true; // matched simpleAllow rule
			}
		return false; // default disallow
	}

	function getHeloMessage()
	{
		$this->clearError('SodiumTunnelServer::getHeloMessage');
		$pkt = $this->messenger->buildEmptyMsg();
		if ($pkt === false)
		{
			$this->appendError('SodiumTunnelServer::getHeloMessage messenger buildEmptyMsg failed: '.$this->messenger->lastError());
			return false;
		}
		return $pkt;
	}

	function getMessage($message)
	{
		$this->clearError('SodiumTunnelServer::getMessage');
		if ($this->remoteUser === false)
		{
			$this->appendError('SodiumTunnelServer::getMessage remoteUser not known failing');
			return false;
		}
		$pkt = $this->messenger->buildMsg($this->remoteUser, $message);
		if ($pkt === false)
		{
			$this->appendError('SodiumTunnelServer::getMessage messenger buildMsg failed: '.$this->messenger->lastError());
			return false;
		}
		return $pkt;
	}
	
	function handleRequest($request)
	{
		$this->clearError('SodiumTunnelServer::handleRequest');
		$messageData = $this->messenger->parseMsg($request, true);
		if ($messageData === false)
		{
			$this->appendError('SodiumTunnelServer::handleRequest messenger parseMsg failed: '.$this->messenger->lastError());
			return false;
		}
		if (!is_array($messageData) || sizeof($messageData) != 2 || strlen($messageData[0]) == 0)
		{
			$this->appendError('SodiumTunnelServer::handleRequest validate helo messageData failed: '.SVarDump($messageData));
			return false;
		}
		$remoteUser = $messageData[0];
		$message = $messageData[1];
		$this->remoteUser = $remoteUser;
		
		if (strlen($message) == 0) // helo
		{
			$replyMessage = $this->getHeloMessage();
			if ($replyMessage === false)
			{
				$this->appendError('SodiumTunnelServer::handleRequest getHeloMessage (helo) failed');
				return false;
			}
			return $replyMessage;
		}
		$json = json_decode($message, true /*associated array*/);
		if (!is_array($json) || sizeof($json) != 2 || !isset($json['t']) || !isset($json['c']))
		{
			$this->appendError('SodiumTunnelServer::handleRequest validate message failed. json: '.SVarDump($json).', message: '.SVarDump($message));
			return false;
		}
		$tunnelAddress = $json['t'];
		$tunnelMessage = $json['c'];
		$parts = parse_url($tunnelAddress);
		$host = (isset($parts['host']) ? $parts['host'] : '');
		$port = (isset($parts['port']) ? $parts['port'] : 80);
		
		if (!$this->checkTunnelRules($this->remoteUser, $host, $port))
		{
			$this->appendError('SodiumTunnelServer::handleRequest checkTunnelRules disallowed tunnel from user: '.$this->remoteUser.' to host: '.$host.', port: '.$port);
			return false;
		}
		$stream = @fsockopen($host, $port, $rErrorCode, $rErrorString);
		if ($stream === false)
		{
			$this->appendError('SodiumTunnelServer::handleRequest fsockopen failed to open tunnelAddress: '.$tunnelAddress.', errorCode: '.$rErrorCode.', errorString: '.$rErrorString);
			return false;
		}
		if (@fwrite($stream, $tunnelMessage) === false)
		{
			$this->appendError('SodiumTunnelServer::handleRequest fwrite tunnelMessage failed');
			@fclose($stream);
			return false;
		}
		if (!@fflush($stream))
		{
			$this->appendError('SodiumTunnelServer::handleRequest fflush failed');
			@fclose($stream);
			return false;
		}
		//------------------
		// This will ensure the stream blocks reading until a short
		// timeout with no input, rather than the 30 second default
		// or dealing with timers and no blocking.
		//------------------
		if (!@stream_set_blocking($stream, true))
		{
			$this->appendError('SodiumTunnelServer::handleRequest stream_set_blocking failed');
			@fclose($stream);
			return false;
		}
		//------------------
		if (!@stream_set_timeout($stream, 0 /*sec*/, SODIUMTSERVER_HTTP_READ_TIMEOUT_USECS /*microsec*/))
		{
			$this->appendError('SodiumTunnelServer::handleRequest stream_set_timeout failed');
			@fclose($stream);
			return false;
		}
		//------------------
		$timeout = time() + SODIUMTSERVER_HTTP_REPLY_TIMEOUT_SECS; // seconds
		$reply = '';
		do
		{
			$reply .= @stream_get_contents($stream);
			if ($reply === false)
			{
				$this->appendError('SodiumTunnelServer::handleRequest stream_get_contents failed');
				@fclose($stream);
				return false;
			}
		} while ($reply == '' && time() < $timeout);
		@fclose($stream);
		$replyMessage = $this->getMessage($reply);
		if ($replyMessage === false)
		{
			$this->appendError('SodiumTunnelServer::handleRequest getMessage reply forward failed');
			return false;
		}
		return $replyMessage;
	}
} // class SodiumTunnelServer
//---------------------------------
?>
