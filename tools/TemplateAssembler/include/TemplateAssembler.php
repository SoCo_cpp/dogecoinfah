<?php
//---------------------------------
/*
 * include/TemplateAssembler.php
 * 
 * 
*/
//---------------------------------
define('TA_VERSION', 1);
define('TA_DEF_TOKEN_ESC_START', '{{');
define('TA_DEF_TOKEN_ESC_END', '}}');
define('TA_DEF_TOKEN_ESC_FUNC', '@');
define('TA_DEF_TOKEN_ESC_VAR', '$');
//-------------------------------------------------------------
// source: Laravel Framework
// https://github.com/laravel/framework/blob/8.x/src/Illuminate/Support/Str.php
if (!function_exists('str_starts_with')) {
    function str_starts_with($haystack, $needle) {
        return (string)$needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0;
    }
}
if (!function_exists('str_ends_with')) {
    function str_ends_with($haystack, $needle) {
        return $needle !== '' && substr($haystack, -strlen($needle)) === (string)$needle;
    }
}
if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
}
//-------------------------------------------------------------
class TemplateAssembler
{
	var $defSessionStates = array(
					'state/header' => false,
					'state/HEADERS_LOADING' => false,
					'state/HEADERS_LOADED' => false
							);
	// 'state/current_read_file'
	// 'state/current_read_file_full'
	// 'state/only_header'
	// 'state/header_end_next_line'

	var $defSessionVariables = array(
					'var/TA_VERSION' => TA_VERSION,
					'var/ESC_START' => TA_DEF_TOKEN_ESC_START,
					'var/ESC_END' => TA_DEF_TOKEN_ESC_END,
					'var/ESC_FUNC' => TA_DEF_TOKEN_ESC_FUNC,
					'var/ESC_VAR' => TA_DEF_TOKEN_ESC_VAR,
					'var/DEPTH_MAX' => 50
							);
	//------------------
	function __construct($mergeDefSessionStates = array(), $mergeDefSessionVariables = array())
	{
		//------------------------------------
		if (is_array($mergeDefSessionStates))
			$this->defSessionStates = XAssArrayMerge($this->defSessionStates, $mergeDefSessionStates);
		//------------------------------------
		if (is_array($mergeDefSessionVariables))
			$this->defSessionVariables = XAssArrayMerge($this->defSessionVariables, $mergeDefSessionVariables);
		//------------------------------------
	}
	//------------------
	function createSession($mergeSession = false)
	{
		//------------------------------------
		$session = XAssArrayConcat($this->defSessionStates, $this->defSessionVariables);
		//------------------------------------
		if (is_array($mergeSession))
			$session = XAssArrayMerge($session, $mergeSession);
		//------------------------------------
		return $session;
	}
	//------------------
	function resetSessionStates($session, $savedStateNames = false, $mergeSession = false)
	{
		//------------------------------------
		if (!is_array($savedStateNames))
			$savedStates = false;
		else
		{
			$savedStates = array();
			foreach($savedStateNames as $stateName)
				if (isset($session[$stateName]))
					$savedStates[$stateName] = $session[$stateName];
		}
		//------------------------------------
		$session = XAssArrayMerge($session, $this->defSessionStates);
		//------------------------------------
		if (is_array($savedStates))
			$session = XAssArrayMerge($session, $savedStates);
		//------------------------------------
		if (is_array($mergeSession))
			$session = XAssArrayMerge($session, $mergeSession);
		//------------------------------------
		return $session;
	}
	//------------------
	function readTemplateFile($session)
	{
		//------------------------------------
		if (!isset($session['state/current_read_file_full']) || $session['state/current_read_file_full'] === false || 
			!isset($session['state/current_read_file']) || $session['state/current_read_file'] === false)
			return array('ok' => false, 'error' => 'read', 'errorMsg' => 'readTemplateFile no current_read_file set', 'session' => $session);
		//------------------------------------
		$fileText = @file_get_contents($session['state/current_read_file_full']);
		if ($fileText === false)
			return array('ok' => false, 'error' => 'read', 'errorMsg' => 'readTemplateFile read file failed', 'session' => $session, 'fileName' => $session['state/current_read_file'], 'fileNameFull' =>$session['state/current_read_file_full']);
		//------------------------------------
		$session = $this->resetSessionStates($session, array('state/current_read_file', 'state/current_read_file_full') /*savedStateNames*/); 
		//------------------------------------
		$reply = $this->parseTemplateText($fileText, $session);
		$reply['fileName'] = $session['state/current_read_file'];
		$reply['fileNameFull'] = $session['state/current_read_file_full'];
		//------------------------------------
		return $reply;
	}
	//------------------
	function readContentFile($session)
	{
		//------------------------------------
		if (!isset($session['state/current_read_file_full']) || $session['state/current_read_file_full'] === false || 
			!isset($session['state/current_read_file']) || $session['state/current_read_file'] === false)
			return array('ok' => false, 'error' => 'read', 'errorMsg' => 'readContentFile no current_read_file set', 'session' => $session);
		//------------------------------------
		$fileText = @file_get_contents($session['state/current_read_file_full']);
		if ($fileText === false)
			return array('ok' => false, 'error' => 'read', 'errorMsg' => 'readContentFile read file failed', 'session' => $session, 'fileName' => $session['state/current_read_file'], 'fileNameFull' =>$session['state/current_read_file_full']);
		//------------------------------------
		$session = $this->resetSessionStates($session, array('state/current_read_file', 'state/current_read_file_full') /*savedStateNames*/); 
		//------------------------------------
		$reply = $this->parseContentText($fileText, $session);
		$reply['fileName'] = $session['state/current_read_file'];
		$reply['fileNameFull'] = $session['state/current_read_file_full'];
		//------------------------------------
		return $reply;
	}
	//------------------
	function parseTemplateText($text, $session, $outTokens = false)
	{
		//------------------------------------
		$tokens = array();
		$outText = '';
		//------------------------------------
		$escStart = $session['var/ESC_START'];
		$escEnd = $session['var/ESC_END'];
		$savedSessionHeaderState = $session['state/header'];
		$session['state/header'] = false;
		//------------------------------------
		$outPos = 0;
		$inPos = 0;
		$done = false;
		while (!$done)
		{
			//------------------------------------
			$stPos = strpos($text, $escStart, $inPos);
			if ($stPos === false)
				$done = true;
			else
			{
				//------------------------------------
				if (!$session['state/header'])
					$outText .= substr($text, $outPos, ($stPos - $outPos));
				//------------------------------------
				$line = substr_count($text, "\n", 0, $stPos);
				$stPos += strlen($escStart);
				//------------------------------------
				$fnPos = strpos($text, $escEnd, $stPos);
				if ($fnPos === false)
					return array('ok' => false, 'error' => 'parse', 'errorMsg' => 'Token started but not finished', 'line' => $line, 'pos' => $stPos, 'token' => substr($text, $stPos, 20));
				//------------------------------------
				$tokenText = trim(substr($text, $stPos, ($fnPos - $stPos)));
				//------------------------------------
				if ($tokenText == '')
					return array('ok' => false, 'error' => 'parse', 'errorMsg' => 'Token empty', 'line' => $line, 'pos' => $stPos, 'token' => substr($text, $stPos, 20));
				//------------------------------------
				$outPos = $inPos = $fnPos + strlen($escEnd);
				//------------------------------------
				$tokenParts = $this->parseTokenName($tokenText, $session);
				if (!$tokenParts['ok'])
				{
					$tokenParts['parentError'] = 'parseTemplateText parseTokenName failed';
					return $tokenParts;
				}
				//------------------------------------
				$tokenResult = $this->handleToken($tokenParts, $session, 'parseTemplateText', $outTokens, 10 /*depthMax*/, true /*nonActive*/);
				if (!$tokenResult['ok'])
				{
					if (!isset($tokenResult['parentError']))
						$tokenResult['parentError'] = '';
					$tokenResult['parentError'] .= ', parseTemplateText handleToken failed';
					if (!isset($tokenResult['failTokens']))
						$tokenResult['failTokens'] = array();
					$tokenResult['failTokens'][] = $tokenParts;
					return $tokenResult;
				}
				//------------------------------------
				if ($tokenResult['handled'])
				{
					//------------------------------------
					if ($tokenResult['sessionModified'])
					{
						//------------------------------------
						$escStart = $session['var/ESC_START'];
						$escEnd   = $session['var/ESC_END'];
						//------------------------------------
						$oldSession = $session;
						$session = $tokenResult['session'];
						//------------------------------------
						if ($session['state/header'] != $oldSession['state/header']) // header state changed
						{
							//------------------------------------
							if (!$session['state/header']) // header state ended 
							{
								//------------------------------------
								if (isset($session['state/header_end_next_line']) && $session['state/header_end_next_line'])
								{
									// header should consume text until after next new line
									$nlPos = strpos($text, "\n", $inPos);
									if ($nlPos !== false)
									{
										$outPos = $inPos = $nlPos + 1; // have outText ignore text until after next new line
										$session['state/header_end_next_line'] = false;
										unset($session['state/header_end_next_line']);
									}
								}
								//------------------------------------
								if (isset($session['state/only_header']) && $session['state/only_header'])
									$done = true; // end of header, only header set, skip rest
								//------------------------------------
							} // if header state ended
							//------------------------------------
						} // if header state changed
						//------------------------------------
					} // if session modified
					//------------------------------------
				} // if handled
				//------------------------------------
				$tokens[] = array('pos' => strlen($outText), 'stPos' => $stPos, 'fnPos' => $fnPos, 'line' => $line, 'full' => $tokenText, 'name' => $tokenParts['name'], 'index' => $tokenParts['index'], 'args' => $tokenParts['args'], 'value' => $tokenParts['value'], 'type' => $tokenParts['type'], 'meta' => $tokenResult['handled'], 'header' => $session['state/header']);
				//------------------------------------
			} // if ($stPos === false) else
			//------------------------------------
		} // while (!$done)
		//------------------------------------
		if (!$session['state/header'])
			$outText .= substr($text, $outPos); // left over
		$session['state/header'] = $savedSessionHeaderState;
		//------------------------------------
		return array('ok' => true, 'error' => false, 'tokens' => $tokens, 'template' => $outText, 'template_raw' => $text, 'session' => $session);
	}
	//------------------
	function parseContentText($text, $session, $outTokens = false)
	{
		//------------------------------------
		$crnlCount = substr_count($text, "\r\n");
		$nlCount = substr_count($text, "\n");
		//------------------------------------
		if ($crnlCount != 0 && $crnlCount != $nlCount)
			array('ok' => false, 'error' => 'split lines', 'errorMsg' => 'Inconsistent line endings detected');
		//------------------------------------
		$lineDel = ($crnlCount > 0 ? "\r\n" : "\n");
		$lines = explode($lineDel, $text);
		//------------------------------------
		$defines = array();
		$lastNote = '';
		//------------------------------------
		for ($l = 0;$l < sizeof($lines);$l++)
		{
			//------------------------------------
			$startLine = $l;
			$line = $lines[$l];
			$trimLine = trim($line);
			//------------------------------------
			if (str_starts_with($trimLine, '#')) // comment line: record comment as last note, but don't parse
				$lastNote = substr($trimLine, 1); // skip comment delimiter and keep right end trimmed
			else if (strlen($trimLine) == 0) // blank line: clear last note, but parse
				$lastNote = '';
			else // parse line
			{
				//------------------------------------
				$wasHeader = $session['state/header'];
				$tokenParts = $this->parseTokenName($line, $session);
				if (!$tokenParts['ok'])
				{
					//------------------------------------
					while (!$tokenParts['ok'] && $tokenParts['error'] == 'not finished' && ++$l < sizeof($lines))
					{
						$line .= $lineDel.$lines[$l];
						$tokenParts = $this->parseTokenName($line, $session);
					}
					//------------------------------------
					if (!$tokenParts['ok'])
					{
						$tokenParts['parentError'] = 'parseContentText parseTokenName failed';
						return $tokenParts;
					}
					//------------------------------------
				}
				//------------------------------------
				$tokenResult = $this->handleToken($tokenParts, $session, 'parseContentText', $outTokens, 10 /*depthMax*/, true /*nonActive*/);
				if (!$tokenResult['ok'])
				{
					$tokenResult['parentError'] = 'parseContentText handleToken failed';
					return $tokenResult;
				}
				//------------------------------------
				if ($tokenResult['handled'])
					if ($tokenResult['sessionModified'])
					{
						//------------------------------------
						$oldSession = $session;
						$session = $tokenResult['session'];
						//------------------------------------
						if ($session['state/header'] != $oldSession['state/header']) // header state changed
						{
							//------------------------------------
							if (!$session['state/header']) // header state ended 
							{
								//------------------------------------
								if (isset($session['state/only_header']) && $session['state/only_header'])
									$done = true; // end of header, only header set, skip rest
								//------------------------------------
							} // if header state ended
							//------------------------------------
						} // if header state changed
						//------------------------------------
					} // if metaResult handled and sessionModified
				//------------------------------------
				$defines[] = array('line' => $startLine, 'full' => $line, 'name' => $tokenParts['name'], 'index' => $tokenParts['index'], 'args' => $tokenParts['args'], 'value' => $tokenParts['value'], 'notes' => $lastNote, 'type' => $tokenParts['type'], 'handled' => $tokenResult['handled'], 'header' => ($wasHeader || $session['state/header']) );
				//------------------------------------
				$lastNote = '';
				//------------------------------------
			} // else not comment or blank line
			//------------------------------------
		} // for lines
		//------------------------------------
		return array('ok' => true, 'error' => false, 'defines' => $defines, 'content' => $text, 'session' => $session);
	}
	//------------------
	function parseDefineFromContentLine($session, $line, $outTokens = false, $notes = '')
	{
		//------------------------------------
		$tokenParts = $this->parseTokenName($line, $session);
		if (!$tokenParts['ok'])
		{
			$tokenParts['parentError'] = 'parseDefineFromContentLine parseTokenName failed';
			return $tokenParts;
		}
		//------------------------------------
		$tokenResult = $this->handleToken($tokenParts, $session, 'parseDefineFromContentLine', $outTokens, 10 /*depthMax*/, true /*nonActive*/);
		if (!$tokenResult['ok'])
		{
			$tokenResult['parentError'] = 'parseDefineFromContentLine handleToken failed';
			return $tokenResult;
		}
		//------------------------------------
		if ($tokenResult['handled'])
			if ($tokenResult['sessionModified'])
			{
				//------------------------------------
				$oldSession = $session;
				$session = $tokenResult['session'];
				//------------------------------------
			} // if  handled and sessionModified
		//------------------------------------
		return array('ok' => true, 'line' => -1, 'full' => $line, 'name' => $tokenParts['name'], 'index' => $tokenParts['index'], 'args' => $tokenParts['args'], 'value' => $tokenParts['value'], 'notes' => $notes, 'type' => $tokenParts['type'], 'handled' => $tokenResult['handled'], 'header' => $session['state/header']);
	}
	//------------------
	function analyzeDefines($defines)
	{
		//------------------------------------
		$metaNames = array();
		$recursiveNames = array();
		$recursiveContentNames = array();
		$recursiveDefaultedNames = array();
		$recursiveReplacedCount = 0;
		$session = $this->createSession();
		//------------------------------------
		foreach ($defines as $define)
		{
			//------------------------------------
			$result = $this->parseTemplateText($define['value'], $session);
			if (!$result['ok'])
			{
				$result['parentError'] = 'analyzeDefine parseTemplate failed';
				$result['defineName'] = $define['name'];
				$result['defineLine'] = $define['line'];
				return $result;
			}
			//------------------------------------
			if (sizeof($result['tokens']) > 0)
			{
				//------------------------------------
				$recursiveNames[] = $define['name'];
				//------------------------------------
				foreach ($result['tokens'] as $recTok)
					if (!in_array($recTok['name'], $recursiveContentNames))
						$recursiveContentNames[] = $recTok['name'];
				//------------------------------------
				$session = $this->resetSessionStates($session);
				$result = $this->assemble($session, $result['tokens'], $result['template'], $defines);
				if (!$result['ok'])
				{
					$result['parentError'] = 'analyzeDefine assemble failed';
					$result['defineName'] = $define['name'];
					$result['defineLine'] = $define['line'];
					return $result;
				}
				//------------------------------------
				$recursiveReplacedCount += $result['replacedCount'];
				//------------------------------------
				foreach ($result['defaultedTokens'] as $defTok)
					if (!in_array($defTok, $recursiveDefaultedNames))
						$recursiveDefaultedNames[] = $defTok;
				//------------------------------------
			}
			//------------------------------------
		} // foreach defines
		//------------------------------------
		return array('ok' => true, 'error' => false, 'recursiveReplacedCount' => $recursiveReplacedCount, 'metaNames' => $metaNames, 'recursiveNames' => $recursiveNames, 'recursiveContentNames' => $recursiveContentNames, 'recursiveDefaultedNames' => $recursiveDefaultedNames);
	}
	//------------------
	function unquote($inTextOrArray, $escapeDecode = false, $ignoreUnclosedQuotes = true, $unclosedQuoteValue = false)
	{
		//------------------------------------
		if (is_array($inTextOrArray))
		{
			foreach (array_keys($inTextOrArray) as $key)
				$inTextOrArray[$key] = $this->unquote($inTextOrArray[$key], $escapeDecode);
			return $inTextOrArray;
		}
		//------------------------------------
		if (!is_string($inTextOrArray))
			return $inTextOrArray; // whatever it is, it isn't quoted and can't be stripped
		//------------------------------------
		$outText = $inTextOrArray;
		$trimText = trim($outText);
		//------------------------------------
		if (str_starts_with($trimText, '"'))
		{
			if (strlen($trimText) >= 2 && str_ends_with($trimText, '"') && !str_ends_with($trimText, '\"'))
				$outText = substr($trimText, 1, -1);
			else if (!$ignoreUnclosedQuotes)
				return $unclosedQuoteValue;
		}
		else if (str_starts_with($trimText, "'"))
		{
			if (strlen($trimText) >= 2 && str_ends_with($trimText, "'") && !str_ends_with($trimText, "\\'"))
				$outText = substr($trimText, 1, -1);
			else if (!$ignoreUnclosedQuotes)
				return $unclosedQuoteValue;
		}
		//------------------------------------
		if ($escapeDecode)
			$outText = $this->escapeDecode($outText);
		//------------------------------------
		return $outText;
	}
	//------------------
	function escapeDecode($in, $del = '\\')
	{
		//------------------
		$out = '';
		//------------------
		$esc = false;
		$len = strlen($in);
		for ($i = 0;$i < $len;$i++)
		{
			//------------------
			$c = $in[$i];
			//------------------
			if ($esc)
			{
				switch ($c)
				{
					case 'n': $out .= "\n"; break;
					case 'r': $out .= "\r"; break;
					case 't': $out .= "\t"; break;
					case 'Q': $out .= '"'; break;
					case 'q': $out .= '\''; break;
					case 'e': $out .= '='; break;
					case 'P': $out .= '('; break;
					case 'p': $out .= ')'; break;
					case 'B': $out .= '['; break;
					case 'b': $out .= ']'; break;
					case 'A': $out .= '<'; break;
					case 'a': $out .= '>'; break;
					case 'U': $out .= '{'; break;
					case 'u': $out .= '}'; break;
					case 'c': $out .= ','; break;
					default: $out .= $c; break; // this includes $del
				}
				$esc = false;
			}
			else if ($c == $del)
				$esc = true;
			else
				$out .= $c;
			//------------------
		}
		//------------------
		return $out;
	}
	//------------------
	function escapeEncode($in, $full = false, $strict = false, $del = '\\')
	{
		//------------------
		$out = '';
		//------------------
		if ($strict)
			$full = true;
		//------------------
		$esc = false;
		$len = strlen($in);
		for ($i = 0;$i < $len;$i++)
		{
			//------------------
			$c = $in[$i];
			//------------------
			switch ($c)
			{
				case $del: $out .= $del.$del; break;
				case "\n": $out .= $del.'n'; break;
				case "\r": $out .= $del.'r'; break;
				case "\t": $out .= $del.'t'; break;
				case '"':  $out .= $del.($strict ? 'Q' : $c); break;
				case '\'': $out .= $del.($strict ? 'q' : $c); break;
				case '=':  $out .= ($full ? $del : '').($strict ? 'e' : $c); break;
				case '(':  $out .= ($full ? $del : '').($strict ? 'P' : $c); break;
				case ')':  $out .= ($full ? $del : '').($strict ? 'p' : $c); break;
				case '[':  $out .= ($full ? $del : '').($strict ? 'B' : $c); break;
				case ']':  $out .= ($full ? $del : '').($strict ? 'b' : $c); break;
				case '<':  $out .= ($full ? $del : '').($strict ? 'A' : $c); break;
				case '>':  $out .= ($full ? $del : '').($strict ? 'a' : $c); break;
				case '{':  $out .= ($full ? $del : '').($strict ? 'U' : $c); break;
				case '}':  $out .= ($full ? $del : '').($strict ? 'u' : $c); break;
				case ',':  $out .= ($full ? $del : '').($strict ? 'c' : $c); break;
				default: $out .= $c; break;
			}
			//------------------
		}
		//------------------
		return $out;
	}
	//------------------
	function parseTokenName($tokenName, $session)
	{
		//------------------------------------
		$full = $tokenName;
		$name = $tokenName;
		$index = '';
		$args = array();
		$value = false;
		$type = 'normal';
		//------------------------------------
		$dPos = strpos($name, '=');
		if ($dPos !== false)
		{
			$newValue = substr($name, ($dPos+1), (strlen($name)-$dPos-1));
			$newName = substr($name, 0, $dPos);
			$unqNewValue = $this->unquote($newValue, true /*stripSlashes*/, false /*ignoreUnclosedQuotes*/, false /*unclosedQuoteValue*/);
			if ($unqNewValue === false)
				return array('ok' => false, 'error' => 'not finished', 'errorMsg' => 'Quoted value\'s end quote not found', 'name' => $newName);
			$name = $newName;
			$value = $unqNewValue;
		}
		//------------------------------------
		if (str_ends_with($name, ')'))
		{
			$dPos = strpos($name, '(');
			if ($dPos !== false)
			{
				$argsText = substr($name, ($dPos+1), (strlen($name)-$dPos-2));
				$name = substr($name, 0, $dPos);
				$args = explode(',', $argsText);
				$args = $this->unquote($args, true /*stripSlashes*/);
			}
		}
		//------------------------------------
		if (str_ends_with($name, ']'))
		{
			$dPos = strpos($name, '[');
			if ($dPos !== false)
			{
				$index = substr($name, ($dPos+1), (strlen($name)-$dPos-2));
				$index = $this->unquote($index, true /*stripSlashes*/);
				$name = substr($name, 0, $dPos);
			}
		}
		//------------------------------------
		if (str_starts_with($name, $session['var/ESC_FUNC']))
		{
			$type = 'func';
			$name = substr($name, strlen($session['var/ESC_FUNC']));
		}
		//------------------------------------
		if (str_starts_with($name, $session['var/ESC_VAR']))
		{
			$type = 'var';
			$name = substr($name, strlen($session['var/ESC_VAR']));
		}
		//------------------------------------
		return array('ok' => true, 'error' => false, 'full' => $full, 'name' => $name, 'index' => $index, 'args' => $args, 'value' => $value, 'type' => $type);
	}
	//------------------
	function findFirstTokenByName($tokenList, $tokenName, $tokenIndex = null, $tokenType = null, $requireValue = false)
	{
		//------------------------------------
		$cnt = sizeof($tokenList);
		for ($i = 0;$i < $cnt;$i++)
			if ($tokenList[$i]['name'] == $tokenName && ($tokenIndex === null || $tokenIndex == $tokenList[$i]['index']) &&
					($tokenType === null || $tokenType == $tokenList[$i]['type']) && ($requireValue === false || $tokenList[$i]['value'] !== false))
				return $tokenList[$i];
		//------------------------------------
		return false;
	}
	//------------------
	function findTokensByName($tokenList, $tokenName, $tokenIndex = null, $tokenType = null, $requireValue = false)
	{
		//------------------------------------
		$foundList = array();
		//------------------------------------
		$cnt = sizeof($tokenList);
		for ($i = 0;$i < $cnt;$i++)
			if ($tokenList[$i]['name'] == $tokenName &&
					($tokenIndex === null || ($tokenIndex === '#' && is_numeric($tokenList[$i]['index'])) || $tokenIndex == $tokenList[$i]['index']) &&
					($tokenType === null || $tokenType == $tokenList[$i]['type']) && ($requireValue === false || $tokenList[$i]['value'] !== false))
				$foundList[] = $tokenList[$i];
		//------------------------------------
		return $foundList;
	}
	//------------------
	function handleToken($inToken, $session, $source, $outTokens = false, $depthMax = false, $nonActive = false)
	{
		//------------------------------------
		$reply = array('ok' => true, 'error' => false, 'handled' => false, 'session' => $session, 'sessionModified' => false, 'output' => false, 'source' => $source, 'nonActive' => $nonActive, 'tokenName' => $inToken);
		//------------------------------------
		if ($depthMax === false)
			$depthMax = $session['var/DEPTH_MAX'];
		if ($depthMax <= 0)
		{
			$reply['ok'] = false;
			$reply['error'] = 'Depth max';
			$reply['errorMsg'] = 'handleToken: Depth max';
			$reply['trace'] = XStackTrace();
			return $reply;
		}
		//------------------
		if (!is_array($inToken) || !isset($inToken['type']) || !isset($inToken['name']) || !isset($inToken['value']) || !isset($inToken['args']) || !is_array($inToken['args']) || !isset($inToken['index']) || ($outTokens !== false && !is_array($outTokens)))
		{
			$reply['ok'] = false;
			$reply['error'] = 'params';
			$reply['errorMsg'] = 'handleToken validate inToken failed';
			return $reply;
		}
		//------------------------------------
		$tokentype = $inToken['type'];
		$tokenName = $inToken['name'];
		$tokenValue = $inToken['value'];
		$tokenIndex = $inToken['index'];
		$tokenArgs = $inToken['args'];
		//------------------------------------
		// Handle Variables
		//------------------------------------
		if ($tokentype == 'var')
		{
			//------------------------------------
			$varIndexComputed = false;
			$reply['handled'] = true;
			$varKey = $varKeyName = 'var/'.$tokenName;
			//------------------------------------
			$varIndexNew = $varIndex = $tokenIndex;
			//------------------------------------
			$varKey = $varKeyName;
			if ($tokenIndex != '')
				$varKey .= '['.$tokenIndex.']';
			//------------------------------------
			if ($tokenIndex === '#' || $tokenIndex === '##') // #: count of numeric, ##: count of any
			{
				$varKey = $varKeyName.'[';
				$varKeyLen = strlen($varKey);
				$matchCount = 0;
				foreach ($session as $key => $value)
					if (strncmp($key, $varKey, $varKeyLen) === 0 && $key[strlen($key)-1] === ']' && ($tokenIndex == '##' || is_numeric(substr($key, $varKeyLen, -1))))
						$matchCount++;
				$varIndexNew = $varIndex = ''.$matchCount;
				$varIndexComputed = true;
				$varKey = $varKeyName.'['.$tokenIndex.']';
				//if ($tokenIndex === '##')
				//	echo "Var Index ## found $matchCount matches named '$varKey' in session:\n".XVarDump($session)."\n";
			}
			else
			{
				if ($tokenIndex === '-@' || $tokenIndex === '--' || $tokenIndex === '++')
				{
					$varIndex = '@';
					$varKey = $varKeyName.'['.$varIndex.']';
				}
				$value = (isset($session[$varKey]) ? $session[$varKey] : false);
				if (($tokenIndex === '@' || $tokenIndex === '-@' || $tokenIndex === '--' || $tokenIndex === '++') && ($value === false || $value === '' || is_numeric($value)))
				{
					$newValue = false;
					if ($value === false || $value == '')
						if ($tokenIndex !== '-@')
							$newValue = $value = 0;
						else // -@ (last)
						{
							$varKey = $varKeyName.'[';
							$varKeyLen = strlen($varKey);
							$matchCount = 0;
							foreach ($session as $key => $value)
								if (strncmp($key, $varKey, $varKeyLen) === 0 && $key[strlen($key)-1] === ']' && is_numeric(substr($key, $varKeyLen, -1)))
									$matchCount++;
							$newValue = $value = $matchCount -1;
							$varKey = $varKeyName.'['.$varIndex.']';
						}
					if ($tokenIndex === '--')
						$newValue = $value - 1;
					if ($tokenIndex === '++')
						$newValue = $value + 1;
					$varIndexNew = $newValue;
					$varIndexComputed = true;
					if ($newValue !== false)
					{
						//echo "Variable compute varKey '$varKey' set to newValue: ".XVarDump($newValue)."\n";
						$session[$varKey] = $newValue;
						$reply['sessionModified'] = true;
						$reply['session'] = $session;
					}
					$varIndex = $value;
					//echo "Variable varKey '$varKey', index '$tokenIndex', current '$value', computed. next value: ".XVarDump($newValue).", token value: ".XVarDump($tokenValue)."\n";
				}
				else
				{
					//echo "Variable varKey '$varKey', index '$tokenIndex', current '$value', not computed. token: ".XVarDump($inToken['full'])."\n";
					$varIndexNew = $varIndex = $value;
					// not computed
				}
			}
			//------------------------------------
			if ($tokenValue === false)
			{
				$reply['output'] = $varIndex;
				//echo "Variable out varKey '$varKey', index ".($varIndexComputed ? "(computed) '".$varIndex."'/" : '')."'$tokenIndex', current ".XVarDump($session[$varKey])."\n";
			}
			else
			{
				//------------------------------------
				if ($varIndexComputed && ($tokenIndex === '-@' || $tokenIndex === '--' || $tokenIndex === '++'))
					$varKey = $varKeyName.'['.$varIndex.']'; // use old index, change is after this use
				else if ($varIndexComputed && $tokenIndex === '#' || $tokenIndex === '##')
				{
					$reply['ok'] = false;
					$reply['error'] = 'Index use not allowed';
					$reply['errorMsg'] = 'Cannot set value of a count index';
					$reply['errorToken'] = $inToken;
					return $reply;
				}
				// else no index, not computed index, or index was @ (current), just set it with original tokenIndex if any
				//------------------------------------
				//if ($tokenIndex !== false && $tokenIndex != '' && $tokenIndex != '@')
				//	echo "Variable varKey '$varKey', index ".($varIndexComputed ? "(computed) '".$varIndex."'/" : '')."'$tokenIndex', current ".XVarDump($session[$varKey]).", setting to value: ".XVarDump($tokenValue)."\n";//.", token: ".XVarDump($inToken)."\n";
				//------------------------------------
				if (!$nonActive)
				{
					$assembleResult = $this->assembleText($session, $tokenValue, $outTokens, 'handleToken variable set value' /*source*/, '' /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText of variable set value failed";
						$assembleResult['handleToken'] = $inToken;
						return $assembleResult;
					}
					$session = $assembleResult['session'];
					$tokenValue = $assembleResult['outText'];
				}
				//------------------------------------
				$session[$varKey] = $tokenValue;
				$reply['sessionModified'] = true;
				$reply['session'] = $session;
				//------------------------------------
			}
			//------------------------------------
			return $reply; // always handled
		} // $tokentype == 'var'
		//------------------------------------
		// Handle Functions
		//------------------------------------
		if ($tokentype == 'func')
		{
			//------------------------------------
			// Handle functions with system content
			//------------------------------------
			if ($tokenName == 'START_HEADER' || $tokenName == 'END_HEADER' || $tokenName == 'END_HEADER_INLINE')
			{
				//------------------------------------
				if ($tokenName == 'END_HEADER')
					$session['state/header_end_next_line'] = true;
				//------------------------------------
				$session['state/header'] = ($tokenName == 'START_HEADER');
				$reply['handled'] = true;
				$reply['session'] = $session;
				$reply['sessionModified'] = true;
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'UNIX_TIMESTAMP')
			{
				//------------------------------------
				$reply['handled'] = true;
				$reply['output'] = ''.time();
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'DATETIME')
			{
				//------------------------------------
				$format = DateTimeInterface::W3C;
				//------------------------------------
				if (sizeof($tokenArgs) > 0)
				{
					$fmt = $tokenArgs[0];
					if (strcasecmp(trim($fmt), 'W3C') == 0)
						$format = DATE_W3C;
					else if (strcasecmp(trim($fmt), 'RSS') == 0)
						$format = DATE_RSS;
					else if (strcasecmp(trim($fmt), 'INTL') == 0)
						$format = 'D, d M Y H:i \U\T\C';
					else if (strcasecmp(trim($fmt), 'US') == 0)
						$format = 'D, M d, Y g:ia \U\T\C';
				}
				//------------------------------------
				$dtNow = new DateTime('now', new DateTimeZone('UTC'));
				//------------------------------------
				$reply['handled'] = true;
				$reply['output'] = $dtNow->format($format);
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'DATE')
			{
				//------------------------------------
				$format = 'd/M/Y';
				//------------------------------------
				if (sizeof($tokenArgs) > 0)
				{
					$fmt = $tokenArgs[0];
					if (strcasecmp(trim($fmt), '/INTL') == 0)
						$format = 'd/m/Y';
					else if (strcasecmp(trim($fmt), '/US') == 0)
						$format = 'n/j/Y';
					else if (strcasecmp(trim($fmt), '-INTL') == 0)
						$format = 'd-m-Y';
					else if (strcasecmp(trim($fmt), '-US') == 0)
						$format = 'n-j-Y';
				}
				//------------------------------------
				$dtNow = new DateTime('now', new DateTimeZone('UTC'));
				//------------------------------------
				$reply['handled'] = true;
				$reply['output'] = $dtNow->format($format);
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'FOR' && !$nonActive) // FOR(outTokenName, outValue, [startIndex], [endIndex])
			{
				//------------------------------------
				if ($outTokens !== false && sizeof($tokenArgs) >= 2)
				{
					//------------------------------------
					$forOutTokenName = $tokenArgs[0];
					$forOutValue     = $tokenArgs[1];
					$forStartIndex   = (sizeof($tokenArgs) >= 3 ? trim($tokenArgs[2]) : 0);
					$forEndIndex     = (sizeof($tokenArgs) >= 4 ? trim($tokenArgs[3]) : '');
					//------------------------------------
					if ($forOutTokenName !== false && $forOutTokenName !== '' && is_numeric($forStartIndex) && ($forEndIndex === '' || is_numeric($forEndIndex)) )
					{
						$outTokenMatches = $this->findTokensByName($outTokens, $forOutTokenName, '#' /*only numeric*/);
						if (sizeof($outTokenMatches) > 0)
						{
							//------------------------------------
							$minIdx = $maxIdx = false;
							//------------------------------------
							foreach ($outTokenMatches as $tokMatch)
							{
								if ($minIdx === false || $tokMatch['index'] < $minIdx)
									$minIdx = $tokMatch['index'];
								if ($maxIdx === false || $tokMatch['index'] > $maxIdx)
									$maxIdx = $tokMatch['index'];
							}
							//------------------------------------
							if ($forStartIndex > $maxIdx)
								$forStartIndex = $maxIdx;
							if ($forStartIndex < $minIdx)
								$forStartIndex = $minIdx;
							//------------------------------------
							if ($forEndIndex === '')
								$forEndIndex = $maxIdx;
							else
							{
								if ($forEndIndex > $maxIdx)
									$forEndIndex = $maxIdx;
								if ($forEndIndex < $minIdx)
									$forEndIndex = $minIdx;
							}
							//------------------------------------
							$out = '';
							if ($forEndIndex >= $forStartIndex) // forward
							{
								for ($i = $forStartIndex;$i <= $forEndIndex;$i++)
									$out .= $session['var/ESC_START'].'$'.$forOutTokenName.'[@]="'.$i.'"'.$session['var/ESC_END'].$forOutValue;
							}
							else // reverse
							{
								for ($i = $forStartIndex;$i >= $forEndIndex;$i--)
									$out .= $session['var/ESC_START'].'$'.$forOutTokenName.'[@]="'.$i.'"'.$session['var/ESC_END'].$forOutValue;
							}
							//------------------------------------
							$reply['handled'] = true;
							$reply['output'] = $out;
							//echo "FOR start $forStartIndex, end $forEndIndex, source $source, out:\n'''$out'''\n";
							//------------------------------------
							return $reply;
						}
						echo "FOR: no token matches source $source, args: ".XVarDump($tokenArgs)."\n";
						//------------------------------------
						return $reply; // malformed, not handled
					}
					//------------------------------------
					echo "FOR malformed: token not specified  or bad start/stop, source $source, args: ".XVarDump($tokenArgs)."\n";
					//------------------------------------
					return $reply; // malformed, not handled
				}
				//------------------------------------
				if ($outTokens === false)
					echo "FOR malformed: no outTokens, source $source\n";
				else
					echo "FOR malformed: not enough args: ".XVarDump($tokenArgs).", source $source\n";
				return $reply; // malformed, not handled
			}
			else if ($tokenName == 'REPEAT' && !$nonActive) // REPEAT(outValue, count, [outDelimiter])
			{
				//------------------------------------
				if ($outTokens !== false && sizeof($tokenArgs) >= 2)
				{
					//------------------------------------
					$repeatOutValue = ''.$tokenArgs[0]; // ensure string
					$repeatCount = $tokenArgs[1];
					$repeatDelimiter = (isset($tokenArgs[2]) ? ''.$tokenArgs[2] : ''); // ensure string
					//------------------------------------
					if (!is_numeric($repeatCount))
					{
						$assembleResult = $this->assembleText($session, $repeatCount, $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
						if (!$assembleResult['ok'])
						{
							if (!isset($assembleResult['parentError']))
								$assembleResult['parentError'] = '';
							$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText failed";
							$assembleResult['handleToken'] = $inToken;
							return $assembleResult;
						}
						$reply['sessionModified'] = true;
						$reply['session'] = $session = $assembleResult['session'];
						$repeatCount = $assembleResult['outText'];
					}
					//------------------------------------
					if (is_numeric($repeatCount) && $repeatCount >= 0 && ($repeatCount * (strlen($repeatOutValue) + strlen($repeatDelimiter))) < 1000 /*sloppy math, but sensible max characters to output*/)
					{
						//------------------------------------
						//echo "REPEAT: (starting loop) outValue ".XVarDump($repeatOutValue).", repeatCount ".XVarDump($repeatCount).", delimiter ".XVarDump($repeatDelimiter).", source $source, args:\n".XVarDump($tokenArgs,false)."\n";
						$out = '';
						while ($repeatCount--)
							$out .= $repeatOutValue.($repeatCount > 0 ? $repeatDelimiter : ''); // add delimiter if not last one (could be empty)
						//------------------------------------
						$reply['handled'] = true;
						$reply['output'] = $out;
						//echo "REPEAT: (loop done) out:\n'''$out'''\n";
						//------------------------------------
						return $reply;
					}
					echo "REPEAT malformed: repeatCount invalid or exceeds output character max, source $source, repeatCount: ".XVarDump($repeatCount).", args: ".XVarDump($tokenArgs)."\n";
					//------------------------------------
					return $reply; // malformed, not handled
				}
				//------------------------------------
				if ($outTokens === false)
					echo "REPEAT malformed: no outTokens, source $source\n";
				else
					echo "REPEAT malformed: not enough args: ".XVarDump($tokenArgs).", source $source\n";
				return $reply; // malformed, not handled
			}
			else if ( ($tokenName == 'BEGIN' || $tokenName == 'RBEGIN') && !$nonActive) // BEGIN(tokenName) / RBEGIN(tokenName) sets token index position to first or last index
			{
				//------------------------------------
				if ($outTokens === false || sizeof($tokenArgs) != 1 || $tokenArgs[0] == '')
				{
					if ($outTokens === false)
						echo "$tokenName malformed: no outTokens, source $source\n";
					else
						echo "$tokenName malformed: invalid arg: ".XVarDump($tokenArgs).", source $source\n";
					return $reply;
				}
				//------------------------------------
				$beginTokenName =  ''.$tokenArgs[0]; // ensure string
				$tokenMatches = $this->findTokensByName($outTokens, $beginTokenName, '#' /*only numeric*/);
				$maxCount = sizeof($tokenMatches);
				//------------------------------------
				if ($maxCount === 0)
				{
					echo "$tokenName: no indexed tokens named '$beginTokenName', source $source\n\toutTokens:\ninToken:\n".XVarDump($inToken, false)."\noutTokens:\n".XVarDump($outTokens, false)."\n";
					return array('ok' => false, 'error' => 'missing', 'errorMsg' => 'Function missing required token');
				}
				else
				{
					if ($tokenName == 'BEGIN')
						$indexValue = 0;
					else
						$indexValue = $maxCount - 1;

					$reply['handled'] = true;
					$reply['output'] = $session['var/ESC_START'].'$'.$beginTokenName.'[@]="'.$indexValue.'"'.$session['var/ESC_END'];
					//echo "$tokenName: token '$beginTokenName', indexValue $indexValue, source $source, out: ".XVarDump($reply['output'])."\n";
				}
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'REFERENCE' && !$nonActive) // REFERENCE(tokenName, [tokenIndex])
			{
				//------------------------------------
				if ($outTokens !== false && sizeof($tokenArgs) >= 1)
				{
					//------------------------------------
					$refName = ''.$tokenArgs[0]; // ensure string
					$refIndex = (isset($tokenArgs[1]) ? ''.$tokenArgs[1] : ''); 
					//------------------------------------
					$assembleResult = $this->assembleText($session, $refName, $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText (name) failed";
						$assembleResult['failToken'] = $inToken;
						$assembleResult['failText'] = $refName;
						return $assembleResult;
					}
					//------------------------------------
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$refNameOut = $assembleResult['outText'];
					//------------------------------------
					if (trim($refNameOut) == '')
					{
						if (!$session["state/HEADERS_LOADING"]) // error, but simply not handled with headers
						{
							$reply['ok'] = false;
							$reply['error'] = "Bad function argument";
							$reply['errorMsg'] = "$tokenName assembleText of name parameter gave a blank reply. refName: ".XVarDump($refName).", assembleResult: ".XVarDump($assembleResult)."\n";
							$reply['parseToken'] = $inToken;
						}
						return $reply;
					}
					//------------------------------------
					if ($refIndex != '')
					{
						$assembleResult = $this->assembleText($session, $refIndex, $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
						if (!$assembleResult['ok'])
						{
							if (!isset($assembleResult['parentError']))
								$assembleResult['parentError'] = '';
							$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText (index) failed";
							$assembleResult['failToken'] = $inToken;
							$assembleResult['failText'] = $refIndex;
							return $assembleResult;
						}
						$reply['sessionModified'] = true;
						$reply['session'] = $session = $assembleResult['session'];
						$refIndexOut = $assembleResult['outText'];
						if (trim($refNameOut) == '')
						{
							if (!$session["state/HEADERS_LOADING"]) // error, but simply not handled with headers
							{
								$reply['ok'] = false;
								$reply['error'] = "Bad function argument";
								$reply['errorMsg'] = "$tokenName assembleText of index parameter gave a blank reply. refIndex: ".XVarDump($refIndex).", assembleResult: ".XVarDump($assembleResult)."\n";
								$reply['parseToken'] = $inToken;
							}
							return $reply;
						}
					}
					else $refIndexOut = $refIndex;
					//------------------------------------
					if ($refIndexOut != '')
						$refNameOut .= '['.$refIndexOut.']';
					//------------------------------------
					$parseReply = $this->parseTokenName($refNameOut, $session);
					if (!$parseReply['ok'])
					{
						if (!isset($parseReply['parentError']))
							$parseReply['parentError'] = '';
						$parseReply['parentError'] .= ", ($source) handleToken [$tokenName] parseTokenName failed";
						$parseReply['failToken'] = $inToken;
						$parseReply['failParseName'] = $refNameOut;
						return $parseReply;
					}
					//------------------------------------
					$recReply = $this->handleToken($parseReply, $session, $source.'(recurs func reference)', $outTokens, ($depthMax-1), $nonActive);
					if (!$recReply['ok'])
					{
						if (!isset($recReply['parentError']))
							$recReply['parentError'] = '';
						$recReply['parentError'] .= ", ($source) handleToken [$tokenName] recursive handleToken failed";
						$recReply['failToken'] = $inToken;
						$recReply['failParsedName'] = $parseReply;
						return $recReply;
					}
					//------------------------------------
					//echo "$tokenName result: ".XVarDump($recReply['output']).", source $source, Ref Name: '$refNameOut', parseReply:\n".XVarDump($parseReply)."\n rec handleToken reply:\n".XVarDump($recReply)."\n\n";
					$reply['handled'] = true;
					$reply['output'] = $recReply['output'];
					return $reply;
				}
				//------------------------------------
				if ($outTokens !== false)
					echo "$tokenName malformed: not enough args: ".XVarDump($tokenArgs).", source $source\n";
				return $reply; // malformed, not handled
			}
			else if (!$nonActive && ($tokenName == 'MATH_ADD' || $tokenName == 'MATH_SUB' || $tokenName == 'MATH_MUL' || $tokenName == 'MATH_DIV' || $tokenName == 'MATH_MIN' || $tokenName == 'MATH_MAX')) // MATH_xx(valLeft,valRight)
			{
				//------------------------------------
				if ($outTokens === false || sizeof($tokenArgs) != 2 || $tokenArgs[0] == '' || $tokenArgs[1] == '')
				{
					if ($outTokens === false)
						echo "$tokenName malformed: no outTokens, source $source\n";
					else
						echo "$tokenName malformed: invalid arg: ".XVarDump($tokenArgs).", source $source\n";
					return $reply;
				}
				//------------------------------------
				$valueA = $tokenArgs[0];
				$valueB = $tokenArgs[1];
				//------------------------------------
				if (!is_numeric($valueA))
				{
					$assembleResult = $this->assembleText($session, $valueA, $outTokens, 'handleToken math func arg' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText failed";
						$assembleResult['handleToken'] = $inToken;
						return $assembleResult;
					}
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$valueA = $assembleResult['outText'];
				}
				//------------------------------------
				if (!is_numeric($valueB))
				{
					$assembleResult = $this->assembleText($session, $valueB, $outTokens, 'handleToken math func arg' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText failed";
						$assembleResult['handleToken'] = $inToken;
						return $assembleResult;
					}
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$valueB = $assembleResult['outText'];
				}
				//------------------------------------
				if (!is_numeric($valueA) || !is_numeric($valueB))
				{
					echo "$tokenName malformed: non-numeric arg: ".XVarDump($tokenArgs).", source $source\n";
					return $reply;
				}
				//------------------------------------
				if ($tokenName == 'MATH_ADD')
					$reply['output'] = $valueA + $valueB;
				else if ($tokenName == 'MATH_SUB')
					$reply['output'] = $valueA - $valueB;
				else if ($tokenName == 'MATH_MUL')
					$reply['output'] = $valueA * $valueB;
				else if ($tokenName == 'MATH_DIV')
					$reply['output'] = $valueA / $valueB;
				else if ($tokenName == 'MATH_MIN')
					$reply['output'] = ($valueA < $valueB ? $valueA : $valueB);
				else if ($tokenName == 'MATH_MAX')
					$reply['output'] = ($valueA > $valueB ? $valueA : $valueB);
				//------------------------------------
				$reply['handled'] = true;
				//echo "$tokenName: valA ".XVarDump($valueA).", valB ".XVarDump($valueB).", args ".XVarDump($tokenArgs).", out: ".XVarDump($reply['output'])."\n";
				//------------------------------------
				return $reply;
			}
			else if (!$nonActive && ($tokenName == 'REPLACE' || $tokenName == 'IREPLACE')) // REPLACE/IREPLACE(inText,targetReplace,outReplace='')  I for case insensitive
			{
				//------------------------------------
				if ($outTokens === false || (sizeof($tokenArgs) != 2 && sizeof($tokenArgs) != 3) || $tokenArgs[1] == '')
				{
					if ($outTokens === false)
						echo "$tokenName malformed: no outTokens, source $source\n";
					else
						echo "$tokenName malformed: invalid arg: ".XVarDump($tokenArgs).", source $source\n";
					return $reply;
				}
				//------------------------------------
				$inText = $tokenArgs[0];
				$targetReplace = $tokenArgs[1];
				$outReplace = (isset($tokenArgs[2]) ? $tokenArgs[2] : ''); // default to replace with blank string (ie delete matches)
				//------------------------------------
				if ($inText == '') // blank input, nothing to replace
				{
					$reply['handled'] = true;
					$reply['output'] = '';
					return $reply;
				}
				else
				{
					$assembleResult = $this->assembleText($session, $inText, $outTokens, 'handleToken replace func arg' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText argument inText failed";
						$assembleResult['handleToken'] = $inToken;
						$assembleResult['failText'] = $inText;
						return $assembleResult;
					}
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$inTextOut = $assembleResult['outText'];
				}
				//------------------------------------
				$assembleResult = $this->assembleText($session, $targetReplace, $outTokens, 'handleToken math func arg' /*source*/, false /*defaultValue*/, ($depthMax -1));
				if (!$assembleResult['ok'])
				{
					if (!isset($assembleResult['parentError']))
						$assembleResult['parentError'] = '';
					$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText argument targetReplace failed";
					$assembleResult['handleToken'] = $inToken;
					return $assembleResult;
				}
				$reply['session'] = $session = $assembleResult['session'];
				$targetReplaceOut = $assembleResult['outText'];
				//------------------------------------
				if ($targetReplaceOut === false || $targetReplaceOut == '')
				{
					$reply['ok'] = false;
					$reply['error'] = "Bad function argument";
					$reply['errorMsg'] = "$tokenName bad argument 2 targetReplace is blank";
					$reply['errorToken'] = $inToken;
					return $reply;					
				}
				//------------------------------------
				if ($outReplace == '')
					$outReplaceOut = '';
				else
				{
					$assembleResult = $this->assembleText($session, $outReplace, $outTokens, 'handleToken replace func arg' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText argument outReplace failed";
						$assembleResult['handleToken'] = $inToken;
						$assembleResult['failText'] = $inText;
						return $assembleResult;
					}
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$outReplaceOut = $assembleResult['outText'];
				}
				//------------------------------------
				if ($tokenName == 'IREPLACE')
					$outText = str_ireplace($targetReplaceOut, $outReplaceOut, $inTextOut);
				else
					$outText = str_replace($targetReplaceOut, $outReplaceOut, $inTextOut);
				//------------------------------------
				$reply['handled'] = true;
				$reply['output'] = $outText;
				//echo "$tokenName: inTextOut ".XVarDump($inTextOut).", targetReplaceOut ".XVarDump($targetReplaceOut).", outReplaceOut ".XVarDump($outReplaceOut).", outText: ".XVarDump($outText)."\n";
				//------------------------------------
				return $reply;
			}
			else if ($tokenName == 'ECHO' || $tokenName == 'ECHO_ASM')
			{
				//------------------------------------
				if ($outTokens === false || sizeof($tokenArgs) != 1 || $tokenArgs[0] == '')
				{
					if ($outTokens === false)
						echo "$tokenName malformed: no outTokens, source $source\n";
					else
						echo "$tokenName malformed: invalid arg: ".XVarDump($tokenArgs).", source $source\n";
					return $reply;
				}
				//------------------------------------
				$outText =  ''.$tokenArgs[0]; // ensure string
				if ($tokenName == 'ECHO_ASM')
				{
					$assembleResult = $this->assembleText($session, $outText, $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
					if (!$assembleResult['ok'])
					{
						if (!isset($assembleResult['parentError']))
							$assembleResult['parentError'] = '';
						$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText failed";
						$assembleResult['handleToken'] = $inToken;
						return $assembleResult;
					}
					$reply['sessionModified'] = true;
					$reply['session'] = $session = $assembleResult['session'];
					$outText = $assembleResult['outText'];
				}
				//------------------------------------
				echo $outText;
				$reply['handled'] = true;
				return $reply;
			}
			//------------------------------------
			// Handle token function
			//------------------------------------
			if ($tokenValue !== false && $outTokens !== false && !$nonActive)
			{
				//------------------------------------
				if (sizeof($tokenArgs) > 0)
				{
					//------------------------------------
					$varKey = 'var/'.$tokenName.'_ARG[';
					$varKeyLen = strlen($varKey);
					//------------------------------------ Remove previous args
					foreach ($session as $key => $value)
						if (strncmp($key, $varKey, $varKeyLen) === 0)
							unset($session[$key]);
					//------------------------------------ Add args
					for ($i = 0;$i < sizeof($tokenArgs);$i++)
					{
						$assembleResult = $this->assembleText($session, $tokenArgs[$i], $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
						if (!$assembleResult['ok'])
						{
							if (!isset($assembleResult['parentError']))
								$assembleResult['parentError'] = '';
							$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText func arg $i failed";
							$assembleResult['handleToken'] = $inToken;
							return $assembleResult;
						}
						if ($assembleResult['outText'] === false || $assembleResult['outText'] == '')
						{
							if (!$session["state/HEADERS_LOADING"]) // error, but simply not handled with headers
							{
								$reply['ok'] = false;
								$reply['error'] = "Bad function argument";
								$reply['errorMsg'] = "$tokenName assembleText of argument $i gave a blank reply. argText: ".XVarDump($tokenArgs[$i]).", assembleResult: ".XVarDump($assembleResult)."\n";
								$reply['parseToken'] = $inToken;
							}
							return $reply;
						}
						$reply['sessionModified'] = true;
						$reply['session'] = $session = $assembleResult['session'];
						$session[$varKey.$i.']'] = $assembleResult['outText'];
					}
					//------------------------------------
				}
				//------------------------------------
				$assembleResult = $this->assembleText($session, $tokenValue, $outTokens, 'handleToken' /*source*/, false /*defaultValue*/, ($depthMax -1));
				if (!$assembleResult['ok'])
				{
					if (!isset($assembleResult['parentError']))
						$assembleResult['parentError'] = '';
					$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText func failed";
					$assembleResult['handleToken'] = $inToken;
					return $assembleResult;
				}
				//------------------------------------
				$reply['sessionModified'] = true;
				$reply['session'] = $assembleResult['session'];
				$reply['handled'] = true;
				$reply['output'] = $assembleResult['outText'];
				//------------------------------------
				return $reply;
			}
			else if ($outTokens !== false && !$nonActive)
			{
				$funcOutToken = $this->findFirstTokenByName($outTokens, $tokenName, $tokenIndex, 'func', true /*requireValue*/);
				if ($funcOutToken !== false)
				{
					//------------------------------------
					$origFuncOutToken = $funcOutToken;
					$funcOutToken['args'] = $tokenArgs;
					$funcOutToken['index'] = $tokenIndex;
					//------------------------------------
					$recReply = $this->handleToken($funcOutToken, $session, $source.'(recurs func out token)', $outTokens, ($depthMax-1), $nonActive);
					if (!$recReply['ok'])
					{
						if (!isset($recReply['parentError']))
							$recReply['parentError'] = '';
						$recReply['parentError'] .= ", ($source) handleToken [$tokenName] recursive handleToken func out token failed";
						$recReply['failTokenIn'] = $inToken;
						$recReply['failTokenOut'] = $origFuncOutToken;
						return $recReply;
					}
					//------------------------------------
					//echo "Function handled recursively token: '$tokenName', source $source, inToken:\n ".XVarDump($inToken, false)."\nfuncOutToken:\n ".XVarDump($origFuncOutToken,false)."\nrecReply:\n".XVarDump($recReply, false)."\n\n";
					//------------------------------------
					return $recReply;
				}
				else echo "Unhandled function token: '$tokenName' no funcOutTokenFound...\n";				
			}
			//------------------------------------
			//echo "Unhandled function token: '$tokenName', source $source, outTokenCount: ".(is_array($outTokens) ? sizeof($outTokens) : XVarDump($outTokens)).",inToken: ".XVarDump($inToken)."\n";
			return $reply; // not handled
		} // $tokentype == 'func'
		//------------------------------------
		// Handle Normal Tokens
		//------------------------------------
		if ($outTokens !== false && !$nonActive)
		{
			//------------------------------------
			// Handle Index
			//------------------------------------
			if ($tokenIndex !== false && $tokenIndex !== '')
			{
				//------------------------------------
				if ($tokenIndex == '#' || $tokenIndex == '##') // # - count of numeric indexes, ## - count of all indexes
				{
					$outTokenMatches = $this->findTokensByName($outTokens, $tokenName, ($tokenIndex == '#' ? '#' /*only numeric*/ : null /*any*/));
					$reply['handled'] = true;
					$reply['output'] = ''.sizeof($outTokenMatches);
					//echo "handleToken $tokenName [$tokenIndex] #/## count: ".$reply['output']."\n";//($tokenIndex == '##' ? ", matches:\n".XVarDump($outTokenMatches).(sizeof($outTokenMatches) == 0 ? "\noutTokens:\n".XVarDump($outTokens) : '') : '')."\n";
					return $reply;						
				}
				else if ($tokenIndex == '@' || $tokenIndex == '-@' || $tokenIndex == '--' || $tokenIndex == '++') // @ - first (set current), -@ - last (set current), -- - decrement, ++  - increment
				{
					$inToken['type'] = 'var'; // call handleToken recursively telling it this is a variable token type, this will get the index and init/inc/dec it
					$otherReply = $this->handleToken($inToken, $session, $source.'(recurs index access)', false /*outTokens*/, ($depthMax-1), $nonActive);
					$tokenIndexValue = $otherReply['output'];
					if (!$otherReply['ok'] || !$otherReply['handled'] || !is_numeric($tokenIndexValue))
					{
						$fullTokenName = $this->getTokenFullName($inToken);
						echo "Token [$tokenIndex] recurs handle index failed for : ".XVarDump($fullTokenName)." reply: ".XVarDump($otherReply)."\n";
						return $reply;
					}
					$outTokenMatch = $this->findFirstTokenByName($outTokens, $tokenName, $tokenIndexValue);
					if ($outTokenMatch === false)
					{
						$outTokenMatch = $this->findFirstTokenByName($outTokens, $tokenName, 'default');
						echo "Token $tokenName [$tokenIndex] source $source, not found index [$tokenIndexValue]: findFirstTokenByName with index 'default' result:".XVarDump($outTokenMatch)."\nSession:\n".XVarDump($session)."\n";
					}
					if ($outTokenMatch !== false)
					{
						//echo "Token $tokenName [$tokenIndex] source $source, found at [$tokenIndexValue]: ".$this->getTokenFullName($outTokenMatch)." value: ".XVarDump($outTokenMatch['value']).", modified session: ".XVarDump($otherReply['session'])."\n";
						$reply['sessionModified'] = true;
						$reply['session'] = $otherReply['session'];
						$reply['handled'] = true;
						$reply['output'] = $outTokenMatch['value']; // could be false
						return $reply;						
					}
					else
					{
						if (str_ends_with($source, 'headers'))
						{
							$foundMatches = $this->findTokensByName($outTokens, $tokenName);
							echo "Token $tokenName [$tokenIndex] indexValue '$tokenIndexValue' not found. Source '$source', outTokenCount: ".sizeof($outTokens)."\noutTokenMatch result:".XVarDump($outTokenMatch)."\nFound Matches:\n".XVarDump($foundMatches, false)."\noutTokens:\n".XVarDump($outTokens, false)."\n";
						}
						$reply['ok'] = false;
						$reply['error'] = "Index";
						$reply['errorMsg'] = "Index value not found";
						return $reply;
					}
				}
				else
				{
					//------------------------------------
					$origTokenIndex = $tokenIndex;
					if (!is_numeric($tokenIndex))
					{
						$assembleResult = $this->assembleText($session, $tokenIndex, $outTokens, 'handleToken index' /*source*/, false /*defaultValue*/, ($depthMax-1));
						if (!$assembleResult['ok'])
						{
							echo "Token $tokenName [$tokenIndex] assembleText of index failed. Source '$source', outTokenCount: ".sizeof($outTokens).", inToken: ".XVarDump($inToken).", result:\n".XVarDump($assembleResult)."\n";//.XVarDump($outTokenMatches, false)."\n";
							$assembleResult['errorMsg'] = 'Failed to assemble token index';
							if (!isset($assembleResult['parentError']))
								$assembleResult['parentError'] = '';
							$assembleResult['parentError'] .= ", ($source) handleToken [$tokenName] assembleText index failed";
							$assembleResult['handleToken'] = $inToken;
							return $assembleResult;
						}
						$reply['sessionModified'] = true;
						$reply['session'] = $session = $assembleResult['session'];
						if ($assembleResult['outText'] === false || $assembleResult['outText'] === '')
						{
							if (!$session["state/HEADERS_LOADING"]) // error, but simply not handled with headers
							{
								//echo "Token $tokenName [$tokenIndex] assembleText of index succeeded with a blank result. Source '$source', inToken: ".XVarDump($inToken).", result:\n".XVarDump($assembleResult)."\n";
								$reply['errorMsg'] = 'Failed to assemble token index';
								$reply['ok'] = false;
								$reply['error'] = 'Bad assembled index';
								$reply['failToken'] = $inToken;
								$reply['assembleResult'] = $assembleResult;
							}
							//else echo "Token $tokenName [$tokenIndex] assembleText of index succeeded with a blank result (loading headers, ignoring).\n";
							return $reply;
						}
						$tokenIndex = $assembleResult['outText'];
					}
					//------------------------------------
					$outTokenMatch = $this->findFirstTokenByName($outTokens, $tokenName, $tokenIndex);
					if ($outTokenMatch === false)
					{
						if (!$session["state/HEADERS_LOADING"] && !str_ends_with($source, 'headers') && $source != 'disassemble token' && $source != 'parseTemplateText')
						{
							//echo "Token $tokenName [$tokenIndex] indexValue '$origTokenIndex' not found, trying default index\n";
							echo "Token $tokenName [$tokenIndex]indexValue '$origTokenIndex' not found. Source '$source', inToken: ".XVarDump($inToken)."\n";
							$reply['errorMsg'] = 'Failed to assemble token index';
							$reply['ok'] = false;
							$reply['error'] = 'Bad token index';
							$reply['failToken'] = $inToken;
							return $reply;
						}
						$outTokenMatch = $this->findFirstTokenByName($outTokens, $tokenName, 'default');
					}
					if ($outTokenMatch !== false)
					{
						//if (!is_numeric($origTokenIndex))
						//	echo "[OK]Token $tokenName [$tokenIndex] indexValue '$origTokenIndex' found. Source '$source', value: ".XVarDump($outTokenMatch)."\n";//.XVarDump($outTokenMatches, false)."\n";
						$reply['handled'] = true;
						$reply['output'] = $outTokenMatch['value']; // could be false
						return $reply;						
					}
					else if (!$session["state/HEADERS_LOADING"] && !str_ends_with($source, 'headers') && $source != 'disassemble token' && $source != 'parseTemplateText')
						echo "Token $tokenName [$tokenIndex] indexValue '$origTokenIndex' not found. Source '$source', outTokenCount: ".sizeof($outTokens)."\n";//.XVarDump($outTokenMatches, false)."\n";
				}
				//------------------------------------
			}
			//------------------------------------
			// Handle Plain
			//------------------------------------
			$outTokenMatch = $this->findFirstTokenByName($outTokens, $tokenName, $tokenIndex);
			if ($outTokenMatch !== false)
			{
				$reply['handled'] = true;
				$reply['output'] = $outTokenMatch['value']; // could be false
				return $reply;						
			}
			//------------------------------------
		}
		//------------------------------------
		// Nothing left, check if inToken has a value
		//------------------------------------
		if ($tokenValue !== false)
		{
			$reply['handled'] = true;
			$reply['output'] = $tokenValue;
			return $reply;						
		}
		//------------------------------------
		if (!$nonActive && $outTokens)// && !str_ends_with($source, 'headers') && $source != 'disassemble token' && $source != 'parseTemplateText'))
			echo "handleToken - Unhandled token: ".$this->getTokenFullName($inToken).", source $source, outTokenCount: ".(is_array($outTokens) ? sizeof($outTokens) : XVarDump($outTokens)).",inToken: ".XVarDump($inToken)."\n";
		//------------------------------------
		return $reply;
	}
	//------------------
	function assemble($session, $inTokens, $template, $outTokens, $defaultValue = false, $depthMax = false)
	{
		//------------------------------------
		if ($depthMax === false)
			$depthMax = $session['var/DEPTH_MAX'];
		if ($depthMax <= 0)
			return array('ok' => false, 'error' => 'Depth max', 'errorMsg' => 'assemble: Depth max', 'trace' => XStackTrace());
		//------------------
		if (isset($outTokens['default']))
			$defaultValue = $outTokens['default']['value'];
		//------------------------------------
		$asmDebug = (isset($session['state/debug_asm']) && $session['state/debug_asm'] ? true : false);
		//------------------------------------
		if (!XArray($session, 'state/HEADERS_LOADED', false) && !XArray($session, 'state/HEADERS_LOADING', false))
		{
			//------------------------------------
			$session['state/HEADERS_LOADING'] = true;
			//------------------------------------
			// Load all of template header to session
			//   Mostly so we can properly load all content headers
			//   These will process again
			foreach ($inTokens as $token)
				if ($token['header'])
				{
					$result = $this->handleToken($token, $session, 'assemble token headers', $outTokens, ($depthMax-1), true /*nonActive*/);
					if (!$result['ok'])
					{
						if (!isset($result['parentError']))
							$result['parentError'] = '';
						$result['parentError'] .= ', assemble inToken header handleToken failed';
						return $result;
					}
					else if ($result['handled'] && $result['sessionModified'])
						$session = $result['session'];
				}
			//------------------------------------
			// Load all of content header to session
				$outTokenCount = sizeof($outTokens);
				for ($i = 0;$i < $outTokenCount;$i++)
					if ($outTokens[$i]['header'])
					{
						$result = $this->handleToken($outTokens[$i], $session, 'assemble content headers', XArrayRemove($outTokens, $i), ($depthMax-1), true /*nonActive*/); // avoiding recursive loop from header is difficult if sending outTokens
						if (!$result['ok'])
						{
							if (!isset($result['parentError']))
								$result['parentError'] = '';
							$result['parentError'] .= ', assemble outToken header handleToken failed';
							return $result;
						}
						else if ($result['handled'])
						{
							if ($result['sessionModified'])
								$session = $result['session'];
						}
					}
			//------------------------------------
			$session['state/HEADERS_LOADING'] = false;
			$session['state/HEADERS_LOADED'] = true;
			//------------------------------------
		} // not state/HEADERS_LOADED or state/HEADERS_LOADING
		//------------------------------------
		$session['state/header'] = false; // ensure start with header off, will go through header again
		$defaultedTokens = array();
		$outPos = 0;
		$outText = '';
		$replacedCount = 0;
		foreach ($inTokens as $token)
		{
			//------------------------------------
			if (!$session['state/header'])
				$outText .= substr($template, $outPos, ($token['pos'] - $outPos));
			//------------------------------------
			$result = $this->assembleToken($session, $token, $outTokens, 'assemble' /*source*/, $defaultValue, $depthMax);
			if (!$result['ok'])
			{
				if (!isset($result['parentError']))
					$result['parentError'] = '';
				$result['parentError'] .= ', assemble assembleToken failed';
				if (!isset($result['failTokens']))
					$result['failTokens'] = array();
				$result['failTokens'][] = $token;
				return $result;
			}
			//------------------------------------
			if ($asmDebug)
				echo "ASM) ".$this->getTokenFullName($token, $session, true /*includeArguments*/).":".$result['replacedCount'].":".($result['outText'] === false ? ' ~' : "'".$this->escapeEncode($result['outText'])."'")."\n";
			//------------------------------------
			$session = $result['session'];
			$outTokenText = $result['outText'];
			$replacedCount += $result['replacedCount'];
			//------------------------------------
			foreach ($result['defaultedTokens'] as $defTok)
				$defaultedTokens[] = $defTok;
			//------------------------------------
			if (!$session['state/header'] && $outTokenText !== false)
				$outText .= $outTokenText;
			//------------------------------------
			$outPos = $token['pos'];
			$replacedCount++;
			//------------------------------------
		}
		//------------------------------------
		$outText .= substr($template, $outPos); // rest of template
		//------------------------------------
		$pageOutFile = false;
		if ($depthMax == $session['var/DEPTH_MAX'])
		{
			$outTokenMatch = $this->findFirstTokenByName($outTokens, "PAGE_OUTPUT_FILENAME");
			if ($outTokenMatch !== false)
			{
				$result = $this->assembleToken($session, $outTokenMatch, $outTokens, $source = 'assemble finish PAGE_OUTPUT_FILENAME lookup', $depthMax);
				if ($result['ok'])
					$pageOutFile = $result['outText'];
			}
		}
		//------------------------------------
		return array('ok' => true, 'fail' => false, 'pageOutFile' => $pageOutFile, 'outText' => $outText, 'replacedCount' => $replacedCount, 'defaultedTokens' => $defaultedTokens, 'session' => $session);
	}
	//------------------
	function assembleText($session, $inText, $outTokens, $source = '', $defaultValue = false, $depthMax = false)
	{
		//------------------
		if ($depthMax === false)
			$depthMax = $session['var/DEPTH_MAX'];
		if ($depthMax <= 0)
			return array('ok'=>false, 'error'=> 'Depth max', 'errorMsg' => 'assembleText: Depth max', 'source' => $source, 'session' => $session, 'trace' => XStackTrace(), 'inText' => $inText);
		//------------------
		$parseResult = $this->parseTemplateText($inText, $session);
		if (!$parseResult['ok'])
		{
			if (!isset($parseResult['parentError']))
				$parseResult['parentError'] = '';
			$parseResult['parentError'] .= ", ($source) assembleText parseTemplateText failed";
			return $parseResult;
		}
		//------------------
		$assembleResult = $this->assemble($session, $parseResult['tokens'], $parseResult['template'], $outTokens, $defaultValue, ($depthMax-1));
		if (!$assembleResult['ok'])
		{
			if (!isset($assembleResult['parentError']))
				$assembleResult['parentError'] = '';
			$assembleResult['parentError'] .= ", ($source) assembleText assemble failed";
			return $assembleResult;
		}
		//------------------
		return $assembleResult;
	}
	//------------------
	function assembleToken($session, $inToken, $outTokens, $source = '', $defaultValue = false, $depthMax = false)
	{
		//------------------------------------
		if ($depthMax === false)
			$depthMax = $session['var/DEPTH_MAX'];
		if ($depthMax <= 0)
			return array('ok'=>false, 'error'=> 'Depth max', 'errorMsg' => 'assembleToken: Depth max', 'source' => $source, 'trace' => XStackTrace(), 'inToken' => $inToken);
		//------------------
		$source = "assembleToken($source)";
		$replacedCount = 0;
		$defaultedTokens = array();
		$outText = false;
		$handled = false;
		//------------------------------------
		$result = $this->handleToken($inToken, $session, $source, $outTokens, ($depthMax-1));
		if (!$result['ok'])
		{
			if (!isset($result['parentError']))
				$result['parentError'] = '';
			$result['parentError'] .= ", ($source) assembleToken handleToken failed";
			if (!isset($result['failTokens']))
				$result['failTokens'] = array();
			$result['failTokens'][] = $inToken;
			return $result;
		}
		//------------------------------------
		if ($result['handled'])
		{
			//------------------------------------
			$handled = true;
			$outText = $result['output'];
			//------------------------------------
			if ($result['sessionModified'])
				$session = $result['session'];
			//------------------------------------
		}
		else
		{
			//------------------------------------
			if (!XArray($session, 'state/HEADERS_LOADING', false))
			{
				//------------------------------------
				//echo "$source: unhandled token '".$this->getTokenFullName($inToken)."' (Line ".$inToken['line'].")\n";
				echo "$source: unhandled token inToken: ".XVarDump($inToken).", outToken count: ".sizeof($outTokens)."\n";
				$result['ok'] = false;
				if (!isset($result['parentError']))
					$result['parentError'] = '';
				$result['parentError'] .= ", ($source) assembleToken handleToken succeeded, but didn't handle the token, forcing failure";
				if (!isset($result['failTokens']))
					$result['failTokens'] = array();
				$result['failTokens'][] = $inToken;
				$result['outTokens'] = $outTokens;
				return $result;
				//------------------------------------
			}
			//------------------------------------
			$inToken['source'] = 'assemble tokens';
			$inToken['parent'] = array();
			$defaultedTokens[] = $inToken;
			//------------------------------------
			$outText = $defaultValue;
			//------------------------------------
		}
		//------------------------------------
		if ($outText !== false && $outText != '')
		{
			//------------------------------------
			$parseResult = $this->parseTemplateText($outText, $session, $outTokens);
			if (!$parseResult['ok'])
			{
				if (!isset($parseResult['parentError']))
					$parseResult['parentError'] = '';
				$parseResult['parentError'] .= ", ($source) parseTemplateText recursively failed";
				$parseResult['failToken'] = $inToken;
				$parseResult['failText'] = $outText;
				return $parseResult;
			}
			//------------------------------------
			// ignore changes to session from parseTemplateText
			//------------------------------------
			if (sizeof($parseResult['tokens']) > 0)
			{
				//------------------------------------
				$result = $this->assemble($session, $parseResult['tokens'], $parseResult['template'] /*stripped from $outText*/, $outTokens, $defaultValue, ($depthMax-1));
				if (!$result['ok'])
				{
					if (!isset($result['parentError']))
						$result['parentError'] = '';
					$result['parentError'] .= ", ($source) assemble recursive tokens failed";
					if (!isset($result['failTokens']))
						$result['failTokens'] = array();
					$result['failTokens'][] = $inToken;
					$result['failResultTokens'] = $parseResult['tokens'];
					return $result;
				}
				//------------------------------------
				$session = $result['session'];
				$outText = $result['outText'];
				$replacedCount += $result['replacedCount'];
				//------------------------------------
				foreach ($result['defaultedTokens'] as $defTok)
					if (!$defTok['header'])
					{
						//------------------------------------
						//echo "$source: (recurs) unhandled recurs token '".$this->getTokenFullName($defTok)."', from parent '".$this->getTokenFullName($inToken)."' (Line ".$inToken['line'].")\n";
						//------------------------------------
						$defTok['source'] .= 'assemble recurs';
						$defTok['parent'][] = $inToken;
						$defaultedTokens[] = $defTok;
						//------------------------------------
					}
				//------------------------------------
			} // if sizeof tokens (has recursive tokens)
			//------------------------------------
		} // if outText and not depth max
		//------------------------------------
		//if ($inToken['name'] === 'GENERATE_PAGE_ALT_LANG_FILENAMES=' || $inToken['name'] === 'PAGE_ALT_LANG_FILENAME' || $inToken['name'] === 'LANGUAGE_CHOOSER_LIST_REPEAT' || $inToken['name'] === 'LANGUAGE_CHOOSER_COUNT' || $inToken['name'] === 'PAGE_ALT_LANG_FILENAME' || $inToken['name'] === 'CURRENT_PAGE_ALT_LANG_FILENAME')
			//echo "assembleToken token outText: ".XVarDump($outText)."\ninToken:".XVarDump($inToken)."\n";
		return array('ok' => true, 'fail' => false, 'handled' => $handled, 'outText' => $outText, 'replacedCount' => $replacedCount, 'defaultedTokens' => $defaultedTokens, 'session' => $session);
	}
	//------------------
	function disassemble($session, $inText, $inTokens, $template, $headerTokens = array(), $outTokens = array())
	{
		//------------------------------------
		for ($i = 0;$i < sizeof($headerTokens);$i++)
			$headerTokens[$i]['header'] = true;
		//------------------------------------
		$outTokens = XArrayConcat($headerTokens, $outTokens);
		//------------------------------------
		if (!XArray($session, 'state/HEADERS_LOADED', false) && !XArray($session, 'state/HEADERS_LOADING', false))
		{
			//------------------------------------
			$session['state/HEADERS_LOADING'] = true;
			//------------------------------------
			for ($i = 0;$i < sizeof($outTokens);$i++)
				if ($outTokens[$i]['header'])
				{
					$result = $this->handleToken($outTokens[$i], $session, 'assemble token headers', XArrayRemove($outTokens, $i), 10 /*depthMax*/, true /*nonActive*/);
					if (!$result['ok'])
					{
						if (!isset($result['parentError']))
							$result['parentError'] = '';
						$result['parentError'] .= ', disassemble outTokens header handleToken failed';
						return $result;
					}
					else if ($result['handled'] && $result['sessionModified'])
						$session = $result['session'];
				}
			//------------------------------------
			$session['state/HEADERS_LOADING'] = false;
			$session['state/HEADERS_LOADED'] = true;
			//------------------------------------
		} // not state/HEADERS_LOADED or state/HEADERS_LOADING
		//------------------
		$session['state/header'] = false; // ensure start with header off, will may go through headers again in tokens
		$newTokens = array();
		$newToken = false;
		$lastTokenName = false;
		$lastTokenPos = false;
		$lastTokenLine = false;
		$nextTempSecLen = 0;
		$inLastTempSecStart = 0;
		$inLastTokenStart = 0;
		//echo "<p>Disassemble IN inText ".strlen($inText).", template ".strlen($template).", inTokens ".sizeof($inTokens).", outTokens ".sizeof($outTokens)."</p>\n";
		//echo "inText: '".$this->escapeEncode($inText)."'\ntemplate: '".$this->escapeEncode($template)."'\ninTokens: ".XVarDump($inTokens)."\n";
		//------------------------------------
		$inTokens[] = array('name' => '', 'pos'=> strlen($template) /*just past end*/, 'line' => 0, 'dontHandle' => true, 'note' => 'Disassemble empty token end placeholder'); // add end placeholder
		//------------------------------------
		foreach ($inTokens as $token)
		{
			//------------------------------------
			$tokenName = $token['name'];
			$tokenPos  = $token['pos'];
			$tokenLine = $token['line'];
			//------------------------------------
			if (!$session['state/header'])
			{
				//------------------------------------
				if ($lastTokenName === false)
					$inLastTokenStart = $tokenPos; // first token
				else
				{
					//------------------------------------
					$nextTempSec = substr($template, $lastTokenPos, ($tokenPos - $lastTokenPos));
					$nextTempSecLen = strlen($nextTempSec);
					//------------------------------------
					if (false) // $lastTokenName == 'UPDATED_DATE_DATE')
					{
						//------------------------------------
						if ($tokenName === '')
							echo "<p>Cur (P$tokenPos (end of inText) [end placeholder])</p>\n";
						else
							echo "<p>Cur (Token L$tokenLine P$tokenPos '$tokenName')</p>\n";
						//------------------------------------
						echo "<p>Last (Token L$lastTokenLine P$lastTokenPos '$lastTokenName')</p>\n";
						echo "<p>inLastTempSecStart $inLastTempSecStart, inLastTokenStart $inLastTokenStart</p>\n";
						echo "<p>nextTempSecLen $nextTempSecLen = tokenPos $tokenPos - lastTokenPos $lastTokenPos</p>\n";
						echo "<p>nextTempSec ($nextTempSecLen/".($tokenPos - $lastTokenPos)."): '$nextTempSec'</p>\n";
						//------------------------------------
					}
					//------------------------------------
					$tokenNotes = '';
					if ($nextTempSecLen == 0)
					{
						$inNextTempSecStart = strlen($inText);
						$tokenNotes .= '@hitend@ ';
						//echo "<p>Hit end, setting len of inText, for token: '$lastTokenName'</p>\n";
					}
					else
					{
						$inNextTempSecStart = strpos($inText, $nextTempSec, $inLastTokenStart);
						if ($inNextTempSecStart === false)
						{
							$inNextTempSecStart = stripos($inText, $nextTempSec, $inLastTokenStart);
							if ($inNextTempSecStart === false)
								return array('ok' => false, 'error' => 'disassemble', 'errorMsg' => 'Next section not found', 'tokenName' => $tokenName, 'tokenPos' => $tokenPos, 'lastTokenPos' => $lastTokenPos, 'lastTokenName' => $lastTokenName, 'tokenLine' => $tokenLine, 'tokenNotes' => $tokenNotes, 'templateExpectedLeft_size' => strlen($nextTempSec), 'templateExpectedLeft_max100' => $this->escapeEncode(substr($nextTempSec, 0, 100)), 'expectedInText_max100' => $this->escapeEncode(substr($inText, $inLastTokenStart, (strlen($nextTempSec) < 100 ? strlen($nextTempSec) : 100))), 'lastNewToken' => $newToken );
							$tokenNotes .= '@case@ ';
						}
					}
					//------------------------------------
					$tokenText = substr($inText, $inLastTokenStart, ($inNextTempSecStart - $inLastTokenStart));
					$tokenTextLen = strlen($tokenText);
					//------------------------------------
					//if ($lastTokenName == 'UPDATED_DATE_DATE')
					//	echo "<p>tokenTextLen $tokenTextLen/".($inNextTempSecStart - $inLastTokenStart)." = inNextTempSecStart $inNextTempSecStart - inLastTokenStart $inLastTokenStart, tokenText '$tokenText', nextTempSec '$nextTempSec', template: '$template', inText: '$inText'</p>\n";
					//------------------------------------
					// @PRIORITY
					$newToken = array('value' => $tokenText, 'line' => $lastTokenLine, 'full' => $lastTokenName, 'name' => $lastTokenName, 'notes' => $tokenNotes, 'index' => '', 'args' => array(), 'type' => 'normal', 'header' => false);
					$outTokens[] = $newToken;
					$newTokens[] = $newToken;
					//------------------------------------
					$inLastTokenStart = $inNextTempSecStart + $nextTempSecLen;
					$inLastTempSecStart = $inNextTempSecStart;
					//------------------------------------
				} // if lastTokenName === false (first token) else
				//------------------------------------
			} // if not header
			//------------------------------------
			$lastTokenName = $tokenName;
			$lastTokenPos  = $tokenPos;
			$lastTokenLine = $tokenLine;
			//------------------------------------
			if (!isset($token['dontHandle']) || !$token['dontHandle'])
			{
				//------------------------------------
				$tokenResult = $this->handleToken($token, $session, 'disassemble token', $outTokens, 10 /*depthMax*/, true /*nonActive*/);
				if (!$tokenResult['ok'])
				{
					if (!isset($result['parentError']))
						$result['parentError'] = '';
					$result['parentError'] .= ', disassemble handleToken failed';
					return $tokenResult;
				}
				//------------------------------------
				if ($tokenResult['handled'])
				{
					//------------------------------------
					if ($tokenResult['sessionModified'])
					{
						//------------------------------------
						$oldSession = $session;
						$session = $tokenResult['session'];
						//------------------------------------
						if ($session['state/header'] != $oldSession['state/header']) // header state changed
						{
							//------------------------------------
							if (!$session['state/header']) // header state ended
							{
								$lastTokenName = false;
								$lastTokenPos = false;
								$lastTokenLine = false;
							}
							//------------------------------------
						} // if session header state changed
						//------------------------------------
					} // if tokenResult sessionModified
					//------------------------------------
				} // if tokenResult handled
				//------------------------------------
			} // if not token dontHandle set
			//------------------------------------
		} // foreach token
		//------------------------------------
		return array('ok' => true, 'fail' => false, 'tokens' => $outTokens, 'newTokens' => $newTokens, 'content' => $inText);
	}
	//------------------
	function disassembleContentTemplate($session, $inTokens, $outTokens, $headerTokens = array(), $extraTokens = array())
	{
		//------------------------------------
		$session['state/header'] = false; // ensure start with header off
		$newTokenCount = 0;
		$endTokens = array();
		//------------------
		for ($i = 0;$i < sizeof($headerTokens);$i++)
			$headerTokens[$i]['header'] = true;
		//------------------------------------
		foreach ($inTokens as $token)
			if ($token['header'] && ($token['type'] != 'func' || ($token['name'] != 'START_HEADER' && $token['name'] != 'END_HEADER' && $token['name'] != 'END_HEADER_INLINE')))
				$headerTokens[] = $token;
		//------------------------------------
		$outTokens = XArrayConcat($extraTokens, $outTokens); // extraTokens take precedence
		//------------------------------------
		foreach ($outTokens as $token)
			if ($token['header'] && $token['type'] == 'func' && $token['name'] == 'RENAME_IN')
			{
				if (sizeof($token['args']) != 1 || $token['value'] === false)
					return array('ok' => false, 'fail' => 'Validate header function', 'errorMsg' => 'disassembleContentTemplate outToken header function RENAME_IN failed to validate args and value', 'token' => $token);
				$oldName = $token['args'][0];
				$newName = $token['value'];
				$renameCount= 0;
				for ($i = 0;$i < sizeof($inTokens);$i++)
					if ($inTokens[$i]['name'] == $oldName)
					{
						$inTokens[$i]['name'] = $newName;
						$renameCount++;
					}
				echo "renamed $renameCount '$oldName' -> '$newName'\n";
			}
			else
			{
				if (isset($token['notes']))
				{
					if (strpos($token['notes'], '@rename:') !== false)
					{
						$spos = strpos($token['notes'], '@rename:');
						$spos += 8;
						$epos = strpos($token['notes'], '@', $spos);
						if ($epos !== false)
							$token['name'] = substr($token['notes'], $spos, ($epos - $spos));
					}
					if (strpos($token['notes'], '@not-header@') !== false)
						$token['header'] = false;
					if (strpos($token['notes'], '@insert@') !== false)
						$inTokens[] = $token;
					if (strpos($token['notes'], '@insert-top@') !== false)
						$inTokens = XArrayConcat(array($token), $inTokens);
					if (strpos($token['notes'], '@insert-top-body@') !== false)
					{
						$newInTokens = array();
						$i = 0;
						while ($i < sizeof($inTokens))
							if ($inTokens[$i]['header'])
								$newInTokens[] = $inTokens[$i++];
							else
								break;
						$newInTokens[] = $token;
						while ($i < sizeof($inTokens))
							$newInTokens[] = $inTokens[$i++];
						$inTokens = $newInTokens;
					}
				} // if (isset($token['notes']))
				if ($token['header'] && ($token['type'] != 'func' || ($token['name'] != 'START_HEADER' && $token['name'] != 'END_HEADER' && $token['name'] != 'END_HEADER_INLINE')))
					$headerTokens[] = $token;
			}
		//------------------------------------
		$resultTokens = XAssArrayClone($headerTokens);
		//------------------------------------
		$fullHeadOutTokens = XAssArrayClone($headerTokens);
		foreach ($outTokens as $token)
			if (!$token['header'])
				$fullHeadOutTokens[] = $token;
		$outTokens = $fullHeadOutTokens;
		//------------------------------------
		if (!XArray($session, 'state/HEADERS_LOADED', false) && !XArray($session, 'state/HEADERS_LOADING', false))
		{
			//------------------------------------
			$session['state/HEADERS_LOADING'] = true;
			//------------------------------------
			for ($i = 0;$i < sizeof($resultTokens);$i++)
				if ($resultTokens[$i]['header'] && ($resultTokens[$i]['type'] != 'func' || ($resultTokens[$i]['name'] != 'START_HEADER' && $resultTokens[$i]['name'] != 'END_HEADER' && $resultTokens[$i]['name'] != 'END_HEADER_INLINE')))
				{
					$effectiveTokens = XArrayRemove($resultTokens, $i);
					$result = $this->handleToken($resultTokens[$i], $session, 'disassembleContentTemplate token headers', $effectiveTokens, 10 /*depthMax*/, true /*nonActive*/);
					if (!$result['ok'])
					{
						if (!isset($result['parentError']))
							$result['parentError'] = '';
						$result['parentError'] .= ', disassembleContentTemplate resultTokens header handleToken failed';
						return $result;
					}
					else if ($result['handled'])
					{
						if ($result['sessionModified'])
							$session = $result['session'];
						$token = $resultTokens[$i];
						if ($result['output'] !== false && isset($token['notes']) && strpos($token['notes'], '@assemble@') !== false)
						{
							echo "token '".$token['name']."': ".XVarDump($result['output'])."\n";
							$token['value'] = $result['output']; // should only be header functions

							$assembleResult = $this->assembleText($session, $result['output'], $effectiveTokens, $source = 'disassembleContentTemplate(@assemble@ header)');
							if (!$assembleResult['ok'])
							{
								if (!isset($assembleResult['parentError']))
									$assembleResult['parentError'] = '';
								$assembleResult['parentError'] .= ', disassembleContentTemplate(@assemble@ header) assembleText failed';
								$assembleResult['parentErrorToken'] = $token;
								return $assembleResult;
							}
							//------------------
							echo "Assembled header token '".$token['name']."\nvalue:\n".XVarDump($token['value'])."\nassembleResult:\n".XVarDump($assembleResult['outText'])."\n";
							$token['value'] = $assembleResult['outText'];
							$token['notes'] = str_replace(array('@assemble@', '@assemble-in@', '@assemble-out@', '@assemble-results@'), array('@assembled@', '@assembled-in@', '@assembled-out@', '@assembled-results@'), $token['notes']);
							$resultTokens[$i] = $token;
							//------------------
							$templateResult = $this->parseTemplateText($token['value'], $session, $effectiveTokens);
							if (!$templateResult['ok'])
							{
								if (!isset($templateResult['parentError']))
									$templateResult['parentError'] = '';
								$templateResult['parentError'] .= ', disassembleContentTemplate parseTemplateText header failed';
								echo "parseTemplateText header failed:\n\token: ".XVarDump($token)."\nresult:".XVarDump($templateResult)."\n";//"\toutToken: ".XVarDump($outToken)."\t\noutTokens:\n".XVarDump($outTokens, false)."\n";
								//return array('ok' => false, 'fail' => 'token missing', 'errorMsg' => 'Token missing', 'token' => $inToken);
								$templateResult['parentErrorToken'] = $token;
								return $templateResult;
							}
							//------------------
							if (sizeof($templateResult['tokens']) > 0)
							{
								//------------------
								$dasmResult = $this->disassemble($session, $token['value'], $templateResult['tokens'], $templateResult['template']);
								if (!$dasmResult['ok'])
								{
									if (!isset($dasmResult['parentError']))
										$dasmResult['parentError'] = '';
									$dasmResult['parentError'] .= ', disassembleContentTemplate disassemble recurs header failed';
									$dasmResult['parentErrorToken'] = $token;
									return $dasmResult;
								}
								//------------------
								foreach ($dasmResult['newTokens'] as $subToken)
								{
									$subToken['line'] = $token['line'];
									$resultTokens[] = $subToken; // @PRIORITY
									$newTokenCount++;
								}
								//------------------
							}
							
						}
					}						
				}
			//------------------------------------
			$session['state/HEADERS_LOADING'] = false;
			$session['state/HEADERS_LOADED'] = true;
			//------------------------------------
		} // not state/HEADERS_LOADED or state/HEADERS_LOADING
		//------------------
		foreach ($inTokens as $inToken)
		{
			//------------------
			if (!isset($inToken['header']) || !$inToken['header']) // inToken headers were already added
			{
				//------------------
				$outToken = $this->findFirstTokenByName($outTokens, $inToken['name'], ($inToken['index'] !== false && $inToken['index'] !== '' ? $inToken['index'] : null));
				//------------------
				if ($outToken === false)
				{
					$inToken['notes'] .= '@missing@ ';
					echo "Token missing:\n\tinToken: ".XVarDump($inToken)."\n";//\toutTokens:\n".XVarDump($outTokens, false)."\n";
					return array('ok' => false, 'fail' => 'token missing', 'errorMsg' => 'disassembleContentTemplate Token missing', 'token' => $inToken);
					$outToken = $inToken;
				}
				//------------------
				if (!isset($inToken['notes']) || !isset($outToken['notes']))
					return array('ok' => false, 'fail' => 'verify fields', 'errorMsg' => 'in or out token missing notes field', 'tokenName' => $inToken['name']);
				//------------------
				if (strpos($outToken['notes'], '@rename:') !== false)
				{
					$spos = strpos($outToken['notes'], '@rename:');
					$spos += 8;
					$epos = strpos($outToken['notes'], '@', $spos);
					if ($epos !== false)
						$inToken['name'] = substr($outToken['notes'], $spos, ($epos - $spos));
				}
				//------------------
				if (strpos($outToken['notes'], '@assemble-in@') !== false || strpos($outToken['notes'], '@assemble-out@') !== false || strpos($outToken['notes'], '@assemble-results@') !== false)
				{
					//------------------
					$pIn = strpos($outToken['notes'], '@assemble-in@');
					$pOut = strpos($outToken['notes'], '@assemble-out@');
					$pResults = strpos($outToken['notes'], '@assemble-results@');
					//------------------
					$order = array();
					if ($pIn !== false)
						$order[$pIn] = 'in';
					if ($pOut !== false)
						$order[$pOut] = 'out';
					if ($pResults !== false)
						$order[$pResults] = 'results';
					//------------------
					$effectiveTokens = array();
					ksort($order);
					foreach ($order as $o)
						if ($o == 'in')
							$effectiveTokens = XArrayConcat($effectiveTokens, $inTokens);
						else if ($o == 'out')
							$effectiveTokens = XArrayConcat($effectiveTokens, $outTokens);
						else if ($o == 'results')
						{
							$effectiveTokens = XArrayConcat($effectiveTokens, $resultTokens);
							$effectiveTokens = XArrayConcat($effectiveTokens, $endTokens);
						}
					//------------------
					$result = $this->assembleText($session, $outToken['value'], $effectiveTokens, $source = 'disassembleContentTemplate(@assemble@)');
					//$result = $this->assembleToken($session, $outToken, $effectiveTokens, 'disassembleContentTemplate(@assemble@)' /*source*/);
					if (!$result['ok'])
					{
						if (!isset($result['parentError']))
							$result['parentError'] = '';
						$result['parentError'] .= ', disassembleContentTemplate(@assemble@) assembleText failed';
						$result['parentErrorToken'] = $outToken;
						return $result;
					}
					//------------------
					echo "Assembled token '".$outToken['name']."' order: ".XVarDump($order).", \nvalue:\n".XVarDump($outToken['value'])."\nresult:\n".XVarDump($result['outText'])."\n";
					$outToken['value'] = $result['outText'];
					$outToken['notes'] = str_replace(array('@assemble-in@', '@assemble-out@', '@assemble-results@'), array('@assembled-in@', '@assembled-out@', '@assembled-results@'), $outToken['notes']);
					$inToken = $outToken;
					//------------------
				}
				//------------------
				if ($outToken['value'] === false || $outToken['value'] == '')
					$resultTokens[] = $inToken; // @PRIORITY
				else
				{
					//------------------
					if ($inToken['notes'] != $outToken['notes'])
						$inToken['notes'] .= $outToken['notes'];
					//------------------
					$effectiveTokens = XArrayConcat($resultTokens, $endTokens);
					$effectiveTokens = XArrayConcat($effectiveTokens, $outTokens);
					//------------------
					$templateResult = $this->parseTemplateText($outToken['value'], $session, $effectiveTokens);
					if (!$templateResult['ok'])
					{
						if (!isset($templateResult['parentError']))
							$templateResult['parentError'] = '';
						$templateResult['parentError'] .= ', disassembleContentTemplate parseTemplateText failed';
						echo "parseTemplateText failed:\n\tinToken: ".XVarDump($inToken)."\nresult:".XVarDump($templateResult)."\n";//"\toutToken: ".XVarDump($outToken)."\t\noutTokens:\n".XVarDump($outTokens, false)."\n";
						//return array('ok' => false, 'fail' => 'token missing', 'errorMsg' => 'Token missing', 'token' => $inToken);
						$templateResult['parentErrorToken'] = $inToken;
						return $templateResult;
					}
					//------------------
					if (sizeof($templateResult['tokens']) > 0)
					{
						//------------------
						$result = $this->disassemble($session, $inToken['value'], $templateResult['tokens'], $templateResult['template']);//, array() /*headerTokens*/, $effectiveTokens);
						if (!$result['ok'])
						{
							if (!isset($result['parentError']))
								$result['parentError'] = '';
							$result['parentError'] .= ', disassembleContentTemplate disassemble recurs failed';
							$result['parentErrorToken'] = $inToken;
							return $result;
						}
						//------------------
						//if ($inToken['name'] == "LIVE_DONATE")
						//	echo "LIVE_DONATE: dsm result: ".XVarDump($result)."\ntemplate tokens:\n".XVarDump($templateResult['tokens'], false)."\n";
						foreach ($result['newTokens'] as $subToken)
							if ($subToken['type'] !== 'func')
							{
								$subToken['line'] = $inToken['line'];
								$resultTokens[] = $subToken; // @PRIORITY
								$newTokenCount++;
							}
						//------------------
						$inToken['value'] = $templateResult['template_raw'];
						$endTokens[] = $inToken;
						//------------------
					}
					else if (strpos($inToken['notes'], '@end@') !== false || strpos($inToken['notes'], '@default@') !== false) // marked to put at end, despite no replacements. May be a default
						$endTokens[] = $inToken; // @PRIORITY
					else
						$resultTokens[] = $inToken; // @PRIORITY
					//------------------
				}
				//------------------
			}
			//------------------
		} // foreach inToken
		//------------------
		$resultTokens = XArrayConcat($resultTokens, $endTokens);
		//------------------
		return array('ok' => true, 'fail' => false, 'newTokenCount' => $newTokenCount, 'recTokenCount' => sizeof($endTokens), 'tokens' => $resultTokens, 'session' => $session);
	}
	//------------------
	function getTokenFullName($token, $session = false, $includeArguments = false)
	{
		//------------------
		if ($session === false)
			$session = $this->createSession();
		//------------------
		return $this->buildContentTokenLine($session, $token, false /*wantNotes*/, (!$includeArguments) /*skipArguments*/, true /*skipValue*/, '' /*lineDel*/);
	}
	//------------------
	function buildContentTokenLine($session, $token, $wantNotes = true, $skipArguments = false, $skipValue = false, $lineDel = "\n")
	{
		$out = '';
		//------------------
		if ($wantNotes && isset($token['notes']) && $token['notes'] != '')
			$out .= '# '.$token['notes'].$lineDel;
		//------------------
		if (isset($token['type']) && $token['type'] == 'func')
			$out .= $session['var/ESC_FUNC'];
		else if (isset($token['type']) && $token['type'] == 'var')
			$out .= $session['var/ESC_VAR'];
		//------------------
		$out .= $token['name'];
		//------------------
		if (isset($token['index']) && $token['index'] != '')
			$out .= '['.$this->escapeEncode($token['index'], true /*full*/).']';
		//------------------
		if (!$skipArguments && isset($token['args']) && is_array($token['args']) && sizeof($token['args']) != 0)
		{
			$quotedArgs = array();
			foreach ($token['args'] as $arg)
				$quotedArgs[] = '"'.$this->escapeEncode($arg, true /*full*/).'"';
			$out .= '('.implode(',', $quotedArgs).')';
		}
		//------------------
		if (!$skipValue && $token['value'] !== false)
		{
			//------------------
			$value = $token['value'];
			//------------------
			$out .= '="'.$this->escapeEncode($value, false /*full*/).'"';
			//------------------
		}
		//------------------
		$out .= $lineDel;
		//------------------
		return $out;
	}
	//------------------
	function buildContentTextFromTokens($session, $tokens, $commentHeader = false, $wantNotes = true, $skipArguments = false, $skipValue = false, $lineDel = "\n")
	{
		//------------------
		$out = '';
		$outHeader = '';
		$outBody = '';
		//------------------
		if ($commentHeader !== false)
		{
			$lines = explode($lineDel, $commentHeader);
			foreach ($lines as $line)
				$out .= "# $line$lineDel";
		}
		//------------------
		foreach ($tokens as $token)
		{
			$outLine = $this->buildContentTokenLine($session, $token, $wantNotes, $skipArguments, $skipValue, $lineDel);
			if (isset($token['header']) && $token['header'])
				$outHeader .= $outLine;
			else
				$outBody .= $outLine;
		}
		//------------------
		if ($outHeader != '')
		{
			$out .= $session['var/ESC_FUNC']."START_HEADER$lineDel";
			$out .= $outHeader;
			$out .= $session['var/ESC_FUNC']."END_HEADER$lineDel";
		}
		//------------------
		$out .= $outBody;
		//------------------
		return $out;
	}
	//------------------
	function listFilesByExtention($directory, $maxDepth = 50, $extentionMask = 'cont')
	{
		//------------------------------------
		$fileList = array();
		//------------------------------------
		$fullFileList = XScanDirectory($directory, false /*includeDirs*/, $maxDepth);
		//------------------------------------
		$extentionMask = strtr(preg_quote($extentionMask, '?'), array('\*' =>'.*', '\?' => '.', '\|' => '|'));
		//------------------------------------
		foreach ($fullFileList as $fileName)
			if (preg_match("/^$extentionMask$/i", pathinfo($fileName, PATHINFO_EXTENSION)))
				$fileList[] = $fileName;
		//------------------------------------
		return $fileList;
	}
	//------------------
	function loadTemplatesByDirectory($directory, $onlyHeader = false, $maxDepth = 50, $extentionMask = 'tp|tpcont')
	{
		//------------------------------------
		$templates = array();
		$directory = XEnsureEndSlash($directory); // for getting relFileName
		//------------------------------------
		$templateFiles = $this->listFilesByExtention($directory, $maxDepth, $extentionMask);
		//------------------------------------
		foreach ($templateFiles as $fileName)
		{
			$session = $this->createSession(array('state/current_read_file_full' => $fileName,
												  'state/current_read_file' => substr($fileName, strlen($directory)),
												  'state/only_header' => $onlyHeader)  /*mergeSession*/);
			$templates[] = $this->readTemplateFile($session);
		}
		//------------------------------------
		return $templates;
	}
	//------------------
	function loadContentsByDirectory($directory, $onlyHeader = false, $maxDepth = 50, $extentionMask = 'cont?|tpcont')
	{
		//------------------------------------
		$contents = array();
		$directory = XEnsureEndSlash($directory); // for getting relFileName
		//------------------------------------
		$contentFiles = $this->listFilesByExtention($directory, $maxDepth, $extentionMask);
		//------------------------------------
		foreach ($contentFiles as $fileName)
		{
			$session = $this->createSession(array('state/current_read_file_full' => $fileName,
												  'state/current_read_file' => substr($fileName, strlen($directory)),
												  'state/only_header' => $onlyHeader)  /*mergeSession*/);
			$contents[] = $this->readContentFile($session);
		}
		//------------------------------------
		return $contents;
	}
	//------------------
	function assembleFiles($templateFileName, $templateRelFileName, $contentFileName, $contentRelFileName, $outputFileName, $jsonTextSessionMerge = '')
	{
		//------------------------------------
		$mergeSession = false;
		//------------------------------------
		if ($jsonTextSessionMerge != '')
		{
			$mergeSession = json_decode($jsonTextSessionMerge, true);
			if ($mergeSession === null)
				return array('ok' => false, 'error' => 'Session merge', 'errorMsg' => 'assembleFiles json decode session failed');
		}
		//------------------------------------
		$templateResult = $this->readTemplateFile($this->createSession(array(	'state/current_read_file_full' => $templateFileName,
																				'state/current_read_file' => $templateRelFileName,
																				'state/only_header' => false)));
		//------------------------------------
		if (!$templateResult['ok'])
		{
			$templateResult['parentError'] = 'assembleFiles readTemplateFile failed';
			return $templateResult;			
		}
		//------------------------------------
		$contentResult = $this->readContentFile($this->createSession( array(	'state/current_read_file_full' => $contentFileName,
																				'state/current_read_file' => $contentRelFileName,
																				'state/only_header' => false)));
		//------------------------------------
		if (!$contentResult['ok'])
		{
			$contentResult['parentError'] = 'assembleFiles readContentFile failed';
			return $contentResult;			
		}
		//------------------------------------
		$assembleResult = $this->assemble($this->createSession($mergeSession), $templateResult['tokens'], $templateResult['template'], $contentResult['defines']);
		if (!$assembleResult['ok'])
		{
			$assembleResult['parentError'] = 'assembleFiles assemble failed';
			return $assembleResult;			
		}
		//------------------------------------
		if (false === @file_put_contents($outputFileName, $assembleResult['outText']))
			return array('ok' => false, 'error' => 'Output file', 'errorMsg' => 'assembleFiles Write assembled output file failed', 'outputFileName' => $outputFileName);
		//------------------------------------
		$assembleResult['outputFileName'] = $outputFileName;
		//------------------------------------
		return $assembleResult;
	}
	//------------------
	function disassembleFiles($inputFileName, $templateFileName, $templateRelFileName, $outputFileName, $textHeaderTokens = '', $textExtraTokens = '', $jsonTextSessionMerge = '')
	{
		//------------------------------------
		$mergeSession = false;
		$headerTokens = array();
		$extraTokens = array();
		//------------------------------------
		if ($jsonTextSessionMerge != '')
		{
			$mergeSession = json_decode($jsonTextSessionMerge, true);
			if ($mergeSession === null)
				return array('ok' => false, 'error' => 'Session merge', 'errorMsg' => 'disassembleFiles json decode session failed');
		}
		//------------------------------------
		$inputText = @file_get_contents($inputFileName);
		if ($inputText === false)
			return array('ok' => false, 'error' => 'Input file', 'errorMsg' => 'disassembleFiles Read input file failed', 'inputFileName' => $inputFileName);
		//------------------------------------
		$templateResult = $this->readTemplateFile($this->createSession(array(	'state/current_read_file_full' => $templateFileName,
																				'state/current_read_file' => $templateRelFileName,
																				'state/only_header' => false)));
		//------------------------------------
		if (!$templateResult['ok'])
		{
			$templateResult['parentError'] = 'disassembleFiles readTemplateFile failed';
			return $templateResult;			
		}
		//------------------------------------
		if ($textHeaderTokens != '')
		{
			//------------------------------------
			$contentResult = $this->parseContentText($textHeaderTokens, $this->createSession());
			//------------------------------------
			if (!$contentResult['ok'])
			{
				$contentResult['parentError'] = 'disassembleFiles parseContentText textHeaderTokens failed';
				return $contentResult;			
			}
			//------------------------------------
			foreach ($contentResult['defines'] as $token)
			{
				$token['header'] = true;
				$headerTokens[] = $token;
			}
			//------------------------------------
		}
		//------------------------------------
		if ($textExtraTokens != '')
		{
			//------------------------------------
			$contentResult = $this->parseContentText($textExtraTokens, $this->createSession());
			//------------------------------------
			if (!$contentResult['ok'])
			{
				$contentResult['parentError'] = 'disassembleFiles parseContentText textExtraTokens failed';
				return $contentResult;			
			}
			//------------------------------------
			$headerTokens = $contentResult['defines'];
			//------------------------------------
		}
		//------------------------------------
		$disassembleResult = $this->disassemble($this->createSession($mergeSession), $inputText, $templateResult['tokens'], $templateResult['template'], $headerTokens, $extraTokens);
		if (!$disassembleResult['ok'])
		{
			$disassembleResult['parentError'] = 'disassembleFiles disassemble failed';
			return $disassembleResult;			
		}
		//------------------------------------
		$outputText = $this->buildContentTextFromTokens($this->createSession($mergeSession), $disassembleResult['tokens']);
		//------------------------------------
		if (false === @file_put_contents($outputFileName, $outputText))
			return array('ok' => false, 'error' => 'Output file', 'errorMsg' => 'disassembleFiles Write disassembled output content file failed', 'outputFileName' => $outputFileName);
		//------------------------------------
		$disassembleResult['outputFileName'] = $outputFileName;
		//------------------------------------
		return $disassembleResult;
	}
	//------------------
	function disassembleContentFiles($inputContentFileName, $inputContentRelFileName, $templateContentFileName, $templateContentRelFileName, $outputFileName, $textHeaderTokens = '', $textExtraTokens = '', $jsonTextSessionMerge = '')
	{
		//------------------------------------
		$mergeSession = false;
		$headerTokens = array();
		$extraTokens = array();
		//------------------------------------
		if ($jsonTextSessionMerge != '')
		{
			$mergeSession = json_decode($jsonTextSessionMerge, true);
			if ($mergeSession === null)
				return array('ok' => false, 'error' => 'Session merge', 'errorMsg' => 'disassembleContentFiles json decode session failed');
		}
		//------------------------------------
		$inputText = @file_get_contents($inputFileName);
		if ($inputText === false)
			return array('ok' => false, 'error' => 'Input file', 'errorMsg' => 'disassembleContentFiles Read input file failed', 'inputFileName' => $inputFileName);
		//------------------------------------
		$inContentResult = $this->readContentFile($this->createSession(array(	'state/current_read_file_full' => $inputContentFileName,
																				'state/current_read_file' => $inputContentRelFileName,
																				'state/only_header' => false)));
		//------------------------------------
		if (!$inContentResult['ok'])
		{
			$inContentResult['parentError'] = 'disassembleContentFiles readContentFile input failed';
			return $inContentResult;			
		}
		//------------------------------------
		$outContentResult = $this->readContentFile($this->createSession(array(	'state/current_read_file_full' => $templateContentFileName,
																				'state/current_read_file' => $templateContentRelFileName,
																				'state/only_header' => false)));
		//------------------------------------
		if (!$outContentResult['ok'])
		{
			$outContentResult['parentError'] = 'disassembleContentFiles readContentFile template failed';
			return $outContentResult;			
		}
		//------------------------------------
		if ($textHeaderTokens != '')
		{
			//------------------------------------
			$contentResult = $this->parseContentText($textHeaderTokens, $this->createSession());
			//------------------------------------
			if (!$contentResult['ok'])
			{
				$contentResult['parentError'] = 'disassembleContentFiles parseContentText textHeaderTokens failed';
				return $contentResult;			
			}
			//------------------------------------
			foreach ($contentResult['defines'] as $token)
			{
				$token['header'] = true;
				$headerTokens[] = $token;
			}
			//------------------------------------
		}
		//------------------------------------
		if ($textExtraTokens != '')
		{
			//------------------------------------
			$contentResult = $this->parseContentText($textExtraTokens, $this->createSession());
			//------------------------------------
			if (!$contentResult['ok'])
			{
				$contentResult['parentError'] = 'disassembleContentFiles parseContentText textExtraTokens failed';
				return $contentResult;			
			}
			//------------------------------------
			$headerTokens = $contentResult['defines'];
			//------------------------------------
		}
		//------------------------------------
		$disassembleContentResult = $this->disassembleContentTemplate($this->createSession($mergeSession), $inContentResult['defines'], $outContentResult['defines'], $headerTokens, $extraTokens);
		if (!$disassembleContentResult['ok'])
		{
			$disassembleContentResult['parentError'] = 'disassembleContentFiles disassemble failed';
			return $disassembleContentResult;			
		}
		//------------------------------------
		$outputText = $this->buildContentTextFromTokens($this->createSession($mergeSession), $disassembleContentResult['tokens']);
		//------------------------------------
		if (false === @file_put_contents($outputFileName, $outputText))
			return array('ok' => false, 'error' => 'Output file', 'errorMsg' => 'disassembleContentFiles Write disassembled content output content file failed', 'outputFileName' => $outputFileName);
		//------------------------------------
		$disassembleContentResult['outputFileName'] = $outputFileName;
		//------------------------------------
		return $disassembleContentResult;
	}
	//------------------
} // class TemplateAssembler
//---------------
?>
