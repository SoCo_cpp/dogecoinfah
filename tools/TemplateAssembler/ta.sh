#!/bin/bash

LOGFILE="ta.log"
RESULT=1

#-----------------
ta_dasm()
{
	#echo "ta_dasm($@)"
	RESULT=0
	[ "$#" -ne 3 ] && echo "Usage: ta_dasm fnT fnI fnO" && return 0
	echo -e "\e[1;34mphp ta.phpsh -a d -t ${1} -i ${2} -o ${3} -w \e[0m"
	php ta.phpsh -a d -t ${1} -i ${2} -o ${3} -w || return 0
	RESULT=1
	return 1
}
#-----------------
# ta_cdasm(fnT, fnI, fnO, [$fnH = 0])
ta_cdasm()
{
	#echo "ta_cdasm($@)"
	RESULT=0
	[ "$#" -ne 3 ] && [ "$#" -ne 4 ] && echo "Usage: ta_cdasm fnT fnI fnO [fnH = 0]" && return 0
	local fnH=''
	[ "$#" -eq 4 ] && [ "$4" != "0" ] && fnH=" -h $4 "
	echo -e "\e[1;34mphp ta.phpsh -a cd -t ${1} -i ${2} -o ${3} ${fnH} -w \e[0m"
	php ta.phpsh -a cd -t ${1} -i ${2} -o ${3} ${fnH} -w || return 0
	RESULT=1
	return 1
}
#-----------------
# ta_asm(fnT, fnI, fnO, dirReleaseOutput)
ta_asm()
{
	#echo "ta_asm($@)"
	RESULT=0
	[ "$#" -ne 4 ] && echo "Usage: ta_asm fnT fnI fnO dirReleaseOutput" && return 0
	if [ "$4" == "0" ]; then
		echo -e "\e[1;34mphp ta.phpsh -a a -t ${1} -i ${2} -o ${3} -w \e[0m"
		php ta.phpsh -a a -t ${1} -i ${2} -o ${3} -w || return 0
	else
		echo -e "\e[1;96mphp ta.phpsh -a a -t ${1} -i ${2} -o ${3} -r ${4} -w \e[0m"
		php ta.phpsh -a a -t ${1} -i ${2} -o ${3} -r ${4} -w || return 0
	fi
	RESULT=1
	return 1
}
#-----------------

# ta_asm_asset_set(assetName, lang, inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0])
ta_asm_asset_set()
{
	echo -e "\e[1;35mta_asm_asset_set($@)\e[0m"
	RESULT=0
	[ "$#" -ne 5 ] && [ "$#" -ne 6 ] && [ "$#" -ne 7 ] && [ "$#" -ne 8 ] && [ "$#" -ne 9 ] && echo "Usage: ta_asm_asset_set assetName, lang, inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0]" && return 0
	local assetName=${1}
	local lang=${2}
	local inPageVer=${3}
	local outPageVer=${4}
	local inTpVer=${5}
	local outTpVer=${inTpVer}
	[ "$#" -gt 5 ] && outTpVer=${6}
	local fnIncludeHeader=0
	[ "$#" -gt 6 ] && [ "$7" != "0" ] && fnIncludeHeader=${7}
	local dirReleaseOutput=0
	[ "$#" -gt 7 ] && [ "$8" != "0" ] && dirReleaseOutput=${8}
	local tpConMultiIn=0
	[ "$#" -gt 8 ] && [ "$9" -ne 0 ] && tpConMultiIn=1

	local inVer="p${inPageVer}_t${inTpVer}"
	local endVer="p${outPageVer}_t${outTpVer}"
	local outVer=${endVer}
	[ "$inVer" != "$endVer" ] && outVer="${inVer}_to_${endVer}"
	local fnTin="${assetName}_${inVer}.tp"
	local fnTout="${assetName}_${endVer}.tp"
	local fnCT="${assetName}_${outVer}.tpcon"
	[ ${tpConMultiIn} -eq 1 ] && fnCT="${assetName}_${endVer}.tpcon"
	local fnI="${assetName}_p${inPageVer}_${lang}.html"
	local fnO="output/${assetName}_${outVer}_${lang}.html"
	local fnC1="content/${assetName}_${inVer}_${lang}.cont1"
	local fnC2="content/${assetName}_${outVer}_${lang}.cont2"

	RESULT=0
	ta_dasm ${fnTin} ${fnI} ${fnC1}
	[ "$RESULT" -ne 1 ] && echo "$0 ta_dasm failed: $?" && return 0
	RESULT=0
	ta_cdasm ${fnCT} ${fnC1} ${fnC2} ${fnIncludeHeader}
	[ "$RESULT" -ne 1 ] && echo "$0 ta_cdasm failed: $?" && return 0
	RESULT=0
	ta_asm ${fnTout} ${fnC2} ${fnO} ${dirReleaseOutput}
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm failed: $?" && return 0
	echo -e "\n"
	RESULT=1
	return 1
}

# ta_asm_asset(assetName, inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0])
ta_asm_asset()
{
	echo -e "\e[1;35mta_asm_asset($@) languages: ${languages[@]}\e[0m"
	RESULT=0
	[ "$#" -ne 4 ] && [ "$#" -ne 5 ] && [ "$#" -ne 6 ] && [ "$#" -ne 7 ] && [ "$#" -ne 8 ]  && echo "Usage: ta_asm_asset assetName,  inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0]" && return 0
	local assetName=${1}
	local inPageVer=${2}
	local outPageVer=${3}
	local inTpVer=${4}
	local outTpVer=${inTpVer}
	[ "$#" -gt 4 ] && outTpVer=${5}
	local fnIncludeHeader=0
	[ "$#" -gt 5 ] && [ "$6" != "0" ] && fnIncludeHeader=${6}
	local dirReleaseOutput=0
	[ "$#" -gt 6 ] && [ "$7" != "0" ] && dirReleaseOutput=${7}
	local tpConMultiIn=0
	[ "$#" -gt 7 ] && [ "$8" -ne 0 ] && tpConMultiIn=1


	RESULT=0
	ta_asm_asset_set ${assetName} ${baselanguage} ${inPageVer} ${outPageVer} ${inTpVer} ${outTpVer} ${fnIncludeHeader} 0 ${tpConMultiIn}
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_asset_set failed: $?" && return 0
	RESULT=0
	ta_asm_asset_set ${assetName} ${baselanguage} ${outPageVer} ${outPageVer} ${outTpVer} ${outTpVer} ${fnIncludeHeader} ${dirReleaseOutput} ${tpConMultiIn}
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_asset_set failed: $?" && return 0
	for LANG in ${languages[@]}; do
		RESULT=0
		ta_asm_asset_set ${assetName} ${LANG} ${inPageVer} ${outPageVer} ${inTpVer} ${outTpVer} ${fnIncludeHeader} ${dirReleaseOutput} ${tpConMultiIn}
		[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_asset_set failed: $?" && return 0
	done
	RESULT=1
	return 1
}

# ta_asm_assets(inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0])
ta_asm_assets()
{
	echo -e "\e[1;35mta_asm_assets($@) languages: ${languages[@]}\e[0m"
	RESULT=0
	[ "$#" -ne 3 ] && [ "$#" -ne 4 ] && [ "$#" -ne 5 ] && [ "$#" -ne 6 ] && [ "$#" -ne 7 ]  && echo "Usage: ta_asm_assets inPageVer, outPageVer, inTpVer, [outTpVer], [fnIncludeHeader = 0], [dirReleaseOutput = 0], [tpConMultiIn = 0]" && return 0
	local inPageVer=${1}
	local outPageVer=${2}
	local inTpVer=${3}
	local outTpVer=${inTpVer}
	[ "$#" -gt 3 ] && outTpVer=${4}
	local fnIncludeHeader=0
	[ "$#" -gt 4 ] && [ "$5" != "0" ] && fnIncludeHeader=${5}
	local dirReleaseOutput=0
	[ "$#" -gt 5 ] && [ "$6" != "0" ] && dirReleaseOutput=${6}
	local tpConMultiIn=0
	[ "$#" -gt 6 ] && [ "$7" -ne 0 ] && tpConMultiIn=1

	for LANG in ${languages[@]}; do
		for ASSET in ${assets[@]}; do
			RESULT=0
			ta_asm_asset_set ${ASSET} ${LANG} ${inPageVer} ${outPageVer} ${inTpVer} ${outTpVer} ${fnIncludeHeader} ${dirReleaseOutput} ${tpConMultiIn}
			[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_asset_set failed: $?" && return 0
		done
	done
	RESULT=1
	return 1
}

ta_asm_disasm()
{
	echo -e "\n\e[1mta_asm_disasm($@)\e[0m\n"
	[ "$#" -ne 2 ] && echo "Usage: ta_asm_disasm [language(s)] [assets step 0:main, 1:web, :2 doc 3: all]" && return 0
	RESULT=0
	local HEADER="header.tpcon"
	local baselanguage="en"
	local tpVer="t1"
	local inPageVer="p0"
	local languages=${2}
	local assetsStep=${1}
	local webAssets=("main" "sponsors" "history" "historychart")
	local docAssets=("doc/getting-started" "doc/faq" "doc/install-win" "doc/install-nix")
	local assets=()
	case ${assetsStep} in
		0) assets=("main") ;;
		1) assets=webAssets ;;
		2) assets=docAssets ;;
		3) assets=( "${webAssets[@]} ${docAssets[@]}" ) ;;
		*) echo "invalid ta_asm_disasm asset step '${assetsStep}'" ;;
	esac
	for LANG in ${languages[@]}; do
		for ASSET in ${assets[@]}; do
			RESULT=0
			local fnTin="${ASSET}_${inPageVer}_${tpVer}.tp"
			local fnI="${ASSET}_${inPageVer}_${LANG}.html"
			local fnC1="content/${ASSET}_${inPageVer}_${tpVer}_${LANG}.cont1"
			RESULT=0
			ta_dasm ${fnTin} ${fnI} ${fnC1}
			[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_disasm failed: $?" && return 0
		done
	done

	for LANG in ${languages[@]}; do
		for ASSET in ${assets[@]}; do
			RESULT=0
			local fnTin="${ASSET}_${inPageVer}_${tpVer}.tp"
			local fnI="${ASSET}_${inPageVer}_${LANG}.html"
			local fnC1="content/${ASSET}_${inPageVer}_${tpVer}_${LANG}.cont1"
			RESULT=0
#			[ "${LANG}" -eq "en" ] && fnTin="${ASSET}_${inPageVer}_${tpVer}.tp"
			ta_cdasm ${fnCT} ${fnC1} ${fnC2} ${fnIncludeHeader}
			[ "$RESULT" -ne 1 ] && echo "$0 ta_cdasm failed: $?" && return 0


#	local inVer="p${inPageVer}_t${inTpVer}"
#	local endVer="p${outPageVer}_t${outTpVer}"
#	local outVer=${endVer}

#	local fnCT="${assetName}_${outVer}.tpcon"

#	local fnC1="content/${assetName}_${inVer}_${lang}.cont1"
#	local fnC2="content/${assetName}_${outVer}_${lang}.cont2"

		done
	done
	
	echo -e "\n\e[1mta_asm_disasm done\e[0m"
	RESULT=1
	return 1
}

ta_asm_main()
{
	echo -e "\n\e[1mta_asm_main($@)\e[0m\n"
	RESULT=0
	local HEADER="header.tpcon"
	local dirReleaseOutput="test8_apr_10_2022"
	local baselanguage="en"
	local languages=("es")
	local assets=("doc/getting-started" "doc/faq" "doc/install-win" "doc/install-nix")


	# Docs
	assets=("doc/getting-started" "doc/faq" "doc/install-win" "doc/install-nix")


	# Docs 0_1 en (no release)
	RESULT=0
	languages=("en")
	ta_asm_assets 0 1 1 1 ${HEADER} 0 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Docs 0_1 es/ru/pt
	RESULT=0
	languages=("es" "ru" "pt")
	ta_asm_assets 0 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Docs 1_1 en/de/fr
	RESULT=0
	languages=("en" "de" "fr")
	ta_asm_assets 1 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0


	# Main
	assets=("main")

	# Main 0_2 en (no release)
	RESULT=0
	languages=("en")
	ta_asm_assets  0 2 1 1 ${HEADER} 0 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Main 2_2 en
	RESULT=0
	ta_asm_assets  2 2 1 1 ${HEADER} ${dirReleaseOutput} 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Main 0_2 de/fr/es/ru/pt
	RESULT=0
	languages=("de" "fr" "es" "ru" "pt")
	ta_asm_assets  0 2 1 1 ${HEADER} ${dirReleaseOutput} 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Sponsors
	assets=("sponsors")

	# Sponsors 0_1 en (no release)
	RESULT=0
	languages=("en")
	ta_asm_assets  0 1 1 1 ${HEADER} 0 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Sponsors 1_1 en 
	RESULT=0
	ta_asm_assets  1 1 1 1 ${HEADER} ${dirReleaseOutput} 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# Sponsors 0_1 de/fr/es/ru/pt
	RESULT=0
	languages=("de" "fr" "es" "ru" "pt")
	ta_asm_assets  0 1 1 1 ${HEADER} ${dirReleaseOutput} 0
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	# History
	assets=("history")

	# History 0_1 en (no release)
	RESULT=0
	languages=("en")
	ta_asm_assets  0 1 1 1 ${HEADER} 0 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0
	# History 1_1 en
	RESULT=0
	ta_asm_assets  1 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0
	# History 0_1 de/fr/es/ru/pt
	RESULT=0
	languages=("de" "fr" "es" "ru" "pt")
	ta_asm_assets  0 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0


	# HistoryChart
	assets=("historychart")

	# HistoryChart 0_1 en (no release)
	RESULT=0
	languages=("en")
	ta_asm_assets  0 1 1 1 ${HEADER} 0 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0
	# HistoryChart 1_1 en
	RESULT=0
	ta_asm_assets  1 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0
	# HistoryChart 0_1 de/fr/es/ru/pt
	RESULT=0
	languages=("de" "fr" "es" "ru" "pt")
	ta_asm_assets  0 1 1 1 ${HEADER} ${dirReleaseOutput} 1
	[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_assets failed: $?" && return 0

	echo -e "\n\e[1m\tta_asm_main done\e[0m"
	RESULT=1
	return 1
}


main()
{
	RESULT=0
	echo -e "\n\n\n\e[1m \e[31mStarting.....\e[0m\n\n\n"
	if [ "$#" -eq 0 ]; then
		ta_asm_main
		[ "$RESULT" -ne 1 ] && echo -e "\e[31m$0 ta_asm_main failed:\e[0m $?" && exit 0
	else
		echo -e "\n\e[1mmain($@)\e[0m\n"
		local languages=()
		local dMode=-1
		local OK=1
		if [ "$#" -ne 4 ]; then
			OK=0
		else
			if [ "${1}" == "-d" ] && [ "${3}" == "-l" ]; then
				dMode="${2}"
				languages="${4}"
			fi
			if [ "${1}" == "-l" ] && [ "${3}" == "-d" ]; then
				dMode="${4}"
				languages=( "${2}" )
			fi
		fi
		[ "${dMode}" -ne 0 ] && [ "${dMode}" -ne 1 ] && [ "${dMode}" -ne 2 ] && [ "${dMode}" -ne 3 ] && OK=0
		echo "OK: ${OK}"
		echo "Languages(${#languages[@]}): ${languages[@]}"
		echo "dMode: ${dMode}"
		if [ "${OK}" != 1 ]; then
			OK=0
			RESULT=0
			echo "TA usage: (no arguments) - main, or disassemble: -d [disasm mode 0/1/2/3] -l language"
			exit 0
		fi
		ta_asm_disasm ${dMode} "${languages[@]}"
		[ "$RESULT" -ne 1 ] && echo "$0 ta_asm_disasm failed: $?" && return 0
	fi
	RESULT=1
	echo -e "\e[32m\nTA all done\n\e[0m"
}
main "${@}" | tee ${LOGFILE}
exit 1
