@START_HEADER
CDASM_TEMPLATE_FILE="doc/getting-started_p1_t1.tpcon"
CDASM_INPUT_FILE="content/doc/getting-started_p1_t1_fr.cont1"
CDASM_OUTPUT_FILE="content/doc/getting-started_p1_t1_fr.cont2"
DASM_TEMPLATE_FILE="doc/getting-started_p1_t1.tp"
DASM_INPUT_FILE="doc/getting-started_p1_fr.html"
DASM_OUTPUT_FILE="content/doc/getting-started_p1_t1_fr.cont1"
# --------------------------
HEADER_TEMPLATE_VERSION="1"
HEADER_TEMPLATE_DATE="1-23-2022"
HEADER_TEMPLATE_NOTES="First header template."
# --------------------------
PAGE_OUTPUT_FILENAME="{{PAGE_FILENAME_LANG_PATH[\\{\\{PAGE_ASSET\\}\\}]}}{{@REFERENCE(\\{\\{PAGE_FILENAME_LANG_TOKEN_BASE[\\\\{\\\\{PAGE_ASSET\\\\}\\\\}]\\}\\},\\{\\{PAGE_LANG_CODE\\}\\})}}"
# --------------------------
PAGE_ALT_LANG_CODE[0]="en"
PAGE_ALT_LANG_CODE[1]="de"
PAGE_ALT_LANG_CODE[2]="fr"
PAGE_ALT_LANG_CODE[3]="es"
PAGE_ALT_LANG_CODE[4]="ru"
PAGE_ALT_LANG_CODE[5]="pt"
# ---
PAGE_ALT_LANG_NAME[0]="English"
PAGE_ALT_LANG_NAME[1]="Deutsch (German)"
PAGE_ALT_LANG_NAME[2]="Français (French)"
PAGE_ALT_LANG_NAME[3]="Español (Spanish)"
PAGE_ALT_LANG_NAME[4]="Pусский (Russian)"
PAGE_ALT_LANG_NAME[5]="Português (Portuguese)"
PAGE_ALT_LANG_NAME[6]="English (default)"
# ---
PAGE_FILENAME_HOME_LANG[en]="index.html"
PAGE_FILENAME_HOME_LANG[de]="index_de.html"
PAGE_FILENAME_HOME_LANG[fr]="index_fr.html"
PAGE_FILENAME_HOME_LANG[es]="index_es.html"
PAGE_FILENAME_HOME_LANG[ru]="index_ru.html"
PAGE_FILENAME_HOME_LANG[pt]="index_pt.html"
# ---
PAGE_FILENAME_HISTORY_LANG[en]="history.html"
PAGE_FILENAME_HISTORY_LANG[de]="history_de.html"
PAGE_FILENAME_HISTORY_LANG[fr]="history_fr.html"
PAGE_FILENAME_HISTORY_LANG[es]="history_es.html"
PAGE_FILENAME_HISTORY_LANG[ru]="history_ru.html"
PAGE_FILENAME_HISTORY_LANG[pt]="history_pt.html"
# ---
PAGE_FILENAME_HISTORYCHART_LANG[en]="historychart.html"
PAGE_FILENAME_HISTORYCHART_LANG[de]="historychart_de.html"
PAGE_FILENAME_HISTORYCHART_LANG[fr]="historychart_fr.html"
PAGE_FILENAME_HISTORYCHART_LANG[es]="historychart_es.html"
PAGE_FILENAME_HISTORYCHART_LANG[ru]="historychart_ru.html"
PAGE_FILENAME_HISTORYCHART_LANG[pt]="historychart_pt.html"
# ---
PAGE_FILENAME_SPONSORS_LANG[en]="sponsors.html"
PAGE_FILENAME_SPONSORS_LANG[de]="sponsors_de.html"
PAGE_FILENAME_SPONSORS_LANG[fr]="sponsors_fr.html"
PAGE_FILENAME_SPONSORS_LANG[es]="sponsors_es.html"
PAGE_FILENAME_SPONSORS_LANG[ru]="sponsors_ru.html"
PAGE_FILENAME_SPONSORS_LANG[pt]="sponsors_pt.html"
# ---
PAGE_FILENAME_BLOG_LANG[en]="index.html"
PAGE_FILENAME_BLOG_LANG[de]="index.html"
PAGE_FILENAME_BLOG_LANG[fr]="index.html"
PAGE_FILENAME_BLOG_LANG[es]="index.html"
PAGE_FILENAME_BLOG_LANG[ru]="index.html"
PAGE_FILENAME_BLOG_LANG[pt]="index.html"
# --------------------------
DOC_GETTING_STARTED_FILENAME_LANG[en]="en/GettingStarted.html"
DOC_GETTING_STARTED_FILENAME_LANG[de]="de/LegLos.html"
DOC_GETTING_STARTED_FILENAME_LANG[fr]="fr/PourCommencer.html"
DOC_GETTING_STARTED_FILENAME_LANG[es]="es/GettingStarted.html"
DOC_GETTING_STARTED_FILENAME_LANG[ru]="ru/GettingStarted.html"
DOC_GETTING_STARTED_FILENAME_LANG[pt]="pt/GettingStarted.html"
# ---
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[en]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[de]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[fr]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[es]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[ru]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[pt]="getstarted"
# ---
DOC_FAQ_FILENAME_LANG[en]="en/FAQ.html"
DOC_FAQ_FILENAME_LANG[de]="de/FAQ.html"
DOC_FAQ_FILENAME_LANG[fr]="fr/FAQ.html"
DOC_FAQ_FILENAME_LANG[es]="es/FAQ.html"
DOC_FAQ_FILENAME_LANG[ru]="ru/FAQ.html"
DOC_FAQ_FILENAME_LANG[pt]="pt/FAQ.html"
# ---
DOC_INSTALL_NIX_FILENAME_LANG[en]="en/InstallLinuxMac.html"
DOC_INSTALL_NIX_FILENAME_LANG[de]="de/InstallLinuxMac.html"
DOC_INSTALL_NIX_FILENAME_LANG[fr]="fr/InstallLinuxMac.html"
DOC_INSTALL_NIX_FILENAME_LANG[es]="es/InstallLinuxMac.html"
DOC_INSTALL_NIX_FILENAME_LANG[ru]="ru/InstallLinuxMac.html"
DOC_INSTALL_NIX_FILENAME_LANG[pt]="pt/InstallLinuxMac.html"
# ---
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[en]="gpu"
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[de]="gpu"
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[fr]="gpu"
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[es]="gpu"
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[ru]="gpu"
DOC_INSTALL_NIX_ANCHOR_GPU_LANG[pt]="gpu"
# ---
DOC_INSTALL_WIN_FILENAME_LANG[en]="en/InstallWindows.html"
DOC_INSTALL_WIN_FILENAME_LANG[de]="de/InstallWindows.html"
DOC_INSTALL_WIN_FILENAME_LANG[fr]="fr/InstallWindows.html"
DOC_INSTALL_WIN_FILENAME_LANG[es]="es/InstallWindows.html"
DOC_INSTALL_WIN_FILENAME_LANG[ru]="ru/InstallWindows.html"
DOC_INSTALL_WIN_FILENAME_LANG[pt]="pt/InstallWindows.html"
# --------------------------
WEBSITE_BASE_URL="https://www.dogecoinfah.com"
# --------------------------
@PAGE_ALT_LANG_FILENAME("")="{{CURRENT_PAGE_ALT_LANG_FILEPATH}}{{@REFERENCE(\\{\\{CURRENT_PAGE_ALT_LANG_FILENAME\\}\\},\\{\\{$PAGE_ALT_LANG_FILENAME_ARG[0\\b\\}\\})}}"
# --------------------------
CURRENT_PAGE_ALT_LANG_FILENAME="{{PAGE_FILENAME_LANG_TOKEN_BASE[\\{\\{PAGE_ASSET\\}\\}]}}"
CURRENT_PAGE_ALT_LANG_COUNT="{{@REFERENCE(\\{\\{CURRENT_PAGE_ALT_LANG_FILENAME\\}\\},##)}}"
# --------------------------
HTML_ALT_LANG_ALTERNATIVE="<link rel=\"alternate\" hreflang=\"{{PAGE_ALT_LANG_CODE[@]}}\" href=\"{{@PAGE_ALT_LANG_FILENAME(\\{\\{PAGE_ALT_LANG_CODE[++]\\}\\})}}\" />"
HTML_LINK_ALTERNATIVES_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@REPEAT(\"\\{\\{HTML_ALT_LANG_ALTERNATIVE\\}\\}\",\"\\{\\{CURRENT_PAGE_ALT_LANG_COUNT\\}\\}\",\"\n\t\t\")}}"
# --------------------------
LANGUAGE_CHOOSER_OPTION="<option value=\"{{PAGE_ALT_LANG_CODE[++]}}\">{{PAGE_ALT_LANG_NAME[++]}}</option>"
LANGUAGE_CHOOSER_OPTION_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@BEGIN(PAGE_ALT_LANG_NAME)}}{{@REPEAT(\"\\{\\{LANGUAGE_CHOOSER_OPTION\\}\\}\",\"\\{\\{CURRENT_PAGE_ALT_LANG_COUNT\\}\\}\",\"\n\t\t\t\t\t\t\t\")}}"
LANGUAGE_CHOOSER_LIST="<li><a hreflang=\"{{PAGE_ALT_LANG_CODE[@]}}\" href=\"{{@PAGE_ALT_LANG_FILENAME(\\{\\{PAGE_ALT_LANG_CODE[++]\\}\\})}}\">{{PAGE_ALT_LANG_NAME[++]}}</a></li>"
LANGUAGE_CHOOSER_LIST_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@BEGIN(PAGE_ALT_LANG_NAME)}}{{@REPEAT(\"\\{\\{LANGUAGE_CHOOSER_LIST\\}\\}\",\"\\{\\{CURRENT_PAGE_ALT_LANG_COUNT\\}\\}\",\"\n\t\t\t\t\t\t\t\t\")}}"
# --------------------------
HTML_HREF_ALWAYS_EN="hreflang=\"en\" "
HTML_HREF_BLANK_NOOPENER=" rel=\"noopener\""
# PAGE_FILENAME_LANG_TOKEN_BASE[default="PAGE_FILENAME_HOME_LANG-default"
PAGE_FILENAME_LANG_TOKEN_BASE[main]="PAGE_FILENAME_HOME_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[history]="PAGE_FILENAME_HISTORY_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[historychart]="PAGE_FILENAME_HISTORYCHART_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[sponsors]="PAGE_FILENAME_SPONSORS_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[blog]="PAGE_FILENAME_BLOG_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[doc-getting-started]="DOC_GETTING_STARTED_FILENAME_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[doc-faq]="DOC_FAQ_FILENAME_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[doc-install-nix]="DOC_INSTALL_NIX_FILENAME_LANG"
PAGE_FILENAME_LANG_TOKEN_BASE[doc-install-win]="DOC_INSTALL_WIN_FILENAME_LANG"
# PAGE_FILENAME_LANG_PATH[default]=""
PAGE_FILENAME_LANG_PATH[main]=""
PAGE_FILENAME_LANG_PATH[history]=""
PAGE_FILENAME_LANG_PATH[historychart]=""
PAGE_FILENAME_LANG_PATH[sponsors]=""
PAGE_FILENAME_LANG_PATH[blog]="blog/"
PAGE_FILENAME_LANG_PATH[doc-getting-started]="doc/"
PAGE_FILENAME_LANG_PATH[doc-faq]="doc/"
PAGE_FILENAME_LANG_PATH[doc-install-nix]="doc/"
PAGE_FILENAME_LANG_PATH[doc-install-win]="doc/"
# ---
@PAGE_FILENAME_LANG_PATH("")="{{PAGE_FILENAME_LANG_PATH[\\{\\{$PAGE_FILENAME_LANG_PATH_ARG[0\\b\\}\\}]}}"
@PAGE_FILENAME_LANG_TOKEN_BASE("")="{{PAGE_FILENAME_LANG_TOKEN_BASE[\\{\\{$PAGE_FILENAME_LANG_TOKEN_BASE_ARG[0\\b\\}\\}]}}"
# --------------------------
@PAGE_FILENAME("")="{{@REFERENCE(\\{\\{@PAGE_FILENAME_LANG_TOKEN_BASE(\\\\{\\\\{$PAGE_FILENAME_ARG[0]\\\\}\\\\})\\}\\},\\{\\{PAGE_LANG_CODE\\}\\})}}"
@PAGE_LOCAL_LINK("")="{{CURRENT_PAGE_REL_FILEPATH}}{{@PAGE_FILENAME_LANG_PATH(\\{\\{$PAGE_LOCAL_LINK_ARG[0]\\}\\})}}{{@PAGE_FILENAME(\\{\\{$PAGE_LOCAL_LINK_ARG[0]\\}\\})}}"
@PAGE_FULL_LINK("")="{{WEBSITE_BASE_URL}}/{{@PAGE_FILENAME_LANG_PATH(\\{\\{$PAGE_FULL_LINK_ARG[0]\\}\\})}}{{@PAGE_FILENAME(\\{\\{$PAGE_FULL_LINK_ARG[0]\\}\\})}}"
@DOC_PAGE_REL_LINK("")="{{CURRENT_DOC_PAGE_REL_FILEPATH}}{{@PAGE_FILENAME(\\{\\{$DOC_PAGE_REL_LINK_ARG[0]\\}\\})}}"
# --------------------------
DOC_GETTING_STARTED_ANCHOR_GETSTARTED="{{DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_INSTALL_NIX_ANCHOR_GPU="{{DOC_INSTALL_NIX_ANCHOR_GPU_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
#  @insert@
HTML_COMMENT_OUTPUT="{{PAGE_ASSET}} P{{PAGE_VERSION}}_T{{TEMPLATE_VERSION}} {{PAGE_LANG_CODE}} {{@DATETIME()}}"
#  @insert@
LANGUAGE_CHOOSER_OPTIONS="{{LANGUAGE_CHOOSER_OPTION_REPEAT}}"
#  @insert@
LANGUAGE_CHOOSER_LIST_ITEMS="{{LANGUAGE_CHOOSER_LIST_REPEAT}}"
#  @insert@
HTML_LINK_ALTERNATIVES="{{HTML_LINK_ALTERNATIVES_REPEAT}}"
HTML_LANG="{{PAGE_LANG_CODE}}"
#  override versions
JS_VER_GENERAL="1_3_1"
JS_VER_LIVEDATA="1_3_1"
JS_VER_TEXTMANAGER="1_3_1"
JS_VER_LANGCHOOSER="1_3_1"
JS_VER_WORKERLIST="1_3_1"
JS_VER_TXIDLIST="1_3_1"
JS_VER_HISTDATA="1_3_1"
JS_VER_HISTDATA_CHART="1_3_1"
JS_VER_HISTFOLDER="1_3_1"
CSS_VER_NS_STYLE="1_3_1"
CSS_VER_NS_XLSTYLE="1_3_1"
CSS_VER_HIST="1_3_1"
# --------------------------
PAGE_ASSET="doc-getting-started"
PAGE_VERSION="1"
TEMPLATE_TYPE="content"
TEMPLATE_VERSION="1"
TEMPLATE_DATE="12-2-2021"
TEMPLATE_NOTES="Can input page versions 0 or 1 to make version p1 cont2 output."
#  override for doc language directory structure
CURRENT_PAGE_REL_FILEPATH="../../"
#  override for doc language directory structure
CURRENT_PAGE_ALT_LANG_FILEPATH="../"
#  @insert@
SEC_INTRO_LINK_1_FILENAME="{{@DOC_PAGE_REL_LINK(doc-faq)}}"
#  @insert@
SEC_START_LIST_1_LINK="{{@DOC_PAGE_REL_LINK(doc-install-win)}}"
#  @insert@
SEC_START_LIST_2_LINK="{{@DOC_PAGE_REL_LINK(doc-install-nix)}}"
#  @insert@ overrides p0 shortened name
EXTREME_OC_NAME="Extreme Overclocking"
#  @insert@ overrides p0 to relative
BASE_RESOURCE_DIR="../"
#  @insert@ overrides p0 to name without space
WEBSITE_TITLE="DogecoinFah"
#  @insert@ overrides to normalize separator
BREADCRUMB_SEPARATOR_FIX="-"
@END_HEADER
HTML_COMMENT_OUTPUT_PLACEHOLDER_DISCARD=""
# @hitend@ 
PAGE_LANG_CODE="fr"
# @hitend@ 
HTML_LINK_ALTERNATIVES_REPEAT="<link rel=\"alternate\" hreflang=\"en\" href=\"../en/GettingStarted.html\" />\n\t\t<link rel=\"alternate\" hreflang=\"de\" href=\"../de/LegLos.html\" />\n\t\t<link rel=\"alternate\" hreflang=\"fr\" href=\"../fr/PourCommencer.html\" />"
HTML_TITLE="Dogecoin Folding @ Home"
HTML_DESCRIPTION="Minez pour des traitements avec Folding @ Home et gagnez des Dogecoins!"
HTML_KEYWORDS="dogecoin,folding @ home,pour commencer,folding"
#  @insert@ overrides p0 to relative
BASE_RESOURCE_DIR="../"
#  @insert@ overrides p0 to relative
BASE_RESOURCE_DIR="../"
#  @insert@ overrides p0 to relative
BASE_RESOURCE_DIR="../"
LANGUAGE_CHOOSER_ICON_IMG_ALT="blue universal language selector icon by language icon org"
LANGUAGE_CHOOSER_NOSCRIPT_TEXT="(Le sélecteur de langue nécessite JavaScript)"
BASE_RESOURCE_DIR="../"
WEBSITE_LOGO_IMG_ALT="Logo DogecoinFah"
# @hitend@ 
PAGE_FULL_LINK="https://www.dogecoinfah.com"
#  @insert@ overrides p0 to name without space
WEBSITE_TITLE="DogecoinFah"
# @hitend@ 
PAGE_LOCAL_LINK="https://www.dogecoinfah.com"
#  BREADCRUMB_SEPARATOR_FIX
BREADCRUMB_HOME="Accueil"
#  @insert@ overrides to normalize separator
BREADCRUMB_SEPARATOR_FIX=""
BREADCRUMB_GETTING_STARTED="Commencer"
UPDATED_DATE_TEXT="Mis"
UPDATED_DATE_DATE="à jour le 6 septembre 2021"
# ---------------------------------------------
SEC_INTRO_ANCHOR="intro"
# @hitend@ 
SEC_INTRO_TITLE_TEXT="Pour commencer"
SEC_INTRO_DESCRIPTION_BEFORE="DogecoinFah est une communauté caritative basée sur le don de Dogecoins à une équipe de "
SEC_INTRO_DESCRIPTION_SHIBE_DEFINITION="A fun term for Dogecoin community members, based on Dogecoin\'s mascot, the Shiba Inu dog."
SEC_INTRO_DESCRIPTION_SHIBE_NAME="shibes"
# @hitend@ 
SEC_INTRO_DESCRIPTION_AFTER=" qui aident les chercheurs scientifiques à trouver des remèdes contre les maladies. L\'équipe de Dogefolders #226715 contribue au projet Folding@home en offrant une puissance informatique afin d\'aider les scientifiques."
SEC_INTRO_LINK_1_FILENAME="./FAQ.html"
SEC_INTRO_LINK_1_TEXT="Foire aux questions"
# ---------------------------------------------
SEC_COMM_ANCHOR="community"
SEC_COMM_TITLE="La Communauté"
SEC_COMM_LIST_1_TEXT_BEFORE=""
PAGE_FULL_LINK="https://www.dogecoinfah.com/"
WEBSITE_DOMAIN_NAME="DogecoinFah.com"
SEC_COMM_LIST_2_TEXT_BEFORE="Reddit "
HTML_HREF_ALWAYS_EN=""
WEBSITE_LINK_REDDIT_LINK="https://www.reddit.com/r/Dogecoinfah"
WEBSITE_LINK_REDDIT_TITLE="/r/DogecoinFah"
SEC_COMM_LIST_3_TEXT_BEFORE="Vous pouvez afficher des informations sur vos points de folding actuels via "
HTML_HREF_ALWAYS_EN=""
SEC_COMM_LIST_3_TEXT_FAH_OFFICIAL="Folding@home web-stats"
SEC_COMM_LIST_3_TEXT_OR=", ou des statistiques analytiques plus détaillées via "
HTML_HREF_ALWAYS_EN=""
EXTREME_OC_NAME="Extreme Overclocking"
SEC_COMM_LIST_4_TEXT_BEFORE="Discord: "
HTML_HREF_ALWAYS_EN=""
WEBSITE_LINK_DISCORD_LINK="https://discord.gg/dHVqwWmFjt"
WEBSITE_LINK_DISCORD_TITLE="DogecoinFah"
# ---------------------------------------------
SEC_FAH_ANCHOR="fah"
SEC_FAH_TITLE="Folding At Home"
SEC_FAH_DESCRIPTION="Folding@home est un système informatique décentralisé qui aide les scientifiques à étudier la COVID-19, Alzheimer, Parkinson et les cancers, et dautres maladies, en exécutant simplement un logiciel sur votre ordinateur. Plus de 100 000 personnes font don de plus de 20 000 téraflops de puissance informatique. Sans valeur,  les points sont attribués par Folding@home pour le travail informatique réalisé et les utilisateurs peuvent participer en équipe pour capitaliser plus de points. Folding@home a commencé à l\'université de Stanford, mais est maintenant basée à la Washington University à la St. Louis School of Medicine."
# ---------------------------------------------
SEC_REWARD_ANCHOR="rewards"
SEC_REWARD_TITLE="Le système de récompenses"
SEC_REWARD_DESCRIPTION="Nous avons créé un système de récompenses automatisé, DogecoinFah, qui paie chaque semaine les folders grâce aux dons à notre équipe de folding (fin samedi TPS). Les folders doivent utiliser une adresse valide de portefeuille Dogecoin comme nom de folders et #226715 comme équipe de folders . Le système automatisé combinera plusieurs récompenses en un seul paiement hebdomadaire. Certaines récompenses donnent plus de Dogecoins aux membres de l\'équipe qui ont foldé plus de points pour l\'équipe, tandis que d\'autres donnent quelques Dogecoins à chaque membre de l\'équipe qui ont foldé des points. Une fois que votre machine a terminé une unité de travail, Folding@home vous récompensera avec des points. Certaines unités de travail prennent une semaine, mais récompense avec plus de points, alors que d\'autres unités de travail peuvent être complétées en heures, mais ne peuvent pas fournir autant de points de récompense. Les points de folding sont utilisés pour vérifier la participation à la récompense, mais sont entièrement contrôlés par Folding@home."
# ---------------------------------------------
SEC_WEBSITE_ANCHOR="website"
SEC_WEBSITE_TITLE="Le site web"
SEC_WEBSITE_DESCRIPTION_BEFORE="Nous avons créé le site web "
PAGE_FULL_LINK="https://www.dogecoinfah.com"
WEBSITE_LINK_WEBSITE_TITLE="DogecoinFah.com"
# @hitend@ 
SEC_WEBSITE_DESCRIPTION_AFTER=" pour aider à la visibilité et présenter des informations sur le projet. Le site Web contient également des données « en direct » sur le montant actuel des paiements, les statistiques de l\'équipe et l\'historique détaillé des paiements passés. Derrière la scène, ce site Web suit les scores de folding des membres de l\'équipe et automatise les paiements."
# ---------------------------------------------
SEC_WIT_ANCHOR="whatittakes"
SEC_WIT_TITLE="Ce que ça coûte"
SEC_WIT_DESCRIPTION="Aucune inscription ou vérification n\'est requise. Les folders sont aussi anonymes au public que leur adresse Dogecoin. Le folding nécessite l\'exécution d\'une application source fermée fournie par Folding@home. Vous devez accepter les conditions d\'utilisation ou les politiques et pratiques de confidentialité de Folding@home."
# ---------------------------------------------
SEC_START_ANCHOR="getstarted"
SEC_START_TITLE="Commencer"
SEC_START_DESCRIPTION_BEFORE="Commencez par installer le logiciel "
HTML_HREF_ALWAYS_EN=""
# @hitend@ 
SEC_START_DESCRIPTION_AFTER=" puis configurez-le avec l\'adresse de votre portefeuille Dogecoin comme votre nom d\'utilisateur et #226715 (Dogefolders) comme équipe de folding. À la fin de la semaine, le système automatisé comparera vos points pour la semaine, au reste de l\'équipe et envoyez des Dogecoins à votre portefeuille! Soyez patient car vous devrez terminer votre première unité de travail avant de recevoir des points ou d\'être récompensé."
SEC_START_LIST_1_LINK="./InstallWindows.html"
SEC_START_LIST_1_TEXT="Installation et configuration sous Windows"
SEC_START_LIST_2_LINK="./InstallLinuxMac.html"
SEC_START_LIST_2_TEXT="Installation et configuration sous Linux et OSX"
SEC_START_LIST_3="Dernier point, nous avons entendu, Android et iOS ne sont pas actuellement pris en charge correctement."
# ---------------------------------------------
SEC_THANKS_ANCHOR="thanks"
SEC_THANKS_TITLE="Remerciements"
SEC_THANKS_DESCRIPTION="Un grand merci à tous ceux qui ont fait don de Dogecoins, de puissance informatique, ou de leur temps pour que cela se produise !"
SEC_THANKS_SALUTE="Joyeux folding!"
HTML_COMMENT_OUTPUT="(assembler comments here)"
BASE_RESOURCE_DIR="../"
LANGUAGE_CHOOSER_ICON_IMG_ALT="blue universal language selector icon by language icon org"
LANGUAGE_CHOOSER_OPTIONS="<!--Language Chooser Options List Here-->"
LANGUAGE_CHOOSER_LIST_ITEMS="<!--Language Chooser List Items Here-->"
# @hitend@ 
HTML_LINK_ALTERNATIVES_REPEAT="<!-- Link Alternatives Here -->"
HTML_LANG="{{PAGE_LANG_CODE}}"
#  @insert@
HTML_LINK_ALTERNATIVES="{{HTML_LINK_ALTERNATIVES_REPEAT}}"
#  just grab the icon alt, discard rest
LANGUAGE_CHOOSER_OLD="<div id=\"lang\" name=\"lang\">\n\t\t\t\t<img src=\"../images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" />\n\t\t\t\t<select id=\"langsel\" name=\"langsel\">\n\t\t\t\t\t<option value=\'en\'>English</option>\n\t\t\t\t\t<option value=\'de\'>Deutsch (German)</option>\n\t\t\t\t\t<option value=\'fr\'>Français (French)</option>\n\t\t\t\t</select>\n\t\t\t\t<noscript>{{LANGUAGE_CHOOSER_NOSCRIPT_TEXT}}</noscript>\n\t\t\t</div>"
# --------------------------
WEBSITE_LOGO_LINK="{{BASE_RESOURCE_DIR}}images/dfahlogo_sm.png"
WEBSITE_TITLE_LINK="{{@PAGE_FULL_LINK(main)}}"
BREADCRUMB_HOME_URL="{{@PAGE_LOCAL_LINK(main)}}"
UPDATED_DATE="({{UPDATED_DATE_TEXT}} {{UPDATED_DATE_DATE}})"
SEC_INTRO_TITLE="DogecoinFah - {{SEC_INTRO_TITLE_TEXT}}"
SEC_INTRO_DESCRIPTION="{{SEC_INTRO_DESCRIPTION_BEFORE}}<dfn title=\"{{SEC_INTRO_DESCRIPTION_SHIBE_DEFINITION}}\">{{SEC_INTRO_DESCRIPTION_SHIBE_NAME}}</dfn>{{SEC_INTRO_DESCRIPTION_AFTER}}"
SEC_INTRO_LINK_1="<a href=\"{{SEC_INTRO_LINK_1_FILENAME}}\">{{SEC_INTRO_LINK_1_TEXT}}</a>"
SEC_COMM_LIST_1="{{SEC_COMM_LIST_1_TEXT_BEFORE}}<a href=\"{{@PAGE_FULL_LINK(main)}}\">{{WEBSITE_DOMAIN_NAME}}</a>"
SEC_COMM_LIST_2="{{SEC_COMM_LIST_2_TEXT_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"{{WEBSITE_LINK_REDDIT_LINK}}\">{{WEBSITE_LINK_REDDIT_TITLE}}</a>"
SEC_COMM_LIST_3="{{SEC_COMM_LIST_3_TEXT_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"https://stats.foldingathome.org/team/226715\">{{SEC_COMM_LIST_3_TEXT_FAH_OFFICIAL}}</a>{{SEC_COMM_LIST_3_TEXT_OR}}<a {{HTML_HREF_ALWAYS_EN}}href=\"https://folding.extremeoverclocking.com/team_summary.php?s=&amp;t=226715\">{{EXTREME_OC_NAME}}</a>"
SEC_COMM_LIST_4="{{SEC_COMM_LIST_4_TEXT_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"{{WEBSITE_LINK_DISCORD_LINK}}\">{{WEBSITE_LINK_DISCORD_TITLE}}</a>"
SEC_WEBSITE_DESCRIPTION="{{SEC_WEBSITE_DESCRIPTION_BEFORE}}<a href=\"{{@PAGE_FULL_LINK(main)}}\">{{WEBSITE_LINK_WEBSITE_TITLE}}</a>{{SEC_WEBSITE_DESCRIPTION_AFTER}}"
SEC_START_DESCRIPTION="{{SEC_START_DESCRIPTION_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"https://foldingathome.org/#downloads\">Folding@home</a>{{SEC_START_DESCRIPTION_AFTER}}"
#  links are overridden
SEC_START_LIST_1="<a href=\"{{SEC_START_LIST_1_LINK}}\">{{SEC_START_LIST_1_TEXT}}</a>"
#  links are overridden
SEC_START_LIST_2="<a href=\"{{SEC_START_LIST_2_LINK}}\">{{SEC_START_LIST_2_TEXT}}</a>"
#  @insert@ @end@  default override for doc language directory structure
CURRENT_PAGE_ALT_LANG_FILEPATH="./"
#  @insert@ @end@  default override for doc language directory structure
CURRENT_PAGE_REL_FILEPATH="./"
#  @insert@ @end@  default
CURRENT_DOC_PAGE_REL_FILEPATH="../"
#  @insert@ default for overridden
HTML_COMMENT_OUTPUT_PLACEHOLDER="\n<!--\n\t{{HTML_COMMENT_OUTPUT}}\n-->"
#  @insert@ replace old or default
LANGUAGE_CHOOSER="<div id=\"rtcorner\">\n\t\t\t\t\t<div id=\"lang\" name=\"lang\">\n\t\t\t\t\t\t<img src=\"{{BASE_RESOURCE_DIR}}images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" /><select id=\"langsel\" name=\"langsel\" style=\"display:none;\">\n\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_OPTIONS}}\n\t\t\t\t\t\t</select>\n\t\t\t\t\t\t<noscript>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_LIST_ITEMS}}\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</noscript>\n\t\t\t\t\t</div>\n\t\t\t\t</div>"
#  @insert@ default for p0, overridden @insert@
HTML_LINK_ALTERNATIVES="{{HTML_LINK_ALTERNATIVES_REPEAT}}"
#  @insert@ @end@ default @insert@ overrides p0 to relative
BASE_RESOURCE_DIR="../"
