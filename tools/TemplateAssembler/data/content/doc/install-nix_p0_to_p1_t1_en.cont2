@START_HEADER
CDASM_DATE="{{@DATE(/INTL)}}"
CDASM_TA_VERSION="{{$TA_VERSION}}"
CDASM_TEMPLATE_FILE="doc/install-nix_p1_t1.tpcon"
CDASM_INPUT_FILE="content/doc/install-nix_p0_t1_en.cont1"
CDASM_OUTPUT_FILE="content/doc/install-nix_p0_to_p1_t1_en.cont2"
DASM_DATE="{{@DATE(/INTL)}}"
DASM_TA_VERSION="{{$TA_VERSION}}"
DASM_TEMPLATE_FILE="doc/install-nix_p0_t1.tp"
DASM_INPUT_FILE="doc/install-nix_p0_en.html"
DASM_OUTPUT_FILE="content/doc/install-nix_p0_t1_en.cont1"
# --------------------------
PAGE_ASSET="doc-install-nix"
PAGE_VERSION="1"
TEMPLATE_TYPE="content"
TEMPLATE_VERSION="1"
TEMPLATE_DATE="11-23-2021"
TEMPLATE_NOTES="Can input page versions 0 or 1 to make version p1 cont2 output."
# --------------------------
PAGE_ALT_LANG_CODE[0]="en"
PAGE_ALT_LANG_CODE[1]="de"
PAGE_ALT_LANG_CODE[2]="fr"
PAGE_ALT_LANG_CODE[3]="es"
# ---
PAGE_ALT_LANG_NAME[0]="English"
PAGE_ALT_LANG_NAME[1]="Deutsch (German)"
PAGE_ALT_LANG_NAME[2]="Français (French)"
PAGE_ALT_LANG_NAME[3]="Español (Spanish)"
# ---
PAGE_ALT_LANG_FILENAME[0]="en/InstallLinuxMac.html"
PAGE_ALT_LANG_FILENAME[1]="de/InstallLinuxMac.html"
PAGE_ALT_LANG_FILENAME[2]="fr/InstallLinuxMac.html"
PAGE_ALT_LANG_FILENAME[3]="es/InstallLinuxMac.html"
# --------------------------
PAGE_FILENAME_HOME_LANG[en]="index"
PAGE_FILENAME_HOME_LANG[de]="index_de"
PAGE_FILENAME_HOME_LANG[fr]="index_fr"
PAGE_FILENAME_HOME_LANG[es]="index_es"
# ---
PAGE_FILENAME_HISTORY_LANG[en]="history"
PAGE_FILENAME_HISTORY_LANG[de]="history"
PAGE_FILENAME_HISTORY_LANG[fr]="history"
PAGE_FILENAME_HISTORY_LANG[es]="history"
# ---
PAGE_FILENAME_HISTORYCHART_LANG[en]="historychart"
PAGE_FILENAME_HISTORYCHART_LANG[de]="historychart"
PAGE_FILENAME_HISTORYCHART_LANG[fr]="historychart"
PAGE_FILENAME_HISTORYCHART_LANG[es]="historychart"
# ---
PAGE_FILENAME_SPONSORS_LANG[en]="sponsors"
PAGE_FILENAME_SPONSORS_LANG[de]="sponsors_de"
PAGE_FILENAME_SPONSORS_LANG[fr]="sponsors_fr"
PAGE_FILENAME_SPONSORS_LANG[es]="sponsors_es"
# ---
PAGE_FILENAME_BLOG_LANG[en]="blog"
PAGE_FILENAME_BLOG_LANG[de]="blog"
PAGE_FILENAME_BLOG_LANG[fr]="blog"
PAGE_FILENAME_BLOG_LANG[es]="blog"
# --------------------------
DOC_GETTING_STARTED_FILENAME_LANG[en]="en/GettingStarted"
DOC_GETTING_STARTED_FILENAME_LANG[de]="de/LegLos"
DOC_GETTING_STARTED_FILENAME_LANG[fr]="fr/PourCommencer"
DOC_GETTING_STARTED_FILENAME_LANG[es]="es/GettingStarted"
# ---
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[en]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[de]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[fr]="getstarted"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[es]="getstarted"
# ---
DOC_FAQ_FILENAME_LANG[en]="en/FAQ"
DOC_FAQ_FILENAME_LANG[de]="de/FAQ"
DOC_FAQ_FILENAME_LANG[fr]="fr/FAQ"
DOC_FAQ_FILENAME_LANG[es]="es/FAQ"
# ---
DOC_INSTALL_NIX_FILENAME_LANG[en]="en/InstallLinuxMac"
DOC_INSTALL_NIX_FILENAME_LANG[de]="de/InstallLinuxMac"
DOC_INSTALL_NIX_FILENAME_LANG[fr]="fr/InstallLinuxMac"
DOC_INSTALL_NIX_FILENAME_LANG[es]="es/InstallLinuxMac"
# ---
DOC_INSTALL_WIN_FILENAME_LANG[en]="en/InstallWindows"
DOC_INSTALL_WIN_FILENAME_LANG[de]="de/InstallWindows"
DOC_INSTALL_WIN_FILENAME_LANG[fr]="fr/InstallWindows"
DOC_INSTALL_WIN_FILENAME_LANG[es]="es/InstallWindows"
#  @insert@
HTML_COMMENT_OUTPUT="P{{PAGE_VERSION}}_T{{TEMPLATE_VERSION}}"
HTML_ALT_LANG_ALTERNATIVE="<link rel=\"alternate\" hreflang=\"{{PAGE_ALT_LANG_CODE[++]}}\" href=\"{{BASE_RESOURCE_DIR}}{{PAGE_ALT_LANG_FILENAME[++]}}\" />"
HTML_LINK_ALTERNATIVES_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@BEGIN(PAGE_ALT_LANG_FILENAME)}}{{@REPEAT(\"\\{\\{HTML_ALT_LANG_ALTERNATIVE\\}\\}\",\"\\{\\{PAGE_ALT_LANG_CODE[#]\\}\\}\",\"\n\t\t\")}}"
LANGUAGE_CHOOSER_COUNT="{{PAGE_ALT_LANG_CODE[#]}}"
LANGUAGE_CHOOSER_OPTION="<option value=\"{{PAGE_ALT_LANG_CODE[++]}}\">{{PAGE_ALT_LANG_NAME[++]}}</option>"
LANGUAGE_CHOOSER_OPTION_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@BEGIN(PAGE_ALT_LANG_NAME)}}{{@REPEAT(\"\\{\\{LANGUAGE_CHOOSER_OPTION\\}\\}\",\"\\{\\{LANGUAGE_CHOOSER_COUNT\\}\\}\",\"\n\t\t\t\t\t\t\t\")}}"
LANGUAGE_CHOOSER_LIST="<li><a hreflang=\"{{PAGE_ALT_LANG_CODE[++]}}\" href=\"{{BASE_RESOURCE_DIR}}{{PAGE_ALT_LANG_FILENAME[++]}}\">{{PAGE_ALT_LANG_NAME[++]}}</a></li>"
LANGUAGE_CHOOSER_LIST_REPEAT="{{@BEGIN(PAGE_ALT_LANG_CODE)}}{{@BEGIN(PAGE_ALT_LANG_FILENAME)}}{{@BEGIN(PAGE_ALT_LANG_NAME)}}{{@REPEAT(\"\\{\\{LANGUAGE_CHOOSER_LIST\\}\\}\",\"\\{\\{LANGUAGE_CHOOSER_COUNT\\}\\}\",\"\n\t\t\t\t\t\t\t\t\")}}"
# --------------------------
HTML_HREF_ALWAYS_EN="hreflang=\"en\" "
# --------------------------
PAGE_FILENAME_HOME="{{PAGE_FILENAME_HOME_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
PAGE_FILENAME_HISTORY="{{PAGE_FILENAME_HISTORY_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
PAGE_FILENAME_HISTORYCHART="{{PAGE_FILENAME_HISTORYCHART_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
PAGE_FILENAME_SPONSORS="{{PAGE_FILENAME_SPONSORS_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
PAGE_FILENAME_BLOG="{{PAGE_FILENAME_BLOG_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_GETTING_STARTED_FILENAME="{{DOC_GETTING_STARTED_FILENAME_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_GETTING_STARTED_ANCHOR_GETSTARTED="{{DOC_GETTING_STARTED_ANCHOR_GETSTARTED_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_FAQ_FILENAME="{{DOC_FAQ_FILENAME_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_INSTALL_NIX_FILENAME="{{DOC_INSTALL_NIX_FILENAME_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
DOC_INSTALL_WIN_FILENAME="{{DOC_INSTALL_WIN_FILENAME_LANG[\\{\\{PAGE_LANG_CODE\\}\\}]}}"
# --------------------------
HTML_HREF_ALWAYS_EN="hreflang=\"en\" "
FULL_HOME_PAGE_LINK="https://www.dogecoinfah.com/{{PAGE_FILENAME_HOME}}.html"
FULL_SPONSORS_PAGE_LINK="https://www.dogecoinfah.com/{{PAGE_FILENAME_SPONSORS}}.html"
FULL_HISTORY_PAGE_LINK="https://www.dogecoinfah.com/{{PAGE_FILENAME_HISTORY}}.html"
FULL_HISTORYCHART_PAGE_LINK="https://www.dogecoinfah.com/{{PAGE_FILENAME_HISTORYCHART}}.html"
FULL_DOC_GETTING_STARTED_LINK="{{BASE_RESOURCE_DIR}}{{DOC_GETTING_STARTED_FILENAME}}.html"
#  @insert@
WEBSITE_HOME_LINK="{{FULL_HOME_PAGE_LINK}}"
#  @insert@
LANGUAGE_CHOOSER_OPTIONS="{{LANGUAGE_CHOOSER_OPTION_REPEAT}}"
#  @insert@
LANGUAGE_CHOOSER_LIST_ITEMS="{{LANGUAGE_CHOOSER_LIST_REPEAT}}"
#  @insert@
HTML_LINK_ALTERNATIVES="{{HTML_LINK_ALTERNATIVES_REPEAT}}"
#  @insert@ overrides to normalize without space
WEBSITE_TITLE="DogecoinFah"
#  @insert@ overrides to normalize caps
FAH_NAME_HOME="home"
#  @insert@ overrides to relative
BASE_RESOURCE_DIR="../"
#  @insert@ overrides to normalize separator
BREADCRUMB_SEPARATOR_FIX="-"
#  @insert@ overrides to normalize title
HTML_TITLE="Dogecoin Folding @ Home"
#  @insert@ overridden
MISSING_LI_CLOSE="</li>"
#  @insert@ overridden
SEC_INSTALL_ANCHOR="install-install"
#  @insert@ overridden
SEC_CONFIGURE_ANCHOR="install-config"
#  @insert@ overridden
SEC_INFO_ANCHOR="install-more"
#  @insert@ overridden
SEC_CLOSING_ANCHOR="thats-it"
@END_HEADER
# @hitend@ 
PAGE_LANG_CODE="en"
#  @insert@ overrides to normalize title
HTML_TITLE="Dogecoin Folding @ Home"
HTML_DESCRIPTION="Dig for cures with Folding @ Home and earn Dogecoins!"
HTML_KEYWORDS="dogecoin doge folding proteins simulate mining"
BASE_RESOURCE_DIR="./"
WEBSITE_LOGO_IMG_ALT="logo"
# @hitend@ 
FULL_HOME_PAGE_LINK="/"
#  @insert@ overrides to normalize without space
WEBSITE_TITLE="Dogecoin Fah"
# @hitend@ 
FULL_HOME_PAGE_LINK="/"
BREADCRUMB_HOME="Home"
# @hitend@ 
FULL_DOC_GETTING_STARTED_LINK="./gettingstarted.html"
BREADCRUMB_GETTING_STARTED="Getting Started"
BREADCRUMB_CURRENT="Install &amp; Configure (Linux/OSX)"
# --------------------------
SEC_INTRO_ANCHOR="intro"
WEBSITE_TITLE="Dogecoinfah"
# @hitend@ 
SEC_INTRO_TITLE_TEXT="Installing Folding@home on Linux / OSX"
SEC_INTRO_CONTENT="Folding is so much easier than mining. We are going to show you how to download the Folding@home client, install it, and configure it."
# --------------------------
SEC_SOFTWARE_ANCHOR="software"
SEC_SOFTWARE_TITLE="The Software"
FAH_NAME_HOME="home"
# @hitend@ 
SEC_SOFTWARE_LIST_TITLE_TEXT="comes in 3 separate parts:"
SEC_SOFTWARE_LIST_1="FahClient - The main folding client that does all the work in the background."
SEC_SOFTWARE_LIST_2="FahControl - An optional graphical configuration and monitoring tool."
SEC_SOFTWARE_LIST_3="FahViewer - An optional graphical protein folding visualizer."
SEC_SOFTWARE_DESCRIPTION="FahClient is the main Folding@home client and the only part that is required. The client can be configured through a web interface or manually. If you\'d prefer an informational graphical tool to configure and monitor your Folding@home client, you can install the FahControl tool. Additionally, the FahViewer will graphically visualize your current folding work in 3D."
# --------------------------
SEC_DOWNLOAD_ANCHOR="install-download"
SEC_DOWNLOAD_TITLE="Downloading"
SEC_DOWNLOAD_DESCRIPTION="Folding@home is no longer in most Linux package managers. You\'ll need to download the appropriate package from Folding@home\'s download page. While built on open source technology, Folding@home is not open source and these are precompiled binary packages."
HTML_HREF_ALWAYS_EN=""
# @hitend@ 
FAH_DOWNLOAD_LINK_TEXT=" Folding@home Download Page"
HTML_HREF_ALWAYS_EN=""
FAH_OPENSOURCE_FAQ_LINK_TEXT="Folding@home Open Source FAQ"
# @hitend@ 
MISSING_LI_CLOSE=""
BASE_RESOURCE_DIR=""
DOWNLOAD_LINUX_IMG_ALT="Image of Linux package download section"
#  @insert@ overridden
SEC_INSTALL_ANCHOR="install"
SEC_INSTALL_TITLE="Installing"
SEC_INSTALL_DESCRIPTION="OSX and most Linux distributions come with a graphical package installer tool as well as command line package management tools, which can install these packages on your system."
BASE_RESOURCE_DIR=""
LINUX_PACKAGE_IMG_ALT="Image of GUI Linux package installer tools"
SEC_INSTALL_DESCRIPTION_PACKAGE="Use the appropriate tool, and if in doubt, you can probably double click on the file to get started installing the package. The package may prompt you for some initial configurations. While there are multiple ways to configure the Folding@home client, you might want to consider if you want folding to start automatically when your computer boots here, as it might be tricky to setup later later."
BASE_RESOURCE_DIR=""
LINUX_CONFIG_PACKAGE_IMG_ALT="Image of Fahclient deb package configuration prompt during install"
#  @insert@ overridden
SEC_CONFIGURE_ANCHOR="install"
SEC_CONFIGURE_TITLE="Configuring"
SEC_CONFIGURE_DESCRIPTION_BEFORE="To participate in DogecoinFah, you must fold with your username set to a valid Dogecoin wallet address and team set to Dogefolders #226715. You can configure these and other options with the web interface, by going to "
HTML_HREF_ALWAYS_EN=""
# @hitend@ 
SEC_CONFIGURE_DESCRIPTION_AFTER=". Your browser will connect to your running FahClient on your local machine with this web page and allow you to configure it. If you prefer, you can do this by running the FahControl instead or manually editing the configuration files."
HTML_HREF_ALWAYS_EN=""
# @hitend@ 
CONFIGURE_WEB_LINK_TEXT="Configuration web interface."
#  @insert@ overridden
SEC_INFO_ANCHOR="install-more"
SEC_INFO_TITLE="Installing / Configuring - More Info"
SEC_INFO_DESCRIPTION="For detailed information on installing Folding@home, see their FAQ section:"
HTML_HREF_ALWAYS_EN=""
FAH_FAQ_LINK_TEXT="Folding@home\'s FAQ Page"
HTML_HREF_ALWAYS_EN=""
FAH_ADVANCED_NIX_INSTALL_INFO_LINK_TEXT="Advanced Linux Install Guide"
HTML_HREF_ALWAYS_EN=""
FAH_ADVANCED_CONFIG_INFO_LINK_TEXT="Advanced Configuration Guide"
# @hitend@ 
MISSING_LI_CLOSE=""
# --------------------------
SEC_PASSKEY_ANCHOR="passkey"
SEC_PASSKEY_TITLE="Extra - Passkey"
SEC_PASSKEY_DESCRIPTION="Registering for a passkey is not required, but gives you bonus points when folding. You are emailed a passkey to configure in your FahClient, when you sign up. Folding@home\'s Passkey page asks for your email address and folding user name. This user name is the Dogecoin wallet address you will be folding as."
BASE_RESOURCE_DIR=""
PASSKEY_IMG_ALT="passkey screenshot"
HTML_HREF_ALWAYS_EN=""
FAH_PASSKEY_SIGNUP_LINK_TEXT="Folding@home\'s Passkey Signup Page"
HTML_HREF_ALWAYS_EN=""
FAH_PASSKEY_GUIDE_LINK_TEXT="See Folding@home\'s Passkey Guide"
# --------------------------
SEC_GPU_ANCHOR="gpu"
SEC_GPU_TITLE="Extra - Configure GPU"
SEC_GPU_DESCRIPTION="Folding@home supports folding with some graphics cards as well. Up to date proprietary drivers may be required. To enable GPU folding, you configure your FahClient by adding a folding slot for your GPU."
BASE_RESOURCE_DIR=""
GPU_CONFIG_IMG_ALT="configure gpu screenshot"
SEC_GPU_LIST_TITLE="See the configuration guide for more details."
HTML_HREF_ALWAYS_EN=""
FAH_ADVANCED_CONFIG_GPU_LINK_TEXT="Advanced Configuration Guide"
#  @insert@ overridden
SEC_CLOSING_ANCHOR="thatsit"
SEC_CLOSING_TITLE="That\'s it!"
SEC_CLOSING_DESCRIPTION="After you\'ve configured your username, team, and (optionally)passkey, Folding@home will continue working in the back ground. You can close the web interface or FahControl application and forget about it. That\'s all there is to it!"
HTML_COMMENT_OUTPUT="(assembler comments here)"
# @hitend@ 
HTML_LINK_ALTERNATIVES_REPEAT="<!-- Link Alternatives Here -->"
BASE_RESOURCE_DIR="../"
LANGUAGE_CHOOSER_ICON_IMG_ALT="blue universal language selector icon by language icon org"
LANGUAGE_CHOOSER_OPTIONS="<!--Language Chooser Options List Here-->"
LANGUAGE_CHOOSER_LIST_ITEMS="<!--Language Chooser List Items Here-->"
# --------------------------
HTML_LANG="{{PAGE_LANG_CODE}}"
WEBSITE_LOGO_LINK="{{BASE_RESOURCE_DIR}}images/dfahlogo_sm.png"
WEBSITE_TITLE_LINK="{{FULL_HOME_PAGE_LINK}}"
# --------------------------
BREADCRUMB_HOME_URL="{{FULL_HOME_PAGE_LINK}}"
BREADCRUMB_GETTING_STARTED_URL="{{FULL_DOC_GETTING_STARTED_LINK}}"
SEC_INTRO_TITLE="{{WEBSITE_TITLE}} - {{SEC_INTRO_TITLE_TEXT}}"
SEC_SOFTWARE_LIST_TITLE="Folding@{{FAH_NAME_HOME}} {{SEC_SOFTWARE_LIST_TITLE_TEXT}}"
SEC_DOWNLOAD_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://foldingathome.org/#downloads\">https://foldingathome.org/#downloads</a>{{FAH_DOWNLOAD_LINK_TEXT}}"
SEC_DOWNLOAD_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/opensource/\">{{FAH_OPENSOURCE_FAQ_LINK_TEXT}}</a>{{MISSING_LI_CLOSE}}"
DOWNLOAD_LINUX_IMG_LINK="{{BASE_RESOURCE_DIR}}images/download_deb_rpm.png"
LINUX_PACKAGE_IMG_LINK="{{BASE_RESOURCE_DIR}}images/Install_deb_rpm.png"
LINUX_CONFIG_PACKAGE_IMG_LINK="{{BASE_RESOURCE_DIR}}images/deb_config.png"
SEC_CONFIGURE_DESCRIPTION="{{SEC_CONFIGURE_DESCRIPTION_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"https://client.foldingathome.org\">https://client.foldingathome.org</a>{{SEC_CONFIGURE_DESCRIPTION_AFTER}}"
SEC_CONFIGURE_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://client.foldingathome.org\">https://client.foldingathome.org</a> - {{CONFIGURE_WEB_LINK_TEXT}}"
SEC_INFO_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/\">{{FAH_FAQ_LINK_TEXT}}</a>"
SEC_INFO_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/linux/manual-installation-advanced/\">{{FAH_ADVANCED_NIX_INSTALL_INFO_LINK_TEXT}}</a>"
SEC_INFO_LIST_3="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/configuration-guide/\">{{FAH_ADVANCED_CONFIG_INFO_LINK_TEXT}}</a>{{MISSING_LI_CLOSE}}"
PASSKEY_IMG_LINK="{{BASE_RESOURCE_DIR}}images/folding-stanford-PasskeyForm.png"
SEC_PASSKEY_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://apps.foldingathome.org/passkey/create\">{{FAH_PASSKEY_SIGNUP_LINK_TEXT}}</a>"
SEC_PASSKEY_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/points/passkey/\">{{FAH_PASSKEY_GUIDE_LINK_TEXT}}</a>"
GPU_CONFIG_IMG_LINK="{{BASE_RESOURCE_DIR}}images/Configure-Slots-Add-FoldingSlots.png"
SEC_GPU_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/configuration-guide/\">{{FAH_ADVANCED_CONFIG_GPU_LINK_TEXT}}</a>"
#  @insert@ default for overridde
HTML_COMMENT_OUTPUT_PLACEHOLDER="\n<!--\n\t{{HTML_COMMENT_OUTPUT}}\n-->"
#  @insert@ default for p0, overridden @insert@
HTML_LINK_ALTERNATIVES="{{HTML_LINK_ALTERNATIVES_REPEAT}}"
#  @insert@ replace old or default
LANGUAGE_CHOOSER="<div id=\"rtcorner\">\n\t\t\t\t\t<div id=\"lang\" name=\"lang\">\n\t\t\t\t\t\t<img src=\"{{BASE_RESOURCE_DIR}}images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" /><select id=\"langsel\" name=\"langsel\" style=\"display:none;\">\n\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_OPTIONS}}\n\t\t\t\t\t\t</select>\n\t\t\t\t\t\t<noscript>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_LIST_ITEMS}}\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</noscript>\n\t\t\t\t\t</div>\n\t\t\t\t</div>"
