Release date: Nov, 18, 2021

Asset: main
Page Version: 2
Template Version: 1

Languages Included:
English
German (de)
French (fr)

This version includes only the main and sponsors page assets. Links to other pages are marked as English and link to only the default file.

The English page was assembled from main_p2_en.html.
The German and French pages were assembled from translated main_p0_en.html
and converted with main_p0_t1_to_p2_t1.tpcon
