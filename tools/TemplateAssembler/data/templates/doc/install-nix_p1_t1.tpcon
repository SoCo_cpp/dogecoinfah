#--------------------------
@START_HEADER
#--------------------------
PAGE_ASSET="doc-install-nix"
PAGE_VERSION="1"
TEMPLATE_TYPE="content"
TEMPLATE_VERSION="1"
TEMPLATE_DATE="12-2-2021"
TEMPLATE_NOTES="Can input page versions 0 or 1 to make version p1 cont2 output."
#--------------------------
# override for doc language directory structure
CURRENT_PAGE_REL_FILEPATH="../../"
# override for doc language directory structure
CURRENT_PAGE_ALT_LANG_FILEPATH="../"
#--------------------------
# @insert@ overrides to normalize without space
WEBSITE_TITLE="DogecoinFah"
# @insert@ overrides to normalize caps
FAH_NAME_HOME="home"
# @insert@ overrides to relative
BASE_RESOURCE_DIR="../"
# @insert@ overrides to normalize separator
BREADCRUMB_SEPARATOR_FIX="-"
# @insert@ overrides to normalize title
HTML_TITLE="Dogecoin Folding @ Home"
# @insert@ overridden
MISSING_LI_CLOSE="</li>"
# @insert@ overridden
SEC_INSTALL_ANCHOR="install-install"
# @insert@ overridden
SEC_CONFIGURE_ANCHOR="install-config"
# @insert@ overridden
SEC_INFO_ANCHOR="install-more"
# @insert@ overridden
SEC_CLOSING_ANCHOR="thats-it"
# @insert@ overrides install image for duplicated download image
LINUX_PACKAGE_INSTALL_IMG_FILE="Install_deb_rpm.png"
#--------------------------
@RENAME_IN("LANGUAGE_CHOOSER")="LANGUAGE_CHOOSER_OLD"
@RENAME_IN("HTML_COMMENT_OUTPUT_PLACEHOLDER")="HTML_COMMENT_OUTPUT_PLACEHOLDER_DISCARD"
#--------------------------
@END_HEADER
#--------------------------
HTML_TITLE="Dogecoin Folding @ Home"
HTML_DESCRIPTION="DogecoinFah project how-to document for installing and configuring Folding at Home on Linux like platforms"
HTML_KEYWORDS="dogecoin,folding @ home,linux,installing,configuring,folding"
#--------------------------
HTML_COMMENT_OUTPUT_PLACEHOLDER_DISCARD=""
HTML_COMMENT_OUTPUT_PLACEHOLDER="\n<!--\n\t{{HTML_COMMENT_OUTPUT}}\n-->"
# @insert@ default for overridde
HTML_COMMENT_OUTPUT_PLACEHOLDER="\n<!--\n\t(assembler comments here)\n-->"
#--------------------------
HTML_LINK_ALTERNATIVES="<link rel=\"alternate\" hreflang=\"en\" href=\"../en/InstallLinuxMac.html\" />\n\t\t<link rel=\"alternate\" hreflang=\"de\" href=\"../de/InstallLinuxMac.html\" />\n\t\t<link rel=\"alternate\" hreflang=\"fr\" href=\"../fr/InstallLinuxMac.html\" />"
# @insert@ default for p0, overridden
HTML_LINK_ALTERNATIVES="<!-- Link Alternatives Here -->"
#--------------------------
# just grab the icon alt, discard rest
LANGUAGE_CHOOSER_OLD="<div id=\"lang\" name=\"lang\">\n\t\t\t\t<img src=\"../images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" />\n\t\t\t\t<select id=\"langsel\" name=\"langsel\">\n\t\t\t\t\t<option value=\'en\'>English</option>\n\t\t\t\t\t<option value=\'de\'>Deutsch (German)</option>\n\t\t\t\t\t<option value=\'fr\'>Français (French)</option>\n\t\t\t\t</select>\n\t\t\t\t<noscript>{{LANGUAGE_CHOOSER_NOSCRIPT_DISCARD}}</noscript>\n\t\t\t</div>"
#LANGUAGE_CHOOSER_OLD="<img src=\"../images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" />\n\t\t\t\t<select id=\"langsel\" name=\"langsel\">\n\t\t\t\t\t<option value=\'en\'>English</option>\n\t\t\t\t\t<option value=\'de\'>Deutsch (German)</option>\n\t\t\t\t\t<option value=\'fr\'>Français (French)</option>\n\t\t\t\t</select>\n\t\t\t\t<noscript>{{LANGUAGE_CHOOSER_NOSCRIPT_DISCARD}}</noscript>"
LANGUAGE_CHOOSER="<div id=\"rtcorner\">\n\t\t\t\t\t<div id=\"lang\" name=\"lang\">\n\t\t\t\t\t\t<img src=\"{{BASE_RESOURCE_DIR}}images/language-icon-org.png\" alt=\"{{LANGUAGE_CHOOSER_ICON_IMG_ALT}}\" /><select id=\"langsel\" name=\"langsel\" style=\"display:none;\">\n\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_OPTIONS}}\n\t\t\t\t\t\t</select>\n\t\t\t\t\t\t<noscript>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t{{LANGUAGE_CHOOSER_LIST_ITEMS}}\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</noscript>\n\t\t\t\t\t</div>\n\t\t\t\t</div>"
# @insert@ replace old or default 
LANGUAGE_CHOOSER="<div id=\"rtcorner\">\n\t\t\t\t\t<div id=\"lang\" name=\"lang\">\n\t\t\t\t\t\t<img src=\"../images/language-icon-org.png\" alt=\"blue universal language selector icon by language icon org\" /><select id=\"langsel\" name=\"langsel\" style=\"display:none;\">\n\t\t\t\t\t\t\t<!--Language Chooser Options List Here-->\n\t\t\t\t\t\t</select>\n\t\t\t\t\t\t<noscript>\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<!--Language Chooser List Items Here-->\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</noscript>\n\t\t\t\t\t</div>\n\t\t\t\t</div>"
#--------------------------
BASE_RESOURCE_DIR="../"
WEBSITE_LOGO_LINK="{{BASE_RESOURCE_DIR}}images/dfahlogo_sm.png"
WEBSITE_LOGO_IMG_ALT="DogecoinFah logo made from a brightly colored computer visualization of a protein wrapping around the Dogecoin letter D logo"
WEBSITE_TITLE_LINK="{{@PAGE_FULL_LINK(main)}}"
# overridden
WEBSITE_TITLE="DogecoinFah"
#--------------------------
BREADCRUMB_HOME_URL="{{@PAGE_LOCAL_LINK(main)}}"
BREADCRUMB_HOME="Home"
BREADCRUMB_GETTING_STARTED_URL="{{@DOC_PAGE_REL_LINK(doc-getting-started)}}"
BREADCRUMB_GETTING_STARTED="Getting Started"
BREADCRUMB_CURRENT="Install &amp; Configure (Linux/OSX)"
#--------------------------
SEC_INTRO_ANCHOR="intro"
SEC_INTRO_TITLE="{{WEBSITE_TITLE}} - {{SEC_INTRO_TITLE_TEXT}}"
SEC_INTRO_CONTENT="Folding is so much easier than mining. We are going to show you how to download the Folding@home client, install it, and configure it."
#--------------------------
SEC_SOFTWARE_ANCHOR="software"
SEC_SOFTWARE_TITLE="The Software"
SEC_SOFTWARE_LIST_TITLE_TEXT="comes in 3 separate parts:"
# overridden
FAH_NAME_HOME="home"
SEC_SOFTWARE_LIST_TITLE="Folding@{{FAH_NAME_HOME}} {{SEC_SOFTWARE_LIST_TITLE_TEXT}}"
SEC_SOFTWARE_LIST_1="FahClient - The main folding client that does all the work in the background."
SEC_SOFTWARE_LIST_2="FahControl - An optional graphical configuration and monitoring tool."
SEC_SOFTWARE_LIST_3="FahViewer - An optional graphical protein folding visualizer."
SEC_SOFTWARE_DESCRIPTION="FahClient is the main Folding@home client and the only part that is required. The client can be configured through a web interface or manually. If you\'d prefer an informational graphical tool to configure and monitor your Folding@home client, you can install the FahControl tool. Additionally, the FahViewer will graphically visualize your current folding work in 3D."
#--------------------------
SEC_DOWNLOAD_ANCHOR="install-download"
SEC_DOWNLOAD_TITLE="Downloading"
SEC_DOWNLOAD_DESCRIPTION="Folding@home is no longer in most Linux package managers. You\'ll need to download the appropriate package from Folding@home\'s download page. While built on open source technology, Folding@home is not open source and these are precompiled binary packages."
SEC_DOWNLOAD_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://foldingathome.org/#downloads\">https://foldingathome.org/#downloads</a>{{FAH_DOWNLOAD_LINK_TEXT}}"
SEC_DOWNLOAD_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/opensource/\">{{FAH_OPENSOURCE_FAQ_LINK_TEXT}}</a>{{MISSING_LI_CLOSE}}"
# overridden
MISSING_LI_CLOSE=""
DOWNLOAD_LINUX_IMG_LINK="{{BASE_RESOURCE_DIR}}images/download_deb_rpm.png"
DOWNLOAD_LINUX_IMG_ALT="Image of Linux package download section"
#--------------------------
# overridden
SEC_INSTALL_ANCHOR="install-install"
SEC_INSTALL_TITLE="Installing"
SEC_INSTALL_DESCRIPTION="OSX and most Linux distributions come with a graphical package installer tool as well as command line package management tools, which can install these packages on your system."
LINUX_PACKAGE_INSTALL_IMG_FILE="Install_deb_rpm.png"
LINUX_PACKAGE_IMG_LINK="{{BASE_RESOURCE_DIR}}images/{{LINUX_PACKAGE_INSTALL_IMG_FILE}}"
LINUX_PACKAGE_IMG_ALT="Image of GUI Linux package installer tools"
SEC_INSTALL_DESCRIPTION_PACKAGE="Use the appropriate tool, and if in doubt, you can probably double click on the file to get started installing the package. The package may prompt you for some initial configurations. While there are multiple ways to configure the Folding@home client, you might want to consider if you want folding to start automatically when your computer boots here, as it might be tricky to setup later later."
LINUX_CONFIG_PACKAGE_IMG_LINK="{{BASE_RESOURCE_DIR}}images/deb_config.png"
LINUX_CONFIG_PACKAGE_IMG_ALT="Image of Fahclient deb package configuration prompt during install"
#--------------------------
# overridden
SEC_CONFIGURE_ANCHOR="install-config"
SEC_CONFIGURE_TITLE="Configuring"
SEC_CONFIGURE_DESCRIPTION="{{SEC_CONFIGURE_DESCRIPTION_BEFORE}}<a {{HTML_HREF_ALWAYS_EN}}href=\"https://client.foldingathome.org\">https://client.foldingathome.org</a>{{SEC_CONFIGURE_DESCRIPTION_AFTER}}"
SEC_CONFIGURE_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://client.foldingathome.org\">https://client.foldingathome.org</a> - {{CONFIGURE_WEB_LINK_TEXT}}"
#--------------------------
# overridden
SEC_INFO_ANCHOR="install-more"
SEC_INFO_TITLE="Installing / Configuring - More Info"
SEC_INFO_DESCRIPTION="For detailed information on installing Folding@home, see their FAQ section:"
SEC_INFO_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/\">{{FAH_FAQ_LINK_TEXT}}</a>"
SEC_INFO_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/linux/manual-installation-advanced/\">{{FAH_ADVANCED_NIX_INSTALL_INFO_LINK_TEXT}}</a>"
SEC_INFO_LIST_3="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/configuration-guide/\">{{FAH_ADVANCED_CONFIG_INFO_LINK_TEXT}}</a>{{MISSING_LI_CLOSE}}"
# overridden
MISSING_LI_CLOSE=""
#--------------------------
SEC_PASSKEY_ANCHOR="passkey"
SEC_PASSKEY_TITLE="Extra - Passkey"
SEC_PASSKEY_DESCRIPTION="Registering for a passkey is not required, but gives you bonus points when folding. You are emailed a passkey to configure in your FahClient, when you sign up. Folding@home\'s Passkey page asks for your email address and folding user name. This user name is the Dogecoin wallet address you will be folding as."
PASSKEY_IMG_LINK="{{BASE_RESOURCE_DIR}}images/folding-stanford-PasskeyForm.png"
PASSKEY_IMG_ALT="passkey screenshot"
SEC_PASSKEY_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://apps.foldingathome.org/passkey/create\">{{FAH_PASSKEY_SIGNUP_LINK_TEXT}}</a>"
SEC_PASSKEY_LIST_2="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/points/passkey/\">{{FAH_PASSKEY_GUIDE_LINK_TEXT}}</a>"
#--------------------------
SEC_GPU_ANCHOR="gpu"
SEC_GPU_TITLE="Extra - Configure GPU"
SEC_GPU_DESCRIPTION="Folding@home supports folding with some graphics cards as well. Up to date proprietary drivers may be required. To enable GPU folding, you configure your FahClient by adding a folding slot for your GPU."
GPU_CONFIG_IMG_LINK="{{BASE_RESOURCE_DIR}}images/Configure-Slots-Add-FoldingSlots.png"
GPU_CONFIG_IMG_ALT="configure gpu screenshot"
SEC_GPU_LIST_TITLE="See the configuration guide for more details."
SEC_GPU_LIST_1="<a {{HTML_HREF_ALWAYS_EN}}href=\"https://test.foldingathome.org/support/faq/installation-guides/configuration-guide/\">{{FAH_ADVANCED_CONFIG_GPU_LINK_TEXT}}</a>"
#--------------------------
# overridden
SEC_CLOSING_ANCHOR="thats-it"
SEC_CLOSING_TITLE="That\'s it!"
SEC_CLOSING_DESCRIPTION="After you\'ve configured your username, team, and (optionally)passkey, Folding@home will continue working in the back ground. You can close the web interface or FahControl application and forget about it. That\'s all there is to it!"
