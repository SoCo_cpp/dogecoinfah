<?php
/*	FoldAPIProxy.php
*
*	Provides optimized access to some bandwidth heavy Folding@home APIs.
*/

define('ACCESS_KEY', 'CHANGEME');
define('USE_TEST_DATA', false); // expected at: ./data/daily_user_summary_team_$TEAMNUMBER.txt.full.bz2
define('REQUIRE_PARSE_DATA_TIMESTAMP', true); // first data line is timestamp. Disable this when using trimmed/grep'd test data.
define('DATA_DIR', './data/');
define('FAH_APP_URL', "https://apps.foldingathome.org/daily_user_summary.txt.bz2");
define('FAH_APP_CMP_FILE_EXT', '.bz2');
define('FAH_APP_FILE_EXT', '.txt');

error_reporting(E_ALL); // E_ALL for all or 0 (zero) for none
set_time_limit(0);
ignore_user_abort(false);
ini_set('output_buffering', 0);
ini_set('zlib.output_compression', 0);

$lastError = "success";
$lastDetails = "";

function getRequestIP()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
	else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	return $_SERVER['REMOTE_ADDR'];
}
function fail($code, $msg)
{
	$now = new DateTime('now');
	$strDate = $now->format("Y-m-d H:i:s");
	$reqip = getRequestIP();
	
	error_log("$strDate [$reqip] FAIL: $msg\n", 3, DATA_DIR."error.log");
	http_response_code($code);
	print($msg);
	die();
}

function warn($msg)
{
	$now = new DateTime('now');
	$strDate = $now->format("Y-m-d H:i:s");
	$reqip = getRequestIP();
	error_log("$strDate [$reqip] WARN: $msg\n", 3, DATA_DIR."error.log");
}

function userSummaryFileName($teamId, $cmp = true)
{
	return DATA_DIR."daily_user_summary_team_$teamId".FAH_APP_FILE_EXT.($cmp === true ? FAH_APP_CMP_FILE_EXT : "");
}

function XVarDump(&$Var, $flatten = true)
{
	//-----------------------------------
	ob_start();
	var_dump($Var);
	$s = ob_get_contents();
	ob_end_clean();
	//-----------------------------------
	if ($flatten === true)
	{
		$lines = explode("\n", $s);
		$s = '';
		foreach ($lines as $l)
			$s .= trim($l)." ";
	}
	//-----------------------------------
	return $s;
}

function extractBz2File($inFile)
{
	global $bzip2_cmd, $lastDetails;
	if (!file_exists($bzip2_cmd))
		return "extractBz2File configured bzip2 command not found: ".XVarDump($bzip2_cmd);
	//------------------
	$cmd = $bzip2_cmd." -d -f '$inFile' 2>&1"; // -d decompress, -f force overwrite existing output file  2>&1
	$retValue = exec($cmd, $output, $value); 
	$lastDetails = "extractBz2File inFile ".XVarDump($inFile)." cmd ".XVarDump($cmd).", retValue ".XVarDump($retValue).", output ".XVarDump($output).", value ".XVarDump($value);
	if ($value != 0)
		return "extractBz2File exec bzip2 failed. Details: $lastDetails";
	//------------------
	return true;
}

function compressBz2File($inFile)
{
	global $bzip2_cmd, $lastDetails;
	//------------------
	if (!file_exists($bzip2_cmd))
		return "compressBz2File configured bzip2 command not found: ".XVarDump($bzip2_cmd);
	//------------------
	$cmd = $bzip2_cmd." --best -f '$inFile' 2>&1"; // -f force overwrite existing output file
	$retValue = exec($cmd, $output, $value); 
	$lastDetails = "compressBz2File inFile ".XVarDump($inFile)." cmd ".XVarDump($cmd).", retValue ".XVarDump($retValue).", output ".XVarDump($output).", value ".XVarDump($value);
	if ($value != 0)
		return "compressBz2File exec bzip2 failed. Details: $lastDetails";
	//------------------
	return true;
}

function download($dataSource, $outputFileName)
{
	global $wget_cmd, $lastDetails;
	if ($wget_cmd !== false)
		$result = downloadWget($dataSource, $outputFileName);
	else
		$result = downloadPHP($dataSource, $outputFileName);
	if ($result !== true)
		return $result.", downloadX details: $lastDetails";
	if (!file_exists($outputFileName))
		return "download validate output file exists after download failed. output file name: '$outputFileName', downloadX details: $lastDetails";
	return true;
}

function downloadWget($dataSource, $outputFileName)
{
	global $wget_cmd, $lastDetails;
	//------------------
	if (!file_exists($wget_cmd))
		return "downloadWget configured wget command not found: ".XVarDump($wget_cmd);
	//------------------
	$wgetLog = DATA_DIR."wget.log";
	$cmd = $wget_cmd." -N -q -S --append-output '$wgetLog'  -t 3 -O '$outputFileName' $dataSource 2>&1"; // -N use timestamping to only download changed -nv no verbose, -t 3 retries, -O output file
	$retValue = exec($cmd, $output, $value); 
	$lastDetails = "downloadWget dataSource ".XVarDump($dataSource).", outputFileName ".XVarDump($outputFileName).", cmd ".XVarDump($cmd).", retValue ".XVarDump($retValue).", output ".XVarDump($output).", value ".XVarDump($value);
	if ($value != 0)
		return "downloadWget exec wget failed. value: $value, cmd: '$cmd', output: ".XVarDump($output).", retValue: ".XVarDump($retValue);
	//------------------
	return true;
}

function downloadPHP($dataSource, $outputFileName)
{
	global $lastDetails;
	//------------------
	warn("Using fallback PHP downloader");
	//------------------
	$lastDetails = "downloadPHP dataSource ".XVarDump($dataSource).", outputFileName ".XVarDump($outputFileName);
	//------------------
	$hout = fopen($outputFileName, 'wb');
	if ($hout === false)
		return "downloadPHP fopen failed to open output file: $outputFileName";
	//------------------
	$hin = fopen($dataSource, 'rb');
	if ($hin === false)
		return "downloadPHP fopen failed to open remote page: $dataSource";
	//------------------
	$readFailed = false;
	$writeFailed = false;
	//------------------
	$ioLen = 0;
	while (!feof($hin) && !$readFailed && !$writeFailed) 
	{
		$data = fread($hin,  8192);
		if ($data === false)
			$readFailed = true;
		else if (false === fwrite($hout, $data))
			$writeFailed = true;
		else
			$ioLen += strlen($data);
	}
	//------------------
	$lastDetails .= ", ioLen ".XVarDump($ioLen).", readFailed ".XVarDump($readFailed).", writeFailed ".XVarDump($writeFailed);
	//------------------
	if (!fclose($hin))
		return "downloadPHP fclose remote handle failed";
	//------------------
	if (!fclose($hout))
		return "downloadPHP fclose local file handle failed";
	//------------------
	if ($readFailed || $writeFailed)
		return "downloadPHP read/write failed. read: ".XVarDump($readFailed).", write: ".XVarDump($writeFailed).", output file name: $outputFileName";
	//------------------
	return true;
}

function getTempFileName($appendExtention = "")
{
	global $lastError;
	//------------------
	$tempDir = sys_get_temp_dir(); // shouldn't fail
	if ($tempDir === false || $tempDir == "")
	{
		$lastError = "getTempFileName validate sys_get_temp_dir failed, returned: ".XVarDump($tempDir);
		return false;
	}
	//------------------
	$tempFileName = tempnam($tempDir, "daily_user_summary_");
	if ($tempFileName === false || $tempFileName == "")
	{
		$lastError = "getTempFileName validate tempnam failed, returned: ".XVarDump($tempFileName);
		return false;
	}
	$tempFileName .= $appendExtention;
	//------------------
	return $tempFileName;
}

// returns int timestamp on success, or string error message on failure
function getDataTimeStamp($inFile)
{
	// Line example: "Sun Aug 21 03:16:48 GMT 2022\n"
	$fileSize = @filesize($inFile);
	$fhIn = @fopen($inFile, 'r');
	if (!$fhIn) 
		return "getDataTimeStamp failed to open input file: ".XVarDump($inFile);
	$firstLine = @fgets($fhIn, 2000);
	if (!@fclose($fhIn))
		return "getDataTimeStamp close input file failed";
	if (!is_string($firstLine)) // fgets returns false on failure
		return "getDataTimeStamp failed to read first line of input file (size ".($fileSize === false ? "[Get file size failed]" : $fileSize)."): ".XVarDump($inFile);
	$firstLine = trim($firstLine);
	$timeStamp = false;
	if ($firstLine !== "")
	{
		$dtTimeStamp = date_create($firstLine);
		if ($dtTimeStamp)
			$timeStamp = date_timestamp_get($dtTimeStamp);
	}
	if ($timeStamp === false)
		return "getDataTimeStamp parse first line into date/time failed. Line read: ".XVarDump($firstLine).", of input file (size ".($fileSize === false ? "[Get file size failed]" : $fileSize)."): ".XVarDump($inFile);
	return $timeStamp; // int
}

function grepTeamMembers($teamId, $inFile, $outFile)
{
	global $grep_cmd, $lastDetails;
	//------------------
	if (!file_exists($grep_cmd))
		return "grepTeamMembers configured grep command not found: ".XVarDump($grep_cmd);
	//------------------
	$cmd = $grep_cmd." \"\\s$teamId$\" $inFile 2>&1 > $outFile"; // redirects stderr to stdout before stdout is redirected to the file
	//------------------
	$retValue = exec($cmd, $output, $value);
	$lastDetails = "grepTeamMembers teamId ".XVarDump($teamId).", inFile ".XVarDump($inFile).", outFile ".XVarDump($outFile).", cmd ".XVarDump($cmd).", retValue ".XVarDump($retValue).", output ".XVarDump($output).", value ".XVarDump($value);
	if ($value != 0)
		return "grepTeamMembers exec grep failed. details: $lastDetails";
	//------------------
	return true;
}

function processData($inFile, $outFile, $version, $teamId)
{
	global $lastDetails;
	$lastDetails = "processData inFile ".XVarDump($inFile).", outFile ".XVarDump($outFile).", version ".XVarDump($version).", teamId ".XVarDump($teamId); //.",  ".XVarDump($)."
	if ($version == 0)
	{
		if (!@rename($inFile, $outFile))
			return "processData version zero simply rename failed. ".XVarDump($inFile)."->".XVarDump($outFile).", details: $lastDetails";
		return true;
	}
	$fhIn = @fopen($inFile, 'r');
	if (!$fhIn) 
		return "processData failed to open input file: ".XVarDump($inFile).", details: $lastDetails";
	$fhOut = @fopen($outFile, 'w');
	if (!$fhOut) 
		return "processData failed to open output file: ".XVarDump($outFile).", details: $lastDetails";
	$lineNum = 0;
	while ( ($line = @fgets($fhIn, 2000)) !== false)
	{
		$cols = explode("\t", $line);
		if (sizeof($cols) != 4)
		{
			@fclose($fhOut);
			@fclose($fhIn);
			return "Line $lineNum doesn't have 4 columns, counted ".sizeof($cols).": ".XVarDump($cols).", details: $lastDetails";
		}
		if (trim($cols[3]) != $teamId)
		{
			@fclose($fhOut);
			@fclose($fhIn);
			return "Line $lineNum teamID doesn't match, teamID expected ".XVarDump($teamId).", got '".trim($cols[3])."': ".XVarDump($cols).", details: $lastDetails";
		}
		$newLine = $cols[0]."\t".$cols[1]."\t".$cols[2]."\n";
		if (@fwrite($fhOut, $newLine) === false)
		{
			@fclose($fhOut);
			@fclose($fhIn);
			return "processData write output file failed at line $lineNum, details: $lastDetails";
		}
		$lineNum++;
	} // while
	$lastDetails .= ", line count ".XVarDump($lineNum);
	if (!@fclose($fhOut))
	{
		if (!@fclose($fhIn))
			return "processData close output and input file failed, details: $lastDetails";
		return "processData close  output file failed, details: $lastDetails";
	}
	if (!@fclose($fhIn))
		return "processData close input file failed, details: $lastDetails";
	return true;
}

function XGet($Name, $Def = "")
{
	return (isset($_GET[$Name]) ? $_GET[$Name] : $Def);
}

function immediatePollUserSummary($teamId, $version)
{
	global $lastDetails;
	if ($teamId === false || !is_numeric($teamId))
		return "immediatePollUserSummary team is invalid: ".XVarDump($teamId);
	if (!is_numeric($version))
		$version = 0;
	$tmpFileName = getTempFileName(FAH_APP_FILE_EXT);
	if ($tmpFileName === false)
		return "immediatePollUserSummary getTempFileName failed: $lastError";
	$tmpFileNameCmp = $tmpFileName.FAH_APP_CMP_FILE_EXT;

	$outFileName = userSummaryFileName($teamId, false /*cmp*/);
	$outFileNameCmp = $outFileName.FAH_APP_CMP_FILE_EXT;
	$outFileNameTmp = $outFileName.'.tmp';
	$inFullFileNameCmp = $outFileName.".full".FAH_APP_CMP_FILE_EXT;
	$fileNameDetails = "immediatePoll file names tmp ($tmpFileName + $tmpFileNameCmp) out ($outFileName + $outFileNameCmp) in full $inFullFileNameCmp";

	if (USE_TEST_DATA === true)
	{
		if (!file_exists($inFullFileNameCmp))
			return "immediatePollUserSummary use test data enabled but source data file not found: $inFullFileNameCmp";
		if (!copy($inFullFileNameCmp, $tmpFileNameCmp))
			return "immediatePollUserSummary test copy previous full failed: $inFullFileNameCmp -> $tmpFileNameCmp, fileNameDetails: $fileNameDetails";
	}
	else
	{
		$result = download(FAH_APP_URL, $inFullFileNameCmp);
		if ($result !== true)
			return "immediatePollUserSummary download failed: $result";
		if (!file_exists($inFullFileNameCmp))
			return "immediatePollUserSummary verify downloaded file exists failed. filename: $tmpFileNameCmp, download details: $lastDetails, fileNameDetails: $fileNameDetails";
		if (!copy($inFullFileNameCmp, $tmpFileNameCmp))
			return "immediatePollUserSummary copy full backup failed inFilename: $tmpFileNameCmp, outFilename: $inFullFileNameCmp, fileNameDetails: $fileNameDetails";
	}
	
	$result = extractBz2File($tmpFileNameCmp);
	if ($result !== true)
		return "immediatePollUserSummary extractBz2File tmp '$tmpFileNameCmp' failed: $result, fileNameDetails: $fileNameDetails";
	if (!file_exists($tmpFileName))
		return "immediatePollUserSummary verify extracted file exists failed. extracted filename: $tmpFileName, extract details: $lastDetails, fileNameDetails: $fileNameDetails";
	if (file_exists($tmpFileNameCmp)) // extraction should have already deleted this
		unlink($tmpFileNameCmp);

	$dataTimeStamp = getDataTimeStamp($tmpFileName);
	if (!is_numeric($dataTimeStamp))
	{
		if (REQUIRE_PARSE_DATA_TIMESTAMP) 
			return "immediatePollUserSummary getDataTimeStamp failed: ".XVarDump($dataTimeStamp).", fileNameDetails: $fileNameDetails, is test data: ".(USE_TEST_DATA ? "!YES! (disable REQUIRE_PARSE_DATA_TIMESTAMP to ignore)" : "NO");
		else
		{
			warn("immediatePollUserSummary getDataTimeStamp failed, but REQUIRE_PARSE_DATA_TIMESTAMP is disabled. Ignoring and setting to current timestamp. Is test data: ".(USE_TEST_DATA ? "YES" : "NO").", failure message (ignored): ".XVarDump($dataTimeStamp));
			$dateTimeStamp = time();
		}
	}
	else if (USE_TEST_DATA)
		warn("Using test data time-stamped: ".date('Y-m-d H:i:s T P', $dataTimeStamp).", from file '$inFullFileNameCmp', modified ".date('Y-m-d H:i:s T P', filemtime($inFullFileNameCmp)));

	$result = grepTeamMembers($teamId, $tmpFileName, $outFileNameTmp, $version);
	if ($result !== true)
		return "immediatePollUserSummary grepTeamMembers failed: $result";
	if (!file_exists($outFileNameTmp))
		return "immediatePollUserSummary verify grepped file exists failed. filename: $outFileNameTmp, grep details: $lastDetails, fileNameDetails: $fileNameDetails";

	$result = processData($outFileNameTmp, $outFileName, $version, $teamId);
	if ($result !== true)
		return "immediatePollUserSummary process data failed: $result";
	if (!file_exists($outFileName))
		return "immediatePollUserSummary verify processed file exists failed. filename: $outFileName, processData details: $lastDetails, fileNameDetails: $fileNameDetails";
	if (file_exists($outFileNameTmp))
		unlink($outFileNameTmp);

	$result = compressBz2File($outFileName);
	if ($result !== true)
		return "immediatePollUserSummary compressBz2File failed: $result, fileNameDetails: $fileNameDetails";
	if (!file_exists($outFileNameCmp))
		return "immediatePollUserSummary verify grepped and compressed file exists failed. compressed filename: $outFileNameCmp, compress details: $lastDetails, fileNameDetails: $fileNameDetails";

	$result = replyWithFile($outFileNameCmp);
	if ($result !== true)
		return "immediatePollUserSummary replyWithFile failed: $result, fileNameDetails: $fileNameDetails";
	return true;
}

function replyWithFile($fileName)
{
	$fileName = realpath($fileName);
	$finfo = pathinfo($fileName);
	$baseFileName = $finfo['basename'];
	if (!file_exists($fileName))
		return "replyWithFile file doesn't exist: $fileName";
	$fp = @fopen($fileName, 'rb');
	if ($fp === false)
		return "replyWithFile file exists, but open file for reading failed: $fileName";
	$fileSize = filesize($fileName);
	$chunkSize = 5 * (1024 * 1024); // 5 MB (5,242,880 bytes)
	//warn("replyWithFile size: ".XVarDump($fileSize).", fileName: $fileName");

	ob_end_clean(); // disable output buffering
	ob_start();
	header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	//header("Cache-Control: public"); // needed for internet explorer
	header('Cache-Control: public, must-revalidate, max-age=0');
	header('Pragma: no-cache');
  	header("Content-Type: application/zip");
  	header("Content-Type: application/octet-stream");
	header("Content-Transfer-Encoding: Binary");
	header("Content-Length: $fileSize");
	header("Content-Disposition: attachment; filename=$baseFileName");
	header("Connection: close");

	//readfile($fileName);

	while (!feof($fp))
	{
		print(@fread($fp, $chunkSize));
		ob_flush();
		flush();
	}
	@fclose($fp);
	ob_end_clean();
	die();        
	return true; // won't get here
}
function handleCommands()
{
	$key = XGet("key", false);
	if ($key === false || $key != ACCESS_KEY)
	{
		sleep(5);
		fail(401, "ERROR: Validate authorization key failed"); // dies
	}
	$cmd = XGet("cmd", false);
	$teamId = XGet("team", false);
	$version = XGet("ver", false);
	if ($cmd == "usersummary") // immediate poll
	{
		$result = immediatePollUserSummary($teamId, $version);
		if ($result !== true)
			return "handleCommands immediatePollUserSummary failed: $result";
		return true;
	}
	else if ($cmd == "lastusersummary")
	{
		$outFileName = userSummaryFileName($teamId);
		if (!file_exists($outFileName))
			$result = immediatePollUserSummary($teamId, $version); // no cached file, poll it
		else
			$result = replyWithFile($outFileName); // hope version matches
		if ($result !== true)
			return "handleCommands last failed: $result";
		return true;		
	}
	fail(400, "ERROR: handleCommands unhandled cmd: ".XVarDump($cmd).", team: ".XVarDump($teamId)); // dies
}


$wget_cmd = exec("which wget", $output, $value);
if ($value != 0 || $wget_cmd == "")
	$wget_cmd = false;

$bzip2_cmd = exec("which bzip2", $output, $value);
if ($value != 0 || $bzip2_cmd == "")
	fail(500, "ERROR: failed to resolve bzip2 command. Output: ".XVarDump($output).", value: ".XVarDump($value)); // dies

$grep_cmd = exec("which grep", $output, $value);
if ($value != 0 || $grep_cmd  == "")
	fail(500, "ERROR: failed to resolve grep command. Output: ".XVarDump($output).", value: ".XVarDump($value)); // dies

$oldUmask = umask(0002);
$result = handleCommands();
umask($oldUmask);
if ($result !== true)
	fail(500, "ERROR: handleCommands failed: $result"); // dies


?>
